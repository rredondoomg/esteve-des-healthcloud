/**
 * @description       : Data Factory
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 09-02-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
public with sharing class ETH_DataFactory {
    public static final String SETTINGS_NAME = 'sharepointComponent';
    public static final String USERNAME = 'testcontentUser@content.omegacrconsulting.com';

    /**
    * @description Creates an account with an address
    * @author fizquierdo@omegacrmconsulting.com | 01-25-2022 
    * @param String country 
    * @param String city 
    * @param String street 
    * @param String postalCode 
    * @param String province 
    * @return Account 
    **/
    public static Account createAccountWithAddress(String country, String city, String street, String postalCode, String province){
        Account account = new Account(
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId(),
            ShippingCity = city,
            ShippingCountry = country,
            ShippingStreet = street,
            ShippingPostalCode = postalCode,
            ShippingState = province,
            LastName = 'Test Account',
            Suffix = 'Test Suffix',
            FirstName = 'Test First Name'
        );
        return account;
    }

    public static void createTimeSlots(Id operatingHoursId, Integer horaInicio, Integer horaFin){
        List<String> days = new List<String>{'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'};
        List<TimeSlot> slotsToInsert = new List<TimeSlot>();
        for(String day: days){
            TimeSlot timeslot = new TimeSlot(
                OperatingHoursId = operatingHoursId,
                DayOfWeek = day,
                StartTime = Time.newInstance(horaInicio, 0, 0, 0),
                EndTime = Time.newInstance(horaFin, 0, 0, 0)
            );
            slotsToInsert.add(timeslot);
        }
        insert slotsToInsert;
    }

    /**
    * @description Create an user with the PermissionSet and insert some settings for the Omega Content Manager
    * @author fizquierdo@omegacrmconsulting.com | 02-08-2022 
    **/
    public static void setUpOmegaContent(){
        // To test the share funcionality
        // Insert user with corect permission set for testing (any profile)
        Profile pf = [SELECT Id FROM Profile WHERE UserLicense.Name = 'Salesforce' LIMIT 1];
        User u = new User(
            Username = USERNAME,
            FirstName = 'Test',
            LastName = 'Omega',
            ProfileId = pf.Id,
            Email = USERNAME,
            Alias = 'test',
            TimeZoneSidKey = 'GMT',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'ISO-8859-1',
            LanguageLocaleKey = 'en_US'
        );

        insert u;

        PermissionSet contUser = [SELECT Id FROM PermissionSet WHERE Name = 'OM_ContentUser' LIMIT 1];
        PermissionSetAssignment psass = new PermissionSetAssignment(
            AssigneeId = u.Id,
            PermissionSetId = contUser.Id
        );

        insert psass;


        System.runAs(u){
            // Insert some settings
            OM_ContentSetting__c settings = new OM_ContentSetting__c(
                OM_Name__c = SETTINGS_NAME,
                OM_Allow_folder_creation__c = true,
                OM_Allow_navigation__c = true,
                OM_Allow_upload_files__c = true,
                OM_Allow_edit__c = true,
                OM_Auth_type__c = 'Shared',
                OM_Drive_id__c = 'test',
                OM_Use_personal_drive__c = false,
                OM_Client_id__c = 'test',
                OM_Client_secret__c = 'test',
                OM_Discovery_docs__c = 'test',
                OM_Provider__c = 'MicrosoftSharePoint',
                OM_Site_id__c = 'test',
                OM_Tenant__c = 'test',
                OM_Scope__c = 'test',
                OM_Redirect_uri__c = 'https://testorg.visualforce.com/apex/OM_OAuthCallback',
                OM_Columns__c = 'name;size'
            );
            insert settings;

            OM_ContentService.updateUserAccess(settings, UserInfo.getUserId(), '{"access_token": "test_token", "refresh_token": "test_refresh"}');
        }
    }

    public static MultiStaticResourceCalloutMock getGeneralMock() {
        Google_Maps_API__mdt api = [SELECT ZonasConflictivas__c, API_Key__c, URL__c FROM Google_Maps_API__mdt WHERE Label='Geolocation'];

        //set mocks for the calls
        MultiStaticResourceCalloutMock multimock = new MultiStaticResourceCalloutMock();
        multimock.setStaticResource(api.URL__c + '?address=Periodista+Fernando+Gomez+de+la+Cruz+18014+Granada+Spain&key=' + api.API_Key__c, 'MockLocationZonaNoConflictiva');
        multimock.setStaticResource(api.ZonasConflictivas__c, 'MockZonasConflictivas');
        multimock.setStaticResource('https://graph.microsoft.com/v1.0/sites/'+'test'+'/drives/'+ 'test' +'/items/' + 'root' + '/children', 'MockSharePoint');
        // 1234567890 is the ID specified in the mock
        multimock.setStaticResource('https://graph.microsoft.com/v1.0/sites/'+'test'+'/drives/'+ 'test' +'/items/' + '1234567890' + '/children', 'MockSharePoint');
        multimock.setStaticResource('https://api.lleida.net/sms/v2/HTTP/1.1', 'OM_SMSIntegrationStaticResource');
        multimock.setStaticResource('https://des-vidsignerdes.cs106.force.com/VidSignerDes/services/apexrest/PruebaVidSigner/Consentimiento/1234567/docstatus/1cb039f1-a8d4-4a06-aab4-86d445d1ebfe', 'MockVidSigner');        
        multimock.setStaticResource('URL_VidSigner1cb039f1-a8d4-4a06-aab4-86d445d1ebfe', 'MockVidSigner');  
        multimock.setStaticResource('https://graph.microsoft.com/v1.0/sites/test/drives/test/items/root:/directorio:/content', 'MockSharePoint'); 

        //AtlasLogin
        multimock.setStaticResource('http://93.90.29.156:9002/login', 'tokenAtlas'); 
        //Atlas stock-equipo
        multimock.setStaticResource('http://93.90.29.156:9002/stock-equipo', 'stockEquipo'); 
        //Atlas stock-fungible
        multimock.setStaticResource('http://93.90.29.156:9002/stock-fungible', 'stockFungible'); 
        //Atlas fungibles-suministrados
        multimock.setStaticResource('http://93.90.29.156:9002/fungibles-suministrados', 'fungiblesSuministrados'); 
        //Atlas equipos-suministrados
        multimock.setStaticResource('http://93.90.29.156:9002/equipos-suministrados', 'equiposSuministrados'); 
        //Atlas pedido-profesional
        multimock.setStaticResource('http://93.90.29.156:9002/pedido-profesional', 'equiposSuministrados'); 
        //Atlas estado-pedido
        multimock.setStaticResource('http://93.90.29.156:9002/estado-pedido', 'estadoPedido'); 
        //SACYL Facturar tratamiento
        multimock.setStaticResource('http://93.90.29.156:9002/facturar-sacyl', 'facturacionSACYL'); 
        //Hermes Correos Express
        multimock.setStaticResource('https://www.test.cexpr.es/wspsc/apiRestGrabacionEnviok8s/json/grabacionEnvio', 'HermesCorreos'); 
        
        
        multimock.setStatusCode(200);
        return multimock;
    }

    /**
    * @description Used in test to create a full case but without tratamiento
    * @author fizquierdo@omegacrmconsulting.com | 01-25-2022 
    * @param Account acc 
    * @param String negocio 
    * @return Case 
    **/
    public static Case createCaseWithAccountTest(Account acc, String negocio){
        if(negocio == 'TRD'){
            negocio = 'Tratamiento_TRD';
        }
        else if (negocio == 'Pharmate'){
            negocio = 'Tratamiento_Pharmate';
        }
        else if (negocio == 'Service'){
            negocio = 'Service_Case';
        }
        else{
            negocio = 'Tratamiento_TRD';
        }

        Cliente__c cliente = new Cliente__c(
            Cod_cliente__c = '12345',
            Name = 'Test Cliente',
            IdentificacionPac__c = 'DNI;CIF',
            Abreviacion__c = 'CL'
        );
        insert cliente;

        Maestro_UPAB__c maestroUPAB = new Maestro_UPAB__c(
            Name = 'Test Maestro UPAB',
            Territorio__c = 'Cataluña',
            Abreviacion__c = 'UPAB'
        );
        insert maestroUPAB;

        Lote__c lote = new Lote__c(
            Name = 'Test Lote',
            Cod_lot__c = '12345',
            Cliente__c = cliente.Id
        );
        insert lote;

        Abs__c abs = new Abs__c(
            Name = 'Test abs',
            Territorio__c = 'Cataluña'
        );
        insert abs;

        Case ca = new Case(
            Origin = 'Email',
            Type = 'TRD',
            RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(negocio).getRecordTypeId(),
            AccountId = acc.Id,
            Fec_ini_pre__c = Date.newInstance(2022, 1, 1),
            Fec_ini_tra__c = Date.newInstance(2022, 1, 2),
            Status = 'En curso',
            Cliente__c = cliente.Id,
            Tipo_Iden__c = 'DNI',
            Upab__c = maestroUPAB.Id,
            Lote__c = lote.Id,
            ABS__c = abs.Id,
            Fre_trat__c = 30
        );

        return ca;
    }

    /**
    * @description Creates an user with System Admin profile
    * @author fizquierdo@omegacrmconsulting.com | 02-02-2022 
    * @return User 
    **/
    public static User createUser(String email){
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        User user = new User(
            Username = email,
            FirstName = 'Test Name',
            LastName = 'Test LastName',
            ProfileId = pf.Id,
            Email = email,
            Alias = 'alias',
            TimeZoneSidKey = 'Europe/Madrid',
            LocaleSidKey = 'es_ES',
            EmailEncodingKey = 'ISO-8859-1',
            LanguageLocaleKey = 'en_US'
        );
        return user;
    }

    /**
    * @description 
    * @author fizquierdo@omegacrmconsulting.com | 02-02-2022 
    * @param Id user 
    * @return ServiceResource 
    **/
    public static ServiceResource createServiceResourceProfesional(Id user){
        ServiceResource serviceResource = new ServiceResource(
            Name = 'Test profesional',
            ResourceType = 'T',
            RelatedRecordId = user,
            IsActive = true
        );
        return serviceResource;
    }

    /**
    * @description 
    * @author fizquierdo@omegacrmconsulting.com | 02-02-2022 
    * @param Id user 
    * @return ServiceResource 
    **/
    public static ServiceResource createServiceResourceBox(Id assetId){
        ServiceResource serviceResource = new ServiceResource(
            Name = 'Test Box',
            ResourceType = 'S',
            IsActive = true,
            AssetId = assetId
        );
        return serviceResource;
    }

    /**
    * @description 
    * @author fizquierdo@omegacrmconsulting.com | 02-02-2022 
    * @param String postalCode 
    * @param Id operatingHours 
    * @param String negocio. 'TRD' o 'Pharmate' 
    * @return ServiceTerritory 
    **/
    public static ServiceTerritory createServiceTerritory(String postalCode, Id operatingHours, String negocio, Id parentTerritoryId){
        if(negocio == 'TRD'){
            negocio = 'Domicilio_TRD';
        }
        else if(negocio == 'Pharmate'){
            negocio = 'Domicilio_Pharmate';
        }
        else{
            negocio = 'Creta';
        }

        ServiceTerritory serviceTerritory = new ServiceTerritory(
            RecordTypeId = Schema.SObjectType.ServiceTerritory.getRecordTypeInfosByDeveloperName().get(negocio).getRecordTypeId(),
            Name = 'Service Terrritory Test',
            PostalCode = postalCode,
            OperatingHoursId = operatingHours,
            IsActive = true,
            Country = 'Spain',
            ParentTerritoryId = parentTerritoryId
        );
        return serviceTerritory;
    }

    /**
    * @description 
    * @author fizquierdo@omegacrmconsulting.com | 02-08-2022 
    * @param String role 
    * @param String serviceResourceId 
    * @param String ServiceTerritoryId 
    * @return ServiceTerritoryMember 
    **/
    public static ServiceTerritoryMember createServiceTerritoryMember(String role, String serviceResourceId, String serviceTerritoryId, Id operatingHoursId){
        ServiceTerritoryMember member = new ServiceTerritoryMember(
            Roles__c = role,
            ServiceResourceId = serviceResourceId,
            ServiceTerritoryId = serviceTerritoryId,
            OperatingHoursId = operatingHoursId,
            EffectiveStartDate = Date.today().addDays(-50),
            EffectiveEndDate = Date.today().addDays(50)
        );
        return member;
    }

    public static Asset createAsset(Id accountId){
        Asset asset = new Asset(
            Name = 'test asset',
            Activo__c = true,
            AccountId = accountId
        );
        return asset;
    }

    /**
    * @description 
    * @author fizquierdo@omegacrmconsulting.com | 02-02-2022 
    * @return OperatingHours 
    **/
    public static OperatingHours createOperatingHours(){
        OperatingHours operatingHours = new OperatingHours(
            Name = 'Test',
            TimeZone = 'Europe/Madrid'
        );
        return operatingHours;
    }

    /**
    * @description 
    * @author fizquierdo@omegacrmconsulting.com | 02-02-2022 
    * @param String tipo. Alta, Fin, Seguimiento, Incidencia 
    * @param String negocio. Pharmate, TRD
    * @param String formato Virtual, Domicilio, CRETA
    * @return WorkType 
    **/
    public static WorkType createWorktype(String tipo, String negocio, String tipo_lugar, String lugar_visita){
        String name = tipo + ' ' + negocio + ' ' + tipo_lugar + ' ' + '(' + lugar_visita + ')';
        if(negocio == 'No Programada'){
            negocio = 'TRD';
        }
        WorkType worktype = new WorkType(
            Name = name,
            EstimatedDuration = 60,
            DurationType = 'Minutes',
            Linea_de_negocio__c = negocio
        );
        return worktype;
    }

    /**
    * @description 
    * @author fizquierdo@omegacrmconsulting.com | 02-02-2022 
    * @param Id cliente 
    * @param Id protocolo 
    * @return tratamiento__c 
    **/
    public static tratamiento__c createTratamiento(Id cliente, Id protocolo, String lineaNegocio){
        tratamiento__c tratamiento = new tratamiento__c(
            Name = 'Tratamiento test',
            Cod_tra__c = 12345678,
            Cliente__c = cliente,
            val_cod_eth__c = false,
            con_min_dia__c = 12,
            fre_vis__c = 30,
            Terapia__c = 'Oxigenoterapia',
            Protocolo__c = protocolo,
            fre_vis_ini__c = 3,
            Linea_de_negocio__c = lineaNegocio,
            LugaresPresencial__c = 'Domicilio',
            LugaresVirtual__c = 'Domicilio',
            Form_vis__c = 'Protocolos',
            Tratamiento_ET__c = 'Asistente de Tos',
            Nombre_Facturacion__c = 'Nombre Test'
        );
        return tratamiento;
    }

    // public static ServiceAppointment createServiceAppointmentAltaTRD(){
        
    // }

    /**
    * @description 
    * @author fizquierdo@omegacrmconsulting.com | 02-02-2022 
    * @return protocolo__c 
    **/
    public static protocolo__c createProtocolo(){
        protocolo__c protocolo = new protocolo__c(
            Name = 'Protocolo test',
            tipo_prot__c = 'Cíclico',
            Nivel_de_Protocolo__c = 'Tratamiento'
        );
        return protocolo;
    }

    public static void createEventForzarFase2(Id owner, Integer frecuencia){
        //El primer día objetivo, crear un evento
        List<Event> eventsToInsert = new List<Event>();
        DateTime fechaBase = DateTime.now().addDays(frecuencia);
    
        for(Integer i = 0; i < frecuencia/2; i++){
            DateTime fecha = DateTime.newInstance(fechaBase.date(), Time.newInstance(3, 30, 0, 0));
            DateTime fechaFin = DateTime.newInstance(fechaBase.date(), Time.newInstance(23, 0, 0, 0));
            Event evento = new Event(
                OwnerId = owner,
                StartDateTime = fecha,
                EndDateTime = fechaFin
                );
            eventsToInsert.add(evento);
            fechaBase = fechaBase.addDays(1);
        }

        for(Integer i = 0; i < frecuencia/2 +1; i++){
            DateTime fecha = DateTime.newInstance(fechaBase.date(), Time.newInstance(5, 30, 0, 0));
            DateTime fechaFin = DateTime.newInstance(fechaBase.date(), Time.newInstance(15, 0, 0, 0));
            Event evento = new Event(
                OwnerId = owner,
                StartDateTime = fecha,
                EndDateTime = fechaFin
            );
            eventsToInsert.add(evento);

            fecha = DateTime.newInstance(fechaBase.date(), Time.newInstance(15, 30, 0, 0));
            fechaFin = DateTime.newInstance(fechaBase.date(), Time.newInstance(16, 30, 0, 0));
            evento = new Event(
                OwnerId = owner,
                StartDateTime = fecha,
                EndDateTime = fechaFin
            );
            eventsToInsert.add(evento);

            fecha = DateTime.newInstance(fechaBase.date(), Time.newInstance(17, 0, 0, 0));
            fechaFin = DateTime.newInstance(fechaBase.date(), Time.newInstance(18, 30, 0, 0));
            evento = new Event(
                OwnerId = owner,
                StartDateTime = fecha,
                EndDateTime = fechaFin
            );
            eventsToInsert.add(evento);

            fechaBase = fechaBase.addDays(1);
        }

        insert eventsToInsert;
    }

    /**
    * @description 
    * @author fizquierdo@omegacrmconsulting.com | 02-02-2022 
    * @param Id protocolo 
    * @return Paso_Protocolo__c 
    **/
    public static Paso_Protocolo__c createPasoProtocolo(Id protocolo, Integer orden, String tipoLugar, String tipoProfesional, String tipoVisita, String lugarVisita, Integer frecuencia){
        Paso_Protocolo__c paso = new Paso_Protocolo__c(
            Name = 'Paso test',
            frecuencia__c = frecuencia,
            orden__c = orden,
            protocolo_Padre__c = protocolo,
            tipo_Lugar__c = tipoLugar,
            tipo_prof__c = tipoProfesional,
            tipo_visita__c = tipoVisita,
            lugar_visita__c = lugarVisita
        );
        return paso;
    }

    public static EmailMessage createEmailMessage(Case ca){
        EmailMessage emailMessage = new EmailMessage(
            FromAddress = 'test@email.com',
            ToAddress = 'test@email.com',
            Subject = 'Test',
            TextBody = 'Test',
            ParentId = ca.Id
        );
        return emailMessage;
    }
}