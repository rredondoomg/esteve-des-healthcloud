/**
 * @description        Concrete implmentation for Google Drive
 * @author             OmegaCRM 
 * @group              Omega content integration
**/
public with sharing virtual class OM_ContentGoogleIntegration implements OM_ContentIntegrationInterface {
    /**
    * @description Transform content wrapper to provider valid JSON
    * @author OmegaCRM  | 01-20-2021 
    * @param content The content to transform
    * @return String The JSON to work with the provider
    **/
    public String transformContent(OM_ContentWrapper content) {
        return JSON.serialize(content);
    }

    /**
    * @description Transform provider valid JSON to Apex wrapper
    * @author OmegaCRM  | 01-20-2021 
    * @param content The content to transform
    * @return OM_ContentWrapper The wrapper instance
    **/
    public OM_ContentWrapper transformContent(String content) {
        Map<String, Object> rawCnt = (Map<String, Object>) JSON.deserializeUntyped(content);
        OM_ContentWrapper cnt = new OM_ContentWrapper();
        cnt.id = String.valueOf(rawCnt.get('id'));
        cnt.name = String.valueOf(rawCnt.get('name'));
        return cnt;
    }

    class ListResponse {
        public List<OM_ContentWrapper> files {get; set; }
    }

    public List<OM_ContentWrapper> listItems(OM_ContentSetting__c settings, OM_ContentWrapper contentAsFilters) {
        OM_ContentService.CalloutWrapper cout = new OM_ContentService.CalloutWrapper();
        cout.endpoint = OM_ContentService.getEndpoint(settings, 'list');
        cout.method = 'GET';
        cout.headers = new Map<String, String> {
            'Content-Type' => 'application/json',
            'Accept' => '*/*'
        };

        // Add select
        cout.endpoint += '?corpus=user&fields=' +EncodingUtil.urlEncode('nextPageToken, files(id, name, mimeType, iconLink, thumbnailLink, webContentLink, fullFileExtension, fileExtension, size, description, contentHints, createdTime, modifiedTime, lastModifyingUser, webViewLink, owners, parents, appProperties)', 'UTF-8');
        Boolean hasQuery = false;

        // Add filters
        if(contentAsFilters.parentId != null) {
            if(!hasQuery) {
                cout.endpoint += '&q=';
                hasQuery = true;  
            }
            cout.endpoint += EncodingUtil.urlEncode('\'' + contentAsFilters.parentId + '\' in parents ', 'UTF-8');
        }

        if(contentAsFilters.name != null) {
            if(!hasQuery) {
                cout.endpoint += '&q=';
                hasQuery = true;
            } else {
                cout.endpoint += EncodingUtil.urlEncode(' and ', 'UTF-8');
            }
            cout.endpoint += EncodingUtil.urlEncode('name contains \'' + contentAsFilters.name + '\' ', 'UTF-8');
        }

        String response = makeJSONCallout(cout, settings, contentAsFilters, null);
        ListResponse result = (ListResponse) JSON.deserialize(response, ListResponse.class);
        return result.files;
    }

    /**
    * @description Gets the metadata info for a file
    * @author OmegaCRM
    * @param settings The settings to use
    * @param content The content wrapper. Should have the id or the name as path in case of Sharepoint
    * @return OM_ContentWrapper The repesentation of the file
    **/
    public virtual OM_ContentWrapper getItem(OM_ContentSetting__c settings, OM_ContentWrapper content) {
        return null;
    }

    /**
    * @description Updates an file or a folder
    * @author OmegaCRM  | 01-19-2021 
    * @param settings The settings to use
    * @param content The content wrapper. Should have the metadata to be updated
    * @return String The JSON of the response
    **/
    public OM_ContentWrapper updateItem(OM_ContentSetting__c settings, OM_ContentWrapper content) {
        OM_ContentService.CalloutWrapper cout = new OM_ContentService.CalloutWrapper();
        cout.endpoint = OM_ContentService.getEndpoint(settings, 'update', content);
        cout.method = 'PATCH';
        cout.headers = new Map<String, String> {
            'Content-Type' => 'application/json'
        };
        //Remove not-writable fields
        content.id = null;
        content.parents = null;
        cout.body = transformContent(content);
        return transformContent(makeJSONCallout(cout, settings, content, null));
    }

    /**
    * @description Uploads a new item
    * @author OmegaCRM  | 01-19-2021 
    * @param settings The settings to use
    * @param content The content wrapper. Should have the metadata to be updated
    * @param fileData Binary content for upload
    * @return String The JSON of the response
    **/
    public OM_ContentWrapper uploadItem(OM_ContentSetting__c settings, OM_ContentWrapper content, Blob fileData) {
        OM_ContentService.CalloutWrapper cout = new OM_ContentService.CalloutWrapper();
        cout.endpoint = OM_ContentService.getEndpoint(settings, 'uploadNew', content);

        String boundary = 'omegacontent';

        cout.method = 'POST';
        cout.headers = new Map<String, String> {
            'Content-Type' => 'multipart/related; boundary="' + boundary +'"',
            'Content-Length' => String.valueOf(fileData.size()),
            'Accept' => '*/*'
        };

        if(content.parentId != null) {
            content.parents = new List<String>{content.parentId};
        }

        String metadataStr = transformContent(content);
        String multipartBody = '';

        // Metadata part body
        multipartBody += '--' + boundary;
        multipartBody += 'Content-Disposition: form-data; name="metadata"\r\n';
        multipartBody += 'Content-Type: application/json; charset=UTF-8\r\n';
        multipartBody += 'Content-Length: '+ metadataStr.length() +' \r\n\r\n';
        multipartBody += metadataStr;
        multipartBody += '\r\n--' + boundary + '\r\n';

        // Content body
        multipartBody += 'Content-Disposition: form-data; name="file"\r\n';
        multipartBody += 'Content-Transfer-Encoding: base64\r\n';
        multipartBody += 'Content-Type: ' + content.mimeType +'; charset=utf-8\r\n';
        multipartBody += 'Content-Length: ' + fileData.size() + '\r\n\r\n';
        multipartBody += EncodingUtil.base64Encode(fileData);
        multipartBody += '\r\n--' + boundary + '--';
        
        cout.body = multipartBody;

        return transformContent(makeJSONCallout(cout, settings, content, null));
    }

    /**
    * @description Create a new folder
    * @author OmegaCRM  | 01-19-2021 
    * @param settings The settings to use
    * @param content The content wrapper. To create folder should have 'id' and 'parentId' properties informed
    * @return String The JSON of the response
    **/
    public OM_ContentWrapper deleteItem(OM_ContentSetting__c settings, OM_ContentWrapper content) {
        return new OM_ContentWrapper();
    }

    /**
    * @description Create a new folder
    * @author OmegaCRM  | 01-19-2021 
    * @param settings The settings to use
    * @param content The content wrapper. To create folder should have 'id' and 'parentId' properties informed
    * @return String The JSON of the response
    **/
    public OM_ContentWrapper createFolder(OM_ContentSetting__c settings, OM_ContentWrapper content) {
        OM_ContentService.CalloutWrapper cout = new OM_ContentService.CalloutWrapper();
        cout.endpoint = OM_ContentService.getEndpoint(settings, 'newFolder', content);
        cout.method = 'POST';
        cout.headers = new Map<String, String> {
            'Content-Type' => 'application/json'
        };
        content.mimeType = 'application/vnd.google-apps.folder';
        if(content.parentId != null) {
            content.parents = new List<String>{content.parentId};
        }
        cout.body = transformContent(content);
        return transformContent(makeJSONCallout(cout, settings, content, null));
    }

    /**
    * @description Create a new folder path
    * @author OmegaCRM  | 01-19-2021 
    * @param settings The settings to use
    * @param content The content wrapper. To create folder should have 'id' and 'parentId' properties informed
    * @return OM_ContentWrapper The wrapper of the response
    **/
    public virtual List<OM_ContentWrapper> createFolderPath(OM_ContentSetting__c settings, OM_ContentWrapper content) {
        List<OM_ContentWrapper> contentsResult = new List<OM_ContentWrapper>();
        List<String> foldersPath = content.name.split('/');
        String parendId = 'root';
        for (String folder : foldersPath) {
            content.name = folder;
            OM_ContentWrapper contentResponse = createFolder(settings,content);
            content.parentId = contentResponse.Id;
            contentsResult.add(contentResponse);
        }
        return contentsResult;
    }

    public Blob downloadItem(OM_ContentSetting__c settings, OM_ContentWrapper content) {
        OM_ContentService.CalloutWrapper cout = new OM_ContentService.CalloutWrapper();
        cout.endpoint = OM_ContentService.getEndpoint(settings, 'get', content);
        cout.endpoint += '?alt=media';
        cout.method = 'GET';
        return makeBlobCallout(cout, settings, content, null);
    }

    /**
    * @description Get the authorization endpoint to begin Oatuh flow
    * @author OmegaCRM  | 01-19-2021 
    * @param settings The settings to use
    * @param userid The user id to append to "state" parameter
    * @return String 
    **/
    public String getAuthorizationEndpoint(OM_ContentSetting__c settings, String userid) {
        String state = userid + '-' + settings.OM_Name__c;
        if(settings.OM_Auth_type__c == 'Shared') {
            state = OM_ContentService.SHARED + '-' + settings.OM_Name__c;
        }
        return 'https://accounts.google.com/o/oauth2/v2/auth' + 
            + '?scope= ' + settings.OM_scope__c +
            + '&include_granted_scopes=true&response_type=code' +
            + '&state=' + state +
            + '&redirect_uri=' + settings.OM_Redirect_uri__c +
            + '&client_id=' + settings.OM_client_id__c +
            + '&access_type=offline' +
            + '&prompt=consent';
    }

    /**
    * @description Get new access token
    * @author OmegaCRM  | 01-19-2021 
    * @param settings The settings to use
    * @param authCode The authorization code from callbakc page
    * @return String 
    **/
    public String getToken(OM_ContentSetting__c settings, String authCode) {
        Http h = new Http();
        HttpRequest request = new HttpRequest();
        request.setMethod('POST');
        request.setEndpoint(OM_ContentService.getEndpoint(settings, 'token'));
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        String payLoad = 'code=' + EncodingUtil.urlEncode(authCode,'UTF-8') 
            + '&client_id=' + EncodingUtil.urlEncode(settings.OM_client_id__c,'UTF-8') 
            + '&client_secret=' + EncodingUtil.urlEncode(settings.OM_client_secret__c,'UTF-8') 
            + '&redirect_uri=' + EncodingUtil.urlEncode(settings.OM_redirect_uri__c,'UTF-8') 
            + '&grant_type=' + EncodingUtil.urlEncode('authorization_code', 'UTF-8') 
            + '&scope=' + EncodingUtil.urlEncode(settings.OM_scope__c, 'UTF-8');
        request.setBody(payload);
        HttpResponse res = h.send(request);

        System.debug(LoggingLevel.ERROR, 'Google token response');
        System.debug(LoggingLevel.ERROR, res.getBody());
        return res.getBody();
    }
    
    /**
    * @description Get new accces token using a refresh token
    * @author OmegaCRM  | 01-19-2021 
    * @param settings The settings to use
    * @param userId The user id to fetch "refresh_token" data
    * @return String 
    **/
    public String refreshToken(OM_ContentSetting__c settings, String userId) {
        System.debug(LoggingLevel.ERROR, 'Get token for Google ' + settings + '  ' + userId);
        Map<String, String> currentAccess = OM_ContentService.getUserAccess(settings.OM_Name__c, userid);

        System.debug(LoggingLevel.ERROR, 'Settings');
        System.debug(LoggingLevel.ERROR, settings);

        Http h = new Http(); 
        HttpRequest request = new HttpRequest();
        request.setMethod('POST');
        request.setEndpoint(OM_ContentService.getEndpoint(settings, 'token'));
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        String payLoad = '&refresh_token=' + EncodingUtil.urlEncode(currentAccess.get('refresh_token'),'UTF-8') 
            + '&client_id=' + EncodingUtil.urlEncode(settings.OM_client_id__c,'UTF-8') 
            + '&client_secret=' + EncodingUtil.urlEncode(settings.OM_client_secret__c,'UTF-8') 
            + '&redirect_uri=' + EncodingUtil.urlEncode(settings.OM_redirect_uri__c,'UTF-8') 
            + '&grant_type=' + EncodingUtil.urlEncode('refresh_token', 'UTF-8') 
            + '&scope=' + EncodingUtil.urlEncode(settings.OM_scope__c, 'UTF-8');
        request.setBody(payload);
        HttpResponse res = h.send(request);

        // In Google, refresh token does not come back in response, so append it
        return res.getBody().replace('}', ', "refresh_token":"' + currentAccess.get('refresh_token') +'"}');
    }

    /**
    * @description Makes the callouts allowing retries on 401 codes
    * @author OmegaCRM  | 01-19-2021 
    * @param calloutw The callout wrapper with service variables
    * @param settings The settings to use
    * @param content The user id to fetch "refresh_token" data
    * @param newAccess The new access in case of a retry
    * @return Object 
    **/
    public Object makeCallout(OM_ContentService.CalloutWrapper calloutw, OM_ContentSetting__c settings, OM_ContentWrapper content, Map<String, String> newAccess) {
        System.debug(LoggingLevel.ERROR, 'Calling out Google' + settings + '  ');
        Object toreturn = '';
        Map<String,String> access;
        if (newAccess != null) {
            access = newAccess;
        } else {
            access = OM_ContentService.getUserAccess(settings.OM_Name__c, UserInfo.getUserId());
        }

        Http h = new Http();
        HttpRequest request = calloutw.makeRequest(access.get('access_token'));
        HttpResponse res = h.send(request);
        
        // Inspect response. In case of 401, refresh token and retry
        if (res.getStatusCode() == 401) {
            String newacc = refreshToken(settings, UserInfo.getUserId());
            Map<String, String> newaccmap = (Map<String, String>) JSON.deserialize(newacc, Map<String, String>.class);
            toreturn = makeCallout(calloutw, settings, content, newaccmap);
            OM_ContentService.updateUserAccess(settings, UserInfo.getUserId(), newacc);
        }
        else if (res.getStatusCode() >= 200 && res.getStatusCode() < 300) {
            if(calloutw.responseBlob) {
                toreturn = res.getBodyAsBlob();
            } else {
                toreturn = res.getBody();
            }
        }
        else if (res.getStatusCode() >= 400) {
            System.debug('Exception Endpoint ' + request.getEndpoint());
            System.debug('Exception body ' + res.getBody());
            Map<String, Object> errorRes = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
            Map<String, Object> errorData = (Map<String, Object>) errorRes.get('error');
            throw new OM_ContentException('Exception occurred when calling Google API: Code ' + res.getStatusCode() + ' Message: ' + String.valueOf(errorData.get('message')));
        }
        System.debug(LoggingLevel.ERROR, toreturn);
        return toreturn;
    }

    private String makeJSONCallout(OM_ContentService.CalloutWrapper calloutw, OM_ContentSetting__c settings, OM_ContentWrapper content, Map<String, String> newAccess) {
        return String.valueOf(makeCallout(calloutw, settings, content, newAccess));
    }

    private Blob makeBlobCallout(OM_ContentService.CalloutWrapper calloutw, OM_ContentSetting__c settings, OM_ContentWrapper content, Map<String, String> newAccess) {
        calloutw.responseBlob = true;
        return (Blob) makeCallout(calloutw, settings, content, newAccess);
    }


}