/**
 * @description       : 
 * @author            : fcorrea@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 09-05-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
@isTest
public with sharing class OM_RecordatorioSMSPacientesBatchTest {
    @TestSetup
    static void makeData(){
        ETH_DataFactory.setUpOmegaContent();
    } 
    
     @isTest
    public static void testOmSmsRecordatorio(){
        Test.startTest();
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);
        
       	Account account = ETH_DataFactory.createAccountWithAddress('Spain', 'Granada', 'Periodista Fernando Gomez de la Cruz', '18014', 'Granada');
        insert account;
       
        OperatingHours operatingHours = ETH_DataFactory.createOperatingHours();
        insert operatingHours;
        
        //timeslots de operating hours
        ETH_DataFactory.createTimeSlots(operatingHours.Id, 5, 23);
        
        ServiceTerritory serviceTerritoryPadre = ETH_DataFactory.createServiceTerritory(null, operatingHours.Id, 'Pharmate', null);
        insert serviceTerritoryPadre;
        
        ServiceTerritory serviceTerritoryHijo = ETH_DataFactory.createServiceTerritory(account.ShippingPostalCode, operatingHours.Id, 'Pharmate', serviceTerritoryPadre.Id);
        insert serviceTerritoryHijo;
        
        //crear un usuario
        User userProfesional = ETH_DataFactory.createUser('usertestethprofesional3@test.com');
        insert userProfesional;
        
        //crear un assigned resource a cada usuario
        ServiceResource serviceResourceProfesional = ETH_DataFactory.createServiceResourceProfesional(userProfesional.Id);
        insert serviceResourceProfesional;
        
        //crear los services territories member
        ServiceTerritoryMember serviceTerritoryMemberProfesional = ETH_DataFactory.createServiceTerritoryMember('Profesional Visita Presencial (Pharmate)', serviceResourceProfesional.Id, serviceTerritoryPadre.Id, operatingHours.Id);
        insert serviceTerritoryMemberProfesional;
        
        //repetimos proceso para CRETA y box
        ServiceTerritory serviceTerritoryCRETA = ETH_DataFactory.createServiceTerritory(account.ShippingPostalCode, operatingHours.Id, 'Creta', null);
        insert serviceTerritoryCRETA;
        
        Asset asset = ETH_DataFactory.createAsset(account.Id);
        insert asset;
        
        ServiceResource serviceResourceBox = ETH_DataFactory.createServiceResourceBox(asset.Id);
        insert serviceResourceBox;
        
        ServiceTerritoryMember serviceTerritoryMemberBox = ETH_DataFactory.createServiceTerritoryMember('', serviceResourceBox.Id, serviceTerritoryCRETA.Id, operatingHours.Id);
        insert serviceTerritoryMemberBox;
        
        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'Pharmate');
        
        protocolo__c protocolo = ETH_DataFactory.createProtocolo();
        insert protocolo;
        
        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, protocolo.Id, 'TRD');
        insert tratamiento;
        
        Paso_Protocolo__c paso1 = ETH_DataFactory.createPasoProtocolo(protocolo.Id, 1, 'Virtual', 'Usuario que da de alta el tratamiento', 'Inicio', 'Domicilio', 7);
        insert paso1;
        Paso_Protocolo__c paso2 = ETH_DataFactory.createPasoProtocolo(protocolo.Id, 2, 'Presencial', 'Profesional Visita Presencial (Pharmate)', 'Seguimiento', 'CRETA', 7);
        insert paso2;
        
        //Add eventos para el coverage
        Calendar calendar = [SELECT Id FROM Calendar WHERE Name='Test Box' LIMIT 1];
        
        //alta
        Worktype worktypeAlta = ETH_DataFactory.createWorktype('Alta', 'Pharmate', paso1.tipo_Lugar__c, paso1.lugar_visita__c);
        insert worktypeAlta;
        //domicilio
        Worktype worktypeSeguimientoDomicilio = ETH_DataFactory.createWorktype('Seguimiento', 'Pharmate', paso2.tipo_Lugar__c, paso2.lugar_visita__c);
        insert worktypeSeguimientoDomicilio;
        
        ca.Tratamiento__c = tratamiento.Id;
        
       System.runAs(userProfesional){
            ETH_DataFactory.createEventForzarFase2(calendar.Id, 7);
            insert ca;
        }
        
        ServiceAppointment cita = [SELECT Id, OwnerId, Owner.Name, RecordTypeId, ServiceTerritoryId, Paso_Protocolo__c, Status,AccountId,ParentRecordId,Tipo_lugar__c,Lugar_visita__c,SchedStartTime FROM ServiceAppointment WHERE ParentRecordId = :account.Id LIMIT 1];
        
        System.runAs(userProfesional){            	
            cita.Status = 'Programada';
            cita.SchedStartTime = DateTime.now()-1;
            update cita;         
        }		
        System.debug('cita.SchedStartTime #### '+cita.SchedStartTime);
        system.debug('StatusCita: ' + cita.Status);
        system.debug('TipoLugarCita: ' + cita.Tipo_lugar__c);
        system.debug('LugarVisitaCita: ' + cita.Lugar_visita__c);
        
        String masterLabel = cita.Status + '-' + cita.Tipo_lugar__c + '-' + cita.Lugar_visita__c;
        			
        ServiceAppointment citaSeguimiento = [SELECT Id, OwnerId, ServiceTerritoryId, Paso_Protocolo__c, Owner.Name, Status,AccountId,ParentRecordId,Tipo_lugar__c,Lugar_visita__c FROM ServiceAppointment WHERE ParentRecordId = :account.Id AND Status != :'Completada' LIMIT 1];		
        List<ServiceAppointment> listSeguimiento = new List<ServiceAppointment>();
        listSeguimiento.add(cita);
        Map<Account, ServiceAppointment> saByAccount = new Map<Account, ServiceAppointment>();
        saByAccount.put(account, cita);
        
       	multimock.setStaticResource('https://api.lleida.net/sms/v2/HTTP/1.1', 'OM_SMSIntegrationStaticResource');
		Test.setMock(HttpCalloutMock.class, multimock); 
        Database.executebatch(new OM_RecordatorioSMSPacientesBatch());
        
        System.assertNotEquals(citaSeguimiento, null);
        
        Test.stopTest();
    }

}