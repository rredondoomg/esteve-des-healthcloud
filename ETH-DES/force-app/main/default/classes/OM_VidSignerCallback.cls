/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 10-05-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
@RestResource(urlMapping='/PruebaVidSigner/*')
global without sharing class OM_VidSignerCallback {

   /**
     * @description Shared constant
     */
    public static final String SHARED = 'SHARED';
    public static final String ACTION_CREATE_FOLDER = 'createFolder';
    public static final String ACTION_CREATE_FOLDER_PATH = 'createFolderPath';
    public static final String ACTION_GET_ITEM = 'getItem';
    public static final String ACTION_UPDATE_ITEM = 'updateItem';
    public static final String ACTION_DELETE_ITEM = 'deleteItem';
    public static final String ACTION_UPLOAD_ITEM = 'uploadItem';
    public static final String ACTION_LIST_ITEMS = 'listsItems';
    public static final String ACTION_DOWNLOAD_ITEM = 'downloadItem';

    @HttpPost
    global static void getSignatureDocumentPost() {

      String documentId;
      String serviceAppointmentId;
      String operationType;
      if(!Test.isRunningTest()){
        RestRequest req = RestContext.request;
        system.debug('URL donde llama VidSigner: '+req.requestURI);
        List<String> paramsURL = req.requestURI.split('/');
        documentId = paramsURL[paramsURL.size()-1];
        serviceAppointmentId = paramsURL[paramsURL.size() - 3];
        operationType = paramsURL[paramsURL.size() - 4];
      }else{
        documentId = '1cb039f1-a8d4-4a06-aab4-86d445d1ebfe';
        serviceAppointmentId = '1234567';
        operationType = 'Consentimiento';
      }

      getVidSignerDocument(documentId, serviceAppointmentId, operationType);

    }


    public static void getVidSignerDocument(String documentId, String serviceAppointmentId, String operationType){
      eth__c ethCS = eth__c.getInstance(UserInfo.getProfileId());
        String str_urlSignedDocument = ethCS.vidsigner_url_getSignedDocument__c; 
        String str_SubscriptionName = ethCS.vidsigner_SubscriptionName__c; 
        String str_SubscriptionPass = ethCS.vidsigner_SubscriptionPass__c ; 
        
        String name_pass64 = EncodingUtil.base64encode(Blob.valueOf(str_SubscriptionName+':'+str_SubscriptionPass)); 
        
        
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();

        system.debug('Endpoint vidSigner: '+str_urlSignedDocument+documentId);
        req.setEndpoint(str_urlSignedDocument+documentId);
        req.setMethod('GET');
        req.setHeader('Content-Type','application/json'); 
        req.setHeader('Authorization','Basic '+name_pass64);
        req.setHeader('Cache-Control','no-cache');   

        res = http.send(req);


        if(res.getStatusCode() == 200) {
          system.debug('Devuelve 200');
          VidSignerWrapperResponse vidSignerResponse = (VidSignerWrapperResponse) JSON.deserialize(res.getBody(), VidSignerWrapperResponse.class);
          vidSignerResponse.ServiceAppointmentId = serviceAppointmentId;
          vidSignerResponse.OperationType = operationType;
          System.debug('@@@@@@@' + vidSignerResponse.additionalData);
          List<String> serializedVidSignerResponse = new List<String>();
          serializedVidSignerResponse.add(JSON.serialize(vidSignerResponse));

          Trigger_Controller__mdt triggerController = [SELECT Sharepoint_VidSigner__c FROM Trigger_Controller__mdt WHERE Label='Trigger Controller'];
          if(triggerController.Sharepoint_VidSigner__c){
            uploadFileSharepoint(serializedVidSignerResponse);
          }
        }else{
          system.debug('Devuelve '+res.getStatusCode());
          system.debug('Respuesta: '+res.getBody());
        }

    }

    public class VidSignerWrapperResponse{
      @AuraEnabled public String FileName{get;set;} 
      @AuraEnabled public String DocContent{get;set;} 
      @AuraEnabled public String AdditionalData{get;set;} 
      @AuraEnabled public String ServiceAppointmentId{get;set;} 
      @AuraEnabled public String OperationType{get;set;} 
    } 

    @future (callout=true)
    public static void uploadFileSharepoint(List<String> serializedVidSignerResponse){

      List<OM_ContentSetting__c> pruebacontentSett = [SELECT Id,
      OM_Allow_folder_creation__c,
      OM_Allow_navigation__c,
      OM_Allow_upload_files__c,
      OM_Allow_edit__c,
      OM_Allow_filters__c,
      OM_Allow_search__c,
      OM_Allow_selection__c,
      OM_Allow_deletion__c,
      OM_Allow_tagging__c,
      OM_Allow_AI_suggestions__c,
      OM_Columns__c,
      OM_Is_personal_account__c,
      OM_Auth_type__c,
      OM_Use_personal_drive__c,
      OM_Author_role_name__c,
      OM_Client_id__c,
      OM_Client_secret__c,
      OM_Discovery_docs__c,
      OM_Env_ltng_url__c,
      OM_Env_vf_url__c,
      OM_Folder_mapping__c,
      OM_Initial_folder_id__c,
      OM_Action_file_click__c,
      OM_Key__c,
      OM_Name__c,
      OM_NLP_salience_threshold__c,
      OM_Tags_displayed__c,
      OM_Provider__c,
      OM_Drive_id__c,
      OM_Redirect_uri__c,
      OM_Scope__c,
      OM_Show_inline_actions__c,
      OM_Site_id__c,
      OM_GCS_bucket_name__c,
      OM_Tenant__c,
      OM_Use_roles__c,
      OM_Use_content_version__c
      // (SELECT Id, Name, OM_As_column__c, OM_Property_name__c, OM_Salesforce_field_name__c, OM_Editable__c, OM_Filterable__c FROM Content_custom_properties__r)
      FROM OM_ContentSetting__c WHERE OM_Name__c = 'sharepointComponent'  LIMIT 1];

      VidSignerWrapperResponse vidSignerResponse = (VidSignerWrapperResponse) JSON.deserialize(serializedVidSignerResponse[0], VidSignerWrapperResponse.class);

      OM_ContentWrapper folderWrapper = new OM_ContentWrapper();

      if(!Test.isRunningTest()){
        List<ServiceAppointment> serviceAppointmentSignedList = [SELECT Id, Account.IdAnonimo__c, Tratamiento__r.Tratamiento__r.Tratamiento_ET__c, Tratamiento__c, Linea_negocio__c FROM ServiceAppointment WHERE Id =: vidSignerResponse.ServiceAppointmentId];
        // system.debug('3 '+vidSignerResponse.OperationType);
        if(vidSignerResponse.OperationType == 'Consentimiento'){
          folderWrapper.name = serviceAppointmentSignedList[0].Account.IdAnonimo__c + '/Firma' + vidSignerResponse.OperationType + String.valueOf(datetime.now().format('yyyy-MM-dd_HH:mm:ss.SSS')).replaceAll(':', '-').replaceAll(' ', '') + '.pdf';
        }
        else{
          if(serviceAppointmentSignedList[0].Tratamiento__c == null || serviceAppointmentSignedList[0].Tratamiento__r.Tratamiento__c == null || serviceAppointmentSignedList[0].Tratamiento__r.Tratamiento__r.Tratamiento_ET__c == null){
            System.debug('Entra en correcto');
            folderWrapper.name = serviceAppointmentSignedList[0].Account.IdAnonimo__c + '/Firma' + '_' + serviceAppointmentSignedList[0].Linea_negocio__c + vidSignerResponse.OperationType + String.valueOf(datetime.now().format('yyyy-MM-dd_HH:mm:ss.SSS')).replaceAll(':', '-').replaceAll(' ', '') + '.pdf';
          }
          else{
            system.debug('1 '+serviceAppointmentSignedList[0].Account.IdAnonimo__c);
            system.debug('2 '+String.valueOf(serviceAppointmentSignedList[0].Tratamiento__r.Tratamiento__r.Tratamiento_ET__c).replaceAll(' ', '%20'));   
            system.debug('3 '+vidSignerResponse.OperationType);
            system.debug('4 '+String.valueOf(datetime.now().format('yyyy-MM-dd_HH:mm:ss.SSS')).replaceAll(':', '-').replaceAll(' ', ''));
            folderWrapper.name = serviceAppointmentSignedList[0].Account.IdAnonimo__c + '/' + String.valueOf(serviceAppointmentSignedList[0].Tratamiento__r.Tratamiento__r.Tratamiento_ET__c).replaceAll(' ', '%20') + '/Firma' + vidSignerResponse.OperationType + String.valueOf(datetime.now().format('yyyy-MM-dd_HH:mm:ss.SSS')).replaceAll(':', '-').replaceAll(' ', '') + '.pdf';
          }
        }
        System.debug(folderWrapper.name);
        folderWrapper.name = EncodingUtil.urlEncode(folderWrapper.name,'UTF-8');
        // EncodingUtil.urlEncode(path + categoria + '/' + contentVersion.PathOnClient,'UTF-8').replace('+', '%20');
      }

      folderWrapper.parentId = 'root';

      if(Test.isRunningTest()){
        folderWrapper.name = 'directorio';
      }

      folderWrapper.mimeType = 'application/pdf';
      
      OM_ContentWrapper folderResponse = runIntegrationAction('uploadItem', 'sharepointComponent', folderWrapper, EncodingUtil.base64Decode(vidSignerResponse.DocContent), pruebacontentSett[0]);

      ServiceAppointment sAtoUpdate = new ServiceAppointment();
      ServiceAppointment cita;
      Account cuentaToUpdateFirmaCons;
      if(!Test.isRunningTest()){
        sAtoUpdate.id = vidSignerResponse.ServiceAppointmentId;
        if(vidSignerResponse.OperationType.equalsIgnoreCase('Consentimiento')){
          cita = [SELECT AccountId, Account.Firma_consentimiento__c FROM ServiceAppointment WHERE Id =: vidSignerResponse.ServiceAppointmentId];
          cuentaToUpdateFirmaCons = new Account(Id = cita.AccountId, Firma_consentimiento__c = true);
          update cuentaToUpdateFirmaCons;
        }
      }

        switch on vidSignerResponse.OperationType {
          when 'Baja' { sAtoUpdate.Firma_baja__c = true;}
          when 'Visita' { sAtoUpdate.Firma_visita__c = true;}
        }
      if(!Test.isRunningTest()){ update sAtoUpdate;}
        
    }
      
    /**
    * @description Method used as an entry point to run any integration action from Apex for any provider
    * @author OmegaCRM  | 02-19-2021 
    * @param actionName The action to execute
    * @param settingsName The settings to use
    * @param content The contetn wrapper (inf any)
    * @param fileData The file data (only for uploads)
    * @return OM_ContentWrapper The result as a wrapper
    **/
    @AuraEnabled
    public static OM_ContentWrapper runIntegrationAction(String actionName, String settingsName, OM_ContentWrapper content, Blob fileData, OM_ContentSetting__c settings) {
        OM_ContentIntegrationInterface integration = getIntegrationClass(settings.OM_Provider__c);
        system.debug('Provider en run itnegration: '+settings.OM_Provider__c);
        system.debug('actionName '+actionName);
        if(actionName == ACTION_GET_ITEM) { return integration.getItem(settings, content);
        } else if(actionName == ACTION_CREATE_FOLDER) { return integration.createFolder(settings, content);
        } else if(actionName == ACTION_DELETE_ITEM) { return integration.deleteItem(settings, content);
        } else if(actionName == ACTION_UPDATE_ITEM) { return integration.updateItem(settings, content);
        } else if(actionName == ACTION_UPLOAD_ITEM) { return integration.uploadItem(settings, content, fileData);
        } else { return null;}
    }

    /**
    * @description Obtains the concrete implmentation class to use based on the provider
    * @author OmegaCRM  | 01-20-2021 
    * @param provider The desired provider
    * @return OM_ContentIntegrationInterface The concrete implementation
    **/
    public static OM_ContentIntegrationInterface getIntegrationClass(String provider) {
      return OM_ContentUtils.getIntegrationClass(provider);
  }

}