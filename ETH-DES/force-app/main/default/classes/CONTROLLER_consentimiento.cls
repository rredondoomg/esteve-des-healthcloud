/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 07-27-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
public with sharing class CONTROLLER_consentimiento {
	
	//public String idvisita { get; set; }
    public String idprescripcion { get; set; }
	public String nombrePaciente { get; set; }
	public String nifPaciente { get; set; }
    public String lan { get; set; }
    public String cType { get; set; }
    public String city { get; set; }
    public String nombreTutor { get; set; }
    public String nifTutor { get; set; }
    public Boolean esTutor { get; set; }
	//public String mes { get; set; }
	//public String lugar { get; set; }
    
    public static List<String> pContent {get;set;}
    public static List<String> pTutorContent {get;set;}
    public static String firstTitle {get;set;}
    public static String secondTitle {get;set;}
    public static String thirdTitle {get;set;}
    public static String tutorHeader {get;set;}
    public static String tutorHeader2 {get;set;} 
    public static String firma {get;set;} 

    /*public String emailContent {
        get{
            List<EmailTemplate> listTemplates = new List<EmailTemplate>([SELECT Id, HtmlValue FROM EmailTemplate WHERE DeveloperName = 'Consentimiento_Informado_Pacientes_Propios_ESP' LIMIT 1]);
            if(!listTemplates.isEmpty()){
               emailContent = listTemplates.get(0).HtmlValue;
           }
           
           return emailContent;
        }
    	set;
	}*/
    
    

	public CONTROLLER_consentimiento() {
		//Cambia el botón a Prescripción (04/11/2019)
		//idvisita = ApexPages.currentPage().getParameters().get('idvisita');		
        idprescripcion = ApexPages.currentPage().getParameters().get('idprescripcion');
		
		nifPaciente = ApexPages.currentPage().getParameters().get('nifPaciente');
        
        lan = ApexPages.currentPage().getParameters().get('lan');
        
        cType = ApexPages.currentPage().getParameters().get('cType');
        
        city = ApexPages.currentPage().getParameters().get('city');
        
        nifTutor = ApexPages.currentPage().getParameters().get('nifT');
        
        nombreTutor = ApexPages.currentPage().getParameters().get('cName');
        
        esTutor = (String.isNotBlank(nombreTutor) && String.isNotBlank(nifTutor)) ? true : false;
        //firma = (lan == 'CAT') ? 'Signatura' : 'Firma';
        
        //Cambia el botón a Prescripción (04/11/2019)
		/*Visita__c v = [select Id, Prescripcion__r.Paciente__r.Name, Prescripcion__r.Paciente__r.CodigoPoblacion__r.Name 
						from Visita__c where Id =: idvisita limit 1];*/

                    ServiceAppointment p = [select Id, Account.Name/*, Account.CodigoPoblacion__r.Name*/ 
						from ServiceAppointment where Id =: idprescripcion limit 1];                
		
        //Cambia el botón a Prescripción (04/11/2019)
		//nombrePaciente = v.Prescripcion__r.Paciente__r.Name;
        nombrePaciente = p.Account.Name;
		
		//mes = getMes(Date.today().month());

		//lugar = v.Prescripcion__r.Paciente__r.CodigoPoblacion__r.Name; 
        getContentText(city,lan, cType);
	}
    
    
	private String getMes(Integer mes){
		Map<Integer, String> mapaMeses = new Map<Integer, String>();
			mapaMeses.put(1, 'Enero');
			mapaMeses.put(2, 'Febrero');
			mapaMeses.put(3, 'Marzo');
			mapaMeses.put(4, 'Abril');
			mapaMeses.put(5, 'Mayo');
			mapaMeses.put(6, 'Junio');
			mapaMeses.put(7, 'Julio');
			mapaMeses.put(8, 'Agosto');
			mapaMeses.put(9, 'Septiembre');
			mapaMeses.put(10, 'Octubre');
			mapaMeses.put(11, 'Noviembre');
			mapaMeses.put(12, 'Diciembre');

		return mapaMeses.get(mes);
	}
    
    public void getContentText(String ciudad, String lan, String cType){
        
        Integer counterP = 0;
        Integer counterT = 0;
        String cmQuery = 'SELECT ';
        for(String contentField:  Schema.getGlobalDescribe().get('Consentimiento_Contenido__mdt').getDescribe().fields.getMap().keySet()){
            cmQuery += contentField + ', ';
            
            if(contentField.containsIgnoreCase('Paragraph_T')){
                counterT ++;
            }else if(contentField.containsIgnoreCase('Paragraph')){
                counterP ++;
            }
        }
        
        cmQuery = cmQuery.removeEnd(', ');
        cmQuery += ' FROM Consentimiento_Contenido__mdt WHERE Contract_Type__c = \'' + cType + '\' AND Language__c = \'' + lan + '\'';
        
        List<Consentimiento_Contenido__mdt> cmtList= new List<Consentimiento_Contenido__mdt>();
        cmtList = Database.query(cmQuery);
        
        pContent = new List<String>();
        pTutorContent = new List<String>();
        
        if(!cmtList.isEmpty()){
            Consentimiento_Contenido__mdt cDoc = cmtList.get(0);
            firstTitle = cDoc.Main_Title__c;   
            secondTitle = cDoc.Second_Title__c;
            thirdTitle = cDoc.Third_Title__c;
            tutorHeader = cDoc.Tutor_Header__c;
            tutorHeader2 = cDoc.Tutor_Header_2__c;
            firma = cDoc.FirmaTitle__c;
            
			for(Integer i = 1; i <= counterP; i++){
                String fString = 'Paragraph_'+i+'__c';
                if(!String.isBlank((String)cDoc.get(fString))){
                    
                    String pString = (String)cDoc.get(fString);
                    
                    pString = pString.replace('[S_0]', '<span style="text-decoration: underline;">');
                    pString = pString.replace('[S_1]', '</span>');
                    pString = pString.replace('[B_0]', '<span style="font-weight: bold;">');
                    pString = pString.replace('[B_1]', '</span>');
                    
                    pString = pString.replace('[PATIENT_NAME]', nombrePaciente);
                    pString = pString.replace('[PATIENT_NIF]', nifPaciente);
                    pString = pString.replace('[CITY]', ciudad);
                    pString = pString.replace('[TODAY_DAY]', String.valueOf(Date.today().day()));
                    pString = pString.replace('[TODAY_MONTH]', getMes(Date.today().month()));
                    pString = pString.replace('[TODAY_YEAR]', String.valueOf(Date.today().year()));
                    pContent.add(pString);
                }
            }
            
            for(Integer i = 1; i <= counterT; i++){
                String fString = 'Paragraph_T'+i+'__c';
                if(!String.isBlank((String)cDoc.get(fString))){
                    
                    String pString = (String)cDoc.get(fString);
                    
                    /*pString = pString.replace('[PATIENT_NAME]', nombrePaciente);
                    pString = pString.replace('[TUTOR_NAME]', nombreTutor);
                    pString = pString.replace('[TUTOR_DNI]', nifTutor);*/
                    pString = pString.replace('[B_0]', '<span style="font-weight: bold;">');
                    pString = pString.replace('[B_1]', '</span>');
                    pString = pString.replace('[S_0]', '<span style="font-weight: bold;">');
                    pString = pString.replace('[S_1]', '</span>');

                    pString = pString.replace('[CITY]', ciudad);
                    pString = pString.replace('[TODAY_DAY]', String.valueOf(Date.today().day()));
                    pString = pString.replace('[TODAY_MONTH]', getMes(Date.today().month()));
                    pString = pString.replace('[TODAY_YEAR]', String.valueOf(Date.today().year()));

                    pTutorContent.add(pString);
                }
            }

            
        }
    }

}