/**
 * @description       Unit test class for OM_ContentMicrosoftIntegration
 * @author            OmegaCRM 
 * @group             Content tests
**/
@istest
public inherited sharing class OM_ContentMicrosoftIntegration_Test {

    public static final String SETTINGS_NAME = 'testSettings';
    public static final String USERNAME = 'testcontentUser@content.omegacrconsulting.com';


    @TestSetup
    static void makeData(){
        // Insert user with corect permission set for testing (any profile)
        Profile pf = [SELECT Id FROM Profile WHERE UserLicense.Name = 'Salesforce' LIMIT 1];
        User u = new User(
            Username = USERNAME,
            FirstName = 'Test',
            LastName = 'Omega',
            ProfileId = pf.Id,
            Email = USERNAME,
            Alias = 'test',
            TimeZoneSidKey = 'GMT',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'ISO-8859-1',
            LanguageLocaleKey = 'en_US'
        );

        insert u;

        PermissionSet contUser = [SELECT Id FROM PermissionSet WHERE Name = 'OM_ContentUser' LIMIT 1];
        PermissionSetAssignment psass = new PermissionSetAssignment(
            AssigneeId = u.Id,
            PermissionSetId = contUser.Id
        );

        insert psass;


        System.runAs(u){
            // Insert some settings
            OM_ContentSetting__c settings = new OM_ContentSetting__c(
                OM_Name__c = SETTINGS_NAME,
                OM_Allow_folder_creation__c = true,
                OM_Allow_navigation__c = true,
                OM_Allow_upload_files__c = true,
                OM_Allow_edit__c = true,
                OM_Auth_type__c = 'Shared',
                OM_Drive_id__c = 'test',
                OM_Use_personal_drive__c = false,
                OM_Client_id__c = 'test',
                OM_Client_secret__c = 'test',
                OM_Discovery_docs__c = 'test',
                OM_Provider__c = 'MicrosoftSharePoint',
                OM_Site_id__c = 'test',
                OM_Tenant__c = 'test',
                OM_Scope__c = 'test',
                OM_Redirect_uri__c = 'https://testorg.visualforce.com/apex/OM_OAuthCallback',
                OM_Columns__c = 'name;size'
            );
            insert settings;


            OM_ContentService.updateUserAccess(settings, UserInfo.getUserId(), '{"access_token": "test_token", "refresh_token": "test_refresh"}');
        }

    }

    @isTest 
    static void testGetToken() {
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new OM_ContentMicrosoftIntegrationMock());
        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        Test.startTest();
        System.runAs(contextuser){
            String tokenResponse = OM_ContentService.getAccessToken(SETTINGS_NAME, 'testCode', UserInfo.getUserId());
            System.assert(tokenResponse.contains('"access_token"'), 'Response should contain an access token');
        }
        Test.stopTest();
    }

    @isTest 
    static void testRefreshToken() {
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new OM_ContentMicrosoftIntegrationMock());
        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        Test.startTest();
        System.runAs(contextuser){
            String tokenResponse = OM_ContentService.refreshToken(SETTINGS_NAME, UserInfo.getUserId());
            System.assert(tokenResponse.contains('"access_token"'), 'Response should contain an access token');
        }
        Test.stopTest();
    }

    @isTest 
    static void testGetAuthorizationEndpoint() {
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new OM_ContentMicrosoftIntegrationMock());
        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        Test.startTest();
        System.runAs(contextuser){
            String tokenResponse = OM_ContentService.getAuthorizationEndpoint(SETTINGS_NAME, UserInfo.getUserId());
            System.assert(tokenResponse.contains('https://login.microsoft'), 'Response should contain valid URl');
            System.assert(tokenResponse.contains('-'+SETTINGS_NAME), 'Response should contain setting name in state');
        }
        Test.stopTest();
    }

    @isTest 
    static void testCreateFolder() {
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new OM_ContentMicrosoftIntegrationMock());
        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        Test.startTest();
        System.runAs(contextuser){
            OM_ContentWrapper folderWrapper = new OM_ContentWrapper();
            folderWrapper.parentId = 'root';
            folderWrapper.name = 'testFolder';
            OM_ContentWrapper folderResponse = OM_ContentService.runIntegrationAction(OM_ContentService.ACTION_CREATE_FOLDER, SETTINGS_NAME, folderWrapper, null);
            System.assertNotEquals(null, folderResponse.id, 'Response should contain valid folder id');

        }
        Test.stopTest();
    }


    @isTest 
    static void testUpdateFile() {
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new OM_ContentMicrosoftIntegrationMock());
        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        Test.startTest();
        System.runAs(contextuser){
            OM_ContentWrapper folderWrapper = new OM_ContentWrapper();
            folderWrapper.id = 'testid';
            folderWrapper.name = 'testfile.txt';
            folderWrapper.appProperties = new Map<String, String> {
                'testprop' => 'testvalue'
            };
            OM_ContentWrapper folderResponse = OM_ContentService.runIntegrationAction(OM_ContentService.ACTION_UPDATE_ITEM, SETTINGS_NAME, folderWrapper, null);
            System.assertNotEquals(null, folderResponse.id, 'Response should contain valid folder id');

        }
        Test.stopTest();
    }

    @isTest 
    static void testDeleteFile() {
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new OM_ContentMicrosoftIntegrationMock());
        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        Test.startTest();
        System.runAs(contextuser){
            OM_ContentWrapper folderWrapper = new OM_ContentWrapper();
            folderWrapper.parentId = 'root';
            folderWrapper.name = 'testFolder';
            
            OM_ContentWrapper folderResponse = OM_ContentService.runIntegrationAction(OM_ContentService.ACTION_DELETE_ITEM, SETTINGS_NAME, folderWrapper, null);
            System.assertEquals(folderResponse.id, folderResponse.id, 'Response should the same id of deleted file');

        }
        Test.stopTest();
    }
    @isTest 
    static void testListFiles() {
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new OM_ContentMicrosoftIntegrationMock());
        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        Test.startTest();
        System.runAs(contextuser){
            OM_ContentWrapper folderWrapper = new OM_ContentWrapper();
            folderWrapper.parentId = 'testFolderParentName';
            folderWrapper.name = 'testFolder';
            
            List<OM_ContentWrapper> listResp = OM_ContentService.runIntegrationActionList(OM_ContentService.ACTION_LIST_ITEMS, SETTINGS_NAME, folderWrapper, null);
            //System.assertNotEquals(null, listResp, 'Response contain something');
            System.assertEquals(null, listResp, 'Responseshould be null');

        }
        Test.stopTest();
    }

    @isTest 
    static void testDownloadFile() {
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new OM_ContentMicrosoftIntegrationMock());
        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        Test.startTest();
        System.runAs(contextuser){
            OM_ContentWrapper content = new OM_ContentWrapper();
            content.id = 'testId';
            OM_ContentSetting__c settings = OM_ContentService.getSettings(SETTINGS_NAME);
            OM_ContentMicrosoftIntegration integration = new OM_ContentMicrosoftIntegration();
            Blob fileResp = integration.downloadItem(settings, content);

            System.assertNotEquals(null, fileResp, 'Response contain something');

        }
        Test.stopTest();
    }

    @isTest 
    static void testCreateFolderWithRetry() {
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new OM_ContentMicrosoftIntegrationMock(true, false));
        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        Test.startTest();
        System.runAs(contextuser){
            OM_ContentWrapper folderWrapper = new OM_ContentWrapper();
            folderWrapper.parentId = 'root';
            folderWrapper.name = 'testFolder';
            System.assertEquals(0, Limits.getCallouts(), 'No callouts should be fired yet');
            OM_ContentWrapper folderResponse = OM_ContentService.runIntegrationAction(OM_ContentService.ACTION_CREATE_FOLDER, SETTINGS_NAME, folderWrapper, null);
            System.assertNotEquals(null, folderResponse.id, 'Created folder should have an Id');
            System.assertEquals(3, Limits.getCallouts(), '3 callouts should be fired. One failing, another to refresh token and the retry');
        }
        Test.stopTest();
    }
    @isTest 
    static void testCreateFolderWithError() {
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new OM_ContentMicrosoftIntegrationMock(true, true));
        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        Test.startTest();
        System.runAs(contextuser){
            OM_ContentWrapper folderWrapper = new OM_ContentWrapper();
            folderWrapper.parentId = 'root';
            folderWrapper.name = 'testFolder';
            
            try {
                OM_ContentService.runIntegrationAction(OM_ContentService.ACTION_CREATE_FOLDER, SETTINGS_NAME, folderWrapper, null);
            } catch (OM_ContentException e) {
                // Good it throwed the exception
                System.assertEquals(true, e.getMessage().contains('500'), 'Error 500 should be thrown');

            }
        }
        Test.stopTest();
    }

    @isTest
    static void testUploadFile () {
        Test.setMock(HttpCalloutMock.class, new OM_ContentMicrosoftIntegrationMock());
        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        Test.startTest();
        System.runAs(contextuser){
            OM_ContentWrapper content = new OM_ContentWrapper();
            content.parentId = 'root';
            content.name = 'testDoc.docx';
            content.mimeType = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
            String fileBase64 = 'UEsDBBQABgAIAAAAIQDfpNJsWgEAACAFAAATAAgCW0NvbnRlbnRfVHlwZXNdLnhtbCCiBAIooAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC0lMtuwjAQRfeV+g+Rt1Vi6KKqKgKLPpYtUukHGHsCVv2Sx7z+vhMCUVUBkQpsIiUz994zVsaD0dqabAkRtXcl6xc9loGTXmk3K9nX5C1/ZBkm4ZQw3kHJNoBsNLy9GUw2ATAjtcOSzVMKT5yjnIMVWPgAjiqVj1Ykeo0zHoT8FjPg973eA5feJXApT7UHGw5eoBILk7LXNX1uSCIYZNlz01hnlUyEYLQUiep86dSflHyXUJBy24NzHfCOGhg/mFBXjgfsdB90NFEryMYipndhqYuvfFRcebmwpCxO2xzg9FWlJbT62i1ELwGRztyaoq1Yod2e/ygHpo0BvDxF49sdDymR4BoAO+dOhBVMP69G8cu8E6Si3ImYGrg8RmvdCZFoA6F59s/m2NqciqTOcfQBaaPjP8ber2ytzmngADHp039dm0jWZ88H9W2gQB3I5tv7bfgDAAD//wMAUEsDBBQABgAIAAAAIQAekRq37wAAAE4CAAALAAgCX3JlbHMvLnJlbHMgogQCKKAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAArJLBasMwDEDvg/2D0b1R2sEYo04vY9DbGNkHCFtJTBPb2GrX/v082NgCXelhR8vS05PQenOcRnXglF3wGpZVDYq9Cdb5XsNb+7x4AJWFvKUxeNZw4gyb5vZm/cojSSnKg4tZFYrPGgaR+IiYzcAT5SpE9uWnC2kiKc/UYySzo55xVdf3mH4zoJkx1dZqSFt7B6o9Rb6GHbrOGX4KZj+xlzMtkI/C3rJdxFTqk7gyjWop9SwabDAvJZyRYqwKGvC80ep6o7+nxYmFLAmhCYkv+3xmXBJa/ueK5hk/Nu8hWbRf4W8bnF1B8wEAAP//AwBQSwMEFAAGAAgAAAAhABbiYAa4AgAADAoAABEAAAB3b3JkL2RvY3VtZW50LnhtbKSW32+bMBDH3yftf0C8t4ak+YWaVOuSdX2oVK3b8+QYB1Cwz7Kd0Oyv3xlCyERXEfoCmPN9+J59d/j27lXk3p5rk4Gc++F14HtcMogzmcz9Xz+/XU19z1gqY5qD5HP/wI1/t/j86baIYmA7waX1ECFNVCg291NrVUSIYSkX1FyLjGkwsLHXDASBzSZjnBSgYzIIwqB8UhoYNwa/95XKPTX+Ecdeu9FiTQt0dsAbwlKqLX9tGOHFkBGZkWkbNOgBwggHYRs1vBg1Jk5VC3TTC4SqWqRRP9IbwY37kQZt0qQfadgmTfuRWukk2gkOiks0bkALanGoEyKo3u7UFYIVtdk6yzN7QGYwrjE0k9seitDrRBDD+GLChAiIeT6MawrM/Z2W0dH/6uTvpEeV//FWe+gu8Vcuy2NzKCMnmue4FiBNmqlThYu+NDSmNWT/XhB7kdfzChV2LJf/tadltZQNsIv84/qLvFL+PjEMOuyIQ5w8ukj495u1EoFZ2Hy419KcLW7YsYHUgEELMGZZx5SuGdVqYjzoecYx/DLMqMaYg2hKvVDJx7LlQcNONbTsY7THpvYL9xe+gHXMuvNKMB8T85JShS1BsOgxkaDpOkdFmEMepoFX7oC74q54ruj8BR4V1hAf3F15RYRHjfjH3A+C1Wq0mq38+tWSb+gut87yZTZcTu9LT+0udvEdcuqJnYzhlrixu5amNcDWddwXi60aUS6LAseUVKCs3w9wT9nWJ+dzVzI+zSQlSjmz4cw+6zcUlsqTlz9owooLw5nr5UWU4vN4OpxWcJU8UedsARtDeBNOysCyJMWIwkkQuuEarAXRmHO+ObOmnMYcW+wkmLrhBsCeDZOdLYel5CJikBt8axRlvJpTvsYz2YN2Kx/lmeTPmWWocjiu46xCLB+rLSHNMW7xFwAA//8DAFBLAwQUAAYACAAAACEA1mSzUfQAAAAxAwAAHAAIAXdvcmQvX3JlbHMvZG9jdW1lbnQueG1sLnJlbHMgogQBKKAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACskstqwzAQRfeF/oOYfS07fVBC5GxKIdvW/QBFHj+oLAnN9OG/r0hJ69BguvByrphzz4A228/BineM1HunoMhyEOiMr3vXKnipHq/uQRBrV2vrHSoYkWBbXl5sntBqTkvU9YFEojhS0DGHtZRkOhw0ZT6gSy+Nj4PmNMZWBm1edYtyled3Mk4ZUJ4wxa5WEHf1NYhqDPgftm+a3uCDN28DOj5TIT9w/4zM6ThKWB1bZAWTMEtEkOdFVkuK0B+LYzKnUCyqwKPFqcBhnqu/XbKe0y7+th/G77CYc7hZ0qHxjiu9txOPn+goIU8+evkFAAD//wMAUEsDBBQABgAIAAAAIQCnJZ7y2gYAAMsgAAAVAAAAd29yZC90aGVtZS90aGVtZTEueG1s7Flbixs3FH4v9D+IeXd8m/ElxCn22G4uu0nIOil91NryjGLNyEjybkwJlPSpL4VCWvrQQN/6UEoLLTT0pT8mkNCmP6JHGtszsuWmSTYQyq5hrct3jj6dc3R0PHPpg/sJQydESMrTjle9UPEQScd8QtOo490ZDUstD0mF0wlmPCUdb0mk98Hl99+7hC+qmCQEgXwqL+KOFys1v1guyzEMY3mBz0kKc1MuEqygK6LyROBT0Juwcq1SaZQTTFMPpTgBtSOQQROCbk6ndEy8y2v1Awb/UiX1wJiJI62crGQK2Mmsqr/kUoZMoBPMOh6sNOGnI3JfeYhhqWCi41XMn1e+fKm8EWJqj2xBbmj+VnIrgcmsZuREdLwR9P3Ab3Q3+g2AqV3coDloDBobfQaAx2PYacbF1tmshf4KWwBlTYfufrNfr1r4gv76Dr4b6I+FN6Cs6e/gh8Mwt2EBlDWDHXzQa/f6tn4DypqNHXyz0u37TQtvQDGj6WwHXQka9XC92w1kytkVJ7wd+MNmbQXPUeVCdGXyqdoXawm+x8UQAMa5WNEUqeWcTPEYcCFm9FhQdECjGAJvjlMuYbhSqwwrdfivP75pGY/iiwQXpLOhsdwZ0nyQHAs6Vx3vGmj1CpBnT548ffjr04e/Pf3ss6cPf1qtvSt3BadRUe7F91/+/fhT9Ncv37149JUbL4v45z9+/vz3P/5NvbJoff3z819/fvbNF3/+8MgB7wp8XISPaEIkukFO0W2ewAYdC5Bj8WoSoxjTokQ3jSROsZZxoAcqttA3lphhB65HbDveFZAuXMAPF/cswkexWCjqAF6PEwt4yDnrceHc03W9VtEKizRyLy4WRdxtjE9ca4dbXh4s5hD31KUyjIlF8xYDl+OIpEQhPcdnhDjEPqbUsushHQsu+VShjynqYeo0yYgeW9GUC12hCfhl6SII/rZsc3gX9Thzqe+TExsJZwMzl0rCLDN+iBcKJ07GOGFF5AFWsYvk0VKMLYNLBZ6OCONoMCFSumRuiqVF9zqkGbfbD9kysZFC0ZkLeYA5LyL7fBbGOJk7OdM0LmKvyhmEKEa3uHKS4PYJ0X3wA073uvsuJZa7X36270AacgeInlkI15Eg3D6PSzbFxKW8KxIrxXYFdUZHbxFZoX1ACMOneEIIunPVhedzy+Y56WsxZJUrxGWba9iOVd1PiSTIFDcOx1JphewRifgePofLrcSzxGmCxT7NN2Z2yAzgqkuc8crGMyuVUqEPrZvETZlY+9ur9VaMrbDSfemO16Ww/PdfzhjI3HsNGfLKMpDY/7NtRphZC+QBM8JQZbjSLYhY7s9F9HEyYgun3NQ+tLkbyltFT0LTl1ZAW7VP8PZqH6gwnn372IE9m3rHDXyTSmdfMtmub/bhtquakIsJffeLmj5epLcI3CMO6HlNc17T/O9rmn3n+bySOa9kzisZt8hbqGTy4sU8Alo/6DFakr1PfaaUsSO1ZORAmrJHwtmfDGHQdIzQ5iHTPIbmajkLFwls2khw9RFV8VGM57BM1awQyZXqSKI5l1A4mWGnbj3BFskhn2Sj1er6uSYIYJWPQ+G1HocyTWWjjWb+AG+j3vQi86B1TUDLvgqJwmI2ibqDRHM9+BISZmdnwqLtYNHS6veyMF8rr8DlhLB+KB74GSMINwjpifZTJr/27pl7ep8x7W3XHNtra65n42mLRCHcbBKFMIzh8tgePmNft3OXWvS0KXZpNFtvw9c6iWzlBpbaPXQKZ64egJoxnne8KfxkgmYyB31SZyrMorTjjdXK0K+TWeZCqj6WcQYzU9n+E6qIQIwmEOtFN7A051atNfUe31Fy7cq7ZznzVXQymU7JWO0Zybswlylxzr4hWHf4AkgfxZNTdMwW4jYGQwXNqjbghEq1seaEikJw51bcSlero2i9b8mPKGbzGK9ulGIyz+CmvaFT2Idhur0ru7/azHGknfTGt+7LhfREIWnuuUD0renOH2/vki+wyvO+xSpL3du5rr3OdftuiTe/EArU8sUsapqxg1o+alM7w4KgsNwmNPfdEWd9G2xHrb4g1nWl6e282ObH9yDy+1CtLpiShir8ahE4XL+SzDKBGV1nl/sKLQTteJ9Ugq4f1oKwVGkFg5Jf9yulVtCtl7pBUK8Ogmql36s9AKOoOKkG2dpD+LHPlqs392Z85+19si61L4x5UuamDi4bYfP2vlqz3t5ndTIa6XkPUbDMJ43asF1v9xqldr07LPn9XqvUDhu9Ur8RNvvDfhi02sMHHjoxYL9bD/3GoFVqVMOw5Dcqmn6rXWr6tVrXb3ZbA7/7YGVr2Pn6e21ew+vyPwAAAP//AwBQSwMEFAAGAAgAAAAhAAlG/5T4AwAA7woAABEAAAB3b3JkL3NldHRpbmdzLnhtbLRW32/bNhB+H7D/wdDzHMuK7DlancKO7SVF3BaVhwF7o0TKJsJfICk77rD/fUdKjJylKJwOeUmo++6+Ox6/I/3u/SNnvT3RhkoxjYYXcdQjopSYiu00+mOz6k+inrFIYMSkINPoSEz0/vrnn94dMkOsBTfTAwphMl5Oo521KhsMTLkjHJkLqYgAsJKaIwufejvgSD/Uql9KrpClBWXUHgdJHI+jlkZOo1qLrKXoc1pqaWRlXUgmq4qWpP0XIvQ5eZuQhSxrToT1GQeaMKhBCrOjygQ2/qNsAO4Cyf57m9hzFvwOw/iM7R6kxk8R55TnApSWJTEGDoizUCAVXeL0BdFT7gvI3W7RU0H4MPar08pHryNIXhCMS4pfxzFuOQYQecJjyOtoRoHGHDl5DESGndPaBrqnhUa6EW7bV15md1shNSoYlAP97UGLer4699dVfA1D81VK3jtkiugSlAMTF8fRwAFwXrLKLbLgnm014jAp06hkBInGAZMK1cxuUJFbqcBpj6DiX+NJA++OakeE1/NfMKkBT5NRg5c7pFFpic4VKkEVN1JYLVnww/KjtDcwlRpE00b4Ge1WeTPvECEQhz0+m+G1xDCQh6zW9PzDcAE++zAU+c1EEu4nTTHZuN7m9sjICorP6VcyE/hDbSwFRr/z/1HB9wqAvkLmT6CGzVGRFUG2hja9UTJ/EitG1ZpqLfWdwCCUN0tGq4poSEBBeGuQF9Xy4Pt8SxCGZ+GN8taG/AnOMIyXG5Dlw1xaK/ltp+Efz+vnaXAqX3jcsAmLL1LaJ9d4dnW5mMybSh16DrJcjpZXyzZLy80zd/1/1mHlBNrjTcQN4oWmqLd2D8TAeRT6YU5FwAsCFww5RfK6CGC/3wCGI8ZW0KoA+G3yDFOjFqTya7ZGetvxth76m1a4TT48cbmriOjftaxVgx40Uo3wgsswTdtIKuw95cFu6iIPUQKuxBOoFvjTXvs+de05ZBYO0g/wPfKC8L7E9Jd5Kximc3fYZI2UajRTbIfTiNHtzg7dMVv4wvA7wn8U26TFEo8lDeY/UOl2Bt7torMlwXbidxlsl50tDba0s42CbdTZxsE2dja4i4lmVDyAfMPS2SvJmDwQfNvhL0xNE8wOKbJobnyQl2wM7RNgevuMPMLjQTC18PNMUczRo3tLkrELb70ZOsraPvN1mHNWzxkwsqgd2MGzYC/x/9TiXqKSghzzIy+6B+SXpnBGDQy7grfGSh2w3zw2TDMsyzuYJFh5e3ITz8bpYtbAI/9GWX8fwLl/IdUcGYJbLISOmtC/l1fJIp6vJv1kNk76aZqO+5NVOu/Hi1E6msXjZLmK/2mHNPxSvf4XAAD//wMAUEsDBBQABgAIAAAAIQCQPg6gYAsAAD9xAAAPAAAAd29yZC9zdHlsZXMueG1svJ1dc9u6EYbvO9P/wNFVe+HIn3KSOc4Zx4lrT+Mcn8hpriESslCDhAqS/uivLwBSEuUlKC649U1ifewDEC/eBZakpN9+f05l9Mh1LlR2Njp4tz+KeBarRGT3Z6Ofd5d770dRXrAsYVJl/Gz0wvPR75/++pffnj7mxYvkeWQAWf4xjc9Gi6JYfhyP83jBU5a/U0uemRfnSqesMA/1/Thl+qFc7sUqXbJCzIQUxcv4cH9/Mqoxug9Fzeci5l9UXKY8K1z8WHNpiCrLF2KZr2hPfWhPSidLrWKe5+agU1nxUiayNebgGIBSEWuVq3nxzhxM3SOHMuEH++6vVG4AJzjAIQBMYpHgGJOaMTaRDU7OcZiTFSZ/SfnzKErjj9f3mdJsJg3JDE1kji5yYPuvbeyTmRyJir/wOStlkduH+lbXD+tH7r9LlRV59PSR5bEQd6YzhpgKA786z3IxMq9wlhfnuWCtLy7sH62vxHnRePqzSMRobFvM/2tefGTybHR4uHrmwvZg6znJsvvVczzf+zpt9sQ8le39dE/NDPdsxPTe9NwGjusDq/5vHO7y9SPX8JLFwrXD5gU38/5gsm+hUlibHZ58WD34UdqBZmWh6kYcoPp/jR2DETd2MOaYVh41r/L5NxU/8GRamBfORq4t8+TP61stlDY+PBt9cG2aJ6c8FVciSXjWeGO2EAn/teDZz5wnm+f/vHReqp+IVZmZv49OJ24WyDz5+hzzpXWmeTVjVpPvNkDad5di07gL/88KdlAr0Ra/4Mymp+jgNcJ1H4U4tBF542jbmeWrY3fvQjV09FYNHb9VQydv1dDkrRo6fauG3r9VQw7z/2xIZAl/rowImwHUXRyPG9Ecj9nQHI+X0ByPVdAcjxPQHM9ER3M88xjN8UxTBKdQsW8WNib7kWe2d3N3rxFh3N1LQhh39woQxt2d8MO4u/N7GHd3Og/j7s7eYdzdyRrPrbZa0bWxWVYMdtlcqSJTBY8K/jycxjLDcjUbDc8uelyTHCQBpsps9UI8mBYz93j3DHEmDV/PC1vVRWoezcV9qU2pP7TjPHvk0hTdEUsSwyMEal6U2jMiIXNa8znXPIs55cSmg9pKMMrKdEYwN5fsnozFs4R4+FZEkqSwntCmfl5YkwiCSZ2yWKvhXVOMLD98E/nwsbKQ6HMpJSdifaeZYo41vDZwmOGlgcMMrwwcZnhh0NCMaohqGtFI1TSiAatpRONWzU+qcatpRONW04jGraYNH7c7UUiX4pu7joP+5+4upLJn2Qf3YyruM2Y2AMOXm/qcaXTLNLvXbLmI7FnpdmzzmLHtfFbJS3RHsaatSVT7ejdFLsxRi6wcPqBbNCpzrXlE9lrziAy25g232I3ZJtsN2hVNPTMtZ0WraR2pl2mnTJbVhna421gxfIZtDHApdE5mg3YswQz+brezVk6KzLfp5fCObVjDbfU6K5F2r0YS9FKq+IEmDV+9LLk2ZdnDYNKlklI98YSOOC20quZa0/KHTpJelv+aLhcsF65W2kL0X+pX1+ejG7YcfEC3komMRreveykTMqLbQVzd3XyL7tTSlpl2YGiAn1VRqJSMWZ8J/NsvPvs7TQfPTRGcvRAd7TnR6SEHuxAEi0xFUgkRyWwzRSZI1lDH+yd/mSmmExrarebVLTEFJyJOWbqsNh0E3jJ58cnkH4LdkOP9i2lhzwsNpjXO9OXl7N88Hp6dvquI5GTOH2XhThm63amLpsMNX9m3cMNX9Tt3lm8q7JQjONgt3PCD3cJRHeyFZHkuvFc9g3lUh7viUR/v8Hqt5imp9LyUdAO4ApKN4ApINoRKlmmWUx6x4xEesONRHy/hlHE8grNojvcPLRIyMRyMSgkHo5LBwag0cDBSAYbfVNOADb+zpgEbfntNBSPaAjRgVPOMdPknujDTgFHNMwejmmcORjXPHIxqnh19ifh8bjbBdEtMA0k15xpIuoUmK3i6VJrpFyLkV8nvGcE5zYp2q9XcfrxBZdV91wRIe1pZEm62KxyVyL/4jKxrlkVwLpNJqRTRKazNIuEit28R84fdShbzhZIJ155++GNNXTqtPrHwuknX+15nBL+J+0URTRfrE+FNzGR/Z+SqMN4K291g2zhNVh/1aAu74Yko01VH4ecMJkf9g93M2Qo+3h28WbG3Ik96RsI2J7sjN7vRrcjTnpGwzfc9I10W3orsmsNfmH5onQinXfNnXUt5Jt9p1yxaB7c22zWR1pFtU/C0axZtWSU6j2N7Ih2q088z/vh+5vHHY1zkp2Ds5Kf09pUf0WWwH/xR2BUUkzRde+sbC0CudpvVXpnzz1JVp7S3rsX0/7zTtdmgZDmPWjlH/a/pbGUZ/zj2Tjd+RO+840f0TkB+RK9M5A1HpSQ/pXdu8iN6Jyk/Ap2t4IqAy1YwHpetYHxItoKUkGw1YBfgR/TeDvgRaKNCBNqoA3YKfgTKqCA8yKiQgjYqRKCNChFoo8INGM6oMB5nVBgfYlRICTEqpKCNChFoo0IE2qgQgTYqRKCNGri394YHGRVS0EaFCLRRIQJtVLdfHGBUGI8zKowPMSqkhBgVUtBGhQi0USECbVSIQBsVItBGhQiUUUF4kFEhBW1UiEAbFSLQRq0+hRduVBiPMyqMDzEqpIQYFVLQRoUItFEhAm1UiEAbFSLQRoUIlFFBeJBRIQVtVIhAGxUi0EZ1F+UGGBXG44wK40OMCikhRoUUtFEhAm1UiEAbFSLQRoUItFEhAmVUEB5kVEhBGxUi0EaFiK75WV8K9N2BfoA/6+m9mb3/pau6Uz+an3Juoo76o1a98rP636b/WamHqPUzeUeu3ugHETMplDtF7bl83eS6Ww9QFyv/uOj+8EuTPvD7iOqPCbjLowB+3DcSnFM57pryzUhQ5B13zfRmJNh1Hndl32YkWAaPu5Ku8+Xq5g+zHIHgrjTTCD7whHdl60Y4HOKuHN0IhCPclZkbgXCAu/JxI/Akssn5dfRJz3GarO/jBISu6dggnPoJXdMSarVKx9AYfUXzE/qq5yf0ldFPQOnpxeCF9aPQCvtRYVJDm2GlDjeqn4CVGhKCpAaYcKkhKlhqiAqTGiZGrNSQgJU6PDn7CUFSA0y41BAVLDVEhUkNlzKs1JCAlRoSsFIPXJC9mHCpISpYaogKkxpu7rBSQwJWakjASg0JQVIDTLjUEBUsNUSFSQ2qZLTUkICVGhKwUkNCkNQAEy41RAVLDVFdUruzKFtSoxRuhOM2YY1A3ILcCMQl50ZgQLXUiA6slhqEwGoJarXSHFctNUXzE/qq5yf0ldFPQOnpxeCF9aPQCvtRYVLjqqU2qcON6idgpcZVS16pcdVSp9S4aqlTaly15JcaVy21SY2rltqkDk/OfkKQ1LhqqVNqXLXUKTWuWvJLjauW2qTGVUttUuOqpTapBy7IXky41LhqqVNqXLXklxpXLbVJjauW2qTGVUttUuOqJa/UuGqpU2pctdQpNa5a8kuNq5bapMZVS21S46qlNqlx1ZJXaly11Ck1rlrqlBpXLd2YEEHw7UjTlOkiovsqtSuWLwo2/Hv7fmaa50o+8iSiPdRvqKMcP239MpRlu99xM+8vzJjZLwdvfFwpqb4ctQa6N14n619wssG2J1H9W1n1067D9eXaqkUXCJuKF6atuP5aJ09Tl6XpK0/4Ums2V0tt/rQBr5v2fIur68pmCq7eXQ/qZsSq922NV2fPCzvlO3ptLcGyrlGqXOPr4Ic6DezqoenPTFa/J2b+uM4SA3iqf0ur6mnyzCqUef2CS3nDqnerpf+tks+L6tWDffflAK9en1VfTeeN1y5RewHj7c5UD+vfNPOMd/Vl9fUdBJ4xn4pMmnTEWgbc3dAydKw3vVv9lX/6HwAAAP//AwBQSwMEFAAGAAgAAAAhAL3Ujb8nAQAAjwIAABQAAAB3b3JkL3dlYlNldHRpbmdzLnhtbJTSzWoCMRAA4Huh7xBy16xSpSyuQimWXkqh7QPE7KyGZjIhE7vap2/can/w4l5CJsl8yYSZLXboxAdEtuQrORoWUoA3VFu/ruTb63JwKwUn7WvtyEMl98ByMb++mrVlC6sXSCmfZJEVzyWaSm5SCqVSbDaAmocUwOfNhiLqlMO4Vqjj+zYMDGHQya6ss2mvxkUxlUcmXqJQ01gD92S2CD51+SqCyyJ53tjAJ629RGsp1iGSAeZcD7pvD7X1P8zo5gxCayIxNWmYizm+qKNy+qjoZuh+gUk/YHwGTI2t+xnTo6Fy5h+HoR8zOTG8R9hJgaZ8XHuKeuWylL9G5OpEBx/Gw2Xz3CEUkkX7CUuKd5FahqgOy9o5ap+fHnKg/rXR/AsAAP//AwBQSwMEFAAGAAgAAAAhAK9WPaTGAQAAiwUAABIAAAB3b3JkL2ZvbnRUYWJsZS54bWzckt9q2zAUxu8HfQeh+8ayE6edqVPo1sBg7GJ0D6Aosi2mP0ZHiZu335HspINQqG92MRuE9J1zftL5OA+Pr0aTo/SgnK1pvmCUSCvcXtm2pr9etrf3lEDgds+1s7KmJwn0cXPz6WGoGmcDEKy3UBlR0y6EvsoyEJ00HBaulxaDjfOGBzz6NjPc/z70t8KZnge1U1qFU1YwtqYTxn+E4ppGCfnViYORNqT6zEuNRGehUz2cacNHaIPz+947IQGwZ6NHnuHKXjD56gpklPAOXBMW2Mz0ooTC8pylndFvgHIeoLgCrIXaz2OsJ0aGlX9xQM7DlGcMnIx8pcSI6ltrnec7jSS0hmB3JIHjGi/bTLNBhspyg1lfuFY7r1Kg59aBzDF25LqmrGBbVuIa/xVbxpVmMVF03IOMkDGRjXLDjdKnswqDAhgDvQqiO+tH7lV84RgC1WLgADtW0+cVY8XzdktHJcfXMVRWd0+TUsS70vd5UpYXhUVFJE465iNHJM4lB+/MRgeunHhRRgL5IQfy0xlu33GkYGt0okQ/ojPLWY74xJ3lSOz/ypG7+/KfODLNBvmu2i68OyFxLv7TCZk2sPkDAAD//wMAUEsDBBQABgAIAAAAIQBqaE2XdwEAAAEDAAARAAgBZG9jUHJvcHMvY29yZS54bWwgogQBKKAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACMklFLwzAQx98Fv0PJe5tmU5HSdaAyXzYQnCi+xeS2xbVJSLJ19dObtmtncYJvd7nf/e/yT9LpociDPRgrlJwgEsUoAMkUF3I9QS/LWXiLAuuo5DRXEiaoAoum2eVFynTClIEnozQYJ8AGXknahOkJ2jinE4wt20BBbeQJ6YsrZQrqfGrWWFO2pWvAozi+wQU4yqmjuBYMda+IjpKc9ZJ6Z/JGgDMMORQgncUkIvjEOjCFPdvQVH6QhXCVhrNoV+zpgxU9WJZlVI4b1O9P8Nti/txcNRSy9ooBylLOEidcDlmKT6GP7O7jE5hrj/vEx8wAdcpkC2qYssGcWhU87rwPxsBXQ3dE7f0WqlIZbr3OIPMYB8uM0M6/aDtlcODpnFq38E+8EsDvqr8G/gbrXgN7Uf+VjDREn6ZH49slgQfesKS1t6u8ju8fljOUjeIRCWMSktGSkOR6nMTxe73noP8kWBwX+L/i1VCxE2itGn7a7BsAAP//AwBQSwMEFAAGAAgAAAAhACk4yMRsAQAAxwIAABAACAFkb2NQcm9wcy9hcHAueG1sIKIEASigAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAnFLLTsMwELwj8Q9R7q1TDhWqNkaoCHHgJSVtz5a9SSwc27INon/PpoGQihs+7cx6RzNrw81nb7IPDFE7W+arZZFnaKVT2rZlvqvvF9d5FpOwShhnscyPGPMbfnkBr8F5DEljzEjCxjLvUvIbxqLssBdxSW1LncaFXiSCoWWuabTEOyffe7SJXRXFmuFnQqtQLfwkmI+Km4/0X1Hl5OAv7uujJz0ONfbeiIT8eZg0S+VSD2xioXZJmFr3yFdETwBeRYtx4MYCDi6oEx4L2HYiCJlof3xVAJtBuPXeaCkSLZY/aRlcdE3KXk5us2Ec2PwKUIIK5XvQ6chJag7hUdvRxliQrSDaIHz37W1CUElhcEvZeSNMRGC/BGxd74UlOTZVpPcWd752d8MavkfOyVnGg05d5YUcvJylnTWgIhYV2Z8cTAQ80HMEM8jTrG1R/dz52xj2tx//JV+tlwWd08J+OIo9fRj+BQAA//8DAFBLAQItABQABgAIAAAAIQDfpNJsWgEAACAFAAATAAAAAAAAAAAAAAAAAAAAAABbQ29udGVudF9UeXBlc10ueG1sUEsBAi0AFAAGAAgAAAAhAB6RGrfvAAAATgIAAAsAAAAAAAAAAAAAAAAAkwMAAF9yZWxzLy5yZWxzUEsBAi0AFAAGAAgAAAAhABbiYAa4AgAADAoAABEAAAAAAAAAAAAAAAAAswYAAHdvcmQvZG9jdW1lbnQueG1sUEsBAi0AFAAGAAgAAAAhANZks1H0AAAAMQMAABwAAAAAAAAAAAAAAAAAmgkAAHdvcmQvX3JlbHMvZG9jdW1lbnQueG1sLnJlbHNQSwECLQAUAAYACAAAACEApyWe8toGAADLIAAAFQAAAAAAAAAAAAAAAADQCwAAd29yZC90aGVtZS90aGVtZTEueG1sUEsBAi0AFAAGAAgAAAAhAAlG/5T4AwAA7woAABEAAAAAAAAAAAAAAAAA3RIAAHdvcmQvc2V0dGluZ3MueG1sUEsBAi0AFAAGAAgAAAAhAJA+DqBgCwAAP3EAAA8AAAAAAAAAAAAAAAAABBcAAHdvcmQvc3R5bGVzLnhtbFBLAQItABQABgAIAAAAIQC91I2/JwEAAI8CAAAUAAAAAAAAAAAAAAAAAJEiAAB3b3JkL3dlYlNldHRpbmdzLnhtbFBLAQItABQABgAIAAAAIQCvVj2kxgEAAIsFAAASAAAAAAAAAAAAAAAAAOojAAB3b3JkL2ZvbnRUYWJsZS54bWxQSwECLQAUAAYACAAAACEAamhNl3cBAAABAwAAEQAAAAAAAAAAAAAAAADgJQAAZG9jUHJvcHMvY29yZS54bWxQSwECLQAUAAYACAAAACEAKTjIxGwBAADHAgAAEAAAAAAAAAAAAAAAAACOKAAAZG9jUHJvcHMvYXBwLnhtbFBLBQYAAAAACwALAMECAAAwKwAAAAA=';
            OM_ContentWrapper fileResponse = OM_ContentService.runIntegrationAction(OM_ContentService.ACTION_UPLOAD_ITEM, SETTINGS_NAME, content, EncodingUtil.base64Decode(fileBase64));
            System.assertNotEquals(null, fileResponse.id, 'Created file should have an Id');

        }
        Test.stopTest();

    }
}