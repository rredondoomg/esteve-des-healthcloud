/**
 * @File Name          OM_SchemaUtilsTest.cls
 * @Description        Test cases for OM_SchemaUtils
 * @Author             OmegaCRM
 * @Group              Tests
 * @Last Modified By   OmegaCRM
**/
@isTest 
private class OM_SchemaUtilsTest {
    
    @isTest static void OM_SchemaUtilsTest() {

        String strObjeto = 'Account';
        String strPicklistValue = 'Rating';

        String idSObj;

        List<RecordType> listRT = [SELECT Id, Name, DeveloperName, SobjectType FROM RecordType LIMIT 1];
        RecordType rt;

        if (!listRT.isEmpty()) {
            rt = listRT[0];
            idSObj = rt.Id;
        }else{
            idSObj = Test.getStandardPricebookId();
        }
        

        Test.startTest();
        System.debug(System.LoggingLevel.ERROR, '*** ' + Test.getStandardPricebookId());

        if (rt != null) {
            OM_SchemaUtils.getRecordTypeId(rt.SobjectType, rt.DeveloperName);
            OM_SchemaUtils.getRecordTypeIdMap(rt.SobjectType);
        }

        OM_SchemaUtils.getSObjects();
        OM_SchemaUtils.getDescribeSObject(strObjeto);
        OM_SchemaUtils.getFieldsSObjectMap(strObjeto);
        OM_SchemaUtils.getFieldsSObjectMapLabels(strObjeto);        
        OM_SchemaUtils.getPicklistValues(strObjeto, strPicklistValue);
        OM_SchemaUtils.getPicklistValuesActiveMap(strObjeto, strPicklistValue);
        OM_SchemaUtils.getFirstValuePicklist(strObjeto, strPicklistValue);
        OM_SchemaUtils.getRequiredFields(strObjeto);
        OM_SchemaUtils.getHelpText(strObjeto, strPicklistValue);
        OM_SchemaUtils.getDependantValuesPicklist(strObjeto, strPicklistValue, strPicklistValue);
        OM_SchemaUtils.base64ToBits('AAAA');
        OM_SchemaUtils.getSObjectTypeFromId(idSObj);
        OM_SchemaUtils.getAllFieldsCommaSeparated(strObjeto);
        OM_SchemaUtils.queryAllFields(idSObj);
        OM_SchemaUtils.getAllFieldsAccesibleCommaSeparated(strObjeto);
        OM_SchemaUtils.queryAllFieldsAccesible(idSObj);
        OM_SchemaUtils.getFieldSet(strObjeto, null);

        Test.stopTest();
    }
}