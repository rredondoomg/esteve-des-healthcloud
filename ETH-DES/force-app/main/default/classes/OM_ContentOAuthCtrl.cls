/**
 * @description       Handles authorization redirects from OAuth flows
 * @author            OmegaCRM 
 * @group             Content authorization
**/
public inherited sharing class OM_ContentOAuthCtrl {

    /**
     * @description Error title from OAuth flow to display on screen
     */
    public String errorTitle {get; set;}

    /**
     * @description Error message from OAuth flow to display on screen
     */
    public String errorMsg {get; set;}

    /**
     * @description Success message from OAuth flow to display on screen
     */
    public String successMsg {get; set;}

    /**
    * @description Action called from OM_OAuthCallback page load to get new access token in a Oauth flow
    * @author OmegaCRM  | 01-20-2021 
    **/
    public void initAuth() {
        String state = ApexPages.currentPage().getParameters().get('state');
        String code = ApexPages.currentPage().getParameters().get('code');
        String[] stateParts;
        String userId;
        String settingName;
        
        try {
            stateParts = state.split('-');
            userId = stateParts.get(0);
            settingName = stateParts.get(1);
        } catch (Exception e) {
            this.errorTitle = Label.omOauthBadParameters;
            this.errorMsg = Label.omOauthBadState;
            return;
        }

        if(code != null) {
            try {
                OM_ContentService.getAccessToken(settingName, code, userid);
                this.successMsg = Label.omOauthSuccess;
                this.errorTitle = null;
                this.errorMsg = null;
            } catch (OM_ContentException e) {
                this.errorTitle = Label.omOauthErrorProvider;
                this.errorMsg = e.getMessage();
            }
        } else {
            this.errorTitle = Label.omOauthBadParameters;
            this.errorMsg = Label.omOauthCodeMissing;
        }
    }
}