/**
 * @description       : 
 * @author            : David López <sdlopez@omegacrmconsulting.com>
 * @last modified on  : 03-28-2022
 * @last modified by  : David López <sdlopez@omegacrmconsulting.com>
**/
@isTest 
private class OM_UtilsTest {
    
    @isTest static void OM_UtilsTest() {

        Test.startTest();

        OM_Utils.createShareRecords(new Map<Id, Id>{UserInfo.getUserId() => UserInfo.getUserId()}, 'Read');

        Test.stopTest();
    }
}