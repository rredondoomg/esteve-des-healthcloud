/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 09-01-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
@isTest
public with sharing class ETH_ServiceAppointmentTriggerHandlerTest {
    @TestSetup
    static void makeData(){
        ETH_DataFactory.setUpOmegaContent();
        //crear un usuario
        User userProfesional = ETH_DataFactory.createUser('usertestethprofesional@test.com');
        insert userProfesional;

        User user = [SELECT Id FROM User WHERE LastName = 'Omega' LIMIT 1];

        System.runAs(user){
            MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
            Test.setMock(HttpCalloutMock.class, multimock);

            Test.startTest();

            Account account = ETH_DataFactory.createAccountWithAddress('Spain', 'Granada', 'Periodista Fernando Gomez de la Cruz', '18014', 'Granada');
            insert account;

            OperatingHours operatingHours = ETH_DataFactory.createOperatingHours();
            insert operatingHours;

            //timeslots de operating hours
            ETH_DataFactory.createTimeSlots(operatingHours.Id, 5, 23);

            ServiceTerritory serviceTerritoryPadre = ETH_DataFactory.createServiceTerritory(null, operatingHours.Id, 'Pharmate', null);
            insert serviceTerritoryPadre;

            ServiceTerritory serviceTerritoryHijo = ETH_DataFactory.createServiceTerritory(account.ShippingPostalCode, operatingHours.Id, 'Pharmate', serviceTerritoryPadre.Id);
            insert serviceTerritoryHijo;

            //crear un assigned resource a cada usuario
            ServiceResource serviceResourceProfesional = ETH_DataFactory.createServiceResourceProfesional(userProfesional.Id);
            insert serviceResourceProfesional;

            //crear los services territories member
            ServiceTerritoryMember serviceTerritoryMemberProfesional = ETH_DataFactory.createServiceTerritoryMember('Profesional Visita Presencial (Pharmate)', serviceResourceProfesional.Id, serviceTerritoryPadre.Id, operatingHours.Id);
            insert serviceTerritoryMemberProfesional;

            //repetimos proceso para CRETA y box
            ServiceTerritory serviceTerritoryCRETA = ETH_DataFactory.createServiceTerritory(account.ShippingPostalCode, operatingHours.Id, 'Creta',  null);
            insert serviceTerritoryCRETA;

            Asset asset = ETH_DataFactory.createAsset(account.Id);
            insert asset;

            ServiceResource serviceResourceBox = ETH_DataFactory.createServiceResourceBox(asset.Id);
            insert serviceResourceBox;

            ServiceTerritoryMember serviceTerritoryMemberBox = ETH_DataFactory.createServiceTerritoryMember('', serviceResourceBox.Id, serviceTerritoryCRETA.Id, operatingHours.Id);
            insert serviceTerritoryMemberBox;

            protocolo__c protocolo = ETH_DataFactory.createProtocolo();
            insert protocolo;

            Paso_Protocolo__c paso1 = ETH_DataFactory.createPasoProtocolo(protocolo.Id, 1, 'Virtual', 'Usuario que da de alta el tratamiento', 'Inicio', 'Domicilio', 7);
            insert paso1;
            Paso_Protocolo__c paso2 = ETH_DataFactory.createPasoProtocolo(protocolo.Id, 2, 'Presencial', 'Profesional Visita Presencial (Pharmate)', 'Seguimiento', 'CRETA', 7);
            insert paso2;

            //alta
            Worktype worktypeAlta = ETH_DataFactory.createWorktype('Alta', 'Pharmate', paso1.tipo_Lugar__c, paso1.lugar_visita__c);
            insert worktypeAlta;
            //domicilio
            Worktype worktypeSeguimientoDomicilio = ETH_DataFactory.createWorktype('Seguimiento', 'Pharmate', paso2.tipo_Lugar__c, paso2.lugar_visita__c);
            insert worktypeSeguimientoDomicilio;

            TokenETH__c oCStoken = new TokenETH__c();
            oCStoken.url__c = 'http://93.90.29.156:9002';
            oCStoken.username__c = 'omega';
            oCStoken.password__c = '44V-7J6RE-oLs&n.b7';
            oCStoken.token__c = '11111111111';
            insert oCStoken;
        }
        Test.stopTest();
    }

    private static void transcursoCita(ServiceAppointment cita, Account account){
        cita.Status = 'Programada';
        cita.SchedStartTime = DateTime.newInstance(Date.today(), Time.newInstance(9,0,0,0));
        update cita;
        cita.Status = 'En Curso';
        cita.ActualStartTime = DateTime.now();
        cita.Fecha_Realizada__c = DateTime.now();
        update cita;
        cita.Status = 'Completada';
        cita.ActualEndTime = DateTime.now();
        cita.Firma_baja__c = true;
        account.Firma_consentimiento__c = true;
        cita.Firma_Visita__c = true;
        update account;
        update cita;
    }

    @isTest
    public static void testCRETAFase1(){
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);

        Account account = [SELECT Id, Firma_consentimiento__c FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'Pharmate');
        protocolo__c protocolo = [SELECT Id FROM protocolo__c WHERE Name = 'Protocolo test'];
        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, protocolo.Id, 'Pharmate');
        insert tratamiento;
        ca.Tratamiento__c = tratamiento.Id;

        User userProfesional = [SELECT Id FROM User WHERE Email='usertestethprofesional@test.com' LIMIT 1];
        ServiceTerritory serviceTerritoryCRETA = [SELECT Id FROM ServiceTerritory WHERE RecordTypeId = :Schema.SObjectType.ServiceTerritory.getRecordTypeInfosByDeveloperName().get('Creta').getRecordTypeId() LIMIT 1];
        Paso_Protocolo__c paso2 = [SELECT Id FROM Paso_Protocolo__c WHERE orden__c = 2 LIMIT 1];
        ServiceResource serviceResourceBox = [SELECT Id FROM ServiceResource WHERE Name = 'Test Box' LIMIT 1];

        System.runAs(userProfesional){
            insert ca;
        }

        ServiceAppointment cita = [SELECT Id, OwnerId, ServiceTerritoryId, Paso_Protocolo__c, Fecha_Realizada__c, Status FROM ServiceAppointment WHERE ParentRecordId = :account.Id LIMIT 1];

        CaseTeamMember caseTeamMember = [SELECT Id, ParentId, TeamRoleId, MemberId FROM CaseTeamMember WHERE ParentId =:ca.Id LIMIT 1];
        System.assertNotEquals(caseTeamMember, null);
        System.assertEquals(caseTeamMember.MemberId, userProfesional.Id);

        Test.startTest();
        System.runAs(userProfesional){
            transcursoCita(cita, account);
        }

        ServiceAppointment citaSeguimiento = [SELECT Id, OwnerId, ServiceTerritoryId, Paso_Protocolo__c, Status, Fecha_Realizada__c FROM ServiceAppointment WHERE ParentRecordId = :account.Id AND Status != :'Completada' LIMIT 1];

        System.assertNotEquals(citaSeguimiento, null);
        System.assertEquals(citaSeguimiento.ServiceTerritoryId, serviceTerritoryCRETA.Id);
        System.assertEquals(citaSeguimiento.Paso_Protocolo__c, paso2.Id);

        AssignedResource assignedResource = [SELECT Id, ServiceAppointmentId, ServiceResourceId FROM AssignedResource WHERE ServiceAppointmentId = :citaSeguimiento.Id LIMIT 1];
        System.assertNotEquals(assignedResource, null);
        System.assertEquals(assignedResource.ServiceResourceId, serviceResourceBox.Id);

        Test.stopTest();
    }

    @isTest
    public static void testCRETAFase2And3(){
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);

        Account account = [SELECT Id, Firma_consentimiento__c FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'Pharmate');
        protocolo__c protocolo = [SELECT Id FROM protocolo__c WHERE Name = 'Protocolo test'];
        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, protocolo.Id, 'Pharmate');
        insert tratamiento;
        ca.Tratamiento__c = tratamiento.Id;

        User userProfesional = [SELECT Id FROM User WHERE Email='usertestethprofesional@test.com' LIMIT 1];
        ServiceTerritory serviceTerritoryCRETA = [SELECT Id FROM ServiceTerritory WHERE RecordTypeId = :Schema.SObjectType.ServiceTerritory.getRecordTypeInfosByDeveloperName().get('Creta').getRecordTypeId() LIMIT 1];
        Paso_Protocolo__c paso2 = [SELECT Id FROM Paso_Protocolo__c WHERE orden__c = 2 LIMIT 1];
        ServiceResource serviceResourceBox = [SELECT Id FROM ServiceResource WHERE Name = 'Test Box' LIMIT 1];
        
        //Add eventos para el coverage
        Calendar calendar = [SELECT Id FROM Calendar WHERE Name='Test Box' LIMIT 1];

        System.runAs(userProfesional){
            ETH_DataFactory.createEventForzarFase2(calendar.Id, 7);
            insert ca;
        }

        ServiceAppointment cita = [SELECT Id, OwnerId, ServiceTerritoryId, Paso_Protocolo__c, Status, Fecha_Realizada__c FROM ServiceAppointment WHERE ParentRecordId = :account.Id LIMIT 1];

        CaseTeamMember caseTeamMember = [SELECT Id, ParentId, TeamRoleId, MemberId FROM CaseTeamMember WHERE ParentId =:ca.Id LIMIT 1];
        System.assertNotEquals(caseTeamMember, null);
        System.assertEquals(caseTeamMember.MemberId, userProfesional.Id);

        Test.startTest();
        System.runAs(userProfesional){
            transcursoCita(cita, account);
        }

        ServiceAppointment citaSeguimiento = [SELECT Id, OwnerId, ServiceTerritoryId, Paso_Protocolo__c, Status, Fecha_Realizada__c FROM ServiceAppointment WHERE ParentRecordId = :account.Id AND Status != :'Completada' LIMIT 1];

        System.assertNotEquals(citaSeguimiento, null);
        System.assertEquals(citaSeguimiento.ServiceTerritoryId, serviceTerritoryCRETA.Id);
        System.assertEquals(citaSeguimiento.Paso_Protocolo__c, paso2.Id);

        AssignedResource assignedResource = [SELECT Id, ServiceAppointmentId, ServiceResourceId FROM AssignedResource WHERE ServiceAppointmentId = :citaSeguimiento.Id LIMIT 1];
        System.assertNotEquals(assignedResource, null);
        System.assertEquals(assignedResource.ServiceResourceId, serviceResourceBox.Id);

        Test.stopTest();
    }

    @isTest
    public static void testAltaProfesionalSeguimiento(){
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);

        Account account = [SELECT Id, Firma_consentimiento__c FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'Pharmate');
        protocolo__c protocolo = [SELECT Id FROM protocolo__c WHERE Name = 'Protocolo test'];
        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, protocolo.Id, 'Pharmate');
        insert tratamiento;
        ca.Tratamiento__c = tratamiento.Id;

        User userProfesional = [SELECT Id FROM User WHERE Email='usertestethprofesional@test.com' LIMIT 1];
        ServiceResource serviceResourceProfesional = [SELECT Id FROM ServiceResource WHERE Name = 'Test profesional' LIMIT 1];
        ServiceTerritory serviceTerritoryPadre = [SELECT Id FROM ServiceTerritory WHERE RecordTypeId = :Schema.SObjectType.ServiceTerritory.getRecordTypeInfosByDeveloperName().get('Domicilio_Pharmate').getRecordTypeId() LIMIT 1];
        Paso_Protocolo__c paso2 = [SELECT Id FROM Paso_Protocolo__c WHERE orden__c = 2 LIMIT 1];
        paso2.lugar_visita__c = 'Domicilio';
        update paso2;
        WorkType worktype = [SELECT Name, Id FROM WorkType WHERE Name LIKE '%Seguimiento%' LIMIT 1];
        worktype.Name = worktype.Name.replace('CRETA', 'Domicilio');
        update worktype;

        System.runAs(userProfesional){
            insert ca;
        }

        ServiceAppointment cita = [SELECT Id, OwnerId, ServiceTerritoryId, Paso_Protocolo__c, Status, Fecha_Realizada__c FROM ServiceAppointment WHERE ParentRecordId = :account.Id LIMIT 1];

        CaseTeamMember caseTeamMember = [SELECT Id, ParentId, TeamRoleId, MemberId FROM CaseTeamMember WHERE ParentId =:ca.Id LIMIT 1];
        System.assertNotEquals(caseTeamMember, null);
        System.assertEquals(caseTeamMember.MemberId, userProfesional.Id);

        Test.startTest();
        System.runAs(userProfesional){
            transcursoCita(cita, account);
        }

        ServiceAppointment citaSeguimiento = [SELECT Id, OwnerId, ServiceTerritoryId, Paso_Protocolo__c, Status, Fecha_Realizada__c FROM ServiceAppointment WHERE ParentRecordId = :account.Id AND Status != :'Completada' LIMIT 1];

        System.assertNotEquals(citaSeguimiento, null);
        System.assertEquals(citaSeguimiento.ServiceTerritoryId, serviceTerritoryPadre.Id);
        System.assertEquals(citaSeguimiento.Paso_Protocolo__c, paso2.Id);

        AssignedResource assignedResource = [SELECT Id, ServiceAppointmentId, ServiceResourceId FROM AssignedResource WHERE ServiceAppointmentId = :citaSeguimiento.Id LIMIT 1];
        System.assertNotEquals(assignedResource, null);
        System.assertEquals(assignedResource.ServiceResourceId, serviceResourceProfesional.Id);

        Test.stopTest();
    }

    @isTest
    public static void testAltaProfesionalSeguimientoFase2And3(){
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);

        Account account = [SELECT Id, Firma_consentimiento__c FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'Pharmate');
        protocolo__c protocolo = [SELECT Id FROM protocolo__c WHERE Name = 'Protocolo test'];
        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, protocolo.Id, 'Pharmate');
        insert tratamiento;
        ca.Tratamiento__c = tratamiento.Id;

        User userProfesional = [SELECT Id FROM User WHERE Email='usertestethprofesional@test.com' LIMIT 1];
        ServiceResource serviceResourceProfesional = [SELECT Id FROM ServiceResource WHERE Name = 'Test profesional' LIMIT 1];
        ServiceTerritory serviceTerritoryPadre = [SELECT Id FROM ServiceTerritory WHERE RecordTypeId = :Schema.SObjectType.ServiceTerritory.getRecordTypeInfosByDeveloperName().get('Domicilio_Pharmate').getRecordTypeId() LIMIT 1];
        Paso_Protocolo__c paso2 = [SELECT Id, frecuencia__c FROM Paso_Protocolo__c WHERE orden__c = 2 LIMIT 1];
        paso2.lugar_visita__c = 'Domicilio';
        paso2.frecuencia__c = 33;
        update paso2;
        WorkType worktype = [SELECT Name, Id FROM WorkType WHERE Name LIKE '%Seguimiento%' LIMIT 1];
        worktype.Name = worktype.Name.replace('CRETA', 'Domicilio');
        update worktype;
        
        System.runAs(userProfesional){
            ETH_DataFactory.createEventForzarFase2(userProfesional.Id, 33);
            insert ca;
        }

        ServiceAppointment cita = [SELECT Id, OwnerId, ServiceTerritoryId, Paso_Protocolo__c, Status, Fecha_Realizada__c FROM ServiceAppointment WHERE ParentRecordId = :account.Id LIMIT 1];

        CaseTeamMember caseTeamMember = [SELECT Id, ParentId, TeamRoleId, MemberId FROM CaseTeamMember WHERE ParentId =:ca.Id LIMIT 1];
        System.assertNotEquals(caseTeamMember, null);
        System.assertEquals(caseTeamMember.MemberId, userProfesional.Id);

        Test.startTest();
        System.runAs(userProfesional){
            transcursoCita(cita, account);
        }

        ServiceAppointment citaSeguimiento = [SELECT Id, OwnerId, ServiceTerritoryId, Paso_Protocolo__c, Status, Fecha_Realizada__c FROM ServiceAppointment WHERE ParentRecordId = :account.Id AND Status != :'Completada' LIMIT 1];

        System.assertNotEquals(citaSeguimiento, null);
        System.assertEquals(citaSeguimiento.ServiceTerritoryId, serviceTerritoryPadre.Id);
        System.assertEquals(citaSeguimiento.Paso_Protocolo__c, paso2.Id);

        AssignedResource assignedResource = [SELECT Id, ServiceAppointmentId, ServiceResourceId FROM AssignedResource WHERE ServiceAppointmentId = :citaSeguimiento.Id LIMIT 1];
        System.assertNotEquals(assignedResource, null);
        System.assertEquals(assignedResource.ServiceResourceId, serviceResourceProfesional.Id);

        Test.stopTest();
    }

    @isTest
    public static void testNoEncontroHueco(){
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);

        Account account = [SELECT Id, Firma_consentimiento__c FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'Pharmate');
        protocolo__c protocolo = [SELECT Id FROM protocolo__c WHERE Name = 'Protocolo test'];
        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, protocolo.Id, 'Pharmate');
        insert tratamiento;
        ca.Tratamiento__c = tratamiento.Id;

        User userProfesional = [SELECT Id FROM User WHERE Email='usertestethprofesional@test.com' LIMIT 1];
        ServiceTerritory serviceTerritoryPadre = [SELECT Id FROM ServiceTerritory WHERE RecordTypeId = :Schema.SObjectType.ServiceTerritory.getRecordTypeInfosByDeveloperName().get('Domicilio_Pharmate').getRecordTypeId() LIMIT 1];
        Paso_Protocolo__c paso2 = [SELECT Id, lugar_visita__c, tipo_prof__c FROM Paso_Protocolo__c WHERE orden__c = 2 LIMIT 1];
        paso2.lugar_visita__c = 'Domicilio';
        paso2.tipo_prof__c = 'Profesional Visita Virtual (Pharmate)';
        update paso2;
        WorkType worktype = [SELECT Name, Id FROM WorkType WHERE Name LIKE '%Seguimiento%' LIMIT 1];
        worktype.Name = worktype.Name.replace('CRETA', 'Domicilio');
        update worktype;

        //este lo creamos sin timeslots para simular que no tiene hueco
        OperatingHours operatingHoursLentamenteProgresivo = ETH_DataFactory.createOperatingHours();
        insert operatingHoursLentamenteProgresivo;
        User userEnfermera = ETH_DataFactory.createUser('usertestethenfermera@test.com');
        insert userEnfermera;
        ServiceResource serviceResourceEnfermera = ETH_DataFactory.createServiceResourceProfesional(userEnfermera.Id);
        insert serviceResourceEnfermera;
        ServiceTerritoryMember serviceTerritoryMemberEnfermera = ETH_DataFactory.createServiceTerritoryMember('Profesional Visita Virtual (Pharmate)', serviceResourceEnfermera.Id, serviceTerritoryPadre.Id, operatingHoursLentamenteProgresivo.Id);
        insert serviceTerritoryMemberEnfermera;        

        System.runAs(userProfesional){
            insert ca;
        }

        ServiceAppointment cita = [SELECT Id, OwnerId, ServiceTerritoryId, Paso_Protocolo__c, Status, Fecha_Realizada__c FROM ServiceAppointment WHERE ParentRecordId = :account.Id LIMIT 1];

        Test.startTest();
        System.runAs(userProfesional){
            transcursoCita(cita, account);
        }

        ServiceAppointment citaSeguimiento = [SELECT Id, OwnerId, ServiceTerritoryId, Paso_Protocolo__c, Status, Fecha_Realizada__c FROM ServiceAppointment WHERE ParentRecordId = :account.Id AND Status != :'Completada' LIMIT 1];

        System.assertNotEquals(citaSeguimiento, null);
        System.assertEquals(citaSeguimiento.ServiceTerritoryId, serviceTerritoryPadre.Id);
        System.assertEquals(citaSeguimiento.Paso_Protocolo__c, paso2.Id);

        AssignedResource assignedResource = [SELECT Id, ServiceAppointmentId, ServiceResourceId FROM AssignedResource WHERE ServiceAppointmentId = :citaSeguimiento.Id LIMIT 1];
        System.assertNotEquals(assignedResource, null);
        System.assertEquals(assignedResource.ServiceResourceId, serviceResourceEnfermera.Id);
    }

    @isTest
    public static void testQueuePharmate(){
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);

        Account account = [SELECT Id, Firma_consentimiento__c FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'Pharmate');
        protocolo__c protocolo = [SELECT Id FROM protocolo__c WHERE Name = 'Protocolo test'];
        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, protocolo.Id, 'Pharmate');
        insert tratamiento;
        ca.Tratamiento__c = tratamiento.Id;

        User userProfesional = [SELECT Id FROM User WHERE Email='usertestethprofesional@test.com' LIMIT 1];
        ServiceResource serviceResourceProfesional = [SELECT Id FROM ServiceResource WHERE Name = 'Test profesional' LIMIT 1];
        ServiceTerritory serviceTerritoryPadre = [SELECT Id FROM ServiceTerritory WHERE RecordTypeId = :Schema.SObjectType.ServiceTerritory.getRecordTypeInfosByDeveloperName().get('Domicilio_Pharmate').getRecordTypeId() LIMIT 1];
        Paso_Protocolo__c paso2 = [SELECT Id, lugar_visita__c, tipo_prof__c FROM Paso_Protocolo__c WHERE orden__c = 2 LIMIT 1];
        paso2.lugar_visita__c = 'Domicilio';
        paso2.tipo_prof__c = 'Profesional Visita Virtual (Pharmate)';
        update paso2;
        WorkType worktype = [SELECT Name, Id FROM WorkType WHERE Name LIKE '%Seguimiento%' LIMIT 1];
        worktype.Name = worktype.Name.replace('CRETA', 'Domicilio');
        update worktype;

        System.runAs(userProfesional){
            insert ca;
        }

        ServiceAppointment cita = [SELECT Id, OwnerId, ServiceTerritoryId, Paso_Protocolo__c, Status, Fecha_Realizada__c FROM ServiceAppointment WHERE ParentRecordId = :account.Id LIMIT 1];

        Test.startTest();
        System.runAs(userProfesional){
            transcursoCita(cita, account);
        }

        ServiceAppointment citaSeguimiento = [SELECT Id, OwnerId, ServiceTerritoryId, Paso_Protocolo__c, Status, Fecha_Realizada__c FROM ServiceAppointment WHERE ParentRecordId = :account.Id AND Status != :'Completada' LIMIT 1];

        System.runAs(userProfesional){
            transcursoCita(citaSeguimiento, account);
        }

        Group grupoPharmateQueue = ETH_ServiceAppointmentTriggerHandler.getQueue('Grupo Pharmate');

        ServiceAppointment nuevaCitaSeguimiento = [SELECT Id, OwnerId, ServiceTerritoryId, Paso_Protocolo__c, Status, Fecha_Realizada__c FROM ServiceAppointment WHERE ParentRecordId = :account.Id AND Status != :'Completada' LIMIT 1];
        System.assertNotEquals(nuevaCitaSeguimiento, null);
        System.assertEquals(nuevaCitaSeguimiento.OwnerId, grupoPharmateQueue.Id);
        System.assertEquals(nuevaCitaSeguimiento.ServiceTerritoryId, serviceTerritoryPadre.Id);
        System.assertEquals(nuevaCitaSeguimiento.Paso_Protocolo__c, paso2.Id);

        AssignedResource nuevoAssignedResource = null;
        try {
            nuevoAssignedResource = [SELECT Id, ServiceAppointmentId, ServiceResourceId FROM AssignedResource WHERE ServiceAppointmentId = :nuevaCitaSeguimiento.Id LIMIT 1];
        } catch (Exception e) {
            System.assertEquals(nuevoAssignedResource, null);
        }

        Task nuevaTarea = [SELECT Id, OwnerId, Service_Appointment__c FROM Task WHERE Service_Appointment__c = :nuevaCitaSeguimiento.Id LIMIT 1];
        System.assertNotEquals(nuevaTarea, null);
        System.assertEquals(nuevaTarea.OwnerId, grupoPharmateQueue.Id);

        Test.stopTest();
    }
    
    // @isTest
    // public static void testSmsValidation(){
    //     Test.startTest();
    //     MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
    //     Test.setMock(HttpCalloutMock.class, multimock);
        
    //     Account account = new Account();
    //     List<Account> accountList = new List<Account>();
    //     accountList = [SELECT Id,PersonMobilePhone,ShippingPostalCode,ShippingCity,ShippingCountry,ShippingStreet,ShippingState,LastName,Name,FirstName FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
    //     if(!accountList.isEmpty()){
    //         account = accountList[0];
    //     }
    //     else{
    //         account = ETH_DataFactory.createAccountWithAddress('Spain', 'Granada', 'Periodista Fernando Gomez de la Cruzrr', '18014', 'Granada');
    //         insert account;
    //     }
        
    //     OperatingHours operatingHours = ETH_DataFactory.createOperatingHours();
    //     insert operatingHours;
        
    //     //timeslots de operating hours
    //     ETH_DataFactory.createTimeSlots(operatingHours.Id, 5, 23);
        
    //     ServiceTerritory serviceTerritoryPadre = ETH_DataFactory.createServiceTerritory(null, operatingHours.Id, 'Pharmate', null);
    //     insert serviceTerritoryPadre;
        
    //     ServiceTerritory serviceTerritoryHijo = ETH_DataFactory.createServiceTerritory(account.ShippingPostalCode, operatingHours.Id, 'Pharmate', serviceTerritoryPadre.Id);
    //     insert serviceTerritoryHijo;
        
    //     //crear un usuario
    //     User userProfesional = ETH_DataFactory.createUser('usertestethprofesionalDos@test.com');
    //     insert userProfesional;
        
    //     //crear un assigned resource a cada usuario
    //     ServiceResource serviceResourceProfesional = ETH_DataFactory.createServiceResourceProfesional(userProfesional.Id);
    //     insert serviceResourceProfesional;
        
    //     //crear los services territories member
    //     ServiceTerritoryMember serviceTerritoryMemberProfesional = ETH_DataFactory.createServiceTerritoryMember('Profesional Visita Presencial (Pharmate)', serviceResourceProfesional.Id, serviceTerritoryPadre.Id, operatingHours.Id);
    //     insert serviceTerritoryMemberProfesional;
        
    //     //repetimos proceso para CRETA y box
    //     ServiceTerritory serviceTerritoryCRETA = ETH_DataFactory.createServiceTerritory(account.ShippingPostalCode, operatingHours.Id, 'Creta', null);
    //     insert serviceTerritoryCRETA;
        
    //     Asset asset = ETH_DataFactory.createAsset(account.Id);
    //     insert asset;
        
    //     ServiceResource serviceResourceBox = ETH_DataFactory.createServiceResourceBox(asset.Id);
    //     insert serviceResourceBox;
        
    //     ServiceTerritoryMember serviceTerritoryMemberBox = ETH_DataFactory.createServiceTerritoryMember('', serviceResourceBox.Id, serviceTerritoryCRETA.Id, operatingHours.Id);
    //     insert serviceTerritoryMemberBox;
        
    //     Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'Pharmate');
        
    //     protocolo__c protocolo = ETH_DataFactory.createProtocolo();
    //     insert protocolo;
        
    //     tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, protocolo.Id, , 'Pharmate');
    //     insert tratamiento;
        
    //     Paso_Protocolo__c paso1 = ETH_DataFactory.createPasoProtocolo(protocolo.Id, 1, 'Virtual', 'Usuario que da de alta el tratamiento', 'Inicio', 'Domicilio', 7);
    //     insert paso1;
    //     Paso_Protocolo__c paso2 = ETH_DataFactory.createPasoProtocolo(protocolo.Id, 2, 'Presencial', 'Profesional Visita Presencial (Pharmate)', 'Seguimiento', 'CRETA', 7);
    //     insert paso2;
        
    //     //Add eventos para el coverage
    //     Calendar calendar = [SELECT Id FROM Calendar WHERE Name='Test Box' LIMIT 1];
        
    //     //alta
    //     Worktype worktypeAlta = ETH_DataFactory.createWorktype('Alta', 'Pharmate', paso1.tipo_Lugar__c, paso1.lugar_visita__c);
    //     insert worktypeAlta;
    //     //domicilio
    //     Worktype worktypeSeguimientoDomicilio = ETH_DataFactory.createWorktype('Seguimiento', 'Pharmate', paso2.tipo_Lugar__c, paso2.lugar_visita__c);
    //     insert worktypeSeguimientoDomicilio;
        
    //     ca.Tratamiento__c = tratamiento.Id;
        
    //    System.runAs(userProfesional){
    //         ETH_DataFactory.createEventForzarFase2(calendar.Id, 7);
    //         insert ca;
    //     }
        
    //     ServiceAppointment cita = [SELECT Id, OwnerId, ServiceTerritoryId, Paso_Protocolo__c, Status,AccountId,ParentRecordId,Tipo_lugar__c,Lugar_visita__c FROM ServiceAppointment WHERE ParentRecordId = :account.Id LIMIT 1];
        
    //     System.runAs(userProfesional){            	
    //         cita.Status = 'Programada';
    //         cita.SchedStartTime = DateTime.now();
    //         update cita;         
    //     }		
			
    //     ServiceAppointment citaSeguimiento = [SELECT Id, OwnerId, ServiceTerritoryId, Paso_Protocolo__c, Status,AccountId,ParentRecordId,Tipo_lugar__c,Lugar_visita__c FROM ServiceAppointment WHERE ParentRecordId = :account.Id AND Status != :'Completada' LIMIT 1];		
    //     List<ServiceAppointment> listSeguimiento = new List<ServiceAppointment>();
    //     listSeguimiento.add(cita);
        
    //     // multimock.setStaticResource('https://api.lleida.net/sms/v2/HTTP/1.1', 'OM_SMSIntegrationStaticResource');
	// 	// Test.setMock(HttpCalloutMock.class, multimock); 
	// 	ETH_ServiceAppointmentTriggerHandler.smsValidation(listSeguimiento);
        
    //     System.assertNotEquals(citaSeguimiento, null);
        
    //     Test.stopTest();
    // }
}