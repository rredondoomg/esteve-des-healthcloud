public with sharing class OM_ActualizarStockPedido {
    
    @AuraEnabled
    public static Map<Boolean, String> actualizarStockPedidos(String recordId){
        Map<Boolean, String> responseMap = new Map<Boolean, String>();

        if(recordId != null){
            List<Pedido_Det__c> lineasFromPedido = [SELECT Id, Pedido__r.Contenedor__r.Cod_Con__c, Producto__r.ProductCode, Stock__c
                                                        FROM Pedido_Det__c 
                                                        WHERE Pedido__c =: recordId
                                                        AND Producto__r.RecordType.DeveloperName = 'Fungibles'];
    
            if(!lineasFromPedido.isEmpty()){
                responseMap = actualizarStockActualFungibles(lineasFromPedido);
            }else{
                responseMap.put(true, 'No hay fungibles que actualizar'); 
            }
            return responseMap;
        }
        
        responseMap.put(true, 'respuestaNull');
        return responseMap;
    }

    public static Map<Boolean, String> actualizarStockActualFungibles(List<Pedido_Det__c> lineasFromPedido){
        TokenETH__c oCStoken = [SELECT Id, url__c, username__c, password__c, token__c FROM TokenETH__c];

        String sUrl = oCStoken.url__c;
        String sUsername = oCStoken.username__c;
        String sPassword = oCStoken.password__c;
        String sToken = oCStoken.token__c;
        String sMethod = 'POST';

        Map<Boolean, String> responseMap = new Map<Boolean, String>();
        map<String,Object> tokenResp = new map<String,Object>();
        
        HttpRequest req = new HttpRequest();
        system.debug('URL TOKEN: '+sUrl + '/login');
        

        Map<String, String> mapAuthen = new Map<String, String>();
        mapAuthen.put('username', sUsername);
        mapAuthen.put('password', sPassword);

        req.setBody(JSON.Serialize(mapAuthen));
        req.setEndpoint(sUrl + '/login');
        req.setMethod(sMethod);
        req.setHeader('Content-Type','application/json');

        Http http = new Http();
        HTTPResponse res = new HTTPResponse();
        res = http.send(req);

        if (res.getStatusCode() == 200){
            system.debug('REsponde 200 el token.');
            tokenResp = (Map<String,Object>)JSON.deserializeUntyped(res.getBody());
            return checkStocks(lineasFromPedido, String.valueOf(tokenResp.get('accessToken')), oCStoken);
        }else if(res.getStatusCode() == 401){
            responseMap.put(true, 'No se ha podido obtener el token de Atlas. Codigo '+res.getStatusCode());
        }else if(res.getStatusCode() == 500){
            responseMap.put(true, 'Error en la comunicación con Atlas. Codigo '+res.getStatusCode());
        }else{
            responseMap.put(true, 'Error en la comunicación con Atlas. Codigo '+res.getStatusCode());
        }

        return responseMap;
    }

    public static Map<Boolean, String> checkStocks(List<Pedido_Det__c> lineasFromPedido, String token, TokenETH__c oCStoken){

        Map<Boolean, String> responseMap = new Map<Boolean, String>();
        //INICIO CONSULTA STOCK FUNGIBLES
        String tipoUsuario = 'P';
        String idUsuario = lineasFromPedido[0].Pedido__r.Contenedor__r.Cod_Con__c;
        String sMethod = 'GET';
        String sUrl = oCStoken.url__c;

        List<Pedido_Det__c> lineasPedidoToUpdateStock = new List<Pedido_Det__c>();
        for (Pedido_Det__c fungible : lineasFromPedido) {
            Map<String, String> bodyMap = new Map<String, String>();
            HttpRequest req = new HttpRequest();
            HTTPResponse res = new HTTPResponse();

            system.debug('URL: '+sUrl + '/stock-fungible');
            req.setEndpoint(sUrl + '/stock-fungible');

            req.setMethod(sMethod);
            req.setHeader('Content-Type','application/json');
            req.setHeader('Authorization', 'Bearer ' + token);

            bodyMap.put('idusuario', idUsuario );
            // bodyMap.put('idusuario', 'PRUEBACONT' );
            bodyMap.put('tipousuario', tipoUsuario );
            
            bodyMap.put('idfungible', fungible.Producto__r.ProductCode); 

            system.debug('BodyMAP WS: '+bodyMap);
            req.setBody(JSON.Serialize(bodyMap));
            
            system.debug('LLAMADA A /stock-fungible');
            Http http = new Http();
            try{
                res = http.send(req);

                if (res.getStatusCode() == 200){
                    system.debug('Ha ido bien la consulta para el fungible: '+fungible);
    
                    StockFungibleResponse stockFungibleResponse = (StockFungibleResponse) JSON.deserialize(res.getBody(), StockFungibleResponse.class);
                    Pedido_Det__c lineaToUpdate = new Pedido_Det__c();
                    lineaToUpdate.Id = fungible.Id;
                    lineaToUpdate.Stock__c = Decimal.valueOf(stockFungibleResponse.disponible);
                    lineasPedidoToUpdateStock.add(lineaToUpdate);
                }else{
                    system.debug('Ha ido MAL la consulta para el fungible: '+fungible);
                    responseMap.put(true, res.getBody());
                }
            }catch(Exception e){
                responseMap.put(true, e.getMessage());

            }
            
        }

        if(responseMap.isEmpty()){
            responseMap.put(false, '');
        }
        update lineasPedidoToUpdateStock;

        return responseMap;
    }


    public class StockFungibleResponse{
        @AuraEnabled public Boolean success {get;set;}
        @AuraEnabled public String code {get;set;}
        @AuraEnabled public String message {get;set;}
        @AuraEnabled public String disponible {get;set;}
    }

}