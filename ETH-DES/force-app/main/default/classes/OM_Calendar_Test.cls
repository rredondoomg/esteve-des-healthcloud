/**
 * @File Name          : OM_Calendar_Test.cls
 * @Description        : 
 * @Author             : David López <sdlopez@omegacrmconsulting.com>
 * @Group              : 
 * @Last Modified By   : David López <sdlopez@omegacrmconsulting.com>
 * @Last Modified On   : 07-10-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/6/2020   David López <sdlopez@omegacrmconsulting.com>     Initial Version
**/
@isTest
private class OM_Calendar_Test {

    @isTest static void testMethod1(){

        List<Site> siteName = [SELECT Name FROM Site LIMIT 1];

        Test.startTest();

        OM_Calendar.createWrappers();
        OM_Calendar.getVisualforceDomain(null);

        if (!siteName.isEmpty()){
            OM_Calendar.getVisualforceDomain(siteName[0].Name);
        }

        Test.stopTest();
    }
}