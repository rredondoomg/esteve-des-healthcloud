public class CustomLookupLwcController {
    // Method to fetch lookup search result   
     @AuraEnabled(cacheable=true)
     public static list<sObject> fetchLookupData(string searchKey , string sObjectApiName) {    
        List < sObject > returnList = new List < sObject > ();
        string sWildCardText = '%' + searchKey + '%';
        string sQuery;
        if(sObjectApiName.equalsIgnoreCase('ServiceAppointment')){
            sQuery = 'Select Id,AppointmentNumber From ' + sObjectApiName + ' Where AppointmentNumber Like : sWildCardText order by createdDate DESC LIMIT 5';
        }else if(sObjectApiName.equalsIgnoreCase('Product2')){
            String RTLookup = 'Fungibles';
            sQuery = 'Select Id,Name,ProductCode  From ' + sObjectApiName + ' Where (Name Like : sWildCardText OR ProductCode =: searchKey )AND RecordType.DeveloperName =: RTLookup order by createdDate DESC LIMIT 5';
        }
        // else if(sObjectApiName.equalsIgnoreCase('Case')){
        //     sQuery = 'Select Id,CaseNumber From ' + sObjectApiName + ' Where CaseNumber Like : sWildCardText order by createdDate DESC LIMIT 5';
        // }
        // else if(sObjectApiName.equalsIgnoreCase('Equipos_sum__c')){
        //     system.debug('Entra en equipo y vale: '+sObjectApiName);
        //     sQuery = 'Select Id,Name From ' + sObjectApiName + ' Where Name Like : sWildCardText order by createdDate DESC LIMIT 5';
        // }
        for (sObject obj: database.query(sQuery)) {
            returnList.add(obj);
        }
        return returnList;
     }
     
     // Method to fetch lookup default value 
     @AuraEnabled
     public static sObject fetchDefaultRecord(string recordId , string sObjectApiName) {
         string sRecId = recordId;    
         string sQuery;
         if(sObjectApiName.equalsIgnoreCase('ServiceAppointment')){
            sQuery = 'Select Id,AppointmentNumber From ' + sObjectApiName + ' Where Id = : sRecId LIMIT 1';
        }else if(sObjectApiName.equalsIgnoreCase('Product2')){
            String RTLookup = 'Fungibles';
            sQuery = 'Select Id,Name From ' + sObjectApiName + ' Where Id = : sRecId AND RecordType.DeveloperName =: RTLookup order by createdDate DESC LIMIT 5';
        }
        // else if(sObjectApiName.equalsIgnoreCase('Case')){
        //     sQuery = 'Select Id,CaseNumber From ' + sObjectApiName + ' Where Id = : sRecId order by createdDate DESC LIMIT 5';
        // }
        // else if(sObjectApiName.equalsIgnoreCase('Equipos_sum__c')){
        //     sQuery = 'Select Id,Name From ' + sObjectApiName + ' Where Id = : sRecId order by createdDate DESC LIMIT 5';
        // }
        for (sObject obj: database.query(sQuery)) {
            return obj;
        }
        return null;
     }
     
     
 }