public with sharing class OM_ActualizarEstadoPedido {
    @AuraEnabled
    public static Map<Boolean, String> actualizarEstadoPedido(String recordId){
        system.debug('recordId: '+recordId);
        
        Map<Boolean, String> responseMap = new Map<Boolean, String>();

        if(recordId != null){
            responseMap = getToken(recordId);        
        }else{
            responseMap.put(true, 'respuestaNull');
            
        }

        return responseMap;
    }


    public static Map<Boolean, String> getToken(String recordId){
        TokenETH__c oCStoken = [SELECT Id, url__c, username__c, password__c, token__c FROM TokenETH__c];

        String sUrl = oCStoken.url__c;
        String sUsername = oCStoken.username__c;
        String sPassword = oCStoken.password__c;
        String sToken = oCStoken.token__c;
        String sMethod = 'POST';

        Map<Boolean,String> responseMap = new Map<Boolean,String>();
        Map<String,Object> tokenResp = new Map<String,Object>();
        
        HttpRequest req = new HttpRequest();
        system.debug('URL TOKEN: '+sUrl + '/login');
        req.setEndpoint(sUrl + '/login');
        req.setMethod(sMethod);
        req.setHeader('Content-Type','application/json');

        Map<String, String> mapAuthen = new Map<String, String>();
        mapAuthen.put('username', sUsername);
        mapAuthen.put('password', sPassword);

        req.setBody(JSON.Serialize(mapAuthen));

        Http http = new Http();
        HTTPResponse res = new HTTPResponse();
        res = http.send(req);

        Pedido__c pedidoToUpdate = new Pedido__c();
        pedidoToUpdate.Id = recordId;

        if (res.getStatusCode() == 200){
            tokenResp = (Map<String,Object>)JSON.deserializeUntyped(res.getBody());
            return actualizarEstadoPedidoDesdeAtlas(String.valueOf(tokenResp.get('accessToken')), oCStoken, recordId);
        }else if(res.getStatusCode() == 401){
            responseMap.put(true, 'No se ha podido obtener el token de Atlas. Codigo '+res.getStatusCode());
            pedidoToUpdate.Actualizacion_estado_OK__c = false;
            pedidoToUpdate.Error_Actualizacion_Estado__c = 'No se ha podido obtener el token de Atlas. Codigo '+res.getStatusCode();
        }else if(res.getStatusCode() == 500){
            responseMap.put(true, 'Error en la comunicación con Atlas. Codigo '+res.getStatusCode());
            pedidoToUpdate.Actualizacion_estado_OK__c = false;
            pedidoToUpdate.Error_Actualizacion_Estado__c = 'Error en la comunicación con Atlas. Codigo '+res.getStatusCode();
        }

        update pedidoToUpdate;

        return responseMap;
    }


    public static Map<Boolean, String> actualizarEstadoPedidoDesdeAtlas(String token, TokenETH__c oCStoken, String recordId){
        Map<Boolean,String> responseMap = new Map<Boolean,String>();
        
        String sUrl = oCStoken.url__c;
        String sUsername = oCStoken.username__c;
        String sPassword = oCStoken.password__c;
        String sToken = oCStoken.token__c;
        String sMethod = 'GET';

        HttpRequest req = new HttpRequest();
        HTTPResponse res = new HTTPResponse();

        req.setEndpoint(sUrl + '/estado-pedido');        
        req.setMethod(sMethod);
        req.setHeader('Content-Type','application/json');
        req.setHeader('Authorization', 'Bearer ' + token);

        Map<String, String> bodyToSend = new Map<String, String>();
        bodyToSend.put('idsalesforce', recordId);
        
        req.setBody(JSON.Serialize(bodyToSend));
        
        system.debug('LLAMADA A /estado-pedido');
        Http http = new Http();
        res = http.send(req);

        WrapperEstadoPedidoResponse atlasResponse;
        String errorMessage = '';
        Pedido__c pedidoToUpdate = new Pedido__c();
        pedidoToUpdate.Id = recordId;

        if (res.getStatusCode() == 200){
            
            atlasResponse = (WrapperEstadoPedidoResponse) JSON.deserialize(res.getBody(), WrapperEstadoPedidoResponse.class);
            if(atlasResponse.code == 200){
                if(atlasResponse.message.equalsIgnoreCase('')){
                    String valorEstado;
                    if(atlasResponse.estado == 'P'){
                        valorEstado = 'Pendiente';
                    }else if(atlasResponse.estado == 'A' ){
                        valorEstado = 'Anulado';
                    }else if(atlasResponse.estado == 'R'){
                        valorEstado = 'Registrado';
                    }else if(atlasResponse.estado == 'E'){
                        valorEstado = 'Entregado';
                    }
                    pedidoToUpdate.Estado__c = valorEstado;
                    pedidoToUpdate.Actualizacion_estado_OK__c = true;
                    pedidoToUpdate.Error_Actualizacion_Estado__c = '';

                    update pedidoToUpdate;
                }else{
                    pedidoToUpdate.Actualizacion_estado_OK__c = false; pedidoToUpdate.Error_Actualizacion_Estado__c = atlasResponse.message;
                }
                responseMap.put(false, '');
            }else{
                pedidoToUpdate.Actualizacion_estado_OK__c = false; pedidoToUpdate.Error_Actualizacion_Estado__c = atlasResponse.message;
                responseMap.put(true, atlasResponse.message);
            }
        }else{
            responseMap.put(true, 'Código: '+res.getStatusCode() + '. Mensaje: '+res.getBody());
            pedidoToUpdate.Actualizacion_estado_OK__c = false; pedidoToUpdate.Error_Actualizacion_Estado__c = 'Código: '+res.getStatusCode() + '. Mensaje: '+res.getBody();
        }

        update pedidoToUpdate;

        return responseMap;

    }



    public class WrapperEstadoPedidoResponse{
        public Boolean success;
        public Integer code;
        public String message;
        public String estado;
    }
}