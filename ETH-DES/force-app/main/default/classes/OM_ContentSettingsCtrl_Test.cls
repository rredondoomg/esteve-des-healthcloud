/**
 * @description       Test class for OM_ContentSettingsCtrl
 * @author            OmegaCRM 
 * @group             Content tests
**/
@isTest
private class OM_ContentSettingsCtrl_Test {

    public static final String USERNAME = 'testcontentUser@content.omegacrconsulting.com';


    @TestSetup
    static void makeData(){
        // Insert user with corect permission set for testing (any profile)
        Profile pf = [SELECT Id FROM Profile WHERE UserLicense.Name = 'Salesforce' LIMIT 1];
        User u = new User(
            Username = USERNAME,
            FirstName = 'Test',
            LastName = 'Omega',
            ProfileId = pf.Id,
            Email = USERNAME,
            Alias = 'test',
            TimeZoneSidKey = 'GMT',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'ISO-8859-1',
            LanguageLocaleKey = 'en_US'
        );

        insert u;

        PermissionSet contUser = [SELECT Id FROM PermissionSet WHERE Name = 'OM_ContentUser' LIMIT 1];
        PermissionSetAssignment psass = new PermissionSetAssignment(
            AssigneeId = u.Id,
            PermissionSetId = contUser.Id
        );

        insert psass;
    }
    
    @isTest
    static void signOutTest() {
        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        Test.startTest();
        System.runAs(contextuser){
            OM_ContentSetting__c contentSetting = new OM_ContentSetting__c(OM_Columns__c = 'name');
            insert contentSetting;

            OM_ContentService.updateUserAccess(contentSetting, UserInfo.getUserId(), '{"access_token": "test", "refresh_token": "test"}'); 

            System.assert(OM_ContentSettingsCtrl.signOut(contentSetting.Id), 'Sign out method should return true');
            List<OM_ContentAccessData__c> accesses = new List<OM_ContentAccessData__c>([SELECT Id FROM OM_ContentAccessData__c WHERE OM_Content_setting__c = :contentSetting.Id]);
            System.assertEquals(0, accesses.size(), 'All accesses should be deleted');
        }
    }

    @isTest
    static void signStatusTest() {
        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        Test.startTest();
        System.runAs(contextuser){
            OM_ContentSetting__c contentSetting = new OM_ContentSetting__c(OM_Columns__c = 'name', OM_Name__c = 'testSettings', OM_Auth_type__c = OM_ContentService.SHARED );
            insert contentSetting;

            OM_ContentService.updateUserAccess(contentSetting, UserInfo.getUserId(), '{"access_token": "test", "refresh_token": "test"}'); 
            
            try {
                OM_ContentSettingsCtrl.checkLoginStatus('noExiste');
            } catch(Exception e) {
                System.assertNotEquals(null, e, 'Exception should be thrown');
            }

            System.assertEquals(true, OM_ContentSettingsCtrl.checkLoginStatus('testSettings'), 'Sign status should be true');
        }
    }

    @isTest
    static void checkValidQueryTest() {
        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        Test.startTest();
        System.runAs(contextuser){
            System.assert(OM_ContentSettingsCtrl.checkValidQuery('Contact','Id != null'), 'Should be a valid query');
            System.assert(!OM_ContentSettingsCtrl.checkValidQuery('Random','Id != null'), 'Should be a valid query');
        }
    }
}