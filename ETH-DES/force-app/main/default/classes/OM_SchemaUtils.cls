/**
 * @File Name          OM_SchemaUtils.cls
 * @Description        Schema util methods
 * @Author             OmegaCRM 
 * @Group              Utilities
 * @Last Modified By   OmegaCRM 
 * @Last Modified On   16/3/2020 23:02:02
**/
global inherited sharing class OM_SchemaUtils {

    // Cachear getGlobalDescribe para mejorar eficiencia
    private static Map<String, Schema.SObjectType> globalDescribe{get {
        if (globalDescribe == null){
            globalDescribe = Schema.getGlobalDescribe();
        }
        return globalDescribe;
    } set;}

    /**
     * @description Obtiene el getDescribe del SObject pasado por parámetro
     * @param  obj SObject en String
     * @return     SObjectType del objeto
     */
    global static Schema.SObjectType getTypeSObject(String obj){        
        return globalDescribe.get(obj);
    }

    /**
     * @description Obtiene el getDescribe del SObject pasado por parámetro
     * @param  obj SObject en String
     * @return     DescribeSObjectResult del objeto
     */
    global static Schema.DescribeSObjectResult getDescribeSObject(String obj){        
        return getTypeSObject(obj).getDescribe();
    }
    
    /**
     * @description Obtiene un mapa con todos los campos del objeto pasado por parámetro
     * @param  obj SObject en String
     * @return     Mapa con <String con nombre del campo, Sobjectfield>
     */
    global static Map<String, Schema.SObjectfield> getFieldsSObjectMap(String obj){
        return getDescribeSObject(obj).Fields.getMap();
    }

    /**
     * @description Obtiene un mapa con todos los campos del objeto pasado por parámetro, y su label
     * @param  obj SObject en String
     * @return     Mapa con <String con nombre del campo, String label del campo>
     */
    global static Map<String, String> getFieldsSObjectMapLabels(String obj){
        Map<String, String> ret = new Map<String, String>();
        Map<String, Schema.Sobjectfield> mapa = getFieldsSObjectMap(obj);

        for (String str : mapa.keySet()){
            ret.put(str, mapa.get(str).getDescribe().getLabel());
        }
        return ret;
    }

    /**
     * @description Obtiene una lista de todos los valores de una picklist
     * @param  obj   SObject en String
     * @param  field Field picklist en String
     * @return       Lista de valores de la picklist
     */
    global static List<Schema.PicklistEntry> getPicklistValues(String obj, String field){
        return getFieldsSObjectMap(obj).get(field).getDescribe().getPicklistValues();
    }
    
    /**
     * @description Obtiene un mapa con todos los valores activos de una picklist
     * @param  obj   SObject en String
     * @param  field Field picklist en String
     * @return       Mapa con <API Name, Label>
     */
    global static Map<String, String> getPicklistValuesActiveMap(String obj, String field){
        Map<String, String> ret = new Map<String, String>();
        for (Schema.PicklistEntry pl : getPicklistValues(obj, field)){
            if (pl.isActive()){
                ret.put(pl.getValue(), pl.getLabel());
            }
        }
        return ret;
    }

    /**
     * @description Obtiene un mapa con todos los Record Type de un objeto
     * @param  obj SObject en String
     * @return     Mapa con <Developer Name, Id del RecordType>
     */
    global static Map<String, Id> getRecordTypeIdMap(String obj){
        Map<String, Id> rtMap = new Map<String, Id>();        
        for(Schema.RecordTypeInfo rt : getDescribeSObject(obj).getRecordTypeInfos()){            
            rtMap.put(rt.getDeveloperName(), rt.getRecordTypeId());            
        }
        return rtMap;
    }

    /**
     * @description Obtiene el Id de un RecordType
     * @param  obj               SObject en String
     * @param  recordTypeDevName Developer Name del Record Type
     * @return                   Id del Record Type
     */
    global static Id getRecordTypeId(String obj, String recordTypeDevName){
        return getRecordTypeIdMap(obj).containsKey(recordTypeDevName) ? getRecordTypeIdMap(obj).get(recordTypeDevName) : null;
    }

    /**
     * @description Devuelve el primer valor de la picklist (útil para métodos de Test)
     * @param  obj   SObject en String
     * @param  field Field picklist en String
     * @return       String con API
     */
    global static String getFirstValuePicklist(String obj, String field){
        List<String> listValue = new List<String>();
        listValue.addAll(getPicklistValuesActiveMap(obj, field).keySet());
        return listValue[0];
    }

    private static Boolean isPlayableSObject(DescribeSObjectResult obDesc) { 
        return !obDesc.isCustomSetting() 
            && !obDesc.isDeprecatedAndHidden()
            && obDesc.isSearchable()  
            && obDesc.isCreateable();
    }

    /**
    * @description Devuelve la lista de sobject
    * @author dcastellon@omegacrmconsulting.com | 12-29-2021 
    * @return List<Object> 
    **/
    global static List<Object> getSObjects() {
        List<Object> objs = new List<Object>();
        for(Schema.sObjectType obj : globalDescribe.values()) {
            DescribeSObjectResult obDesc = obj.getDescribe();
            if(isPlayableSObject(obDesc)) {
                objs.add(new Map<String, Object> {
                    'label' => obDesc.getLabel(), 
                    'value' => obDesc.getName()
                });
            }
        }
        return objs;
    }

    /**
     * @description Devuelve una lista de los campos requeridos (no "requeridos" como tal en el setup de SF, isNillable comprueba si un campo puede tener valor nulo o no, por eso descarto valores Boolean y los que se completan de manera estándar)
     * @param  obj SObject en String
     * @return     List<String> con los campos
     */
    global static List<String> getRequiredFields(String obj){
        List<String> ret = new List<String>();
        Map<String, Schema.Sobjectfield> mapaFields = getFieldsSObjectMap(obj);
        for (String fieldName : mapaFields.keySet()){
            if(!mapaFields.get(fieldName).getDescribe().isNillable()){
                if (
                    mapaFields.get(fieldName).getDescribe().getType() != Schema.DisplayType.Boolean && 
                    fieldName != 'id' && 
                    fieldName != 'ownerid' && 
                    fieldName != 'createddate' && 
                    fieldName != 'createdbyid' && 
                    fieldName != 'lastmodifieddate' && 
                    fieldName != 'lastmodifiedbyid' && 
                    fieldName != 'systemmodstamp')
                {
                    ret.add(fieldName);
                }
            }
        }
        return ret;
    }

    /**
     * @description Devuelve el texto de ayuda (Inline Help Text) del campo
     * @param  obj   SObject en String 
     * @param  field API Name del campo
     * @return       Inline Help Text
     */
    global static String getHelpText(String obj, String field){
        Map<String, Schema.Sobjectfield> mapa = getFieldsSObjectMap(obj);
        if (mapa.containsKey(field)){
            return mapa.get(field).getDescribe().getInlineHelpText();
        }
        return null;
    }

    /**
     * @description Devuelve el tipo del campo consultado
     * @param  obj   SObject en String 
     * @param  field API Name del campo
     * @return       Type Text
     */
    global static Schema.DisplayType getType(String obj, String field){
        Map<String, Schema.Sobjectfield> mapa = getFieldsSObjectMap(obj);
        if (mapa.containsKey(field)){
            return mapa.get(field).getDescribe().getType();
        }
        return null;
    }

    /**
     * @description Devuelve el tipo de objeto como String a patir de un Id
     * @param  id    Id de Salesforce
     * @return       String con tipo de SObject
     */
    global static String getSObjectTypeFromId(Id id){
        return String.valueOf(id.getSobjectType());
    }

    /**
    * @description Obtiene el listado de campos de un objeto, separado por comas
    * @param String SObject en String 
    * @return       String con campos
    **/
    global static String getAllFieldsCommaSeparated(String obj) {
        return String.join(new List<String>(getFieldsSObjectMap(obj).keySet()), ',');
    }
    
    /**
    * @description Obtiene el SObject con todos los campos correspondiente al Id 
    * @param Id Id de Salesforce
    * @return   SObject 
    **/
    global static SObject queryAllFields(Id id) {
        List<SObject> objs = Database.query('SELECT ' + getAllFieldsCommaSeparated(getSObjectTypeFromId(id)) + ' FROM ' + getSObjectTypeFromId(id) + ' WHERE Id = :id');
        return objs.isEmpty() ? null : objs[0];
    }
    
    /**
    * @description Obtiene el listado de campos accesible por el usuario para el objeto pasado por parámetro
    * @author David López <sdlopez@omegacrmconsulting.com> | 05-23-2021 
    * @param String SObject en String 
    * @return       String con campos 
    **/
    global static String getAllFieldsAccesibleCommaSeparated(String obj) {

        List<String> fields = new List<String>();

        for (Schema.SObjectfield field : getFieldsSObjectMap(obj).values()) {
            Schema.DescribeFieldResult dfr = field.getDescribe();
            if (dfr.isAccessible()) {
                fields.add(dfr.getName());
            }
        }

        return !fields.isEmpty() ? String.join(fields, ',') : null;
    }

    /**
    * @description Obtiene el SObject con todos los campos accesibles por el usuario correspondiente al Id 
    * @param Id Id de Salesforce
    * @return   SObject 
    **/
    global static SObject queryAllFieldsAccesible(Id id) {
        
        String sObjectType = getSObjectTypeFromId(id);
        String fields = getAllFieldsAccesibleCommaSeparated(sObjectType);

        if (fields != null) {
            List<SObject> objs = Database.query('SELECT ' + fields + ' FROM ' + sObjectType + ' WHERE Id = :id');
            return objs.isEmpty() ? null : objs[0];
        }

        return null;
    }

    /**
    * @description Obtiene un Mapa con los Fieldset del objeto pasado por parámetro
    * @author David López <sdlopez@omegacrmconsulting.com> | 10-26-2021 
    * @param String obj -> SObject en String 
    * @return Map<String, Schema.FieldSet> 
    **/
    global static Map<String,Schema.FieldSet> getFieldSetMap(String obj){
        return getDescribeSObject(obj)?.FieldSets?.getMap();
    }

    /**
    * @description Obtiene el listado de FieldSetMember del objeto y FieldSet pasado por parámetro
    * @author David López <sdlopez@omegacrmconsulting.com> | 10-26-2021 
    * @param String obj -> SObject en String 
    * @param String fieldSetName -> Nombre del Fieldset
    * @return List<Schema.FieldSetMember> 
    **/
    global static List<Schema.FieldSetMember> getFieldSet(String obj, String fieldSetName){
        return getFieldSetMap(obj)?.get(fieldSetName)?.getFields();
    }

    /**
     * @description Obtiene valores dependientes para cada valor de una picklist de control
     * @param  obj              SObject en String 
     * @param  dependantField   API Name del campo dependiente
     * @param  controllingField API Name del campo de control
     * @return                  Map<String, Map<String, String>> -> Para cada valor de picklist, sus posibles valores dependientes en Map<value, label>
     */
    global static Map<String, Map<String, String>> getDependantValuesPicklist(String obj, String dependantField, String controllingField){

		Map<String, Map<String, String>> objResults = new Map<String, Map<String, String>>();
		List<String> controllingValues = new List<String>();

		for (Schema.PicklistEntry ple : getPicklistValues(obj, controllingField)) {
			String value = ple.getValue();
			objResults.put(value, new Map<String, String>());
			controllingValues.add(value);
		}

		for (PicklistEntryWrapper plew : (List<PicklistEntryWrapper>)JSON.deserialize(JSON.serialize(getPicklistValues(obj, dependantField)), List<PicklistEntryWrapper>.class)) {
            
			String validForBits = base64ToBits(plew.validFor);
			for (Integer i = 0; i < validForBits.length(); i++) {
				if ((validForBits.mid(i, 1)).equals('1')) {
					objResults.get(controllingValues.get(i)).put(plew.value, plew.label);
				}
			}
		}

        return objResults;
    }

    @TestVisible
	private static String base64ToBits(String validFor) {
		if (validFor == null || validFor == ''){ return ''; }

		String validForBits = '';
		for (Integer i = 0; i < validFor.length(); i++) {
			validForBits += decimalToBinary(BASE_64_CHARS.indexOf(validFor.mid(i, 1))).leftPad(6, '0');
		}
		return validForBits;
	}

    private static String decimalToBinary(Integer val) {
		String bits = '';
		while (val > 0) {
			Integer remainder = Math.mod(val, 2);
			val = Integer.valueOf(Math.floor(val / 2));
			bits = String.valueOf(remainder) + bits;
		}
		return bits;
	}

	private static final String BASE_64_CHARS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

    private class PicklistEntryWrapper {
		private String active {get; set;}
		private String defaultValue {get; set;}
		private String label {get; set;}
		private String value {get; set;}
		private String validFor {get; set;}
    }
    
    
    
}