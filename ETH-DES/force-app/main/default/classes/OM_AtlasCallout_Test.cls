/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 09-01-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
@isTest
public with sharing class OM_AtlasCallout_Test {
    @TestSetup
    static void makeData(){
        ETH_DataFactory.setUpOmegaContent();
        //crear un usuario
        User userProfesional = ETH_DataFactory.createUser('usertestethprofesional@test.com');
        
        insert userProfesional;
        User emailFromuser = [SELECT Id, codigo__c FROM User WHERE Email = 'usertestethprofesional@test.com'];
        system.debug(' user:: '+emailFromuser);
        User user = [SELECT Id FROM User WHERE LastName = 'Omega' LIMIT 1];

        System.runAs(user){
            MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
            Test.setMock(HttpCalloutMock.class, multimock);

            Test.startTest();

            Account account = ETH_DataFactory.createAccountWithAddress('Spain', 'Granada', 'Periodista Fernando Gomez de la Cruz', '18014', 'Granada');
            insert account;

            OperatingHours operatingHours = ETH_DataFactory.createOperatingHours();
            insert operatingHours;

            //timeslots de operating hours
            ETH_DataFactory.createTimeSlots(operatingHours.Id, 5, 23);

            ServiceTerritory serviceTerritoryPadre = ETH_DataFactory.createServiceTerritory(null, operatingHours.Id, 'TRD', null);
            insert serviceTerritoryPadre;

            ServiceTerritory serviceTerritoryHijo = ETH_DataFactory.createServiceTerritory(account.ShippingPostalCode, operatingHours.Id, 'TRD', serviceTerritoryPadre.Id);
            insert serviceTerritoryHijo;

            //crear un assigned resource a cada usuario
            ServiceResource serviceResourceProfesional = ETH_DataFactory.createServiceResourceProfesional(userProfesional.Id);
            insert serviceResourceProfesional;

            //crear los services territories member
            ServiceTerritoryMember serviceTerritoryMemberProfesional = ETH_DataFactory.createServiceTerritoryMember('Asistencial', serviceResourceProfesional.Id, serviceTerritoryPadre.Id, operatingHours.Id);
            insert serviceTerritoryMemberProfesional;

            //repetimos proceso para CRETA y box
            ServiceTerritory serviceTerritoryCRETA = ETH_DataFactory.createServiceTerritory(account.ShippingPostalCode, operatingHours.Id, 'Creta',  null);
            insert serviceTerritoryCRETA;

            Asset asset = ETH_DataFactory.createAsset(account.Id);
            insert asset;

            ServiceResource serviceResourceBox = ETH_DataFactory.createServiceResourceBox(asset.Id);
            insert serviceResourceBox;

            ServiceTerritoryMember serviceTerritoryMemberBox = ETH_DataFactory.createServiceTerritoryMember('', serviceResourceBox.Id, serviceTerritoryCRETA.Id, operatingHours.Id);
            insert serviceTerritoryMemberBox;

            //alta
            Worktype worktypeAlta = ETH_DataFactory.createWorktype('Alta', 'TRD', 'Presencial', 'Domicilio');
            insert worktypeAlta;
            //domicilio
            Worktype worktypeSeguimientoDomicilio = ETH_DataFactory.createWorktype('Seguimiento', 'TRD', 'Presencial', 'Domicilio');
            insert worktypeSeguimientoDomicilio;

            Worktype worktypeBaja = ETH_DataFactory.createWorktype('Fin', 'TRD', 'Presencial', 'Domicilio');
            insert worktypeBaja;


            Worktype worktypeSeguimiento = ETH_DataFactory.createWorktype('Seguimiento', 'TRD', 'Virtual', 'CRETA');
            insert worktypeSeguimiento;


            TokenETH__c oCStoken = new TokenETH__c();
            oCStoken.url__c = 'http://93.90.29.156:9002';
            oCStoken.username__c = 'omega';
            oCStoken.password__c = '44V-7J6RE-oLs&n.b7';
            oCStoken.token__c = '11111111111';
            insert oCStoken;

            // Product2 equipoTest = [SELECT Id, Name FROM Product2 WHERE Name = 'Equipo TEST'];

            Product2 equipoTest = new Product2();
            equipoTest.Name = 'Equipo Test';
            equipoTest.ProductCode = 'EquipoTest';
            equipoTest.Marca__C = 'EquipoTest';
            equipoTest.Modelo__c = 'EquipoTest';
            equipoTest.Identificador__c = 'EquipoTest123';
            equipoTest.ProductCode = 'EquipoTest';
            insert equipoTest;

        }
        Test.stopTest();
    }

    private static void crearTratamientosVisitaEquiposYFungibles(String serviceAppId, String tratamientoId, String pacienteId, Integer numeroPrincipales){
        Visita_Tratamiento__c visTrat = new Visita_Tratamiento__c();
        visTrat.Name = 'TratamientoTest';
        visTrat.Service_Appointment__c = serviceAppId;
        visTrat.Tratamiento__c = tratamientoId;
        visTrat.Paciente__c = pacienteId;

        insert visTrat;

        CodeSet codSet = new CodeSet();
        codSet.Name = 'GENERICO';
        codSet.Code = 'GENERICO'; 
        codSet.IsActive = true; 
        codSet.IsCustomCode = false; 
        codSet.IsPrimary = true; 
        // codSet.CodeSetKey = 345342343;
        insert codSet;

        Product2 prodEquipo = new Product2();
        prodEquipo.Name = 'EquipoSuministrado';
        prodEquipo.ProductCode = 'EquipoSuministrado';
        prodEquipo.Marca__c = 'MarcaEquipoSum';
        prodEquipo.Modelo__c = 'ModeloEquipoSum';
        prodEquipo.Identificador__c = 'Ident';
        prodEquipo.familia_aprt__c = 'ACPAP';
        // prodEquipo.familia_fun__c = 'AC';
        prodEquipo.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Aparatos').getRecordTypeId();
        insert prodEquipo;

        Equipos_Sum__c equipoSum = new Equipos_Sum__c();
        equipoSum.Name = 'EquipoSuministrado';
        equipoSum.Marca_modelo_equipo__c = prodEquipo.Id;
        equipoSum.Tratamiento__c = tratamientoId;
        equipoSum.PersonAccount__c = pacienteId;
        equipoSum.Visita_tratamiento__c = visTrat.Id;
        equipoSum.Cod_eth__c = '0000021311';
        equipoSum.Num_Fabricante__c = '12531';
        if(numeroPrincipales >= 1){
            equipoSum.Equipo_principal__c = true;
        }
        insert equipoSum;


        Product2 prodEquipo2 = new Product2();
        prodEquipo2.Name = 'EquipoSuministrado2';
        prodEquipo2.ProductCode = 'EquipoSuministrado2';
        prodEquipo2.Marca__c = 'MarcaEquipoSum2';
        prodEquipo2.Modelo__c = 'ModeloEquipoSum2';
        prodEquipo2.Identificador__c = 'Ident2';
        prodEquipo2.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Aparatos').getRecordTypeId();
        insert prodEquipo2;

        Equipos_Sum__c equipoSum2 = new Equipos_Sum__c();
        equipoSum2.Name = 'EquipoSuministrado2';
        equipoSum2.Marca_modelo_equipo__c = prodEquipo2.Id;
        equipoSum2.Tratamiento__c = tratamientoId;
        equipoSum2.PersonAccount__c = pacienteId;
        equipoSum2.Visita_tratamiento__c = visTrat.Id;
        equipoSum2.Cod_eth__c = '0000021312';
        equipoSum2.Num_Fabricante__c = '12532';
        if(numeroPrincipales >=2){
            equipoSum2.Equipo_principal__c = true;
        }
        insert equipoSum2;


        Product2 prodFungible = new Product2();
        prodFungible.Name = 'fungibleSuministrado';
        prodFungible.ProductCode = '2';
        prodFungible.Marca__c = 'MarcafungibleSum';
        prodFungible.Modelo__c = 'ModelofungibleSum';
        prodFungible.Identificador__c = 'IdentFung';
        prodFungible.familia_fun__c = 'AC';

        prodFungible.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Fungibles').getRecordTypeId();
        insert prodFungible;

        Fungibles_Sum__c fungibleSum = new Fungibles_Sum__c();
        fungibleSum.Marca_modelo_fungible__c = prodFungible.Id;
        fungibleSum.Case__c = tratamientoId;
        fungibleSum.Paciente__c = pacienteId;
        fungibleSum.Visita_tratamiento__c = visTrat.Id;
        fungibleSum.Cantidad__c = 1;
        insert fungibleSum;


        //Lecturas anteriores
        Account account = [SELECT Id FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'TRD');
        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, null, 'TRD');
        tratamiento.fam_aprt_comp__c = 'ACPAP';
        insert tratamiento;
        ca.Tratamiento__c = tratamiento.Id;
        insert ca;

        WorkType worktypeAlta = [SELECT Name, Id, EstimatedDuration FROM WorkType WHERE Name LIKE '%Alta%' LIMIT 1];
        User userProfesional = [SELECT Id FROM User WHERE Email='usertestethprofesional@test.com' LIMIT 1];

        ServiceAppointment sapp = new ServiceAppointment(
            DurationType = 'Minutes',
            RecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('Visita_TRD').getRecordTypeId(),
            Status = 'Prevista',
            ParentRecordId = account.Id,
            WorkTypeId = worktypeAlta.Id,
            Duration = worktypeAlta.EstimatedDuration,
            Tratamiento__c = ca.Id,
            Linea_negocio__c = 'TRD',
            Fecha_Realizada__c = Datetime.now()
        );
        insert sapp;

        Visita_Tratamiento__c visTra2 = new Visita_Tratamiento__c();
        visTra2.Name = 'TratamientoTest';
        visTra2.Service_Appointment__c = sapp.Id;
        visTra2.Tratamiento__c = ca.Id;
        visTra2.Paciente__c = account.Id;

        insert visTra2;



        Product2 prodEquipo5 = new Product2();
        prodEquipo5.Name = 'EquipoSuministrado';
        prodEquipo5.ProductCode = 'EquipoSuministrado';
        prodEquipo5.Marca__c = 'MarcaEquipoSum2';
        prodEquipo5.Modelo__c = 'ModeloEquipoSum2';
        prodEquipo5.Identificador__c = 'Ident3';
        prodEquipo5.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Aparatos').getRecordTypeId();
        insert prodEquipo5;

        Equipos_Sum__c equipoSum5 = new Equipos_Sum__c();
        equipoSum5.Name = 'EquipoSuministrado';
        equipoSum5.Marca_modelo_equipo__c = prodEquipo5.Id;
        equipoSum5.Tratamiento__c = tratamientoId;
        equipoSum5.PersonAccount__c = pacienteId;
        equipoSum5.Visita_tratamiento__c = visTra2.Id;
        equipoSum5.Cod_eth__c = '0000021312';
        equipoSum5.Num_Fabricante__c = '12532';
        if(numeroPrincipales >=2){
            equipoSum5.Equipo_principal__c = true;
        }
        insert equipoSum5;

    }

    private static void transcursoCita(ServiceAppointment cita){
        cita.Status = 'Programada';
        cita.SchedStartTime = DateTime.newInstance(Date.today(), Time.newInstance(9,0,0,0));
        update cita;
        cita.Status = 'En Curso';
        cita.ActualStartTime = DateTime.now();
        update cita;
    }


    @isTest
    public static void checkEquiposFungiblesPresencial(){
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);
        
        Account account = [SELECT Id FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'TRD');
        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, null, 'TRD');
        insert tratamiento;
        ca.Tratamiento__c = tratamiento.Id;
        insert ca;



        WorkType worktypeAlta = [SELECT Name, Id, EstimatedDuration FROM WorkType WHERE Name LIKE '%Alta%' LIMIT 1];
        User userProfesional = [SELECT Id FROM User WHERE Email='usertestethprofesional@test.com' LIMIT 1];

        ServiceAppointment sapp = new ServiceAppointment(
            DurationType = 'Minutes',
            RecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('Visita_TRD').getRecordTypeId(),
            Status = 'Prevista',
            ParentRecordId = account.Id,
            WorkTypeId = worktypeAlta.Id,
            Duration = worktypeAlta.EstimatedDuration, 
            ActualStartTime = Datetime.now(), 
            Fecha_Realizada__c = Datetime.now(), 
            Tratamiento__c = ca.Id,
            Tipo_lugar__c = 'Presencial',
            Linea_negocio__c = 'TRD'
        );
        // insert sapp;


        Test.startTest();
        System.runAs(userProfesional){
            insert sapp;
            crearTratamientosVisitaEquiposYFungibles(sapp.Id, ca.Id, account.Id, 0);

            transcursoCita(sapp);
        }
        Test.stopTest();

        OM_AtlasCallout.checkEquiposFungibles(sapp.Id);
        
    }

    @isTest
    public static void checkEquiposFungiblesVirtual(){
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);
        
        Account account = [SELECT Id FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'TRD');
        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, null, 'TRD');
        insert tratamiento;
        ca.Tratamiento__c = tratamiento.Id;
        insert ca;



        WorkType worktypeAlta = [SELECT Name, Id, EstimatedDuration FROM WorkType WHERE Name LIKE '%Alta%' LIMIT 1];
        User userProfesional = [SELECT Id FROM User WHERE Email='usertestethprofesional@test.com' LIMIT 1];

        ServiceAppointment sapp = new ServiceAppointment(
            DurationType = 'Minutes',
            RecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('Visita_TRD').getRecordTypeId(),
            Status = 'Prevista',
            ParentRecordId = account.Id,
            WorkTypeId = worktypeAlta.Id,
            Duration = worktypeAlta.EstimatedDuration, 
            ActualStartTime = Datetime.now(), 
            Fecha_Realizada__c = Datetime.now(),
            Tratamiento__c = ca.Id,
            Tipo_lugar__c = 'Virtual',
            Linea_negocio__c = 'TRD'
        );
        // insert sapp;


        Test.startTest();
        System.runAs(userProfesional){
            insert sapp;
            crearTratamientosVisitaEquiposYFungibles(sapp.Id, ca.Id, account.Id, 0);

            transcursoCita(sapp);
        }
        Test.stopTest();

        OM_AtlasCallout.checkEquiposFungibles(sapp.Id);
        
    }

    @isTest
    public static void checkEquiposSuministrados(){
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);
        
        Account account = [SELECT Id FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'TRD');
        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, null, 'TRD');
        insert tratamiento;
        ca.Tratamiento__c = tratamiento.Id;
        insert ca;

        WorkType worktypeAlta = [SELECT Name, Id, EstimatedDuration FROM WorkType WHERE Name LIKE '%Alta%' LIMIT 1];
        User userProfesional = [SELECT Id FROM User WHERE Email='usertestethprofesional@test.com' LIMIT 1];

        ServiceAppointment sapp = new ServiceAppointment(
            DurationType = 'Minutes',
            RecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('Visita_TRD').getRecordTypeId(),
            Status = 'Prevista',
            ParentRecordId = account.Id,
            WorkTypeId = worktypeAlta.Id,
            Duration = worktypeAlta.EstimatedDuration, 
            ActualStartTime = Datetime.now(),
            Fecha_Realizada__c = Datetime.now(),
            Tratamiento__c = ca.Id,
            Linea_negocio__c = 'TRD'
        );
        insert sapp;

        crearTratamientosVisitaEquiposYFungibles(sapp.Id, ca.Id, account.Id, 0);

        Test.startTest();
        System.runAs(userProfesional){
            transcursoCita(sapp);
        }
        Test.stopTest();

        OM_AtlasCallout.checkEquiposSuministrados(sapp.Id);
        
    }

    @isTest
    public static void informarEquiposRetirados(){
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);
        
        Account account = [SELECT Id FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'TRD');
        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, null, 'TRD');
        insert tratamiento;
        ca.Tratamiento__c = tratamiento.Id;
        insert ca;

        WorkType worktypeAlta = [SELECT Name, Id, EstimatedDuration FROM WorkType WHERE Name LIKE '%Alta%' LIMIT 1];
        User userProfesional = [SELECT Id FROM User WHERE Email='usertestethprofesional@test.com' LIMIT 1];

        ServiceAppointment sapp = new ServiceAppointment(
            DurationType = 'Minutes',
            RecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('Visita_TRD').getRecordTypeId(),
            Status = 'Prevista',
            ParentRecordId = account.Id,
            WorkTypeId = worktypeAlta.Id,
            Duration = worktypeAlta.EstimatedDuration, 
            ActualStartTime = Datetime.now(), 
            Tratamiento__c = ca.Id,
            Fecha_Realizada__c = Datetime.now(),
            Linea_negocio__c = 'TRD'
        );
        insert sapp;

        crearTratamientosVisitaEquiposYFungibles(sapp.Id, ca.Id, account.Id, 0);

        Test.startTest();
        System.runAs(userProfesional){
            transcursoCita(sapp);
        }
        Test.stopTest();

        OM_AtlasCallout.informarEquiposRetirados(sapp.Id);
        
    }

    @isTest
    public static void comprobarStocksPresencial(){
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);
        
        Account account = [SELECT Id FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'TRD');
        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, null, 'TRD');
        insert tratamiento;
        ca.Tratamiento__c = tratamiento.Id;
        insert ca;

        WorkType worktypeAlta = [SELECT Name, Id, EstimatedDuration FROM WorkType WHERE Name LIKE '%Alta%' LIMIT 1];
        User userProfesional = [SELECT Id FROM User WHERE Email='usertestethprofesional@test.com' LIMIT 1];

        Contenedor__c contenedorServiceTerritory = new Contenedor__c();
        // contenedorServiceTerritory.Usuario_asignado__c = userProfesional.Id;
        contenedorServiceTerritory.Cod_con__c = 'codigoCont';
        insert contenedorServiceTerritory;

        OperatingHours oHours = new OperatingHours();
        oHours.Name = 'HorarioCreta';
        oHours.TimeZone = 'Europe/Madrid';
        insert oHours;

        ServiceTerritory sTerritor = new ServiceTerritory();
        sTerritor.Name = 'TestServTerr';
        sTerritor.RecordTypeId = Schema.SObjectType.ServiceTerritory.getRecordTypeInfosByDeveloperName().get('Creta').getRecordTypeId();
        sTerritor.Street = 'Avinguda de Josep Tarradellas 91';
        sTerritor.City = 'Barcelona';
        sTerritor.State = 'Barcelona';
        sTerritor.PostalCode = '08029';
        sTerritor.Country = 'Spain';
        sTerritor.StateCode = '08';
        sTerritor.CountryCode = 'ES';
        sTerritor.Contenedor__c = contenedorServiceTerritory.Id;
        sTerritor.sto_pro__c = true;
        sTerritor.OperatingHoursId = oHours.Id;
        sTerritor.IsActive = true;

        insert sTerritor;

        ServiceAppointment sapp = new ServiceAppointment(
            DurationType = 'Minutes',
            RecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('Visita_TRD').getRecordTypeId(),
            Status = 'Prevista',
            ParentRecordId = account.Id,
            WorkTypeId = worktypeAlta.Id,
            Duration = worktypeAlta.EstimatedDuration, 
            ActualStartTime = Datetime.now(),
            Fecha_Realizada__c = Datetime.now(),
            Tipo_lugar__c = 'Presencial', 
            ServiceTerritoryId = sTerritor.Id, 
            Tratamiento__c = ca.Id,
            Linea_negocio__c = 'TRD'
        );
        insert sapp;

        Asset assetForServiceResource = new Asset();
        assetForServiceResource.AccountId = account.Id;
        assetForServiceResource.IsCompetitorProduct = false;
        assetForServiceResource.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByDeveloperName().get('Box').getRecordTypeId();
        assetForServiceResource.IsInternal = false;
        // assetForServiceResource.AssetLevel = 1;
        assetForServiceResource.Activo__c = true;
        assetForServiceResource.Name = 'TestAsset';
        insert assetForServiceResource;

        List<ServiceResource> pruebaService = [SELECT Id, resourceType FROM ServiceResource WHERE RelatedRecordId =: userProfesional.Id];
        pruebaService[0].resourceType = 'S';
        // pruebaService[0].ServiceAppointment = sapp.Id;
        pruebaService[0].RelatedRecordId = null;
        pruebaService[0].AssetId = assetForServiceResource.Id;

        update pruebaService[0];
        system.debug('Service resource vale: '+pruebaService);

        AssignedResource assigResource = new AssignedResource();
        assigResource.ServiceAppointmentId = sapp.Id;
        assigResource.ServiceResourceId = pruebaService[0].Id;
        assigResource.IsPrimaryResource = false;
        insert assigResource;

        crearTratamientosVisitaEquiposYFungibles(sapp.Id, ca.Id, account.Id, 0);

        Test.startTest();
        System.runAs(userProfesional){
            transcursoCita(sapp);
        }
        Test.stopTest();

        OM_AtlasCallout.comprobarStocks(sapp.Id);
        
    }

    @isTest
    public static void comprobarStocksVirtual(){
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);
        
        Account account = [SELECT Id FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'TRD');
        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, null, 'TRD');
        insert tratamiento;
        ca.Tratamiento__c = tratamiento.Id;
        insert ca;

        WorkType worktypeAlta = [SELECT Name, Id, EstimatedDuration FROM WorkType WHERE Name LIKE '%Alta%' LIMIT 1];
        User userProfesional = [SELECT Id FROM User WHERE Email='usertestethprofesional@test.com' LIMIT 1];

        Contenedor__c contenedorServiceTerritory = new Contenedor__c();
        // contenedorServiceTerritory.Usuario_asignado__c = userProfesional.Id;
        contenedorServiceTerritory.Cod_con__c = 'codigoCont';
        insert contenedorServiceTerritory;

        OperatingHours oHours = new OperatingHours();
        oHours.Name = 'HorarioCreta';
        oHours.TimeZone = 'Europe/Madrid';
        insert oHours;

        ServiceTerritory sTerritor = new ServiceTerritory();
        sTerritor.Name = 'TestServTerr';
        sTerritor.RecordTypeId = Schema.SObjectType.ServiceTerritory.getRecordTypeInfosByDeveloperName().get('Creta').getRecordTypeId();
        sTerritor.Street = 'Avinguda de Josep Tarradellas 91';
        sTerritor.City = 'Barcelona';
        sTerritor.State = 'Barcelona';
        sTerritor.PostalCode = '08029';
        sTerritor.Country = 'Spain';
        sTerritor.StateCode = '08';
        sTerritor.CountryCode = 'ES';
        sTerritor.Contenedor__c = contenedorServiceTerritory.Id;
        sTerritor.sto_pro__c = true;
        sTerritor.OperatingHoursId = oHours.Id;
        sTerritor.IsActive = true;

        insert sTerritor;

        ServiceAppointment sapp = new ServiceAppointment(
            DurationType = 'Minutes',
            RecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('Visita_TRD').getRecordTypeId(),
            Status = 'Prevista',
            ParentRecordId = account.Id,
            WorkTypeId = worktypeAlta.Id,
            Duration = worktypeAlta.EstimatedDuration, 
            ActualStartTime = Datetime.now(),
            Fecha_Realizada__c = Datetime.now(),
            Tipo_lugar__c = 'Virtual', 
            ServiceTerritoryId = sTerritor.Id,
            Tratamiento__c = ca.Id,
            Linea_negocio__c = 'TRD'
        );
        insert sapp;

        Asset assetForServiceResource = new Asset();
        assetForServiceResource.AccountId = account.Id;
        assetForServiceResource.IsCompetitorProduct = false;
        assetForServiceResource.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByDeveloperName().get('Box').getRecordTypeId();
        assetForServiceResource.IsInternal = false;
        // assetForServiceResource.AssetLevel = 1;
        assetForServiceResource.Activo__c = true;
        assetForServiceResource.Name = 'TestAsset';
        insert assetForServiceResource;

        List<ServiceResource> pruebaService = [SELECT Id, resourceType FROM ServiceResource WHERE RelatedRecordId =: userProfesional.Id];
        pruebaService[0].resourceType = 'S';
        // pruebaService[0].ServiceAppointment = sapp.Id;
        pruebaService[0].RelatedRecordId = null;
        pruebaService[0].AssetId = assetForServiceResource.Id;

        update pruebaService[0];
        system.debug('Service resource vale: '+pruebaService);

        AssignedResource assigResource = new AssignedResource();
        assigResource.ServiceAppointmentId = sapp.Id;
        assigResource.ServiceResourceId = pruebaService[0].Id;
        assigResource.IsPrimaryResource = false;
        insert assigResource;

        crearTratamientosVisitaEquiposYFungibles(sapp.Id, ca.Id, account.Id, 0);

        Test.startTest();
        System.runAs(userProfesional){
            transcursoCita(sapp);
        }
        Test.stopTest();

        OM_AtlasCallout.comprobarStocks(sapp.Id);
        
    }

    @isTest
    public static void comprobarStocksPresencialResourceT(){
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);
        
        Account account = [SELECT Id FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'TRD');
        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, null, 'TRD');
        insert tratamiento;
        ca.Tratamiento__c = tratamiento.Id;
        insert ca;

        WorkType worktypeAlta = [SELECT Name, Id, EstimatedDuration FROM WorkType WHERE Name LIKE '%Alta%' LIMIT 1];
        User userProfesional = [SELECT Id FROM User WHERE Email='usertestethprofesional@test.com' LIMIT 1];

        
        ServiceAppointment sapp = new ServiceAppointment(
            DurationType = 'Minutes',
            RecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('Visita_TRD').getRecordTypeId(),
            Status = 'Prevista',
            ParentRecordId = account.Id,
            WorkTypeId = worktypeAlta.Id,
            Duration = worktypeAlta.EstimatedDuration, 
            ActualStartTime = Datetime.now(),
            Fecha_Realizada__c = Datetime.now(),
            Tipo_lugar__c = 'Presencial',
            Tratamiento__c = ca.Id,
            Linea_negocio__c = 'TRD'
        );
        insert sapp;

        Contenedor__c contenedorFromUser = new Contenedor__c();
        contenedorFromUSer.Usuario_asignado__c = userProfesional.Id;
        contenedorFromUSer.Cod_con__c = 'codigoCont';
        insert contenedorFromUSer;

        List<ServiceResource> pruebaService = [SELECT Id FROM ServiceResource WHERE RelatedRecordId =: userProfesional.Id];
        system.debug('Service resource vale: '+pruebaService);

        AssignedResource assigResource = new AssignedResource();
        assigResource.ServiceAppointmentId = sapp.Id;
        assigResource.ServiceResourceId = pruebaService[0].Id;
        assigResource.IsPrimaryResource = false;
        insert assigResource;
       

        crearTratamientosVisitaEquiposYFungibles(sapp.Id, ca.Id, account.Id, 0);

        Test.startTest();
        System.runAs(userProfesional){
            transcursoCita(sapp);
        }
        Test.stopTest();

        OM_AtlasCallout.comprobarStocks(sapp.Id);
        
    }


    
    
}