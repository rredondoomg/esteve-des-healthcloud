public with sharing class Escanear_Equipo_Controller {
    public static String ServiceAppointmentId{get;set;}
    public static String VisitaTratamientoId{get;set;}
    public static Boolean HayErrores{get;set;}
    public static String MensajeError{get;set;}
    public static Boolean EstadoInicial{get;set;}
    public static String causaSustitucionValue{get;set;}

    public List<SelectOption> picklistCausaSustitucionValue {get{
		List<SelectOption> opciones = new List<SelectOption>();

        Schema.DescribeFieldResult describeResult = Equipos_sum__c.Causa_Sustitucion__c.getDescribe();
        List<Schema.PicklistEntry> entries = describeResult.getPicklistValues();
        opciones.add(new SelectOption('', 'Seleccione causa'));
        for (Schema.PicklistEntry pEntry : entries) {
            if (pEntry.isActive()) {
                opciones.add(new SelectOption(pEntry.getValue(), pEntry.getLabel()));
            }
        }
        //causaSustitucionValue = entries[0].getValue();
        return opciones;
	}set;}

    
    public  Escanear_Equipo_Controller() {
        EstadoInicial = true;
        HayErrores = false;    
        causaSustitucionValue = picklistCausaSustitucionValue[0].getValue();    
    }

    public static PageReference processButtonClick() {
        EstadoInicial = true;
        HayErrores = false;
        PageReference pageRef = ApexPages.currentPage();

        String sPageURL = pageRef.getUrl();
        String[] sURLVariables = sPageURL.split('IdTratamientoVisita=');
        VisitaTratamientoId = sURLVariables[1].split('&')[0];
        String[] sURLVariables3 = sPageURL.split('serviceAppointment=');
        ServiceAppointmentId = sURLVariables3[1].split('&')[0];

        String codigoAtlas = (String)Cache.Org.getPartition('local.codigoequipo').get(UserInfo.getUserId());
        Boolean equipoPpal = (Boolean)Cache.Org.getPartition('local.codigoequipo').get('Ppal'+UserInfo.getUserId());
        Boolean equipoTel = (Boolean)Cache.Org.getPartition('local.codigoequipo').get('Tel'+UserInfo.getUserId());
        String causaSust = (String)Cache.Org.getPartition('local.codigoequipo').get('causaSust'+UserInfo.getUserId());

        Map<String, Object> respuestaStockEquipo = OM_TranscursoVisitas_Controller.checkStockEquipo(codigoAtlas, ServiceAppointmentId, VisitaTratamientoId);
        
        if(respuestaStockEquipo.containsKey('true')){
            HayErrores = true;
            pageRef.getParameters().put('HayErrores', String.valueOf(HayErrores));
            MensajeError = (String) respuestaStockEquipo.get('true');
            
            pageRef.getParameters().put('MensajeError', String.valueOf(HayErrores));
            EstadoInicial = false;

        }else {
            

            Map<String, String> respuestaInsertNewEquipo = OM_TranscursoVisitas_Controller.insertNewEquipoSuministrado(VisitaTratamientoId, codigoAtlas, 
                                                                                                                        equipoPpal, equipoTel, 
                                                                                                                        (OM_TranscursoVisitas_Controller.StockEquiposResponse)respuestaStockEquipo.get('datos'), 
                                                                                                                        causaSust);
            if(respuestaInsertNewEquipo.containsKey('true')){
                HayErrores = true; pageRef.getParameters().put('HayErrores', String.valueOf(HayErrores));
                MensajeError = (String) respuestaInsertNewEquipo.get('true'); pageRef.getParameters().put('MensajeError', String.valueOf(HayErrores));
            }else{
                HayErrores = false; pageRef.getParameters().put('HayErrores', String.valueOf(HayErrores));
                MensajeError = 'El equipo se ha insertado correctamente. Ya puede cerrar la pantalla.';
                pageRef.getParameters().put('MensajeError', String.valueOf(HayErrores));
            }
            EstadoInicial = false; pageRef.getParameters().put('EstadoInicial', String.valueOf(EstadoInicial));

        }
        
        return null;
    }

    @RemoteAction
    public static void setCodigoEnController(String decodedText) {
        EstadoInicial = false;
        Cache.Org.put('local.codigoequipo.'+UserInfo.getUserId(), decodedText, 3500, Cache.Visibility.ALL, true);
        String codigoAtlas = (String)Cache.Org.getPartition('local.codigoequipo').get(UserInfo.getUserId());
    }

    @RemoteAction
    public static void setEquipoPpalEnController(Boolean checkedValue) {
        Cache.Org.put('local.codigoequipo.'+'Ppal'+UserInfo.getUserId(), checkedValue, 3500, Cache.Visibility.ALL, true);
        Boolean valorPpal = (Boolean)Cache.Org.getPartition('local.codigoequipo').get('Ppal'+UserInfo.getUserId());
    }

    @RemoteAction
    public static void setEquipoTelEnController(Boolean checkedValue) {
        Cache.Org.put('local.codigoequipo.'+'Tel'+UserInfo.getUserId(), checkedValue, 3500, Cache.Visibility.ALL, true);
        Boolean valorTel = (Boolean)Cache.Org.getPartition('local.codigoequipo').get('Tel'+UserInfo.getUserId());
    }

    @RemoteAction
    public static void setCausaSustEnController(String causaSust) {
        Cache.Org.put('local.codigoequipo.'+'causaSust'+UserInfo.getUserId(), causaSust, 3500, Cache.Visibility.ALL, true);
        String valorcausaSust = (String)Cache.Org.getPartition('local.codigoequipo').get('causaSust'+UserInfo.getUserId());
    }
    
}