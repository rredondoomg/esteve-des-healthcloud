/**
 * @File Name          OM_LWC_pack_Test.cls
 * @Description        Test class for covering cases for OM_LWC_pack class
 * @Author             sdlopez@omegacrmconsulting.com
 * @Group              Tests
 * @Last Modified By   dcastellon@omegacrmconsulting.com
 * @Last Modified On   23/12/2019 9:40:45

**/
@isTest 
public class OM_LWC_pack_Test {
    
    @isTest 
    static void testListviewMethodsSOQL() {
        Test.startTest();
        OM_LWC_pack.getValues('RecordType', 'Name', true, 'Id != null');
        OM_LWC_pack.getValues('Account', 'Rating', false, null);
        OM_LWC_pack.getListViewColumns('Account', 'Id, Name, CreatedBy.Name', null, true, null, null);
        OM_LWC_pack.getListViewColumns('Account', 'Id, Name, CreatedBy.Name', null, true, '#, #, Test', null);
        OM_LWC_pack.getRecords('Account', 'Id', 'Id != null', 'Id', 1);
        //OM_LWC_pack.getReportData(idOrDevname, filterIndexes, filterValues);
        OM_LWC_pack.getResults('Account', 'Name', null, 'Id != null', false);
        OM_LWC_pack.getResults('Account', 'Name', '001', null, false);
        OM_LWC_pack.getResults('Account', 'Id,CreatedDate,AccountSource', '001', null, false);
        
        Test.stopTest();
    }
    @isTest 
    static void testListviewMethodsSOSL() {
        Test.startTest();
        OM_LWC_pack.getValues('RecordType', 'Name', true, 'Id != null');
        OM_LWC_pack.getValues('Account', 'Rating', false, null);
        OM_LWC_pack.getListViewColumns('Account', 'Id, Name, CreatedBy.Name', null, true, null, null);
        OM_LWC_pack.getListViewColumns('Account', 'Id, Name, CreatedBy.Name', null, true, '#, #, Test', null);
        OM_LWC_pack.getRecords('Account', 'Id', 'Id != null', 'Id', 1);
        //OM_LWC_pack.getReportData(idOrDevname, filterIndexes, filterValues);
        OM_LWC_pack.getResults('Account', 'Name', null, 'Id != null', true);
        OM_LWC_pack.getResults('Account', 'Name', '001', null, true);
        OM_LWC_pack.getResults('Account', 'Id,Name,CreatedDate,AccountSource,Parent.Name', '001', null, true);
        
        Test.stopTest();
    }

    @isTest 
    static void testDML() {
        Test.startTest();

        Map<String, List<OM_LWC_pack.ResultWrapper>> result1 = OM_LWC_pack.saveRecords(new List<SObject>{new Account(Name = 'Test')}, 'insert', null);
        System.assertEquals(2, result1.keySet().size(), 'Results maps should contains two keys: error and success');
        System.assertEquals(1, result1.get('success').size(), 'One record has been inserted');
        Account insertedAcc = (Account) result1.get('success').get(0).record;
        System.assertNotEquals(null, insertedAcc.Id, 'Id of the inserted Account can not be null');
        
        Map<String, List<OM_LWC_pack.ResultWrapper>> result2 =  OM_LWC_pack.saveRecords(new List<SObject>{ insertedAcc }, 'update', null);
        System.assertEquals(2, result2.keySet().size(), 'Results maps should contains two keys: error and success');

        //Remove the account and try to update again
        String accId = insertedAcc.Id;
        delete insertedAcc;

        Map<String, List<OM_LWC_pack.ResultWrapper>> result3 =  OM_LWC_pack.saveRecords(new List<SObject>{ new Account(Id = accId)}, 'update', null);
        System.assertEquals(2, result3.keySet().size(), 'Results maps should contains two keys: error and success');
        System.assertEquals(1, result3.get('error').size(), 'One record has failed (Invalid id)');

        Test.stopTest();
    }

    @isTest 
    static void testSObjects() {
        Test.startTest();
        OM_LWC_pack.getSObjects();
        OM_LWC_pack.describeToJSON('Account');
        Test.stopTest();
    }

    @isTest 
    static void testReports() {
        Test.startTest();
        OM_LWC_pack.getSObjects();
        OM_LWC_pack.describeToJSON('Account');
        Test.stopTest();
    }
 
}