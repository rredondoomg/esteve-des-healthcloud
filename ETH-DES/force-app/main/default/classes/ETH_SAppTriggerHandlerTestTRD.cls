/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 09-01-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
@isTest
public with sharing class ETH_SAppTriggerHandlerTestTRD {
    @TestSetup
    static void makeData(){
        ETH_DataFactory.setUpOmegaContent();
        //crear un usuario
        User userProfesional = ETH_DataFactory.createUser('usertestethprofesional@test.com');
        insert userProfesional;

        User user = [SELECT Id FROM User WHERE LastName = 'Omega' LIMIT 1];

        System.runAs(user){
            MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
            Test.setMock(HttpCalloutMock.class, multimock);

            Test.startTest();

            Account account = ETH_DataFactory.createAccountWithAddress('Spain', 'Granada', 'Periodista Fernando Gomez de la Cruz', '18014', 'Granada');
            insert account;

            OperatingHours operatingHours = ETH_DataFactory.createOperatingHours();
            insert operatingHours;

            //timeslots de operating hours
            ETH_DataFactory.createTimeSlots(operatingHours.Id, 5, 23);

            ServiceTerritory serviceTerritoryPadre = ETH_DataFactory.createServiceTerritory(null, operatingHours.Id, 'TRD', null);
            insert serviceTerritoryPadre;

            ServiceTerritory serviceTerritoryHijo = ETH_DataFactory.createServiceTerritory(account.ShippingPostalCode, operatingHours.Id, 'TRD', serviceTerritoryPadre.Id);
            insert serviceTerritoryHijo;

            //crear un assigned resource a cada usuario
            ServiceResource serviceResourceProfesional = ETH_DataFactory.createServiceResourceProfesional(userProfesional.Id);
            insert serviceResourceProfesional;

            //crear los services territories member
            ServiceTerritoryMember serviceTerritoryMemberProfesional = ETH_DataFactory.createServiceTerritoryMember('Asistencial', serviceResourceProfesional.Id, serviceTerritoryPadre.Id, operatingHours.Id);
            insert serviceTerritoryMemberProfesional;

            //repetimos proceso para CRETA y box
            ServiceTerritory serviceTerritoryCRETA = ETH_DataFactory.createServiceTerritory(account.ShippingPostalCode, operatingHours.Id, 'Creta',  null);
            insert serviceTerritoryCRETA;

            Asset asset = ETH_DataFactory.createAsset(account.Id);
            insert asset;

            ServiceResource serviceResourceBox = ETH_DataFactory.createServiceResourceBox(asset.Id);
            insert serviceResourceBox;

            ServiceTerritoryMember serviceTerritoryMemberBox = ETH_DataFactory.createServiceTerritoryMember('', serviceResourceBox.Id, serviceTerritoryCRETA.Id, operatingHours.Id);
            insert serviceTerritoryMemberBox;

            //alta
            Worktype worktypeAlta = ETH_DataFactory.createWorktype('Alta', 'TRD', 'Presencial', 'Domicilio');
            insert worktypeAlta;
            worktypeAlta = ETH_DataFactory.createWorktype('Alta', 'TRD', 'Presencial', 'CRETA');
            insert worktypeAlta;
            //domicilio
            Worktype worktype = ETH_DataFactory.createWorktype('Seguimiento', 'TRD', 'Presencial', 'Domicilio');
            insert worktype;
            worktype = ETH_DataFactory.createWorktype('Seguimiento', 'TRD', 'Presencial', 'CRETA');
            insert worktype;
            worktype = ETH_DataFactory.createWorktype('Seguimiento', 'TRD', 'Virtual', 'CRETA');
            insert worktype;
            worktype = ETH_DataFactory.createWorktype('Seguimiento', 'TRD', 'Virtual', 'Domicilio');
            insert worktype;
            

            TokenETH__c oCStoken = new TokenETH__c();
            oCStoken.url__c = 'http://93.90.29.156:9002';
            oCStoken.username__c = 'omega';
            oCStoken.password__c = '44V-7J6RE-oLs&n.b7';
            oCStoken.token__c = '11111111111';
            insert oCStoken;
        }
        Test.stopTest();
    }

    private static void transcursoCita(ServiceAppointment cita, Account account){
        cita.Status = 'Programada';
        cita.SchedStartTime = DateTime.newInstance(Date.today(), Time.newInstance(9,0,0,0));
        update cita;
        cita.Status = 'En Curso';
        cita.ActualStartTime = DateTime.now();
        cita.Fecha_Realizada__c = DateTime.now();
        update cita;
        cita.Status = 'Completada';
        cita.ActualEndTime = DateTime.now();
        cita.Firma_baja__c = true;
        account.Firma_consentimiento__c = true;
        cita.Firma_Visita__c = true;
        update account;
        update cita;
    }

    @isTest
    public static void testCRETATRD(){
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);
        
        Account account = [SELECT Id, Firma_consentimiento__c FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'TRD');
        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, null, 'TRD');
        insert tratamiento;
        ca.Tratamiento__c = tratamiento.Id;
        insert ca;

        WorkType worktype = [SELECT Name, Id, EstimatedDuration FROM WorkType WHERE Name LIKE '%Alta%' LIMIT 1];
        User userProfesional = [SELECT Id FROM User WHERE Email='usertestethprofesional@test.com' LIMIT 1];

        ServiceAppointment sapp = new ServiceAppointment(
            DurationType = 'Minutes',
            RecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('Visita_TRD').getRecordTypeId(),
            Status = 'Prevista',
            ParentRecordId = account.Id,
            WorkTypeId = worktype.Id,
            Duration = worktype.EstimatedDuration,
            Tratamiento__c = ca.Id,
            Linea_negocio__c = 'TRD',
            Lugar_visita__c = 'Domicilio',
            Tipo_lugar__c = 'Presencial'
        );

        insert sapp;

        Test.startTest();
        System.runAs(userProfesional){
            transcursoCita(sapp, account);
        }
        Test.stopTest();

        ServiceAppointment citaSeguimiento = [SELECT Id, OwnerId, ServiceTerritoryId, Paso_Protocolo__c, Status, Fecha_Realizada__c FROM ServiceAppointment WHERE ParentRecordId = :account.Id AND Status != :'Completada' LIMIT 1];
        System.assertNotEquals(citaSeguimiento, null);
    }

    @isTest
    public static void testProfesionalTRD(){
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);
        
        Account account = [SELECT Id, Firma_consentimiento__c FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'TRD');
        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, null, 'TRD');
        //solo domicilio
        tratamiento.LugaresVirtual__c = '';
        insert tratamiento;
        ca.Tratamiento__c = tratamiento.Id;
        insert ca;

        WorkType worktype = [SELECT Name, Id, EstimatedDuration FROM WorkType WHERE Name LIKE '%Alta%' LIMIT 1];
        User userProfesional = [SELECT Id FROM User WHERE Email='usertestethprofesional@test.com' LIMIT 1];

        ServiceAppointment sapp = new ServiceAppointment(
            DurationType = 'Minutes',
            RecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('Visita_TRD').getRecordTypeId(),
            Status = 'Prevista',
            ParentRecordId = account.Id,
            WorkTypeId = worktype.Id,
            Duration = worktype.EstimatedDuration,
            Tratamiento__c = ca.Id,
            Linea_negocio__c = 'TRD',
            Lugar_visita__c = 'Domicilio',
            Tipo_lugar__c = 'Presencial'
        );

        insert sapp;

        Test.startTest();
        System.runAs(userProfesional){
            transcursoCita(sapp, account);
        }
        Test.stopTest();

        ServiceAppointment citaSeguimiento = [SELECT Id, OwnerId, ServiceTerritoryId, Paso_Protocolo__c, Status, Fecha_Realizada__c FROM ServiceAppointment WHERE ParentRecordId = :account.Id AND Status != :'Completada' LIMIT 1];
        System.assertNotEquals(citaSeguimiento, null);
    }

    @isTest
    public static void testQueueContact(){
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);
        
        Account account = [SELECT Id, Firma_consentimiento__c FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'TRD');
        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, null, 'TRD');
        //solo domicilio
        tratamiento.LugaresVirtual__c = '';
        insert tratamiento;
        ca.Tratamiento__c = tratamiento.Id;
        insert ca;

        WorkType worktype = [SELECT Name, Id, EstimatedDuration FROM WorkType WHERE Name LIKE '%Alta%' LIMIT 1];
        User userProfesional = [SELECT Id FROM User WHERE Email='usertestethprofesional@test.com' LIMIT 1];

        ServiceTerritoryMember stm = [SELECT Id FROM ServiceTerritoryMember WHERE Roles__c = 'Asistencial'];
        delete stm;

        ServiceAppointment sapp = new ServiceAppointment(
            DurationType = 'Minutes',
            RecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('Visita_TRD').getRecordTypeId(),
            Status = 'Prevista',
            ParentRecordId = account.Id,
            WorkTypeId = worktype.Id,
            Duration = worktype.EstimatedDuration,
            Tratamiento__c = ca.Id,
            Linea_negocio__c = 'TRD',
            Lugar_visita__c = 'Domicilio',
            Tipo_lugar__c = 'Presencial'
        );
        insert sapp;

        Test.startTest();
        System.runAs(userProfesional){
            transcursoCita(sapp, account);
        }

        Group grupoContact = ETH_ServiceAppointmentTriggerHandler.getQueue('Contact Center');

        ServiceAppointment citaSeguimiento = [SELECT Id, OwnerId, ServiceTerritoryId, Paso_Protocolo__c, Status, Fecha_Realizada__c FROM ServiceAppointment WHERE ParentRecordId = :account.Id AND Status != :'Completada' LIMIT 1];
        System.assertNotEquals(citaSeguimiento, null);
        System.assertEquals(citaSeguimiento.OwnerId, grupoContact.Id);

        ServiceResource serviceResourceProfesional = [SELECT Id FROM ServiceResource WHERE Name = 'Test profesional' LIMIT 1];

        Task nuevaTarea = [SELECT Id, OwnerId, Service_Appointment__c FROM Task WHERE Service_Appointment__c = :citaSeguimiento.Id LIMIT 1];
        System.assertNotEquals(nuevaTarea, null);
        System.assertEquals(nuevaTarea.OwnerId, grupoContact.Id);
    }
}