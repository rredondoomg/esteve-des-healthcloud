/**
 * @description        Base implmentatntion for files in Microsoft Graph (Sharepoint)
 * @author             OmegaCRM 
 * @group              Omega content integration
**/
public virtual without sharing class OM_ContentMicrosoftIntegration implements OM_ContentIntegrationInterface {

    /**
    * @description Transform content wrapper to provider valid JSON
    * @author OmegaCRM  | 01-20-2021 
    * @param content The content to transform
    * @return String The JSON to work with the provider
    **/
    public virtual String transformContent(OM_ContentWrapper content) {
        /*return '{ ' +
           + ' "id" : "' + content.id + '",' +
           + ' "name" : "' + content.name + '",' +
        + '}';*/
        return JSON.serialize(content, true);
    }

    public Blob downloadItem(OM_ContentSetting__c settings, OM_ContentWrapper content) {
        OM_ContentService.CalloutWrapper cout = new OM_ContentService.CalloutWrapper();

        // This endpoint is not working as state in docs - https://docs.microsoft.com/en-us/graph/api/driveitem-get-content?view=graph-rest-1.0&tabs=http
        cout.endpoint = OM_ContentService.getEndpoint(settings, 'get', content).replace('?select', '/content?select');
        //cout.endpoint = OM_ContentService.getEndpoint(settings, 'get', content);
        cout.method = 'GET';
        cout.headers = new Map<String, String> {
            'Content-Type' => 'application/json'
        };

       // OM_ContentWrapper fileResponse = transformContent(makeJSONCallout(cout, settings, content, null));

       // cout.endpoint = fileResponse.downloadUrl;

        return makeBlobCallout(cout, settings, content, null);
    }


    class ListResponse {
        public List<OM_ContentWrapper> value {get; set; }
    }

    

    public List<OM_ContentWrapper> listItems(OM_ContentSetting__c settings, OM_ContentWrapper contentAsFilters) {
        OM_ContentService.CalloutWrapper cout = new OM_ContentService.CalloutWrapper();
        cout.endpoint = OM_ContentService.getEndpoint(settings, 'list');
        cout.method = 'GET';
        cout.headers = new Map<String, String> {
            'Content-Type' => 'application/json',
            'Accept' => '*/*'
        };

        // Add select
        String selectClause = '?$select=' +EncodingUtil.urlEncode('id,size,name,file,description,folder,sharepointids,content.downloadUrl,graph.sourceUrl,createdBy,lastModifiedBy,fileSystemInfo,webUrl,parentReference', 'UTF-8');
        String expandClause = '&expand' + EncodingUtil.urlEncode('listitem($expand=fields),thumbnails', 'UTF-8');

        Boolean hasQuery = false;

        // Add filters
        if(contentAsFilters.parentId != null) {
            cout.endpoint += '/' + contentAsFilters.parentId.replaceAll(' ', '%20');
        } else {
            cout.endpoint += '/root';
             
        }

        if(contentAsFilters.name != null) {
            cout.endpoint += ('/search' + EncodingUtil.urlEncode('(q=\'' + contentAsFilters.name+ '\')', 'UTF-8') + selectClause);
        } else {
            cout.endpoint += '/children' + (selectClause + expandClause);
        }

        String response = makeJSONCallout(cout, settings, contentAsFilters, null);
        ListResponse result = (ListResponse) JSON.deserialize(response, ListResponse.class);
        return result.value;
    }

    /**
    * @description Transform provider valid JSON to Apex wrapper
    * @author OmegaCRM  | 01-20-2021 
    * @param raw The content to transform
    * @return OM_ContentWrapper The wrapper instance
    **/
    public OM_ContentWrapper transformContent(String raw) {
        /*Map<String, Object> rawCnt = (Map<String, Object>) JSON.deserializeUntyped(raw);
        OM_ContentWrapper cnt = new OM_ContentWrapper();
        cnt.id = String.valueOf(rawCnt.get('id'));
        cnt.name = String.valueOf(rawCnt.get('name'));
        return cnt;*/

        System.debug('raw');
        System.debug(raw.contains('downloadUrl'));
        raw = raw.replace('@odata.context', 'odatacontext');
        raw = raw.replace('@microsoft.graph.conflictBehavior', 'conflictBehavior');
        raw = raw.replace('@microsoft.graph.downloadUrl', 'downloadUrl');
        
        return (OM_ContentWrapper) JSON.deserialize(raw, OM_ContentWrapper.class);
    }

    /**
    * @description Gets the metadata info for a file
    * @author OmegaCRM
    * @param settings The settings to use
    * @param content The content wrapper. Should have the id or the name as path in case of Sharepoint
    * @return OM_ContentWrapper The repesentation of the file
    **/
    public virtual OM_ContentWrapper getItem(OM_ContentSetting__c settings, OM_ContentWrapper content) {

        OM_ContentService.CalloutWrapper cout = new OM_ContentService.CalloutWrapper();
        cout.endpoint = OM_ContentService.getEndpoint(settings, 'get', content);
        cout.method = 'GET';
        cout.headers = new Map<String, String> {
            'Content-Type' => 'application/json'
        };

        //cout.body = transformContent(content);
        
        OM_ContentWrapper driveResponse = transformContent(makeJSONCallout(cout, settings, content, null));
    
        return driveResponse;
    }

    /**
    * @description Updates an file or a folder
    * @author OmegaCRM  | 01-19-2021 
    * @param settings The settings to use
    * @param content The content wrapper. Should have the metadata to be updated
    * @return OM_ContentWrapper The wrapper of the response
    **/
    public virtual OM_ContentWrapper updateItem(OM_ContentSetting__c settings, OM_ContentWrapper content) {

        OM_ContentService.CalloutWrapper cout = new OM_ContentService.CalloutWrapper();
        cout.endpoint = OM_ContentService.getEndpoint(settings, 'update', content);
        cout.method = 'PATCH';
        cout.headers = new Map<String, String> {
            'Content-Type' => 'application/json'
        };
        Map<String, String> customProperties = content.appProperties;
        //Remove not-writable fields
        content.id = null;
        content.parents = null;
        content.appProperties = null;
        content.sharepointIds = null;
        content.conflictBehavior = null;
        content.size = null;
        content.downloadUrl = null;
        cout.body = transformContent(content);
        
        OM_ContentWrapper driveResponse = transformContent(makeJSONCallout(cout, settings, content, null));

        if(customProperties != null && customProperties.size() > 0) {
            // Now, update ListItem to append custom properties
            String listurl = OM_ContentService.getEndpoint(settings, 'base', driveResponse) + '/lists/' + driveResponse.sharepointIds.get('listId') + '/items/' + driveResponse.sharepointIds.get('listItemUniqueId');
            System.debug(LoggingLevel.ERROR, 'List item endp ' + listurl);
    
            OM_ContentService.CalloutWrapper coutList = new OM_ContentService.CalloutWrapper();
            coutList.endpoint = listurl;
            coutList.method = 'PATCH';
            coutList.headers = new Map<String, String> {
                'Content-Type' => 'application/json'
            };
            coutList.body = '{ "fields" : '+ JSON.serialize(customProperties) + ' }';
    
    
            makeJSONCallout(coutList, settings, content, null);
        }
        
        return driveResponse;
    }

    /**
    * @description Uploads a new item
    * @author OmegaCRM  | 01-19-2021 
    * @param settings The settings to use
    * @param content The content wrapper. Should have the metadata to be updated
    * @param fileData Binary content for upload
    * @return OM_ContentWrapper The wrapper of the response
    **/
    public virtual OM_ContentWrapper uploadItem(OM_ContentSetting__c settings, OM_ContentWrapper content, Blob fileData) {
        OM_ContentService.CalloutWrapper cout = new OM_ContentService.CalloutWrapper();
        cout.endpoint = OM_ContentService.getEndpoint(settings, 'uploadNew', content);
        cout.method = 'PUT';
        cout.headers = new Map<String, String> {
            'Content-Type' => content.mimeType
        };
        cout.bodyBlob = fileData;
        return transformContent(makeJSONCallout(cout, settings, content, null));
    }

    /**
    * @description Create a new folder
    * @author OmegaCRM  | 01-19-2021 
    * @param settings The settings to use
    * @param content The content wrapper. To create folder should have 'id'
    * @return OM_ContentWrapper The wrapper of the response. Delete action does not return anything, so the same content input will be returned
    **/
    public virtual OM_ContentWrapper deleteItem(OM_ContentSetting__c settings, OM_ContentWrapper content) {
        OM_ContentService.CalloutWrapper cout = new OM_ContentService.CalloutWrapper();
        cout.endpoint = OM_ContentService.getEndpoint(settings, 'update', content);
        cout.method = 'DELETE';
        cout.headers = new Map<String, String> {
            'Content-Type' => 'application/json'
        };
        makeJSONCallout(cout, settings, content, null);
        return content;
    }

    /**
    * @description Create a new folder
    * @author OmegaCRM  | 01-19-2021 
    * @param settings The settings to use
    * @param content The content wrapper. To create folder should have 'id' and 'parentId' properties informed
    * @return OM_ContentWrapper The wrapper of the response
    **/
    public virtual List<OM_ContentWrapper> createFolderPath(OM_ContentSetting__c settings, OM_ContentWrapper content) {
        List<OM_ContentWrapper> contentsResult = new List<OM_ContentWrapper>();
        List<String> foldersPath = content.name.split('/');
        String parendId = 'root';
        for (String folder : foldersPath) {
            content.name = folder;
            OM_ContentWrapper contentResponse = createFolder(settings,content);
            content.parentId = contentResponse.Id;
            contentsResult.add(contentResponse);
        }
        return contentsResult;
    }

    /**
    * @description Create a new folder
    * @author OmegaCRM  | 01-19-2021 
    * @param settings The settings to use
    * @param content The content wrapper. To create folder should have 'id' and 'parentId' properties informed
    * @return OM_ContentWrapper The wrapper of the response
    **/
    public virtual OM_ContentWrapper createFolder(OM_ContentSetting__c settings, OM_ContentWrapper content) {
        String conflictBehavior = content.conflictBehavior != null ? content.conflictBehavior : 'rename';
        OM_ContentService.CalloutWrapper cout = new OM_ContentService.CalloutWrapper();
        cout.endpoint = OM_ContentService.getEndpoint(settings, 'newFolder', content);
        cout.method = 'POST';
        cout.headers = new Map<String, String> {
            'Content-Type' => 'application/json'
        };
        cout.body = '{'+
            + '"name": "' + content.name + '",'+
            + '"folder": {}, '+
            + '"@microsoft.graph.conflictBehavior": "'+conflictBehavior+'"'+
            + ' }';
        return transformContent(makeJSONCallout(cout, settings, content, null));
    }
    
    /**
    * @description Get new access token
    * @author OmegaCRM  | 01-19-2021 
    * @param settings The settings to use
    * @param authCode The authorization code from callbakc page
    * @return String 
    **/
    public virtual String getToken(OM_ContentSetting__c settings, String authCode) {

        Http h = new Http();
        HttpRequest request = new HttpRequest();
        request.setMethod('POST');
        request.setEndpoint(OM_ContentService.getEndpoint(settings, 'token'));
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');


        String payLoad = (settings.OM_Is_personal_account__c == false ? 'tenant=' + EncodingUtil.urlEncode(settings.OM_Tenant__c,'UTF-8') : '' ) 
            + '&code=' + EncodingUtil.urlEncode(authCode,'UTF-8') 
            + '&client_id=' + EncodingUtil.urlEncode(settings.OM_client_id__c,'UTF-8') 
            + '&client_secret=' + EncodingUtil.urlEncode(settings.OM_client_secret__c,'UTF-8') 
            + '&redirect_uri=' + EncodingUtil.urlEncode(settings.OM_redirect_uri__c,'UTF-8') 
            + '&grant_type=' + EncodingUtil.urlEncode('authorization_code', 'UTF-8') 
            + '&scope=' + EncodingUtil.urlEncode(settings.OM_scope__c, 'UTF-8');
        request.setBody(payload);
        HttpResponse res = h.send(request);

        return res.getBody();
    }

    /**
    * @description Get new accces token using a refresh token
    * @author OmegaCRM  | 01-19-2021 
    * @param settings The settings to use
    * @param userId The user id to fetch "refresh_token" data
    * @return String 
    **/
    public virtual String refreshToken(OM_ContentSetting__c settings, String userId) {
        // TODO David - Change to new service. Change also below field names
        Map<String, String> currentAccess = OM_ContentService.getUserAccess(settings.OM_Name__c, userid);
        Http h = new Http();
        HttpRequest request = new HttpRequest();
        request.setMethod('POST');
        request.setEndpoint(OM_ContentService.getEndpoint(settings, 'token'));
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        String payLoad = (settings.OM_Is_personal_account__c == false ? 'tenant=' + EncodingUtil.urlEncode(settings.OM_Tenant__c,'UTF-8') : '' )
            + '&refresh_token=' + EncodingUtil.urlEncode(currentAccess.get('refresh_token'),'UTF-8') 
            + '&client_id=' + EncodingUtil.urlEncode(settings.OM_client_id__c,'UTF-8') 
            + '&client_secret=' + EncodingUtil.urlEncode(settings.OM_client_secret__c,'UTF-8') 
            + '&redirect_uri=' + EncodingUtil.urlEncode(settings.OM_redirect_uri__c,'UTF-8') 
            + '&grant_type=' + EncodingUtil.urlEncode('refresh_token', 'UTF-8') 
            + '&scope=' + EncodingUtil.urlEncode(settings.OM_scope__c, 'UTF-8');
        request.setBody(payload);
        HttpResponse res = h.send(request);

        return res.getBody();
    }

    /**
    * @description Get the authorization endpoint to begin Oatuh flow
    * @author OmegaCRM  | 01-19-2021 
    * @param settings The settings to use
    * @param userid The user id to append to "state" parameter
    * @return String 
    **/
    public virtual String getAuthorizationEndpoint(OM_ContentSetting__c settings, String userid) {

        String state = userid + '-' + settings.OM_Name__c;
        if(settings.OM_Auth_type__c == 'Shared') {
            state = OM_ContentService.SHARED + '-' + settings.OM_Name__c;
        }

        if (settings.OM_Is_personal_account__c == false) { 
            return 'https://login.microsoftonline.com/' 
                + settings.OM_Tenant__c + '/oauth2/v2.0/authorize'+
                + '?client_id=' + settings.OM_client_id__c +
                + '&response_type=code&response_mode=query' +
                + '&redirect_uri=' + settings.OM_Redirect_uri__c +
                + '&scope='+ settings.OM_scope__c + 
                + '&state=' + state;
        } else {
            return 'https://login.live.com/oauth20_authorize.srf' +
                + '?client_id=' + settings.OM_client_id__c +
                + '&response_type=code&response_mode=query' +
                + '&redirect_uri=' + settings.OM_Redirect_uri__c +
                + '&scope='+ settings.OM_scope__c + 
                + '&state=' + state;
        }
    }

    /**
    * @description Makes the callouts allowing retries on 401 codes
    * @author OmegaCRM  | 01-19-2021 
    * @param calloutw The callout wrapper with service variables
    * @param settings The settings to use
    * @param content The user id to fetch "refresh_token" data
    * @param newAccess The new access in case of a retry
    * @return String 
    **/
    public virtual Object makeCallout(OM_ContentService.CalloutWrapper calloutw, OM_ContentSetting__c settings, OM_ContentWrapper content, Map<String, String> newAccess) {
        System.debug(LoggingLevel.ERROR, 'Calling out  Microsoft ' + calloutw.endpoint + '  ');
        Object toreturn = '';
        Map<String,String> access = newAccess != null ? newAccess : OM_ContentService.getUserAccess(settings.OM_Name__c, UserInfo.getUserId());

        Http h = new Http();
        HttpRequest request = calloutw.makeRequest(access.get('access_token'));
        HttpResponse res = h.send(request);

        System.debug('Response code ' + res.getStatusCode());

        // Inspect response. In case of 401, refresh token and retry
        if (res.getStatusCode() == 401) {
            System.debug(LoggingLevel.ERROR, 'Token has expired, refreshing');
            String newacc = refreshToken(settings, UserInfo.getUserId());
            Map<String, String> newaccmap = (Map<String, String>) JSON.deserialize(newacc, Map<String, String>.class);

            Eventbus.publish(new OM_Content_event__e(OM_Settings_name__c = settings.OM_Name__c,  OM_Action__c = 'refreshToken', OM_UserId__c = UserInfo.getUserId(), OM_JSON_data__c = newacc));
            toreturn = makeCallout(calloutw, settings, content, newaccmap);
            // OM_ContentService.updateUserAccessFuture(settings.OM_Name__c, UserInfo.getUserId(), newacc);
            // OM_ContentService.updateUserAccess(settings, UserInfo.getUserId(), newacc);
        }

        else if (res.getStatusCode() >= 200 && res.getStatusCode() < 300) {
            
            if(calloutw.responseBlob) {
                toreturn = res.getBodyAsBlob();
            } else {
                toreturn = res.getBody();
            }
        }

        // Follow redirects
        else if (res.getStatusCode() == 302) {
            String location = res.getHeader('Location');
            calloutw.endpoint = location;
            request = calloutw.makeRequest(access.get('access_token'));
            res = h.send(request);
            if(calloutw.responseBlob) {
                toreturn = res.getBodyAsBlob();
            } else {
                toreturn = res.getBody();
            }
        }
        else if (res.getStatusCode() >= 400) {
            Map<String, Object> errorRes = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
            Map<String, Object> errorData = (Map<String, Object>) errorRes.get('error');
            System.debug(LoggingLevel.ERROR, 'Response error');
            System.debug(LoggingLevel.ERROR, res.getBody());
            throw new OM_ContentException('Exception occurred when calling Micrsoft API: Code ' + res.getStatusCode() + ' Message: ' + String.valueOf(errorData.get('message')));
        }
        return toreturn;
    }

    private String makeJSONCallout(OM_ContentService.CalloutWrapper calloutw, OM_ContentSetting__c settings, OM_ContentWrapper content, Map<String, String> newAccess) {
        return String.valueOf(makeCallout(calloutw, settings, content, newAccess));
    }

    private Blob makeBlobCallout(OM_ContentService.CalloutWrapper calloutw, OM_ContentSetting__c settings, OM_ContentWrapper content, Map<String, String> newAccess) {
        calloutw.responseBlob = true;
        return (Blob) makeCallout(calloutw, settings, content, newAccess);
    }

    public class SharepointDrivesReponse {
        public List<SharepointDrive> value;
    }

    public class SharepointDrive {
        public SharepointList list_x;
        public String name;
        public String id;
    }

    public class SharepointList {
        public List<Column> columns;
    }

    public class Column{
        public String columnGroup;
        public String description;
        public String displayName;
        public boolean enforceUniqueValues;
        public boolean hidden;
        public String id;
        public boolean indexed;
        public String name;
        public boolean readOnly;
        public boolean required;
    }
    
}