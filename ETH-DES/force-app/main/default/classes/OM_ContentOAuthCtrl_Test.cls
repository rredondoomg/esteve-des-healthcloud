@IsTest
public inherited sharing class OM_ContentOAuthCtrl_Test {
    public static final String SETTINGS_NAME = 'testSettings';
    public static final String USERNAME = 'testcontentUser@content.omegacrconsulting.com';


    @TestSetup
    static void makeData(){
        // Insert user with corect permission set for testing (any profile)
        Profile pf = [SELECT Id FROM Profile WHERE UserLicense.Name = 'Salesforce' LIMIT 1];
        User u = new User(
            Username = USERNAME,
            FirstName = 'Test',
            LastName = 'Omega',
            ProfileId = pf.Id,
            Email = USERNAME,
            Alias = 'test',
            TimeZoneSidKey = 'GMT',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'ISO-8859-1',
            LanguageLocaleKey = 'en_US'
        );

        insert u;

        PermissionSet contUser = [SELECT Id FROM PermissionSet WHERE Name = 'OM_ContentUser' LIMIT 1];
        PermissionSetAssignment psass = new PermissionSetAssignment(
            AssigneeId = u.Id,
            PermissionSetId = contUser.Id
        );

        insert psass;


        System.runAs(u){
            // Insert some settings
            OM_ContentSetting__c settings = new OM_ContentSetting__c(
                OM_Name__c = SETTINGS_NAME,
                OM_Allow_folder_creation__c = true,
                OM_Allow_navigation__c = true,
                OM_Allow_upload_files__c = true,
                OM_Allow_edit__c = true,
                OM_Auth_type__c = 'Shared',
                OM_Drive_id__c = 'test',
                OM_Use_personal_drive__c = false,
                OM_Client_id__c = 'test',
                OM_Client_secret__c = 'test',
                OM_Discovery_docs__c = 'test',
                OM_Provider__c = 'MicrosoftSharePoint',
                OM_Site_id__c = 'test',
                OM_Tenant__c = 'test',
                OM_Scope__c = 'test',
                OM_Redirect_uri__c = 'https://testorg.visualforce.com/apex/OM_OAuthCallback',
                OM_Columns__c = 'name;size'
            );
            insert settings;
        }
    }

    @IsTest
    static void testParameterCombinations(){
        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];

        Test.setMock(HttpCalloutMock.class, new OM_ContentMicrosoftIntegrationMock());
        Test.startTest();
        System.runAs(contextuser){
            PageReference pageRef = Page.OM_OAuthCallback;
            Test.setCurrentPage(pageRef);
            OM_ContentOAuthCtrl ctrl = new OM_ContentOAuthCtrl();
            
            // Malformed state
            ApexPages.currentPage().getParameters().put('state', 'badstate');
            ctrl.initAuth();
            System.assertEquals(label.omOauthBadParameters, ctrl.errorTitle, 'Error title should be the expected label');
            System.assertEquals(label.omOauthBadState, ctrl.errorMsg, 'Error message should be the expected label');
            
            // Not malformed state but missing code
            ApexPages.currentPage().getParameters().put('state', 'badstate-badstate');
            ctrl.initAuth();
            System.assertEquals(label.omOauthBadParameters, ctrl.errorTitle, 'Error title should be the expected label');
            System.assertEquals(label.omOauthCodeMissing, ctrl.errorMsg, 'Error message should be the expected label');

            // Valid code but invalid state
            ApexPages.currentPage().getParameters().put('state', 'badstate-badstate');
            ApexPages.currentPage().getParameters().put('code', 'validcode');
            ctrl.initAuth();
            System.assertEquals(label.omOauthErrorProvider, ctrl.errorTitle, 'Error title should be the expected label');

             // Valid code and state
             ApexPages.currentPage().getParameters().put('state', OM_ContentService.SHARED + '-' + SETTINGS_NAME);
             ApexPages.currentPage().getParameters().put('code', 'validcode');
             ctrl.initAuth();
             System.assertEquals(Label.omOauthSuccess, ctrl.successMsg, 'Success title should be the expected label');
             System.assertEquals(null, ctrl.errorTitle, 'Error title should be null');

        }
        Test.stopTest();
    }
}