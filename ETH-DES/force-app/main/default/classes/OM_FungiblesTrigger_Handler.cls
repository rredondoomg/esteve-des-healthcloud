/**
 * @description       : 
 * @author            : sdiaz@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 09-08-2022
 * @last modified by  : sdiaz@omegacrmconsulting.com
**/
public with sharing class OM_FungiblesTrigger_Handler {


    public static void enviarFungibleCorreosExpressHermes(List<Fungibles_sum__c> newList, Map<Id, Fungibles_sum__c> oldMap){
        system.debug('-- Método: enviarFungibleCorreosExpressHermes --');

            Set<Id> fungiblesSettoGetData = new Set<Id>();
            for (Fungibles_sum__c fungibleAux : newList) {
                if(fungibleAux.Pendiente__c == false && oldMap.get(fungibleAux.Id).Pendiente__c == true){
                    fungiblesSettoGetData.add(fungibleAux.Id);
                }
            }

            if(!fungiblesSettoGetData.isEmpty()){
                system.debug('Set: '+fungiblesSettoGetData);
            
                Map<Id, Fungibles_sum__c> idToFungibleMap = new Map<Id, Fungibles_sum__c>([SELECT Id, Visita_tratamiento__r.Service_Appointment__r.Tipo_lugar__c FROM Fungibles_sum__c WHERE Id IN: fungiblesSettoGetData]);
                system.debug('Mapa: '+idToFungibleMap);
                for (Fungibles_sum__c fungibleAux : newList) {
                    system.debug('fungibleAux.Id: '+fungibleAux.Id);

                    system.debug('idToFungibleMap.get(fungibleAux.Id).Visita_tratamiento__r.Service_Appointment__r.Tipo_lugar__c.equalsIgnoreCase(Virtual) '+idToFungibleMap.get(fungibleAux.Id).Visita_tratamiento__r.Service_Appointment__r.Tipo_lugar__c.equalsIgnoreCase('Virtual'));
                    system.debug('oldMap.get(fungibleAux.Id).Pendiente__c == true '+oldMap.get(fungibleAux.Id).Pendiente__c);
                    
                    if(fungibleAux.Pendiente__c == false && 
                        idToFungibleMap.get(fungibleAux.Id).Visita_tratamiento__r.Service_Appointment__r.Tipo_lugar__c.equalsIgnoreCase('Virtual') &&
                        oldMap.get(fungibleAux.Id).Pendiente__c == true){
                            callCorreosExpressHermes(fungibleAux.Id);
                    }
                }
            }
            
    }

    @future(callout=true)
    public static void callCorreosExpressHermes(String fungibleAuxId){
        system.debug('Llamaria a Correos EXPRESS');


        // Fungibles_sum__c fungible = (Fungibles_sum__c) JSON.deserialize(fungibleAux,  Fungibles_sum__c.class);

        Map<Id, Fungibles_sum__c> datosFungible = new Map<Id, Fungibles_sum__c>([SELECT Id, Name, Paciente__r.Name, Paciente__r.NIF__c, 
                                                                                    Paciente__r.ShippingStreet, Paciente__r.ShippingCity, 
                                                                                    Paciente__r.ShippingPostalCode, Paciente__r.Phone, 
                                                                                    Paciente__r.PersonEmail, Paciente__r.PersonMobilePhone
                                                                                    FROM Fungibles_sum__c 
                                                                                    WHERE Id =: fungibleAuxId]);   

        Boolean isSandBox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandBox;

        String entorno = isSandBox ? 'Test_Hermes' : 'Pro_Hermes';
        
        Configuracion_Hermes__mdt configuracionHermes  = [SELECT Id, DeveloperName, URL__c, Username__c, Password__c, Method__c,
                            solicitante__c, codRte__c, nomRte__c, dirRte__c, pobRte__c, codPosNacRte__c, 
                            paisISORte__c, telefRte__c, numBultos__c, kilos__c, producto__c, portes__c, entrSabado__c 
                            FROM Configuracion_Hermes__mdt 
                            WHERE DeveloperName =: entorno limit 1];

        String sUrl = configuracionHermes.URL__c;
        String sUsername = configuracionHermes.Username__c ;
        String sPassword = configuracionHermes.Password__c ;
        String sMethod = configuracionHermes.Method__c ;

        Map<String,Object> mapRes = new Map<String,Object>();
        HttpRequest req = new HttpRequest();
        // system.debug('URL TOKEN: '+sUrl + '/login');
        req.setEndpoint(sUrl);
        req.setMethod(sMethod);
        req.setHeader('Content-Type','application/json');

        // Map<String, String> body = new Map<String, String>();
        // body.put('username', sUsername);
        // body.put('password', sPassword);

        // String authValue = accountId + ':' + authToken;
        Blob headerValue = Blob.valueOf(sUsername +':'+sPassword);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization',authorizationHeader);

        WrapperHermesCorreosExpress bodyToSend = new WrapperHermesCorreosExpress();

        bodyToSend.solicitante = configuracionHermes.solicitante__c;
        // bodyToSend.numEnvio;
        bodyToSend.ref = datosFungible.get(fungibleAuxId).Name;
        // bodyToSend.refCliente;
        Datetime dt = datetime.now();

        // String formattedDate = dt.format('dd/MM/yyyy');
        String day = dt.day() < 10 ? '0' + String.valueOf(dt.day()) : String.valueOf(dt.day());
        String month = dt.month() < 10 ? '0' + String.valueOf(dt.month()) : String.valueOf(dt.month());
        String year = String.valueOf(dt.year());

        bodyToSend.fecha = day+month+year;
        bodyToSend.codRte =  configuracionHermes.codRte__c;
        bodyToSend.nomRte = configuracionHermes.nomRte__c;
        // bodyToSend.nifRte;
        bodyToSend.dirRte = configuracionHermes.dirRte__c;
        bodyToSend.pobRte = configuracionHermes.pobRte__c;
        bodyToSend.codPosNacRte = configuracionHermes.codPosNacRte__c;
        bodyToSend.paisISORte = configuracionHermes.paisISORte__c;
        // bodyToSend.codPosIntRte;
        // bodyToSend.contacRte;
        bodyToSend.telefRte = configuracionHermes.telefRte__c;
        // bodyToSend.emailRte;
        // bodyToSend.codDest;
        bodyToSend.nomDest = datosFungible.get(fungibleAuxId).Paciente__r.Name != null ? datosFungible.get(fungibleAuxId).Paciente__r.Name : '';
        bodyToSend.nifDest = datosFungible.get(fungibleAuxId).Paciente__r.NIF__c != null ? datosFungible.get(fungibleAuxId).Paciente__r.NIF__c : '';
        bodyToSend.dirDest = datosFungible.get(fungibleAuxId).Paciente__r.ShippingStreet != null ? datosFungible.get(fungibleAuxId).Paciente__r.ShippingStreet : '';
        bodyToSend.pobDest = datosFungible.get(fungibleAuxId).Paciente__r.ShippingCity != null ? datosFungible.get(fungibleAuxId).Paciente__r.ShippingCity : '';
        bodyToSend.codPosNacDest = datosFungible.get(fungibleAuxId).Paciente__r.ShippingPostalCode != null ? datosFungible.get(fungibleAuxId).Paciente__r.ShippingPostalCode : '';
        bodyToSend.paisISODest = 'ES';
        // bodyToSend.codPosIntDest;
        bodyToSend.contacDest = datosFungible.get(fungibleAuxId).Paciente__r.Name;
        bodyToSend.telefDest = datosFungible.get(fungibleAuxId).Paciente__r.PersonMobilePhone != null ? datosFungible.get(fungibleAuxId).Paciente__r.PersonMobilePhone : datosFungible.get(fungibleAuxId).Paciente__r.Phone != null ? datosFungible.get(fungibleAuxId).Paciente__r.Phone : ' ';
        bodyToSend.emailDest = datosFungible.get(fungibleAuxId).Paciente__r.PersonEmail != null ? datosFungible.get(fungibleAuxId).Paciente__r.PersonEmail : '';
        // bodyToSend.contacOtrs;
        // bodyToSend.telefOtrs;
        // bodyToSend.emailOtrs;
        // bodyToSend.observac;
        bodyToSend.numBultos = configuracionHermes.numBultos__c;
        bodyToSend.kilos = configuracionHermes.kilos__c;
        // bodyToSend.volumen;
        // bodyToSend.alto;
        // bodyToSend.largo;
        // bodyToSend.ancho;
        bodyToSend.producto = configuracionHermes.producto__c;
        bodyToSend.portes = configuracionHermes.portes__c;
        // bodyToSend.reembolso;
        bodyToSend.entrSabado = configuracionHermes.entrSabado__c;
        // bodyToSend.seguro;
        // bodyToSend.numEnvioVuelta;
        bodyToSend.codDirecDestino = '';
        bodyToSend.password = '';


        // public List<ListaBultos> listaBultos;

        List<listaInformacionAdicional> listInfoAdicional = new List<ListaInformacionAdicional>();
        listaInformacionAdicional infoAdicional = new listaInformacionAdicional();
        infoAdicional.tipoEtiqueta='1';
        infoAdicional.posicionEtiqueta='N'; 
        infoAdicional.horaDesdeRecogida= '08:00';
        infoAdicional.horaHastaRecogida= '17:00';
        infoAdicional.codificacionUnicaB64= '1';
        infoAdicional.creaRecogida='S';

        // Fecha recogida, cogemos la fecha actual y le sumamos 2 días;
        Datetime fechaRecogidaTemp = datetime.now().adddays(2);

        String dayfechaRecogida = fechaRecogidaTemp.day() < 10 ? '0' + String.valueOf(fechaRecogidaTemp.day()) : String.valueOf(fechaRecogidaTemp.day());
        String monthfechaRecogida = fechaRecogidaTemp.month() < 10 ? '0' + String.valueOf(fechaRecogidaTemp.month()) : String.valueOf(fechaRecogidaTemp.month());
        String yearfechaRecogida = String.valueOf(fechaRecogidaTemp.year());

        infoAdicional.fechaRecogida=dayfechaRecogida+monthfechaRecogida+yearfechaRecogida;
        listInfoAdicional.add(infoAdicional);
        bodyToSend.listaInformacionAdicional= listInfoAdicional;

        List<ListaBultos> listaBultos = new List<ListaBultos>();
        ListaBultos bulto = new ListaBultos();
        bulto.orden = '01';
        bulto.codUnico = '';
        bulto.orden = '1';
        bulto.codBultoCli = '';
        bulto.descripcion = '';
        bulto.observaciones = '';
        bulto.kilos = '';
        bulto.volumen = '';
        bulto.alto = '';
        bulto.largo = '';
        bulto.ancho = '';

        listaBultos.add(bulto);
        bodyToSend.listaBultos = listaBultos;


        // List<ListaInformacionAdicional> listaInfoAdicional = new List<ListaInformacionAdicional>();
        // listaInfoAdicional.add(new ListaInformacionAdicional(creaRecogida = 'S', ));
        // bodyToSend.listaInformacionAdicional = listaInfoAdicional;

        system.debug('Body a enviar: '+JSON.Serialize(bodyToSend));
        req.setBody(JSON.Serialize(bodyToSend));

        Http http = new Http();
        HTTPResponse res = new HTTPResponse();
        if(!Test.isRunningTest()){
            res = http.send(req);
        } 
        system.debug('Codigo respuesta: '+res.getStatusCode());
        system.debug('Body respuesta: '+res.getBody());

        //Codigo != 200
        if(res.getStatusCode() != 200){
            update new Fungibles_Sum__c(Id = fungibleAuxId, Error_Integracion_Hermes__c = res.getBody(), Pendiente__c = true);
        }else{ //Código 200
            HermesResponse respuestaHermes = (HermesResponse) System.JSON.deserialize(res.getBody(), HermesResponse.class);
            if(respuestaHermes.codigoRetorno == 0){
                update new Fungibles_Sum__c(Id = fungibleAuxId, Error_Integracion_Hermes__c = '', Codigo_Etiqueta__c=respuestaHermes.etiqueta[0].etiqueta1);
            }else{
                update new Fungibles_Sum__c(Id = fungibleAuxId, Error_Integracion_Hermes__c = respuestaHermes.mensajeRetorno, Pendiente__c = true);   
            }           
        }

    }


    public static void enviarFungibleAtlas(List<Fungibles_sum__c> newList, Map<Id, Fungibles_sum__c> oldMap){
        List<Fungibles_sum__c> fungiblesToSendAtlasList = new List<Fungibles_sum__c>();
        Set<Id> fungiblesEnvioSet = new Set<Id>();
        
        for(Fungibles_sum__c fungAux : newList){
            if(!fungAux.Pendiente__c && oldMap.get(fungAux.Id).Pendiente__c){
                fungiblesToSendAtlasList.add(fungAux);
                fungiblesEnvioSet.add(fungAux.Id);
            }
        }

        if(!fungiblesToSendAtlasList.isEmpty()){
            Map<Id, Fungibles_sum__c> idToFungibleDataMap = new Map<Id, Fungibles_sum__c>([SELECT Id, 
                                                                Marca_modelo_fungible__r.ProductCode,
                                                                Visita_Tratamiento__r.Paciente__r.Idanonimo__c,
                                                                Contenedor__r.Cod_con__c, LastModifiedBy.Name,
                                                                Cantidad__c,
                                                                Visita_Tratamiento__r.Service_Appointment__r.Fecha_Realizada__c, 
                                                                Visita_Tratamiento__r.Service_Appointment__r.Tipo_lugar__c,
                                                                LastModifiedBy.Codigo__c,
                                                                Visita_Tratamiento__r.Service_Appointment__c
                                                                FROM Fungibles_sum__c 
                                                                WHERE Id IN : fungiblesEnvioSet]);

            getTokenFungibles(JSON.serialize(idToFungibleDataMap));
        }
        

        

    }




    @future(callout=true)
    public static void getTokenFungibles(String idToFungibleDataMap){

        Map<Id, Fungibles_sum__c> idToFungibleDataMapDeserialize = (Map<Id, Fungibles_sum__c>) JSON.deserialize(idToFungibleDataMap,  Map<Id, Fungibles_sum__c>.class);

        TokenETH__c oCStoken = [SELECT Id, url__c, username__c, password__c, token__c FROM TokenETH__c];

        String sUrl = oCStoken.url__c;
        String sUsername = oCStoken.username__c;
        String sPassword = oCStoken.password__c;
        String sToken = oCStoken.token__c;
        String sMethod = 'POST';


        Map<String,Object> mapRes = new Map<String,Object>();

        HttpRequest req = new HttpRequest();
        // system.debug('URL TOKEN: '+sUrl + '/login');
        req.setEndpoint(sUrl + '/login');
        req.setMethod(sMethod);
        req.setHeader('Content-Type','application/json');

        Map<String, String> mapAuthen = new Map<String, String>();
        mapAuthen.put('username', sUsername);
        mapAuthen.put('password', sPassword);

        // System.debug('## serialize: ' + JSON.Serialize(mapAuthen));

        //serializa la llamda a formato json
        req.setBody(JSON.Serialize(mapAuthen));

        Http http = new Http();
        HTTPResponse res = new HTTPResponse();

        res = http.send(req);


        if (res.getStatusCode() == 200){
            mapRes = (Map<String,Object>)JSON.deserializeUntyped(res.getBody() );
            formacionDatosFungiblesWS(oCStoken, String.valueOf(mapRes.get('accessToken')), idToFungibleDataMapDeserialize);
        }else{
            system.debug('REsponde '+res.getStatusCode());
        } 

    }


    public static void formacionDatosFungiblesWS(TokenETH__c oCStoken, String tokenObtenido, Map<Id, Fungibles_sum__c> idToFungibleDataMapDeserialize){
        // system.debug('Estoy en formacion Datos fungibles WS');


        List<Fungibles> fungiblesToCallout = new List<Fungibles>();
        for (Fungibles_sum__c fungAux : idToFungibleDataMapDeserialize.values()) {
            Fungibles fungibleAux = new Fungibles();
            fungibleAux.fgvs_id = Integer.valueOf(fungAux.Marca_modelo_fungible__r.ProductCode);
            fungibleAux.fgvs_paciente = fungAux.Visita_Tratamiento__r.Paciente__r.Idanonimo__c;
            fungibleAux.fgvs_contenedor = fungAux.Visita_Tratamiento__r.Service_Appointment__r.Tipo_lugar__c.equalsIgnoreCase('Virtual') ? null : fungAux.Contenedor__r.Cod_con__c;
            fungibleAux.fgvs_usuario = fungAux.LastModifiedBy.Codigo__c;
            if(fungAux.Visita_Tratamiento__c != null && fungAux.Visita_Tratamiento__r.Service_Appointment__c != null && fungAux.Visita_Tratamiento__r.Service_Appointment__r.Fecha_Realizada__c != null){
                fungibleAux.fgvs_fec_vis = fungAux.Visita_Tratamiento__r.Service_Appointment__r.Fecha_Realizada__c.addHours(2);
            }
            fungibleAux.id_salesforce = fungAux.Id;
            fungibleAux.fgvs_cantidad = Integer.valueOf(fungAux.Cantidad__c);
            fungibleAux.fgvs_tip_vis = fungAux.Visita_Tratamiento__r.Service_Appointment__r.Tipo_lugar__c.equalsIgnoreCase('Virtual') ? 'V' : 'P';
            fungiblesToCallout.add(fungibleAux);
        }

        MainWrapperFungibles mWfungibles = new MainWrapperFungibles();
        mWfungibles.fungibles = fungiblesToCallout;
        String sMethod = 'POST';

        String sUrl = oCStoken.url__c;

        HttpRequest req = new HttpRequest();
        HTTPResponse res = new HTTPResponse();

        req.setEndpoint(sUrl + '/fungibles-suministrados');
        // req.setEndpoint('https://testvanguardsergio.free.beeceptor.com');

        req.setMethod(sMethod);
        req.setHeader('Content-Type','application/json');
        req.setHeader('Authorization', 'Bearer ' + tokenObtenido);

        req.setBody(JSON.serialize(mWfungibles));

        // system.debug('Body que envio a Atlas: '+JSON.serialize(mWfungibles));

        Http http = new Http();
        res = http.send(req);

        if (res.getStatusCode() == 200){
            // system.debug('REsponde '+ res.getStatusCode() +' /equipos-fungibles');           
        }else{
            // system.debug('REsponde '+ res.getStatusCode() +' /equipos-fungibles');           

            List<Fungibles_sum__c> fungiblesToUpdateList = new List<Fungibles_sum__c>();
            List<Log_error__c> errorsToInsertList = new List<Log_error__c>();
            for (Fungibles_sum__c fungAux : idToFungibleDataMapDeserialize.values()) {
                errorsToInsertList.add(new Log_error__c(
                    Id_de_registro__c = fungAux.Id,
                    Endpoint__c = sUrl + '/fungibles-suministrados',
                    Tipo_de_registro__c = 'Fungibles_sum__c',
                    Body__c = JSON.serialize(mWfungibles),
                    Error__c = res.getBody()
                ));

                fungiblesToUpdateList.add(new Fungibles_sum__c(Id = fungAux.Id, Error_Envio_Atlas__c = true));
            }

            insert errorsToInsertList;
            update fungiblesToUpdateList;
        }        

    }

    public class WrapperResponse{
        public Boolean success;
        public Integer code;
        public String message;
    }

    public class MainWrapperFungibles{
        public List<fungibles> fungibles;
    }

    public class Fungibles {
        public Integer fgvs_id;
        public String fgvs_paciente;
        public String fgvs_contenedor;
        public String fgvs_usuario;
        public DateTime fgvs_fec_vis;
        public String id_salesforce;
        public Integer fgvs_cantidad;
        public String fgvs_tip_vis;
    }



    public class WrapperHermesCorreosExpress{

        public String solicitante;
        // public String numEnvio;
        public String ref;
        // public String refCliente;
        public String fecha;
        public String codRte;
        public String nomRte;
        // public String nifRte;
        public String dirRte;
        public String pobRte;
        public String codPosNacRte;
        public String paisISORte;
        // public String codPosIntRte;
        // public String contacRte;
        public String telefRte;
        // public String emailRte;
        // public String codDest;
        public String nomDest;
        public String nifDest;
        public String dirDest;
        public String pobDest;
        public String codPosNacDest;
        public String paisISODest;
        // public String codPosIntDest;
        public String contacDest;
        public String telefDest;
        public String emailDest;
        // public String contacOtrs;
        // public String telefOtrs;
        // public String emailOtrs;
        // public String observac;
        public String numBultos;
        public String kilos;
        // public String volumen;
        // public String alto;
        // public String largo;
        // public String ancho;
        public String producto;
        public String portes;
        // public String reembolso;
        public String entrSabado;
        // public String seguro;
        // public String numEnvioVuelta;
        public List<ListaBultos> listaBultos;
        public String codDirecDestino;
        public String password;
        public List<ListaInformacionAdicional> listaInformacionAdicional;
    
    }

    public class ListaInformacionAdicional {
        public String tipoEtiqueta;
        public String etiquetaPDF;
        public String posicionEtiqueta;
        public String hideSender;
        public String logoCliente;
        public String codificacionUnicaB64;
        public String textoRemiAlternativo;
        public String idioma;
        public String creaRecogida;
        public String fechaRecogida;
        public String horaDesdeRecogida;
        public String horaHastaRecogida;
        public String referenciaRecogida;
    }

    

    public class ListaBultos {
        public String codUnico;
        public String orden;
        public String codBultoCli;
        public String descripcion;
        public String observaciones;
        public String kilos;
        public String volumen;
        public String alto;
        public String largo;
        public String ancho;
    }

    public class ListaEtiquetas{
        public String etiqueta1;
    }

    public class HermesResponse{
        public Integer codigoRetorno;
        public String mensajeRetorno;
        public String datosResultado;
        public List<ListaBultos> listaBultos;
        public List<ListaEtiquetas> etiqueta;
        public String numRecogida;
        public String fechaRecogida;
        public String horaRecogidaDesde;
        public String horaRecogidaHasta;
        public String direccionRecogida;
        public String poblacionRecogida;

    }
}