/**
 * @description       Test class for OM_ContentFlowUtils
 * @author            OmegaCRM 
 * @group             Content test
**/
@isTest
public inherited sharing class OM_ContentFlowService_Test {
    public static final String SETTINGS_NAME = 'testSettings';
    public static final String USERNAME = 'testcontentUser@content.omegacrconsulting.com';


    @TestSetup
    static void makeData(){
        // Insert user with corect permission set for testing (any profile)
        Profile pf = [SELECT Id FROM Profile WHERE UserLicense.Name = 'Salesforce' LIMIT 1];
        User u = new User(
            Username = USERNAME,
            FirstName = 'Test',
            LastName = 'Omega',
            ProfileId = pf.Id,
            Email = USERNAME,
            Alias = 'test',
            TimeZoneSidKey = 'GMT',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'ISO-8859-1',
            LanguageLocaleKey = 'en_US'
        );

        insert u;

        PermissionSet contUser = [SELECT Id FROM PermissionSet WHERE Name = 'OM_ContentUser' LIMIT 1];
        PermissionSetAssignment psass = new PermissionSetAssignment(
            AssigneeId = u.Id,
            PermissionSetId = contUser.Id
        );

        insert psass;


        System.runAs(u){
            // Insert some settings
            OM_ContentSetting__c settings = new OM_ContentSetting__c(
                OM_Name__c = SETTINGS_NAME,
                OM_Allow_folder_creation__c = true,
                OM_Allow_navigation__c = true,
                OM_Allow_upload_files__c = true,
                OM_Allow_edit__c = true,
                OM_Auth_type__c = 'Shared',
                OM_Drive_id__c = 'test',
                OM_Use_personal_drive__c = false,
                OM_Client_id__c = 'test',
                OM_Client_secret__c = 'test',
                OM_Discovery_docs__c = 'test',
                OM_Provider__c = 'GoogleDrive',
                OM_Site_id__c = 'test',
                OM_Tenant__c = 'test',
                OM_Scope__c = 'test',
                OM_Redirect_uri__c = 'https://testorg.visualforce.com/apex/OM_OAuthCallback',
                OM_Columns__c = 'name;size'
            );
            insert settings;

            OM_ContentService.updateUserAccess(settings, UserInfo.getUserId(), '{"access_token": "test_token", "refresh_token": "test_refresh"}');
        }

    }

    @IsTest
    static void testFlowAction(){
        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        Test.setMock(HttpCalloutMock.class, new OM_ContentGoogleIntegrationMock());
        Test.startTest();
        System.runAs(contextuser){
            OM_ContentFlowService.OM_ContentFlowInput input = new OM_ContentFlowService.OM_ContentFlowInput();
            input.settingsName = SETTINGS_NAME;
            input.actionName = OM_ContentService.ACTION_CREATE_FOLDER;
            input.contentId = '';
            input.contentName = 'testFolder';
            input.contentDescription = 'test folder description';
            input.contentParentId = 'root';

            List<OM_ContentFlowService.OM_ContentFlowOutput> listResults = OM_ContentFlowService.runIntegration(new List<OM_ContentFlowService.OM_ContentFlowInput> { input });
            System.assertEquals(1, listResults.size(), 'Flow should obtain list of created folders');
        }
        Test.stopTest();
    }
}