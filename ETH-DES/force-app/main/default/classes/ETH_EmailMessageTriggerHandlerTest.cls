/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 10-07-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
@isTest
public with sharing class ETH_EmailMessageTriggerHandlerTest {
    @TestSetup
    static void makeData(){
        ETH_DataFactory.setUpOmegaContent();
    }

    @isTest
    public static void testEmailToCase(){
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);

        Account account = ETH_DataFactory.createAccountWithAddress('Spain', 'Granada', 'Periodista Fernando Gomez de la Cruz', '18014', 'Granada');
        insert account;

        OperatingHours operatingHours = ETH_DataFactory.createOperatingHours();
        insert operatingHours;

        ServiceTerritory serviceTerritoryPadre = ETH_DataFactory.createServiceTerritory(null, operatingHours.Id, 'Pharmate', null);
        insert serviceTerritoryPadre;

        ServiceTerritory serviceTerritoryHijo = ETH_DataFactory.createServiceTerritory(account.ShippingPostalCode, operatingHours.Id, 'Pharmate', serviceTerritoryPadre.Id);
        insert serviceTerritoryHijo;
    
        User user = ETH_DataFactory.createUser('usertesteth@test.com');
        insert user;

        ServiceResource serviceResource = ETH_DataFactory.createServiceResourceProfesional(user.Id);
        insert serviceResource;

        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'Service');

        protocolo__c protocolo = ETH_DataFactory.createProtocolo();
        insert protocolo;

        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, protocolo.Id, 'Pharmate');
        insert tratamiento;

        Paso_Protocolo__c paso = ETH_DataFactory.createPasoProtocolo(protocolo.Id, 1, 'Presencial', 'Profesional Responsable (Pharmate)', 'Inicio', 'Domicilio', 7);
        insert paso;

        Worktype worktype = ETH_DataFactory.createWorktype('Alta', 'Pharmate', paso.tipo_Lugar__c, paso.lugar_visita__c);
        insert worktype;

        ca.Tratamiento__c = tratamiento.Id;

        ca.Subject = 'Test';
        ca.Description = 'Test';
        ca.Type = '';
        ca.Subtipo__c = '';
        EmailMessage emailMessage;

        Test.startTest();
        System.runAs(user){
            insert ca;
            emailMessage = ETH_DataFactory.createEmailMessage(ca);
            insert emailMessage;
        }
        Test.stopTest();

        ca = [SELECT Id, Type, Priority, Subtipo__c FROM Case WHERE Id =:ca.Id LIMIT 1];
        System.assertEquals(ca.Type, 'Colas');
        System.assertEquals(ca.Subtipo__c, 'Archivado');
        System.assertEquals(ca.Priority, 'Baja');
    }
}