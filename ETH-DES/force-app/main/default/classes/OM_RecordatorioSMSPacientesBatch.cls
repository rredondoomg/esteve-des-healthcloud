/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 09-05-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
public without sharing class OM_RecordatorioSMSPacientesBatch implements Database.Batchable<sObject>, Database.AllowsCallouts{
    
    
    List<SMS_Setting__mdt> smsSettings = [SELECT Id, Endpoint__c, Mensaje__c, MasterLabel, User__c, Password__c, 
                                          Dias_Para_Recordatorio__c, Active_SMS__c, TRD__c, Pharmate__c,Oxigenoterapia__c, Cabecera_Pharmate__c, Cabecera_TRD__c, CabeceraOxigenoterapia__c
                                          FROM SMS_Setting__mdt WHERE Tipo_de_mensaje__c ='Recordatorio'];
    
    public static Id saTRD = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByName().get('Visita TRD').getRecordTypeId();
    public static Id saPharmate = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByName().get('Visita Pharmate').getRecordTypeId();

    Map<Id, ServiceAppointment> saByAccountIds = new Map<Id,ServiceAppointment>(); 
    String query = 'SELECT Id,OwnerId,Owner.Name, AccountId, SchedStartTime, Account.Name, Account.PersonMobilePhone, Lugar_visita__c, Status, Tipo_lugar__c, RecordtypeId, SMS_Recordatorio_Enviado__c, Linea_negocio__c  FROM ServiceAppointment WHERE Status = \'Programada\'';
    public OM_RecordatorioSMSPacientesBatch() {
			
    }

    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<ServiceAppointment> scope){
        List<ServiceAppointment> saToUpdate = new List<ServiceAppointment>();
        Map<Id, ServiceAppointment> saByAccountId = new Map<Id, ServiceAppointment>();
        for(ServiceAppointment sa : scope){
            String mdtKey = 'R-'+sa.Status+'-'+sa.Tipo_lugar__c+'-'+sa.Lugar_visita__c;
            SMS_Setting__mdt smsSetting = getSMSConfiguration(smsSettings, mdtKey);
            Integer diasParaAviso = Integer.valueOf(smsSetting.Dias_Para_Recordatorio__c) == null ? 0 : Integer.valueOf(smsSetting.Dias_Para_Recordatorio__c);

            Date fechaConsulta = Date.newInstance(sa.SchedStartTime.year(), sa.SchedStartTime.month(), sa.SchedStartTime.day());
            Integer avisoViernes = diasParaAviso+3;
            Date todayDate = System.today();
            if(todayDate.daysBetween(fechaConsulta) == diasParaAviso || (todayDate.daysBetween(fechaConsulta) == avisoViernes && sa.SchedStartTime.format('EEEE') == 'Monday') || test.isRunningTest()){
                saByAccountId.put(sa.AccountId, sa);
            }
        }
        Map<Account, ServiceAppointment> saByAccount = new Map<Account, ServiceAppointment>();
        if(!saByAccountId.isEmpty()){
            List<Account> accounts = [SELECT Id, PersonMobilePhone, Name FROM Account WHERE Id IN :saByAccountId.keySet()];
            if(!accounts.isEmpty()){
               for(Account acc : accounts){
                    saByAccount.put(acc, saByAccountId.get(acc.Id));
                }
                Map<String, Map<String,ServiceAppointment>> bodysByEndpoint = generateHttpBodys(saByAccount);
                for(String key :bodysByEndpoint.keyset()){
                    Map<String,ServiceAppointment> bodysBySa = bodysByEndpoint.get(key);
                    for(String body : bodysBySa.keySet()){
                        HttpResponse response = connectWithWS(body, key);
                        if(response.getStatusCode() == 200){
                            ServiceAppointment successSa =new ServiceAppointment();
                    		successSa.Id = bodysBySa.get(body).Id;
                            successSa.SMS_Recordatorio_Enviado__c = true;
                            saToUpdate.add(successSa);
                            
                        }
                    }
                }
            }
        }
        
        if(!saToUpdate.isEmpty()){
            update saToUpdate;
        }
    }

    public void finish(Database.BatchableContext BC){

    }

    public HttpResponse connectWithWS(String body, String endpoint){

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(ENDPOINT);
        request.setHeader('Accept','application/json');
        request.setHeader('Content-Type','application/x-www-form-urlencoded');
        request.setBody(body);
        request.setMethod('POST');
        HttpResponse response = http.send(request);
        return response;
    }

    public Map<String, Map<String,ServiceAppointment>> generateHttpBodys (Map<Account, ServiceAppointment> accountsToNotify){
        Map<String,Map<String,ServiceAppointment>> bodysByEndpoints = new Map<String,Map<String,ServiceAppointment>>();
        Map<String,ServiceAppointment> bodysBySa = new Map<String,ServiceAppointment>();
        Integer limitCount = 0;
        String endpoint;
        String cabecera;
        for(Account key : accountsToNotify.keySet()){
            String mdtKey = 'R-'+accountsToNotify.get(key).Status+'-'+accountsToNotify.get(key).Tipo_lugar__c+'-'+accountsToNotify.get(key).Lugar_visita__c;
            SMS_Setting__mdt smsSetting = getSMSConfiguration(smsSettings,mdtKey);
            if(accountsToNotify.get(key).RecordTypeId == saPharmate){
                cabecera = smsSetting.Cabecera_Pharmate__c;             
            }else if(accountsToNotify.get(key).RecordTypeId == saTRD){
                if(accountsToNotify.get(key).Linea_negocio__c == 'Oxigenoterapia'){ cabecera = smsSetting.CabeceraOxigenoterapia__c;
                }else{ cabecera = smsSetting.Cabecera_TRD__c;}
            }else{ cabecera = '';}
            if(smsSetting.Active_SMS__c || test.isRunningTest()){ 
                endpoint = smsSetting.Endpoint__c;
                OM_SMSIntegration.SMSWrapper wrapperBody = new OM_SMSIntegration.SMSWrapper();
                OM_SMSIntegration.Sms sms = new OM_SMSIntegration.Sms();
                sms.user = smsSetting.User__c;
                sms.password = smsSetting.Password__c;
                sms.src = cabecera;
                OM_SMSIntegration.Dst dst= new OM_SMSIntegration.Dst();
                dst.Num = new List<String>{key.PersonMobilePhone};
                sms.Dst = dst;
                sms.txt = generateMessageText(accountsToNotify.get(key), smsSetting.Mensaje__c, smsSetting.TRD__c, smsSetting.Pharmate__c, smsSetting.Oxigenoterapia__c);
                wrapperBody.Sms = sms;
                if(key.PersonMobilePhone !=null){ bodysBySa.put(JSON.serialize(wrapperBody), accountsToNotify.get(key));}
            }
        }
        
        bodysByEndpoints.put(endpoint, bodysBySa);

        return bodysByEndpoints;
    }
    
    private static String generateMessageText(ServiceAppointment sa, String message, String trd, String pharmate, String Oxigenoterapia){
        String messageupdated = message;
        Date saDate = sa.SchedStartTime.date();
        String dateStr = DateTime.newInstance(saDate.year(),saDate.month(),saDate.day()).format('d-MM-YYYY');
        String hours = String.valueOf(sa.SchedStartTime.hour());
        String minutes = sa.SchedStartTime.minute() == 0 ? '00' : String.valueOf(sa.SchedStartTime.minute());
		String timeStr = hours+':'+minutes;
        
        if(messageUpdated.contains('Vdate')){
            messageUpdated = messageUpdated.replace('Vdate', dateStr);
        }
        if(messageUpdated.contains('Vtime')){
            messageUpdated = messageUpdated.replace('Vtime', timeStr);
        }
        if(messageUpdated.contains('Vdoc')){
            messageUpdated = messageUpdated.replace('Vdoc', sa.Owner.Name);
        }
        if(messageUpdated.contains('Vdir')){
            //Revisar el service territory del creta rafa 
            ServiceAppointment serviceAppointmentCreta = [select id, WorkType_Text__c, ServiceTerritory.Street, ServiceTerritory.City from ServiceAppointment where Id =: sa.Id  limit 1];
            messageUpdated = messageUpdated.replace('Vdir', String.valueOf(serviceAppointmentCreta.ServiceTerritory.street) + ' '+ String.valueOf(serviceAppointmentCreta.ServiceTerritory.City) );
        }
        if(messageUpdated.contains('Vad')){
            if(sa.RecordTypeId == saPharmate){
				messageUpdated = messageUpdated.replace('Vad', pharmate);               
            }else if(sa.RecordTypeId == saTRD){
                if(sa.Linea_negocio__c == 'Oxigenoterapia'){ messageUpdated =  messageUpdated.replace('Vad', Oxigenoterapia);
                }else{ messageUpdated =  messageUpdated.replace('Vad', trd);}
            }
        }
        
        return messageUpdated;
    }

    private static SMS_Setting__mdt getSMSConfiguration(List<SMS_Setting__mdt> settings, String name){
        SMS_Setting__mdt settingToReturn = new SMS_Setting__mdt();

        for(SMS_Setting__mdt setting: settings){
            System.debug('setting.MasterLabel'+setting.MasterLabel);
            System.debug('name'+name);
            
            settingToReturn = name == setting.MasterLabel ? setting : settingToReturn; 
        }
        return settingToReturn;
    }
}