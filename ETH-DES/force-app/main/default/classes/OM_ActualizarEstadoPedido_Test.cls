@isTest
public with sharing class OM_ActualizarEstadoPedido_Test {
    @TestSetup
    static void makeData(){
        ETH_DataFactory.setUpOmegaContent();
        //crear un usuario
        User userProfesional = ETH_DataFactory.createUser('usertestethprofesional@test.com');
        
        insert userProfesional;
        User emailFromuser = [SELECT Id, codigo__c FROM User WHERE Email = 'usertestethprofesional@test.com'];
        system.debug(' user:: '+emailFromuser);
        User user = [SELECT Id FROM User WHERE LastName = 'Omega' LIMIT 1];

        System.runAs(user){
            MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
            Test.setMock(HttpCalloutMock.class, multimock);

            Test.startTest();

            Account account = ETH_DataFactory.createAccountWithAddress('Spain', 'Granada', 'Periodista Fernando Gomez de la Cruz', '18014', 'Granada');
            insert account;

            Contenedor__c contenedor = new Contenedor__c();
            contenedor.Cod_con__c = 'codigoCont';
            insert contenedor;

            Cliente__c clienteTest = new Cliente__c();
            clienteTest.Cod_cliente__c = '7';
            clienteTest.CIF__c = '54567654L';
            clienteTest.Email__c = 'ClienteTest@test.com';
            clienteTest.IdentificacionPac__c = 'CIF';
            clienteTest.Name = 'Cliente Test';
            insert clienteTest;

            Lote__c lote = new Lote__c();
            lote.cliente__c = clienteTest.Id;
            lote.Cod_lot__c = 'CodLo';
            insert lote;

            Pedido__c pedidoTest = new Pedido__c();
            pedidoTest.Contenedor__c = contenedor.Id;
            pedidoTest.Cliente__c = clienteTest.Id;
            pedidoTest.Observaciones__c = '';
            pedidoTest.Lote__c = lote.Id;
            insert pedidoTest;

            TokenETH__c oCStoken = new TokenETH__c();
            oCStoken.url__c = 'http://93.90.29.156:9002';
            oCStoken.username__c = 'omega';
            oCStoken.password__c = '44V-7J6RE-oLs&n.b7';
            oCStoken.token__c = '11111111111';
            insert oCStoken;

            // Product2 equipoTest = [SELECT Id, Name FROM Product2 WHERE Name = 'Equipo TEST'];

            Product2 equipoTest = new Product2();
            equipoTest.Name = 'Equipo Test';
            equipoTest.ProductCode = '765';
            equipoTest.Marca__C = 'EquipoTest';
            equipoTest.Modelo__c = 'EquipoTest';
            equipoTest.Identificador__c = 'EquipoTest123';
            equipoTest.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Aparatos').getRecordTypeId();
            insert equipoTest;

            Pedido_det__c lineaPedido = new Pedido_det__c();
            lineaPedido.producto__c = equipoTest.Id;
            lineaPedido.Cantidad__c = 1;
            lineaPedido.Pedido__c = pedidoTest.Id;
            insert lineaPedido;


            Product2 fungibleTest = new Product2();
            fungibleTest.Name = 'Equipo Test';
            fungibleTest.ProductCode = '754';
            fungibleTest.Marca__C = 'fungibleTest';
            fungibleTest.Modelo__c = 'fungibleTest';
            fungibleTest.Identificador__c = 'fungibleTest123';
            fungibleTest.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Fungibles').getRecordTypeId();
            insert fungibleTest;

            Pedido_det__c lineaPedido2 = new Pedido_det__c();
            lineaPedido2.producto__c = fungibleTest.Id;
            lineaPedido2.Cantidad__c = 1;
            lineaPedido2.Pedido__c = pedidoTest.Id;
            insert lineaPedido2;

        }
        Test.stopTest();
    }

    @isTest
    public static void actualizarEstadoPedido(){
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);
        
        List<Pedido__c> pedidoToUpdateEstado = [SELECT Id FROM Pedido__c LIMIT 1];        

        Map<Boolean, String> response = OM_ActualizarEstadoPedido.actualizarEstadoPedido(pedidoToUpdateEstado[0].Id);
        
        delete pedidoToUpdateEstado;
    }
}