public with sharing class OM_Lookup {

    public class SObjectWrapper {
        @AuraEnabled public String label;
        @AuraEnabled public String value;
        @AuraEnabled public String tabInfo;
        public SObjectWrapper(String lab, String val, Schema.DescribeTabResult tabInfo) {
            this.label = lab;
            this.value = val;
            this.tabInfo = JSON.serialize(tabInfo);
            //this.tabInfo = tabInfo.getIcons()[0].getUrl();
        }
    }

    @AuraEnabled
    public static List<SObjectWrapper> getSalesforceObjects() {

        Map<String, Schema.DescribeTabResult> tabsMap = new Map<String, Schema.DescribeTabResult >();
        for(Schema.DescribeTabSetResult tabSet : Schema.describeTabs()) {
            for(Schema.DescribeTabResult tab : tabSet.getTabs()) {
                tabsMap.put(tab.getSobjectName().toLowerCase(), tab);
            }
        }

        List<SObjectWrapper> objs = new List<SObjectWrapper>();
        Map<String, Schema.sObjectType> gd = Schema.getGlobalDescribe();
        for(Schema.sObjectType obj : gd.values()) {
            DescribeSObjectResult obDesc = obj.getDescribe();
            if(isValidSObject(obDesc)) {
                objs.add(new SObjectWrapper(obDesc.getLabel(), obDesc.getName(), tabsMap.get(obDesc.getName().toLowerCase())));
            }
        }
        return objs;
    }

    public static Boolean isValidSObject(DescribeSObjectResult obDesc) { 
        return !obDesc.isCustomSetting() 
            && !obDesc.isDeprecatedAndHidden()
            && !obDesc.getName().endsWith('Share')
            && !obDesc.getName().endsWith('__mdt')
            && !obDesc.getName().endsWith('__e')
            && (obDesc.isSearchable() || obDesc.isQueryable())
            && (obDesc.isAccessible() || obDesc.isCreateable() || obDesc.isUpdateable());
    }

    /**
    * @description 
    * @author dcastellon@omegacrmconsulting.com | 28/10/2019 
    * @param objectApiName 
    * @param fields 
    * @param value 
    * @param filter 
    * @return List<SObject> 
    **/
    @AuraEnabled
    public static List<SObject> getResults(String objectApiName, String fields, String value, String filter, Boolean sosl, String recordsNumber) {
        return sosl==true ? getResultsSOSL(objectApiName, fields, value, filter, recordsNumber) : getResultsSOQL(objectApiName, fields, value, filter, recordsNumber);
    }

    private static List<SObject> getResultsSOQL(String objectApiName, String fields, String value, String filter, String recordsNumber) {
        
        String query = 'SELECT ' + fields + ' FROM ' + objectApiName;
        Map<String, schema.Sobjectfield> fieldmap = OM_SchemaUtils.getFieldsSObjectMap(objectApiName);

        if(String.isNotBlank(value)){
            List<String> fieldList = fields != null ?
                fields.deleteWhitespace() != '' ? fields.deleteWhitespace().split(',') : new List<String>()
                : new List<String>();
                
            if(!fieldList.isEmpty()){
                query += ' WHERE (';
                for(String fld: fieldList){
                    Schema.DescribeFieldResult fdes = fieldmap.containsKey(fld) ? fieldmap.get(fld).getDescribe() : null;
                    if(fdes != null && fdes.isFilterable()) {
                        Schema.DisplayType ftype = fdes.getType();
                        if(ftype == Schema.DisplayType.STRING) {
                            query += fld + ' LIKE \'%' + value + '%\' OR ';
                        //Dont search for certains types
                        } else if(ftype == Schema.DisplayType.ID
                                || ftype == Schema.DisplayType.REFERENCE
                                || ftype == Schema.DisplayType.BOOLEAN
                                || ftype == Schema.DisplayType.DATE
                                || ftype == Schema.DisplayType.DATETIME
                                || ftype == Schema.DisplayType.ADDRESS
                                || ftype == Schema.DisplayType.INTEGER
                                || ftype == Schema.DisplayType.DOUBLE
                                || ftype == Schema.DisplayType.CURRENCY
                            ) {
                            query += '';
                        }  else if(ftype == Schema.DisplayType.MULTIPICKLIST) {
                            query += fld + ' INCLUDES (\'' + value + '\') OR ';
                        } else  {
                            query += fld + ' = \'' + value + '\' OR ';
                        }
                    } 
                }
                query = query.removeEnd(' OR ') + ')';

                //440 - PS - Si no encuentra ningún campo filtrable, no filtra
                query = query.removeEnd(' WHERE ()');

            } else { 
                return new List<SObject>();
            }
        }

        if(String.isNotBlank(filter)){
            query += (String.isNotBlank(value) ? ' AND ' : ' WHERE ') + filter;
        }

        query += ' LIMIT ' + (String.isNotBlank(recordsNumber) ? recordsNumber : '4');
        
        try {
            return Database.query(query);
        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage() + ' query ' + query);
        }

    }

    private static List<SObject> getResultsSOSL(String objectApiName, String fields,  String value, String filter, String recordsNumber) {
        if(String.isBlank(value) || value.trim().length()<2){
            return null;
        }

        List<String> fieldList = fields != null ?
                fields.deleteWhitespace() != '' ? fields.deleteWhitespace().split(',') : new List<String>()
                : new List<String>();

        String whereQuery = ''; //pasar por parámetro
        //String SOSL_search_string = 'FIND \'zar\' IN NAME FIELDS RETURNING Maestros__c(Id, Name WHERE RecordType.DeveloperName = \'poblaciones\') LIMIT 4';
        String searchQuery = 'FIND ';
        searchQuery += '\'' + value + '\'';
        searchQuery += ' IN ALL FIELDS RETURNING ';//se puede mejorar y aceptar variable aquí para personalizarlo
        searchQuery += objectApiName; 
        searchQuery += '(Id,' + (String.join(fieldList, ','));
        if(String.isNotBlank(whereQuery)){
            searchQuery += whereQuery;
        }
        searchQuery += ')';
        searchQuery += ' LIMIT ' + (String.isNotBlank(recordsNumber) ? recordsNumber : '4');


        
        try {
            List<List <sObject>> results = search.query(searchQuery);
            return results[0];
        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage() + ' query ' + searchQuery);
        }

    }
}