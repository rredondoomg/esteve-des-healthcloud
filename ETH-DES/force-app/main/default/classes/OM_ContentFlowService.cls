/**
 * @description       Class for use integration services from Flows
 * @author            OmegaCRM 
 * @group             Content flow utilities
**/
public inherited sharing class OM_ContentFlowService {
    /**
     * @description Inner class to model Flow input parameters
     */
    public class OM_ContentFlowInput {
        @InvocableVariable(label='Settings name') public String settingsName;
        @InvocableVariable(label='Action name') public String actionName;
        @InvocableVariable(label='Content ID') public String contentId;
        @InvocableVariable(label='Content name') public String contentName;
        @InvocableVariable(label='Content description') public String contentDescription;
        @InvocableVariable(label='Content parent id') public String contentParentId;
    }

    /**
     * @description Inner class to model Flow input parameters
     */
    public class OM_ContentFlowOutput {
        @InvocableVariable(label='Content Id') public String Id;
        @InvocableVariable(label='Content name') public String Name;
        @InvocableVariable(label='Content ID') public String Description;
    }

    public static List<OM_ContentFlowOutput> transformWrappersToOutput(List<OM_ContentWrapper> wrappers) {
        List<OM_ContentFlowOutput> toret = new List<OM_ContentFlowOutput>();
        for(OM_ContentWrapper wrap : wrappers) {
            OM_ContentFlowOutput output = new OM_ContentFlowOutput();
            output.Id = wrap.id;
            output.Name = wrap.name;
            output.Description = wrap.description;
            toret.add(output);
        }
        return toret;
    }

    /**
    * @description Method to call integration actions from a flow as a Apex action
    * @author OmegaCRM  | 02-19-2021 
    * @param inputs The list of inputs from the flow
    * @return List<OM_ContentWrapper> 
    **/
    @InvocableMethod(label='Call content action')
    public static List<OM_ContentFlowOutput> runIntegration(List<OM_ContentFlowInput> inputs) {
        List<OM_ContentWrapper> toreturn = new List<OM_ContentWrapper>();
        for(OM_ContentFlowInput input : inputs) {
            OM_ContentWrapper cnt = new OM_ContentWrapper();
            cnt.id = input.contentId;
            cnt.name = input.contentName;
            cnt.parentId = input.contentParentId;
            cnt.description = input.contentDescription;
            toreturn.add(OM_ContentService.runIntegrationAction(input.actionName, input.settingsName, cnt, null));
        }

        List<OM_ContentFlowOutput> outputFlows = transformWrappersToOutput(toreturn);
        return outputFlows;
    }
}