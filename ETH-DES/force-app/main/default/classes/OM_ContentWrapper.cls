/**
 * @description       Class for wraping provider files and be able to pass them back and forth throught APEX and LWC
 * @author            OmegaCRM 
 * @group             Omega content integration
**/
public with sharing class OM_ContentWrapper {
    /**
     * @description The drive/library Id. Only for Micrososft. Indicate here an id work with the item in a Drive (Document Library) different than the default one
     */

    @AuraEnabled
    public String driveId {get; set;}

    @AuraEnabled
    public String kind {get; set;}

    /**
     * @description The id of the file or folder
     */
    @AuraEnabled
    public String id { get; set; }

    

    /**
     * @description The name of the file or folder
     */
    @AuraEnabled
    public String name {get; set;}

    /**
     * @description The mimeType of the file or folder
     */
    @AuraEnabled
    public String mimeType {
        get {
            if(file != null && file.mimeType != null) return file.mimeType;
            return mimeType;
        } 
        set;
    }

    /**
     * @description The type of the resource
     */
    @AuraEnabled
    public String resourceType {get; set;}

    /**
     * @description The created date in  of the file or folder
     */
    @AuraEnabled
    public String createdDate {get; set;}
    
    /**
     * @description The description of the file (only Google Drive at the moment)
     */
    @AuraEnabled
    public String description {get; set;}
    
    /**
     * @description The file extension (only Google Drive)
     */
    @AuraEnabled
    public String fileExtension {get; set;}
    
    /**
     * @description The full file extension (only Google Drive)
     */
    @AuraEnabled
    public String fullFileExtension {get; set;}
    
    /**
     * @description The icon url (only Google Drive)
     */
    @AuraEnabled
    public String iconLink {get; set;}
    
    /**
     * @description The thumbnail url (only Google Drive)
     */
    @AuraEnabled
    public String thumbnailLink {get; set;}
    
    @AuraEnabled
    public String webContentLink {get; set;}
    
    @AuraEnabled
    public String removeParents {get; set;}
    
    @AuraEnabled
    public String addParents {get; set;}
    
    @AuraEnabled
    public List<Map<String, String>> owners;
    
    @AuraEnabled
    public List<String> parents {get; set;}
    
    @AuraEnabled
    public String parentId {get; set;}

    @AuraEnabled
    public String siteId {get; set;}
    
    
    @AuraEnabled
    public String size {get; set;}

    /**
     * @description The custom app porperty map (only Google Drive)
     */
    @AuraEnabled
    public Map<String,String> appProperties {get; set;}

    /**
     * @description The related Sharepoint Ids such as drives, list and so on (only Microsoft Sharepoint)
     */
    @AuraEnabled
    public Map<String,String> sharepointIds {get; set;}

    /**
     * @description The conflict behaviour when creating a new folder (only Microsoft Sharepoint). 'rename' or 'replace'
     */
    @AuraEnabled
    public string conflictBehavior = 'rename';

    /**
     * @description The file download URL (only Microsoft Sharepoint)
     */
    @AuraEnabled
    public String downloadUrl {get; set;}

    /**
     * @description The file metadata (only Microsoft Sharepoint)
     */
    @AuraEnabled
    public SharepointFileDescription file {
        get; 
        set;
    }

    /**
     * @description The folder metadata (only Microsoft Sharepoint)
     */
    @AuraEnabled
    public Map<String,String> folder {get; set;}

    @AuraEnabled
    public Boolean isFolder {
        get {
            return this.folder != null;
        }
    }


    /**
     * @description Constructs empty wrapper
     */
    public OM_ContentWrapper() {
        this.appProperties = new Map<String, String>();
        this.sharepointIds = new Map<String, String>();
    }

    /**
     * @description Gets all Salesforce content records from a give Drive File
     * @param  pr Salesforce record to be wrapped
     */
    public OM_ContentWrapper(OM_Content__c pr){
        this.name = pr.Name;
        this.id = pr.OM_GDrive_Id__c;
        this.description = pr.OM_Tags__c;
        this.mimeType = pr.OM_Mime_Type__c;
        this.appProperties = new Map<String, String>();
        this.sharepointIds = new Map<String, String>();
    }

    /**
    * @description Find a Salesforce record matching provider file based on a current mapping
    * @author OmegaCRM
    * @param mapping The children mappings from of the applied Content Settings
    * @return OM_Content__c 
    **/
    public OM_Content__c findSalesforceRecord(List<OM_ContentCustomProperty__c> mapping) {
        return [
            SELECT Id, OM_GDrive_Id__c, OM_Tags__c, OM_Mime_Type__c
            FROM OM_Content__c
            WHERE OM_GDrive_Id__c = :this.id WITH SECURITY_ENFORCED LIMIT 1];
    }

    /**
    * @description Converts provider file into Salesforce record using mapping
    * @author OmegaCRM
    * @param mapping The children mappings from of the applied Content Settings
    * @return OM_Content__c 
    **/
    public OM_Content__c toSalesforceRecord(List<OM_ContentCustomProperty__c> mapping) {
        OM_Content__c c = new OM_Content__c(
            Name = this.name,
            OM_GDrive_Id__c = this.id,
            OM_Tags__c = this.description,
            OM_Mime_Type__c = this.mimeType
        );
        //Append custom properties
        if(mapping != null) {
            System.debug(LoggingLevel.ERROR, 'Custom mapping specified');
            for(OM_ContentCustomProperty__c mapp : mapping) {
                System.debug(LoggingLevel.ERROR, 'Mapping to' + mapp.OM_Salesforce_field_name__c + ' from ' + mapp.OM_Property_name__c);
                c.put(mapp.OM_Salesforce_field_name__c, this.appProperties.get(mapp.OM_Property_name__c));
            }
        }
        return c;
    }

    class SharepointFileDescription {
        public String mimeType;
        public Map<String, String> hashes;
    }
}