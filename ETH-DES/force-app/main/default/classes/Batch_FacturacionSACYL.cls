global without sharing class Batch_FacturacionSACYL implements Database.Batchable<sObject>, Database.AllowsCallouts{
    public List<Integracion_Facturacion__c> logFacturacionList = new List<Integracion_Facturacion__c>();


    global Batch_FacturacionSACYL() {

    }

    global List <case> start(Database.BatchableContext bc) {
        
        //Mes actual
        Integer month = Integer.valueOf(String.valueOf(date.today().format()).split('/')[1]);
        
        //Año actual
        Integer year = Integer.valueOf(String.valueOf(date.today().format()).split('/')[2]);

        month = (month==1? 12 : month-1);
        year = (month==1? (year-1) : year);

        //Días del mes
        Integer monthDays = Date.daysInMonth(year,month);

        String query = 'SELECT id, CaseNumber, Cod_Iden__c, Account.PersonContactId, Account.FirstName, Account.LastName, '+
                    + 'Account.Suffix, Account.NIF__c, Account.SeguridadSocial__c, Account.NumeroPoliza__c, '+
                    + 'Account.HealthCloudGA__Age__pc, Cliente__r.Cod_cliente__c, Lote__r.Cod_lot__c, '+
                    + 'tratamiento__r.Cod_tra__c, tratamiento__r.Nombre_Facturacion__c, '+
                    + 'Med_Presc__r.Cod_med__c, Med_Presc__r.NombreCompleto__c, Tipo_Iden__c, '+
                    + 'Presc_number__c, Fec_ini_tra__c, Fec_fin_tra__c, AccountId '+ 
                    + 'FROM Case '+
                    + 'where (Fec_fin_tra__c = null OR (Fec_fin_tra__c >= '+ Datetime.newInstance(year, month, 1).format('YYYY-MM-dd') + ' AND Fec_fin_tra__c < ' + Datetime.newInstance(year, month, monthDays).format('YYYY-MM-dd')+ ')) '+
                    + 'and Fec_ini_tra__c <= ' + Datetime.newInstance(year, month, monthDays).format('YYYY-MM-dd') + ' AND Cliente__r.Cod_cliente__c =\'7\' and Account.FirstName !=\'Test\' ';

        system.debug('Query: '+query);

        system.debug('Resultado query: '+Database.query(query).size());
        return Database.query(query);
           
    }

    global void execute(Database.BatchableContext BC, List<Case> scope) {
        // Map <String, Case> caseIdToMapErrors = new Map <String, Case> ();
        system.debug('Casos a facturar: '+scope);

        Set<Id> contactIds = new Set<Id>();
        for(Case auxCase : scope){
            contactIds.add(auxCase.Account.PersonContactId);
        }

        Map<Id, Integer> patientIdToIngresoDays = new Map<Id, Integer>();

        //Mes actual
        Integer month = Integer.valueOf(String.valueOf(date.today().format()).split('/')[1]);
        //Año actual
        Integer year = Integer.valueOf(String.valueOf(date.today().format()).split('/')[2]);
        //Días del mes

        month = (month==1? 12 : month-1);
        year = (month==1? (year-1) : year);

        Integer monthDays = Date.daysInMonth(year,month);

        List<PersonLifeEvent> ingresosPacientes = [select id, EventDate, ExpirationDate, EventType, PrimaryPerson.AccountId, PrimaryPersonId from PersonLifeEvent
                                                    where EventType = 'Ingreso hospitalario'
                                                    and EventDate >=: Datetime.newInstance(year, month, 1)
                                                    and (ExpirationDate <: Datetime.newInstance(year, month + 1, 1) OR ExpirationDate = null)
                                                    and IsDeleted= false
                                                    and PrimaryPersonId IN: contactIds];

        for (PersonLifeEvent ingresoAux : ingresosPacientes) {
            if(patientIdToIngresoDays.isEmpty() || !patientIdToIngresoDays.containsKey(ingresoAux.PrimaryPerson.AccountId)){        
                patientIdToIngresoDays.put(ingresoAux.PrimaryPerson.AccountId, Date.valueOf(ingresoAux.ExpirationDate).daysBetween(Date.valueOf(ingresoAux.EventDate)));
            }else{
                Integer daysIngreso = patientIdToIngresoDays.get(ingresoAux.PrimaryPerson.AccountId);
                daysIngreso += Date.valueOf(ingresoAux.ExpirationDate).daysBetween(Date.valueOf(ingresoAux.EventDate));
                patientIdToIngresoDays.put(ingresoAux.PrimaryPerson.AccountId, daysIngreso);
            }
        }
                        
        // system.debug('Mapa de ingreso de clientes: '+patientIdToIngresoDays);
            for(Case auxCase : scope){

                try{

                    String requestBody = '';

                    try{
                        Map<Boolean, String> mapResponse = getToken(auxCase, patientIdToIngresoDays);
                        system.debug('Mapa custom respuesta: '+mapResponse);

                        //Si la llamada al token y a facturar han ido bien:
                        if(mapResponse.containsKey(false) ){
                            
                            Integracion_Facturacion__c elementoFacturado = insertLogFacturacion(auxCase, true, 200, 'Facturación correcta.', 
                                                                                                auxCase.Id, month, year, 
                                                                                                String.valueOf(year) + String.valueOf(month) +auxCase.Id, 
                                                                                                patientIdToIngresoDays);

                            logFacturacionList.add(elementoFacturado);
                        
                        }else{

                            Integracion_Facturacion__c elementoFacturado = insertLogFacturacion(auxCase, false, Integer.valueOf(mapResponse.get(true).split(' - ')[0]), mapResponse.get(true).split(' - ')[1], 
                                                                                                auxCase.Id, month, year, 
                                                                                                String.valueOf(year) + String.valueOf(month) +auxCase.Id, 
                                                                                                patientIdToIngresoDays);

                            logFacturacionList.add(elementoFacturado);
                        }
                    }catch(System.CalloutException coex){

                        Integracion_Facturacion__c elementoFacturado = insertLogFacturacion(auxCase, false, null, coex.getMessage(), 
                                                                                                auxCase.Id, month, year, 
                                                                                                String.valueOf(year) + String.valueOf(month) +auxCase.Id, 
                                                                                                patientIdToIngresoDays);
                        logFacturacionList.add(elementoFacturado);

                    }catch(System.JSONException jsonex){

                        Integracion_Facturacion__c elementoFacturado = insertLogFacturacion(auxCase, false, null, jsonex.getMessage(), 
                                                                                                auxCase.Id, month, year, 
                                                                                                String.valueOf(year) + String.valueOf(month) +auxCase.Id, 
                                                                                                patientIdToIngresoDays);

                        logFacturacionList.add(elementoFacturado);
                    }

                }catch(Exception e){
                    Integracion_Facturacion__c elementoFacturado = insertLogFacturacion(auxCase, false, null, e.getMessage(), 
                                                                                                auxCase.Id, month, year, 
                                                                                                String.valueOf(year) + String.valueOf(month) +auxCase.Id, 
                                                                                                patientIdToIngresoDays);

                    logFacturacionList.add(elementoFacturado);
                }
            }

        upsert logFacturacionList IdExterno__c;
    }

    public static Integracion_Facturacion__c insertLogFacturacion(Case auxCase, Boolean success, Integer code, String message, String caseId, Decimal mesFact, Decimal anioFact, String externalId, Map<Id, Integer> patientIdToIngresoDays){
        
        //Mes actual
        Integer month = Integer.valueOf(String.valueOf(date.today().format()).split('/')[1]);
        //Año actual
        Integer year = Integer.valueOf(String.valueOf(date.today().format()).split('/')[2]);
        //Días del mes

        month = (month==1? 12 : month-1);
        year = (month==1? (year-1) : year);

        Integer monthDays = Date.daysInMonth(year,month);
        
        Integracion_Facturacion__c elementoFacturado = new Integracion_Facturacion__c();
        elementoFacturado.Success__c = success;
        if(code != null){
            elementoFacturado.Code__c = code;
        }
        elementoFacturado.Message__c = message;
        elementoFacturado.Case__c = caseId;
        elementoFacturado.Mes_de_facturacion__c = mesFact;
        elementoFacturado.Anio_de_facturacion__c = anioFact;
        elementoFacturado.IdExterno__c = externalId;

        Map<String, Object> bodyToSend = new Map<String, Object>();

        bodyToSend.put('fact_id_tratamiento', auxCase.CaseNumber);
        bodyToSend.put('fact_cod_iden', auxCase.Cod_Iden__c);
        bodyToSend.put('fact_cod_cli', auxCase.Cliente__r.Cod_cliente__c);
        bodyToSend.put('fact_lote', auxCase.Lote__r.Cod_lot__c);
        bodyToSend.put('fact_tip_iden', auxCase.Tipo_Iden__c);
        bodyToSend.put('fact_nom_pac', auxCase.Account.FirstName);
        bodyToSend.put('fact_ape1_pac', auxCase.Account.LastName);
        bodyToSend.put('fact_ape2_pac', auxCase.Account.Suffix);
        bodyToSend.put('fact_dni', auxCase.Account.NIF__c);
        bodyToSend.put('fact_nss', auxCase.Account.SeguridadSocial__c);
        bodyToSend.put('fact_num_pol', auxCase.Account.NumeroPoliza__c);
        bodyToSend.put('fact_edad_pac', auxCase.Account.HealthCloudGA__Age__pc.split(' ')[0]);
        bodyToSend.put('fact_cod_tra', String.valueOf(auxCase.tratamiento__r.Cod_tra__c));
        bodyToSend.put('fact_nom_tra', auxCase.tratamiento__r.Nombre_Facturacion__c);
        bodyToSend.put('fact_medico', auxCase.Med_Presc__r.Cod_med__c!= null ?  auxCase.Med_Presc__r.Cod_med__c : ''  + ' - ' + auxCase.Med_Presc__r.NombreCompleto__c != null ?  auxCase.Med_Presc__r.NombreCompleto__c : '');
        bodyToSend.put('fact_num_presc', auxCase.Presc_number__c);
        bodyToSend.put('fact_ingreso_pac', patientIdToIngresoDays.get(auxCase.AccountId));
        bodyToSend.put('fact_fec_ini_tra', String.valueOf(auxCase.Fec_ini_tra__c) + 'T00:00:00.000');
        bodyToSend.put('fact_fec_fin_tra', auxCase.Fec_fin_tra__c != null ? String.valueOf(auxCase.Fec_fin_tra__c) + 'T00:00:00.000' : '');
        bodyToSend.put('idsalesforce', auxCase.Id);           
        
        //Si el tratamiento empezó antes de este mes, cogemos el día 1 de este mes


        if(Integer.valueOf(String.valueOf(auxCase.Fec_ini_tra__c.format()).split('/')[1]) == month &&  
            Integer.valueOf(String.valueOf(auxCase.Fec_ini_tra__c.format()).split('/')[2]) == year){
            //Si el tratamiento comenzó este mes, cogemos el día de inicio
            bodyToSend.put('fact_fec_ini_per', String.valueOf(auxCase.Fec_ini_tra__c).split(' ')[0] + 'T00:00:00.000');

        }else{
            // bodyToSend.put('fact_fec_ini_per',String.valueOf(Datetime.newInstance(year, month, 1, 0, 0, 0)).replace(' ','T')+'.000');
            bodyToSend.put('fact_fec_ini_per',String.valueOf(Datetime.newInstance(year, month, 1, 0, 0, 0)).split(' ')[0] + 'T00:00:00.000');
        }

        //Si el tratamiento no ha terminado, o termina el mes que viene --> se coge el final de este mes

        if(auxCase.Fec_fin_tra__c != null && (Integer.valueOf(String.valueOf(auxCase.Fec_fin_tra__c.format()).split('/')[1]) == month &&  
            Integer.valueOf(String.valueOf(auxCase.Fec_fin_tra__c.format()).split('/')[2]) == year)){
                //El tratamiento acaba este mes --> Se coge su fecha de finalización
                bodyToSend.put('fact_fec_fin_per', String.valueOf(auxCase.Fec_fin_tra__c).split(' ')[0] + 'T00:00:00.000');
        }else{
            bodyToSend.put('fact_fec_fin_per', String.valueOf(Datetime.newInstance(year, month, Date.daysInMonth(year,month), 0, 0, 0)).split(' ')[0] + 'T00:00:00.000');
        }

        system.debug('Mapa del body a serializar: '+bodyToSend); 
        

        elementoFacturado.Body_Enviado__c = JSON.Serialize(bodyToSend);

        return elementoFacturado;

    }

    global void finish(Database.BatchableContext BC) {
    	
    }

    public static Map<Boolean, String> getToken(Case caseToBill, Map<Id, Integer> patientIdToIngresoDays){
        TokenETH__c oCStoken = [SELECT Id, url__c, username__c, password__c, token__c FROM TokenETH__c];

        String sUrl = oCStoken.url__c;
        String sUsername = oCStoken.username__c;
        String sPassword = oCStoken.password__c;
        String sToken = oCStoken.token__c;
        String sMethod = 'POST';

        Map<Boolean,String> responseMap = new Map<Boolean,String>();
        Map<String,Object> tokenResp = new Map<String,Object>();
        
        HttpRequest req = new HttpRequest();
        system.debug('URL TOKEN: '+sUrl + '/login');
        req.setEndpoint(sUrl + '/login');
        req.setMethod(sMethod);
        req.setHeader('Content-Type','application/json');

        Map<String, String> mapAuthen = new Map<String, String>();
        mapAuthen.put('username', sUsername);
        mapAuthen.put('password', sPassword);

        req.setBody(JSON.Serialize(mapAuthen));

        Http http = new Http();
        HTTPResponse res = new HTTPResponse();
        system.debug('Antes de llamar al token');
        system.debug('req vale: '+req);

        res = http.send(req);
        system.debug('Respuesta token: '+res);
        system.debug('La url vale: '+sUrl + '/login');

        if (res.getStatusCode() == 200){
            tokenResp = (Map<String,Object>)JSON.deserializeUntyped(res.getBody());
            return callFacturacionSACYL(String.valueOf(tokenResp.get('accessToken')), oCStoken, caseToBill, patientIdToIngresoDays);
        }else if(res.getStatusCode() == 401){
            responseMap.put(true, res.getStatusCode() + ' - No se ha podido obtener el token de Atlas.');
        }else if(res.getStatusCode() == 500){
            responseMap.put(true, res.getStatusCode() + ' - Error en la comunicación con Atlas.');
        }

        // update pedidoToUpdate;

        return responseMap;
    }

    public static Map<Boolean, String> callFacturacionSACYL(String token, TokenETH__c oCStoken, Case caseToBill, Map<Id, Integer> patientIdToIngresoDays){
        system.debug('Dentro de callFacturacionSACYL');
        Map<Boolean,String> responseMap = new Map<Boolean,String>();

        String sUrl = oCStoken.url__c;
        String sUsername = oCStoken.username__c;
        String sPassword = oCStoken.password__c;
        String sToken = oCStoken.token__c;
        String sMethod = 'POST';

        HttpRequest req = new HttpRequest();
        HTTPResponse res = new HTTPResponse();

        req.setEndpoint(sUrl + '/facturar-sacyl');        
        req.setMethod(sMethod);
        req.setHeader('Content-Type','application/json');
        req.setHeader('Authorization', 'Bearer ' + token);

        Map<String, Object> bodyToSend = new Map<String, Object>();

        bodyToSend.put('fact_id_tratamiento', caseToBill.CaseNumber);
        bodyToSend.put('fact_cod_iden', caseToBill.Cod_Iden__c);
        bodyToSend.put('fact_cod_cli', caseToBill.Cliente__r.Cod_cliente__c);
        bodyToSend.put('fact_lote', caseToBill.Lote__r.Cod_lot__c);
        bodyToSend.put('fact_tip_iden', caseToBill.Tipo_Iden__c);
        bodyToSend.put('fact_nom_pac', caseToBill.Account.FirstName);
        bodyToSend.put('fact_ape1_pac', caseToBill.Account.LastName);
        bodyToSend.put('fact_ape2_pac', caseToBill.Account.Suffix);
        bodyToSend.put('fact_dni', caseToBill.Account.NIF__c);
        bodyToSend.put('fact_nss', caseToBill.Account.SeguridadSocial__c);
        bodyToSend.put('fact_num_pol', caseToBill.Account.NumeroPoliza__c);
        bodyToSend.put('fact_edad_pac', caseToBill.Account.HealthCloudGA__Age__pc.split(' ')[0]);
        bodyToSend.put('fact_cod_tra', String.valueOf(caseToBill.tratamiento__r.Cod_tra__c));
        bodyToSend.put('fact_nom_tra', caseToBill.tratamiento__r.Nombre_Facturacion__c);
        bodyToSend.put('fact_medico', caseToBill.Med_Presc__r.Cod_med__c!= null ?  caseToBill.Med_Presc__r.Cod_med__c : ''  + ' - ' + caseToBill.Med_Presc__r.NombreCompleto__c != null ?  caseToBill.Med_Presc__r.NombreCompleto__c : '');
        bodyToSend.put('fact_num_presc', caseToBill.Presc_number__c);
        bodyToSend.put('fact_ingreso_pac', patientIdToIngresoDays.get(caseToBill.AccountId));
        bodyToSend.put('fact_fec_ini_tra', String.valueOf(caseToBill.Fec_ini_tra__c) + 'T00:00:00.000');
        bodyToSend.put('fact_fec_fin_tra', caseToBill.Fec_fin_tra__c != null ? String.valueOf(caseToBill.Fec_fin_tra__c) + 'T00:00:00.000' : '');
        bodyToSend.put('idsalesforce', caseToBill.Id);           

        //Mes actual
        Integer month = Integer.valueOf(String.valueOf(date.today().format()).split('/')[1]);
        //Año actual
        Integer year = Integer.valueOf(String.valueOf(date.today().format()).split('/')[2]);
        //Número de días del mes actual

        month = (month==1? 12 : month-1);
        year = (month==1? (year-1) : year);

        Integer daysMonth = Date.daysInMonth(year,month);
        

        if(Integer.valueOf(String.valueOf(caseToBill.Fec_ini_tra__c.format()).split('/')[1]) == month &&  
            Integer.valueOf(String.valueOf(caseToBill.Fec_ini_tra__c.format()).split('/')[2]) == year){
            //Si el tratamiento comenzó este mes, cogemos el día de inicio
            bodyToSend.put('fact_fec_ini_per', String.valueOf(caseToBill.Fec_ini_tra__c).split(' ')[0] + 'T00:00:00.000');

        }else{
            // bodyToSend.put('fact_fec_ini_per',String.valueOf(Datetime.newInstance(year, month, 1, 0, 0, 0)).replace(' ','T')+'.000');
            bodyToSend.put('fact_fec_ini_per',String.valueOf(Datetime.newInstance(year, month, 1, 0, 0, 0)).split(' ')[0] + 'T00:00:00.000');
        }


        if(caseToBill.Fec_fin_tra__c != null && Integer.valueOf(String.valueOf(caseToBill.Fec_fin_tra__c.format()).split('/')[1]) == month &&  
            Integer.valueOf(String.valueOf(caseToBill.Fec_fin_tra__c.format()).split('/')[2]) == year){
                //El tratamiento acaba este mes --> Se coge su fecha de finalización
                bodyToSend.put('fact_fec_fin_per', String.valueOf(caseToBill.Fec_fin_tra__c).split(' ')[0] + 'T00:00:00.000');
        }else{
            bodyToSend.put('fact_fec_fin_per', String.valueOf(Datetime.newInstance(year, month, Date.daysInMonth(year,month), 0, 0, 0)).split(' ')[0] + 'T00:00:00.000');
        }

        system.debug('Mapa del body a serializar: '+bodyToSend);
        

        req.setBody(JSON.Serialize(bodyToSend));
        system.debug('Body que mando: '+JSON.Serialize(bodyToSend));
        system.debug('LLAMADA A /facturar-sacyl');
        Http http = new Http();
        res = http.send(req);

        WrapperRespuestaFacturacion SACYLResponse;

        if (res.getStatusCode() == 200){
            system.debug('Respuesta SACYL: '+res.getBody());
            SACYLResponse = (WrapperRespuestaFacturacion) JSON.deserialize(res.getBody(), WrapperRespuestaFacturacion.class);
            if(SACYLResponse.success == true){
                responseMap.put(false, '');
            }else{
                responseMap.put(true, SACYLResponse.code + ' - ' + SACYLResponse.message);
            }
        }else{
            responseMap.put(true, res.getStatusCode() + ' - ' +res.getBody());
        }

        return responseMap;

    }


    public class WrapperRespuestaFacturacion{
        public Boolean success;
        public Integer code;
        public String message;
    }
}