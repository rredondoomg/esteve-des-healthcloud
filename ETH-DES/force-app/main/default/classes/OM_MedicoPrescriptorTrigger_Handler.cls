public with sharing class OM_MedicoPrescriptorTrigger_Handler {
    public static void beforeDelete(Map<Id, Med_Presc__c> oldMap){
        OM_MedicoPrescriptorTrigger_Service.checkDeleteMedicPres(oldMap);
    }
}