@isTest
public with sharing class ContenedorTriggerTest {

  
    @isTest
    public static void usertContenedor(){
       	User userProfesional = ETH_DataFactory.createUser('usertestethprofesional@test.com');
        insert userProfesional;
        Contenedor__c sapp = new Contenedor__c(
            Identificador__c = 'CONTENEDOR10',
            Cod_con__c = 'CONTENEDOR10',
            Name = 'Vehiculo 1C',
            Activo__c = true,
            Almacen__c = 'GV'
        );
        
        Test.startTest();
        System.runAs(userProfesional){
        	Upsert sapp Identificador__c;
            sapp.Name='Vehiculo 1B';
            Upsert sapp Identificador__c;
        }
        Test.stopTest();
    
    }
    
}