/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 04-29-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
public with sharing class ETH_AgendamientoWrapper {
    public DateTime fecha;
    public ServiceTerritoryMember member;
    public String lugarVisita;
    public String tipoLugar;
    public Double frecuencia;
    public Id tratamiento;

    public ETH_AgendamientoWrapper(DateTime fecha, ServiceTerritoryMember member) {
        this.fecha = fecha;
        this.member = member;
    }

    public ETH_AgendamientoWrapper(String lugarVisita, String tipoLugar, Double frecuencia, Id tratamiento){
        this.lugarVisita = lugarVisita;
        this.tipoLugar = tipoLugar;
        this.frecuencia = frecuencia;
        this.tratamiento = tratamiento;
    }
}