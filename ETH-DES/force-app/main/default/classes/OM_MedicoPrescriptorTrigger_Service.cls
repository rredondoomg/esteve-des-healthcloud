public with sharing class OM_MedicoPrescriptorTrigger_Service {
    public static void checkDeleteMedicPres(Map<Id, Med_Presc__c> oldMap) {
        Set<Id> medicosPresSet = new Set<Id>();
        for (Id identMedPres : oldMap.keySet()) {
            medicosPresSet.add(identMedPres);
        }

        //Mapa para incluir los medicos prescriptores que tienen algun caso sin finalizar (y no será permitido su borrado)
        Map<Id, Boolean> medicPresToHasUnfinishedCases = new Map<Id, Boolean>();
        
        //Casos cuyo status != Finalizado
        for (Case auxCase : [SELECT Id, Med_Presc__c FROM Case WHERE Med_Presc__c IN: medicosPresSet AND (Recordtype.DeveloperName = 'Tratamiento_TRD' OR Recordtype.DeveloperName = 'Tratamiento_Pharmate') AND Status != 'Finalizado' AND Status != 'Closed']) {
            if(medicPresToHasUnfinishedCases.isEmpty()){
                medicPresToHasUnfinishedCases.put(auxCase.Med_Presc__c, true);
            }else{
                if(!medicPresToHasUnfinishedCases.containsKey(auxCase.Med_Presc__c)){
                    medicPresToHasUnfinishedCases.put(auxCase.Med_Presc__c, true);
                }
            }
        }

        if(!medicPresToHasUnfinishedCases.isEmpty()){
            for (Med_Presc__c medPresAux : oldMap.values()) {
                if(medicPresToHasUnfinishedCases.containsKey(medPresAux.Id)){
                    medPresAux.addError('No se puede eliminar al médico prescriptor por tener tratamientos activos');
                }
            }
        }

    }
}