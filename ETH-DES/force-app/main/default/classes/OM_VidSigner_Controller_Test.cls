@isTest
public with sharing class OM_VidSigner_Controller_Test {
	@TestSetup
    static void makeData(){
        ETH_DataFactory.setUpOmegaContent();
        //crear un usuario
        User userProfesional = ETH_DataFactory.createUser('usertestethprofesional@test.com');
        insert userProfesional;

        User user = [SELECT Id FROM User WHERE LastName = 'Omega' LIMIT 1];

        System.runAs(user){
			//BGR start
			ETH__c oETH = new ETH__c(
				Name = 'ETH',
				HoraVisita__c = '08:00:00',
				vidsigner_url__c='URL',
				vidsigner_SubscriptionName__c='idsigner_SubscriptionName',
				vidsigner_SubscriptionPass__c='vidsigner_SubscriptionPass'

			);
			insert oETH;
			
            MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
            Test.setMock(HttpCalloutMock.class, multimock);

            Test.startTest();

            Account account = ETH_DataFactory.createAccountWithAddress('Spain', 'Granada', 'Periodista Fernando Gomez de la Cruz', '18014', 'Granada');
            insert account;

            OperatingHours operatingHours = ETH_DataFactory.createOperatingHours();
            insert operatingHours;

            //timeslots de operating hours
            ETH_DataFactory.createTimeSlots(operatingHours.Id, 5, 23);

            ServiceTerritory serviceTerritoryPadre = ETH_DataFactory.createServiceTerritory(null, operatingHours.Id, 'Pharmate', null);
            insert serviceTerritoryPadre;

            ServiceTerritory serviceTerritoryHijo = ETH_DataFactory.createServiceTerritory(account.ShippingPostalCode, operatingHours.Id, 'Pharmate', serviceTerritoryPadre.Id);
            insert serviceTerritoryHijo;

            //crear un assigned resource a cada usuario
            ServiceResource serviceResourceProfesional = ETH_DataFactory.createServiceResourceProfesional(userProfesional.Id);
            insert serviceResourceProfesional;

            //crear los services territories member
            ServiceTerritoryMember serviceTerritoryMemberProfesional = ETH_DataFactory.createServiceTerritoryMember('Profesional Visita Presencial (Pharmate)', serviceResourceProfesional.Id, serviceTerritoryPadre.Id, operatingHours.Id);
            insert serviceTerritoryMemberProfesional;

            //repetimos proceso para CRETA y box
            ServiceTerritory serviceTerritoryCRETA = ETH_DataFactory.createServiceTerritory(account.ShippingPostalCode, operatingHours.Id, 'Creta',  null);
            insert serviceTerritoryCRETA;

            protocolo__c protocolo = ETH_DataFactory.createProtocolo();
            insert protocolo;

            Paso_Protocolo__c paso1 = ETH_DataFactory.createPasoProtocolo(protocolo.Id, 1, 'Virtual', 'Usuario que da de alta el tratamiento', 'Inicio', 'Domicilio', 7);
            insert paso1;
            Paso_Protocolo__c paso2 = ETH_DataFactory.createPasoProtocolo(protocolo.Id, 2, 'Presencial', 'Profesional Visita Presencial (Pharmate)', 'Seguimiento', 'CRETA', 7);
            insert paso2;

            //alta
            Worktype worktypeAlta = ETH_DataFactory.createWorktype('Alta', 'Pharmate', paso1.tipo_Lugar__c, paso1.lugar_visita__c);
            insert worktypeAlta;
            //domicilio
            Worktype worktypeSeguimientoDomicilio = ETH_DataFactory.createWorktype('Seguimiento', 'Pharmate', paso2.tipo_Lugar__c, paso2.lugar_visita__c);
            insert worktypeSeguimientoDomicilio;

			Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'Pharmate');
			tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, protocolo.Id, 'TRD');
			insert tratamiento;
			ca.Tratamiento__c = tratamiento.Id;

			System.runAs(userProfesional){
				insert ca;
			}
        }
        Test.stopTest();
    }

	@isTest static void getVisitType(){
		Account account = [SELECT Id, account.NIF__c FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];

        ServiceAppointment cita = [SELECT Id, OwnerId, ServiceTerritoryId, Paso_Protocolo__c, Status FROM ServiceAppointment WHERE ParentRecordId = :account.Id LIMIT 1];

		OM_VidSigner_Controller.getVisitType(cita.Id);
	}

	@isTest static void getVisitStatusEnCurso(){
		Account account = [SELECT Id, account.NIF__c FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];

        ServiceAppointment cita = [SELECT Id, OwnerId, ServiceTerritoryId, Paso_Protocolo__c, Status FROM ServiceAppointment WHERE ParentRecordId = :account.Id LIMIT 1];

		OM_VidSigner_Controller.getVisitStatusEnCurso(cita.Id);
	}

	@isTest static void testMethodConsentimiento() { //BGR: quitamos el (seeAllData=true)

        Account account = [SELECT Id, account.NIF__c FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];

        ServiceAppointment cita = [SELECT Id, OwnerId, ServiceTerritoryId, Paso_Protocolo__c, Status FROM ServiceAppointment WHERE ParentRecordId = :account.Id LIMIT 1];

		Test.startTest();

		String out = OM_VidSigner_Controller.getPatientInfoJSON(cita.Id);

		Test.setMock(HttpCalloutMock.class, new YourHttpCalloutMockImpl());
		
		OM_VidSigner_Controller.getJSONVidSigner(cita.Id, '33445566B', 'cons', 'ESP', 'Privado', 'Zaragoza', '', '', null, 'Consentimiento');
		OM_VidSigner_Controller.getJSONVidSigner(cita.Id, '33445566B', 'baja', 'ESP', 'Privado', 'Zaragoza', '', '', null, 'Baja');
		OM_VidSigner_Controller.getJSONVidSigner(cita.Id, '33445566B', 'parte_visita', 'ESP', 'Privado', 'Zaragoza', '', '', null, 'parte_visita');

		//test para los controllers de las 2 visualforce
		PageReference pageRef = Page.consentimiento;
		pageRef.getParameters().put('idprescripcion', cita.Id);
		pageRef.getParameters().put('nifPaciente', '33445566B');
		Test.setCurrentPage(pageRef);
		
		CONTROLLER_consentimiento test_cons = new CONTROLLER_consentimiento();

		test_cons.getContentText('Zaragoza','ESP', 'PR');	

		PageReference pageRef2 = Page.baja_voluntaria;
		pageRef2.getParameters().put('idvisita', cita.Id);
		pageRef2.getParameters().put('nifPaciente', '33445566B');
		Test.setCurrentPage(pageRef2);			
				
		Test.stopTest();

		String outGetPage = OM_VidSigner_Controller.getPage(cita.Id, '33445566B', 'cons', 'ESP', 'PR', 'Zaragoza', 'Name', '', null);

		String expectedValue = EncodingUtil.base64encode(blob.valueOf('Unit.Test_Cons'));

	}

	@isTest static void testMethodBajaVoluntariaTutor() { //BGR: quitamos el (seeAllData=true)

		Account account = [SELECT Id, account.NIF__c FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];

        ServiceAppointment cita = [SELECT Id, OwnerId, ServiceTerritoryId, Paso_Protocolo__c, Status FROM ServiceAppointment WHERE ParentRecordId = :account.Id LIMIT 1];

		Test.startTest();

		String out = OM_VidSigner_Controller.getPatientInfoJSON(cita.Id);

		Test.setMock(HttpCalloutMock.class, new YourHttpCalloutMockImpl());
		
		OM_VidSigner_Controller.getJSONVidSigner(cita.Id, '33445566B', 'cons', 'ESP', 'Privado', 'Zaragoza', 'Test Name', '34445566D',null, 'Consentimiento');
		OM_VidSigner_Controller.getJSONVidSigner(cita.Id, '33445566B', 'baja', 'ESP', 'Privado', 'Zaragoza', 'Test Name', '34445566D',null, 'Baja');
		OM_VidSigner_Controller.getJSONVidSigner(cita.Id, 'parte_visita', 'baja', 'ESP', 'Privado', 'Zaragoza', 'Test Name', '34445566D',null, 'parte_visita');

		//test para los controllers de las 2 visualforce
		PageReference pageRef = Page.consentimiento;
		pageRef.getParameters().put('idprescripcion', cita.Id);
		pageRef.getParameters().put('nifPaciente', '33445566B');
		Test.setCurrentPage(pageRef);
			
		PageReference pageRef2 = Page.baja_voluntaria;
		pageRef2.getParameters().put('idprescripcion', cita.Id);
		pageRef2.getParameters().put('nifPaciente', '33445566B');
		Test.setCurrentPage(pageRef2);
		
		//ET201801-246
		CONTROLLER_baja_voluntaria test_baja = new CONTROLLER_baja_voluntaria();

		test_baja.getContentText('Zaragoza','ESP', 'PR');
				
		Test.stopTest();

		String outGetPage = OM_VidSigner_Controller.getPage(cita.Id, '33445566B', 'baja', 'ESP', 'PR', 'Zaragoza', 'Name', '', null);

		String expectedValue = EncodingUtil.base64encode(blob.valueOf('Unit.Test_Baja'));

	}

	@isTest static void testMethodParteVisitaWorkTypeAlta() { //BGR: quitamos el (seeAllData=true)

		Account account = [SELECT Id, account.NIF__c FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];

        ServiceAppointment cita = [SELECT Id, OwnerId, ServiceTerritoryId, Paso_Protocolo__c, Status FROM ServiceAppointment WHERE ParentRecordId = :account.Id LIMIT 1];

		Test.startTest();

		String out = OM_VidSigner_Controller.getPatientInfoJSON(cita.Id);

		Test.setMock(HttpCalloutMock.class, new YourHttpCalloutMockImpl());
		
		OM_VidSigner_Controller.getJSONVidSigner(cita.Id, '33445566B', 'cons', 'ESP', 'Privado', 'Zaragoza', 'Test Name', '34445566D',null, 'Consentimiento');
		OM_VidSigner_Controller.getJSONVidSigner(cita.Id, '33445566B', 'baja', 'ESP', 'Privado', 'Zaragoza', 'Test Name', '34445566D',null, 'Baja');
		OM_VidSigner_Controller.getJSONVidSigner(cita.Id, '33445566B', 'parte_visita', 'ESP', 'Privado', 'Zaragoza', 'Test Name', '34445566D',null, 'parte_visita');

		//test para los controllers de las 2 visualforce
		PageReference pageRef = Page.parte_visita;
		pageRef.getParameters().put('idprescripcion', cita.Id);
		pageRef.getParameters().put('nifPaciente', '33445566B');
		Test.setCurrentPage(pageRef);

		CONTROLLER_parte_visita  testAccPlan = new CONTROLLER_parte_visita();
				
		Test.stopTest();

		String outGetPage = OM_VidSigner_Controller.getPage(cita.Id, '33445566B', 'parte_visita', 'ESP', 'PR', 'Zaragoza', 'Name', '', null);
		system.debug('outGetPage vale: '+outGetPage);
		String expectedValue = EncodingUtil.base64encode(blob.valueOf('Unit.Test_parte_visita'));

	}

	@isTest static void testMethodParteVisitaWorkTypeBaja() { //BGR: quitamos el (seeAllData=true)

		Account account = [SELECT Id, account.NIF__c FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];

        ServiceAppointment cita = [SELECT Id, OwnerId, ServiceTerritoryId, workType.Id, Paso_Protocolo__c, Status FROM ServiceAppointment WHERE ParentRecordId = :account.Id LIMIT 1];

		Paso_Protocolo__c paso1 = [SELECT Id, tipo_Lugar__c, lugar_visita__c FROM Paso_Protocolo__c WHERE tipo_Lugar__c = 'Virtual'];

		Worktype worktypeBaja = ETH_DataFactory.createWorktype('Fin', 'Pharmate', paso1.tipo_Lugar__c, paso1.lugar_visita__c);
		insert worktypeBaja;

		cita.workType.Id = worktypeBaja.Id;
		update cita;

		Test.startTest();

		String out = OM_VidSigner_Controller.getPatientInfoJSON(cita.Id);

		Test.setMock(HttpCalloutMock.class, new YourHttpCalloutMockImpl());
		
		OM_VidSigner_Controller.getJSONVidSigner(cita.Id, '33445566B', 'cons', 'ESP', 'Privado', 'Zaragoza', 'Test Name', '34445566D',null, 'Consentimiento');
		OM_VidSigner_Controller.getJSONVidSigner(cita.Id, '33445566B', 'baja', 'ESP', 'Privado', 'Zaragoza', 'Test Name', '34445566D',null, 'Baja');
		OM_VidSigner_Controller.getJSONVidSigner(cita.Id, '33445566B', 'parte_visita', 'ESP', 'Privado', 'Zaragoza', 'Test Name', '34445566D',null, 'parte_visita');

		//test para los controllers de las 2 visualforce
		PageReference pageRef = Page.parte_visita;
		pageRef.getParameters().put('idprescripcion', cita.Id);
		pageRef.getParameters().put('nifPaciente', '33445566B');
		Test.setCurrentPage(pageRef);

		CONTROLLER_parte_visita  testAccPlan = new CONTROLLER_parte_visita();

		Test.stopTest();

		String outGetPage = OM_VidSigner_Controller.getPage(cita.Id, '33445566B', 'parte_visita', 'ESP', 'PR', 'Zaragoza', 'Name', '', null);
		system.debug('outGetPage vale: '+outGetPage);
		String expectedValue = EncodingUtil.base64encode(blob.valueOf('Unit.Test_parte_visita'));

	}

}