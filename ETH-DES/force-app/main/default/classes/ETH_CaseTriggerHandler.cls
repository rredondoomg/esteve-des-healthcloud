/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 05-24-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
public with sharing class ETH_CaseTriggerHandler {
    // Name of our SharePoint settings
    public static final String SETTINGS_NAME = 'sharepointComponent';

    /**
    * @description Creates a folder (inside of the pacient's folder) for the new treatment
    * @author fizquierdo@omegacrmconsulting.com | 01-25-2022 
    * @param String nameFolder 
    * @param String parentFolder 
    **/
    @future (callout=true)
    public static void createFolderSharePoint(String nameFolder, String parentFolder){
        OM_ContentWrapper folderWrapper = new OM_ContentWrapper();
        folderWrapper.parentId = 'root';
        folderWrapper.name = parentFolder + '/' + nameFolder;
        folderWrapper.conflictBehavior = 'replace';
        OM_ContentMicrosoftIntegration integ = new OM_ContentMicrosoftIntegration();
        OM_ContentSetting__c sharepointSettings = OM_ContentService.getSettings(SETTINGS_NAME);
        integ.createFolderPath(sharepointSettings, folderWrapper);
    }

    /**
    * @description For each case (tratamiento) locate the first step of his protocol
    * @author fizquierdo@omegacrmconsulting.com | 01-28-2022 
    * @param Map<Id Id> caseTratamientoMap 
    * @return Map<Id, Paso_Protocolo__c> 
    **/
    public static Map<Id, Paso_Protocolo__c> getFirstStepProtocol(Map<Id, Id> caseTratamientoMap){
        // case - (tipo) tratamiento - protocolo - paso
        Map<Id, Paso_Protocolo__c> firstStepProtocol = new Map<Id, Paso_Protocolo__c>();
        List<String> idsTratamientos = caseTratamientoMap.values();
        Map<Id, Id> tratamientoProtocoloMap = new Map<Id, Id>();
        Map<Id, Paso_Protocolo__c> protocoloPasoMap = new Map<Id, Paso_Protocolo__c>();

        for(tratamiento__c tratamiento : [SELECT Id, Protocolo__c FROM tratamiento__c WHERE Id IN :idsTratamientos]){
            tratamientoProtocoloMap.put(tratamiento.Id, tratamiento.Protocolo__c);
        }

        List<String> idsProtocolos = tratamientoProtocoloMap.values();

        for(Paso_Protocolo__c pasoProtocolo : [SELECT Id, protocolo_Padre__c, tipo_visita__c, tipo_Lugar__c, tipo_prof__c, lugar_visita__c, frecuencia__c, Name, protocolo_Padre__r.Name FROM Paso_Protocolo__c WHERE protocolo_Padre__c IN :idsProtocolos AND tipo_visita__c = 'Inicio']){
            protocoloPasoMap.put(pasoProtocolo.protocolo_Padre__c, pasoProtocolo);
        }

        for(Id caseId : caseTratamientoMap.keySet()){
            Id idTratamiento = caseTratamientoMap.get(caseId);
            Id idProtocolo = tratamientoProtocoloMap.get(idTratamiento);
            //firstStepProtocol.put(caseId, protocoloPasoMap.get(idProtocolo));
            firstStepProtocol.put(idTratamiento, protocoloPasoMap.get(idProtocolo));
        }
        
        return firstStepProtocol;
    }


    /**
    * @description Map with the User Id and the related Service Resource
    * @author fizquierdo@omegacrmconsulting.com | 02-01-2022 
    * @return Map<Id, ServiceResource> 
    **/
    public static Map<Id, ServiceResource> getServiceResourceByUserId(List<Id> userIds){
        Map<Id, ServiceResource> serviceResources = new Map<Id, ServiceResource>();
        for(ServiceResource serviceResource: [SELECT Id, RelatedRecordId FROM ServiceResource WHERE RelatedRecordId IN :userIds]){
            serviceResources.put(serviceResource.RelatedRecordId, serviceResource);
        }
        return serviceResources;
    }

    /**
    * @description Get the name of the treatment,the name of the parent folder for this pacient and his postal code
    * @author fizquierdo@omegacrmconsulting.com | 01-25-2022 
    * @param List<Id> caseIds 
    * @return Map<Id, Case> 
    **/
    public static Map<Id, Case> getRelatedInfoFromCase(List<Id> caseIds){
        Map<Id, Case> cases = new Map<Id, Case>([SELECT Id, Cliente__r.Abreviacion__c, Upab__r.Abreviacion__c, Tratamiento__r.Name, Tratamiento__r.Tratamiento_ET__c, Account.IdAnonimo__c, Account.ShippingPostalCode, Account.ShippingState, Account.ShippingCity, Account.ShippingCountry, Account.ShippingStreet, Account.Name, Account.Movilidad__c FROM Case WHERE Id IN :caseIds]);
        if(cases == null){
            return new Map<Id, Case>();
        }
        return cases;
    }

    /**
    * @description Get address information of the account. (Used in Before Insert)
    * @author fizquierdo@omegacrmconsulting.com | 03-18-2022 
    * @param List<Id> accountIds 
    * @return Map<Id, Account> 
    **/
    public static Map<Id, Account> getAddressByAccount(List<Id> accountIds){
        Map<Id, Account> accountPostalCodeProvincia = new Map<Id, Account>([SELECT Id, ShippingPostalCode, ShippingState, ShippingCity, ShippingCountry, ShippingStreet, ShippingLatitude, ShippingLongitude FROM Account WHERE Id IN :accountIDs]);
        if(accountPostalCodeProvincia == null){
            return new Map<Id, Account>();
        }
        return accountPostalCodeProvincia;
    }

    public static Map<Id, Tratamiento__c> getTratamientosInfo(List<Id> tratamientosIds){
        return new Map<Id, Tratamiento__c>([SELECT Id, No_plan_altas__c, fre_vis_ini__c, Name FROM tratamiento__c WHERE Id IN :tratamientosIds]);
    }

    public static String getCodigoPacientePharmate(String abreviacionCliente, String abreviacionUPAB, Case ca){
        String codigoBuscar = '%' + abreviacionCliente + '-' + abreviacionUPAB + '%';
        List<Case> cases = [SELECT Cod_Iden__c FROM Case WHERE Cod_Iden__c LIKE :codigoBuscar AND AccountId =:ca.AccountId];
        if(!cases.isEmpty()){
            //Existe la combinación de cliente más upab para este paciente, devolvemos esa
            return cases[0].Cod_Iden__c;
        }
        else{
            //Buscamos si existen combinaciones para hacer +1
            cases = [SELECT Cod_Iden__c FROM Case WHERE Cod_Iden__c LIKE :codigoBuscar ORDER BY Cod_Iden__c DESC];
            if(!cases.isEmpty()){
                // El primero es el de mayor número
                List<String> parts = cases[0].Cod_Iden__c.split('-');
                Integer n = Integer.valueOf(parts[2]) + 1;
                // Controlar si tenemos que añadir el 0 delante o no
                String nCliente = String.valueOf(n).length() > 1 ? String.valueOf(n) : '0' + String.valueOf(n);
                return abreviacionCliente + '-' + abreviacionUPAB + '-' + nCliente;
            }
            else{
                //Empezamos la serie en 1
                return abreviacionCliente + '-' + abreviacionUPAB + '-' + '01';
            }
        }
    }

    public static Map<Id, Maestro_UPAB__c> getUpabAbreviacion(List<Id> upabIds){
        return new Map<Id, Maestro_UPAB__c>([SELECT Abreviacion__c FROM Maestro_UPAB__c WHERE Id IN :upabIds]);
    }

    public static Map<Id, Cliente__c> getClienteAbreviacion(List<Id> clienteIds){
        return new Map<Id, Cliente__c>([SELECT Abreviacion__c FROM Cliente__c WHERE Id IN :clienteIds]);
    }

    // public static Map<Id, EmailMessage> getEmailMessagesByCase(List<Id> caseIds){
    //     Map<Id, EmailMessage> emailMessages = new Map<Id, EmailMessage>();

    //     for(EmailMessage message: [SELECT Id, FromAddress, ParentId FROM EmailMessage WHERE ParentId IN :caseIds]){
    //         emailMessages.put(message.ParentId, message);
    //     }

    //     return emailMessages;
    // }
}