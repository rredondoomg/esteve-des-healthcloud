/**
 * @description       Mocking class for Microsoft callouts tests
 * @author            OmegaCRM 
 * @group             Content tests
**/
@isTest
public class OM_ContentMicrosoftIntegrationMock implements HttpCalloutMock {

    Integer tries = 0;
    Boolean with401 = false;
    Boolean with500 = false;
    Boolean errorToken = false;

    /**
    * @description Constructor for error cases
    * @author OmegaCRM  | 02-19-2021 
    * @param with401 Generate code 401 error on first try
    * @param with500 Generate code 500 error on first try
    **/
    public OM_ContentMicrosoftIntegrationMock (Boolean with401, Boolean with500) {
        this.with401 = with401;
        this.with500 = with500;
    }

    /**
    * @description Default constructor
    * @author OmegaCRM  | 02-19-2021 
    **/
    public OM_ContentMicrosoftIntegrationMock () {
        this.with401 = false;
        this.with500 = false;
    }

    /**
    * @description Constructor for errors in access tokens
    * @param errorToken Generate an error on acess token
    * @author OmegaCRM  | 02-19-2021 
    **/
    public OM_ContentMicrosoftIntegrationMock (Boolean errorToken) {
        this.errorToken = errorToken;
        this.with401 = false;
        this.with500 = false;
    }
    
    /**
    * @description Generated a mocked response for the callout
    * @author OmegaCRM  | 02-19-2021 
    * @param req The request to respond as mock
    * @return HTTPResponse The mocked response
    **/
    public HTTPResponse respond(HTTPRequest req) {

        HTTPResponse toreturn;

        // Handle error responses
        if (this.tries == 0) {
            if(this.with401 == true) {
                toreturn = error401();
            }
            if(this.with500 == true) {
                toreturn = error500();
            }
        }

        if ((this.with401 && this.tries >= 1) || !this.with401) {
            toreturn = getResponseForAction(req);
        }

        this.tries++;

        return toreturn;
    }

    /**
    * @description Generates a mocked response for the callout based on the request given
    * @author OmegaCRM  | 02-19-2021 
    * @param req The request to mock response for
    * @return HttpResponse The mocked response
    **/
    public HttpResponse getResponseForAction(HttpRequest req) {
        HTTPResponse toreturn;
        if(req.getEndpoint().contains('/token')) {
            toreturn = getTokenResponse();
        } else if (req.getEndpoint().contains('/children') && req.getMethod() == 'POST' ) {
            toreturn = getCreateFolderResponse();
        } else if (req.getEndpoint().contains(':/content')) {
            toreturn = getUploadFileResponse();
        } else if (req.getMethod() == 'DELETE') {
            toreturn = getDeleteFileResponse();
        } else if (req.getEndpoint().endsWith('/drives?expand=list($expand=columns)')) {
            toreturn = getDrivesResponse();
        } else if (req.getEndpoint().contains('/children') && req.getMethod() == 'GET') {
            toreturn = getListResponse();
        } else {
            toreturn = getBaseResponse();
        }

        return toreturn;
    }

    /**
    * @description Creates a mocked response for creating a folder
    * @author OmegaCRM  | 02-19-2021 
    * @return HTTPResponse The JSON response
    **/
    public HTTPResponse getCreateFolderResponse() {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{ "id":"1234567890", "name": "testFolder"}');
        res.setStatusCode(200);
        return res;
    }

    /**
    * @description Creates a mocked response for uploading a file
    * @author OmegaCRM  | 02-19-2021 
    * @return HTTPResponse The JSON response
    **/
    public HTTPResponse getUploadFileResponse() {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{ "id":"1234567890", "name": "tesFile.txt"}');
        res.setStatusCode(200);
        return res;
    }

    /**
    * @description Creates a mocked response for deleting a file
    * @author OmegaCRM  | 02-19-2021 
    * @return HTTPResponse The JSON response
    **/
    public HTTPResponse getDeleteFileResponse() {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setStatusCode(200);
        return res;
    }

    /**
    * @description Creates a mocked response for a token request
    * @author OmegaCRM  | 02-19-2021 
    * @return HTTPResponse The JSON response
    **/
    public HTTPResponse getTokenResponse() {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        if (this.errorToken) {
            res.setBody('{ "error":"invalid_scope", "error_description": "Test long text description for error" }');

        } else {
            res.setBody('{ "access_token":"testtoken", "refresh_token": "test_refresh" }');
        }
        res.setStatusCode(200);
        return res;
    }

    /**
    * @description Creates a mocked response for any other request
    * @author OmegaCRM  | 02-19-2021 
    * @return HTTPResponse The JSON response
    **/
    public HttpResponse getBaseResponse () {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{ "id":"testid", "name": "testfile.txt", "sharepointIds" : {"listId" : "testlistId", "listItemUniqueId": "testlistItemUniqueId"} }');
        res.setStatusCode(200);
        return res;
    }

    /**
    * @description Creates a mocked response for any other request
    * @author OmegaCRM  | 02-19-2021 
    * @return HTTPResponse The JSON response
    **/
    public HttpResponse getListResponse () {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"value" :[{ "id":"testid", "name": "testfile.txt", "sharepointIds" : {"listId" : "testlistId", "listItemUniqueId": "testlistItemUniqueId"} }]}');
        res.setStatusCode(200);
        return res;
    }

    /**
    * @description Creates a mocked response for drive info
    * @author OmegaCRM  | 02-19-2021 
    * @return HTTPResponse The JSON response
    **/
    public HttpResponse getDrivesResponse () {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"value" :[{ "id":"testid", "name": "testfile.txt", "list" : {"columns" : [{"displayName" : "testColumn"}] }}]}');
        res.setStatusCode(200);
        return res;
    }
 

    /**
    * @description Creates a mocked response with code 401 (error)
    * @author OmegaCRM  | 02-19-2021 
    * @return HTTPResponse The JSON response
    **/
    public HTTPResponse error401() {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{ "error": "Invalid access token" }');
        res.setStatusCode(401);
        return res;
    }

    /**
    * @description Creates a mocked response with code 500 (error)
    * @author OmegaCRM  | 02-19-2021 
    * @return HTTPResponse The JSON response
    **/
    public HTTPResponse error500() {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{ "error": { "message" : "Internal server error" }}');
        res.setStatusCode(500);
        return res;
    }
}