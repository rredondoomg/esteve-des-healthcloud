/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 08-24-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
public with sharing class CONTROLLER_parte_visita {

	public static List<Equipos_sum__c> equiposSuministrados {get;set;}
    public static List<Fungibles_sum__c> fungiblesSuministrados {get;set;}
	public static List<Boolean> showEquiposSuministrados {get;set;}
    private static Integer maxEquiposSuministrados = 8;
    public static List<Boolean> showFungibles {get;set;}
    private static Integer maxFungiblesSuministrados = 5;
    public static List<ServiceAppointment> dataFromPaciente {get;set;}
    public static String timeFormat {get;set;}
    public static String dateFormat {get;set;}
    public static Boolean esInicial {get;set;}
    public static Boolean esSeguimiento {get;set;}
    public static Boolean esIncidencia {get;set;}
    public static Boolean esBaja {get;set;}
    public static String dni {get;set;}

	// public String nombrePaciente { get; set; }
	public String nifPaciente { get; set; }
	// public String cipPaciente { get; set; }
	// public String city { get; set; }
	// public String lan { get; set; }
	// public String fecha { get; set; }
	// public String tratamiento { get; set; }
	public String idServiceAppointment { get; set; }

    public CONTROLLER_parte_visita() {

        idServiceAppointment = ApexPages.currentPage().getParameters().get('idprescripcion');
		nifPaciente = ApexPages.currentPage().getParameters().get('nifPaciente');
		
        
        system.debug('idServiceAppointment '+idServiceAppointment);
  

        dataFromPaciente = [SELECT Id, Account.FirstName, Account.Lastname, Account.NIF__c, 
                            Account.SeguridadSocial__c, Worktype.Name
                            FROM ServiceAppointment WHERE Id =: idServiceAppointment];

        if(nifPaciente == null){
            dni = dataFromPaciente[0].Account.NIF__c;
        }
        else{
            dni = nifPaciente;
        }

        esInicial = false;
        esSeguimiento = false;
        esIncidencia = false;
        esBaja = false;

        // List<Worktype> wTypeFromServiceApp = [SELECT Id, Name FROM WorkType WHERE ServiceAppointment =: idServiceAppointment];
        if(String.valueOf(dataFromPaciente[0].WorkType.name) != null){
            system.debug('WorkType no está vacio');
            if(String.valueOf(dataFromPaciente[0].WorkType.name).containsIgnoreCase('Alta')){
                esInicial = true;
            }else if(String.valueOf(dataFromPaciente[0].WorkType.name).containsIgnoreCase('Fin')){esBaja = true;}else if(String.valueOf(dataFromPaciente[0].WorkType.name).containsIgnoreCase('Seguimiento')){esSeguimiento = true;}else if(String.valueOf(dataFromPaciente[0].WorkType.name).containsIgnoreCase('Incidencia')){esIncidencia = true;}
        }

		showEquiposSuministrados = new List<Boolean>();
        showFungibles = new List<Boolean>();
        for (Integer i = 0; i < maxEquiposSuministrados; i ++) {
            showEquiposSuministrados.add(false);
        }

        for (Integer i = 0; i < maxFungiblesSuministrados; i ++) {
            showFungibles.add(false);
        }

        List<Visita_tratamiento__c> visitaTratamiento = [SELECT id FROM Visita_tratamiento__c WHERE Service_Appointment__c =: idServiceAppointment];
        if(!visitaTratamiento.isEmpty()){

            equiposSuministrados = [SELECT id, marca__c, modelo__c FROM Equipos_sum__c WHERE Visita_tratamiento__c =: visitaTratamiento[0].Id LIMIT 7];
            if(!equiposSuministrados.isEmpty()){
                for (Integer i = 0; i < equiposSuministrados.size(); i ++) {showEquiposSuministrados[i] = true;}
            }
            
            fungiblesSuministrados = [SELECT id, Marca_modelo_fungible__r.Name, Cantidad__c FROM Fungibles_sum__c WHERE Visita_tratamiento__c =: visitaTratamiento[0].Id LIMIT 5];
            if(!fungiblesSuministrados.isEmpty()){
                for (Integer i = 0; i < fungiblesSuministrados.size(); i ++) {showFungibles[i] = true;}
            }
            
        }

        system.debug('Datetime: '+datetime.now().format('dd/MM/yyyy-hh:mm'));
        String [] formatedDatetime = String.valueOf(datetime.now().format('dd/MM/yyyy-hh:mm')).split('-');
        dateFormat = formatedDatetime[0];
        timeFormat = formatedDatetime[1];
	

	}
}