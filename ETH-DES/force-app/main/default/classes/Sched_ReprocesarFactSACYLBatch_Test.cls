@isTest
public with sharing class Sched_ReprocesarFactSACYLBatch_Test {
    @TestSetup
    static void makeData(){
        ETH_DataFactory.setUpOmegaContent();

        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);    

        User userProfesional = ETH_DataFactory.createUser('usertestethprofesional@test.com');
        insert userProfesional;

        User user = [SELECT Id FROM User WHERE LastName = 'Omega' LIMIT 1];

        system.runAs(user){
            Test.startTest();
    
            Account account = ETH_DataFactory.createAccountWithAddress('Spain', 'Granada', 'Periodista Fernando Gomez de la Cruz', '18014', 'Granada');
            account.PersonBirthdate = Date.valueOf(Datetime.newInstance(1967, 9, 16));
            insert account;

            Account contactId= [select id, PersonContactId from Account where id=:account.id limit 1];
    
            Cliente__c cliente = new Cliente__c(Name = 'ClienteTEST', CIF__c = 'Q3469002D', Cod_cliente__c = '7', IdentificacionPac__c = 'DNI');
            insert cliente;

            PersonLifeEvent ingreso = new PersonLifeEvent(Name = 'Ingreso', PrimaryPersonId = contactId.PersonContactId, EventDate = Date.today().addMonths(-1), ExpirationDate= Date.today().addMonths(-1).adddays(2) , EventType = 'Ingreso hospitalario');
            insert ingreso;

            tratamiento__c trat = new Tratamiento__c(Cod_tra__c = 1233, Nombre_Facturacion__c = 'NombreFactTest', Cliente__c = cliente.Id, Linea_de_negocio__c = 'Oxigenoterapia',
                                                        Terapia__c = 'Oxigenoterapia', con_min_dia__c = 0, fre_vis__c = 30, fre_vis_ini__c = 5, Tratamiento_ET__c = 'Oxígeno Líquido');
            insert trat;

            Med_Presc__c medPres = new Med_Presc__c(Cod_med__c = '123', Nombre__c = 'Medico Pre');

            Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'TRD');
            ca.Fec_ini_tra__c = Date.today().addMonths(-1); 
            ca.Cliente__c = cliente.Id;
            ca.Tipo_Iden__c = 'DNI';
            ca.tratamiento__c = trat.Id;
            ca.Presc_number__c = '123123';
            insert ca;

            TokenETH__c oCStoken = new TokenETH__c();
            oCStoken.url__c = 'http://93.90.29.156:9002';
            oCStoken.username__c = 'omega';
            oCStoken.password__c = '44V-7J6RE-oLs&n.b7';
            oCStoken.token__c = '11111111111';
            insert oCStoken;

            Integracion_Facturacion__c elementoFacturado = new Integracion_Facturacion__c();
            elementoFacturado.Success__c = false;
            elementoFacturado.Code__c = 400;
            elementoFacturado.Message__c = 'Error al facturar';
            elementoFacturado.Case__c = ca.Id;
            elementoFacturado.Mes_de_facturacion__c = 9;
            elementoFacturado.Anio_de_facturacion__c = 2022;
            elementoFacturado.IdExterno__c = String.valueOf(elementoFacturado.Mes_de_facturacion__c) + String.valueOf(elementoFacturado.Anio_de_facturacion__c) +ca.Id;
            
            insert elementoFacturado;
        }
        

    }

    static testMethod void initScheduleRefacturacionSACYL() {            
        Test.starttest();
        
                    
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);    

        List<Integracion_Facturacion__c> elementToReprocess = [SELECT Id, Reprocesar__c From Integracion_Facturacion__c Limit 1];
        elementToReprocess[0].Reprocesar__c = true;
        update elementToReprocess[0];
        
        Test.stopTest();
   }
}