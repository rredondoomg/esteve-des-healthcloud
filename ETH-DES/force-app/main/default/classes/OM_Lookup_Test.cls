/**
 * @description       : 
 * @author            : GO TEAM
 * @group             : 
 * @last modified on  : 02-15-2022
 * @last modified by  : dcastellon@omegacrmconsulting.com
**/
@isTest
public with sharing class OM_Lookup_Test {

    @isTest 
    static void testSObjects() {
        Test.startTest();
        List<OM_Lookup.SObjectWrapper> objs = OM_Lookup.getSalesforceObjects();
        Test.stopTest();
        System.assertNotEquals(0, objs.size());
    }

    @isTest 
    static void testResultsSOQL() {
        Test.startTest();
        OM_Lookup.getResults('Account', 'Name,CreatedDate', '001', 'Id != null', false,'4');
        OM_Lookup.getResults('Account', 'Name,CreatedDate', null, 'Id != null', false,'4');
        Test.stopTest();
    }

    @isTest 
    static void testResultsSOSL() {
        Test.startTest();
        OM_Lookup.getResults('Account', 'Name', '001', null, true,'4');
        OM_Lookup.getResults('Account', 'Name,CreatedDate,AccountSource,Parent.Name', '001', null, true,'4');
        Test.stopTest();
    }
}