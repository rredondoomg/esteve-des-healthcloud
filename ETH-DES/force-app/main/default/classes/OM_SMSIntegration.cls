/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 08-19-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
public with sharing class OM_SMSIntegration implements Queueable, Database.AllowsCallouts{
    
    public static String ENDPOINT = 'https://api.lleida.net/sms/v2/HTTP/1.1';
    
    public static Id saTRD = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByName().get('Visita TRD').getRecordTypeId();
    public static Id saPharmate = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByName().get('Visita Pharmate').getRecordTypeId();
    
    Map<Account, ServiceAppointment> accountsToNotify = new Map<Account, ServiceAppointment>();
    
    List<SMS_Setting__mdt> smsSettings = [SELECT Id, Endpoint__c, MasterLabel, Mensaje__c, User__c, Password__c, Active_SMS__c, TRD__c, Pharmate__c  FROM SMS_Setting__mdt];
    
    public OM_SMSIntegration(Map<Account, ServiceAppointment> accountsToNotify) {
        this.accountsToNotify = accountsToNotify;
    }
    
    public void execute(QueueableContext context) {
        Map<String,Map<String,ServiceAppointment>> bodysByEndpoint = generateHttpBodys(this.accountsToNotify);
        processBodys(bodysByEndpoint);
        if(this.accountsToNotify.size() > 0 ){
            System.enqueueJob(new OM_SMSIntegration(this.accountsToNotify));
        }
    }
    
    public void processBodys(Map<String,Map<String,ServiceAppointment>> bodysByEndpoint){
        List<ServiceAppointment> saToUpdate = new List<ServiceAppointment>();
        for(String key :bodysByEndpoint.keyset()){
            Map<String,ServiceAppointment> bodysBySa = bodysByEndpoint.get(key);
            System.debug('MAPA NUEVO   ### '+bodysBySa);
            for(String body : bodysBySa.keySet()){
                HttpResponse response = connectWithWS(body, key);
                System.debug('response CODE  ### '+response.getStatusCode());
                if(response.getStatusCode() == 200){
                    ServiceAppointment successSa = bodysBySa.get(body);
                    if(!Test.isRunningTest()){
                        successSa.SMS_Visita_Programada_Enviado__c =  true;
					}
                    
                    saToUpdate.add(successSa);
                    
                }
            }
        }
        if(!saToUpdate.isEmpty() && !Test.isRunningTest()){
            update saToUpdate;
        }
    }
    
    public HttpResponse connectWithWS(String body, String endpoint){
        System.debug('endpoint: ' + endpoint);
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpoint);
        request.setHeader('Accept','application/json');
        request.setHeader('Content-Type','application/x-www-form-urlencoded');
        request.setBody(body);
        request.setMethod('POST');
        HttpResponse response = http.send(request);
        return response;
    }
    
    public Map<String,Map<String,ServiceAppointment>> generateHttpBodys (Map<Account, ServiceAppointment> accountsToNotify){
        Map<String,Map<String,ServiceAppointment>> bodysByEndpoints = new Map<String,Map<String,ServiceAppointment>>();
        List<String> bodys = new List<String>();
        Map<String,ServiceAppointment> bodysBySa = new Map<String,ServiceAppointment>();
        Integer limitCount = 0;
        String enpoint;
        String user;
        String password;
        for(Account key : this.accountsToNotify.keySet()){
            ServiceAppointment sa= accountsToNotify.get(key);
            String mdtKey = accountsToNotify.get(key).Status+'-'+accountsToNotify.get(key).Tipo_lugar__c+'-'+accountsToNotify.get(key).Lugar_visita__c;
            SMS_Setting__mdt smsSetting = getSMSConfiguration(smsSettings, mdtKey);
            endpoint = !test.isRunningTest() ? smsSetting.Endpoint__c : 'https://api.lleida.net/sms/v2/HTTP/1.1';
            user = !Test.isRunningTest() ? smsSetting.User__c : 'esteveteijin';
            password = !Test.isRunningTest() ? smsSetting.Password__c : 'T.A_ij,i-8';
            limitCount ++;
            if(smsSetting.Active_SMS__c || test.isRunningTest()){
                SMSWrapper wrapperBody = new SMSWrapper();
                Sms sms = new Sms();
                sms.user = user;
                sms.password = password;
                sms.src = 'Esteve';
                Dst dst= new Dst();
                dst.Num = new List<String>{'+34'+key.PersonMobilePhone};
                sms.Dst = dst;
                sms.txt = sa != null ? generateMessageText(sa, smsSetting.Mensaje__c, smsSetting.TRD__c, smsSetting.Pharmate__c) : '';
                wrapperBody.Sms = sms;
                bodysBySa.put(JSON.serialize(wrapperBody), accountsToNotify.get(key)); 
            }
            
            accountsToNotify.remove(key);
            
            if(limitCount > 190){
                break;
            }
        }
        bodysByEndpoints.put(endpoint, bodysBySa);
        
        return bodysByEndpoints;
    }
    
    private static String generateMessageText(ServiceAppointment sa, String message, String trd, String pharmate){
        String messageupdated = message != null ? message : '';
        Date saDate = sa.SchedStartTime != null ? sa.SchedStartTime.date() : System.today();
        String dateStr = DateTime.newInstance(saDate.year(),saDate.month(),saDate.day()).format('d-MM-YYYY');
        String hours = sa.SchedStartTime != null ? String.valueOf(sa.SchedStartTime.hour()) : String.valueOf(System.now().hour());
        Integer saMin = sa.SchedStartTime != null ? sa.SchedStartTime.minute() : System.now().minute();
        String minutes = saMin < 10 ? '0' + String.valueOf(saMin) : String.valueOf(saMin);
        String timeStr = hours+':'+minutes;
        String ownerName = sa.Owner.Name != null ? sa.Owner.Name : '';
        
        if(messageUpdated.contains('Vdate')){
            messageUpdated = messageUpdated.replace('Vdate', dateStr);
        }
        if(messageUpdated.contains('Vtime')){
            messageUpdated = messageUpdated.replace('Vtime', timeStr);
        }
        if(messageUpdated.contains('Vdoc')){
            messageUpdated = messageUpdated.replace('Vdoc', ownerName);
        }
        if(messageUpdated.contains('Vad')){
            if(sa.RecordTypeId == saPharmate){
                messageUpdated = messageUpdated.replace('Vad', pharmate);               
            }else if(sa.RecordTypeId == saTRD){
                messageUpdated =  messageUpdated.replace('Vad', trd);
            }
        }
        
        return messageUpdated;
    }
    
    private static SMS_Setting__mdt getSMSConfiguration(List<SMS_Setting__mdt> settings, String name){
        SMS_Setting__mdt settingToReturn = new SMS_Setting__mdt();
        
        for(SMS_Setting__mdt setting: settings){
            settingToReturn = name == setting.MasterLabel ? setting : settingToReturn; 
        }
        
        return settingToReturn;
    }
    
    public class SMSWrapper {
        
        public Sms sms;
    }
    public class Dst {
        public List<String> num;
    }
    
    public class Sms {
        public String user;
        public String password;
        public String src;
        public Dst dst;
        public String txt;
    }
}