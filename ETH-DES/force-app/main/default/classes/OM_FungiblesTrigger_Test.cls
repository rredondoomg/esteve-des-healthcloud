/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 09-05-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
@isTest
public with sharing class OM_FungiblesTrigger_Test {
    @TestSetup
    static void makeData(){
        ETH_DataFactory.setUpOmegaContent();
        //crear un usuario
        User userProfesional = ETH_DataFactory.createUser('usertestethprofesional@test.com');
        insert userProfesional;

        User user = [SELECT Id FROM User WHERE LastName = 'Omega' LIMIT 1];

        System.runAs(user){
            MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
            Test.setMock(HttpCalloutMock.class, multimock);

            Test.startTest();

            Account account = ETH_DataFactory.createAccountWithAddress('Spain', 'Granada', 'Periodista Fernando Gomez de la Cruz', '18014', 'Granada');
            insert account;

            OperatingHours operatingHours = ETH_DataFactory.createOperatingHours();
            insert operatingHours;

            //timeslots de operating hours
            ETH_DataFactory.createTimeSlots(operatingHours.Id, 5, 23);

            ServiceTerritory serviceTerritoryPadre = ETH_DataFactory.createServiceTerritory(null, operatingHours.Id, 'TRD', null);
            insert serviceTerritoryPadre;

            ServiceTerritory serviceTerritoryHijo = ETH_DataFactory.createServiceTerritory(account.ShippingPostalCode, operatingHours.Id, 'TRD', serviceTerritoryPadre.Id);
            insert serviceTerritoryHijo;

            //crear un assigned resource a cada usuario
            ServiceResource serviceResourceProfesional = ETH_DataFactory.createServiceResourceProfesional(userProfesional.Id);
            insert serviceResourceProfesional;

            //crear los services territories member
            ServiceTerritoryMember serviceTerritoryMemberProfesional = ETH_DataFactory.createServiceTerritoryMember('Técnico', serviceResourceProfesional.Id, serviceTerritoryPadre.Id, operatingHours.Id);
            insert serviceTerritoryMemberProfesional;

            //repetimos proceso para CRETA y box
            ServiceTerritory serviceTerritoryCRETA = ETH_DataFactory.createServiceTerritory(account.ShippingPostalCode, operatingHours.Id, 'Creta',  null);
            insert serviceTerritoryCRETA;

            Asset asset = ETH_DataFactory.createAsset(account.Id);
            insert asset;

            ServiceResource serviceResourceBox = ETH_DataFactory.createServiceResourceBox(asset.Id);
            insert serviceResourceBox;

            ServiceTerritoryMember serviceTerritoryMemberBox = ETH_DataFactory.createServiceTerritoryMember('', serviceResourceBox.Id, serviceTerritoryCRETA.Id, operatingHours.Id);
            insert serviceTerritoryMemberBox;

            //alta
            Worktype worktypeAlta = ETH_DataFactory.createWorktype('Alta', 'TRD', 'Presencial', 'Domicilio');
            insert worktypeAlta;
            //domicilio
            Worktype worktypeSeguimientoDomicilio = ETH_DataFactory.createWorktype('Seguimiento', 'TRD', 'Presencial', 'Domicilio');
            insert worktypeSeguimientoDomicilio;

            Worktype worktypeBaja = ETH_DataFactory.createWorktype('Fin', 'TRD', 'Presencial', 'Domicilio');
            insert worktypeBaja;


            Worktype worktypeSeguimiento = ETH_DataFactory.createWorktype('Seguimiento', 'TRD', 'Virtual', 'CRETA');
            insert worktypeSeguimiento;


            TokenETH__c oCStoken = new TokenETH__c();
            oCStoken.url__c = 'http://93.90.29.156:9002';
            oCStoken.username__c = 'omega';
            oCStoken.password__c = '44V-7J6RE-oLs&n.b7';
            oCStoken.token__c = '11111111111';
            insert oCStoken;

            // Product2 equipoTest = [SELECT Id, Name FROM Product2 WHERE Name = 'Equipo TEST'];

            Product2 equipoTest = new Product2();
            equipoTest.Name = 'Equipo Test';
            equipoTest.ProductCode = 'EquipoTest';
            equipoTest.Marca__C = 'EquipoTest';
            equipoTest.Modelo__c = 'EquipoTest';
            equipoTest.Identificador__c = 'EquipoTest123';
            equipoTest.ProductCode = 'EquipoTest';
            insert equipoTest;

        }
        Test.stopTest();
    }

    private static void crearTratamientosVisitaEquiposYFungibles(String serviceAppId, String tratamientoId, String pacienteId, Integer numeroPrincipales){
        Visita_Tratamiento__c visTrat = new Visita_Tratamiento__c();
        visTrat.Name = 'TratamientoTest';
        visTrat.Service_Appointment__c = serviceAppId;
        visTrat.Tratamiento__c = tratamientoId;
        visTrat.Paciente__c = pacienteId;

        insert visTrat;

        Product2 prodFungible = new Product2();
        prodFungible.Name = 'fungibleSuministrado';
        prodFungible.ProductCode = '34';
        prodFungible.Marca__c = 'MarcafungibleSum';
        prodFungible.Modelo__c = 'ModelofungibleSum';
        prodFungible.Identificador__c = 'IdentFung';
        prodFungible.familia_fun__c = 'AC';

        prodFungible.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Fungibles').getRecordTypeId();
        insert prodFungible;

        Fungibles_Sum__c fungibleSum = new Fungibles_Sum__c();
        fungibleSum.Marca_modelo_fungible__c = prodFungible.Id;
        fungibleSum.Case__c = tratamientoId;
        fungibleSum.Paciente__c = pacienteId;
        fungibleSum.Visita_tratamiento__c = visTrat.Id;
        fungibleSum.Cantidad__c = 1;
        fungibleSum.Pendiente__c = true;
        insert fungibleSum;


        Account account = [SELECT Id FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'TRD');
        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, null, 'TRD');
        tratamiento.fam_aprt_comp__c = 'ACPAP';
        insert tratamiento;
        ca.Tratamiento__c = tratamiento.Id;
        insert ca;

        WorkType worktypeAlta = [SELECT Name, Id, EstimatedDuration FROM WorkType WHERE Name LIKE '%Alta%' LIMIT 1];
        User userProfesional = [SELECT Id FROM User WHERE Email='usertestethprofesional@test.com' LIMIT 1];

        ServiceAppointment sapp = new ServiceAppointment(
            DurationType = 'Minutes',
            RecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('Visita_TRD').getRecordTypeId(),
            Status = 'Prevista',
            ParentRecordId = account.Id,
            WorkTypeId = worktypeAlta.Id,
            Duration = worktypeAlta.EstimatedDuration,
            Tratamiento__c = ca.Id,
            Linea_negocio__c = 'TRD',
            Tipo_lugar__c = 'Virtual',
            Fecha_Realizada__c = Date.today()
        );
        insert sapp;

        Visita_Tratamiento__c visTra2 = new Visita_Tratamiento__c();
        visTra2.Name = 'TratamientoTest';
        visTra2.Service_Appointment__c = sapp.Id;
        visTra2.Tratamiento__c = ca.Id;
        visTra2.Paciente__c = account.Id;

        insert visTra2;

    }


    @isTest
    public static void enviarFungibleAtlas(){
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);
        
        Account account = [SELECT Id FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'TRD');
        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, null, 'TRD');
        insert tratamiento;
        ca.Tratamiento__c = tratamiento.Id;
        insert ca;

        WorkType worktypeAlta = [SELECT Name, Id, EstimatedDuration FROM WorkType WHERE Name LIKE '%Alta%' LIMIT 1];
        User userProfesional = [SELECT Id FROM User WHERE Email='usertestethprofesional@test.com' LIMIT 1];

        ServiceAppointment sapp = new ServiceAppointment(
            DurationType = 'Minutes',
            RecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('Visita_TRD').getRecordTypeId(),
            Status = 'Prevista',
            ParentRecordId = account.Id,
            WorkTypeId = worktypeAlta.Id,
            Duration = worktypeAlta.EstimatedDuration, 
            Tratamiento__c = ca.Id,
            Linea_negocio__c = 'TRD',
            Tipo_lugar__c = 'Virtual',
            Fecha_Realizada__c = Datetime.now()
        );
        insert sapp;

        crearTratamientosVisitaEquiposYFungibles(sapp.Id, ca.Id, account.Id, 0);


        Test.startTest();
        System.runAs(userProfesional){
            transcursoCita(sapp);
        }
        Test.stopTest();

        List<Fungibles_sum__c> fungSum = [SELECT Id FROM Fungibles_Sum__c LIMIT 1];
        fungSum[0].Pendiente__c = false;
        update fungSum[0];

    }

    private static void transcursoCita(ServiceAppointment cita){
        cita.Status = 'Programada';
        cita.SchedStartTime = DateTime.newInstance(Date.today(), Time.newInstance(9,0,0,0));
        update cita;
        cita.Status = 'En Curso';
        cita.ActualStartTime = DateTime.now();
        update cita;
    }

    @isTest
    public static void enviarFungibleHermes(){
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);
        
        Account account = [SELECT Id FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'TRD');
        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, null, 'TRD');
        insert tratamiento;
        ca.Tratamiento__c = tratamiento.Id;
        insert ca;

        WorkType worktypeAlta = [SELECT Name, Id, EstimatedDuration FROM WorkType WHERE Name LIKE '%Alta%' LIMIT 1];
        User userProfesional = [SELECT Id FROM User WHERE Email='usertestethprofesional@test.com' LIMIT 1];

        ServiceAppointment sapp = new ServiceAppointment(
            DurationType = 'Minutes',
            RecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('Visita_TRD').getRecordTypeId(),
            Status = 'Prevista',
            ParentRecordId = account.Id,
            WorkTypeId = worktypeAlta.Id,
            Duration = worktypeAlta.EstimatedDuration, 
            Tratamiento__c = ca.Id,
            Linea_negocio__c = 'TRD',
            Tipo_lugar__c = 'Virtual'
        );
        insert sapp;

        crearTratamientosVisitaEquiposYFungibles(sapp.Id, ca.Id, account.Id, 0);


        Test.startTest();
        System.runAs(userProfesional){
            transcursoCita(sapp);
        }
        Test.stopTest();

        List<Fungibles_sum__c> fungSum = [SELECT Id FROM Fungibles_Sum__c LIMIT 1];
        fungSum[0].Pendiente__c = false;
        update fungSum[0];

    }
}