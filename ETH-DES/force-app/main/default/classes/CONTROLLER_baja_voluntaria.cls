/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 08-22-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
public with sharing class CONTROLLER_baja_voluntaria {
	
	//public String idvisita { get; set; }
	public String idprescripcion { get; set; }
	public String nombrePaciente { get; set; }
	public String nifPaciente { get; set; }
	public String cipPaciente { get; set; }
	public String city { get; set; }
	public String lan { get; set; }
	public String fecha { get; set; }
	public String tratamiento { get; set; }
    /*public String aparatosHeader {get; set;}
    public String codeHeader {get; set;}*/
	
	public static List<String> pContent {get;set;}
	public static String mainTitle {get;set;}
    
    public static String firmaPaciente {get;set;}
    //public static String firmaAsistencial {get;set;}
    public static String fechaTitle {get;set;}
	
	public CONTROLLER_baja_voluntaria() {
		//Cambia el botón a Prescripción (04/11/2019)
		//idvisita = ApexPages.currentPage().getParameters().get('idvisita');	
		idprescripcion = ApexPages.currentPage().getParameters().get('idprescripcion');
		
		nifPaciente = ApexPages.currentPage().getParameters().get('nifPaciente');

		lan = ApexPages.currentPage().getParameters().get('lan');
        city = ApexPages.currentPage().getParameters().get('city');		

		//Cambia el botón a Prescripción (04/11/2019)
		/*Visita__c v = [select Id, Prescripcion__c, Prescripcion__r.Paciente__r.Name, Prescripcion__r.Paciente__r.IdSanitario__c, Prescripcion__r.cod_tra__r.Name 
						from Visita__c where Id =: idvisita limit 1];*/


						// Prescripcion__c p = [select Id, Paciente__r.Name, Paciente__r.IdSanitario__c, cod_tra__r.Name 
						// from Prescripcion__c where Id =: idprescripcion limit 1];
		ServiceAppointment p = [select Id, Account.Name, AccountId, Tratamiento__r.Cod_Iden__c, Tratamiento__r.Tratamiento__r.Name
						from ServiceAppointment where Id =: idprescripcion limit 1];
		
		//Cambia el botón a Prescripción (04/11/2019)
		/*nombrePaciente = v.Prescripcion__r.Paciente__r.Name;

		cipPaciente = v.Prescripcion__r.Paciente__r.IdSanitario__c;
		*/
		nombrePaciente = p.Account.Name;

		// cipPaciente =p.Account.IdSanitario__c;
		cipPaciente =p.Tratamiento__r.Cod_Iden__c;

		fecha = Datetime.now().format('dd/MM/YYYY');

		//Cambia el botón a Prescripción (04/11/2019)	
		//tratamiento = v.Prescripcion__r.Tratamiento__r.Name;
		tratamiento = p.Tratamiento__r.Tratamiento__r.Name;
        
         
        //firmaPaciente = (lan == 'CAT') ? 'Signatura Pacient o Representant' : Firma Paciente o Representante
        
        //equipos = getEquipos(v);
        
		getContentText(city,lan,tratamiento);

	}

	private String getMes(Integer mes){
		Map<Integer, String> mapaMeses = new Map<Integer, String>();
			mapaMeses.put(1, 'Enero');
			mapaMeses.put(2, 'Febrero');
			mapaMeses.put(3, 'Marzo');
			mapaMeses.put(4, 'Abril');
			mapaMeses.put(5, 'Mayo');
			mapaMeses.put(6, 'Junio');
			mapaMeses.put(7, 'Julio');
			mapaMeses.put(8, 'Agosto');
			mapaMeses.put(9, 'Septiembre');
			mapaMeses.put(10, 'Octubre');
			mapaMeses.put(11, 'Noviembre');
			mapaMeses.put(12, 'Diciembre');

		return mapaMeses.get(mes);
	}

	public void getContentText(String ciudad, String lan, String tratamiento){
		Integer counterP = 0;
        String cmQuery = 'SELECT ';
        for(String contentField:  Schema.getGlobalDescribe().get('Baja_Voluntaria__mdt').getDescribe().fields.getMap().keySet()){
            cmQuery += contentField + ', ';
            
            if(contentField.containsIgnoreCase('Paragraph')){
            	counterP ++;
            }
        }

		cmQuery = cmQuery.removeEnd(', ');
        cmQuery += ' FROM Baja_Voluntaria__mdt WHERE Language__c = \'' + lan + '\'';

		List<Baja_Voluntaria__mdt> bvList= new List<Baja_Voluntaria__mdt>();
		bvList = Database.query(cmQuery);

		pContent = new List<String>();

		if(!bvList.isEmpty()){
			Baja_Voluntaria__mdt bDoc = bvList.get(0);

			mainTitle = bDoc.Main_Title__c;
            firmaPaciente = bDoc.Firma_Paciente__c;
    		/*firmaAsistencial = bDoc.Firma_Personal__c;
            aparatosHeader = bDoc.AparatosHeader__c;
        	codeHeader = bDoc.CodeHeader__c;*/
            fechaTitle = bDoc.Fecha__c;

			for(Integer i = 1; i <= counterP; i++){
                String fString = 'Paragraph_'+i+'__c';
                if(!String.isBlank((String)bDoc.get(fString))){
                    
                    String pString = (String)bDoc.get(fString);
					 
                    pString = pString.replace('[S_0]', '<span style="text-decoration: underline;">');
                    pString = pString.replace('[S_1]', '</span>');
                    pString = pString.replace('[B_0]', '<span style="font-weight: bold;">');
                    pString = pString.replace('[B_1]', '</span>');
                    
                        
                    pString = pString.replace('[PATIENT_NAME]', nombrePaciente);
                    pString = pString.replace('[PATIENT_NIF]', nifPaciente);
					pString = pString.replace('[PATIENT_CIP]', cipPaciente);
                    pString = pString.replace('[NOMBRE_TRATAMIENTO]',tratamiento);
                    pString = pString.replace('[CITY]', ciudad);
                    pString = pString.replace('[TODAY_DAY]', String.valueOf(Date.today().day()));
                    pString = pString.replace('[TODAY_MONTH]', getMes(Date.today().month()));
                    pString = pString.replace('[TODAY_YEAR]', String.valueOf(Date.today().year()));
                    pContent.add(pString);
                }
            }
		}
	}
    
    
}