/**
* Test class for CustomLookupLwcController class
* @author: Sergio Díaz Real
* @created: 09/06/2022
* @modified: 09/06/2022
*/
@isTest
public with sharing class CustomLookupLwcController_Test {
    @TestSetup
    static void makeData(){
        ETH_DataFactory.setUpOmegaContent();
        //crear un usuario
        User userProfesional = ETH_DataFactory.createUser('usertestethprofesional@test.com');
        insert userProfesional;

        User user = [SELECT Id FROM User WHERE LastName = 'Omega' LIMIT 1];

        System.runAs(user){
            MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
            Test.setMock(HttpCalloutMock.class, multimock);

            Test.startTest();

            Account account = ETH_DataFactory.createAccountWithAddress('Spain', 'Granada', 'Periodista Fernando Gomez de la Cruz', '18014', 'Granada');
            insert account;

            OperatingHours operatingHours = ETH_DataFactory.createOperatingHours();
            insert operatingHours;

            //timeslots de operating hours
            ETH_DataFactory.createTimeSlots(operatingHours.Id, 5, 23);

            ServiceTerritory serviceTerritoryPadre = ETH_DataFactory.createServiceTerritory(null, operatingHours.Id, 'TRD', null);
            insert serviceTerritoryPadre;

            ServiceTerritory serviceTerritoryHijo = ETH_DataFactory.createServiceTerritory(account.ShippingPostalCode, operatingHours.Id, 'TRD', serviceTerritoryPadre.Id);
            insert serviceTerritoryHijo;

            //crear un assigned resource a cada usuario
            ServiceResource serviceResourceProfesional = ETH_DataFactory.createServiceResourceProfesional(userProfesional.Id);
            insert serviceResourceProfesional;

            //crear los services territories member
            ServiceTerritoryMember serviceTerritoryMemberProfesional = ETH_DataFactory.createServiceTerritoryMember('Profesional TRD', serviceResourceProfesional.Id, serviceTerritoryPadre.Id, operatingHours.Id);
            insert serviceTerritoryMemberProfesional;

            //repetimos proceso para CRETA y box
            ServiceTerritory serviceTerritoryCRETA = ETH_DataFactory.createServiceTerritory(account.ShippingPostalCode, operatingHours.Id, 'Creta',  null);
            insert serviceTerritoryCRETA;

            Asset asset = ETH_DataFactory.createAsset(account.Id);
            insert asset;

            ServiceResource serviceResourceBox = ETH_DataFactory.createServiceResourceBox(asset.Id);
            insert serviceResourceBox;

            ServiceTerritoryMember serviceTerritoryMemberBox = ETH_DataFactory.createServiceTerritoryMember('', serviceResourceBox.Id, serviceTerritoryCRETA.Id, operatingHours.Id);
            insert serviceTerritoryMemberBox;

            //alta
            Worktype worktypeAlta = ETH_DataFactory.createWorktype('Alta', 'TRD', 'Presencial', 'Domicilio');
            insert worktypeAlta;
            //domicilio
            Worktype worktypeSeguimientoDomicilio = ETH_DataFactory.createWorktype('Seguimiento', 'TRD', 'Presencial', 'Domicilio');
            insert worktypeSeguimientoDomicilio;

            Worktype worktypeBaja = ETH_DataFactory.createWorktype('Fin', 'TRD', 'Presencial', 'Domicilio');
            insert worktypeBaja;


            Worktype worktypeSeguimiento = ETH_DataFactory.createWorktype('Seguimiento', 'TRD', 'Virtual', 'CRETA');
            insert worktypeSeguimiento;


            TokenETH__c oCStoken = new TokenETH__c();
            oCStoken.url__c = 'http://93.90.29.156:9002';
            oCStoken.username__c = 'omega';
            oCStoken.password__c = '44V-7J6RE-oLs&n.b7';
            oCStoken.token__c = '11111111111';
            insert oCStoken;

            // Product2 equipoTest = [SELECT Id, Name FROM Product2 WHERE Name = 'Equipo TEST'];

            Product2 equipoTest = new Product2();
            equipoTest.Name = 'Equipo Test';
            equipoTest.ProductCode = 'EquipoTest';
            equipoTest.Marca__C = 'EquipoTest';
            equipoTest.Modelo__c = 'EquipoTest';
            equipoTest.Identificador__c = 'EquipoTest123';
            equipoTest.ProductCode = 'EquipoTest';
            insert equipoTest;

        }
        Test.stopTest();
    }

    private static void transcursoCita(ServiceAppointment cita){
        cita.Status = 'Programada';
        cita.SchedStartTime = DateTime.newInstance(Date.today(), Time.newInstance(9,0,0,0));
        update cita;
        cita.Status = 'En Curso';
        cita.ActualStartTime = DateTime.now();
        update cita;
        // cita.Status = 'Completada';
        // cita.ActualEndTime = DateTime.now();
        // cita.Firma_baja__c = true;
        // cita.Firma_consentimiento__c = true;
        // cita.Firma_Visita__c = true;
        // update cita;
    }

    private static void crearTratamientosVisitaEquiposYFungibles(String serviceAppId, String tratamientoId, String pacienteId, Integer numeroPrincipales){
        Visita_Tratamiento__c visTrat = new Visita_Tratamiento__c();
        visTrat.Name = 'TratamientoTest';
        visTrat.Service_Appointment__c = serviceAppId;
        visTrat.Tratamiento__c = tratamientoId;
        visTrat.Paciente__c = pacienteId;

        insert visTrat;

        CodeSet codSet = new CodeSet();
        codSet.Name = 'GENERICO';
        codSet.Code = 'GENERICO'; 
        codSet.IsActive = true; 
        codSet.IsCustomCode = false; 
        codSet.IsPrimary = true; 
        // codSet.CodeSetKey = 345342343;
        insert codSet;

        Product2 prodEquipo = new Product2();
        prodEquipo.Name = 'EquipoSuministrado';
        prodEquipo.ProductCode = 'EquipoSuministrado';
        prodEquipo.Marca__c = 'MarcaEquipoSum';
        prodEquipo.Modelo__c = 'ModeloEquipoSum';
        prodEquipo.Identificador__c = 'Ident';
        prodEquipo.familia_aprt__c = 'ACPAP';
        // prodEquipo.familia_fun__c = 'AC';
        prodEquipo.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Aparatos').getRecordTypeId();
        insert prodEquipo;

        Equipos_Sum__c equipoSum = new Equipos_Sum__c();
        equipoSum.Name = 'EquipoSuministrado';
        equipoSum.Marca_modelo_equipo__c = prodEquipo.Id;
        equipoSum.Tratamiento__c = tratamientoId;
        equipoSum.PersonAccount__c = pacienteId;
        equipoSum.Visita_tratamiento__c = visTrat.Id;
        equipoSum.Cod_eth__c = '0000021311';
        equipoSum.Num_Fabricante__c = '12531';
        if(numeroPrincipales >= 1){
            equipoSum.Equipo_principal__c = true;
        }
        insert equipoSum;

        Datetime dt = Datetime.now();
        String formattedDate = dt.format('dd/MM/yy');
        Datetime now = datetime.now();

        CareObservation lecturaEquipoSum = new CareObservation();
        lecturaEquipoSum.Name = 'Lectura ' + formattedDate;
        lecturaEquipoSum.Equipos_suministrados__c = equipoSum.Id;
        lecturaEquipoSum.Visita_tratamiento__c = visTrat.Id;//Seguimiento tratamiento
        lecturaEquipoSum.EffectiveDateTime = now;//Fecha y hora actual
        lecturaEquipoSum.NumericValue = 20;
        lecturaEquipoSum.ObservationStatus = 'Registered'; 
        lecturaEquipoSum.CodeId = codSet.Id; //OBLIGATORIO METER GENERICO
        lecturaEquipoSum.IsMonitoredRemotely = false;
        lecturaEquipoSum.Lect_anterior__c = 0;
        lecturaEquipoSum.ObservedSubjectId = pacienteId; //Paciente
        lecturaEquipoSum.Consumo__c = 0;
        
        insert lecturaEquipoSum;

        CareObservation lecturaEquipoSum3 = new CareObservation();
        lecturaEquipoSum3.Name = 'Lectura ' + formattedDate;
        lecturaEquipoSum3.Equipos_suministrados__c = equipoSum.Id;
        lecturaEquipoSum3.Visita_tratamiento__c = visTrat.Id;//Seguimiento tratamiento
        lecturaEquipoSum3.EffectiveDateTime = now;//Fecha y hora actual
        lecturaEquipoSum3.NumericValue = 20;
        lecturaEquipoSum3.ObservationStatus = 'Registered'; 
        lecturaEquipoSum3.CodeId = codSet.Id; //OBLIGATORIO METER GENERICO
        lecturaEquipoSum3.IsMonitoredRemotely = false;
        lecturaEquipoSum3.Lect_anterior__c = 0;
        lecturaEquipoSum3.ObservedSubjectId = pacienteId; //Paciente
        lecturaEquipoSum3.Consumo__c = 0;
        
        insert lecturaEquipoSum3;




        Product2 prodEquipo2 = new Product2();
        prodEquipo2.Name = 'EquipoSuministrado2';
        prodEquipo2.ProductCode = 'EquipoSuministrado2';
        prodEquipo2.Marca__c = 'MarcaEquipoSum2';
        prodEquipo2.Modelo__c = 'ModeloEquipoSum2';
        prodEquipo2.Identificador__c = 'Ident2';
        prodEquipo2.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Aparatos').getRecordTypeId();
        insert prodEquipo2;

        Equipos_Sum__c equipoSum2 = new Equipos_Sum__c();
        equipoSum2.Name = 'EquipoSuministrado2';
        equipoSum2.Marca_modelo_equipo__c = prodEquipo2.Id;
        equipoSum2.Tratamiento__c = tratamientoId;
        equipoSum2.PersonAccount__c = pacienteId;
        equipoSum2.Visita_tratamiento__c = visTrat.Id;
        equipoSum2.Cod_eth__c = '0000021312';
        equipoSum2.Num_Fabricante__c = '12532';
        if(numeroPrincipales >=2){
            equipoSum2.Equipo_principal__c = true;
        }
        insert equipoSum2;

        CareObservation lecturaEquipoSum2 = new CareObservation();
        lecturaEquipoSum2.Name = 'Lectura ' + formattedDate;
        lecturaEquipoSum2.Equipos_suministrados__c = equipoSum2.Id;
        lecturaEquipoSum2.Visita_tratamiento__c = visTrat.Id;//Seguimiento tratamiento
        lecturaEquipoSum2.EffectiveDateTime = now;//Fecha y hora actual
        lecturaEquipoSum2.NumericValue = 20;
        lecturaEquipoSum2.ObservationStatus = 'Registered'; 
        lecturaEquipoSum2.CodeId = codSet.Id; //OBLIGATORIO METER GENERICO
        lecturaEquipoSum2.IsMonitoredRemotely = false;
        lecturaEquipoSum2.Lect_anterior__c = 0;
        lecturaEquipoSum2.ObservedSubjectId = pacienteId; //Paciente
        lecturaEquipoSum2.Consumo__c = 0;
        
        insert lecturaEquipoSum2;

        CareObservation lecturaEquipoSum4 = new CareObservation();
        lecturaEquipoSum4.Name = 'Lectura2 ' + formattedDate;
        lecturaEquipoSum4.Equipos_suministrados__c = equipoSum2.Id;
        lecturaEquipoSum4.Visita_tratamiento__c = visTrat.Id;//Seguimiento tratamiento
        lecturaEquipoSum4.EffectiveDateTime = now;//Fecha y hora actual
        lecturaEquipoSum4.NumericValue = 20;
        lecturaEquipoSum4.ObservationStatus = 'Registered'; 
        lecturaEquipoSum4.CodeId = codSet.Id; //OBLIGATORIO METER GENERICO
        lecturaEquipoSum4.IsMonitoredRemotely = false;
        lecturaEquipoSum4.Lect_anterior__c = 0;
        lecturaEquipoSum4.ObservedSubjectId = pacienteId; //Paciente
        lecturaEquipoSum4.Consumo__c = 0;
        
        insert lecturaEquipoSum4;


        Product2 prodFungible = new Product2();
        prodFungible.Name = 'fungibleSuministrado';
        prodFungible.ProductCode = 'fungibleSuministrado';
        prodFungible.Marca__c = 'MarcafungibleSum';
        prodFungible.Modelo__c = 'ModelofungibleSum';
        prodFungible.Identificador__c = 'IdentFung';
        prodFungible.familia_fun__c = 'AC';

        prodFungible.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Fungibles').getRecordTypeId();
        insert prodFungible;

        Fungibles_Sum__c fungibleSum = new Fungibles_Sum__c();
        //fungibleSum.Name = 'fungibleSuministrado';
        fungibleSum.Marca_modelo_fungible__c = prodFungible.Id;
        fungibleSum.Case__c = tratamientoId;
        fungibleSum.Paciente__c = pacienteId;
        fungibleSum.Visita_tratamiento__c = visTrat.Id;
        fungibleSum.Cantidad__c = 1;
        insert fungibleSum;


        //Lecturas anteriores
        Account account = [SELECT Id FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'TRD');
        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, null, 'TRD');
        tratamiento.fam_aprt_comp__c = 'ACPAP';
        insert tratamiento;
        ca.Tratamiento__c = tratamiento.Id;
        insert ca;

        WorkType worktypeAlta = [SELECT Name, Id, EstimatedDuration FROM WorkType WHERE Name LIKE '%Alta%' LIMIT 1];
        User userProfesional = [SELECT Id FROM User WHERE Email='usertestethprofesional@test.com' LIMIT 1];

        ServiceAppointment sapp = new ServiceAppointment(
            DurationType = 'Minutes',
            RecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('Visita_TRD').getRecordTypeId(),
            Status = 'Prevista',
            ParentRecordId = account.Id,
            WorkTypeId = worktypeAlta.Id,
            Duration = worktypeAlta.EstimatedDuration,
            Tratamiento__c = ca.Id,
            Linea_negocio__c = 'TRD'
        );
        insert sapp;

        Visita_Tratamiento__c visTra2 = new Visita_Tratamiento__c();
        visTra2.Name = 'TratamientoTest';
        visTra2.Service_Appointment__c = sapp.Id;
        visTra2.Tratamiento__c = ca.Id;
        visTra2.Paciente__c = account.Id;

        insert visTra2;



        Product2 prodEquipo5 = new Product2();
        prodEquipo5.Name = 'EquipoSuministrado';
        prodEquipo5.ProductCode = 'EquipoSuministrado';
        prodEquipo5.Marca__c = 'MarcaEquipoSum2';
        prodEquipo5.Modelo__c = 'ModeloEquipoSum2';
        prodEquipo5.Identificador__c = 'Ident3';
        prodEquipo5.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Aparatos').getRecordTypeId();
        insert prodEquipo5;

        Equipos_Sum__c equipoSum5 = new Equipos_Sum__c();
        equipoSum5.Name = 'EquipoSuministrado';
        equipoSum5.Marca_modelo_equipo__c = prodEquipo5.Id;
        equipoSum5.Tratamiento__c = tratamientoId;
        equipoSum5.PersonAccount__c = pacienteId;
        equipoSum5.Visita_tratamiento__c = visTra2.Id;
        equipoSum5.Cod_eth__c = '0000021312';
        equipoSum5.Num_Fabricante__c = '12532';
        if(numeroPrincipales >=2){
            equipoSum5.Equipo_principal__c = true;
        }
        insert equipoSum5;

        CareObservation lecturaEquipoSum5 = new CareObservation();
        lecturaEquipoSum5.Name = 'Lectura ' + formattedDate;
        lecturaEquipoSum5.Equipos_suministrados__c = equipoSum5.Id;
        lecturaEquipoSum5.Visita_tratamiento__c = visTra2.Id;//Seguimiento tratamiento
        lecturaEquipoSum5.EffectiveDateTime = now;//Fecha y hora actual
        lecturaEquipoSum5.NumericValue = 20;
        lecturaEquipoSum5.ObservationStatus = 'Registered'; 
        lecturaEquipoSum5.CodeId = codSet.Id; //OBLIGATORIO METER GENERICO
        lecturaEquipoSum5.IsMonitoredRemotely = false;
        lecturaEquipoSum5.Lect_anterior__c = 0;
        lecturaEquipoSum5.ObservedSubjectId = pacienteId; //Paciente
        lecturaEquipoSum5.Consumo__c = 0;
        lecturaEquipoSum5.EffectiveDateTime = Datetime.now() - 3;
        
        insert lecturaEquipoSum5;

        CareObservation lecturaEquipoSum6 = new CareObservation();
        lecturaEquipoSum6.Name = 'Lectura ' + formattedDate;
        lecturaEquipoSum6.Equipos_suministrados__c = equipoSum5.Id;
        lecturaEquipoSum6.Visita_tratamiento__c = visTra2.Id;//Seguimiento tratamiento
        lecturaEquipoSum6.EffectiveDateTime = now;//Fecha y hora actual
        lecturaEquipoSum6.NumericValue = 20;
        lecturaEquipoSum6.ObservationStatus = 'Registered'; 
        lecturaEquipoSum6.CodeId = codSet.Id; //OBLIGATORIO METER GENERICO
        lecturaEquipoSum6.IsMonitoredRemotely = false;
        lecturaEquipoSum6.Lect_anterior__c = 0;
        lecturaEquipoSum6.ObservedSubjectId = pacienteId; //Paciente
        lecturaEquipoSum6.Consumo__c = 0;
        lecturaEquipoSum6.EffectiveDateTime = Datetime.now() - 3;
        
        insert lecturaEquipoSum6;

    }

    @isTest
    public static void fetchLookupDataServiceApp(){
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);
        
        Account account = [SELECT Id FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'TRD');
        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, null, 'TRD');
        insert tratamiento;
        ca.Tratamiento__c = tratamiento.Id;
        insert ca;

        WorkType worktypeAlta = [SELECT Name, Id, EstimatedDuration FROM WorkType WHERE Name LIKE '%Alta%' LIMIT 1];
        User userProfesional = [SELECT Id FROM User WHERE Email='usertestethprofesional@test.com' LIMIT 1];

        ServiceAppointment sapp = new ServiceAppointment(
            DurationType = 'Minutes',
            RecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('Visita_TRD').getRecordTypeId(),
            Status = 'Prevista',
            ParentRecordId = account.Id,
            WorkTypeId = worktypeAlta.Id,
            Duration = worktypeAlta.EstimatedDuration, 
            Tratamiento__c = ca.Id,
            Linea_negocio__c = 'TRD',
            Tipo_lugar__c = 'Virtual'
        );
        insert sapp;

        crearTratamientosVisitaEquiposYFungibles(sapp.Id, ca.Id, account.Id, 0);


        Test.startTest();
        System.runAs(userProfesional){
            transcursoCita(sapp);
        }
        Test.stopTest();

        
        CustomLookupLwcController.fetchLookupData('SA-', 'ServiceAppointment');

    }

    @isTest
    public static void fetchLookupDataProduct2(){
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);
        
        Account account = [SELECT Id FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'TRD');
        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, null, 'TRD');
        insert tratamiento;
        ca.Tratamiento__c = tratamiento.Id;
        insert ca;

        WorkType worktypeAlta = [SELECT Name, Id, EstimatedDuration FROM WorkType WHERE Name LIKE '%Alta%' LIMIT 1];
        User userProfesional = [SELECT Id FROM User WHERE Email='usertestethprofesional@test.com' LIMIT 1];

        ServiceAppointment sapp = new ServiceAppointment(
            DurationType = 'Minutes',
            RecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('Visita_TRD').getRecordTypeId(),
            Status = 'Prevista',
            ParentRecordId = account.Id,
            WorkTypeId = worktypeAlta.Id,
            Duration = worktypeAlta.EstimatedDuration, 
            Tratamiento__c = ca.Id,
            Linea_negocio__c = 'TRD',
            Tipo_lugar__c = 'Virtual'
        );
        insert sapp;

        crearTratamientosVisitaEquiposYFungibles(sapp.Id, ca.Id, account.Id, 0);


        Test.startTest();
        System.runAs(userProfesional){
            transcursoCita(sapp);
        }
        Test.stopTest();

        
        CustomLookupLwcController.fetchLookupData('fungibleSumi', 'Product2');

    }


    @isTest
    public static void fetchDefaultRecordServiceApp(){
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);
        
        Account account = [SELECT Id FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'TRD');
        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, null, 'TRD');
        insert tratamiento;
        ca.Tratamiento__c = tratamiento.Id;
        insert ca;

        WorkType worktypeAlta = [SELECT Name, Id, EstimatedDuration FROM WorkType WHERE Name LIKE '%Alta%' LIMIT 1];
        User userProfesional = [SELECT Id FROM User WHERE Email='usertestethprofesional@test.com' LIMIT 1];

        ServiceAppointment sapp = new ServiceAppointment(
            DurationType = 'Minutes',
            RecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('Visita_TRD').getRecordTypeId(),
            Status = 'Prevista',
            ParentRecordId = account.Id,
            WorkTypeId = worktypeAlta.Id,
            Duration = worktypeAlta.EstimatedDuration, 
            Tratamiento__c = ca.Id,
            Linea_negocio__c = 'TRD',
            Tipo_lugar__c = 'Virtual'
        );
        insert sapp;

        crearTratamientosVisitaEquiposYFungibles(sapp.Id, ca.Id, account.Id, 0);


        Test.startTest();
        System.runAs(userProfesional){
            transcursoCita(sapp);
        }
        Test.stopTest();

        List<ServiceAppointment> sApps = [SELECT Id FROM ServiceAppointment LIMIT 1];
        CustomLookupLwcController.fetchDefaultRecord(sApps[0].Id, 'ServiceAppointment');

    }

    @isTest
    public static void fetchDefaultRecordProduct2(){
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);
        
        Account account = [SELECT Id FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'TRD');
        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, null, 'TRD');
        insert tratamiento;
        ca.Tratamiento__c = tratamiento.Id;
        insert ca;

        WorkType worktypeAlta = [SELECT Name, Id, EstimatedDuration FROM WorkType WHERE Name LIKE '%Alta%' LIMIT 1];
        User userProfesional = [SELECT Id FROM User WHERE Email='usertestethprofesional@test.com' LIMIT 1];

        ServiceAppointment sapp = new ServiceAppointment(
            DurationType = 'Minutes',
            RecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('Visita_TRD').getRecordTypeId(),
            Status = 'Prevista',
            ParentRecordId = account.Id,
            WorkTypeId = worktypeAlta.Id,
            Duration = worktypeAlta.EstimatedDuration, 
            Tratamiento__c = ca.Id,
            Linea_negocio__c = 'TRD',
            Tipo_lugar__c = 'Virtual'
        );
        insert sapp;

        crearTratamientosVisitaEquiposYFungibles(sapp.Id, ca.Id, account.Id, 0);


        Test.startTest();
        System.runAs(userProfesional){
            transcursoCita(sapp);
        }
        Test.stopTest();

        List<Product2> prod = [SELECT Id FROM Product2 WHERE RecordType.DeveloperName = 'Fungibles' LIMIT 1];
        CustomLookupLwcController.fetchDefaultRecord(prod[0].Id, 'Product2');

    }
}