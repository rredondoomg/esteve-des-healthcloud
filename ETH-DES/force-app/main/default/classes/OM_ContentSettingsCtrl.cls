/**
 * @description       Controller for the Content Setting UI
 * @author            dcastellon@omegacrmconsulting.com
 * @group             Content settings
**/
public without sharing class OM_ContentSettingsCtrl {

    /**
    * @description Delete all the Content access data for the given settings
    * @author dcastellon@omegacrmconsulting.com | 01-27-2021 
    * @param settingsId 
    * @return Boolean 
    **/
    @AuraEnabled
    public static Boolean signOut(String settingsId){
        Database.delete(Database.query('SELECT Id FROM OM_ContentAccessData__c WHERE OM_Content_setting__c = \'' + String.escapeSingleQuotes(settingsId) + '\''));
        return true;
    }
    
    /**
    * @description Checks if the current user is logged in in the provider
    * @author OmegaCRM  | 05-25-2021 
    * @param settingsName The settings name from the form
    * @return Boolean If is logged or not
    **/
    @AuraEnabled
    public static Boolean checkLoginStatus(String settingsName){
        try {
            return OM_ContentService.getUserAccess(settingsName, UserInfo.getUserId()) != null;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    /**
    * @description Check if the typed condition is correct
    * @author dcastellon@omegacrmconsulting.com | 01-27-2021 
    * @param objectName 
    * @param condition 
    * @return Boolean 
    **/
    @AuraEnabled
    public static Boolean checkValidQuery(String objectName, String condition){
        try {
            String query = 'SELECT Id FROM ' + String.escapeSingleQuotes(objectName);
            query += String.isNotBlank(condition) ? ' WHERE ' + condition : '';
            query += ' LIMIT 0';
            Database.query(query);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


}