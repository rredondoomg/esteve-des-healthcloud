/**
 * @description        Main service class to perfom actions versus all supported providers
 * @author             OmegaCRM 
 * @group              Omega content integration
 * @last modified on   01-20-2021
 * @last modified by   OmegaCRM 
**/
public without sharing class OM_ContentService {

    /**
     * @description Shared constant
     */
    public static final String SHARED = 'SHARED';
    public static final String ACTION_CREATE_FOLDER = 'createFolder';
    public static final String ACTION_CREATE_FOLDER_PATH = 'createFolderPath';
    public static final String ACTION_GET_ITEM = 'getItem';
    public static final String ACTION_UPDATE_ITEM = 'updateItem';
    public static final String ACTION_DELETE_ITEM = 'deleteItem';
    public static final String ACTION_UPLOAD_ITEM = 'uploadItem';
    public static final String ACTION_LIST_ITEMS = 'listsItems';
    public static final String ACTION_DOWNLOAD_ITEM = 'downloadItem';


    /**
     * @description Inner class to model callouts internally and simplify retries
     */
    public class CalloutWrapper {
        public String endpoint;
        public String method = 'GET';
        public Map<String, String> headers;
        public String body = null;
        public Blob bodyBlob = null;
        public Boolean responseBlob = false;

        /**
        * @description Generates a HTTPRequest based on callout properties
        * @author OmegaCRM  | 02-21-2021 
        * @param accessToken The acces token to include in crendentials
        * @return HttpRequest The request
        **/
        public HttpRequest makeRequest(String accessToken) {
            HttpRequest request = new HttpRequest();
            request.setMethod(this.method);
            request.setEndpoint(this.endpoint);
            if(this.headers != null) {
                for(String header : this.headers.keySet()) {
                    if(this.headers.get(header) != null) {
                        request.setHeader(header, this.headers.get(header));
                    }
                }
            }
            request.setHeader('Accept', '*/*');
            request.setHeader('Authorization', 'Bearer ' + accessToken);
            if( this.body != null ) {
                request.setBody(this.body);
            }
            if( this.bodyBlob != null ) {
                request.setBodyAsBlob(this.bodyBlob);
            }

            return request;
        }
    }

    /**
    * @description Gets an enpoint for a provider and a service, replacing needed variables to work properly
    * @author OmegaCRM  | 01-20-2021 
    * @param settings The settings to use, from where the provider is obtained
    * @param service The desired service endpoint
    * @param content The content wrapper to work with. Used to replace some variables when needed
    * @return string The final endpoint 
    **/
    public static string getEndpoint(OM_ContentSetting__c settings, String service, OM_ContentWrapper content) {
        return OM_ContentUtils.getEndpoint(settings, service, content);
    }

    /**
    * @description Gets an enpoint for a provider and a service, replacing needed variables to work properly
    * @author OmegaCRM  | 01-20-2021 
    * @param settings The settings to use, from where the provider is obtained
    * @param service The desired service endpoint
    * @return string The final endpoint 
    **/
    public static string getEndpoint(OM_ContentSetting__c settings, String service) {
        return getEndpoint(settings, service, null);
    }

    /**
    * @description Gets all endpoints for a provider
    * @author OmegaCRM  | 01-20-2021 
    * @param settingsName The settings to use, from where the provider is obtained
    * @return Map<String, String> The final endpoints map by service for selected provider
    **/
    @AuraEnabled
    public static Map<String, String> getAllEndpoints(String settingsName) {
        OM_ContentSetting__c settings = getSettings(settingsName);
        return OM_ContentUtils.getAllEndpoints(settings);
    }

    /**
    * @description Gets the Drive list. Only for Microsoft connections
    * @author OmegaCRM  | 02-02-2021 
    * @param settingsName The settings name to use
    * @return String The SIte Id
    **/
    @AuraEnabled
    public static List<Map<String, Object>> listSharepointDrives(String settingsName) {
        // https://graph.microsoft.com/v1.0/sites/mlaso.sharepoint.com:/sites/omegadev03
        System.debug(LoggingLevel.ERROR, 'Get sharpoint drive list');
        OM_ContentSetting__c settings = getSettings(settingsName);
        OM_ContentIntegrationInterface integration = getIntegrationClass(settings.OM_Provider__c);
        String endpoint =  getEndpoint(settings, 'base') + '/drives?expand=list($expand=columns)';
        CalloutWrapper wrap = new CalloutWrapper();
        wrap.endpoint = endpoint;
        wrap.headers = new Map<String, String>{
            'Content-Type' => 'application/json'
        };
        String response = (String) integration.makeCallout(wrap, settings, null, null);

        response = response.replace('"list"', '"list_x"');

        OM_ContentMicrosoftIntegration.SharepointDrivesReponse driveResponse = (OM_ContentMicrosoftIntegration.SharepointDrivesReponse) JSON.deserialize(response, OM_ContentMicrosoftIntegration.SharepointDrivesReponse.class);
        

        Set<String> internalColumnNames = new Set<String>{
            '_ModerationComments',
            '_CheckinComment',
            'CheckoutUser',
            'CheckedOutTitle',
            'LinkCheckedOutTitle',
            'ContentType',
            'ContentTypeId',
            '_CopySource',
            'Created',
            'Created_x0020_Date',
            'Author',
            'Created_x0020_By',
            'Modified_x0020_By',
            'Edit',
            '_EditMenuTableEnd',
            '_EditMenuTableStart',
            'PermMask',
            'EncodedAbsUrl',
            'File_x0020_Size',
            'FileSizeDisplay',
            'File_x0020_Type',
            'GUID',
            '_HasCopyDestinations',
            'xd_ProgID',
            'HTML_x0020_File_x0020_Type',
            'ID',
            'CheckedOutUserId',
            'InstanceID',
            'IsCheckedoutToLocal',
            '_IsCurrentVersion',
            'xd_Signature',
            'FSObjType',
            '_Level',
            'Combine',
            'Modified',
            'Last_x0020_Modified',
            'Editor',
            'FileLeafRef',
            'LinkFilenameNoMenu',
            'LinkFilename',
            'BaseName',
            'Order',
            'owshiddenversion',
            'FileDirRef',
            'ProgId',
            'MetaInfo',
            'RepairDocument',
            'ScopeId',
            'SelectTitle',
            'SelectFilename',
            'ServerUrl',
            '_SharedFileIndex',
            'ParentLeafName',
            '_SourceUrl',
            'ParentVersionString',
            'TemplateUrl',
            'Title',
            'DocIcon',
            '_UIVersion',
            'UniqueId',
            'FileRef',
            '_UIVersionString',
            'VirusStatus',
            'WorkflowInstanceID',
            'WorkflowVersion',
            'ComplianceFlags',
            '_ComplianceTag',
            '_ComplianceTagWrittenTime',
            '_ComplianceTagUserId',
            '_IsRecord',
            '_CommentCount',
            '_LikeCount',
            '_DisplayName',
            'AppAuthor',
            'AppEditor',
            'ComplianceAssetId',
            'MediaServiceAutoTags',
            '_ExtendedDescription',
            'ItemChildCount',
            'FolderChildCount',
            '_ComplianceFlags'
        };
        System.debug(driveResponse);
        List<Map<String, Object>> toReturn = new List<Map<String, Object>>();
        for(OM_ContentMicrosoftIntegration.SharepointDrive drive : driveResponse.value) {
         System.debug(drive);

            Map<String, Object> driveMapEntry = new Map<String, Object>();
            driveMapEntry.put('name', drive.name);
            driveMapEntry.put('id', drive.id);

            List<Map<String, Object>> driveListColumns = new List<Map<String, Object>>();
            for(OM_ContentMicrosoftIntegration.Column col : drive.list_x.columns) {
                driveListColumns.add(new Map<String, Object> {
                    'name' => col.name,
                    'displayName' => col.displayName,
                    'custom' => !internalColumnNames.contains(col.name)
                });

            }
            driveMapEntry.put('columns', driveListColumns);
            toReturn.add(driveMapEntry);

        }
        System.debug(toReturn);

        return toReturn;
    }

    /**
    * @description Gets the Sharepoint site ID. Only for Sharepoint connections
    * @author OmegaCRM  | 02-02-2021 
    * @param settingsName The settings name to use
    * @param siteUrl The URL of the site
    * @return String The SIte Id
    **/
    @AuraEnabled
    public static String getSharepointSiteId(String settingsName, String siteUrl) {
        // https://graph.microsoft.com/v1.0/sites/mlaso.sharepoint.com:/sites/omegadev03
        System.debug(LoggingLevel.ERROR, 'Get sharpoint Site id');
        OM_ContentSetting__c settings = getSettings(settingsName);
        OM_ContentIntegrationInterface integration = getIntegrationClass(settings.OM_Provider__c);
        String finalUrl = siteUrl.replace('https://', '').replace('sharepoint.com/', 'sharepoint.com:/');
        String endpoint = 'https://graph.microsoft.com/v1.0/sites/' + finalUrl;
        CalloutWrapper wrap = new CalloutWrapper();
        wrap.endpoint = endpoint;
        wrap.headers = new Map<String, String>{
            'Content-Type' => 'application/json'
        };
        String response = (String) integration.makeCallout(wrap, settings, null, null);
        System.debug(LoggingLevel.ERROR, response);
        Map<String, Object> data = (Map<String, Object>) JSON.deserializeUntyped(response);
        return String.valueOf(data.get('id'));
    }


    /**
    * @description Obtains the concrete implmentation class to use based on the provider
    * @author OmegaCRM  | 01-20-2021 
    * @param provider The desired provider
    * @return OM_ContentIntegrationInterface The concrete implementation
    **/
    public static OM_ContentIntegrationInterface getIntegrationClass(String provider) {
        return OM_ContentUtils.getIntegrationClass(provider);
    }

    /**
    * @description Return the settings record to use base on its unique name
    * @author OmegaCRM  | 01-20-2021 
    * @param settingsName The unique name for the setting
    * @return OM_ContentSetting__c The record matching the name
    **/
    @AuraEnabled
    public static OM_ContentSetting__c getSettings(String settingsName) {
        try {
            return [SELECT
                Id,
                OM_Allow_folder_creation__c,
                OM_Allow_navigation__c,
                OM_Allow_upload_files__c,
                OM_Allow_edit__c,
                OM_Allow_filters__c,
                OM_Allow_search__c,
                OM_Allow_selection__c,
                OM_Allow_deletion__c,
                OM_Allow_tagging__c,
                OM_Allow_AI_suggestions__c,
                OM_Columns__c,
                OM_Is_personal_account__c,
                OM_Auth_type__c,
                OM_Use_personal_drive__c,
                OM_Author_role_name__c,
                OM_Client_id__c,
                OM_Client_secret__c,
                OM_Discovery_docs__c,
                OM_Env_ltng_url__c,
                OM_Env_vf_url__c,
                OM_Folder_mapping__c,
                OM_Initial_folder_id__c,
                OM_Action_file_click__c,
                OM_Key__c,
                OM_Name__c,
                OM_NLP_salience_threshold__c,
                OM_Tags_displayed__c,
                OM_Provider__c,
                OM_Drive_id__c,
                OM_Redirect_uri__c,
                OM_Scope__c,
                OM_Show_inline_actions__c,
                OM_Site_id__c,
                OM_GCS_bucket_name__c,
                OM_Tenant__c,
                OM_Use_roles__c,
                OM_Use_content_version__c,
                (SELECT Id, Name, OM_As_column__c, OM_Property_name__c, OM_Salesforce_field_name__c, OM_Editable__c, OM_Filterable__c FROM Content_custom_properties__r)
                FROM OM_ContentSetting__c
                WHERE OM_Name__c = :settingsName
                WITH SECURITY_ENFORCED
                LIMIT 1];
        } catch (Exception e) {
            throw new OM_ContentException('The settings does not exist **' + e.getMessage());
        }
    }

    /**
    * @description Get the settings for an specific object / record
    * @author OmegaCRM  | 02-19-2021 
    * @param settingsName The settings to use
    * @param recordId The record Id
    * @return OM_ContentSetting__c 
    **/
    @AuraEnabled
    public static Map<String, Object> getSettingsForObject(String settingsName, String recordId) {
        OM_ContentSetting__c settings = null;
        OM_ContentSetting__c mainSettings = getSettings(settingsName);
        List<OM_ContentCustomProperty__c> customProperties = mainSettings.Content_custom_properties__r != null && mainSettings.Content_custom_properties__r.size() > 0 ? mainSettings.Content_custom_properties__r : new List<OM_ContentCustomProperty__c>();

        if (recordId != null) {
            String objectName = Id.valueOf(recordId).getSObjectType().getDescribe().getName();
    
            // TODO find by criteria
            try {
                List<OM_ContentSetting__c> objectsettings = [SELECT
                    Id,
                    OM_Criteria__c,
                    OM_Drive_id__c,
                    OM_Allow_folder_creation__c,
                    OM_Allow_navigation__c,
                    OM_Allow_upload_files__c,
                    OM_Allow_edit__c,
                    OM_Allow_filters__c,
                    OM_Allow_search__c,
                    OM_Allow_selection__c,
                    OM_Allow_deletion__c,
                    OM_Allow_tagging__c,
                    OM_Allow_AI_suggestions__c,
                    OM_Columns__c,
                    OM_Use_personal_drive__c,
                    OM_Initial_folder_id__c,
                    OM_Initial_folder_path__c,
                    OM_Action_file_click__c,
                    OM_Is_personal_account__c,
                    OM_Auth_type__c,
                    OM_Author_role_name__c,
                    OM_Folder_mapping__c,
                    OM_Key__c,
                    OM_Name__c,
                    OM_NLP_salience_threshold__c,
                    OM_Tags_displayed__c,
                    OM_Scope__c,
                    OM_Show_inline_actions__c,
                    OM_GCS_bucket_name__c,
                    OM_Use_roles__c,
                    OM_Use_content_version__c,
                    OM_Parent_setting__r.OM_Name__c,
                    OM_Parent_setting__r.OM_Provider__c,
                    OM_Parent_setting__r.OM_Drive_id__c,
                    OM_Parent_setting__r.OM_Site_id__c,
                    (SELECT Id, Name, OM_As_column__c, OM_Property_name__c, OM_Salesforce_field_name__c, OM_Editable__c, OM_Filterable__c FROM Content_custom_properties__r)
                FROM OM_ContentSetting__c
                WHERE OM_Object__c = :objectName
                WITH SECURITY_ENFORCED];
                
                settings = findCorrectSettingForRecord(objectsettings, objectName, recordId);
                System.debug('Setting final obtained (criteria) ' + settings.OM_Criteria__c);

                if (settings != null) {
                    // Find record and replace variables
                    // API 51 --> sObject record = Database.query('SELECT FIELDS(ALL) FROM ' + objectName + ' WHERE Id =: recordId ');
                    sObject record = Database.query('SELECT ' + String.join(new List<String>(OM_SchemaUtils.getFieldsSObjectMap(String.escapeSingleQuotes(objectName)).keySet()), ',') +' FROM ' + String.escapeSingleQuotes(objectName) + ' WHERE Id =: recordId ');
                    
                    if(settings.OM_Initial_folder_id__c != null && settings.OM_Initial_folder_id__c.contains('{!record') ) {
                        // Replace with record field vale
                        String fieldToAccess = settings.OM_Initial_folder_id__c.replace('{!record.', '').replace('}', '');
                        settings.OM_Initial_folder_id__c = String.valueOf(record.get(fieldToAccess));
                    } else {
                        if (settings.OM_Initial_folder_path__c != null && settings.OM_Initial_folder_path__c.contains('{!record')) {
                            String fieldToAccess = settings.OM_Initial_folder_path__c.replace('{!record.', '').replace('}', '');
                            settings.OM_Initial_folder_id__c = 'root:/' + String.valueOf(record.get(fieldToAccess)) + ':/';
                        }
                    }

                    // some fields only exists on parent, so DUMP
                    settings.OM_Auth_type__c = mainSettings.OM_Auth_type__c;
                    // Apply the global properties for this
                    if (settings.Content_custom_properties__r == null || settings.Content_custom_properties__r.size() == 0) {
                        //customProperties = mainSettings.Content_custom_properties__r;
                    } else {
                        customProperties.addAll(settings.Content_custom_properties__r);
                    }

                }
            } catch( Exception e) {
                System.debug(LoggingLevel.ERROR, 'No existing match for this object: ' + e.getMessage());
            }
    
        }

        if( settings == null) {
            settings = mainSettings;
        }

        Map<String, Object> toRet = new Map<String, Object>();
        toRet.put('settings', settings);
        toRet.put('customProperties', customProperties);
        return toRet;
    }

    public static OM_ContentSetting__c findCorrectSettingForRecord(List<OM_ContentSetting__c> settings, String objectName, String recordId) {
        OM_ContentSetting__c result = null;
        System.debug('Finding correct setting for this record ' + recordId);
        for(OM_ContentSetting__c sett : settings) {
            if(sett.OM_Criteria__c != null) {
                System.debug('Checking criteria  ' + sett.OM_Criteria__c);

                List<sObject> res = Database.query('SELECT Id FROM ' + objectName + ' WHERE ' + sett.OM_Criteria__c + ' AND Id = :recordId');
                System.debug('Results with criteria  ' + res);
                if(res != null && res.size() > 0) {
                    result = sett;
                    break;
                }
            } else {
                System.debug('No criteria found. Assignning base  ' + sett);
                result = sett;
            }
        }

        return result;
    }

    /**
    * @description Get the JSON access data in a app connection settings for a given user. If the connnection is of type "Shared", userid is ignored
    * @author OmegaCRM  | 01-20-2021 
    * @param settingsName The unique name of the settings to use
    * @param userId The Salesforce user id
    * @return Map<String, String> The access data in Map format
    **/
    public static Map<String, String> getUserAccess(String settingsName, String userId) {
        OM_ContentSetting__c settings  = getSettings(settingsName);

        String uId = settings.OM_Auth_type__c == 'Shared' ? null : userId;
        List<OM_ContentAccessData__c> accData = [SELECT Id, OM_JSON_access_data__c FROM OM_ContentAccessData__c WHERE OM_User__c = :uId AND OM_Content_setting__c = :settings.Id WITH SECURITY_ENFORCED LIMIT 1];

        if(accData.size() == 1) {
            return (Map<String, String>) JSON.deserialize(accData.get(0).OM_JSON_access_data__c, Map<String, String>.class);
        }

        return null;
    }

    /**
    * @description Get the access for the current user
    * @author OmegaCRM  | 01-20-2021 
    * @param settingsName The unique name of the settings to use 
    * @return Map<String, String> 
    **/
    @AuraEnabled
    public static Map<String, String> getCurrentUserAccess(String settingsName) {
        return getUserAccess(settingsName, UserInfo.getUserId());
    }

    /**
    * @description Removes access for the application for the current user
    * @author OmegaCRM  | 03-09-2021 
    * @param settingsName The settings to remove access for
    * @return Boolean True if deletion was successfull, false otherwise
    **/
    @AuraEnabled
    public static Boolean removeCurrentUserAccess(String settingsName) {
        List<OM_ContentAccessData__c> accData = [SELECT Id FROM OM_ContentAccessData__c WHERE OM_User__c = :UserInfo.getUserId() AND OM_Content_setting__r.OM_Name__c = :settingsName WITH SECURITY_ENFORCED LIMIT 1];
        if(accData != null && !accData.isEmpty() && OM_ContentAccessData__c.SObjectType.getDescribe().isDeletable()) {
            delete accData;
            return true;
        }
        return false;
    }

    /**
    * @description Upserts OM_ContentAccessData__c record for a user
    * @author OmegaCRM  | 01-20-2021 
    * @param settings The settings to use
    * @param userid The Salesforce user ID
    * @param newAccess The JSON of the new access data obtained
    * @return OM_ContentAccessData__c 
    **/
    public static OM_ContentAccessData__c updateUserAccess(OM_ContentSetting__c settings, String userid, String newAccess) {
        String uId = settings.OM_Auth_type__c == 'Shared' ?  OM_ContentService.SHARED : userId;

        OM_ContentAccessData__c access = new OM_ContentAccessData__c(
            OM_Content_setting__c = settings.Id,
            OM_JSON_access_data__c = newAccess,
            OM_External_id__c = settings.Id + uId,
            OM_User__c = (uid != SHARED && settings.OM_Auth_type__c != 'Shared' ? uid : null)
        );
        
        if (OM_ContentAccessData__c.SObjectType.getDescribe().isCreateable() && OM_ContentAccessData__c.SObjectType.getDescribe().isUpdateable()) {
            upsert access OM_External_id__c;
        }
        
        return access;
    }

    /**
    * @description Upserts Google Drives files as Salesforce record
    * @author OmegaCRM
    * @param settingsName The custom metadata name to use
    * @param contents The list of contents to upsert
    * @return List<OM_Content__c> The upserted records
    **/
    @RemoteAction
    @AuraEnabled
    public static List<OM_Content__c> upsertContents(String recordId, String settingsName, List<OM_ContentWrapper> contents) {
        //OM_ContentSetting__c settings = getSettings(settingsName);
        //List<OM_ContentCustomProperty__c> mapping = settings.Content_custom_properties__r;

        Map<String, Object> sett = getSettingsForObject(settingsName, recordId);
        //OM_ContentSetting__c settings = (OM_ContentSetting__c) sett.get('settings');
        List<OM_ContentCustomProperty__c> mapping = (List<OM_ContentCustomProperty__c>) sett.get('customProperties');


        List<OM_Content__c> records = new List<OM_Content__c>();
        System.debug(LoggingLevel.ERROR, 'Contents in parameter ' + contents);
        // List<OM_Authorship__c> authorships = new List<OM_Authorship__c>();
        for(OM_ContentWrapper w : contents) {
            records.add(w.toSalesforceRecord(mapping));
            // if(w.appProperties != null && w.appProperties.containsKey('author') && w.appProperties.get('author') != null && w.appProperties.get('author') != ''){
            //     String aId = w.appProperties.get('author');
            //     authorships.add(new OM_Authorship__c(
            //         OM_Author__c = aId, 
            //         OM_Content__r = new OM_Content__c(OM_GDrive_Id__c = w.id),
            //         OM_External_Id__c = w.id+'_'+aId
            //     ));
            // }
        }
        System.debug(LoggingLevel.ERROR, 'Contents a Salesforce records ');
        System.debug(LoggingLevel.ERROR, records);

        if (OM_Content__c.SObjectType.getDescribe().isCreateable() && OM_Content__c.SObjectType.getDescribe().isUpdateable()) {
            upsert records OM_GDrive_Id__c;
        }

        //Upsert authors if any
        // if(!authorships.isEmpty()) {
        //     upsert authorships OM_External_Id__c;
        // }
        return records;
    }

    
    /**
    * @description Deletes OM_Content__c records based on wrappers
    * @author OmegaCRM  | 02-19-2021 
    * @param settingsName The settings to use
    * @param contents The list of content to delete
    * @return Boolean If records has been deleted
    **/
    @RemoteAction
    @AuraEnabled
    public static Boolean deleteContents(String settingsName, List<OM_ContentWrapper> contents) {
        // OM_ContentSetting__c settings = getSettings(settingsName);
        List<OM_Content__c> records = new List<OM_Content__c>();
        System.debug(LoggingLevel.ERROR, 'Contents in parameter ' + contents);
        for(OM_ContentWrapper w : contents) {
            records.add(w.findSalesforceRecord(null));
        }
        Boolean deleted = false;
        if(!records.isEmpty() && Schema.sObjectType.OM_Content__c.isDeletable()) {
            delete records;
            deleted = true;
        }
        return deleted;
    }

    

    /**
    * @description Obtains an stores the access token for a user from an Oauth flow
    * @author OmegaCRM  | 01-20-2021 
    * @param settingsName The unique name of the settings to use
    * @param authCode The authorization code from auth callback
    * @param userid The Salesforce user ID
    * @return String The JSON access data
    **/
    public static String getAccessToken(String settingsName, String authCode, String userid) {
        System.debug(LoggingLevel.ERROR, 'Call provider auth endpoint');
        OM_ContentSetting__c settings = getSettings(settingsName);
        OM_ContentIntegrationInterface integration = getIntegrationClass(settings.OM_Provider__c);
        String accessData = integration.getToken(settings, authCode);

        if(accessData.contains('access_token')) {
            updateUserAccess(settings, userid, accessData);
            return accessData;
        } else {
            Map<String, Object> respjson = (Map<String, Object> ) JSON.deserializeUntyped(accessData);
            String error = respjson.containsKey('error_description') ? String.valueOf(respjson.get('error_description')) :  String.valueOf(respjson.get('error'));
            throw new OM_ContentException('OAuth error ' + error);
        }
    }

    /**
    * @description Refreshes and stores a new access token for a user
    * @author OmegaCRM  | 01-20-2021 
    * @param settingsName The unique name of the settings to use
    * @param userid The Salesforce user ID
    * @return String The JSON access data
    **/
    @AuraEnabled
    public static String refreshToken(String settingsName, String userid) {
        String uid = userid == null ? UserInfo.getUserId() : userid;
        System.debug(LoggingLevel.ERROR, 'Call provider refresh token endpoint');
        OM_ContentSetting__c settings = getSettings(settingsName);
        OM_ContentIntegrationInterface integration = getIntegrationClass(settings.OM_Provider__c);
        String accessData = integration.refreshToken(settings, uid);
        System.debug(LoggingLevel.ERROR, 'Refreshed token ' + accessData);

        updateUserAccess(settings, uid, accessData);
        return accessData;
    }
    
    
    /**
    * @description Gets an endpoint to begin OAuth flow for a provider
    * @author OmegaCRM  | 01-20-2021 
    * @param settingsName The unique name of the settings to use
    * @param userid The Salesforce user ID
    * @return String The final auth enpoint
    **/
    @AuraEnabled
    public static String getAuthorizationEndpoint(String settingsName, String userid) {
        String uid = userid == null ? UserInfo.getUserId() : userid;
        OM_ContentSetting__c settings = getSettings(settingsName);
        return getIntegrationClass(settings.OM_Provider__c).getAuthorizationEndpoint(settings, uid);
    }

    /**
    * @description Method used as an entry point to run any integration action from Apex for any provider
    * @author OmegaCRM  | 02-19-2021 
    * @param actionName The action to execute
    * @param settingsName The settings to use
    * @param content The contetn wrapper (inf any)
    * @param fileData The file data (only for uploads)
    * @return OM_ContentWrapper The result as a wrapper
    **/
    @AuraEnabled
    public static OM_ContentWrapper runIntegrationAction(String actionName, String settingsName, OM_ContentWrapper content, Blob fileData) {
        OM_ContentSetting__c settings = getSettings(settingsName);
        OM_ContentIntegrationInterface integration = getIntegrationClass(settings.OM_Provider__c);
        if(actionName == ACTION_GET_ITEM) {
            return integration.getItem(settings, content);
        } else if(actionName == ACTION_CREATE_FOLDER) {
            return integration.createFolder(settings, content);
        } else if(actionName == ACTION_DELETE_ITEM) {
            return integration.deleteItem(settings, content);
        } else if(actionName == ACTION_UPDATE_ITEM) {
            return integration.updateItem(settings, content);
        } else if(actionName == ACTION_UPLOAD_ITEM) {
            return integration.uploadItem(settings, content, fileData);
        } else {
            return null;
        }
    }

    /**
    * @description Method used as an entry point to run any integration action from Apex for any provider
    * @author OmegaCRM  | 02-19-2021 
    * @param actionName The action to execute
    * @param settingsName The settings to use
    * @param content The contetn wrapper (inf any)
    * @param fileData The file data (only for uploads)
    * @return List<OM_ContentWrapper> The result as a list of wrappers
    **/
    @AuraEnabled
    public static List<OM_ContentWrapper> runIntegrationActionList(String actionName, String settingsName, OM_ContentWrapper content, Blob fileData) {
        OM_ContentSetting__c settings = getSettings(settingsName);
        OM_ContentIntegrationInterface integration = getIntegrationClass(settings.OM_Provider__c);
        if(actionName == ACTION_CREATE_FOLDER) {
            return new List<OM_ContentWrapper> { integration.createFolder(settings, content) };
        } else if(actionName == ACTION_CREATE_FOLDER_PATH) {
            return integration.createFolderPath(settings, content);
        } else if(actionName == ACTION_DELETE_ITEM) {
            return new List<OM_ContentWrapper> { integration.deleteItem(settings, content) };
        } else if(actionName == ACTION_UPDATE_ITEM) {
            return new List<OM_ContentWrapper> { integration.updateItem(settings, content) };
        } else if(actionName == ACTION_UPLOAD_ITEM) {
            return new List<OM_ContentWrapper> { integration.uploadItem(settings, content, fileData) };
        } else if(actionName == ACTION_LIST_ITEMS) {
            return integration.listItems(settings, content);
        } else {
            return null;
        }
    }
}