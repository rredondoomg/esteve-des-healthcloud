/**
 * @File Name          : OM_Calendar.cls
 * @Description        : 
 * @Author             : David López <sdlopez@omegacrmconsulting.com>
 * @Group              : 
 * @Last Modified By   : David López <sdlopez@omegacrmconsulting.com>
 * @Last Modified On   : 07-10-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/6/2020   David López <sdlopez@omegacrmconsulting.com>     Initial Version
**/
public with sharing class OM_Calendar {

    /**
     * Info obtained from an Event (eventClick / eventMouseEnter / eventMouseLeave / eventDrop / eventResize)
     */
    public class EventWrapper{
        @AuraEnabled public String id;                      // Unique identifier of an event.
        @AuraEnabled public String groupId;                 // Events that share a groupId will be dragged and resized together automatically.
        @AuraEnabled public Boolean allDay;                 // Determines if the event is shown in the “all-day” section of relevant views. In addition, if true the time text is not displayed with the event.
        @AuraEnabled public Datetime start;                 // When an event begins.
        @AuraEnabled public Datetime endEvent;              // When an event ends. Can be null if "allDay" event
        @AuraEnabled public String title;                   // The text that will appear on an event.
        @AuraEnabled public String url;                     // A URL that can be used to custom navigation
        @AuraEnabled public Boolean editable;               // The value overriding the "editable" setting for this specific event.
        @AuraEnabled public Boolean overlap;                // The value overriding the "eventOverlap" setting for this specific event. If false, prevents this event from being dragged/resized over other events.
        @AuraEnabled public String backgroundColor;         // The eventBackgroundColor override for this specific event.
        @AuraEnabled public String borderColor;             // The eventBorderColor override for this specific event.
        @AuraEnabled public String textColor;               // The eventTextColor override for this specific event.
        @AuraEnabled public String description;             // Custom Field to store any information that you want (Type of SObject, callback message...)

        public EventWrapper(){
            this.allDay = false;
            this.editable = false;
            this.overlap = false;
        }
    }
    
    /**
     * Info obtained from the set of Dates rendered
     */
    public class CurrentDateWrapper{
        @AuraEnabled public String type;                    // Type of view rendered (dayGridMonth, timeGridWeek, timeGridDay...)
        @AuraEnabled public String title;                   // Title text that is displayed at the top of the header (such as “September 2009” or “Sep 7 - 13 2009”).
        @AuraEnabled public Datetime activeStart;           // A Datetime with the first visible day. In month view, often before the 1st day of the month, [most months do not begin on the first day-of-week].
        @AuraEnabled public Datetime activeEnd;             // A Datetime with the last visible day.
        @AuraEnabled public Datetime currentStart;          // A Datetime with the start of the interval the view is trying to represent. In month view, this will be the first of the month.
        @AuraEnabled public Datetime currentEnd;            // A Datetime with the end of the interval the view is trying to represent

        public CurrentDateWrapper(){}
    }

    /**
     * Info obtained from a Date cell clicked
     */
    public class DateWrapper{
        @AuraEnabled public Datetime dateClicked;           // A Datetime for the clicked day/time
        @AuraEnabled public String dateStr;                 // An ISO8601 string representation of the date. Will have a time zone offset according to the calendar’s timeZone
        @AuraEnabled public Boolean allDay;                 // Whether the click happened on an all-day cell.

        public DateWrapper(){}
    }

    @AuraEnabled(cacheable=true)
    public static String getVisualforceDomain(String siteName){
        if (String.isNotBlank(siteName)){
            Site sfSite = [SELECT Id FROM Site WHERE Name = :siteName LIMIT 1];
		    return [SELECT SecureURL FROM SiteDetail WHERE DurableId = :sfSite.Id LIMIT 1].SecureURL;
        }else{
            return 'https://' + URL.getOrgDomainUrl().getHost().split('\\.')[0].toLowerCase() + '--c.' + [SELECT InstanceName FROM Organization LIMIT 1].InstanceName.toLowerCase() + '.visual.force.com';
        }
    }

    @testVisible
    private static void createWrappers(){
        new OM_Calendar.EventWrapper();
        new OM_Calendar.CurrentDateWrapper();
        new OM_Calendar.DateWrapper();
    }
}