public with sharing class OM_EnviarPedido {
    
    @AuraEnabled
    public static Map<Boolean, String> enviarPedidoAtlas(String recordId){
        system.debug('recordId: '+recordId);
        
        Map<Boolean, String> responseMap = new Map<Boolean, String>();

        if(recordId != null){
List<Pedido__c> pedidosConLineas = [SELECT Id, Name, Estado__c, Contenedor__r.Cod_con__c, LastModifiedBy.Codigo__c, Lote__c, Observaciones__c, Lineas__c, 
                                                    CreatedDate, Cliente__r.cod_cliente__c, Lote__r.cod_lot__c, Cliente__c,
                                                    (SELECT Id, Name, pedido__c, orden__c, Producto__c, Stock__c, Cantidad__c, Producto__r.ProductCode,
                                                    Producto__r.RecordType.DeveloperName 
                                                    FROM Pedidos_Detalle__r) 
                                                FROM Pedido__c 
                                                WHERE Id =: recordId];

            system.debug('Pedido y lineas vale: '+pedidosConLineas);


            if(!pedidosConLineas.isEmpty()){
                if(!pedidosConLineas[0].Pedidos_Detalle__r.isEmpty()){
                    WrapperPedido pedidoToSendAtlas = new WrapperPedido();
                    List<Lineas> lineasPedidos = new List<Lineas>();
                    for (Pedido__c pedido : pedidosConLineas) {

                        DateTime dT = pedidosConLineas[0].CreatedDate;
                        // Date datePedido = date.newinstance(dT.year(), dT.month(), dT.day());

                        DateTime myDateTime = DateTime.newInstance(dT.year(), dT.month(), dT.day(), dT.hour(), dT.minute(), dT.second());
                        String formatted = myDateTime.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS');

                        pedidoToSendAtlas.ped_contenedor = pedidosConLineas[0].Contenedor__r.Cod_con__c;
                        pedidoToSendAtlas.ped_usuario = pedidosConLineas[0].LastModifiedBy.Codigo__c;
                        // pedidoToSendAtlas.ped_fecha = String.valueOf(datePedido);
                        pedidoToSendAtlas.ped_fecha = String.valueOf(formatted);
                        
                        pedidoToSendAtlas.ped_titulo = pedidosConLineas[0].Name;
                        pedidoToSendAtlas.ped_observaciones = String.isNotBlank(pedidosConLineas[0].Observaciones__c) ? pedidosConLineas[0].Observaciones__c : null;
                        pedidoToSendAtlas.ped_cliente = pedidosConLineas[0].Cliente__c != null && pedidosConLineas[0].Cliente__r.cod_cliente__c != null ? Integer.valueOf(pedidosConLineas[0].Cliente__r.cod_cliente__c) : null;
                        pedidoToSendAtlas.ped_lote = pedidosConLineas[0].Lote__c != null && pedidosConLineas[0].Lote__r.cod_lot__c != null ? pedidosConLineas[0].Lote__r.cod_lot__c : null;
                        pedidoToSendAtlas.idsalesforce = pedidosConLineas[0].Id;
                        
                        for (pedido_det__c lineaPedido : pedido.Pedidos_Detalle__r) {
                            Lineas lineaPedidoAux = new Lineas();
                            lineaPedidoAux.ped_linea = Integer.valueOf(lineaPedido.Orden__c);
                            lineaPedidoAux.ped_tip_art = lineaPedido.Producto__r.RecordType.DeveloperName.equalsIgnoreCase('Aparatos') ? 'A' : 'F';
                            lineaPedidoAux.ped_id_art = Integer.valueOf(lineaPedido.Producto__r.ProductCode);
                            lineaPedidoAux.ped_cantidad = Integer.valueOf(lineaPedido.Cantidad__c);
                            lineaPedidoAux.idsalesforce = lineaPedido.Id;
                            lineasPedidos.add(lineaPedidoAux);
                        }
                        pedidoToSendAtlas.lineas = lineasPedidos;
                    }

                    responseMap = getToken(pedidoToSendAtlas);
                }else{
                    responseMap.put(true, 'Para enviar un pedido deben existir líneas del mismo.');
                }
            }else{
                responseMap.put(true, 'respuestaNull');
            }
        return responseMap;
        
        }else{
            responseMap.put(true, 'respuestaNull');
            return responseMap;
        }
            
    }



    public static Map<Boolean, String> getToken(WrapperPedido wrapperPedido){
        TokenETH__c oCStoken = [SELECT Id, url__c, username__c, password__c, token__c FROM TokenETH__c];

        String sUrl = oCStoken.url__c;
        String sUsername = oCStoken.username__c;
        String sPassword = oCStoken.password__c;
        String sToken = oCStoken.token__c;
        String sMethod = 'POST';

        Map<Boolean,String> responseMap = new Map<Boolean,String>();
        Map<String,Object> tokenResp = new Map<String,Object>();
        
        HttpRequest req = new HttpRequest();
        system.debug('URL TOKEN: '+sUrl + '/login');
        req.setEndpoint(sUrl + '/login');
        req.setMethod(sMethod);
        req.setHeader('Content-Type','application/json');

        Map<String, String> mapAuthen = new Map<String, String>();
        mapAuthen.put('username', sUsername);
        mapAuthen.put('password', sPassword);

        req.setBody(JSON.Serialize(mapAuthen));

        Http http = new Http();
        HTTPResponse res = new HTTPResponse();
        res = http.send(req);

        Pedido__c pedidoToUpdate = new Pedido__c();
        pedidoToUpdate.Id = wrapperPedido.idsalesforce;

        if (res.getStatusCode() == 200){
            system.debug('REsponde 200 el token.');
            tokenResp = (Map<String,Object>)JSON.deserializeUntyped(res.getBody());

            return enviarPedidoAtlas(String.valueOf(tokenResp.get('accessToken')), oCStoken, wrapperPedido);
        
        }else if(res.getStatusCode() == 401){
            system.debug('REsponde 400');
            responseMap.put(true, 'No se ha podido obtener el token de Atlas. Codigo '+res.getStatusCode());
            pedidoToUpdate.Envio_Pedido_OK__c = false;
            pedidoToUpdate.Error_Envio_Pedido__c = 'No se ha podido obtener el token de Atlas. Codigo '+res.getStatusCode();
        }else if(res.getStatusCode() == 500){
            system.debug('REsponde 500');
            responseMap.put(true, 'Error en la comunicación con Atlas. Codigo '+res.getStatusCode());
            pedidoToUpdate.Envio_Pedido_OK__c = false;
            pedidoToUpdate.Error_Envio_Pedido__c = 'Error en la comunicación con Atlas. Codigo '+res.getStatusCode();
        }

        update pedidoToUpdate;
        return responseMap;
    }

    public static Map<Boolean, String> enviarPedidoAtlas(String token, TokenETH__c oCStoken, WrapperPedido wrapperPedido){
        Map<Boolean,String> responseMap = new Map<Boolean,String>();
        
        String sUrl = oCStoken.url__c;
        String sUsername = oCStoken.username__c;
        String sPassword = oCStoken.password__c;
        String sToken = oCStoken.token__c;
        String sMethod = 'POST';

        HttpRequest req = new HttpRequest();
        HTTPResponse res = new HTTPResponse();

        system.debug('URL: '+sUrl + '/pedido-profesional');
        req.setEndpoint(sUrl + '/pedido-profesional');
        // req.setEndpoint('https://testvanguardsergio.free.beeceptor.com');
        
        req.setMethod(sMethod);
        req.setHeader('Content-Type','application/json');
        req.setHeader('Authorization', 'Bearer ' + token);

        system.debug('Body que mando '+JSON.Serialize(wrapperPedido));
        req.setBody(JSON.Serialize(wrapperPedido));
        
        system.debug('LLAMADA A /pedido-profesional');
        Http http = new Http();
        res = http.send(req);

        Pedido__c pedidoToUpdate = new Pedido__c();
        pedidoToUpdate.Id = wrapperPedido.idsalesforce;

        WrapperAtlasResponse atlasResponse;
        if (res.getStatusCode() == 200){
            system.debug('Ccódigo '+res.getStatusCode()+'. Respuesta: '+res.getBody());

            atlasResponse = (WrapperAtlasResponse) JSON.deserialize(res.getBody(), WrapperAtlasResponse.class);
            if(atlasResponse.code == 200){
                responseMap.put(false, '');
                pedidoToUpdate.Envio_Pedido_OK__c = true;
                pedidoToUpdate.Error_Envio_Pedido__c = '';
            }else{
                if(atlasResponse.message.equalsIgnoreCase('Item already exists')){
                    responseMap.put(true, 'El pedido ya existe en Atlas.');
                    pedidoToUpdate.Envio_Pedido_OK__c = false;
                    pedidoToUpdate.Error_Envio_Pedido__c = 'Código: '+res.getStatusCode() + '. Mensaje: '+res.getBody();
                }else{
                    responseMap.put(true, atlasResponse.message);
                    pedidoToUpdate.Envio_Pedido_OK__c = false;
                    pedidoToUpdate.Error_Envio_Pedido__c = 'Código: '+res.getStatusCode() + '. Mensaje: '+res.getBody();
                }
            }
        }else{
            system.debug('Error con código '+res.getStatusCode()+'. Respuesta: '+res.getBody());
            // atlasResponse = (WrapperAtlasResponse) JSON.deserialize(res.getBody(), WrapperAtlasResponse.class);
            responseMap.put(true, 'Código: '+res.getStatusCode() + '. Mensaje: '+res.getBody());
            pedidoToUpdate.Envio_Pedido_OK__c = false;
            pedidoToUpdate.Error_Envio_Pedido__c = 'Código: '+res.getStatusCode() + '. Mensaje: '+res.getBody();
        }

        update pedidoToUpdate;
        
        return responseMap;

    }


     //Wrapper para mandar pedido
     public class WrapperPedido{
        public String ped_contenedor;
        public String ped_usuario;
        public String ped_fecha;
        public String ped_titulo;
        public String ped_observaciones;
        public Integer ped_cliente;
        public String ped_lote;
        public String idsalesforce;
        public List<Lineas> lineas;
    }
    

	public class Lineas {
        // public String id_pedido_padre;
		public Integer ped_linea;
		public String ped_tip_art;
		public Integer ped_id_art;
		public Integer ped_cantidad;
		public String idsalesforce;
	}

    public class WrapperAtlasResponse{
        public String success;
        public Decimal code;
        public String message;
    }
}