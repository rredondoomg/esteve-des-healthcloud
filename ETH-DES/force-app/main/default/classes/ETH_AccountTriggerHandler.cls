/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 05-06-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
public with sharing class ETH_AccountTriggerHandler {
    // To save 'Zonas Conflictivas'
    private static List<List<Geolocation>> zonasConflictivas;

    // Name of our SharePoint settings
    public static final String SETTINGS_NAME = 'sharepointComponent';
    
    // Wrapper Class to save latitude and longitude
    public class Geolocation{
        public Double lat;
        public Double lon;

        public Geolocation(Double lat, Double lon){
            this.lat = lat;
            this.lon = lon;
        }
    }

    /**
    * @description Global method to get the location and check if it is a 'Zona Conflictiva'
    * @author fizquierdo@omegacrmconsulting.com | 01-05-2022 
    * @param List<Id> accountIDs List of account id to update
    **/
    @future (callout=true)
    public static void setZonasConflictivas(List<Id> accountIDs){
        // Get the accounts from the IDs
        List<Account> accountsToUpdate = new List<Account>([SELECT ShippingCity, ShippingStreet, ShippingCountry, ShippingPostalCode, ShippingLatitude, ShippingLongitude, ZonaConflictiva__c FROM Account WHERE Id IN :accountIDs]);

        // Get the 'Zonas Conflictiva' just in case there is a change in the map
        getZonasConflictivas();

        for(Account account: accountsToUpdate) {
            // Get the location (latitude and longitude)
            // Ya no hay Google Maps, se calculan por flow con geocode
            // Geolocation location = getGeolocation(account);
            // Save the coordinates
            // account.ShippingLatitude = Double.valueOf(location.lat);
            // account.ShippingLongitude = Double.valueOf(location.lon);

            //Check if the location is a 'Zona Conflictiva'
            if(ETH_AccountTriggerHandler.checkZonaConflictiva(account)){
                account.ZonaConflictiva__c = true;
            }
            else{
                account.ZonaConflictiva__c = false; 
            }
        }

        update accountsToUpdate;
    }

    /**
    * @description Method to get the location (latitude and longitude) of the account address
    * @author fizquierdo@omegacrmconsulting.com | 01-05-2022 
    * @param Account account 
    * @return Geolocation 
    **/
    // public static Geolocation getGeolocation(Account account){
    //     //Get API KEY from Custom Metadata
    //     Google_Maps_API__mdt api = [SELECT API_Key__c, URL__c FROM Google_Maps_API__mdt WHERE Label='Geolocation'];

    //     //Build the URL
    //     //Replace blank spaces with '+'
    //     String city = account.ShippingCity.replace(' ', '+');
    //     String street = account.ShippingStreet.replace(' ', '+');
    //     String country = account.ShippingCountry.replace(' ', '+');
    //     String postalCode = account.ShippingPostalCode;
    //     String url = api.URL__c + '?address=' + street + '+' + postalCode + '+' + city + '+' + country + '&key=' + api.API_Key__c;

    //     Http http = new Http();
    //     HttpRequest request = new HttpRequest();
    //     request.setMethod('GET');
    //     request.setEndpoint(url);

    //     //Send request and get the response
    //     HttpResponse hresp = http.send(request);
    //     //Parse to JSON
    //     JSONParser parser = JSON.createParser(hresp.getBody());
    //     Double lat;
    //     Double lng;
    //     //Iterate over the JSON
    //     while (parser.nextToken() != null) {
    //         if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && 
    //             (parser.getText() == 'lat')) {
    //             // Get the value.
    //             parser.nextToken();
    //             // Compute the grand total price for all invoices.
    //             lat = parser.getDoubleValue();
    //         }

    //         if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && 
    //             (parser.getText() == 'lng')) {
    //             // Get the value.
    //             parser.nextToken();
    //             // Compute the grand total price for all invoices.
    //             lng = parser.getDoubleValue();
    //             break;
    //             // No need to keep iterating
    //         }
    //     }
    //     // Return the coordinates
    //     return new Geolocation(Double.valueOf(lat), Double.valueOf(lng));
    // }


    /**
    * @description Method to get the KML file of the map and then save the 'Zonas Conflictivas' in a static variable (parseXML)
    * @author fizquierdo@omegacrmconsulting.com | 01-05-2022 
    **/
    public static void getZonasConflictivas(){
        // Get the URL
        Google_Maps_API__mdt api = [SELECT ZonasConflictivas__c FROM Google_Maps_API__mdt WHERE Label='Geolocation'];

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setMethod('GET');
        request.setEndpoint(api.ZonasConflictivas__c);

        //Restart the list
        zonasConflictivas = new List<List<Geolocation>>();

        HttpResponse hresp = http.send(request);

        Dom.Document doc = hresp.getBodyDocument();
        try{
            DOM.XmlNode rootNode=doc.getRootElement();
            // Iterate over the KML
            parseXML(rootNode);
        }catch(exception e){
            System.debug(e.getMessage());
        }
    }
    
    /**
    * @description Recursive function to find the coordinates of a 'Zona Conflictiva' in the KML file and then save it to the static variable 'zonasConflictivas'
    * @author fizquierdo@omegacrmconsulting.com | 01-05-2022 
    * @param DOM.XMLNode node 
    **/
    private static void parseXML(DOM.XMLNode node) {
        if (node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
            if(node.getName()=='coordinates'){
                // Remove the third coordinate (it is always 0) and split by '\n'
                List<String> pointsString = node.getText().trim().remove(',0').split('\n');
                // Create the new polygon: a list of 'Geolocation' (latitude, longitude)
                List<Geolocation> polygon = new List<Geolocation>();
                for(String point: pointsString){
                    List<String> coordinate = point.split(',');
                    polygon.add(new Geolocation(Double.valueOf(coordinate[1]),Double.valueOf(coordinate[0])));
                }
                // Add to the static variable
                zonasConflictivas.add(polygon);
            }
        }
        // Recursive call
        for (Dom.XMLNode child: node.getChildElements()) {
            parseXML(child);
        }
    }

    /**
    * @description Algorithm to check if a location is inside a polygon ('Zona Conflictiva')
    * @author fizquierdo@omegacrmconsulting.com | 01-05-2022 
    * @param List<Geolocation> polygon. Zona Conflictiva
    * @param Geolocation location 
    * @return Boolean 
    **/
    private static Boolean polygonContains(List<Geolocation> polygon, Geolocation location){
        //Source: https://pretagteam.com/question/how-to-detect-that-a-point-is-inside-a-polygon-using-google-maps
        Integer crossings = 0;
        Integer j;
        Geolocation a;
        Geolocation b;

        for(Integer i = 0; i < polygon.size(); i++){
            a = polygon.get(i);
            j = i + 1;
            if(j >= polygon.size()){
                j = 0;
            }
            b = polygon.get(j);
            if(rayCrossesSegment(location, a, b)){
                crossings++;
            } 
        }

        return (Math.mod(crossings, 2) == 1);
    }

    /**
    * @description Part of the algorithm to check if a location is inside a polygon
    * @author fizquierdo@omegacrmconsulting.com | 01-05-2022 
    * @param Geolocation point. Location
    * @param Geolocation a. Vertex of the polygon
    * @param Geolocation b. Vertex of the polygon
    * @return Boolean 
    **/
    private static Boolean rayCrossesSegment(Geolocation point, Geolocation a, Geolocation b){
        //Source: https://pretagteam.com/question/how-to-detect-that-a-point-is-inside-a-polygon-using-google-maps
        Double px = point.lon;
        Double py = point.lat;
        Double ax = a.lon;
        Double ay = a.lat;
        Double cx = b.lon;
        Double cy = b.lat;

        if (ay > cy) {
            ax = b.lon;
            ay = b.lat;
            cx = a.lon;
            cy = a.lat;
        }
        // alter longitude to cater for 180 degree crossings
        if (px < 0) {
            px += 360;
        }
        if (ax < 0) {
            ax += 360;
        }
        if (cx < 0) {
            cx += 360;
        }

        if (py == ay || py == cy){
            py += 0.00000001;
        } 
        if ((py > cy || py < ay) || (px > Math.max(ax, cx))){
            return false;
        } 
        if (px < Math.min(ax, cx)){
            return true;
        }

        Double red, blue;
        // These Booleans are to represent the 'infinity'
        Boolean infinityRed = false;
        Boolean infinityBlue = false;

        if(ax != cx) red = ((cy - ay) / (cx - ax));
        else infinityRed = true;

        if(ax != px) blue = ((py - ay) / (px - ax));
        else infinityBlue = true;
    

        if(infinityRed && infinityBlue) return true;

        else if(infinityRed != infinityBlue){
            if(infinityRed) return false;
            else return true;
        }
        else return (blue >= red); 
    }

    /**
    * @description For a given a location, find if it is within a 'Zona Conflictiva'
    * @author fizquierdo@omegacrmconsulting.com | 01-05-2022 
    * @param Account account 
    * @return Boolean 
    **/
    public static Boolean checkZonaConflictiva(Account account){
        for(List<Geolocation> polygon: zonasConflictivas){
            if(polygonContains(polygon, new Geolocation(Double.valueOf(account.ShippingLatitude), Double.valueOf(account.ShippingLongitude)))){
                return true;
            }
        }
        return false;
    }

    /**
    * @description To create the pacient folder
    * @author fizquierdo@omegacrmconsulting.com | 01-25-2022 
    * @param String nameFolder 
    **/
    @future (callout=true)
    public static void createFolderSharePoint(String nameFolder){
        OM_ContentWrapper folderWrapper = new OM_ContentWrapper();
        folderWrapper.parentId = 'root';
        folderWrapper.name = nameFolder;
        OM_ContentWrapper folderResponse = OM_ContentService.runIntegrationAction(OM_ContentService.ACTION_CREATE_FOLDER, SETTINGS_NAME, folderWrapper, null);
    }
}