/**
 * @description       : For testing the ETH_AccountTriggerHandler class
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 05-06-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
@isTest
public with sharing class ETH_AccountTriggerHandlerTest {
    @TestSetup
    static void makeData(){
        ETH_DataFactory.setUpOmegaContent();
    }

    @isTest
    public static void createFolderSharePoint(){
        Google_Maps_API__mdt api = [SELECT ZonasConflictivas__c, API_Key__c, URL__c FROM Google_Maps_API__mdt WHERE Label='Geolocation'];

        //set mocks for the calls
        MultiStaticResourceCalloutMock multimock = new MultiStaticResourceCalloutMock();
        multimock.setStaticResource(api.URL__c + '?address=Carrer+del+Turia+6+08038+Barcelona+Spain&key=' + api.API_Key__c, 'MockLocation');
        multimock.setStaticResource(api.ZonasConflictivas__c, 'MockZonasConflictivas');
        multimock.setStaticResource('https://graph.microsoft.com/v1.0/sites/'+'test'+'/drives/'+ 'test' +'/items/' + 'root' + '/children', 'MockSharePoint');
        multimock.setStatusCode(200);
        Test.setMock(HttpCalloutMock.class, multimock);

        Account account = ETH_DataFactory.createAccountWithAddress('Spain', 'Barcelona', 'Carrer del Turia 6', '08038', 'Barcelona');
        Test.startTest();
        insert account;
        Test.stopTest();
    }

    /**
    * @description Test if the system marks a location as 'Zona Conflictiva'
    * @author fizquierdo@omegacrmconsulting.com | 01-25-2022 
    **/
    @isTest
    public static void setZonasConflictivasPositiveTest(){
        Google_Maps_API__mdt api = [SELECT ZonasConflictivas__c, API_Key__c, URL__c FROM Google_Maps_API__mdt WHERE Label='Geolocation'];

        //set mocks for the calls
        MultiStaticResourceCalloutMock multimock = new MultiStaticResourceCalloutMock();
        multimock.setStaticResource(api.URL__c + '?address=Carrer+del+Turia+6+08038+Barcelona+Spain&key=' + api.API_Key__c, 'MockLocation');
        multimock.setStaticResource(api.ZonasConflictivas__c, 'MockZonasConflictivas');
        multimock.setStaticResource('https://graph.microsoft.com/v1.0/sites/'+'test'+'/drives/'+ 'test' +'/items/' + 'root' + '/children', 'MockSharePoint');
        multimock.setStatusCode(200);
        Test.setMock(HttpCalloutMock.class, multimock);

        Account account = ETH_DataFactory.createAccountWithAddress('Spain', 'Barcelona', 'Carrer del Turia 6', '08038', 'Barcelona');
        account.ShippingLatitude = 41.3647184;
        account.ShippingLongitude = 2.141572;
        Test.startTest();
        insert account;
        Test.stopTest();

        Account accountUpdated = [SELECT ShippingLatitude, ShippingLongitude, ZonaConflictiva__c FROM Account WHERE Id =: account.Id];
        // System.assertEquals(accountUpdated.ShippingLatitude, 41.3647184, 'Correct latitude');
        // System.assertEquals(accountUpdated.ShippingLongitude, 2.141572, 'Correct longitude');
        System.assertEquals(accountUpdated.ZonaConflictiva__c, true, 'Correct checkbox');
    }

    /**
    * @description Test if when we change the location, the system controlls if the new location is 'Zona Conflictiva'
    * @author fizquierdo@omegacrmconsulting.com | 01-25-2022 
    **/
    @isTest
    public static void updateAccountLocationTest(){
        Google_Maps_API__mdt api = [SELECT ZonasConflictivas__c, API_Key__c, URL__c FROM Google_Maps_API__mdt WHERE Label='Geolocation'];

        //set mocks for the calls
        MultiStaticResourceCalloutMock multimock = new MultiStaticResourceCalloutMock();
        multimock.setStaticResource(api.URL__c + '?address=Carrer+del+Turia+6+08038+Barcelona+Spain&key=' + api.API_Key__c, 'MockLocation');
        multimock.setStaticResource(api.URL__c + '?address=Periodista+Fernando+Gomez+de+la+Cruz+18014+Granada+Spain&key=' + api.API_Key__c, 'MockLocationZonaNoConflictiva');
        multimock.setStaticResource(api.ZonasConflictivas__c, 'MockZonasConflictivas');
        multimock.setStaticResource('https://graph.microsoft.com/v1.0/sites/'+'test'+'/drives/'+ 'test' +'/items/' + 'root' + '/children', 'MockSharePoint');
        multimock.setStatusCode(200);
        Test.setMock(HttpCalloutMock.class, multimock);

        Account account = ETH_DataFactory.createAccountWithAddress('Spain', 'Barcelona', 'Carrer del Turia 6', '08038', 'Barcelona');

        insert account;
        account.ShippingCountry = 'Spain';
        account.ShippingCity = 'Granada';
        account.ShippingStreet = 'Periodista Fernando Gomez de la Cruz';
        account.ShippingPostalCode = '18014';
        account.ShippingState = 'Granada';
        account.ShippingLatitude = 37.1959052;
        account.ShippingLongitude = -3.619564;

        Test.startTest();
        update account;
        Test.stopTest();

        Account accountUpdated = [SELECT ShippingLatitude, ShippingLongitude, ZonaConflictiva__c FROM Account WHERE Id =: account.Id];
        // System.assertEquals(accountUpdated.ShippingLatitude, 37.1959052, 'Correct latitude');
        // System.assertEquals(accountUpdated.ShippingLongitude, -3.619564, 'Correct longitude');
        System.assertEquals(accountUpdated.ZonaConflictiva__c, false, 'Correct checkbox');
    }
    
    /**
    * @description Test if the system does not mark a location as 'Zona Conflictiva'
    * @author fizquierdo@omegacrmconsulting.com | 01-25-2022 
    **/
    @isTest
    public static void setZonasConflictivasNegativeTest(){
        Google_Maps_API__mdt api = [SELECT ZonasConflictivas__c, API_Key__c, URL__c FROM Google_Maps_API__mdt WHERE Label='Geolocation'];

        //set mocks for the calls
        MultiStaticResourceCalloutMock multimock = new MultiStaticResourceCalloutMock();
        multimock.setStaticResource(api.URL__c + '?address=Periodista+Fernando+Gomez+de+la+Cruz+18014+Granada+Spain&key=' + api.API_Key__c, 'MockLocationZonaNoConflictiva');
        multimock.setStaticResource(api.ZonasConflictivas__c, 'MockZonasConflictivas');
        multimock.setStaticResource('https://graph.microsoft.com/v1.0/sites/'+'test'+'/drives/'+ 'test' +'/items/' + 'root' + '/children', 'MockSharePoint');
        multimock.setStatusCode(200);
        Test.setMock(HttpCalloutMock.class, multimock);

        Account account = ETH_DataFactory.createAccountWithAddress('Spain', 'Granada', 'Periodista Fernando Gomez de la Cruz', '18014', 'Granada');
        account.ShippingLatitude = 37.1959052;
        account.ShippingLongitude = -3.619564;
        
        Test.startTest();
        insert account;
        Test.stopTest();

        Account accountUpdated = [SELECT ShippingLatitude, ShippingLongitude, ZonaConflictiva__c FROM Account WHERE Id =: account.Id];
        // System.assertEquals(accountUpdated.ShippingLatitude, 37.1959052, 'Correct latitude');
        // System.assertEquals(accountUpdated.ShippingLongitude, -3.619564, 'Correct longitude');
        System.assertEquals(accountUpdated.ZonaConflictiva__c, false, 'Correct checkbox');
    }
}