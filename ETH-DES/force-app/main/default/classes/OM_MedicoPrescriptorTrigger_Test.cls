/**
 * @File Name          OM_MedicoPrescriptorTrigger_Test.cls
 * @Description        Test class for covering cases for OM_MedicoPrescriptorTrigger class
 * @Author             sdiaz@omegacrmconsulting.com
 * @Group              Tests
 * @Last Modified By   sdiaz@omegacrmconsulting.com
 * @Last Modified On   17/03/2022 17:42
**/
@isTest
public with sharing class OM_MedicoPrescriptorTrigger_Test {
    final static Id RT_Tratamiento_Case =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Tratamiento TRD').getRecordTypeId();
    
    @TestSetup
    static void makeData(){
        ETH_DataFactory.setUpOmegaContent();
        //crear un usuario
        User userProfesional = ETH_DataFactory.createUser('usertestethprofesional@test.com');
        insert userProfesional;

        User user = [SELECT Id FROM User WHERE LastName = 'Omega' LIMIT 1];

        System.runAs(user){
            MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
            Test.setMock(HttpCalloutMock.class, multimock);

            Test.startTest();

            Account account = ETH_DataFactory.createAccountWithAddress('Spain', 'Granada', 'Periodista Fernando Gomez de la Cruz', '18014', 'Granada');
            insert account;

            OperatingHours operatingHours = ETH_DataFactory.createOperatingHours();
            insert operatingHours;

            //timeslots de operating hours
            ETH_DataFactory.createTimeSlots(operatingHours.Id, 5, 23);

            ServiceTerritory serviceTerritoryPadre = ETH_DataFactory.createServiceTerritory(null, operatingHours.Id, 'Pharmate', null);
            insert serviceTerritoryPadre;

            ServiceTerritory serviceTerritoryHijo = ETH_DataFactory.createServiceTerritory(account.ShippingPostalCode, operatingHours.Id, 'Pharmate', serviceTerritoryPadre.Id);
            insert serviceTerritoryHijo;
        

            //crear un assigned resource a cada usuario
            ServiceResource serviceResourceProfesional = ETH_DataFactory.createServiceResourceProfesional(userProfesional.Id);
            insert serviceResourceProfesional;

            //crear los services territories member
            ServiceTerritoryMember serviceTerritoryMemberProfesional = ETH_DataFactory.createServiceTerritoryMember('Profesional Visita Presencial (Pharmate)', serviceResourceProfesional.Id, serviceTerritoryPadre.Id, operatingHours.Id);
            insert serviceTerritoryMemberProfesional;

            //repetimos proceso para CRETA y box
            ServiceTerritory serviceTerritoryCRETA = ETH_DataFactory.createServiceTerritory(account.ShippingPostalCode, operatingHours.Id, 'Creta',  null);
            insert serviceTerritoryCRETA;

            Asset asset = ETH_DataFactory.createAsset(account.Id);
            insert asset;

            ServiceResource serviceResourceBox = ETH_DataFactory.createServiceResourceBox(asset.Id);
            insert serviceResourceBox;

            ServiceTerritoryMember serviceTerritoryMemberBox = ETH_DataFactory.createServiceTerritoryMember('', serviceResourceBox.Id, serviceTerritoryCRETA.Id, operatingHours.Id);
            insert serviceTerritoryMemberBox;

            protocolo__c protocolo = ETH_DataFactory.createProtocolo();
            insert protocolo;

            Paso_Protocolo__c paso1 = ETH_DataFactory.createPasoProtocolo(protocolo.Id, 1, 'Virtual', 'Usuario que da de alta el tratamiento', 'Inicio', 'Domicilio', 7);
            insert paso1;
            Paso_Protocolo__c paso2 = ETH_DataFactory.createPasoProtocolo(protocolo.Id, 2, 'Presencial', 'Profesional Visita Presencial (Pharmate)', 'Seguimiento', 'CRETA', 7);
            insert paso2;

            //alta
            Worktype worktypeAlta = ETH_DataFactory.createWorktype('Alta', 'Pharmate', paso1.tipo_Lugar__c, paso1.lugar_visita__c);
            insert worktypeAlta;
            //domicilio
            Worktype worktypeSeguimientoDomicilio = ETH_DataFactory.createWorktype('Seguimiento', 'Pharmate', paso2.tipo_Lugar__c, paso2.lugar_visita__c);
            insert worktypeSeguimientoDomicilio;
        }
        Test.stopTest();        
    }

    @isTest
    static void deleteMedicPresInProgressCase(){
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);

        User userProfesional = [SELECT Id FROM User WHERE Email='usertestethprofesional@test.com' LIMIT 1];
        Account account = [SELECT Id FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
        protocolo__c protocolo = [SELECT Id FROM protocolo__c WHERE Name = 'Protocolo test'];
        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'Pharmate');
        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, protocolo.Id, 'TRD');
        insert tratamiento;
        ca.Tratamiento__c = tratamiento.Id;

        Med_Presc__c medicPres = new Med_Presc__c();
        medicPres.Name = 'TestOM_MedicoPrescriptorTrigger_Test1';
        insert medicPres;
                 
        ca.Med_Presc__c = medicPres.Id;
        ca.Status = 'In progress';
        System.runAs(userProfesional){
            insert ca;
        }

        Med_Presc__c medicPres2 = new Med_Presc__c();
        medicPres2.Name = 'TestOM_MedicoPrescriptorTrigger_Test2';
        insert medicPres2;

        Case ca2 = ETH_DataFactory.createCaseWithAccountTest(account, 'Pharmate');
        tratamiento__c tratamiento2 = ETH_DataFactory.createTratamiento(ca2.Cliente__c, protocolo.Id, 'TRD');
        insert tratamiento2;
        ca2.Tratamiento__c = tratamiento2.Id;
        ca2.Med_Presc__c = medicPres2.Id;
        ca2.Status = 'In progress';
        System.runAs(userProfesional){
            insert ca2;
        }

        List<Med_Presc__c> medicPresToDelete = [SELECT Id FROM Med_Presc__c WHERE Name = 'TestOM_MedicoPrescriptorTrigger_Test1' OR Name = 'TestOM_MedicoPrescriptorTrigger_Test2'];
        try{
            delete medicPresToDelete;   
        }catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('No se puede eliminar al médico prescriptor por tener tratamientos activos') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        } 
    }

     @isTest
    static void deleteMedicPresClosedCase(){
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);

        User userProfesional = [SELECT Id FROM User WHERE Email='usertestethprofesional@test.com' LIMIT 1];
        Account account = [SELECT Id FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
        protocolo__c protocolo = [SELECT Id FROM protocolo__c WHERE Name = 'Protocolo test'];
        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'Pharmate');
        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, protocolo.Id, 'TRD');
        insert tratamiento;
        ca.Tratamiento__c = tratamiento.Id;
        ca.Status = 'Closed';
        
        Med_Presc__c medicPres = new Med_Presc__c();
        medicPres.Name = 'TestOM_MedicoPrescriptorTrigger_Test3';
        insert medicPres;
        ca.Med_Presc__c = medicPres.Id;
        
        System.runAs(userProfesional){
            insert ca;
        }

        Med_Presc__c medicPresToDelete = [SELECT Id FROM Med_Presc__c WHERE Name = 'TestOM_MedicoPrescriptorTrigger_Test3'];
        delete medicPresToDelete;
    }
    
}