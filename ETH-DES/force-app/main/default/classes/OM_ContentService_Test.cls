/**
 * @description       Unit test class for ContentService
 * @author            OmegaCRM 
 * @group             Content tests
**/
@IsTest
public inherited sharing class OM_ContentService_Test {
    
    public static final String SHAREPOINT_SETTINGS_NAME = 'testSHAREPOINT';
    public static final String ONEDRIVE_SETTINGS_NAME = 'testONEDRIVE';
    public static final String GOOGLEDRIVE_SETTINGS_NAME = 'testGOOGLEDRIVE';
    public static final String USERNAME = 'testcontentUser@content.omegacrconsulting.com';

    @TestSetup
    static void makeData(){

        String namespace = [SELECT NamespacePrefix FROM Organization].NamespacePrefix;
        String fieldName = namespace != null && String.isNotBlank(namespace) ? namespace + '__OM_GDrive_id__c' : 'OM_GDrive_id__c';

        // Insert user with correct permission set for testing (any profile)
        Profile pf = [SELECT Id FROM Profile WHERE UserLicense.Name = 'Salesforce' LIMIT 1];
        User u = new User(
            Username = USERNAME,
            FirstName = 'Test',
            LastName = 'Omega',
            ProfileId = pf.Id,
            Email = USERNAME,
            Alias = 'test',
            TimeZoneSidKey = 'GMT',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'ISO-8859-1',
            LanguageLocaleKey = 'en_US'
        );

        insert u;

        PermissionSet contUser = [SELECT Id FROM PermissionSet WHERE Name = 'OM_ContentUser' LIMIT 1];
        PermissionSetAssignment psass = new PermissionSetAssignment(
            AssigneeId = u.Id,
            PermissionSetId = contUser.Id
        );

        insert psass;


        System.runAs(u){

            // Insert some settings
            OM_ContentSetting__c spsettings = new OM_ContentSetting__c(
                OM_Name__c = SHAREPOINT_SETTINGS_NAME,
                OM_Allow_folder_creation__c = true,
                OM_Allow_navigation__c = true,
                OM_Allow_upload_files__c = true,
                OM_Allow_edit__c = true,
                OM_Auth_type__c = 'Individual',
                OM_Drive_id__c = 'test',
                OM_Use_personal_drive__c = false,
                OM_Client_id__c = 'test',
                OM_Client_secret__c = 'test',
                OM_Discovery_docs__c = 'test',
                OM_Provider__c = 'MicrosoftSharePoint',
                OM_Site_id__c = 'test',
                OM_Tenant__c = 'test',
                OM_Scope__c = 'test',
                OM_Redirect_uri__c = 'https://testorg.visualforce.com/apex/OM_OAuthCallback',
                OM_Columns__c = 'name;size'
            );
            insert spsettings;

            OM_ContentSetting__c onedsettings = new OM_ContentSetting__c(
                OM_Name__c = ONEDRIVE_SETTINGS_NAME,
                OM_Allow_folder_creation__c = true,
                OM_Allow_navigation__c = true,
                OM_Allow_upload_files__c = true,
                OM_Allow_edit__c = true,
                OM_Auth_type__c = 'Individual',
                OM_Drive_id__c = 'test',
                OM_Use_personal_drive__c = true,
                OM_Client_id__c = 'test',
                OM_Client_secret__c = 'test',
                OM_Discovery_docs__c = 'test',
                OM_Provider__c = 'MicrosoftOneDrive',
                OM_Site_id__c = 'test',
                OM_Tenant__c = 'test',
                OM_Scope__c = 'test',
                OM_Redirect_uri__c = 'https://testorg.visualforce.com/apex/OM_OAuthCallback',
                OM_Columns__c = 'name;size'
            );
            insert onedsettings;

            OM_ContentSetting__c gdrivesettings = new OM_ContentSetting__c(
                OM_Name__c = GOOGLEDRIVE_SETTINGS_NAME,
                OM_Allow_folder_creation__c = true,
                OM_Allow_navigation__c = true,
                OM_Allow_upload_files__c = true,
                OM_Allow_edit__c = true,
                OM_Auth_type__c = 'Individual',
                OM_Drive_id__c = 'test',
                OM_Use_personal_drive__c = true,
                OM_Client_id__c = 'test',
                OM_Client_secret__c = 'test',
                OM_Discovery_docs__c = 'test',
                OM_Provider__c = 'GoogleDrive',
                OM_Site_id__c = 'test',
                OM_Tenant__c = 'test',
                OM_Scope__c = 'test',
                OM_Redirect_uri__c = 'https://testorg.visualforce.com/apex/OM_OAuthCallback',
                OM_Columns__c = 'name;size'
            );
            insert gdrivesettings;

            // Insert object settings for OM_Content
            OM_ContentSetting__c objectsettings = new OM_ContentSetting__c(
                OM_Allow_folder_creation__c = true,
                OM_Allow_navigation__c = true,
                OM_Allow_upload_files__c = true,
                OM_Allow_edit__c = true,
                OM_Object__c = 'OM_Content__c',
                OM_Parent_setting__c = gdrivesettings.Id,
                OM_Initial_folder_id__c = '{!record.'+fieldName+'}',
                OM_Columns__c = 'name;size'

            );
            insert objectsettings;

            // Insert some custom properties
            OM_ContentCustomProperty__c custommapping = new OM_ContentCustomProperty__c(
                OM_As_column__c = true,
                OM_Editable__c = true,
                OM_Filterable__c = true,
                OM_Property_name__c = 'testProperty',
                OM_Salesforce_field_name__c = 'Name',
                OM_Parent_setting__c = gdrivesettings.Id
            );
            insert custommapping;



            OM_ContentService.updateUserAccess(spsettings,u.Id, '{"access_token": "test_sharepoint_token", "refresh_token": "test_refresh"}');
            OM_ContentService.updateUserAccess(onedsettings,u.Id, '{"access_token": "test_onedrive_token", "refresh_token": "test_refresh"}');
            OM_ContentService.updateUserAccess(gdrivesettings,u.Id, '{"access_token": "test_google_token", "refresh_token": "test_refresh"}');
        }

    }

    @IsTest
    static void testGetAuthEndpoint(){
        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        Test.startTest();
        System.runAs(contextuser){
            String endpo = OM_ContentService.getAuthorizationEndpoint(GOOGLEDRIVE_SETTINGS_NAME, contextUser.Id);
            String endponull = OM_ContentService.getAuthorizationEndpoint(GOOGLEDRIVE_SETTINGS_NAME, null);
            System.assertEquals(endpo, endponull, 'Auth endpoint should be the same for the current user when passing null value');
        }
        Test.stopTest();
    }

    @IsTest
    static void testEndpointsGoogle(){
        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        Test.startTest();
        System.runAs(contextuser){
            OM_ContentSetting__c settings = OM_ContentService.getSettings(GOOGLEDRIVE_SETTINGS_NAME);
            System.assertEquals(true, settings.OM_Provider__c == 'GoogleDrive', 'Provider should be correct');

            String endpBase = OM_ContentService.getEndpoint(settings, 'base');
            System.assertEquals('https://www.googleapis.com/drive/v3', endpBase, 'The base endpoint should be correct');

            Map<String, String> endps = OM_ContentService.getAllEndpoints(GOOGLEDRIVE_SETTINGS_NAME);
            System.assertEquals(10, endps.keySet().size(), 'Should contain all defined enpoint for provider');

        }
        Test.stopTest();
    }

    @IsTest
    static void testGetSettingForObject(){
        String contentExtId = 'testtesttest';
        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        Test.startTest();
        System.runAs(contextuser){
            // Insert a test Om_Content
            OM_Content__c content = new OM_Content__c(
                OM_GDrive_id__c = contentExtId
            );

            insert content;

            String contentId = content.Id; 

            try {
                OM_ContentService.getSettings('NOTEXISTINGSETTINGS');
            } catch (OM_ContentException e) {
                System.assertNotEquals(null, e, 'Exception should be thrown for not existing settings');

            }

            Map<String, Object> settingsWithoutObj = OM_ContentService.getSettingsForObject(GOOGLEDRIVE_SETTINGS_NAME, null);
            System.assertEquals(null, ((OM_ContentSetting__c) settingsWithoutObj.get('settings')).OM_Initial_folder_id__c, 'Field should not be informed in main settings');

            Map<String, Object> settingsForRecord = OM_ContentService.getSettingsForObject(GOOGLEDRIVE_SETTINGS_NAME, contentId);
            System.assertEquals(contentExtId, ((OM_ContentSetting__c) settingsForRecord.get('settings')).OM_Initial_folder_id__c, 'Field Initial folder should have been replaced by record field');

            delete content;

            try {
                OM_ContentService.getSettingsForObject(GOOGLEDRIVE_SETTINGS_NAME, contentId);
            } catch (Exception e) {
                System.assertNotEquals(null, e, 'An exception should be thrown');
            }
        }
        Test.stopTest();
    }

    @IsTest
    static void testSalesforceDataCreation(){
        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        Test.startTest();
        System.runAs(contextuser){
            OM_ContentWrapper content1 = new OM_ContentWrapper();
            content1.id = 'test1';
            content1.name = 'test1';


            OM_ContentWrapper content2 = new OM_ContentWrapper();
            content2.id = 'test2';
            content2.name = 'test2';
            List<OM_ContentWrapper> listContents = new List<OM_ContentWrapper> { content1, content2 };

            List<OM_Content__c> contents = OM_ContentService.upsertContents(null, GOOGLEDRIVE_SETTINGS_NAME, listContents);
            System.assertEquals(2, contents.size(), '2 OM_Content__c records should be created');

            OM_ContentService.deleteContents(GOOGLEDRIVE_SETTINGS_NAME, listContents);
            contents = [SELECT Id FROM OM_Content__c];
            System.assertEquals(0, contents.size(), '2 OM_Content__c records should be deleted');

        }
        Test.stopTest();
    }


    @IsTest
    static void testUserAccess(){
        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        Test.startTest();
        System.runAs(contextuser){
            Map<String, String> access = OM_ContentService.getCurrentUserAccess(GOOGLEDRIVE_SETTINGS_NAME);
            System.assert(access.containsKey('access_token'), 'An access token should existi form current user');


            Map<String, String> access2 = OM_ContentService.getUserAccess(GOOGLEDRIVE_SETTINGS_NAME, UserInfo.getUserId());
            System.assertEquals(access.get('access_token'), access2.get('access_token'), 'Access should be the same as the previous call');

            Map<String, String> accessNotExisting = OM_ContentService.getUserAccess(GOOGLEDRIVE_SETTINGS_NAME, 'NOTEXISTINGUSERID');
            System.assertEquals(null, accessNotExisting, 'Access should not exist for this user');
        }
        Test.stopTest();
    }

    @IsTest
    static void testListForMicrosoftConfiguration(){
        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        Test.setMock(HttpCalloutMock.class, new OM_ContentMicrosoftIntegrationMock());
        Test.startTest();
        System.runAs(contextuser){
            List<Map<String,Object>> driveList = OM_ContentService.listSharepointDrives(SHAREPOINT_SETTINGS_NAME);
            System.assertEquals(1, driveList.size(), 'Drive list should contain data');
            
            String sharepointSiteId = OM_ContentService.getSharepointSiteId(SHAREPOINT_SETTINGS_NAME, 'https://myorg.sharepoint.com/test/lib');
            System.assertNotEquals(null, sharepointSiteId, 'Site Id should be valid');

        }
        Test.stopTest();
    }

    @IsTest
    static void testGoodTokens(){
        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        // Test versus Microsfot endpoints
        Test.setMock(HttpCalloutMock.class, new OM_ContentMicrosoftIntegrationMock());
        System.runAs(contextuser){
            
            Test.startTest();
            List<OM_ContentAccessData__c> accessSharepoint = [SELECT Id FROM OM_ContentAccessData__c WHERE OM_User__c = :contextUser.Id AND OM_Content_setting__r.OM_Name__c = :SHAREPOINT_SETTINGS_NAME];
            System.assertEquals(1, accessSharepoint.size(), 'Only one record should be present for the user');

            String accessToken = OM_ContentService.getAccessToken(SHAREPOINT_SETTINGS_NAME, 'testcode', contextuser.Id);
            System.assertNotEquals(null, accessToken, 'Token response should not be null');

            // Stop test here to reset limits
            Test.stopTest();


            // Access data should be overriden. Re-query and assert
            accessSharepoint = [SELECT Id FROM OM_ContentAccessData__c WHERE OM_User__c = :contextUser.Id AND OM_Content_setting__r.OM_Name__c = :SHAREPOINT_SETTINGS_NAME];
            System.assertEquals(1, accessSharepoint.size(), 'Only one record should be present for the user after get another token');


            String refreshToken = OM_ContentService.refreshToken(SHAREPOINT_SETTINGS_NAME, contextuser.Id);
            System.assertNotEquals(null, refreshToken, 'Refresh token response should not be null');

            // Access data should be overriden. Re-query and assert
            accessSharepoint = [SELECT Id FROM OM_ContentAccessData__c WHERE OM_User__c = :contextUser.Id AND OM_Content_setting__r.OM_Name__c = :SHAREPOINT_SETTINGS_NAME];
            System.assertEquals(1, accessSharepoint.size(), 'Only one record should be present for the user after get another token');



        }
    }

    @IsTest
    static void testBadTokens(){
        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        // Test versus Microsfot endpoints
        Test.setMock(HttpCalloutMock.class, new OM_ContentMicrosoftIntegrationMock(true));
        System.runAs(contextuser){
            
            Test.startTest();
            List<OM_ContentAccessData__c> accessSharepoint = [SELECT Id FROM OM_ContentAccessData__c WHERE OM_User__c = :contextUser.Id AND OM_Content_setting__r.OM_Name__c = :SHAREPOINT_SETTINGS_NAME];
            System.assertEquals(1, accessSharepoint.size(), 'Only one record should be present for the user');

            try {
                OM_ContentService.getAccessToken(SHAREPOINT_SETTINGS_NAME, 'testcode', contextuser.Id);
            } catch (OM_ContentException e) {
                System.assertNotEquals(null, e, 'Token request should have thrown an exception');
            }

            // Stop test here to reset limits
            Test.stopTest();
        }
    }

    @IsTest
    static void testIntegrationActions(){
        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        // Test versus Microsfot endpoints
        Test.setMock(HttpCalloutMock.class, new OM_ContentMicrosoftIntegrationMock(true));
        System.runAs(contextuser){
            
            Test.startTest();
            OM_ContentWrapper testContent = new OM_ContentWrapper();
            testContent.id = 'testid';
            testContent.name = 'testname';

            OM_ContentWrapper result = OM_ContentService.runIntegrationAction('notexistingaction', SHAREPOINT_SETTINGS_NAME, testContent, null);
            System.assertEquals(null, result, 'With no action null is returned');

            result = OM_ContentService.runIntegrationAction(OM_ContentService.ACTION_UPDATE_ITEM, SHAREPOINT_SETTINGS_NAME, testContent, null);
            System.assertNotEquals(null, result, 'Something should be returned for Update action');

            result = OM_ContentService.runIntegrationAction(OM_ContentService.ACTION_DELETE_ITEM, SHAREPOINT_SETTINGS_NAME, testContent, null);
            System.assertNotEquals(null, result, 'Something should be returned for Update action');


            OM_ContentWrapper testFolder = new OM_ContentWrapper();
            testFolder.parentId = 'root';
            testFolder.name = 'testname';
            result = OM_ContentService.runIntegrationAction(OM_ContentService.ACTION_CREATE_FOLDER, SHAREPOINT_SETTINGS_NAME, testFolder, null);
            System.assertNotEquals(null, result, 'Something should be returned for Create folder action');


            OM_ContentWrapper testUpload = new OM_ContentWrapper();
            testUpload.parentId = 'root';
            testUpload.name = 'testname';
            testUpload.mimeType = 'text/plain';
            result = OM_ContentService.runIntegrationAction(OM_ContentService.ACTION_UPLOAD_ITEM, SHAREPOINT_SETTINGS_NAME, testUpload, EncodingUtil.base64Decode('aGpvbGE='));
            System.assertNotEquals(null, result, 'Something should be returned for Upload action');

            Test.stopTest();
        }
    }
}