global with sharing class Batch_ReprocesarFacturacionSACYL implements Database.Batchable<sObject>, Database.AllowsCallouts{
    public List<Integracion_Facturacion__c> logFacturacionList = new List<Integracion_Facturacion__c>();


    global Batch_ReprocesarFacturacionSACYL() {

    }

    global List <Integracion_Facturacion__c> start(Database.BatchableContext bc) {

        String query = 'SELECT id, Anio_de_facturacion__c, Mes_de_facturacion__c, Case__r.id, Case__r.Account.PersonContactId, Case__r.CaseNumber, Case__r.Cod_Iden__c, Case__r.Account.FirstName, Case__r.Account.LastName, ' + 
                        + 'Case__r.Account.Suffix, Case__r.Account.NIF__c, Case__r.Account.SeguridadSocial__c, Case__r.Account.NumeroPoliza__c, '+
                        + 'Case__r.Account.HealthCloudGA__Age__pc, Case__r.Cliente__r.Cod_cliente__c, Case__r.Lote__r.Cod_lot__c, '+
                        + 'Case__r.Tratamiento__r.Cod_tra__c, Case__r.Tratamiento__r.Nombre_Facturacion__c, '+
                        + 'Case__r.Med_Presc__r.Cod_med__c, Case__r.Med_Presc__r.NombreCompleto__c, Case__r.Tipo_Iden__c, '+
                        + 'Case__r.Presc_number__c, Case__r.Fec_ini_tra__c, Case__r.Fec_fin_tra__c, Case__r.AccountId '+
                        + ' FROM Integracion_Facturacion__c ' + 
                        + ' WHERE Reprocesar__c = true and Case__r.Account.FirstName !=\'Test\'';

        system.debug('Query: '+query);

        // system.debug('Resultado query: '+Database.query(query).size());
        return Database.query(query);
           
    }

    global void execute(Database.BatchableContext BC, List<Integracion_Facturacion__c> scope) {
        // Map <String, Case> caseIdToMapErrors = new Map <String, Case> ();
        system.debug('Casos a facturar: '+scope);

        Set<Id> contactIds = new Set<Id>();
        for(Integracion_Facturacion__c auxIntFacturacion : scope){
            contactIds.add(auxIntFacturacion.Case__r.Account.PersonContactId);
        }

        Map<Id, Integer> patientIdToIngresoDays = new Map<Id, Integer>();

        //Mes actual
        Integer month = Integer.valueOf(String.valueOf(date.today().format()).split('/')[1]);
        //Año actual
        Integer year = Integer.valueOf(String.valueOf(date.today().format()).split('/')[2]);

        month = (month==1? 12 : month-1);
        year = (month==1? (year-1) : year);

        //Días del mes
        Integer monthDays = Date.daysInMonth(year,month);

        List<PersonLifeEvent> ingresosPacientes = [select id, EventDate, ExpirationDate, EventType,PrimaryPersonId, PrimaryPerson.AccountId from PersonLifeEvent
                                                    where EventType = 'Ingreso hospitalario'
                                                    and EventDate >=: Datetime.newInstance(year, month, 1)
                                                    and (ExpirationDate <: Datetime.newInstance(year, month + 1, 1) OR ExpirationDate = null)
                                                    and IsDeleted= false
                                                    and PrimaryPersonId IN: contactIds];


        Map<Id, List<PersonLifeEvent>> pacienteIdToIngresosListMap = new Map<Id, List<PersonLifeEvent>>();
        for (PersonLifeEvent ingresoAux : ingresosPacientes) {
            if(pacienteIdToIngresosListMap.isEmpty() || !pacienteIdToIngresosListMap.containsKey(ingresoAux.PrimaryPerson.AccountId)){List<PersonLifeEvent> auxList = new List<PersonLifeEvent>();
                auxList.add(ingresoAux);
                pacienteIdToIngresosListMap.put(ingresoAux.PrimaryPerson.AccountId, auxList);
            }else{List<PersonLifeEvent> auxList = pacienteIdToIngresosListMap.get(ingresoAux.PrimaryPersonId);
                auxList.add(ingresoAux);
                pacienteIdToIngresosListMap.put(ingresoAux.PrimaryPerson.AccountId, auxList);
            }
        }
                        
        // system.debug('Mapa de ingreso de clientes: '+patientIdToIngresoDays);
            for(Integracion_Facturacion__c auxIntegracionFact : scope){

                try{

                    String requestBody = '';

                    try{
                        Map<Boolean, String> mapResponse = getToken(auxIntegracionFact, patientIdToIngresoDays);
                        system.debug('Respuesta SACYL: '+mapResponse);

                        //Si la llamada al token y a facturar han ido bien:
                        if(mapResponse.containsKey(false) ){

                            Integracion_Facturacion__c elementoFacturado = insertLogFacturacion(auxIntegracionFact.Case__r, true, 200, 'Facturación correcta.', 
                                                                                                auxIntegracionFact.Case__r.Id, auxIntegracionFact.Mes_de_facturacion__c, auxIntegracionFact.Anio_de_facturacion__c, 
                                                                                                String.valueOf(auxIntegracionFact.Anio_de_facturacion__c) + String.valueOf(auxIntegracionFact.Mes_de_facturacion__c) +auxIntegracionFact.Case__r.Id, 
                                                                                                pacienteIdToIngresosListMap);

                            
                            // Integracion_Facturacion__c elementoFacturado = new Integracion_Facturacion__c();
                            // elementoFacturado.success__c = true;
                            // system.debug('If donde haría split: '+mapResponse);

                            // elementoFacturado.code__c = 200;
                            // elementoFacturado.message__c = 'Facturación correcta.';
                            // elementoFacturado.Case__c = auxIntegracionFact.Case__r.Id;
                            // elementoFacturado.Mes_de_facturacion__c = month; 
                            // elementoFacturado.Anio_de_facturacion__c = year;
                            // elementoFacturado.IdExterno__c = String.valueOf(year) + String.valueOf(month) +auxIntegracionFact.Case__r.Id;
                            // // elementoFacturado.Reprocesar__c = false;

                            logFacturacionList.add(elementoFacturado);
                        
                        }else{Integracion_Facturacion__c elementoFacturado = insertLogFacturacion(auxIntegracionFact.Case__r, false, Integer.valueOf(mapResponse.get(true).split(' - ')[0]), mapResponse.get(true).split(' - ')[1], 
                                                                                                auxIntegracionFact.Case__r.Id,auxIntegracionFact.Mes_de_facturacion__c, auxIntegracionFact.Anio_de_facturacion__c, 
                                                                                                String.valueOf(auxIntegracionFact.Anio_de_facturacion__c) + String.valueOf(auxIntegracionFact.Mes_de_facturacion__c) +auxIntegracionFact.Case__r.Id,  
                                                                                                pacienteIdToIngresosListMap);


                            // Integracion_Facturacion__c elementoFacturado = new Integracion_Facturacion__c();
                            // elementoFacturado.success__c = false;
                            // elementoFacturado.code__c = Integer.valueOf(mapResponse.get(true).split(' - ')[0]);
                            // system.debug('Else donde haría split: '+mapResponse);
                            // elementoFacturado.message__c = mapResponse.get(true).split(' - ')[1];
                            // elementoFacturado.Case__c = auxIntegracionFact.Case__r.Id;
                            // elementoFacturado.Mes_de_facturacion__c = month; 
                            // elementoFacturado.Anio_de_facturacion__c = year;
                            // elementoFacturado.IdExterno__c = String.valueOf(year) + String.valueOf(month) +auxIntegracionFact.Case__r.Id;
                            // elementoFacturado.Reprocesar__c = false;

                            logFacturacionList.add(elementoFacturado);
                        }
                    }catch(System.CalloutException coex){Integracion_Facturacion__c elementoFacturado = insertLogFacturacion(auxIntegracionFact.Case__r, false, null, coex.getMessage(), 
                                                                                                auxIntegracionFact.Case__r.Id, auxIntegracionFact.Mes_de_facturacion__c, auxIntegracionFact.Anio_de_facturacion__c, 
                                                                                                String.valueOf(auxIntegracionFact.Anio_de_facturacion__c) + String.valueOf(auxIntegracionFact.Mes_de_facturacion__c) +auxIntegracionFact.Case__r.Id, 
                                                                                                pacienteIdToIngresosListMap);


                        // Integracion_Facturacion__c elementoFacturado = new Integracion_Facturacion__c();
                        // elementoFacturado.success__c = false;
                        // // elementoFacturado.code__c = mapResponse.get(true).split('-')[0];
                        // elementoFacturado.message__c = coex.getMessage();
                        // elementoFacturado.Case__c = auxIntegracionFact.Case__r.Id;
                        // elementoFacturado.Mes_de_facturacion__c = month; 
                        // elementoFacturado.Anio_de_facturacion__c = year;
                        // elementoFacturado.IdExterno__c = String.valueOf(year) + String.valueOf(month) + auxIntegracionFact.Case__r.Id;
                        // elementoFacturado.Reprocesar__c = false;

                        logFacturacionList.add(elementoFacturado);

                    }catch(System.JSONException jsonex){Integracion_Facturacion__c elementoFacturado = insertLogFacturacion(auxIntegracionFact.Case__r, false, null, jsonex.getMessage(), 
                                                                                                auxIntegracionFact.Case__r.Id, auxIntegracionFact.Mes_de_facturacion__c, auxIntegracionFact.Anio_de_facturacion__c, 
                                                                                                String.valueOf(auxIntegracionFact.Anio_de_facturacion__c) + String.valueOf(auxIntegracionFact.Mes_de_facturacion__c) +auxIntegracionFact.Case__r.Id, 
                                                                                                pacienteIdToIngresosListMap);


                        // Integracion_Facturacion__c elementoFacturado = new Integracion_Facturacion__c();
                        // elementoFacturado.success__c = false;
                        // // elementoFacturado.code__c = mapResponse.get(true).split('-')[0];
                        // elementoFacturado.message__c = jsonex.getMessage();
                        // elementoFacturado.Case__c = auxIntegracionFact.Case__r.Id;
                        // elementoFacturado.Mes_de_facturacion__c = month; 
                        // elementoFacturado.Anio_de_facturacion__c = year;
                        // elementoFacturado.IdExterno__c = String.valueOf(year) + String.valueOf(month) +auxIntegracionFact.Case__r.Id;
                        // elementoFacturado.Reprocesar__c = false;

                        logFacturacionList.add(elementoFacturado);
                    }

                }catch(Exception e){Integracion_Facturacion__c elementoFacturado = insertLogFacturacion(auxIntegracionFact.Case__r, false, null, e.getMessage(), 
                                                                                                auxIntegracionFact.Case__r.Id, auxIntegracionFact.Mes_de_facturacion__c, auxIntegracionFact.Anio_de_facturacion__c, 
                                                                                                String.valueOf(auxIntegracionFact.Anio_de_facturacion__c) + String.valueOf(auxIntegracionFact.Mes_de_facturacion__c) +auxIntegracionFact.Case__r.Id, 
                                                                                                pacienteIdToIngresosListMap);


                    // Integracion_Facturacion__c elementoFacturado = new Integracion_Facturacion__c();
                    // elementoFacturado.success__c = false;
                    // // elementoFacturado.code__c = mapResponse.get(true).split('-')[0];
                    // elementoFacturado.message__c = e.getMessage();
                    // elementoFacturado.Case__c = auxIntegracionFact.Case__r.Id;
                    // elementoFacturado.Mes_de_facturacion__c = month; 
                    // elementoFacturado.Anio_de_facturacion__c = year;
                    // elementoFacturado.IdExterno__c = String.valueOf(year) + String.valueOf(month) + auxIntegracionFact.Case__r.Id;
                    // elementoFacturado.Reprocesar__c = false;
        
                    logFacturacionList.add(elementoFacturado);
                }
            }

        upsert logFacturacionList IdExterno__c;
    }

    global void finish(Database.BatchableContext BC) {
    	
    }

    public static Map<Boolean, String> getToken(Integracion_Facturacion__c intFact, Map<Id, Integer> patientIdToIngresoDays){
        TokenETH__c oCStoken = [SELECT Id, url__c, username__c, password__c, token__c FROM TokenETH__c];

        String sUrl = oCStoken.url__c;
        String sUsername = oCStoken.username__c;
        String sPassword = oCStoken.password__c;
        String sToken = oCStoken.token__c;
        String sMethod = 'POST';

        Map<Boolean,String> responseMap = new Map<Boolean,String>();
        Map<String,Object> tokenResp = new Map<String,Object>();
        
        HttpRequest req = new HttpRequest();
        system.debug('URL TOKEN: '+sUrl + '/login');
        req.setEndpoint(sUrl + '/login');
        req.setMethod(sMethod);
        req.setHeader('Content-Type','application/json');

        Map<String, String> mapAuthen = new Map<String, String>();
        mapAuthen.put('username', sUsername);
        mapAuthen.put('password', sPassword);

        req.setBody(JSON.Serialize(mapAuthen));

        Http http = new Http();
        HTTPResponse res = new HTTPResponse();
        res = http.send(req);

        if (res.getStatusCode() == 200){
            tokenResp = (Map<String,Object>)JSON.deserializeUntyped(res.getBody());
            return callFacturacionSACYL(String.valueOf(tokenResp.get('accessToken')), oCStoken, intFact, patientIdToIngresoDays);
        }else if(res.getStatusCode() == 401){responseMap.put(true, res.getStatusCode() + ' - No se ha podido obtener el token de Atlas.');
        }else if(res.getStatusCode() == 500){responseMap.put(true, res.getStatusCode() + ' - Error en la comunicación con Atlas.');
        }

        // update pedidoToUpdate;

        return responseMap;
    }

    public static Integracion_Facturacion__c insertLogFacturacion(Case auxCase, Boolean success, Integer code, String message, String caseId, Decimal mesFact, Decimal anioFact, String externalId, Map<Id, List<PersonLifeEvent>> pacienteIdToIngresosListMap){
        
        //Mes actual
        // Integer month = Integer.valueOf(String.valueOf(date.today().format()).split('/')[1]);
        Integer month = Integer.valueOf(mesFact);
        //Año actual
        // Integer year = Integer.valueOf(String.valueOf(date.today().format()).split('/')[2]);
        Integer year = Integer.valueOf(anioFact);

        month = (month==1? 12 : month-1);
        year = (month==1? (year-1) : year);

        //Días del mes
        Integer monthDays = Date.daysInMonth(year,month);
        
        Integracion_Facturacion__c elementoFacturado = new Integracion_Facturacion__c();
        elementoFacturado.Success__c = success;
        if(code != null){
            elementoFacturado.Code__c = code;
        }
        elementoFacturado.Message__c = message;
        elementoFacturado.Case__c = caseId;
        elementoFacturado.Mes_de_facturacion__c = mesFact;
        elementoFacturado.Anio_de_facturacion__c = anioFact;
        elementoFacturado.IdExterno__c = externalId;
        elementoFacturado.Reprocesar__c = false;

        Map<String, Object> bodyToSend = new Map<String, Object>();

        bodyToSend.put('fact_id_tratamiento', auxCase.CaseNumber);
        bodyToSend.put('fact_cod_iden', auxCase.Cod_Iden__c);
        bodyToSend.put('fact_cod_cli', auxCase.Cliente__r.Cod_cliente__c);
        bodyToSend.put('fact_lote', auxCase.Lote__r.Cod_lot__c);
        bodyToSend.put('fact_tip_iden', auxCase.Tipo_Iden__c);
        bodyToSend.put('fact_nom_pac', auxCase.Account.FirstName);
        bodyToSend.put('fact_ape1_pac', auxCase.Account.LastName);
        bodyToSend.put('fact_ape2_pac', auxCase.Account.Suffix);
        bodyToSend.put('fact_dni', auxCase.Account.NIF__c);
        bodyToSend.put('fact_nss', auxCase.Account.SeguridadSocial__c);
        bodyToSend.put('fact_num_pol', auxCase.Account.NumeroPoliza__c);
        bodyToSend.put('fact_edad_pac', auxCase.Account.HealthCloudGA__Age__pc.split(' ')[0]);
        bodyToSend.put('fact_cod_tra', String.valueOf(auxCase.tratamiento__r.Cod_tra__c));
        bodyToSend.put('fact_nom_tra', auxCase.tratamiento__r.Nombre_Facturacion__c);
        bodyToSend.put('fact_medico', auxCase.Med_Presc__r.Cod_med__c!= null ?  auxCase.Med_Presc__r.Cod_med__c : ''  + ' - ' + auxCase.Med_Presc__r.NombreCompleto__c != null ?  auxCase.Med_Presc__r.NombreCompleto__c : '');
        bodyToSend.put('fact_num_presc', auxCase.Presc_number__c);


        Integer diasIngreso = 0;

        
        system.debug('Valor conflictivo: '+pacienteIdToIngresosListMap.get(auxCase.AccountId));
        if(pacienteIdToIngresosListMap.containsKey(auxCase.AccountId)){
            for(PersonLifeEvent ingresoAux : pacienteIdToIngresosListMap.get(auxCase.AccountId)){
                if(ingresoAux.EventDate >= Datetime.newInstance(year, month, monthDays) && 
                    (ingresoAux.ExpirationDate < Datetime.newInstance(year, month + 1, 1) || ingresoAux.ExpirationDate == null)){diasIngreso += Date.valueOf(ingresoAux.ExpirationDate).daysBetween(Date.valueOf(ingresoAux.EventDate));
                }
            }
        }
        
        // bodyToSend.put('fact_ingreso_pac', patientIdToIngresoDays.get(auxCase.AccountId));
        bodyToSend.put('fact_ingreso_pac', diasIngreso);
        bodyToSend.put('fact_fec_ini_tra', String.valueOf(auxCase.Fec_ini_tra__c) + 'T00:00:00.000');
        bodyToSend.put('fact_fec_fin_tra', auxCase.Fec_fin_tra__c != null ? String.valueOf(auxCase.Fec_fin_tra__c) + 'T00:00:00.000' : '');
        bodyToSend.put('idsalesforce', auxCase.Id);           
        
        //Si el tratamiento comenzó este mes, cogemos el día de inicio
        if(Integer.valueOf(String.valueOf(auxCase.Fec_ini_tra__c.format()).split('/')[1]) == month &&  
            Integer.valueOf(String.valueOf(auxCase.Fec_ini_tra__c.format()).split('/')[2]) == year){
            // bodyToSend.put('fact_fec_ini_per',String.valueOf(Datetime.newInstance(year, month, 1, 0, 0, 0)).replace(' ','T')+'.000');
            bodyToSend.put('fact_fec_ini_per', String.valueOf(auxCase.Fec_ini_tra__c).split(' ')[0] + 'T00:00:00.000');
        //Si el tratamiento empezó antes de este mes, cogemos el día 1 de este mes     
        }else{
            bodyToSend.put('fact_fec_ini_per',String.valueOf(Datetime.newInstance(year, month, 1, 0, 0, 0)).split(' ')[0] + 'T00:00:00.000');
        }


        //El tratamiento acaba este mes --> Se coge su fecha de finalización
        if(auxCase.Fec_fin_tra__c != null && (Integer.valueOf(String.valueOf(auxCase.Fec_fin_tra__c.format()).split('/')[1]) == month &&  
            Integer.valueOf(String.valueOf(auxCase.Fec_fin_tra__c.format()).split('/')[2]) == year)){
                bodyToSend.put('fact_fec_fin_per', String.valueOf(auxCase.Fec_fin_tra__c).split(' ')[0] + 'T00:00:00.000');
        //Si el tratamiento no ha terminado, o termina el mes que viene --> se coge el final de este mes
        }else{
            bodyToSend.put('fact_fec_fin_per', String.valueOf(Datetime.newInstance(year, month, Date.daysInMonth(year,month), 0, 0, 0)).split(' ')[0] + 'T00:00:00.000');
        }

        system.debug('Mapa del body a serializar: '+bodyToSend); 
        

        elementoFacturado.Body_Enviado__c = JSON.Serialize(bodyToSend);

        return elementoFacturado;

    }

    public static Map<Boolean, String> callFacturacionSACYL(String token, TokenETH__c oCStoken, Integracion_Facturacion__c intFact, Map<Id, Integer> patientIdToIngresoDays){
        system.debug('Dentro de callFacturacionSACYL');
        Map<Boolean,String> responseMap = new Map<Boolean,String>();

        String sUrl = oCStoken.url__c;
        String sUsername = oCStoken.username__c;
        String sPassword = oCStoken.password__c;
        String sToken = oCStoken.token__c;
        String sMethod = 'POST';

        HttpRequest req = new HttpRequest();
        HTTPResponse res = new HTTPResponse();

        req.setEndpoint(sUrl + '/facturar-sacyl');        
        req.setMethod(sMethod);
        req.setHeader('Content-Type','application/json');
        req.setHeader('Authorization', 'Bearer ' + token);

        Map<String, Object> bodyToSend = new Map<String, Object>();

        bodyToSend.put('fact_id_tratamiento', intFact.Case__r.CaseNumber);
        bodyToSend.put('fact_cod_iden', intFact.Case__r.Cod_Iden__c);
        bodyToSend.put('fact_cod_cli', intFact.Case__r.Cliente__r.Cod_cliente__c);
        bodyToSend.put('fact_lote', intFact.Case__r.Lote__r.Cod_lot__c);
        bodyToSend.put('fact_tip_iden', intFact.Case__r.Tipo_Iden__c);
        bodyToSend.put('fact_nom_pac', intFact.Case__r.Account.FirstName);
        bodyToSend.put('fact_ape1_pac', intFact.Case__r.Account.LastName);
        bodyToSend.put('fact_ape2_pac', intFact.Case__r.Account.Suffix);
        bodyToSend.put('fact_dni', intFact.Case__r.Account.NIF__c);
        bodyToSend.put('fact_nss', intFact.Case__r.Account.SeguridadSocial__c);
        bodyToSend.put('fact_num_pol', intFact.Case__r.Account.NumeroPoliza__c);
        bodyToSend.put('fact_edad_pac', intFact.Case__r.Account.HealthCloudGA__Age__pc.split(' ')[0]);
        bodyToSend.put('fact_cod_tra', String.valueOf(intFact.Case__r.tratamiento__r.Cod_tra__c));
        bodyToSend.put('fact_nom_tra', intFact.Case__r.tratamiento__r.Nombre_Facturacion__c);
        bodyToSend.put('fact_medico', intFact.Case__r.Med_Presc__r.Cod_med__c!= null ?  intFact.Case__r.Med_Presc__r.Cod_med__c : ''  + ' - ' + intFact.Case__r.Med_Presc__r.NombreCompleto__c != null ?  intFact.Case__r.Med_Presc__r.NombreCompleto__c : '');
        bodyToSend.put('fact_ingreso_pac', patientIdToIngresoDays.get(intFact.Case__r.AccountId));
        bodyToSend.put('fact_num_presc', intFact.Case__r.Presc_number__c);
        bodyToSend.put('fact_fec_ini_tra', String.valueOf(intFact.Case__r.Fec_ini_tra__c) + 'T00:00:00.000');
        bodyToSend.put('fact_fec_fin_tra', intFact.Case__r.Fec_fin_tra__c != null ? String.valueOf(intFact.Case__r.Fec_fin_tra__c) + 'T00:00:00.000' : '');
        bodyToSend.put('idsalesforce', intFact.Case__r.Id);           

        //Mes actual
        Integer month = Integer.valueOf(intFact.Mes_de_facturacion__c);

        //Año actual
        Integer year = Integer.valueOf(intFact.Anio_de_facturacion__c);

        month = (month==1? 12 : month-1);
        year = (month==1? (year-1) : year);

        //Número de días del mes actual
        Integer daysMonth = Date.daysInMonth(year,month);

        //Si el tratamiento comenzó este mes, cogemos el día de inicio
            if(Integer.valueOf(String.valueOf(intFact.Case__r.Fec_ini_tra__c.format()).split('/')[1]) == month &&  
            Integer.valueOf(String.valueOf(intFact.Case__r.Fec_ini_tra__c.format()).split('/')[2]) == year){
            // bodyToSend.put('fact_fec_ini_per',String.valueOf(Datetime.newInstance(year, month, 1, 0, 0, 0)).replace(' ','T')+'.000');
            bodyToSend.put('fact_fec_ini_per', String.valueOf(intFact.Case__r.Fec_ini_tra__c).split(' ')[0] + 'T00:00:00.000');
        //Si el tratamiento empezó antes de este mes, cogemos el día 1 de este mes     
        }else{
            bodyToSend.put('fact_fec_ini_per',String.valueOf(Datetime.newInstance(year, month, 1, 0, 0, 0)).split(' ')[0] + 'T00:00:00.000');
        }


        //El tratamiento acaba este mes --> Se coge su fecha de finalización
        if(intFact.Case__r.Fec_fin_tra__c != null && (Integer.valueOf(String.valueOf(intFact.Case__r.Fec_fin_tra__c.format()).split('/')[1]) == month &&  
            Integer.valueOf(String.valueOf(intFact.Case__r.Fec_fin_tra__c.format()).split('/')[2]) == year)){
                bodyToSend.put('fact_fec_fin_per', String.valueOf(intFact.Case__r.Fec_fin_tra__c).split(' ')[0] + 'T00:00:00.000');
        //Si el tratamiento no ha terminado, o termina el mes que viene --> se coge el final de este mes
        }else{
            bodyToSend.put('fact_fec_fin_per', String.valueOf(Datetime.newInstance(year, month, Date.daysInMonth(year,month), 0, 0, 0)).split(' ')[0] + 'T00:00:00.000');
        }

        system.debug('Mapa del body a serializar: '+bodyToSend);
        

        req.setBody(JSON.Serialize(bodyToSend));
        system.debug('Body que mando: '+JSON.Serialize(bodyToSend));
        system.debug('LLAMADA A /facturar-sacyl');
        Http http = new Http();
        res = http.send(req);

        WrapperRespuestaFacturacion SACYLResponse;

        if (res.getStatusCode() == 200){
            
            SACYLResponse = (WrapperRespuestaFacturacion) JSON.deserialize(res.getBody(), WrapperRespuestaFacturacion.class);
            if(SACYLResponse.code == 200){
                responseMap.put(false, '');
            }else{
                responseMap.put(true, res.getStatusCode() + ' - ' + SACYLResponse.message);
            }
        }else{
            responseMap.put(true, res.getStatusCode() + ' - ' +res.getBody());
        }

        return responseMap;

    }


    public class WrapperRespuestaFacturacion{
        public Boolean success;
        public Integer code;
        public String message;
    }
}