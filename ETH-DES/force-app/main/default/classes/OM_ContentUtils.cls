/**
 * @description       Utilities for other classes
 * @author            OmegaCRM 
 * @group             Content utilities
**/
public inherited sharing class OM_ContentUtils {

    /**
     * @description Endpoints by provider and service
     */
    public static Map<String, Map<String, String>> endpoints = new Map<String, Map<String, String>> {
        'MicrosoftSharePoint' => new Map<String, String> {
            'token'             => 'https://login.microsoftonline.com/{tenant}/oauth2/v2.0/token',
            'base'              => 'https://graph.microsoft.com/v1.0/sites/{siteid}',
            'newFolder'         => 'https://graph.microsoft.com/v1.0/sites/{siteid}/drives/{driveId}/items/{parentId}/children',
            'uploadSession'     => 'https://graph.microsoft.com/v1.0/sites/{siteid}/drives/{driveId}/items/{parentId}:/{filename}:/createUploadSession',
            'uploadNew'         => 'https://graph.microsoft.com/v1.0/sites/{siteid}/drives/{driveId}/items/{parentId}:/{filename}:/content',
            'uploadExisting'    => 'https://graph.microsoft.com/v1.0/sites/{siteid}/drives/{driveId}/items/{parentId}:/{filename}:/content',
            'list'              => 'https://graph.microsoft.com/v1.0/sites/{siteid}/drives/{driveId}/items',
            'get'               => 'https://graph.microsoft.com/v1.0/sites/{siteid}/drives/{driveId}/items/{id}?select=id,size,name,file,description,folder,sharepointids,parentReference,content.downloadUrl,graph.sourceUrl,createdBy,lastModifiedBy,fileSystemInfo,webUrl&expand=listItem,thumbnails',
            'update'            => 'https://graph.microsoft.com/v1.0/sites/{siteid}/drives/{driveId}/items/{id}?select=id,name,description,file,folder,sharepointIds,parentReference&expand=listItem',
            'preview'           => 'https://graph.microsoft.com/v1.0/sites/{siteid}/drives/{driveId}/items/{id}/preview'
        },
        'MicrosoftOneDrive' => new Map<String, String> {
            'token'             => '{baseUrlToken}',
            'base'              => 'https://graph.microsoft.com/v1.0',
            'newFolder'         => 'https://graph.microsoft.com/v1.0/{oneDrivePath}/items/{parentId}/children',
            'uploadSession'     => 'https://graph.microsoft.com/v1.0/{oneDrivePath}/items/{parentId}:/{filename}:/createUploadSession',
            'uploadNew'         => 'https://graph.microsoft.com/v1.0/{oneDrivePath}/items/{parentId}:/{filename}:/content',
            'uploadExisting'    => 'https://graph.microsoft.com/v1.0/{oneDrivePath}/items/{parentId}:/{filename}:/content',
            'list'              => 'https://graph.microsoft.com/v1.0/{oneDrivePath}/items',
            'get'               => 'https://graph.microsoft.com/v1.0/{oneDrivePath}/items/{id}',
            'update'            => 'https://graph.microsoft.com/v1.0/{oneDrivePath}/items/{id}',
            'preview'           => 'https://graph.microsoft.com/v1.0/{oneDrivePath}/items/{id}/preview'
        },
        'GoogleDrive' => new Map<String, String> {
            'token'             => 'https://oauth2.googleapis.com/token',
            'base'              => 'https://www.googleapis.com/drive/v3',
            'list'              => 'https://www.googleapis.com/drive/v3/files',
            'newFolder'         => 'https://www.googleapis.com/drive/v3/files',
            'uploadSession'     => 'https://www.googleapis.com/upload/drive/v3/files/?uploadType=resumable',
            'uploadNew'         => 'https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart',
            'uploadExisting'    => 'https://www.googleapis.com/upload/drive/v3/files/{id}?uploadType=multipart',
            'get'               => 'https://www.googleapis.com/drive/v3/files/{id}',
            'update'            => 'https://www.googleapis.com/drive/v3/files/{id}',
            'preview'           => 'https://www.googleapis.com/drive/v3/files/{id}'
        }
    };

    /**
    * @description Get a valid mime type from a "FileType" field value con ContentVersion
    * @author OmegaCRM  | 02-19-2021 
    * @param cvFileType The FileType field value
    * @return String The matching mimetype
    **/
    public static String getMimeForContentVersionType(String cvFileType) {
        switch on cvFileType {
            when 'JPG' {
                return 'image/jpg';
            }
            when 'WORD_X' {
                return 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
            }
            when else {
                return 'application/octet-stream';
            }
        }
    }

    /**
    * @description Obtains the concrete implmentation class to use based on the provider
    * @author OmegaCRM  | 01-20-2021 
    * @param provider The desired provider
    * @return OM_ContentIntegrationInterface The concrete implmentation
    **/
    public static OM_ContentIntegrationInterface getIntegrationClass(String provider) {
        OM_ContentIntegrationInterface concreteClass;
        Type t;
        if(provider == 'MicrosoftSharePoint') {
            t = Type.forName('OM_ContentMicrosoftIntegration');
            concreteClass = (OM_ContentIntegrationInterface) t.newInstance();
        }

        if(provider == 'MicrosoftOneDrive') {
            t = Type.forName('OM_ContentOneDriveIntegration');
            concreteClass = (OM_ContentIntegrationInterface) t.newInstance();
        }

        if(provider == 'GoogleDrive') {
            t = Type.forName('OM_ContentGoogleIntegration');
            concreteClass = (OM_ContentIntegrationInterface) t.newInstance();
        }
        
        return concreteClass;
    }

    /**
    * @description Replaces all invalid characters
    * @author OmegaCRM  | 02-23-2021 
    * @param fileOrFolderName The file or folder name
    * @return String The cleaned result
    **/
    public static String stripInvalidCharactersSharepoint(String fileOrFolderName) {

        String result = fileOrFolderName;

        result = result.replace('~', '')
            .replace('#', '')
            .replace('&', '')
            .replace('*', '')
            .replace('{', '')
            .replace('}', '')
            .replace(':', '')
            .replace('(', '')
            .replace(')', '')
            .replace('+', '')
            .replace('|', '')
            .replace('..', '')
            .replace('\\', '');

        if (result.endsWith('.')) {
            result = result.substring(0, result.length() - 1);
        }

        return result;
    }

    /**
    * @description Gets an enpoint for a provider and a service, replacing needed variables to work properly
    * @author OmegaCRM  | 01-20-2021 
    * @param settings The settings to use, from where the provider is obtained
    * @param service The desired service endpoint
    * @param content The content wrapper to work with. Used to replace some variables when needed
    * @return string The final endpoint 
    **/
    public static string getEndpoint(OM_ContentSetting__c settings, String service, OM_ContentWrapper content) {
        String rawendp = endpoints.get(settings.OM_Provider__c).get(service);
        if (settings.OM_Is_personal_account__c == false) {
            rawendp = rawendp.replace('{baseUrlToken}', 'https://login.microsoftonline.com/{tenant}/oauth2/v2.0/token');
        } else {
            rawendp = rawendp.replace('{baseUrlToken}', 'https://login.live.com/oauth20_token.srf');
        }

        if (settings.OM_Tenant__c != null) {
            rawendp = rawendp.replace('{tenant}', settings.OM_Tenant__c);
        }

        if (settings.OM_Use_personal_drive__c == true) {
            rawendp = rawendp.replace('{oneDrivePath}', 'me/drive');
        } else {
            rawendp = rawendp.replace('{oneDrivePath}', 'drives/{driveId}');
        }

        if(content != null) {
            if(content.siteId != null) {
                rawendp = rawendp.replace('{siteid}',content.siteId);
            }

            if(content.parentId != null) {
                rawendp = rawendp.replace('{parentId}', content.parentId);
            }
            if(content.id != null) {
                rawendp = rawendp.replace('{id}', content.id);
                System.debug('before ' + rawendp);

                if(rawendp.contains('items/:')) {
                    rawendp = rawendp.replace('items/:', 'root:');
                    System.debug('after ' + rawendp);

                }
            }
            if(content.name != null) {
                rawendp = rawendp.replace('{filename}', content.name);
                System.debug('before ' + rawendp);
                // Fro Microsoft, allow usign relative paths from parent Id in filename
                if(rawendp.contains(':/:/')) {
                    rawendp = rawendp.replace(':/:/', ':/');
                    System.debug('after ' + rawendp);
                }
            }
            if(content.driveId != null) {
                rawendp = rawendp.replace('{driveId}', content.driveId);
            }
        }

        if (settings.OM_Site_id__c != null) {
            rawendp = rawendp.replace('{siteid}', settings.OM_Site_id__c);
        }

        if (settings.OM_Drive_id__c != null) {
            rawendp = rawendp.replace('{driveId}', settings.OM_Drive_id__c);
        }


        return rawendp;
    }

    /**
    * @description Gets all endpoints for a provider
    * @author OmegaCRM  | 01-20-2021 
    * @param settings The settings to use, from where the provider is obtained
    * @return Map<String, String> The final endpoints map by service for selected provider
    **/
    public static Map<String, String> getAllEndpoints(OM_ContentSetting__c settings) {
        Map<String, String> endps = endpoints.get(settings.OM_Provider__c);
        Map<String, String> finalEnpoints = endpoints.get(settings.OM_Provider__c);
        for(String service : endps.keySet()) {
            finalEnpoints.put(service, endps.get(service));
            //finalEnpoints.put(service, getEndpoint(settings, service, null));
        }
        return finalEnpoints;
    }
}