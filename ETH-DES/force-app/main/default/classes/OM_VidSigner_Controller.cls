/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 08-24-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
public with sharing class OM_VidSigner_Controller {
    
    public static String nombrePaciente {get;set;}

    @AuraEnabled
    public static String getVisitType(String recordId){
        system.debug('Estoy en getVisitType y recordId vale? '+recordId);
        ServiceAppointment sApp = [SELECT Id, Worktype.Name FROM ServiceAppointment WHERE Id =: recordId];

        String visitTypeReturn;

        if(String.valueOf(sApp.Worktype.Name).containsIgnoreCase('fin')){
            visitTypeReturn = 'Fin';
        }else if(String.valueOf(sApp.Worktype.Name).containsIgnoreCase('alta')){
            visitTypeReturn = 'Alta';
        }else{
            visitTypeReturn = 'Otras';
        }
        

        return visitTypeReturn;
    }

    @AuraEnabled
    public static String getDNIPaciente(String recordId){
        ServiceAppointment sApp = [SELECT Id, Account.NIF__c FROM ServiceAppointment WHERE Id =: recordId];

        return sApp.Account.NIF__c;
    }

    @AuraEnabled(cacheable=true)
    public static Boolean getVisitStatusEnCurso(String recordId){
        system.debug('Estoy en getVisitStatusEnCurso y recordId vale? '+recordId);
        ServiceAppointment sApp = [SELECT Id, Status FROM ServiceAppointment WHERE Id =: recordId];

        Boolean statusEnCurso = false;

        if(sApp.Status == 'En curso'){
            statusEnCurso = true;
        }        

        return statusEnCurso;
    }
    
    public static Consentimiento_Contenido__mdt getContentInfo (String lan, String cType) {

	    // String cTypeCode = (cType == Label.Publico) ? 'PB' : 'PR';
        // Por el momento, ponemos siempre público
	    String cTypeCode = 'PB';
        
        List<Consentimiento_Contenido__mdt> contentInfo = new List<Consentimiento_Contenido__mdt>();
        contentInfo = [SELECT Id, TutorBox_Text__c, TutorBox_Title__c, WhatsappForm_Text__c, WhatsappForm_Title__c, NewsletterForm_Text__c, NewsletterForm_Title__c, Answer_No__c, Answer_Yes__c, VideollamadaForm_Text__c, VideollamadaForm_Title__c
                       	FROM Consentimiento_Contenido__mdt WHERE Contract_Type__c =:cTypeCode AND Language__c =: lan];
        
        if(!contentInfo.isEmpty()){
            return contentInfo.get(0);
        }
        else{
            String errorString = Label.CMT_ERROR.replace('[1]',cType).replace('[2]',lan);
            throw new customException(errorString);
        }
    }
    
    
    
    @AuraEnabled
    public static String getLanguageSJON() {

        String lanString = '[';
        
        Schema.DescribeFieldResult describeResult = Consentimiento_Contenido__mdt.Language__c.getDescribe();
        List<Schema.PicklistEntry> entries = describeResult.getPicklistValues();
        
        for (Schema.PicklistEntry pEntry : entries) {
            if (pEntry.isActive()) {
                //languageList.add(new OptionClass(pEntry.getValue(), pEntry.getLabel()));
                lanString += '{"value":"' + pEntry.getValue() + '","label":"' + pEntry.getLabel() + '"},';
                
            }
        }
        
        if(String.isNotBlank(lanString)){
            lanString = lanString.removeEnd(',');
            lanString += ']';
        }
                    
        return lanString;
	}

    @AuraEnabled
    public static String getPatientInfoJSON(String recordId){ 
        system.debug('Entra en getPatientInfoJSON');
        String nif = '';
        String cType = '';
        String cliAtlasId = '';//405
        
        String patientString = '';
        try{
            ServiceAppointment p = [select Id, Account.NIF__c, Tratamiento__c,
            Tratamiento__r.AccountId, Account.Name, Account.ParentId from ServiceAppointment where Id = :recordId limit 1];
            system.debug('Service appointment que saco: '+p);

            nif = p.Account.NIF__c != NULL ? p.Account.NIF__c : '';
            nombrePaciente = p.Account.Name;
            cliAtlasId = p.Tratamiento__r.AccountId;
            system.debug('nif '+nif);
            system.debug('nombrePaciente '+nombrePaciente);
            system.debug('cliAtlasId '+cliAtlasId);
            
            //cType = (String.isNotBlank(v.Prescripcion__r.Paciente__r.Es_Publico__c) && v.Prescripcion__r.Paciente__r.Es_Publico__c == 'S') ? Label.Publico : Label.Privado;
            //Hasta que se pueda sincronizar el campo Es_Publico__c con Atlas, se hace según una lista establecida que determina si es público.
            Set<String> publicTypes = new Set<String>(Label.IsPublic.split(';'));
            system.debug('publicTypes '+publicTypes);
            
            cType = (p.Account.ParentId != NULL && publicTypes.contains(p.Tratamiento__r.AccountId)) ? Label.Publico : Label.Privado;
            system.debug('cType '+cType);
            
            patientString = '{"nif":"' + nif + '","cType":"' + cType + '","cliAtlasId":"' + cliAtlasId + '"}';
            system.debug('patientString '+patientString);
        
        }catch(exception e){
            system.debug('error en getPatientInfoJSON: '+e.getMessage());
        }

        system.debug('Devuelvo esto: '+patientString);

        return patientString;
    }

    //Cambia el botón a Prescripción (04/11/2019)
    /*@AuraEnabled
    public static Boolean updateNIF(String recordId, String nif){        
        try{
            Visita__c v = [select Id, NIF__c from Visita__c where Id = :recordId limit 1];
			
            v.NIF__c = nif;
            
            update v;
        
        }catch(exception e){
            system.debug('error en updateNIF: '+e.getMessage());
            return false;
        }

        return true;
    }*/

    //devuelve la vf en base64
    public static String getPage(String recordId, String nif, String doc, String lan, String cType, String city, String cName, String nif_tutor, String tutorType){              
        /*String pageName;
            if(doc == 'cons') pageName = 'consentimiento';
            else if(doc == 'baja') pageName = 'baja_voluntaria';*/

        system.debug('Entra en getPage');
        
        // String pageName = (doc == 'cons') ? 'consentimiento' : ((doc == 'baja') ? 'baja_voluntaria' : ((doc == 'compOsasun') ? 'Compromiso_Osasunbidea_Page' : ''));         
        String pageName = (doc == 'cons') ? 'consentimiento' : ((doc == 'baja') ? 'baja_voluntaria' : ((doc == 'compOsasun') ? 'Compromiso_Osasunbidea_Page' : ((doc == 'parte_Visita') ? 'parte_visita' : '')));         
        // String contractType = (cType == Label.Publico) ? 'PB' : 'PR';
        String contractType = 'PB';

        system.debug('nombre pagina: '+pageName);

        PageReference pageRef = new PageReference('/apex/'+pageName);
            pageRef.setRedirect(true);
            //pageRef.getParameters().put('idvisita', recordId);
            pageRef.getParameters().put('idprescripcion', recordId);
            pageRef.getParameters().put('nifPaciente', nif);
            pageRef.getParameters().put('lan', lan);
            pageRef.getParameters().put('cType', contractType);
        	pageRef.getParameters().put('city', city);
        	pageRef.getParameters().put('cName', cName);
            pageRef.getParameters().put('nifT', nif_tutor);
            
            if(String.isNotBlank(tutorType)){
                pageRef.getParameters().put('cTutor', tutorType);
            }
            String out;

            if(Test.isRunningTest()) {
                if(doc == 'cons'){
                    out = EncodingUtil.base64encode(blob.valueOf('Unit.Test_Cons'));
                }else if(doc == 'baja'){
                    out = EncodingUtil.base64encode(blob.valueOf('Unit.Test_Baja'));    
                }else if(doc == 'compOsasun'){
                    out = EncodingUtil.base64encode(blob.valueOf('Unit.Test_Comp'));                     
                }else if(doc == 'parte_visita'){
                    out = EncodingUtil.base64encode(blob.valueOf('Unit.Test_parte_visita'));                     
                }
            }else{
                system.debug('pageRef.getContent() '+pageRef.getContent());
                out = EncodingUtil.base64encode(pageRef.getContent());
            }

        return out;
    }

    //devuelve la list<Signer> con el paciente
    public static List<Signer> getSigners(String recordId, String nif, String cName, String nifT, String nPaciente, Consentimiento_Contenido__mdt cInfo, String doc){              
        List<Signer> out = new List<Signer>();
	
        String signerNIF;
        String str_Anchor;
        String str_NamePaciente;
        Form formsInfo;
        
        if(doc == 'cons'){
            
            if(String.isBlank(cName) || String.isBlank(nifT)){//No es tutor

                //Cambia el botón a Prescripción (04/11/2019)
                str_NamePaciente = [select Id, Account.Name from ServiceAppointment 
                                        where Id =: recordId].Account.Name;
                signerNIF = nif;
                //utilizamos la palabra Cuadro_Firma en el documento para ubicar la firma. Tiene que ser una palabra que no aparezca en el resto del texto.                            
                str_Anchor = 'Cuadro_Firma_Paciente';
                formsInfo = getForm(cInfo,null,null,null, false);
                
            }else{
                str_NamePaciente = cName;
                
                signerNIF = nifT;
                //utilizamos la palabra Cuadro_Firma en el documento para ubicar la firma. Tiene que ser una palabra que no aparezca en el resto del texto.                            
                str_Anchor = 'Cuadro_Firma_Tutor';
                
                formsInfo = getForm(cInfo,nPaciente,cName,nifT, true);
                
            }
            
            //out.add(new Signer(str_NamePaciente, 'NIF', signerNIF, new Visible(1, 60, 25, str_Anchor),getForm(),'ESTEVETEIJINHEALTHCARE-602E-VIDM0019','es'));
            out.add(new Signer(str_NamePaciente, 'NIF', signerNIF, new Visible(1, 60, 25, str_Anchor),formsInfo,null,null));
        }else if (doc == 'baja'){//Es baja
            //Cambia el botón a Prescripción (04/11/2019)
            str_NamePaciente = [select Id, Account.Name from ServiceAppointment 
                                        where Id =: recordId].Account.Name;

            signerNIF = nif;
            //utilizamos la palabra Cuadro_Firma en el documento para ubicar la firma. Tiene que ser una palabra que no aparezca en el resto del texto.                            
            str_Anchor = 'Cuadro_Firma_Paciente';
            out.add(new Signer(str_NamePaciente, 'NIF', signerNIF, new Visible(1, 60, 25, str_Anchor),null,null,null));

        }else if (doc == 'compOsasun'){
            if(String.isBlank(cName) || String.isBlank(nifT)){//No es tutor

                //Cambia el botón a Prescripción (04/11/2019)
                str_NamePaciente = [SELECT Id, Account.Name FROM ServiceAppointment WHERE Id =: recordId].Account.Name;
                signerNIF = nif;
            }else{
                str_NamePaciente = cName;
                signerNIF = nifT;
            }
            
            str_Anchor = 'Espacio_Firma';
            out.add(new Signer(str_NamePaciente, 'NIF', signerNIF, new Visible(1, 60, 25, str_Anchor),null,null,null));
        }else if(doc == 'parte_visita'){
            system.debug('Entra en el nuevo else if de parte visita');
            //Cambia el botón a Prescripción (04/11/2019)
            str_NamePaciente = [select Id, Account.Name from ServiceAppointment 
                                        where Id =: recordId].Account.Name;

            signerNIF = nif;
            //utilizamos la palabra Cuadro_Firma en el documento para ubicar la firma. Tiene que ser una palabra que no aparezca en el resto del texto.                            
            str_Anchor = 'Cuadro_Firma_Paciente';
            out.add(new Signer(str_NamePaciente, 'NIF', signerNIF, new Visible(1, 60, 25, str_Anchor),null,null,null));
        }
        return out;
    }

    //devuelve el JSON para la url del vidsigner con todos los datos
    @AuraEnabled
    public static Map<String, Object> getJSONVidSigner(String recordId, String nif, String doc, 
                                                       String lan, String cType, String city, String cName, 
                                                       String nif_tutor, String tutorType, String operationType/*, String deviceType*/){    
        system.debug('Entra en getJSONVidSigner');
        // system.debug('Tipo de dispositivo que llega '+deviceType); 
        //NIF firmante
        String str_FileName = (String.isNotBlank(cName) && String.isNotBlank(nif_tutor)) ? nif_tutor : nif;

        //Cambia el botón a Prescripción (04/11/2019)
        /*Visita__c v = [select Id, Prescripcion__r.AtlasId__c, Prescripcion__r.cod_tra__r.Identificador__c,
                                Prescripcion__r.Paciente__r.IdAnonimo__c, Prescripcion__r.Paciente__r.Name 
                                from Visita__c where Id = :recordId limit 1];*/
        ServiceAppointment p = [select Id, Tratamiento__c,
                                Account.IdAnonimo__c, Account.Name, Tratamiento__r.AccountId
                                from ServiceAppointment where Id = :recordId limit 1];
        
        Consentimiento_Contenido__mdt cInfo = new Consentimiento_Contenido__mdt();

        if(doc == 'cons'){
            operationType = 'Consentimiento';
            //Cambia el botón a Prescripción (04/11/2019)
        	//str_FileName += '_CONS_'+v.Prescripcion__r.Paciente__r.IdAnonimo__c+'.pdf'; //  “<#DNI>_CONS_<#ID_ANONYM>.pdf”   
            str_FileName += '_CONS_'+ p.Account.IdAnonimo__c+'.pdf'; //  “<#DNI>_CONS_<#ID_ANONYM>.pdf”  
            cInfo = getContentInfo (lan, cType);
            
        }else if(doc == 'baja'){ //  “<#DNI>_BAJA_<#COD_CLI>_<#COD_TRA>_<#ID_LIN>.pdf” 
            operationType = 'Baja';

            //Cambia el botón a Prescripción (04/11/2019)
            /*String ss = (v.Prescripcion__r.cod_tra__r.Identificador__c).substringAfter('tratamiento_');
            str_FileName += '_BAJA_'+ss+'_'+v.Prescripcion__r.AtlasId__c+'.pdf';*/

            //CAMBIO Sergio
            // String ss = (p.Tratamiento__c).substringAfter('tratamiento_');
            String ss = String.valueOf(p.Tratamiento__c);
            //FIN CAMBIO Sergio
            str_FileName += '_BAJA_'+ss+'_'+p.Tratamiento__r.AccountId+'.pdf';
            
            //Comprobamos si existe el custom metadata type
            List<Baja_Voluntaria__mdt> cmt_baja = new List<Baja_Voluntaria__mdt>([SELECT Id FROM Baja_Voluntaria__mdt WHERE Language__c =: lan]);
            if(cmt_baja.isEmpty()){
                String errorString = Label.CMT_BAJA_ERROR.replace('[1]', lan);
            	throw new customException(errorString);
            }

        }else if(doc == 'parte_visita'){
            
            operationType = 'Visita';

            String ss = String.valueOf(p.Tratamiento__c);
            // str_FileName += '_Visita_'+ss+'_'+p.Tratamiento__r.AccountId+'.pdf'; 
            str_FileName += '_Visita_'+ p.Id +'.pdf'; 

        
        //405 - Osasunbidea
        }else if(doc == 'compOsasun'){ //  <NIF_FIRMANTE>_COS_<id_paciente_interno>.pdf
            str_FileName +=  '_COS_' + p.Account.IdAnonimo__c + '.pdf';
        }

        system.debug('Tipo de operación '+operationType);
        system.debug('URL FORMADA: https://des-vidsignerdes.cs106.force.com/VidSignerDes/services/apexrest/PruebaVidSigner/'+ OperationType + '/' + recordId);

        //Cambia el botón a Prescripción (04/11/2019)
        String nPaciente = p.Account.Name;
        String str_AdditionalData = ''; 
        
        //Consentimiento_Contenido__mdt cInfo = getContentInfo (lan, cType);

        system.debug('GetSigners: '+getSigners(recordId, nif, cName, nif_tutor, nPaciente, cInfo, doc));

        eth__c ethCS = eth__c.getInstance(UserInfo.getProfileId());
        String str_urlSiteCallback = ethCS.url_site_callback__c; 
        
        Map<String,Object> m = new Map<String,Object>();
            m.put('Filename', str_FileName);
            m.put('DocContent', getPage(recordId, nif, doc, lan, cType, city, cName, nif_tutor, tutorType));
            m.put('Signers', getSigners(recordId, nif, cName, nif_tutor, nPaciente, cInfo, doc));
        	m.put('AdditionalData', str_AdditionalData);
        	m.put('OrderedSignatures', true);
        	m.put('DocusignInfo', null);
        	// m.put('NotificationURL', null);  
        	// m.put('NotificationURL', 'https://eth--fullqa.lightning.force.com/services/apexrest/PruebaVidSigner/');  
        	m.put('NotificationURL', str_urlSiteCallback + '/services/apexrest/PruebaVidSigner/'+ OperationType + '/' + recordId);  
            system.debug('URL que monto del callback '+str_urlSiteCallback + '/services/apexrest/PruebaVidSigner/'+ OperationType + '/' + recordId);
            
        	m.put('Tag', null);
        	// m.put('eMail', 'sergio.diaz@wearevanguard.eu');
        
            // system.debug('Visualforce base64 '+getPage(recordId, nif, doc, lan, cType, city, cName, nif_tutor, tutorType);
            system.debug('Mapa vidSigner '+m);
        String jsResponse = JSON.serialize(m).replace('Id_RB','Id').replace('Id_CH','Id');
        //String jsResponse = JSON.serialize(m);
        system.debug('-----> getJSONVidSigner: ' + jsResponse);
        Map<String, Object> result = peticionFirma(jsResponse);
        system.debug('Result getJSONVidSigner: '+result);
        return result;
    }

    //hace la primera llamada a la api de vidsigner
    public static Map<String, Object> peticionFirma(String js){
        
        Map<String, Object> out = new Map<String, Object>();

        system.debug('MEtadato Vid');
        //Obtenemos la información del custom setting sin necesidad de hacer una query
        eth__c ethCS = eth__c.getInstance(UserInfo.getProfileId());
        String str_url = ethCS.vidsigner_url__c; 
        String str_SubscriptionName = ethCS.vidsigner_SubscriptionName__c; 
        String str_SubscriptionPass = ethCS.vidsigner_SubscriptionPass__c ; 
        
        String name_pass64 = EncodingUtil.base64encode(Blob.valueOf(str_SubscriptionName+':'+str_SubscriptionPass)); 
        
        
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();

        req.setEndpoint(str_url);
        req.setMethod('POST');
        req.setHeader('Content-Type','application/json'); 
        req.setHeader('Authorization','Basic '+name_pass64);
        req.setHeader('Cache-Control','no-cache');   

        system.debug('Token '+name_pass64);
        system.debug('str_url '+str_url);
        // system.debug('str_SubscriptionName '+str_SubscriptionName);
        // system.debug('str_SubscriptionPass '+str_SubscriptionPass);
        system.debug('Headers:');
        system.debug('Content-Type - application/json');
        system.debug('Authorization - Basic '+name_pass64);
        system.debug('Cache-Control - no-cache');
                        
        req.setBody(js);
          
        try{
            res = http.send(req);
            System.debug('---> res: ' + res.toString());
            System.debug('---> res: ' + res.getBody());

            out.put('success', true);
        }catch(exception e){
            System.debug('---> Error: ' + e.getMessage());
            out.put('success', false);
            out.put('message', 'Error: '+e.getMessage());
        }
        return out;
    } 
    
    public static Form getForm(Consentimiento_Contenido__mdt cInfo, String nombrePaciente, String nombreTutor, String nifTutor, Boolean esTutor ){              
        
        Form out= new Form();
        
        //Layout
        Layout lObj = new Layout('Helvetica', '10'); 
        
        //Positions
        PositionObj positionGR = new PositionObj(null, null, 100, 1,'QUESTION_WS');//posx, posy, sizex, page, ANCHOR
        PositionObj positionRB = new PositionObj(null, null, 150, 1,'ANSWER_WS');
        PositionObj positionCH1 = new PositionObj(null, null, 100, 1,'CW_1');
        PositionObj positionCH2 = new PositionObj(null, null, 100, 1,'CW_2');
        
        PositionObj positionGR_NL = new PositionObj(null, null, 100, 1,'QUESTION_NL');//posx, posy, sizex, page, ANCHOR
        PositionObj positionRB_NL = new PositionObj(null, null, 140, 1,'ANSWER_NL');
        PositionObj positionCH1_NL = new PositionObj(null, null, 100, 1,'CN_1');
        PositionObj positionCH2_NL = new PositionObj(null, null, 100, 1,'CN_2');

        PositionObj positionGR_VL = new PositionObj(null, null, 100, 1,'QUESTION_VL');//posx, posy, sizex, page, ANCHOR
        PositionObj positionRB_VL = new PositionObj(null, null, 140, 1,'ANSWER_VL');
        PositionObj positionCH1_VL = new PositionObj(null, null, 100, 1,'CV_1');
        PositionObj positionCH2_VL = new PositionObj(null, null, 100, 1,'CV_2');
        
        //Titles
        TitleObj titleGroup = new TitleObj(cInfo.WhatsappForm_Title__c, positionGR);
        TitleObj titleRB = new TitleObj(cInfo.WhatsappForm_Text__c, positionRB);
        TitleObj titleCH1 = new TitleObj(cInfo.Answer_Yes__c, positionCH1);
        TitleObj titleCH2 = new TitleObj(cInfo.Answer_No__c, positionCH2);
        
        TitleObj titleGroup_NL = new TitleObj(cInfo.NewsletterForm_Title__c, positionGR_NL);
        TitleObj titleRB_NL = new TitleObj(cInfo.NewsletterForm_Text__c, positionRB_NL);
        TitleObj titleCH1_NL = new TitleObj(cInfo.Answer_Yes__c, positionCH1_NL);
        TitleObj titleCH2_NL = new TitleObj(cInfo.Answer_No__c, positionCH2_NL);

        TitleObj titleGroup_VL = new TitleObj(cInfo.VideollamadaForm_Title__c, positionGR_VL);
        TitleObj titleRB_VL = new TitleObj(cInfo.VideollamadaForm_Text__c, positionRB_VL);
        TitleObj titleCH1_VL = new TitleObj(cInfo.Answer_Yes__c, positionCH1_VL);
        TitleObj titleCH2_VL = new TitleObj(cInfo.Answer_No__c, positionCH2_VL);
        
        
        //Choices
        List<ChoiceObj> choicesList = new List<ChoiceObj>();
        ChoiceObj chObj1 = new ChoiceObj(titleCH1);
        ChoiceObj chObj2 = new ChoiceObj(titleCH2);
        choicesList.add(chObj1);
        choicesList.add(chObj2);
        
        List<ChoiceObj> choicesList_NL = new List<ChoiceObj>();
        ChoiceObj chObj1_NL = new ChoiceObj(titleCH1_NL);
        ChoiceObj chObj2_NL = new ChoiceObj(titleCH2_NL);
        choicesList_NL.add(chObj1_NL);
        choicesList_NL.add(chObj2_NL);

        List<ChoiceObj> choicesList_VL = new List<ChoiceObj>();
        ChoiceObj chObj1_VL = new ChoiceObj(titleCH1_VL);
        ChoiceObj chObj2_VL = new ChoiceObj(titleCH2_VL);
        choicesList_VL.add(chObj1_VL);
        choicesList_VL.add(chObj2_VL);
           
        //Radiobuttons
        List<RadiobuttonObj> rbList = new List<RadiobuttonObj>();
        RadiobuttonObj rb = new RadiobuttonObj('idrb_WS', titleRB, choicesList, null,0);
        rbList.add(rb);
        
        List<RadiobuttonObj> rbList_NL = new List<RadiobuttonObj>();
        RadiobuttonObj rb_NL = new RadiobuttonObj('idrb_NL', titleRB_NL, choicesList_NL, null,0);
        rbList_NL.add(rb_NL);

        List<RadiobuttonObj> rbList_VL = new List<RadiobuttonObj>();
        RadiobuttonObj rb_VL = new RadiobuttonObj('idrb_VL', titleRB_VL, choicesList_VL, null,0);
        rbList_VL.add(rb_VL);
        
        //Groups
        List<GroupObj> groupList = new List<GroupObj>();
        GroupObj gr = new GroupObj(titleGroup, rbList, null, null);
        GroupObj gr_NL = new GroupObj(titleGroup_NL, rbList_NL, null, null);
        GroupObj gr_VL = new GroupObj(titleGroup_VL, rbList_VL, null, null);
        groupList.add(gr);
        groupList.add(gr_NL);
        groupList.add(gr_VL);
        
        //Si es tutor añadimos la checkbox
        if(esTutor){
            
        	//Position
            PositionObj positionGR_CHB = new PositionObj(null, null, 160, 1,'CB_TITLE');
            PositionObj positionCH = new PositionObj(null, null, 160, 2,'Check_Tutor');
            
            String boxTitle = cInfo.TutorBox_Title__c;
            boxTitle = boxTitle.replace('[PATIENT_NAME]', nombrePaciente);
            boxTitle = boxTitle.replace('[TUTOR_NAME]', nombreTutor);
            boxTitle = boxTitle.replace('[TUTOR_DNI]', nifTutor);
            TitleObj titleGroup_CHB = new TitleObj(boxTitle, positionGR_CHB);
            TitleObj titleCH = new TitleObj(cInfo.TutorBox_Text__c, positionCH);
            
            //Checkboxes
            List<CheckboxObj> chList = new List<CheckboxObj>();
            CheckBoxObj chb = new CheckBoxObj('id_ch', titleCH, false, null);
            chList.add(chb);
            
            GroupObj gr_CHB = new GroupObj(titleGroup_CHB, null, chList, null);
            groupList.add(gr_CHB);
        }
        
        //Sections
        List<Section> sectionList = new List<Section>();
        Section sectObj = new Section();
        sectObj.Groups = groupList;
        sectionList.add(sectObj);
        out.Layout = lObj;
        out.Sections = sectionList;    
        
        return out;
    }
    
    public class Signer {
        public String SignerName {get;set;} 
        public String TypeOfID {get;set;} 
        public String NumberID {get;set;} 
        public String DeviceName {get;set;}
        public Visible Visible {get;set;} 
        public Form Form {get;set;} 
        public String Language {get;set;}
        public String SignerGUI {get;set;}

        public Signer(String str_SignerName, String str_TypeOfID, String str_NumberID, Visible obj_Visible, Form obj_Form, String str_DeviceName, String str_Language){
            this.SignerName = str_SignerName;
            this.TypeOfID = str_TypeOfID;
            this.NumberID = str_NumberID;
            this.Visible = obj_Visible;
            this.Form = obj_Form;
            this.DeviceName = str_DeviceName;
            this.Language = str_Language;
        }
    }

    public class Visible {
        public Integer Page {get;set;} 
        public Integer SizeX {get;set;}
        public Integer SizeY {get;set;}
        public String Anchor {get;set;}

        public Visible(Integer int_Page, Integer int_SizeX, Integer int_SizeY, String str_Anchor){
            this.Page = int_Page;
            this.SizeX = int_SizeX;
            this.SizeY = int_SizeY;
            this.Anchor = str_Anchor;
        }
    }
    
    public class Form {
        public Layout Layout {get;set;} 
        public List<Section> Sections {get;set;} 
                
	}
    public class Layout {
        public String FontFamily {get;set;} 
        public String FontSize {get;set;}
        
        public Layout(String str_ff, String str_fs){
            this.FontFamily = str_ff;
            this.FontSize = str_fs;
        }    
    }
    
    public class Section {
        public List<GroupObj> Groups {get;set;} 
	}
	
	public class GroupObj {
       
        public TitleObj Title {get;set;}
        public List<RadiobuttonObj> RadioButtons {get;set;} 
        public List<CheckBoxObj> CheckBoxes {get;set;} 
        public List<TextBoxObj> TextBoxes {get;set;} 
        
        public GroupObj(TitleObj obj_Title, List<RadiobuttonObj> list_RadioButtons, List<CheckBoxObj> list_CheckBoxes, List<TextBoxObj> list_TextBoxes){
            if(list_RadioButtons != NULL){
                this.RadioButtons = new List<RadiobuttonObj>();
                this.RadioButtons.addAll(list_RadioButtons);
            }
            
            if(list_CheckBoxes != NULL){
                this.CheckBoxes = new List<CheckBoxObj>();
                this.CheckBoxes.addAll(list_CheckBoxes);
            }
            
            if(list_TextBoxes != NULL){
                this.TextBoxes = new List<TextBoxObj>();
                this.TextBoxes.addAll(list_TextBoxes);
            }
            
			this.Title = obj_Title;
        }
	}
    
    public class TitleObj {
		public String Text {get;set;}
		public PositionObj Position {get;set;}
        //public String Anchor {get;set;}
		
		//public TitleObj(String str_Text_T, PositionObj obj_Position, String str_Anchor){
		public TitleObj(String str_Text, PositionObj obj_Position){
			this.Text = str_Text;
			this.Position = obj_Position;
            //this.Anchor = str_Anchor;
        }
	}
	
	public class RadiobuttonObj {
        
		public String Id_RB {get;set;}
		public TitleObj Title {get;set;}
        public List<ChoiceObj> Choices {get;set;}  
		public ConditionObj Condition {get;set;}
		public Integer SelectedChoice {get;set;}
		
		public RadiobuttonObj(String str_Id_RB, TitleObj obj_Title, List<ChoiceObj> list_Choices, ConditionObj obj_Condition,Integer int_SelectedChoice){
			this.Id_RB = str_Id_RB;
			this.Title = obj_Title;
			this.Choices = list_Choices;
			this.Condition = obj_Condition;
			this.SelectedChoice = int_SelectedChoice;
            
            if(list_Choices != NULL){
                this.Choices = new List<ChoiceObj>();
                this.Choices.addAll(list_Choices);
            }
        }
    }
    
    public class CheckBoxObj {
        
		public String Id_CH {get;set;}
		public TitleObj Title {get;set;}
        public ConditionObj Condition {get;set;}
        public Boolean Response {get;set;}
		
		public CheckBoxObj(String str_Id_CH, TitleObj obj_Title, Boolean bl_Response, ConditionObj obj_Condition){
			this.Id_CH = str_Id_CH;
			this.Title = obj_Title;
			this.Response = bl_Response;
            this.Condition = obj_Condition;
        }
	}
    
    public class TextBoxObj {
        /*
		public String Id_TX {get;set;}
		public TitleObj Title {get;set;}
        public Integer MaxLines {get;set;}
		
		public TextBoxObj(String str_Id_TX, TitleObj obj_Title, Integer int_MaxLines){
			this.Id_TX = str_Id_TX;
			this.Title = obj_Title;
			this.MaxLines = int_MaxLines;
        }*/
    }
    
    public class PositionObj {
        
		public Integer PosX {get;set;}
		public Integer PosY {get;set;}
		public Integer SizeX {get;set;}
		public Integer Page {get;set;}
        public String Anchor {get;set;}
		
		public PositionObj(Integer int_PosX, Integer int_PosY, Integer int_SizeX, Integer int_Page, String str_Anchor){
			this.PosX = int_PosX;
			this.PosY = int_PosY;
			this.SizeX = int_SizeX;
			this.Page = int_Page;
            this.Anchor = str_Anchor;
        }
	}
	
    public class ConditionObj {}
    
	public class ChoiceObj {
        
		public TitleObj Title {get;set;}
		
		public ChoiceObj(TitleObj obj_Title){
			this.Title = obj_Title;
        }
	}
    
    public class customException extends Exception {}
		
}