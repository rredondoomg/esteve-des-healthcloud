/**
 * @description       Custom exception class from Content-related exception handling
 * @author            OmegaCRM 
 * @group             Content utilities
**/
public with sharing class OM_ContentException extends Exception {

}