/**
 * @File Name          OM_LWC_pack.cls
 * @Description        Main class for Apex actions in LWC components
 * @Author             dcastellon@omegacrmconsulting.com
 * @Group              Utilities
**/
public with sharing class OM_LWC_pack {


    /**
    * @description 
    * @author dcastellon@omegacrmconsulting.com | 28/10/2019 
    * @param objName 
    * @param fieldName 
    * @param areRecords 
    * @return String 
    **/
    @AuraEnabled
    public static String getValues(String objName, String fieldName, Boolean areRecords, String filter){
        
        List<GenericWrapper> values = new List<GenericWrapper>();

        if(areRecords==false){
            values = getPLValues(objName, fieldName);
        } else {
            String query = 'SELECT Id, ' + fieldName + ' FROM ' + objName;
            if(filter != '' && filter.left(8) != 'ORDER BY'){
                query += ' WHERE ' + filter;
            }
            if(!filter.contains('ORDER BY')){
                query += ' ORDER BY ' + fieldName + ' ASC';
            }
            if(!filter.contains('LIMIT')){
                query += ' LIMIT 2000';
            }
            for(sObject record: Database.query(query)){
                if((String)record.get(fieldName) != '' && (String) record.get(fieldName) != null){
                    values.add(new GenericWrapper((String) record.get(fieldName), (String)record.get('Id')));
                }
            }
        }
        
        return JSON.serialize(values);
    }

    /**
    * @description 
    * @author dcastellon@omegacrmconsulting.com | 28/10/2019 
    * @param objName 
    * @param fieldName 
    * @return List<GenericWrapper> 
    **/
    @AuraEnabled
    public static List<GenericWrapper> getPLValues(String objName, String fieldName){

        Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
        Map<String, Schema.SObjectField> fieldMap = gd.get(objName.toLowerCase()).getDescribe().fields.getMap(); 
        List<Schema.PicklistEntry> picklistValues = fieldMap.get(fieldName).getDescribe().getPickListValues();
        
        List<GenericWrapper> options = new List<GenericWrapper>();

        for (Schema.PicklistEntry pv : picklistValues) {
            if(pv.isActive()){
                options.add(new GenericWrapper(pv.getLabel(), pv.getValue()));
            }
        }

        return options;
    }


    /**
    * @description 
    * @author sdlopez@omegacrmconsulting.com | 28/10/2019 
    * @param objectApiName 
    * @param fields 
    * @return List<ColumnWrapper> 
    **/
    @AuraEnabled(cacheable=true)
    public static List<ColumnWrapper> getListViewColumns(String objectApiName, String fields, String filters, Boolean showRelationshipObjectLabel, String labelList, String htmlFields){

        List<ColumnWrapper> ret = new List<ColumnWrapper>();

        List<String> listFields = (fields.replace(' ', '').toLowerCase()).split(',');
        Map<String, Schema.Sobjectfield> mapFieldsObject = OM_SchemaUtils.getFieldsSObjectMap(objectApiName);

        Integer cont = 0;
        List<String> listLabels;

        if (String.isNotBlank(labelList)){
            listLabels = labelList.split(',');
            for (Integer i = 0; i < listLabels.size(); i++){
                listLabels[i] = listLabels[i].trim();
            }
        }

        for (String str : listFields){
            if (str.contains('.')){
                List<String> strObject = str.split('\\.');
                String nomObj = objectApiName;
                String cadenaRel = '';
                for (Integer i = 0; i < strObject.size() - 1; i++){
                    if (strObject[i].containsIgnoreCase('__r')){
                        strObject[i] = strObject[i].replace('__r', '__c').replace('__R', '__c');
                    }else{
                        strObject[i] = strObject[i] + 'id';
                    }
                    Schema.DescribeFieldResult fieldDescribe = OM_SchemaUtils.getFieldsSObjectMap(nomObj).get(strObject[i]).getDescribe();


                    nomObj = fieldDescribe.referenceto[0].getDescribe().getName();
                    if (i != strObject.size() - 2 ){
                        cadenaRel += fieldDescribe.getRelationshipName() + '.';
                    }else{
                        Schema.DescribeFieldResult dfr = OM_SchemaUtils.getFieldsSObjectMap(nomObj).get(strObject[i+1]).getDescribe();
                        ret.add(new ColumnWrapper(
                            listLabels != null && listLabels[cont] != '#' ? listLabels[cont] : (showRelationshipObjectLabel ? fieldDescribe.getLabel() : dfr.getLabel()),
                            cadenaRel + mapFieldsObject.get(strObject[i]).getDescribe().getRelationshipName() + '.' + dfr.getName(),
                            String.valueOf(dfr.getType()),
                            dfr.isSortable()
                        ));
                    }
                }
            }else{
                Schema.DescribeFieldResult dfr = mapFieldsObject.get(str).getDescribe();
                ColumnWrapper w =  new ColumnWrapper(
                    listLabels != null && listLabels[cont] != '#' ? listLabels[cont] : dfr.getLabel(),
                    dfr.getName(),
                    String.valueOf(dfr.getType()),
                    dfr.isSortable()
                );

                if (String.valueOf(dfr.getType()).toLowerCase() == 'picklist') {
                    List<Map<String, String>> opts = new List<Map<String, String>>();
                    for(Schema.PicklistEntry entry : dfr.getPicklistValues()) {
                        opts.add( new Map<String, String> {
                            'label' =>  entry.getLabel(),
                            'value' => entry.getValue()
                        });
                    }
                    w.setOptions(opts);

                }
                ret.add(w);
            }
            cont++;
        }

        if (String.isNotBlank(htmlFields)){
            htmlFields = htmlFields.toLowerCase();
            htmlFields = htmlFields.replace(' ', '');
            Set<String> listHtmlFields = new Set<String>(htmlFields.split(','));

            for (ColumnWrapper cw : ret){
                if (listHtmlFields.contains(cw.fieldName.toLowerCase())){
                    cw.type = 'HTML';
                }
            }
        }

        return ret; //new List<ColumnWrapper>{new ColumnWrapper('Id','Id','text'),new ColumnWrapper('Name','Name','text')};
    }

    /**
    * @description 
    * @author dcastellon@omegacrmconsulting.com | 28/10/2019 
    * @param objectApiName 
    * @param fields 
    * @param filters 
    * @param orderBy 
    * @param limitCount 
    * @return List<SObject> 
    **/
    @AuraEnabled
    public static List<SObject> getRecords(String objectApiName, String fields, String filters, String orderBy, Integer limitCount){
        String query = 'SELECT ' + fields + ' FROM ' + objectApiName + ' ';
        if(filters != null && filters != ''){
            query += filters.startsWithIgnoreCase('WHERE') ? filters : ' WHERE ' + filters;
        }
        if(orderBy != null && orderBy != ''){
            query += orderBy.startsWithIgnoreCase('ORDER BY') ? orderBy : ' ORDER BY ' + orderBy;
        }
        if (limitCount != null && limitCount != 0){
            query += ' LIMIT ' + limitCount;
        }
        return Database.query(query);
    }

    @AuraEnabled
    public static Map<String, List<ResultWrapper>> saveRecords(List<SObject> records, String action, String externalIdField){
        Map<String, List<ResultWrapper>> result = new Map<String, List<ResultWrapper>>{
            'success' => new List<ResultWrapper>(),
            'error' => new List<ResultWrapper>()
        };

        if(action == 'insert' || action == 'update') {
            Database.SaveResult[] srList;
            if(action == 'insert') {
                srList = Database.insert(records, false);
            }
            if(action == 'update') {
                srList = Database.update(records, false);
            }

            Integer rIndex = 0;
            for(Database.SaveResult res : srList){
                if(!res.isSuccess()){
                    for(Database.Error err : res.getErrors()){
                        result.get('error').add(new ResultWrapper(err, rIndex, records[rIndex]));
                    }
                } else {
                    result.get('success').add(new ResultWrapper(res, rIndex, records[rIndex]));
                }
                rIndex++;
            }
        } 
        //Dynamic Upsert currently not supported (Error: Upsert with a field specification requires a concrete SObject type). See IDEAS below:
        // https://success.salesforce.com/ideaView?id=08730000000aNhVAAU
        // https://success.salesforce.com/ideaView?id=08730000000E1eZAAS

        // else if (action == 'upsert') {
        //     //Get reference to External id field in schema
        //     List<String> exidsplit = externalIdField.split('\\.');
        //     Schema.SobjectField extIdField = Schema.getGlobalDescribe().get(exidsplit[0]).getDescribe().fields.getMap().get(exidsplit[1]);

        //     Database.UpsertResult[] srList = Database.upsert(records, extIdField, false);

        //     Integer rIndex = 0;
        //     for(Database.UpsertResult res : srList){
        //         if(!res.isSuccess()){
        //             for(Database.Error err : res.getErrors()){
        //                 result.get('error').add(new ResultWrapper(err, rIndex, records[rIndex]));
        //             }
        //         } else {
        //             result.get('success').add(new ResultWrapper(res, rIndex, records[rIndex]));
        //         }
        //         rIndex++;
        //     }
        // } 
        else {
            throw new OM_LWCException('Invalid DML operation');
        }

        return result;

    }

        /**
    * @description Gets al lsit of all application object. Should be searchable
    * @author OmegaCRM  | 23/8/2019
    * @return List<Object> List of label/value options as map
    */
    @AuraEnabled(cacheable=true)
    public static List<Object> getSObjects(){
        Map<String, Schema.sObjectType> gd = Schema.getGlobalDescribe();
        List<Object> objs = new List<Object>();
        for(Schema.sObjectType obj : gd.values()) {
            DescribeSObjectResult obDesc = obj.getDescribe();
            if(isPlayableSObject(obDesc)) {
                Map<String, Object> objmap = new Map<String, Object> {
                    'label' => obDesc.getLabel(), 
                    'value' => obDesc.getName()
                };

                if(obDesc.getChildRelationships().size() > 0) {
                    List<Map<String, String>> children = new List<Map<String, String>>();
                    for(Schema.ChildRelationship child : obDesc.getChildRelationships()) {
                        DescribeSObjectResult childdesc = child.getChildSObject().getDescribe();
                        if(isPlayableSObject(childdesc) && child.getRelationshipName() != null) {
                            children.add(new Map<String, String> {
                                'field' => child.getField().getDescribe().getName(), 
                                'relationship' => child.getRelationshipName(), 
                                'object' => child.getChildSObject().getDescribe().getName()
                            });
                        }
                    }
                    objmap.put('children', children);
                }

                objs.add(objmap);
            }
        }
        return objs;
    }

    public static Boolean isPlayableSObject(DescribeSObjectResult obDesc) { 
        return !obDesc.isCustomSetting() 
            && !obDesc.isDeprecatedAndHidden()
            && obDesc.isSearchable()  
            && obDesc.isCreateable();
    }

    /**
    * @description Creates a JSON string of schema information about a single object (Salesforce durrenly doesnt serialize "DescribeSObjectResult" class)
    * @author OmegaCRM  | 23/8/2019
    * @param String SObject API name
    * @return String SObject schema representation as JSON
    */
    @AuraEnabled(cacheable=true)
    public static String describeToJSON(String objectName) {
        DescribeSObjectResult obDesc = Schema.getGlobalDescribe().get(objectName).getDescribe();

        JSONGenerator gen = JSON.createGenerator(true);
        
        //Begin describe object
        gen.writeStartObject();

        // Write string data to the JSON string.
        gen.writeStringField('label', obDesc.getLabel());
        gen.writeStringField('labelPlural',  obDesc.getLabelPlural());
        gen.writeStringField('localName',  obDesc.getLocalName());
        if(obDesc.getKeyPrefix() != null) gen.writeStringField('keyPrefix',  obDesc.getKeyPrefix());

        //Write boolean data
        gen.writeBooleanField('custom',  obDesc.isCustom());
        gen.writeBooleanField('customSetting',  obDesc.isCustomSetting());
        gen.writeBooleanField('createable',  obDesc.isCreateable());
        gen.writeBooleanField('accessible',  obDesc.isAccessible());
        gen.writeBooleanField('mergeable',  obDesc.isMergeable());
        gen.writeBooleanField('deletable',  obDesc.isDeletable());
        gen.writeBooleanField('queryable',  obDesc.isQueryable());
        gen.writeBooleanField('updateable',  obDesc.isUpdateable());
        gen.writeBooleanField('searchable',  obDesc.isSearchable());
        gen.writeBooleanField('undeletable',  obDesc.isUndeletable());

        //Write array of fields
        gen.writeFieldName('fields');
        gen.writeStartArray();
        for(Schema.SObjectField f : obDesc.fields.getMap().values()) {
            gen.writeObject(f.getDescribe());
        }
        gen.writeEndArray();

        //End describe object
        gen.writeEndObject();

        // Get the JSON string.
        String pretty = gen.getAsString();
        return pretty;
    }

    public class ResultWrapper {
        @AuraEnabled public String id;
        @AuraEnabled public String status;
        @AuraEnabled public String message;
        @AuraEnabled public String row;
        @AuraEnabled public SObject record;

        public ResultWrapper(Database.Error err, Integer index, SObject record) {
            this.message = err.getMessage();
            this.row = String.valueOf(index);
            this.record = record;
        }

        public ResultWrapper(Database.UpsertResult res, Integer index, SObject record) {
            this.id = res.getId();
            this.message = 'Saved succesfully';
            this.row = String.valueOf(index);
            this.record = record;
        }

        public ResultWrapper(Database.SaveResult res, Integer index, SObject record) {
            this.id = res.getId();
            this.message = 'Saved succesfully';
            this.row = String.valueOf(index);
            this.record = record;
        }
    }

    public class ColumnWrapper{
        @AuraEnabled public String label;
        @AuraEnabled public String fieldName;
        @AuraEnabled public String type;
        @AuraEnabled public Boolean sortable;
        @AuraEnabled public Boolean isClickable = false;
        @AuraEnabled public Boolean truncate = false;
        @AuraEnabled public List<Map<String, String>> options = null;

        public ColumnWrapper(String l, String f, String t, Boolean so){
            label = l;
            fieldName = f;
            type = t;
            sortable = so;
        }

        public void setOptions( List<Map<String, String>> opts) {
            this.options = opts;
        }
    }

    public class DatatableWrapper{
        @AuraEnabled public List<OM_LWC_pack.ColumnWrapper> columns = new List<OM_LWC_pack.ColumnWrapper>();
        @AuraEnabled public List<Map<String, Object>> rows = new List<Map<String, Object>>();
    }

    @AuraEnabled
    public static String runQuery(String query){
        try {
            return JSON.serialize(Database.query(query));
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static String getReportData(String idOrDevname, List<Integer> filterIndexes, List<String> filterValues) {
        // Get the report ID
        List <Report> reportList = [SELECT Id,DeveloperName FROM Report where Id = :idOrDevname OR DeveloperName =: idOrDevname];
        String reportId = (String) reportList.get(0).get('Id');

        System.debug(LoggingLevel.ERROR, filterIndexes.size());
        System.debug(LoggingLevel.ERROR, filterValues.size());

         System.debug(LoggingLevel.ERROR, filterIndexes);
        System.debug(LoggingLevel.ERROR, filterValues);
        if(filterIndexes.size() > 0) {
            if(filterIndexes.size() != filterValues.size()) {
                throw new OM_LWCException('Filter indexes and values dont match');
            }
            Reports.ReportDescribeResult describe = Reports.ReportManager.describeReport(reportId);
            Reports.ReportMetadata reportMd = describe.getReportMetadata();
            // Override filter and run report
            Integer i = 0;
            for(Integer index : filterIndexes) {
                Reports.ReportFilter filter = reportMd.getReportFilters()[index];
                filter.setValue(filterValues[i]);
                i++;
            }
            return JSON.serialize(Reports.ReportManager.runReport(reportId, reportMd));
        } else {
            // Run a report synchronously
            return JSON.serialize(Reports.ReportManager.runReport(reportId, true));
        }

    }

    public class GenericWrapper{
        public String label;
        public String value;
        
        public GenericWrapper(String label, String value){
            this.label = label;
            this.value = value;
        }
    }

    /**
    * @description 
    * @author dcastellon@omegacrmconsulting.com | 28/10/2019 
    * @param objectApiName 
    * @param fields 
    * @param value 
    * @param filter 
    * @return List<SObject> 
    **/
    @AuraEnabled
    public static List<SObject> getResults(String objectApiName, String fields, String value, String filter, Boolean sosl) {
        return sosl==true ? getResultsSOSL(objectApiName, value, filter) : getResultsSOQL(objectApiName, fields, value, filter);
    }

    private static List<SObject> getResultsSOQL(String objectApiName, String fields, String value, String filter) {
        
        String query = 'SELECT ' + fields + ' FROM ' + objectApiName;
        Map<String, schema.Sobjectfield> fieldmap = OM_SchemaUtils.getFieldsSObjectMap(objectApiName);

        if(String.isNotBlank(value)){
            List<String> fieldList = fields.deleteWhitespace().split(',');
            if(!fieldList.isEmpty()){
                query += ' WHERE (';
                for(String fld: fieldList){
                    Schema.DescribeFieldResult fdes = fieldmap.containsKey(fld) ? fieldmap.get(fld).getDescribe() : null;
                    if(fdes != null && fdes.isFilterable()) {
                        Schema.DisplayType ftype = fdes.getType();
                        if(ftype == Schema.DisplayType.STRING) {
                            query += fld + ' LIKE \'%' + value + '%\' OR ';
                        //Dont search for certains types
                        } else if(ftype == Schema.DisplayType.ID
                                || ftype == Schema.DisplayType.REFERENCE
                                || ftype == Schema.DisplayType.BOOLEAN
                                || ftype == Schema.DisplayType.DATE
                                || ftype == Schema.DisplayType.DATETIME
                                || ftype == Schema.DisplayType.ADDRESS
                                || ftype == Schema.DisplayType.INTEGER
                                || ftype == Schema.DisplayType.DOUBLE
                                || ftype == Schema.DisplayType.CURRENCY
                            ) {
                            query += '';
                        }  else if(ftype == Schema.DisplayType.MULTIPICKLIST) {
                            query += fld + ' INCLUDES (\'' + value + '\') OR ';
                        } else  {
                            query += fld + ' = \'' + value + '\' OR ';
                        }
                    } 
                }
                query = query.removeEnd(' OR ') + ')';

                //440 - PS - Si no encuentra ningún campo filtrable, no filtra
                query = query.removeEnd(' WHERE ()');

            } else { 
                return new List<SObject>();
            }
        }

        if(String.isNotBlank(filter)){
            query += (String.isNotBlank(value) ? ' AND ' : ' WHERE ') + filter;
        }

        query += ' LIMIT 4';
        
        try {
            return Database.query(query);
        } catch(Exception e) {
            throw new OM_LWCException(e.getMessage() + ' query ' + query);
        }

    }

    private static List<SObject> getResultsSOSL(String objectApiName, String value, String filter) {
        if(String.isBlank(value) || value.trim().length()<3){
            return null;
        }

        String whereQuery = ''; //pasar por parámetro
        //String SOSL_search_string = 'FIND \'zar\' IN NAME FIELDS RETURNING Maestros__c(Id, Name WHERE RecordType.DeveloperName = \'poblaciones\') LIMIT 4';
        String searchQuery = 'FIND ';
        searchQuery += '\'' + value + '\'';
        searchQuery += ' IN NAME FIELDS RETURNING ';//se puede mejorar y aceptar variable aquí para personalizarlo
        searchQuery += objectApiName; 
        searchQuery += '(Id, Name';
        if(String.isNotBlank(whereQuery)){
            searchQuery += whereQuery;
        }
        searchQuery += ')';
        searchQuery += ' LIMIT 4';


        
        try {
            List<List <sObject>> results = search.query(searchQuery);
            return results[0];
        } catch(Exception e) {
            throw new OM_LWCException(e.getMessage() + ' query ' + searchQuery);
        }

    }
    
    public class OM_LWCException extends Exception {}   
}