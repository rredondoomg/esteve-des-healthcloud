/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 06-06-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/

// TEST PARA LAS VALIDACIONES CRUZADAS

@isTest
public with sharing class ETH_TratamientoProtocoloPasoTest {
    public class NoErrorException extends Exception {}

    @TestSetup
    static void makeData(){
        ETH_DataFactory.setUpOmegaContent();
        //crear un usuario
        User userProfesional = ETH_DataFactory.createUser('usertestethprofesional@test.com');
        insert userProfesional;

        User user = [SELECT Id FROM User WHERE LastName = 'Omega' LIMIT 1];

        System.runAs(user){
            MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
            Test.setMock(HttpCalloutMock.class, multimock);

            Account account = ETH_DataFactory.createAccountWithAddress('Spain', 'Granada', 'Periodista Fernando Gomez de la Cruz', '18014', 'Granada');
            insert account;

            OperatingHours operatingHours = ETH_DataFactory.createOperatingHours();
            insert operatingHours;

            ServiceTerritory serviceTerritoryPadre = ETH_DataFactory.createServiceTerritory(null, operatingHours.Id, 'Pharmate', null);
            insert serviceTerritoryPadre;

            ServiceTerritory serviceTerritoryHijo = ETH_DataFactory.createServiceTerritory(account.ShippingPostalCode, operatingHours.Id, 'Pharmate', serviceTerritoryPadre.Id);
            insert serviceTerritoryHijo;
        
            // User user = ETH_DataFactory.createUser('usertesteth@test.com');
            // insert user;

            ServiceResource serviceResource = ETH_DataFactory.createServiceResourceProfesional(user.Id);
            insert serviceResource;

            Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'Pharmate');

            protocolo__c protocolo = ETH_DataFactory.createProtocolo();
            insert protocolo;

            tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, protocolo.Id, 'Pharmate');
            insert tratamiento;

            Paso_Protocolo__c paso = ETH_DataFactory.createPasoProtocolo(protocolo.Id, 1, 'Presencial', 'Profesional Responsable (Pharmate)', 'Inicio', 'Domicilio', 7);
            insert paso;

            Worktype worktype = ETH_DataFactory.createWorktype('Alta', 'Pharmate', paso.tipo_Lugar__c, paso.lugar_visita__c);
            insert worktype;

            ca.Tratamiento__c = tratamiento.Id;

            Test.startTest();
            System.runAs(user){
                insert ca;
            }
            Test.stopTest();
        }
    }

    @isTest
    public static void testValidacionDeleteProtocolo(){        
        List<protocolo__c> protocoloToDelete = [SELECT Id FROM protocolo__c WHERE Name = 'Protocolo test'];
        try{
            delete protocoloToDelete;
            throw new NoErrorException('Eliminó un protocolo relacionado.');
        }catch(Exception e){
            System.debug(e.getMessage(  ));
            System.assertEquals(e.getMessage().contains('No es posible eliminar este protocolo debido a que existen tratamientos relacionados.'), true);
        }
    }

    @isTest
    public static void testValidacionDeletePaso(){
        List<Paso_Protocolo__c> pasoToDelete = [SELECT Id FROM Paso_Protocolo__c WHERE Name = 'Paso test'];
        try{
            delete pasoToDelete;
            throw new NoErrorException('Eliminó un paso de protocolo relacionado.');
        }catch(Exception e){
            System.debug(e.getMessage());
            System.assertEquals(e.getMessage().contains('No es posible eliminar este paso de protocolo debido a que existen tratamientos relacionados.'), true);
        }
    }

    @isTest
    public static void testValidacionDeleteTipoTratamiento(){
        List<tratamiento__c> tratamientoToDelete = [SELECT Id FROM tratamiento__c];
        try{
            delete tratamientoToDelete;
            throw new NoErrorException('Eliminó un tratamiento relacionado.');
        }catch(Exception e){
            System.debug(e.getMessage());
            System.assertEquals(e.getMessage().contains('No es posible eliminar un Tipo de tratamiento con tratamientos asociados.'), true);
        }
    }

    @isTest
    public static void testValidacionDeleteMaestroUPAB(){
        List<Maestro_UPAB__c> maestrosUPAB = [SELECT Id FROM Maestro_UPAB__c];
        try{
            delete maestrosUPAB;
            throw new NoErrorException('Eliminó un maestro UPAB relacionado.');
        }catch(Exception e){
            System.debug(e.getMessage());
            System.assertEquals(e.getMessage().contains('No es posible eliminar este Maestro UPAB debido a que existen tratamientos relacionados.'), true);
        }
    }

    @isTest
    public static void testValidacionChangeNameMaestroUPAB(){
        List<Maestro_UPAB__c> maestrosUPAB = [SELECT Id, Name FROM Maestro_UPAB__c];
        maestrosUPAB[0].Name = 'Changed Name';
        try{
            update maestrosUPAB;
            throw new NoErrorException('Cambió el nombre de un maestro UPAB relacionado.');
        }catch(Exception e){
            System.debug(e.getMessage());
            System.assertEquals(e.getMessage().contains('No es posible cambiar el nombre de este Maestro UPAB debido a que existen tratamientos relacionados.'), true);
        }
    }
    
    @isTest
    public static void testValidacionChangeCodidgoCliente(){
        List<Cliente__c> clientes = [SELECT Id, Cod_cliente__c FROM Cliente__c];
        clientes[0].Cod_cliente__c = 'Changed Cod';
        try{
            update clientes;
            throw new NoErrorException('Cambió el código de un cliente relacionado.');
        }catch(Exception e){
            System.debug(e.getMessage());
            System.assertEquals(e.getMessage().contains('No es posible cambiar el código de cliente debido a que tiene tratamientos relacionados.'), true);
        }
    }
}