/**
 * @description       Test class for OM_ContentUtils
 * @author            OmegaCRM 
 * @group             Content tests
**/
@isTest
public inherited sharing class OM_ContentUtils_Test {
    public static final String USERNAME = 'testcontentUser@content.omegacrconsulting.com';

    @TestSetup
    static void makeData(){
        // Insert user with corect permission set for testing (any profile)
        Profile pf = [SELECT Id FROM Profile WHERE UserLicense.Name = 'Salesforce' LIMIT 1];
        User u = new User(
            Username = USERNAME,
            FirstName = 'Test',
            LastName = 'Omega',
            ProfileId = pf.Id,
            Email = USERNAME,
            Alias = 'test',
            TimeZoneSidKey = 'GMT',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'ISO-8859-1',
            LanguageLocaleKey = 'en_US'
        );

        insert u;

        PermissionSet contUser = [SELECT Id FROM PermissionSet WHERE Name = 'OM_ContentUser' LIMIT 1];
        PermissionSetAssignment psass = new PermissionSetAssignment(
            AssigneeId = u.Id,
            PermissionSetId = contUser.Id
        );

        insert psass;
    }
    @IsTest
    static void testUtilsForOneDrive(){
        OM_ContentSetting__c settings = new OM_ContentSetting__c(
            OM_Name__c = 'OneDriveSettings',
            OM_Allow_folder_creation__c = true,
            OM_Allow_navigation__c = true,
            OM_Allow_upload_files__c = true,
            OM_Allow_edit__c = true,
            OM_Auth_type__c = 'Shared',
            OM_Drive_id__c = 'test',
            OM_Use_personal_drive__c = true,
            OM_Client_id__c = 'test',
            OM_Client_secret__c = 'test',
            OM_Discovery_docs__c = 'test',
            OM_Provider__c = 'MicrosoftOneDrive',
            OM_Site_id__c = 'test',
            OM_Tenant__c = 'test',
            OM_Scope__c = 'test',
            OM_Redirect_uri__c = 'https://testorg.visualforce.com/apex/OM_OAuthCallback',
            OM_Columns__c = 'name;size'
        );

        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        Test.startTest();
        System.runAs(contextuser){
            OM_ContentIntegrationInterface integrationClass = OM_ContentUtils.getIntegrationClass(settings.OM_Provider__c);
            System.assertNotEquals(null, integrationClass, 'Concrete class should not be null');
            System.assert(integrationClass instanceof OM_ContentOneDriveIntegration, 'Concrete class should be the correct one for provider');

            OM_ContentWrapper testContent = new OM_ContentWrapper ();
            testContent.id = 'test';
            testContent.name = 'test';

            String personalDriveEndp = OM_ContentUtils.getEndpoint(settings, 'newFolder', testContent);
            System.assert(personalDriveEndp.contains('me/drive'), 'Personal Drive should be in endpoint');

            settings.OM_Use_personal_drive__c = false;
            String driveIdEndp = OM_ContentUtils.getEndpoint(settings, 'newFolder', testContent);
            System.assert(driveIdEndp.contains('drives/' + settings.OM_Drive_id__c), 'Drive id should be in endpoint');


            testContent.driveId = 'testnewdrive';
            String contentDriveIdEndp = OM_ContentUtils.getEndpoint(settings, 'newFolder', testContent);
            System.assert(contentDriveIdEndp.contains('drives/' + testContent.driveId), 'Drive be the one specified in content wrapper');

        }
        Test.stopTest();
        
    }

    @IsTest
    static void testUtilsForContentVersion(){

        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        Test.startTest();
        System.runAs(contextuser){
           
            String msWordMime = OM_ContentUtils.getMimeForContentVersionType('WORD_X');
            System.assertEquals(msWordMime, 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'MimeType for Word should be correct');

            String jpgMime = OM_ContentUtils.getMimeForContentVersionType('JPG');
            System.assertEquals(jpgMime, 'image/jpg', 'MimeType for JPG should be correct');

            String anyMime = OM_ContentUtils.getMimeForContentVersionType('_NOTEXISTING_');
            System.assertEquals(anyMime, 'application/octet-stream', 'MimeType for any other should be application/octet-stream');


        }
        Test.stopTest();
        
    }

    @IsTest
    static void testStripInvalidSharepoint(){

        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        Test.startTest();
        System.runAs(contextuser){
           
            String invalid1 = OM_ContentUtils.stripInvalidCharactersSharepoint('My account S.A.');
            System.assertEquals(invalid1, 'My account S.A', 'Invalid characters shoudl be removed');

            String invalid2 = OM_ContentUtils.stripInvalidCharactersSharepoint('My account# S.A.');
            System.assertEquals(invalid2, 'My account S.A', 'Invalid characters shoudl be removed');

            String invalid3 = OM_ContentUtils.stripInvalidCharactersSharepoint('~My account# S.A.');
            System.assertEquals(invalid3, 'My account S.A', 'Invalid characters shoudl be removed');

            String invalid4 = OM_ContentUtils.stripInvalidCharactersSharepoint('~My ..account# S.A.');
            System.assertEquals(invalid4, 'My account S.A', 'Invalid characters shoudl be removed');
        }
        Test.stopTest();
        
    }

    @IsTest
    static void testAllWrapperFields(){

        User contextUser = [SELECT Id FROM User WHERE Username = :USERNAME LIMIT 1];
        Test.startTest();
        System.runAs(contextuser){
           
            OM_ContentWrapper testContentEmptyCtor = new OM_ContentWrapper ();
            testContentEmptyCtor.id = 'test';
            testContentEmptyCtor.name = 'test';
            testContentEmptyCtor.driveId = 'test';
            testContentEmptyCtor.kind = 'test';
            testContentEmptyCtor.id = 'test';
            testContentEmptyCtor.name = 'test';
            testContentEmptyCtor.mimeType = 'test';
            testContentEmptyCtor.resourceType = 'test';
            testContentEmptyCtor.createdDate = 'test';
            testContentEmptyCtor.description = 'test';
            testContentEmptyCtor.fileExtension = 'test';
            testContentEmptyCtor.fullFileExtension = 'test';
            testContentEmptyCtor.iconLink = 'test';
            testContentEmptyCtor.thumbnailLink = 'test';
            testContentEmptyCtor.webContentLink = 'test';
            testContentEmptyCtor.removeParents = 'test';
            testContentEmptyCtor.addParents = 'test';
            testContentEmptyCtor.parentId = 'test';
            testContentEmptyCtor.size = 'test';


            OM_Content__c sfContent = new OM_Content__c(
                OM_GDrive_Id__c = 'testid',
                OM_Tags__c = '#tag1 #tag2 #tag3 #tag4',
                OM_Mime_Type__c = 'text/plain',
                Name = 'testfile.txt'
            );

            OM_ContentWrapper testContentFromRecord = new OM_ContentWrapper (sfContent);
            System.assertEquals('testid', testContentFromRecord.id, 'Ids should match');

            // Insert content to cover the trigger
            insert sfContent;

            OM_Content__c sfContent2 = new OM_Content__c(
                OM_GDrive_Id__c = 'testid',
                OM_Tags__c = 'tagsWithoutSymbol',
                OM_Mime_Type__c = 'text/plain',
                Name = 'testfile.txt'
            );

            insert sfContent2;



        }
        Test.stopTest();
        
    }
}