/**
 * @description       : Utils Methods
 * @author            : OmegaCRM
 * @test class        : OM_UtilsTest
 * @last modified on  : 03-28-2022
 * @last modified by  : David López <sdlopez@omegacrmconsulting.com>
**/
global inherited sharing class OM_Utils {

    /**
    * @description Crea registro de Sharing
    * @author David López <sdlopez@omegacrmconsulting.com> | 03-28-2022 
    * @param mapRecordUser - Mapa idRegistro => idUserGroup
    * @param accessLevel - Edit / Read / All
    **/
    global static void createShareRecords(Map<Id, Id> mapRecordUser, String accessLevel) {

        List<SObject> recordsInsert = new List<SObject>();

        for (Id recordId : mapRecordUser.keySet()) {

            String objectName = OM_SchemaUtils.getSObjectTypeFromId(recordId);
            Boolean isCustom = objectName.containsIgnoreCase('__c');
            String shareObjectName = isCustom ? objectName.replace('__c', '__Share') : objectName + 'Share';

            SObject shareObject = OM_SchemaUtils.getTypeSObject(shareObjectName).newSObject();

            shareObject.put(isCustom ? 'ParentId' : (objectName + 'Id'), recordId);
            shareObject.put((isCustom ? '' : objectName) + 'AccessLevel', accessLevel);
            shareObject.put('UserOrGroupId', mapRecordUser.get(recordId));

            recordsInsert.add(shareObject);
        }

        List<Database.SaveResult> lsr = Database.insert(recordsInsert, false);

        for (Integer i = 0; i < lsr.size(); i++) {
            if (!lsr[i].isSuccess()) {
                System.debug(System.LoggingLevel.ERROR, '***Error on createShareRecords: ' + String.join(lsr[i].getErrors(), ' / ') + '. \nRecord: ' + JSON.serialize(recordsInsert[i]));
            }
        }
    }
}