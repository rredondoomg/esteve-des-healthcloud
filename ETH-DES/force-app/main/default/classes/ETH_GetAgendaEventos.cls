/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 08-12-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
public without sharing class ETH_GetAgendaEventos {
    //id recurso, mapa(fecha, eventos)
    /**
    * @description Devuelve: Map(idRecurso, Map(fecha, eventos de ese día))
    * @author fizquierdo@omegacrmconsulting.com | 04-12-2022 
    * @param Date fechaMinimaRango 
    * @param Date fechaMaximaRango 
    * @param List<Id> recursosId 
    * @return Map<Id, Map<String, List<Event>>> 
    **/
    public static Map<Id, Map<String, List<Event>>> getAgendaEventos(Date fechaMinimaRango, Date fechaMaximaRango, List<Id> recursosId){
        Map<Id, Map<String, List<Event>>> agenda = new Map<Id, Map<String, List<Event>>>();

        //Una entrada en el mapa por cada recurso
        for(Id recursoId: recursosId){
            Map<String, List<Event>> agendaRecurso = new Map<String, List<Event>>();
            agenda.put(recursoId, agendaRecurso);
        }

        for(Event evento: [SELECT OwnerId, DurationInMinutes, EndDateTime, StartDateTime, IsAllDayEvent FROM Event WHERE (DAY_ONLY(convertTimezone(EndDateTime)) >= :fechaMinimaRango AND DAY_ONLY(convertTimezone(StartDateTime)) <= :fechaMaximaRango) AND OwnerId IN :recursosId AND ((Service_Appointment__c != null AND Service_Appointment__r.Status != 'Finalizado') OR Service_Appointment__c = null) ORDER BY StartDateTime ASC]){
            Map<String, List<Event>> agendaRecurso = agenda.get(evento.OwnerId);

            Date fecha = evento.StartDateTime.date();
            String fechaString = String.valueOf(fecha);
            if(!agendaRecurso.containsKey(fechaString)){
                List<Event> eventos = new List<Event>();
                eventos.add(evento);
                agendaRecurso.put(fechaString, eventos);
            }
            else{
                agendaRecurso.get(fechaString).add(evento);
            }
        }

        return agenda;
    }
}