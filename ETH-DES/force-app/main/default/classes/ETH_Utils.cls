/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 05-24-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
public with sharing class ETH_Utils {
    /**
    * @description Get info of each worktype
    * @author fizquierdo@omegacrmconsulting.com | 01-31-2022 
    * @return Map<String, Worktype> 
    **/
    public static Map<String, Worktype> getWorktypes(){
        Map<String, Worktype> worktypes = new Map<String, Worktype>();
        for(Worktype wt: [SELECT Id, Name, EstimatedDuration, OperatingHoursId FROM Worktype]){
            worktypes.put(wt.Name, wt);
        }
        return worktypes;
    }

    /**
    * @description 
    * @author fizquierdo@omegacrmconsulting.com | 03-02-2022 
    * @return Map<Id, Id> 
    **/
    public static Map<Id, Id> getWorktypeGroupEquivalence(){
        Map<Id, Id> equivalence = new Map<Id, Id>();
        for(WorkTypeGroupMember member: [SELECT WorkTypeId, WorkTypeGroupId FROM WorkTypeGroupMember]){
            equivalence.put(member.WorkTypeId, member.WorkTypeGroupId);
        }
        return equivalence;
    }

    /**
    * @description Dado los parámetros, encontrar su worktype
    * @author fizquierdo@omegacrmconsulting.com | 01-31-2022 
    * @param Map<String Worktype> worktypes 
    * @param String tipo. Alta, Seguimiento, Fin, Incidencia 
    * @param String negocio Pharmate, TRD
    * @param String formato Domicilio, CRETA, Virtual. Domicilio y CRETA vienen de lugar_visita__c en paso. Virtual de tipo_Lugar__c  en paso.
    * @return Worktype 
    **/
    public static Worktype getWorktypeByName(Map<String, Worktype> worktypes, String tipo, String negocio, String lugar_visita, String tipo_Lugar){
        if(tipo == 'Inicio'){
            tipo = 'Alta';
        }
        else if(tipo == 'Evaluación'){
            tipo = 'Seguimiento';
        }
        for(String nombre: worktypes.keySet()){
            if(nombre.contains(tipo) && nombre.contains(negocio) && nombre.contains(tipo_Lugar) && nombre.contains(lugar_visita)){
                return worktypes.get(nombre);
            }
        }
        //lanzar error
        return null;
    }

    /**
    * @description For example, to get all the worktypes of 'Seguimiento' or 'Fin'
    * @author fizquierdo@omegacrmconsulting.com | 02-04-2022 
    * @param Map<String Worktype> worktypes 
    * @param String tipo 
    * @return List<Id> 
    **/
    public static List<Id> getWorktypeByTipo(Map<String, Worktype> worktypes, String tipo){
        List<Id> worktypeFiltered = new List<Id>();
        for(String nombre: worktypes.keySet()){
            if(nombre.contains(tipo)){
                worktypeFiltered.add(worktypes.get(nombre).Id);
            }
        }
        return worktypeFiltered;
    }

    /**
    * @description Logic of the field 'AppointmentType' in ServiceAppointment
    * @author fizquierdo@omegacrmconsulting.com | 02-01-2022 
    * @param String lugar. Domicilio, CRETA, Hospital 
    * @param String formato. Presecial, Virtual 
    * @return String 
    **/
    public static String getAppointmentType(String lugar, String formato){
        if(formato == 'Virtual'){
            if(lugar == 'Domicilio'){
                return 'video';
            }
            else{
                return 'listen';
            }
        }
        else if(lugar == 'Domicilio'){
            return 'People';
        }
        else{
            return 'Company';
        }
    }

    /**
    * @description Get the custom metadata to controll the activation/desactivation of triggers
    * @author fizquierdo@omegacrmconsulting.com | 02-24-2022 
    * @return Trigger_Controller__mdt 
    **/
    public static Trigger_Controller__mdt getTriggerController(){
        Trigger_Controller__mdt triggerController = [SELECT Account_Trigger__c, Case_Trigger__c, Service_Appointment_Trigger__c, Create_Folder_Account_Trigger__c, Sharepoint_VidSigner__c, Auto_Altas_Case_Trigger__c, Create_Folder_Case_Trigger__c FROM Trigger_Controller__mdt WHERE Label='Trigger Controller'];
        return triggerController;
    }

    public static Map<String, CaseTeamRole> getCaseTeamRolesByName(){
        Map<String, CaseTeamRole> caseTeamRoles = new Map<String, CaseTeamRole>();
        for(CaseTeamRole caseTeamRole : [SELECT Id, Name FROM CaseTeamRole]){
            caseTeamRoles.put(caseTeamRole.Name, caseTeamRole);
        }
        return caseTeamRoles;
    }

    public static String getDayOfTheWeek(Datetime fecha){
        return fecha.format('EEEE');
    }

    public static Integer getValorDayOfTheWeek(String dayOfTheWeek){
        Integer intDayOfTheWeek;
        switch on dayOfTheWeek{
            when 'Monday'{
                intDayOfTheWeek =1;
            }
            when 'Tuesday'{
                intDayOfTheWeek =2;
            }
            when 'Wednesday'{
                intDayOfTheWeek =3;
            }
            when 'Thursday'{
                intDayOfTheWeek =4;
            }
            when 'Friday'{
                intDayOfTheWeek =5;
            }
            when 'Saturday'{
                intDayOfTheWeek =6;
            }
            when 'Sunday'{
                intDayOfTheWeek =7;
            }
        }
        return intDayOfTheWeek;
    }
}