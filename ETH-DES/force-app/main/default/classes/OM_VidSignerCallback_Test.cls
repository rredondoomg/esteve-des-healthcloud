@isTest
public with sharing class OM_VidSignerCallback_Test {
    @TestSetup
    static void makeData(){
        ETH_DataFactory.setUpOmegaContent();
        //crear un usuario
        User userProfesional = ETH_DataFactory.createUser('usertestethprofesional@test.com');
        insert userProfesional;

        User user = [SELECT Id FROM User WHERE LastName = 'Omega' LIMIT 1];

        System.runAs(user){
			//BGR start
			ETH__c oETH = new ETH__c(
				Name = 'ETH',
				HoraVisita__c = '08:00:00',
				vidsigner_url__c='URL',
				vidsigner_SubscriptionName__c='idsigner_SubscriptionName',
				vidsigner_SubscriptionPass__c='vidsigner_SubscriptionPass',
                vidsigner_url_getSignedDocument__c = 'URL_VidSigner'

			);
			insert oETH;
			
            MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
            Test.setMock(HttpCalloutMock.class, multimock);

            Test.startTest();

            Account account = ETH_DataFactory.createAccountWithAddress('Spain', 'Granada', 'Periodista Fernando Gomez de la Cruz', '18014', 'Granada');
            insert account;

            OperatingHours operatingHours = ETH_DataFactory.createOperatingHours();
            insert operatingHours;

            //timeslots de operating hours
            ETH_DataFactory.createTimeSlots(operatingHours.Id, 5, 23);

            ServiceTerritory serviceTerritoryPadre = ETH_DataFactory.createServiceTerritory(null, operatingHours.Id, 'Pharmate', null);
            insert serviceTerritoryPadre;

            ServiceTerritory serviceTerritoryHijo = ETH_DataFactory.createServiceTerritory(account.ShippingPostalCode, operatingHours.Id, 'Pharmate', serviceTerritoryPadre.Id);
            insert serviceTerritoryHijo;
        

            //crear un assigned resource a cada usuario
            ServiceResource serviceResourceProfesional = ETH_DataFactory.createServiceResourceProfesional(userProfesional.Id);
            insert serviceResourceProfesional;

            //crear los services territories member
            ServiceTerritoryMember serviceTerritoryMemberProfesional = ETH_DataFactory.createServiceTerritoryMember('Profesional Visita Presencial (Pharmate)', serviceResourceProfesional.Id, serviceTerritoryPadre.Id, operatingHours.Id);
            insert serviceTerritoryMemberProfesional;

            //repetimos proceso para CRETA y box
            ServiceTerritory serviceTerritoryCRETA = ETH_DataFactory.createServiceTerritory(account.ShippingPostalCode, operatingHours.Id, 'Creta',  null);
            insert serviceTerritoryCRETA;

            // Asset asset = ETH_DataFactory.createAsset(account.Id);
            // insert asset;
			// system.debug('Asset insertado '+asset);

            // ServiceResource serviceResourceBox = ETH_DataFactory.createServiceResourceBox(asset.Id);
            // insert serviceResourceBox;

            // ServiceTerritoryMember serviceTerritoryMemberBox = ETH_DataFactory.createServiceTerritoryMember('', serviceResourceBox.Id, serviceTerritoryCRETA.Id, operatingHours.Id);
            // insert serviceTerritoryMemberBox;

            protocolo__c protocolo = ETH_DataFactory.createProtocolo();
            insert protocolo;

            Paso_Protocolo__c paso1 = ETH_DataFactory.createPasoProtocolo(protocolo.Id, 1, 'Virtual', 'Usuario que da de alta el tratamiento', 'Inicio', 'Domicilio', 7);
            insert paso1;
            Paso_Protocolo__c paso2 = ETH_DataFactory.createPasoProtocolo(protocolo.Id, 2, 'Presencial', 'Profesional Visita Presencial (Pharmate)', 'Seguimiento', 'CRETA', 7);
            insert paso2;

            //alta
            Worktype worktypeAlta = ETH_DataFactory.createWorktype('Alta', 'Pharmate', paso1.tipo_Lugar__c, paso1.lugar_visita__c);
            insert worktypeAlta;
            //domicilio
            Worktype worktypeSeguimientoDomicilio = ETH_DataFactory.createWorktype('Seguimiento', 'Pharmate', paso2.tipo_Lugar__c, paso2.lugar_visita__c);
            insert worktypeSeguimientoDomicilio;

			// Account account = [SELECT Id FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];
			Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'Pharmate');
			// protocolo__c protocolo = [SELECT Id FROM protocolo__c WHERE Name = 'Protocolo test'];
			tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, protocolo.Id, 'TRD');
			insert tratamiento;
			ca.Tratamiento__c = tratamiento.Id;

			// User userProfesional = [SELECT Id FROM User WHERE Email='usertestethprofesional@test.com' LIMIT 1];
			// ServiceTerritory serviceTerritoryCRETA = [SELECT Id FROM ServiceTerritory WHERE RecordTypeId = :Schema.SObjectType.ServiceTerritory.getRecordTypeInfosByDeveloperName().get('Creta').getRecordTypeId() LIMIT 1];
			// Paso_Protocolo__c paso2 = [SELECT Id FROM Paso_Protocolo__c WHERE orden__c = 2 LIMIT 1];
			// ServiceResource serviceResourceBox = [SELECT Id FROM ServiceResource WHERE Name = 'Test Box' LIMIT 1];

			System.runAs(userProfesional){
				insert ca;
			}
        }
        Test.stopTest();
    }


    @isTest static void testMethodConsentimiento() { //BGR: quitamos el (seeAllData=true)

        Account account = [SELECT Id, account.NIF__c FROM Account WHERE FirstName = 'Test First Name' LIMIT 1];

		// generateTestData();
        ServiceAppointment cita = [SELECT Id, OwnerId, ServiceTerritoryId, Paso_Protocolo__c, Status FROM ServiceAppointment WHERE ParentRecordId = :account.Id LIMIT 1];

		Test.startTest();

        // HttpRequest request = new HttpRequest();
        // request.setMethod('POST');
        // request.setEndpoint('https://des-vidsignerdes.cs106.force.com/VidSignerDes/services/apexrest/PruebaVidSigner/Consentimiento/1234567/docstatus/1cb039f1-a8d4-4a06-aab4-86d445d1ebfe');
        
        // request.setBody('{}');
        
        // RestContext.response= res;
        // HttpResponse res = h.send(request);


        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://des-vidsignerdes.cs106.force.com/VidSignerDes/services/apexrest/PruebaVidSigner/Consentimiento/1234567/docstatus/1cb039f1-a8d4-4a06-aab4-86d445d1ebfe');
        // request.setHeader('Accept','application/json');
        // request.setHeader('Content-Type','application/x-www-form-urlencoded');
        // request.setBody('{}');
        request.setMethod('POST');
        HttpResponse response = http.send(request);
       
        system.debug('Debajo de la llamad');

        // system.debug('URL: '+req.requestURI);

       
        OM_VidSignerCallback.getSignatureDocumentPost();
        Test.stopTest();
	}
}