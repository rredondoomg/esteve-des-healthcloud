/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 05-04-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
@IsTest
public with sharing class OM_FileUploader_Test {
  @TestSetup
  static void makeData(){
    List<Account> accs = new List<Account> {
      new Account(Name= 'Test 1'),
      new Account(Name= 'Test 2'),
      new Account(Name= 'Test 3')
    };

    insert accs;
  }

  @IsTest
  public static void testUpload () {
    List<Account> accs = [SELECT Id FROM Account];

    String base64 = 'aG9sYSBtdW5kbw==';
    String filename = 'testUpload.txt';
    String recordId = accs.get(0).Id;
    List<String> relatedRecordIds = new List<String>{ accs.get(1).Id };
    String shareType = 'V';

    OM_FileUploader.UploadResult res;

    Test.startTest();
    res = OM_FileUploader.uploadContent(base64, filename, recordId, relatedRecordIds, shareType);
    system.debug('res');
    system.debug(res.toString());
    Test.stopTest();

    System.assertNotEquals(null, res.contentDocument, 'Content document should not be null');
    System.assertNotEquals(null, res.record, 'Content version should not be null');
    System.assertEquals(2, res.links.size(), '2 links should be created');
  }

}