/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 09-01-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
@isTest
public with sharing class ETH_CaseTriggerHandlerTest {
    @TestSetup
    static void makeData(){
        ETH_DataFactory.setUpOmegaContent();
    }

    /**
    * @description Alta Pharmate. Creates a ServiceAppointment, an AssignedResource and a Task
    * @author fizquierdo@omegacrmconsulting.com | 02-02-2022 
    **/
    @isTest
    public static void testAltaPharmate(){
        MultiStaticResourceCalloutMock multimock = ETH_DataFactory.getGeneralMock();
        Test.setMock(HttpCalloutMock.class, multimock);

        Account account = ETH_DataFactory.createAccountWithAddress('Spain', 'Granada', 'Periodista Fernando Gomez de la Cruz', '18014', 'Granada');
        insert account;

        OperatingHours operatingHours = ETH_DataFactory.createOperatingHours();
        insert operatingHours;

        ServiceTerritory serviceTerritoryPadre = ETH_DataFactory.createServiceTerritory(null, operatingHours.Id, 'Pharmate', null);
        insert serviceTerritoryPadre;

        ServiceTerritory serviceTerritoryHijo = ETH_DataFactory.createServiceTerritory(account.ShippingPostalCode, operatingHours.Id, 'Pharmate', serviceTerritoryPadre.Id);
        insert serviceTerritoryHijo;
    
        User user = ETH_DataFactory.createUser('usertesteth@test.com');
        insert user;

        ServiceResource serviceResource = ETH_DataFactory.createServiceResourceProfesional(user.Id);
        insert serviceResource;

        Case ca = ETH_DataFactory.createCaseWithAccountTest(account, 'Pharmate');

        protocolo__c protocolo = ETH_DataFactory.createProtocolo();
        insert protocolo;

        tratamiento__c tratamiento = ETH_DataFactory.createTratamiento(ca.Cliente__c, protocolo.Id, 'Pharmate');
        insert tratamiento;

        Paso_Protocolo__c paso = ETH_DataFactory.createPasoProtocolo(protocolo.Id, 1, 'Presencial', 'Profesional Responsable (Pharmate)', 'Inicio', 'Domicilio', 7);
        insert paso;

        Worktype worktype = ETH_DataFactory.createWorktype('Alta', 'Pharmate', paso.tipo_Lugar__c, paso.lugar_visita__c);
        insert worktype;

        ca.Tratamiento__c = tratamiento.Id;

        Test.startTest();
        System.runAs(user){
            insert ca;
        }
        Test.stopTest();

        ServiceAppointment cita = [SELECT Id, OwnerId, ServiceTerritoryId, Paso_Protocolo__c FROM ServiceAppointment WHERE ParentRecordId = :account.Id LIMIT 1];
        ca = [SELECT Cod_Iden__c FROM Case WHERE Id =:ca.Id LIMIT 1];
        System.assertNotEquals(cita, null);
        System.assertEquals(cita.OwnerId, user.Id);
        System.assertEquals(cita.ServiceTerritoryId, serviceTerritoryPadre.Id);
        System.assertEquals(cita.Paso_Protocolo__c, paso.Id);
        System.assertEquals('CL-UPAB-01', ca.Cod_Iden__c);

        AssignedResource assignedResource = [SELECT Id, ServiceAppointmentId, ServiceResourceId FROM AssignedResource WHERE ServiceAppointmentId = :cita.Id LIMIT 1];
        System.assertNotEquals(assignedResource, null);
        System.assertEquals(assignedResource.ServiceResourceId, serviceResource.Id);
    }
}