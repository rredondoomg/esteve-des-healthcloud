/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 03-22-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
public with sharing class ETH_ProtocoloTriggerHandler {
    public static Map<Id, Integer> protocolosWithCases(List<Id> protocolosId){
        Map<Id, Id> tratamientoProtocoloMap = new Map<Id, Id>();
        for(tratamiento__c tratamiento: [SELECT Id, Protocolo__c FROM tratamiento__c WHERE Protocolo__c IN :protocolosId]){
            tratamientoProtocoloMap.put(tratamiento.Id, tratamiento.Protocolo__c);
        }

        Map<Id, Integer> protocolosWithCases = new Map<Id, Integer>();
        for(AggregateResult tratamiento : [SELECT Tratamiento__c, COUNT(Id)n_tratamientos FROM Case WHERE Tratamiento__c IN :tratamientoProtocoloMap.keySet() GROUP BY Tratamiento__c]) {
            String tratamientoId = String.valueOf(tratamiento.get('Tratamiento__c'));
            protocolosWithCases.put(tratamientoProtocoloMap.get(tratamientoId), Integer.valueOf(tratamiento.get('n_tratamientos')));
        }

        return protocolosWithCases;
    }

    public static Map<Id, Integer> tratamientosWithCases(List<Id> tratamientosId){
        Map<Id, Integer> tratamientosWithCases = new Map<Id, Integer>();
        for(AggregateResult tratamiento : [SELECT Tratamiento__c, COUNT(Id)n_tratamientos FROM Case WHERE Tratamiento__c IN :tratamientosId GROUP BY Tratamiento__c]) {
            tratamientosWithCases.put(String.valueOf(tratamiento.get('Tratamiento__c')), Integer.valueOf(tratamiento.get('n_tratamientos')));
        }
        return tratamientosWithCases;
    }
}