public with sharing class OM_FileUploader {


    public class UploadResult {
        @AuraEnabled public List<ContentDocumentLink> links;
        @AuraEnabled public sObject record;
        @AuraEnabled public ContentDocument contentDocument;
        @AuraEnabled public Boolean isOk;
        @AuraEnabled public Boolean isPending;
        @AuraEnabled public Boolean isUploading;
        @AuraEnabled public String message;
    }
  /*
   * Creates a content version from a given file's base64 and name
   *
   * @param {String} base64 - base64 string that represents the file
   * @param {String} filename - full file name with extension, i.e. 'products.csv'
   * @param {String} recordId - Id of the record you want to attach this file to
   *
   * @return {ContentVersion} - returns the created ContentDocumentLink Id if the
   *   upload was successful, otherwise returns null
   */
  @AuraEnabled
  public static UploadResult uploadContent(
    String base64,
    String filename,
    String recordId,
    List<String> relatedRecordIds,
    String shareType
  ) {

    UploadResult result = new UploadResult();

    ContentVersion cv;
    
    try {

        if (ContentVersion.sObjectType.getDescribe().isAccessible() && ContentVersion.sObjectType.getDescribe().isCreateable()) {
            cv = createContentVersion(base64, filename);
            
            // Query to get the ContentDocument
            cv = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id LIMIT 1];
    
            List<ContentDocumentLink> links = new List<ContentDocumentLink>();
    
            if(recordId != null) {
                links.add(createContentLink(cv, recordId, shareType));
            }
    
            if( relatedRecordIds != null ) {
                for (String rel : relatedRecordIds) {
                    links.add(createContentLink(cv, rel, shareType));
                }
            }

            insert links;

            result.isOk = true;
            result.isPending = false;
            result.isUploading = false;
            result.message = 'Uploaded';
            result.links = links;
            result.record = cv;
            result.contentDocument = new ContentDocument(Id = cv.ContentDocumentId);
        } else {
            result.isOk = false;
            result.isPending = false;
            result.isUploading = false;
            result.message = 'You dont have permission to create or access ContentVersion';
        }
    } catch(Exception e) {
        result.isOk = false;
        result.isPending = false;
        result.isUploading = false;
        result.message = e.getMessage();
    }

    return result;
  }
  /*
   * @method createContentVersion() [private]
   * @desc Creates a content version from a given file's base64 and name
   *
   * @param {String} base64 - base64 string that represents the file
   * @param {String} filename - full file name with extension, i.e. 'products.csv'
   *
   * @return {ContentVersion} - returns the newly created ContentVersion, or null
   *   if there was an error inserting the record
   */
  private static ContentVersion createContentVersion(
    String base64,
    String filename
  ) {
    ContentVersion cv = new ContentVersion();
    cv.VersionData = EncodingUtil.base64Decode(base64);
    cv.Title = filename;
    cv.PathOnClient = filename;
    try {
      insert cv;
      return cv;
    } catch (DMLException e) {
      System.debug(e);
      return null;
    }
  }

  /*
   * @method createContentLink() [private]
   * @desc Creates a content link for a given ContentVersion and record
   *
   * @param {String} contentVersionId - Id of the ContentVersion of the file
   * @param {String} recordId - Id of the record you want to attach this file to
   *
   * @return {ContentDocumentLink} - returns the newly created ContentDocumentLink,
   *   or null if there was an error inserting the record
   */
  private static ContentDocumentLink createContentLink(
    ContentVersion cv,
    String recordId,
    String shareType
  ) {
    if (cv == null || recordId == null) {
      return null;
    }
    ContentDocumentLink cdl = new ContentDocumentLink();
    cdl.ContentDocumentId = cv.ContentDocumentId;
    cdl.LinkedEntityId = recordId;
    cdl.ShareType = shareType;
    
    return cdl;
  }
}