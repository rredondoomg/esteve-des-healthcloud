/**
 * @description        Base interface for all providers
 * @author             OmegaCRM 
 * @group              Omega content integration
**/
public interface OM_ContentIntegrationInterface {

    /**
    * @description Transform content wrapper to provider valid JSON
    * @author OmegaCRM  | 01-20-2021 
    * @param content The content to transform
    * @return String The JSON to work with the provider
    **/
    String transformContent(OM_ContentWrapper content);

    /**
    * @description Transform provider valid JSON to Apex wrapper
    * @author OmegaCRM  | 01-20-2021 
    * @param content The content to transform
    * @return OM_ContentWrapper The wrapper instance
    **/
    OM_ContentWrapper transformContent(String content);

    /**
    * @description List items from the provider
    * @author OmegaCRM  | 01-19-2021 
    * @param settings The settings to use
    * @param content The content wrapper. This instance is used as filter. So for example, informing "name" field, wil search by name with that value
    * @return List<OM_ContentWrapper> The content wrapper list of the results
    **/
    List<OM_ContentWrapper> listItems(OM_ContentSetting__c settings, OM_ContentWrapper contentAsFilters);

    /**
    * @description Search items in the provider
    * @author OmegaCRM  | 01-19-2021 
    * @param settings The settings to use
    * @param term The term to search for
    * @return List<OM_ContentWrapper> The content wrapper list of the results
    **/
    //List<OM_ContentWrapper> searchItems(OM_ContentSetting__c settings, String term);

    /**
    * @description Downloads a file a file 
    * @author OmegaCRM  | 01-19-2021 
    * @param settings The settings to use
    * @param content The content wrapper. Should have at least the Id
    * @return Blob The Blob of the file
    **/
    Blob downloadItem(OM_ContentSetting__c settings, OM_ContentWrapper content);

    /**
    * @description Gets the metadata info for a file
    * @author OmegaCRM
    * @param settings The settings to use
    * @param content The content wrapper. Should have the id or the name as path in case of Sharepoint
    * @return OM_ContentWrapper The repesentation of the file
    **/
    OM_ContentWrapper getItem(OM_ContentSetting__c settings, OM_ContentWrapper content);

    /**
    * @description Updates an file or a folder
    * @author OmegaCRM  | 01-19-2021 
    * @param settings The settings to use
    * @param content The content wrapper. Should have the metadata to be updated
    * @return String The JSON of the response
    **/
    OM_ContentWrapper updateItem(OM_ContentSetting__c settings, OM_ContentWrapper content);

    /**
    * @description Uploads a new item
    * @author OmegaCRM  | 01-19-2021 
    * @param settings The settings to use
    * @param content The content wrapper. Should have the metadata to be updated
    * @param fileData Binary content for upload
    * @return String The JSON of the response
    **/
    OM_ContentWrapper uploadItem(OM_ContentSetting__c settings, OM_ContentWrapper content, Blob fileData);
    

    /**
    * @description Create a new folder
    * @author OmegaCRM  | 01-19-2021 
    * @param settings The settings to use
    * @param content The content wrapper. To create folder should have 'id' and 'parentId' properties informed
    * @return String The JSON of the response
    **/
    OM_ContentWrapper deleteItem(OM_ContentSetting__c settings, OM_ContentWrapper content);

    /**
    * @description Create a new folder
    * @author OmegaCRM  | 01-19-2021 
    * @param settings The settings to use
    * @param content The content wrapper. To create folder should have 'id' and 'parentId' properties informed
    * @return String The JSON of the response
    **/
    OM_ContentWrapper createFolder(OM_ContentSetting__c settings, OM_ContentWrapper content);
    
    /**
    * @description Create a new folder path
    * @author OmegaCRM  | 01-19-2021 
    * @param settings The settings to use
    * @param content The content wrapper. To create folder should have 'id' and 'parentId' properties informed
    * @return String The JSON of the response
    **/
    List<OM_ContentWrapper> createFolderPath(OM_ContentSetting__c settings, OM_ContentWrapper content);

    /**
    * @description Get the authorization endpoint to begin Oatuh flow
    * @author OmegaCRM  | 01-19-2021 
    * @param settings The settings to use
    * @param userid The user id to append to "state" parameter
    * @return String 
    **/
    String getAuthorizationEndpoint(OM_ContentSetting__c settings, String userid);

    /**
    * @description Get new access token
    * @author OmegaCRM  | 01-19-2021 
    * @param settings The settings to use
    * @param authCode The authorization code from callbakc page
    * @return String 
    **/
    String getToken(OM_ContentSetting__c settings, String authCode);
    
    /**
    * @description Get new accces token using a refresh token
    * @author OmegaCRM  | 01-19-2021 
    * @param settings The settings to use
    * @param userId The user id to fetch "refresh_token" data
    * @return String 
    **/
    String refreshToken(OM_ContentSetting__c settings, String userId);

    /**
    * @description Makes the callouts allowing retries on 401 codes
    * @author OmegaCRM  | 01-19-2021 
    * @param calloutw The callout wrapper with service variables
    * @param settings The settings to use
    * @param content The user id to fetch "refresh_token" data
    * @param newAccess The new access in case of a retry
    * @return Object 
    **/
    Object makeCallout(OM_ContentService.CalloutWrapper calloutw, OM_ContentSetting__c settings, OM_ContentWrapper content, Map<String, String> newAccess);
}