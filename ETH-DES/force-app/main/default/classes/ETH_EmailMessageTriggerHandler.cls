/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 10-07-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
public with sharing class ETH_EmailMessageTriggerHandler {
    public static List<Tipificado_Casos__mdt>  getTipificadoCasos(){
        return [SELECT Activo__c, Buscar_En_Asunto__c, Buscar_En_Cuerpo__c, Keywords__c, Territorio__c, Orden__c, Prioridad__c, Tipificacion__c, Email__c FROM Tipificado_Casos__mdt WHERE Activo__c = true ORDER BY Orden__c ASC];
    }

    public static Case tipificarCasoEmail(List<Tipificado_Casos__mdt> tipificadoCasos, Case ca, EmailMessage emailMessage){
        // Filtrar el aviso legal porque contiene palabras que pueden hacer que entre
        String filteredTextBody = emailMessage.TextBody != null ? emailMessage.TextBody.substringBefore('AVISO LEGAL') : '';

        for(Tipificado_Casos__mdt tipificado: tipificadoCasos){
            if(emailMessage.ToAddress.contains(tipificado.Email__c)){
                List<String> keywords = tipificado.Keywords__c != null ? tipificado.Keywords__c.split(';') : null;
                if(keywords != null){ 
                    for(String keyword: keywords){
                        if( (tipificado.Buscar_En_Asunto__c && emailMessage.Subject.containsIgnoreCase(keyword)) || (tipificado.Buscar_En_Cuerpo__c && filteredTextBody.containsIgnoreCase(keyword))){
                            setCase(ca, tipificado);
                            return ca;
                        }
                        // La última parte del if es para las reglas que quieren decir en el resto de casos. Tendrán la el orden más alto
                    }
                }
                else if(!tipificado.Buscar_En_Cuerpo__c && !tipificado.Buscar_En_Asunto__c){
                    setCase(ca, tipificado);
                    return ca;
                }
            }
        }
        return null;
    }

    public static void setCase(Case ca, Tipificado_Casos__mdt tipificado){
        ca.Type = 'Colas';
        ca.Subtipo__c = tipificado.Tipificacion__c;
        ca.Priority = tipificado.Prioridad__c;
        ca.Territorio__c = tipificado.Territorio__c;
    }
}