// @ts-check
/**
 * Enhanced datatable implementation. Similar API to lightning-datatable but with more functionalities
 * such as searching, filtering, sorting, multiple header rows, column types and more
 * @module OmDatatable
 * @version 1.2.0
 * @requires OmDatatableCell
 * @requires OmDatatableUtils
 * @requires OmDatatableTile
 * @example
 * mycolumns = [
 *   { label: 'Name', fieldName: 'name', editable: true, type: 'text', isClickable: true},
 *   { label: 'Name Upper', value: (item) => item.name.toUpperCase(), editable: true, type: 'text'},
 *   { label: 'Phone', fieldName: 'phone', editable: true, type: 'phone'},
 * ]
 */
import { LightningElement, api, track } from 'lwc';
import { getItemValue, removeDiacritics, getFilterAttributes, sortFunction } from 'c/omDatatableUtils'
import selectAll from '@salesforce/label/c.omSelectAll';
import select from '@salesforce/label/c.omSelect';
import showing from '@salesforce/label/c.omShowing';
import to from '@salesforce/label/c.omTo';
import of from '@salesforce/label/c.omOf';
import previous from '@salesforce/label/c.omPrevious';
import next from '@salesforce/label/c.omNext';
import show from '@salesforce/label/c.omShow';
import search from '@salesforce/label/c.omSearch';
import noResultFound from '@salesforce/label/c.omNoResultFound';
import filterIn from '@salesforce/label/c.omFilterIn';
import filteredBy from '@salesforce/label/c.omFilteredBy';
import unhideColumns from '@salesforce/label/c.omUnhideColumns';
import omFilterOperator from '@salesforce/label/c.omFilterOperator';
import showAs from '@salesforce/label/c.omShowAs';
import table from '@salesforce/label/c.omTable';
import list from '@salesforce/label/c.omList';

const SLDS_ORDER_1 = 'slds-col slds-order_1';
const SLDS_ORDER_2 = 'slds-col slds-order_2';
const SLDS_ORDER_3 = 'slds-col slds-order_3';
const LAST_STACKED = 'last-stacked';

/**
 * Enhanced datatable implmentation with buil-in sorting, edidion/mass-edition, filtering and column filtering, fixed header, stacked columns and more
 * @class OmDatatable
 * @element datatable
 * @slot paginator To replace bottom paginator with custom one
 * @slot header To replace the full header area of the table container
 * @slot topLeftActions To append custom actions (components) in the top left area of the table container
 * @slot topRightActions To append custom actions (components) in the top right area of the table container
 * @slot bottomRightActions To append custom actions (components) in the bottom right area of the table container
 * @slot bottomLÑeftActions To append custom actions (components) in the bottom left area of the table container
 */
export default class OmDatatable extends LightningElement {

    /**
     * Columns to be managed by the table. See reference for more info
     * @type {DatatableColumn[]}
     * @default []
     */
    @api get columns() {
        return this.fullColumns
    }

    set columns(value) {
        this.setFullColumns(value)
    }

    @track fullColumns;

    _clickableColumns

    /**
     * Column keys to be clickable
     * @type {DatatableColumn[]}
     * @default []
     * @todo David
     */
    @api get clickableColumns() {
        return this._clickableColumns;
    }

    set clickableColumns(val) {
        this._clickableColumns = val;
        if(this.fullColumns && val) {
            this.setFullColumns(this.fullColumns)
        }
    }

    /**
     * Pre-selected items
     * @type object[]
     * @default []
     */
    @api get selectedRecords(){
        return this.privateSelectedRecords;
    }

    set selectedRecords(value){
        this.privateSelectedRecords = value;
        this.preSelectItems(value)
        this.computeRows()
    }

    /**
     * If the table should have a button to export to Excel file
     * @type Boolean
     * @default false
     */
    @api exportable = false

    /**
     * If the table is exportable, the name of the file exported
     * @type String
     * @default 'datatableExport.xls'
     */
    @api exportFileName = 'datatableExport.xls'

    /**
     * If the table should unhide all columns before export
     * @type boolean
     * @default false
     */
    @api exportHiddenColumns = false

    /**
     * If the table should have a button to display detail columns
     * @type Boolean
     * @default false
     */
    @api
    get detailable() {
        return this._detailable
    }

    set detailable (val) {
        this._detailable = val
        this.computeColumnModel();
    }

    _detailable = false

    /**
     * HTML template to apply to the table details
     * @type String
     * @default '''
     */
    @api detailsTemplate = undefined

    /**
     * Function that will recieve each row and should return an object with additional data to pass to the detail template
     * @type Function
     * @default '''
     */
    @api detailsData;

    /**
     * If the table should allow inline editing for rows
     * @type Boolean
     * @default false
     * @deprecated
     */
    @api editable = false

    /**
     * If the table should wait until confirmation to emit edition events
     * @type Boolean
     * @default false
     * @deprecated
     */
    @api editOnDemand = false

    /**
     * If the message with "No results found" should be displayed when there is no data to render
     * @type Boolean
     * @default false
     */
    @api hideNoResults = false

    /**
     * Level of the alert for the "No result found". Valid values are 'shade', 'info', 'warning', 'error'
     * @type String
     * @default 'shade'
     */
    @api noResultsLevel = 'shade'

    /**
     * Height of the "no results" panel, in pixels.
     * @type string
     * @default undefined
     */
    @api noResultsHeight;

    /**
     * If the table should allow inline editing for rows
     * @type Boolean
     * @default false
     * @private
     */
    massiveEditable = false

    /**
     * If the table should allow to search by columns
     * @type Boolean
     * @default false
     */
    @api
    get filterable() { return this._filterable }

    set filterable(val) {
        this._filterable = val
        this.computeColumnModel()
    }

    _filterable = false

    /**
     * Rows/items to be managed by the table
     * @type object[]
     * @default []
     */
    @api
    get rows(){
        return this.privateRows;
    }

    /**
     * Reflected property setter. Sets keys on rows to be processed later
     * @private
     */
    set rows(value) {
        this.privateRows = []
        if(value) {
            this.privateRows = value.map((r, idx) => {
                const row = {...r}
                row.key = `rk${idx}`
                return row
            })
        }
        this.computeRows()
    }

    /**
     * Label to display in the search input
     * @type string
     * @default ''
     */
    @api searchLabel = '';

    /**
     * Number of row to display per page
     * @type number
     * @default 0
     */
    @api rowsPerPage = 0;

    /**
     * If the selection would allow multiple rows. Only applies if 'selectable' is true
     * @type Boolean
     * @default false
     */
    @api multiselectable;

    /**
     * If the table should allow selection of records
     * @type Boolean
     * @default false
     */
    @api
    get selectable() {
        return this._selectable
    }

    set selectable (val) {
        this._selectable = val
        this.computeColumnModel();
    }

    _selectable = false

    /**
     * If the table header should be hidden
     * @type Boolean
     * @default false
     */
    @api hideHeader = false;


    /**
     * If the search input should be displayed
     * @type Boolean
     * @default false
     */
    @api showSearchInput;

    /**
     * If the row numbers should be displayed
     * @type Boolean
     * @default false
     */
     @api showRowNumberColumn;

    /**
     * The unique field present in all 'rows' passed to the table.
     * This field is important as it will be neccesary to speed up thing a find row status quickier
     * @type string
     * @default 'Id'
     */
    @api uniqueField = 'Id';

    /**
     * Tooltip information
     * @type string[]
     * @default null
     * @todo David
     */
    @api tooltipInfo;

    /**
     * Whether the infinity scrolling is active or not
     * @type Boolean
     * @default false
     */
    @api enableInfiniteLoading = false

    /**
     * By default, the datatable of scroll will try to load the augment the page size if pagination is enabled.
     * Disable this to get full control of the inifinite scroll and hook you "onloadmore" event
     * @type Boolean
     * @default false
     */
    @api disableInfiniteLoadingDefaults = false

    /**
     * Whether a spinner should be displayed in the body of the table
     * @type Boolean
     * @default false
     */
    @api isLoading = false

    /**
     * Event name to emit when click on a clickable cell
     * @type string
     * @default ''
     */
    @api eventMethod = '';

    /**
     * If the checkbox selectors must be disabled
     * @type Boolean
     * @default false
     */
    @api disabledSelectors;

    /**
     * If the search should only apply to data appearing in columns instead of all fields
     * @type boolean
     * @default false
     */
    @api searchOnlyInColumns = false

    /**
     * If the search should ignore diacritics when searching (high performance impact)
     * @type boolean
     * @default false
     */
    @api fuzzySearch = false

    /**
     * If the rows should be draggable
     * @type boolean
     * @default false
     */
    @api draggableRows = false

    /**
     * If the table column headers will be resizable or not
     * @type boolean
     * @default false
     */
    @api resizable = false

    /**
     * If the table column with must be adjusted to avoid horizontal scroll
     * @type {boolean|string}
     * @default false
     */
    @api adjustColumnsWidth = false

    /**
     * If the table column with must be adjusted to avoid horizontal scroll. If set to true "fixedWidth" in columns will not work
     * @type {boolean}
     * @default false
     */
     @api adjustToParentWidth = false

    /**
     * True to apply "striped" style to the column rows
     * @type boolean
     * @default false
     */
    @api striped = false

    /**
     * True to disable row hover
     * @type boolean
     * @default false
     */
    @api noHover = false

    /**
     * True to remove cell paddings
     * @type boolean
     * @default false
     */
    @api compact = false

    /**
     * True to apply borders to the cells
     * @type boolean
     * @default false
     */
    @api bordered = false

    /**
     * Configuration in-table for the filter status, that is, where to display the rows and pagination information
     * @type object
     * @default "{ position: 'bottom', details: true }"
     */
    @api filterStatus = {
        position: 'bottom',
        details: true
    }

    get statusBottom() {
        return this.filterStatus.position === 'bottom'
    }

    get statusTop() {
        return this.filterStatus.position === 'top'
    }

    get filterStatusText() {
        // console.time('omDatatable get filterStatusText')

        let text = `${this.labels.showing  } ${  this.currentStartRecord  } ${  this.labels.to  } ${  this.currentLastRecord  } ${  this.labels.of  } ${  this.paginator.totalRecords}`

        if(this.filterColumnLabels && this.filterColumnLabels.size > 0 && this.filterStatus.details) {
            text += ` ·  ${  this.labels.filteredBy  } `
            this.filterColumnLabels.forEach((val) => {
                text += (`${val.label  }, `)
            })
            text = text.substring(0, text.length - 2);
        }
        // console.timeEnd('omDatatable get filterStatusText')

        return text
    }

    /**
     * Search term to apply.
     * @type string
     * @default ''
     */
    @api
    get searchTerm(){
        return this.privateSearchTerm;
    }

    /**
     * Reflected setter for 'seachTerm'. Evaluates new rows when changing
     * @type string
     * @private
     */
    set searchTerm(val){
        if(val === null || val === undefined) this.privateSearchTerm = '';
        else this.privateSearchTerm = val;

        this.computeRows()
        this.emitSearchValue()
    }

    /**
     * If full paginator should be hidden
     * @type Boolean
     * @default false
     */
    @api hidePaginator;

    /**
     * If paginator "Show {} of {}" text should be hidden
     * @type Boolean
     * @default false
     */
    @api hidePaginatorShow;

    /**
     * If paginator central buttons should be hidden
     * @type Boolean
     * @default false
     */
    @api hideNumberButtons;

    /**
     * If paginator "Next" and "Previous" buttons should be hidden
     * @type Boolean
     * @default false
     */
    @api hideControlButtons = false;

    /**
     * If paginator rows per page selector should be hidden
     * @type Boolean
     * @default false
     */
    @api hideRowsNumberSelector = false;

    /**
     * If select all checkbox should be hidden
     * @type Boolean
     * @default false
     */
    @api hideSelectAll = false;

    /**
     * If column actions should be hidden in all columns
     * @type Boolean
     * @default false
     */
    @api hideColumnActions = false;

    /**
     * The CSS class to apply to each row. Receives a "row" object and should return a string
     * @type function
     * @params "row" Each row
     * @default undefined
     */
    @api rowClass;

    /**
     * The CSS inline style to apply to each row. Receives a "row" object and should return a string
     * @type function
     * @params "row" Each row
     * @default undefined
     */
    @api rowStyle;

    /**
     * The CSS inline style to apply to each cell. Receives a "cell" object and should return a string
     * @type function
     * @params "cellValue" Each cell
     * @params "row" Each cell
     * @params "cell" Each cell
     * @default undefined
     */
    @api cellStyle;

    /**
     * Set if the cell have to be disabled
     * @type function
     * @params "cellValue" Each cell
     * @params "row" Each cell
     * @params "cell" Each cell
     * @default undefined
     */
    @api disabledCell;

    /**
     * The actions to display for each row. Receives a "row" object and should return an object with the properties
     * "label", "disabled", "event", "name", "iconName". Will be displayed in the column of type "actions"
     * @type function
     * @params "row" Each row
     * @default undefined
     */
    @api rowActions;

    /**
     * Default render mode. Valid values are 'table', 'list', 'grid'
     * @type String
     * @params "row" Each row
     * @default 'table'
     *
     */
    @api get defaultView() {
        return this.viewMode
    }

    set defaultView(val) {
        this.viewMode = val
    }

    get isTable() { return this.viewMode === 'table' }

    get isList() { return this.viewMode === 'list' }

    get isGrid() { return this.viewMode === 'grid' }

    /**
     * Default value for column filter operator. Valid values are 'AND' or 'OR'
     * @type String
     * @default 'OR'
     *
     */
    @api
    get defaultFilterOperator() {
        return this.filterOperator
    }

    set defaultFilterOperator(val) {
        this.filterOperator = val
    }

    /**
     * Function to calculate row icon / image. It also can be a fixed string
     * @type function | string
     * @params "row" Each row
     * @default 'standard:record'
     *
     */
    @api listIcon = 'standard:record'

    /**
     * Function to indicate if the row is selectable or not
     * @type function
     * @params "row" Each row
     * @default undefined
     *
     */
    @api selectStatus;

    /**
     * Api method to select / deselect all rows
     * @param {boolean} check It select (true) or unselect (false) all visible rows
+     */
    @api selectAll(check){
        this.template.querySelectorAll('input[type="checkbox"]').forEach(cb => {cb.checked = check})
        if(check === true) {
            this.selectAllDisplayedRows();
        } else {
            this.selectedRows = []
        }

        this.computeRows()
        this.emitSelection()
    }

    selectAllDisplayedRows() {
        this.displayedRows.forEach(row => {
            if(!this.selectedRows.find(r => r.key === row.key) && row.selectable) {
                this.selectedRows.push(row)
            }
        })
    }

    /**
     * Add pre-selected item to selection array
     * @param {Array} items Pre-selected items to add to selection
     * @private
     */
    preSelectItems(items) {
        if(Array.isArray(items)){
            items.forEach(val => {
                if(!this.selectedRows.find(row => val[this.uniqueField] === row.item[this.uniqueField])) {
                    const existing = this.computedRows.find(row => row.item[this.uniqueField] === val[this.uniqueField])
                    if(existing) {
                        this.selectedRows.push(existing)
                    } else {
                        this.selectedRows.push({ item: val })
                    }
                }
            })
        } else if(!this.selectedRows.find(row => items === row.item[this.uniqueField])) {
            const existing = this.computedRows.find(row => row.item[this.uniqueField] === items)
            if(existing) {
                this.selectedRows.push(existing)
            } else {
                const it = {}
                it[this.uniqueField] = items
                this.selectedRows.push({ item: it })
            }
        }
    }

    /**
     * Datetime format to show (in moment.js notation). Example: 'DD MM YYYY hh:mm:ss'
     * @type string
     * @default ''
     */
    @api datetimeFormat = '';

    /**
     * True to force table menu options to be hidden
     * @type Boolean
     * @default false
     */
    @api hideOptionsMenu = false

    /**
     * True to delegate the sorting to the parent component, that is, the datatable will not handle sorting
     * @type Boolean
     * @default false
     */
    @api delegateSort = false


    /**
     * Private order object. Track column key and order selected
     * @type object
     * @private
     */
    @track order = {
        key: null,
        direction: 'asc'
    }

    viewMode = 'table'

    computedRows = [];

    privateRows = [];

    privateSelectedRecords;

    // Variable with uniqueIds of rows selected
    @track selectedRows = []

    numberEditingCells = 0

    get canDragRows() {
        return this.numberEditingCells === 0 && this.draggableRows
    }

    @track detailedRows = []

    @track displayedRows = [];

    @track responsiveClasses = {
        paginator : {
            body : 'slds-grid slds-grid_align-spread slds-var-m-top_small',
            left : SLDS_ORDER_1,
            center : SLDS_ORDER_2,
            right : SLDS_ORDER_3
        },
        top : {
            input : 'slds-col slds-size_2-of-8 slds-col_bump-right',
            buttons : 'slds-col slds-col_bump-left slds-text-align_right slds-p-right_x-small'
        },
        isMobile : false
    }

    /**
     * Seach term
     * @type string
     * @private
     */
    @track privateSearchTerm = '';

    timer = 0;

    /**
     * @typedef {object} Paginator
     * @property {number} totalPages - The total number of pages
     * @property {number} currentPage - The total number of pages
     * @property {number} pageSize - The current page size
     * @property {number} totalRecords - The total number of records
     */

    /**
     * @type Paginator
     */
    @track paginator = { totalPages: 0, currentPage: 1, pageSize: 10, totalRecords: 0 };

    pageRowsOptions = [{ label: 5, value: 5 } , { label: 10, value: 10 } , { label: 20, value: 20 } , { label: 50, value: 50 } , { label: 100, value: 100 } , { label: 200, value: 200 }];

    labels = {
        selectAll,
        select,
        showing,
        to,
        of,
        previous,
        next,
        show,
        search,
        noResultFound,
        filterIn,
        filteredBy,
        unhideColumns,
        showAsTable: `${showAs  } ${  table}`,
        showAsList: `${showAs  } ${  list}`,
    };

    get getSearchLabel() {
        return this.searchLabel ? this.searchLabel : this.labels.search;
    }

    _maxTreeDepth = 1

    _numberOfGroupings = 0

    setFullColumns(columns) {
        if(!columns || (columns && columns.length === 0)) {
            return
        }
        this._maxTreeDepth = 1
        this._numberOfGroupings = 0
        const finalCols = []
        let lastStackingIndex = 0
        const self = this

        function clearInvalidValues(st) {
            let res = st.replace(/\s/g,'')
            // res = res.replace('_','-')
            res = res.replace('.','-')
            return res;
        }

        function setColumnBaseData(col, groupKey, idx, depth) {
            // var autoCol = JSON.parse(JSON.stringify(col))
            const autoCol = { ...col}
            // dump 'value'

            if(col.value) {
                autoCol.value = col.value;
            }
            if(depth >= self._maxTreeDepth) self._maxTreeDepth = depth;

            autoCol.depth = depth

            if(!autoCol.filter) {
                autoCol.filter = ''
            }

            autoCol.filterAttributes = getFilterAttributes(col)

            if(self._clickableColumns && self._clickableColumns.includes(autoCol.fieldName)) {
                autoCol.isClickable = true
            }

            if(autoCol.sortable === undefined){
                if(autoCol.columns){
                    autoCol.sortable = false
                } else {
                    autoCol.sortable = true
                }
            }

            if(autoCol.truncate === undefined){
                autoCol.truncate = false
            }
            if(autoCol.wrapText === undefined){
                autoCol.wrapText = false
            }
            if(autoCol.editable === undefined){
                autoCol.editable = false
            }
            if(autoCol.massiveEditable === undefined || autoCol.editable === false){
                autoCol.massiveEditable = false
            }
            if(autoCol.hidable === undefined){
                autoCol.hidable = false
            }
            if(autoCol.hidden === undefined){
                autoCol.hidden = false
            }
            if(autoCol.stackable === undefined){
                autoCol.stackable = false
            }
            if(autoCol.stacked === undefined){
                autoCol.stacked = false
            }
            if(autoCol.filterable === undefined){
                autoCol.filterable = true
            }
            if(autoCol.hideActions === undefined){
                autoCol.hideActions = false
            }
            if(!autoCol.key){
                if(col.fieldName) autoCol.key = clearInvalidValues(col.fieldName)
                else if(col.label) autoCol.key = clearInvalidValues(col.label)
                else if(col.index) autoCol.key = clearInvalidValues(col.index)
            }
            if(groupKey) {
                autoCol.groupKey = groupKey;
            }

            // Compute style from properties if not existing
            if(!autoCol.style) {
                let stl = ''
                if(autoCol.fixedWidth) {
                    stl += ` width: ${  autoCol.fixedWidth  }px;`
                    stl += ` min-width: ${  autoCol.fixedWidth  }px;`
                }

                autoCol.style = stl
            }

            // restrict thing for actions
            if(autoCol.type === 'actions') {
                autoCol.sortable = false
            }

            if(autoCol.rowGrouping !== undefined) {
                self._numberOfGroupings += 1;
            }

            // set index based on inital stacked status
            if(depth === 0) {
                if(autoCol.stacked) {
                    // If we have previous not stacked cols, go before them
                    if(lastStackingIndex < idx) {
                        finalCols.forEach((c) => {
                            if(c.stacked === false) c.index += 1;
                        })
                    }
                    autoCol.index = lastStackingIndex;
                    lastStackingIndex += 1;
                } else {
                    autoCol.index = idx;
                }
            }


            if(col.columns){
                autoCol.columns = col.columns.map((childCol, i) => setColumnBaseData(childCol, autoCol.key, i, depth + 1))
            }

            return autoCol;
        }


        if(columns) {
            this.fullColumns = [];
            columns.forEach((col, idx) => {
                const autoCol = setColumnBaseData(col, null, idx, 0)
                finalCols.push(autoCol)
            })
            this.fullColumns = finalCols;
            this.computeColumnModel()
        }
    }

    @track columnModel = {}

    computeColumnModel() {
        // console.time('omDatatable computeColumnModel')
        const self = this;
        if(!this.fullColumns || this.fullColumns.length === 0) {
            this.columnModel = {rowspan : 1, rows : []}
            return
        }
        let rows = [];
        let topRow = {};
        const dataColumns = [];
        let visible = 0;
        let maxDepth = 1;
        let hiddenColumns = 0;
        let stackedColumns = 0;

        function setColumnColspanRowspan(column, depth) {
            if(depth + 1 > maxDepth) maxDepth = depth + 1;
            let colspan = 1;
            column.depth = depth;// just for testing
            if(column.hidden === true) hiddenColumns += 1;
            if(column.stacked === true) stackedColumns += 1;
            if(column.columns){
                colspan = 0;
                column.columns.forEach((col) => {
                    if(!col.hidden) colspan += 1;
                    colspan += setColumnColspanRowspan(col, depth + 1) - 1;
                })
            } else {
                dataColumns.push(column)
                if(column.massiveEditable === true) {
                    self.massiveEditable = true
                }
            }
            column.colspan = colspan;

            // If it has children column or is last leve, stay to 1
            if(depth > 0 && column.columns || depth === self._maxTreeDepth) {
                column.rowspan = 1
            }
            else {
                column.rowspan = self._maxTreeDepth - depth + 1
            }

            if(rows[depth]){
                rows[depth].columns.push(column)
            } else {
                rows[depth] = {columns : [column]}
            }

            return colspan;
        }

        this.fullColumns.sort((a, b)=> a.index - b.index).forEach((column) => {
            setColumnColspanRowspan(column, 0)
        })

        visible = dataColumns.length + (this.detailable ? 1 : 0) + (this.selectable ? 1 : 0)

        topRow = rows[0]
        topRow.columns = topRow.columns.map(col => {
            if(col.columns) {
                col.rowspan = 1
            } else {
                col.rowspan = maxDepth
            }

            return col
        })
        rows.shift()

        rows = rows.map((row, idx) => {
            row.index = idx
            return row
        })

        let noFilterableRowspan = maxDepth
        if(this.filterable) noFilterableRowspan += 1
        if(this.massiveEditable) noFilterableRowspan += 1

        this.columnModel = {
            visible,
            maxDepth : this._maxTreeDepth,
            noFilterableRowspan,
            rowspan : maxDepth,
            topRow,
            headerRows : rows,
            dataColumns,
            stacked : stackedColumns,
            hasStacked: stackedColumns > 0,
            hidden : hiddenColumns
        }
        this._isColumnFixed = false;
        this._dontStackAllColumns = false
        this._hasToCalculateWidths = true

        // console.timeEnd('omDatatable computeColumnModel')

    }

    get tableClass() {
        // console.time('omDatatable get tableClass')
        let base = 'slds-table slds-table_bordered slds-table_edit slds-table_fixed-layout'

        if(this.adjustToParentWidth) {
            base += ' table-full '
        } else {
            base += ' table-auto '
        }

        if(this.resizable) {
            base += ' slds-table_resizable-cols '
        }
        if(this.striped) {
            base += ' slds-table_striped '
        }
        if(this.noHover) {
            base += ' slds-no-row-hover '
        }
        if(this.bordered) {
            base += ' slds-table_col-bordered '
        }
        if(this.columnModel?.hasStacked) {
            base += ' with-stacked-cols'
        }
        // console.timeEnd('omDatatable get tableClass')

        return base;
    }

    get cellClass() {
        return `om-datatable-td ${this.noHover ? '' : ' slds-cell-edit '}`
    }

    filterOperator = 'OR'

    get filterOption() {
        return {
            label: `${omFilterOperator  } ${  this.filterOperator}`,
            icon: 'utility:toggle'
        }
    }

    toggleFilterOption() {
        if(this.filterOperator === 'AND') this.filterOperator = 'OR'
        else this.filterOperator = 'AND'
        this.computeRows()
    }

    get anyColumnHidden() {
        return this.columnModel.dataColumns.find((c) => c.hidden === true) !== undefined
    }

    unHideAllColumns () {
        this.columnModel.dataColumns.forEach((c) => { c.hidden = false })
        this._isColumnFixed = false;
        this._dontStackAllColumns = false;
    }

    switchViewMode (ev) {
        const mode = ev.target.dataset.value
        this.viewMode = mode
    }

    get showOptionsMenu() {
        return !this.hideOptionsMenu && (this.filterable)
    }


    get hasDisplayedRows() {
        return this.displayedRows.length > 0
    }

    get pagesList() {
        // console.time('omDatatable get pagesList')

        const pages = [];
        let points = false;
        for(let i = 1 ; i <= this.paginator.totalPages ; i++){
            if(this.paginator.totalPages <=5 || (this.paginator.totalPages > 5 && (i <= 2 || i > (this.paginator.totalPages -2 ))) || this.paginator.currentPage === i){
                if(this.paginator.currentPage.toString()===i.toString()){
                    pages.push({pageNumber: i, isActive: true})
                } else{
                    pages.push({pageNumber: i, isActive: false})
                }
            }
            if(this.paginator.totalPages>5 && i>2 && i<(this.paginator.totalPages-2) && !points){
                pages.push({pageNumber: '...', isActive: false})
                points = true;
            }
        }
        // console.timeEnd('omDatatable get pagesList')

        return pages;
    }

    get currentStartRecord() {
        return this.paginator.totalRecords === 0
            ? 0
            : ((this.paginator.currentPage - 1) * this.paginator.pageSize) + 1
    }

    get currentLastRecord() {
        return ((this.paginator.currentPage - 1) * this.paginator.pageSize) + this.paginator.pageSize > this.paginator.totalRecords
            ? this.paginator.totalRecords
            : ((this.paginator.currentPage - 1) * this.paginator.pageSize) + this.paginator.pageSize
    }

    get showPrevious() {
        return this.paginator.currentPage > 1 && !this.hideNumberButtons;
    }


    get numberOfColumns() {
        // console.time('omDatatable get numberOfColumns')

        let number = this.fullColumns.length
        if (this.detailable) number += 1
        if (this.selectable) number += 1
        // console.timeEnd('omDatatable get numberOfColumns')
        return number
    }

    get hasActions (){
        return this.detailable
    }

    goPrevious() {
        // TODO IF
        this.paginator.currentPage -= 1;
        this.computePagination()
        this.resetTableScroll()
    }

    get showNext() {
        return (!!(this.paginator.totalPages > 1 && this.paginator.currentPage !== this.paginator.totalPages)) && !this.hideNumberButtons;
    }

    goNext() {
        // TODO IF
        // console.time('onDatatable goNext')
        this.paginator.currentPage += 1;
        this.computePagination()
        this.resetTableScroll()
        // console.timeEnd('onDatatable goNext')

    }

    goToPage(evt) {
        const {page} = evt.currentTarget.dataset
        if(page !== '...') {
            this.paginator.currentPage = page;
            this.computePagination()
            this.resetTableScroll()
        }
    }

    onSearchKeyUp(evt) {
        const isEnterKey = evt.keyCode === 13;
        if (isEnterKey) {
            this.privateSearchTerm = evt.target.value;
            this.emitSearchValue()
            this.paginator.currentPage = 1;
            this.computeRows()
        }
    }

    /**
     * Throttled search
     * @param {Object} evt The change event
     */
    goSearch(evt){
        const term = evt.currentTarget.value
        if (this._searchThrottlingTimeout) {
            clearTimeout(this._searchThrottlingTimeout)
        }
        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this._searchThrottlingTimeout = setTimeout(() => {
            this.privateSearchTerm = term;
            this.paginator.currentPage = 1;
            // re-compute rows as search has changed
            this.emitSearchValue() // DCR
            this.computeRows()
            this._searchThrottlingTimeout = null;
        }, 750)

    }

    orderByColumn (event) {
        event.stopPropagation();
        // Find column by key in column collection and return if any column is found
        /* let selectedColumnKey = event.currentTarget.dataset.columnKey;
        let selectedColumn = this.columnModel.dataColumns.find((c) => c.key === selectedColumnKey)
        if(!selectedColumn) return; */

        const selectedColumn = event.detail.column;


        // Only do anything if the clicked column is sortable
        if(selectedColumn.sortable === true) {
            // If there are not order or we change the column, just order ir asc
            if(this.order.key === null || (this.order.key !== selectedColumn.key) ) {
                this.order.key = selectedColumn.key;
                this.order.direction = 'asc';
            // If the order is the same than before, swith direction
            } else if (this.order.key === selectedColumn.key) {
                this.order.direction = (this.order.direction === 'asc') ? 'desc' : 'asc';
            }

            this.dispatchEvent(new CustomEvent('sort', { detail: { column: selectedColumn, order: this.order }}))

            // re-compute rows as order has changed
            if(!this.delegateSort) {
                this.computeRows()
            }
        }
    }

    connectedCallback() {
        let rpp = this.rowsPerPage;
        if (typeof this.rowsPerPage === 'string') {
            rpp = parseInt(this.rowsPerPage, 10);
        }

        if (this.rowsPerPage > 0) {
            this.paginator.pageSize = rpp;
            this.computeRows();
        }

        window.addEventListener("resize", () => {
            this.setResponsiveClasses();
            /*
                const tableElm = this.template.querySelector('table');
                tableElm.style.width = `auto`;
                // eslint-disable-next-line @lwc/lwc/no-async-operation
                window.requestAnimationFrame(() => {
                    this.calculateTableStyle()
                })
            */
        }, { passive: true })


    }

    _isResizing = false;

    _resizingColumn = null;

    _resizingCellRect = null;

    handleResize(ev) {
        const col = ev.detail.column;
        const {clientX} = ev.detail
        const th = this.template.querySelector(`th[data-column-key="${  col.key }"]`)
        const cellRect = th.getBoundingClientRect()
        const isstacked = th.classList.contains('stacked')
        // For the stacked ones, do nothign at the momment
        if(!isstacked) {
            const width = Math.round(clientX - cellRect.left)
            th.style.width = `${width  }px`;
            th.style.minWidth = `${width  }px`;
            th.style.maxWidth = `${width  }px`;
            this.calculateTableStyle()
        }

    }

    calculateInitialTableStyle(tableElm) {
        // console.time('omDatatable calculateInitialTableStyle')

        const dataMap = new Map();
        // console.time('omDatatable calculateInitialTableStyle adjust init')

        const tableContainer = this.template.querySelector('.datatable-container');
        const tableBounds = tableContainer.getBoundingClientRect();
        let totalColumnsWidth = 0;

        // console.timeEnd('omDatatable calculateInitialTableStyle adjust init')

        // If the parameter is set adjust columns width to their cell's content and to the table container width.
        if (this.adjustColumnsWidth) {
            // console.time('omDatatable calculateInitialTableStyle adjust')
            // Traverse first row of columns.
            // TODO ¿what appens with stacked columns?
            let extraWidth = 0;
            // console.time('omDatatable calculateInitialTableStyle adjust fisrt bucle')

            tableElm.querySelectorAll('thead>tr:first-child>th').forEach(th => {
                const k = th.dataset.columnKey
                if (k) {
                    // Is a custom column, add to map.
                    dataMap.set(k, {
                        key: k,
                        // The width of the header contents (+30 extra pixel for compensate not measured space).
                        maxWidth: (this.adjustColumnsWidth === 'ignore-headers')? 0 : (th.querySelector('c-om-datatable-head').getContentsWidth() + 30)
                    });
                } else {
                    // Isn't a custom column, add to extra width.
                    extraWidth += th.offsetWidth;
                }
            })
            // console.timeEnd('omDatatable calculateInitialTableStyle adjust fisrt bucle')


            // Traverse all the content cells to get the max width of the columns.
            // console.time('omDatatable calculateInitialTableStyle adjust second bucle')
            tableElm.querySelectorAll('tbody>tr>td[data-column-key]>c-om-datatable-cell').forEach(dtc => {
                const data = dataMap.get(dtc.column.key);
                if (data) {
                    const width = dtc.getContentsWidth();
                    if (data.maxWidth < width) {
                        data.maxWidth = width;
                    }
                }
            });
            // console.timeEnd('omDatatable calculateInitialTableStyle adjust second bucle')

            // Convert map to sorted array (from wider to finest). Get the combinated width of the columns.
            // console.time('omDatatable calculateInitialTableStyle adjust sorted')

            const sortedData = new Array(dataMap.size);
            dataMap.forEach(val => {
                // +15 extra pixel for compensate not measured space.
                val.maxWidth += 15;
                totalColumnsWidth += val.maxWidth;
                sortedData.push(val);
            });
            sortedData.sort((a,b) => b.maxWidth - a.maxWidth);
            // console.timeEnd('omDatatable calculateInitialTableStyle adjust sorted')

            // console.time('omDatatable calculateInitialTableStyle adjust while')

            // The available space for the resizable columns (container width - scroll bar width - no resizable columns width).
            const availableWidth = tableBounds.width - 8 - extraWidth;
            // If the columns overflow the container width reduce them from wider to finest.
            let index = 0;
            const len = sortedData.length;
            while (totalColumnsWidth > availableWidth) {
                // Reduce to the next column width or to 0 if there's no more columns.
                const baseWidth = (index === len - 1) ? 0 : sortedData[index + 1].maxWidth;
                // The reduction itself. The total columns width is updated.
                for (let i = 0; i <= index; ++i) {
                    totalColumnsWidth -= sortedData[i].maxWidth - baseWidth;
                    sortedData[i].maxWidth = baseWidth;
                }
                // If the reduction was enough...
                if (totalColumnsWidth <= availableWidth) {
                    // If it was too much add width to the reduced columns to match the container width.
                    if (totalColumnsWidth < availableWidth) {
                        const toAdd = (availableWidth - totalColumnsWidth) / (index + 1);
                        for (let i = 0; i <= index; ++i) {
                            sortedData[i].maxWidth += toAdd;
                        }
                    }
                    // End the reduction process.
                    break;
                } else {
                    // Repeat with the next index.
                    index += 1;
                }
            }
            // console.timeEnd('omDatatable calculateInitialTableStyle adjust while')


            // Set the width in the headers.
            // console.time('omDatatable calculateInitialTableStyle adjust write')
            tableElm.querySelectorAll('thead>tr>th[data-column-key]').forEach(th => {
                const w = dataMap.get(th.dataset.columnKey)?.maxWidth;
                if(w) {
                    th.style.cssText += `
                        width: ${w}px;
                        min-width: ${w}px;
                        max-width: ${w}px;
                    `
                }
            });
            // console.timeEnd('omDatatable calculateInitialTableStyle adjust write')

            // Set the width of the table itself.
            tableElm.style.width = `${totalColumnsWidth  }px`;
            // console.timeEnd('omDatatable calculateInitialTableStyle adjust')

        } else {
            tableElm.querySelectorAll('th').forEach(th => {
                const w = th.scrollWidth + 5
                // let w2 = th.scrollWidth
                if(th.colSpan === 1 && !th.style.width && w) {
                    th.style.width = `${w  }px`
                    th.style.maxWidth = `${w  }px`
                    th.style.minWidth = `${w  }px`
                    totalColumnsWidth += w;
                }

            })
            // Remove the scroll bar from the width
            tableElm.style.width = `${tableBounds.width - 8   }px`;
            // tableElm.style.width = (totalColumnsWidth - 8 ) + 'px';
            // tableContainer.style.width = (tableBounds.width - 8 ) + 'px';
        }
        // console.timeEnd('omDatatable calculateInitialTableStyle')
    }

    /**
     * Re-calculates the table style based on parent contraints
     * @public
     */
    @api
    recalculateInitialTableStyle() {
        const tableElm = this.template.querySelector('table')
        if(tableElm && tableElm.offsetWidth > 0) {
            this.calculateInitialTableStyle(tableElm)
            this._isTableRendered = true
        }
    }

    calculateTableStyle() {
        const tableElm = this.template.querySelector('table')
        let tableWidth = 0;
        let processedColKeys = [];

        const widthsByGroup = {};
        tableElm.querySelectorAll('th[colspan="1"]').forEach(th => {
            if(th.colSpan > 1 || processedColKeys.includes(th.dataset.columnKey)) return;

            let wdt = parseInt(th.scrollWidth, 10);
            if(!th.style.width || th.style.width === 'auto') {
                th.style.width = `${wdt }px`;
            } else {
                wdt = parseInt(th.style.width, 10);
            }
            tableWidth += wdt;
            processedColKeys.push(th.dataset.columnKey)

            if(th.dataset.columnGroupKey) {
                if(widthsByGroup[th.dataset.columnGroupKey]) {
                    widthsByGroup[th.dataset.columnGroupKey] += wdt;
                } else {
                    widthsByGroup[th.dataset.columnGroupKey] = wdt;
                }
            }
        })

        tableElm.querySelectorAll('th:not([colspan="1"])').forEach(th => {
            if(th.dataset.columnKey) {
                th.style.width = `${widthsByGroup[th.dataset.columnKey] }px`;
            }
        });



        if(tableWidth > 0) {
            tableElm.style.width = `${tableWidth  }px`;
        } else {
            tableElm.style.width = 'auto';
        }
        processedColKeys = null;
    }


    createFixedColumns(onlyNew) {
        let headerCells = [];

        if(onlyNew) {
            headerCells = this.template.querySelectorAll('[data-stacked="true"]:not(.stacked)');
        } else {
            headerCells = this.template.querySelectorAll('[data-stacked="true"]');
        }

        // const containerLeft = this._tableContainer.getBoundingClientRect().left;

        const elmsLength = headerCells.length;
        const initialWidth = (this.showRowNumberColumn ? 60 : 0) + ((this.selectable || this.multiselectable) ? 32 : 0) + (this.hasActions ? 40 : 0);
        let lastParent;
        let lastElm;
        let currentWidth = initialWidth;

        // console.log('createFixedColumns cell list size', elmsLength)
        // console.time('omDatatable fixColumns')

        // Batch changes. First read from DOM. Then update DOM
        // console.time('omDatatable fixColumns read')

        let cellStyles = new Array(elmsLength)
        for(let index = 0; index < elmsLength; ++index) {

            const cell = headerCells[index]
            const zindex = (elmsLength + 1 ) - index
            const currentParent = cell.parentNode.dataset.index;
            if(!lastElm) {
                // First element in row
                lastElm = cell
                currentWidth = initialWidth;
            } else {
                // this cell is in another row?
                if(currentParent !== lastParent) {
                    cellStyles[index-1].classes[1] = LAST_STACKED
                    currentWidth = initialWidth;
                } else {
                    currentWidth += lastElm.offsetWidth;
                }
                lastElm = cell
            }
            lastParent = currentParent;
            cellStyles[index] = {
                style:` 
                    left: ${currentWidth}px;
                `,
                classes: ['stacked']
            }
        }
        // });
        // console.timeEnd('omDatatable fixColumns read')

        // eslint-disable-next-line @lwc/lwc/no-async-operation
        requestAnimationFrame(() => {

            // console.time('omDatatable fixColumns write')

            headerCells.forEach((cell, index) => {
                cell.style.cssText += cellStyles[index].style;
                cell.classList.remove(LAST_STACKED)
                cell.classList.add(...cellStyles[index].classes)
                if(index === elmsLength - 1) {
                    cell.classList.add(LAST_STACKED)
                }
            });

            // console.timeEnd('omDatatable fixColumns write')

            cellStyles = null;
            headerCells = null;
        })
        // console.timeEnd('omDatatable fixColumns')


    }

    _isColumnFixed = false;

    onTableScroll (ev) {
        // console.time('omDatatable scroll')
        if(this.enableInfiniteLoading === true) {
            const container = ev.target;
            if(container.scrollHeight - container.scrollTop < container.clientHeight + 10) {
                this.loadMore();
                container.scrollTop -= 20;
            }
        }
        // console.timeEnd('omDatatable scroll')


    }

    _initialPageSize;

    /**
     * Call 'laodMore" actions
     * @public
     */
    @api
    loadMore() {
        // console.log('initite loading should fire')
        // No hacer nada si hemos llegado al maximo
        if(!this.disableInfiniteLoadingDefaults && this.paginator.totalPages > 1) {
            if(!this._initialPageSize) {
                this._initialPageSize = this.paginator.pageSize;
            }
            // console.log('cargando siguiente pagina')
            this.paginator.pageSize = parseInt(this.paginator.pageSize + this._initialPageSize, 10)
            this.computeRows()
        }

        /**
         * Event fired when hiiting the bottom of the scrolling area
         * @event loadmore
         */
        this.dispatchEvent(new CustomEvent('loadmore', { composed: true, bubbles: true } ))
    }

    /**
     * Resets the table scroll to the initial state
     * @public
     */
    @api
    resetTableScroll() {
        this._tableContainer.scrollTop = 0;
        this._tableContainer.scrollLeft = 0;
    }

    _tableContainer = null;

    _dontStackAllColumns = true;

    _hasToCalculateWidths = true;

    // _isTableRendered = false;
    renderedCallback() {
        // console.time('omDatatable renderedCallback')

        if(!this._isTableRendered) {
            // console.time('omDatatable calculateStyle')
            this._tableContainer = this.template.querySelector('.datatable-container')

            const tableElm = this.template.querySelector('table')
            if(tableElm && tableElm.offsetWidth > 0) {
                this.calculateInitialTableStyle(tableElm)
                this._isTableRendered = true
            }
            // console.timeEnd('omDatatable calculateStyle')

        }

        if(this._isTableRendered && this._hasToCalculateWidths) {
            this.calculateTableStyle();
            this._hasToCalculateWidths = false;
        }

        if(this._isTableRendered && !this._isColumnFixed) {
            // eslint-disable-next-line @lwc/lwc/no-async-operation
            requestAnimationFrame(() => {
                this.createFixedColumns(this._dontStackAllColumns);
            })
            this._isColumnFixed = true;
        }

        if(this.exporting === true) {
            this.finishExportToExcel()
        } else {
            // console.time('omDatatable renderedCallback setResponsiveClasses')
            this.setResponsiveClasses()
            // console.timeEnd('omDatatable renderedCallback setResponsiveClasses')
        }

        if(this._numberOfGroupings > 0) {
            this.groupRows();
        }

        // console.timeEnd('omDatatable renderedCallback')
        // console.timeEnd('omDatatable render');

        // console.log('omDatatable render memory ' + performance.memory.usedJSHeapSize + ' / ' + performance.memory.totalJSHeapSize + '(MAX: ' + performance.memory.jsHeapSizeLimit +' )')
    }

    groupRows() {
        // console.time('Row grouping..')

        const tds = this.template.querySelectorAll('td')
        const rowGroups = {}
        const groupingColumns = this.columnModel.dataColumns
            .filter(c => c.rowGrouping !== undefined)
            .sort((c1, c2) => c1.rowGrouping - c2.rowGrouping)


        const filteredTds = Array.from(tds).filter(td => td.dataset.rowgroup !== undefined)

        for(let i = 0; i < filteredTds.length; i++) {

            const td = filteredTds[i]
            const {rowgroup} = td.dataset;
            if(rowgroup) {
                // Columns has row group, get the value of the cell and add it to subgroups
                const row = this.displayedRows[parseInt(td.dataset.rowindex, 10)]

                const groupLevel = groupingColumns.findIndex(c => c.key === td.dataset.columnKey)
                let groupKey = '' // esto hay que ajustarlo
                for(let j = 0; j <= groupLevel; j++) {
                    const col = groupingColumns[j]
                    groupKey += getItemValue(row, col)
                }

                if(rowGroups[groupKey]) {
                    rowGroups[groupKey].push(td)
                } else {
                    rowGroups[groupKey] = [td]
                }

            }

        }

        // groups generated, apply rowspans

        // eslint-disable-next-line
        window.requestAnimationFrame(() => {
            Object.keys(rowGroups).forEach(groupkey => {
                const tdG = rowGroups[groupkey];
                tdG.forEach((td, idx) => {
                    if(idx === 0) {
                        td.rowSpan = tdG.length;
                        td.style.display = 'table-cell'
                        td.classList.add('group-cell');
                        // Hide the edit button
                        td.childNodes[0].inTile = true
                    } else {
                        td.classList.remove('group-cell');
                        td.style.display = 'none'
                    }
                })
            })

        })
        // console.timeEnd('Row grouping..')

    }

    _lastWidth;

    setResponsiveClasses(){
        if(this._tableContainer){
            const elmWidth = this._tableContainer.clientWidth;
            if(elmWidth === this._lastWidth) return;
            this._lastWidth = elmWidth;
            if(elmWidth > 900) {
                this.responsiveClasses.paginator.body = 'slds-grid slds-grid_align-spread slds-m-top_small'
                this.responsiveClasses.paginator.left = SLDS_ORDER_1
                this.responsiveClasses.paginator.center = SLDS_ORDER_2
                this.responsiveClasses.paginator.right = SLDS_ORDER_3
                this.responsiveClasses.isMobile = false
            } else if(elmWidth <= 900 && elmWidth > 480) {
                this.responsiveClasses.paginator.body = 'slds-grid slds-grid_align-spread slds-wrap slds-m-top_small'
                this.responsiveClasses.paginator.left = SLDS_ORDER_1
                this.responsiveClasses.paginator.center = 'slds-col slds-size_2-of-2 slds-text-align_center slds-order_3'
                this.responsiveClasses.paginator.right = SLDS_ORDER_2
                this.responsiveClasses.isMobile = false
            } else if(elmWidth <= 480) {
                this.responsiveClasses.paginator.body = 'slds-grid slds-m-top_small'
                this.responsiveClasses.paginator.left = 'slds-col slds-hide'
                this.responsiveClasses.paginator.center = 'slds-col slds-text-align_center'
                this.responsiveClasses.paginator.right = 'slds-col slds-hide'
                this.responsiveClasses.isMobile = true
            }
        }
    }

    isChecked(el) {
        // First we check in selectedRows,
        const sel = this.selectedRows.find(row => row.item[this.uniqueField] === el[this.uniqueField])
        if(sel) return true
        return false;
    }


    handleColumnAction(ev) {
        const {action} = ev.detail
        const col = this.columnModel.dataColumns.find(c => c.key === ev.detail.column.key)

        switch (action) {
        case 'hide':
            col.hidden = !col.hidden
            this._isColumnFixed = false;
            this._dontStackAllColumns = false
            this._hasToCalculateWidths = true;
            break;
        case 'wrap':
            col.wrapText = !col.wrapText
            this._isColumnFixed = false;
            this._dontStackAllColumns = false
            this._hasToCalculateWidths = true;
            break;
        case 'stack':
            col.stacked = !col.stacked
            this.setFullColumns(this.fullColumns)
            break;
        case 'exact':
            col.filterAttributes.exact = !col.filterAttributes.exact
            break;
        case 'onlyDates':
            col.filterAttributes.onlyByDates = !col.filterAttributes.onlyByDates
            break;
        default:
            break;
        }
    }

    enterDrillMode(ev) {
        const rk = ev.target.dataset.key
        this.detailedRows.push(rk)
        this.computeRows()
    }

    /**
     * Exists row edit mode
     * @param {object} ev The click event
     * @private
     */
    exitDrillMode(ev) {
        const rk = ev.target.dataset.key
        const union = this.detailedRows.filter(k => k !== rk)
        this.detailedRows = union
        this.computeRows()
    }

    /**
     * Computes the final row model and stores it in 'computedRows'
     * @fires rowslength
     * @private
     */
    computeRows() {

        // console.time('omDatatable --> computeRows()')
        if(!this.privateRows) return;

        /**
         * Event containing the full rows length
         * @event rowslength
         * @type number
         */
        this.dispatchEvent(new CustomEvent('rowslength', { detail: this.privateRows.length, composed: true, bubbles: true } ))

        let rows = [];
        if(this.privateRows && this.privateRows.length > 0) {
            this.privateRows.forEach(row => {

                const rowKey = row.key
                const rowDrilled = this.detailedRows.find(edrKey => edrKey === rowKey)

                // Only add to final rows if there is NOT search term OR if there is search term and matches it
                // If the row should be added based on filters (column or/and global)
                let toAddRow = true
                const computedRow = {
                    drilled: rowDrilled !== undefined,
                    selected: this.isChecked(row),
                    item: row,
                    key: rowKey,
                    // value: this.rowValue ? recompose(row, this.rowValue) : ''
                }
                const cssClass = this.rowClass && (typeof this.rowClass === 'function') ? this.rowClass(row) : '';
                const cssStyle = this.rowStyle && (typeof this.rowStyle === 'function') ? this.rowStyle(row) : '';
                const rowActions = this.rowActions && (typeof this.rowActions === 'function') ? this.rowActions(computedRow) : '';
                const selectable = this.selectStatus && (typeof this.selectStatus === 'function') ? this.selectStatus(computedRow) : true;
                computedRow.actions = rowActions
                computedRow.cssClass = `slds-hint-parent ${  computedRow.selected ? 'slds-is-selected ' : ''  }${cssClass}`
                computedRow.cssStyle =  cssStyle
                computedRow.selectable = selectable && !this.disabledSelectors
                computedRow.unselectable = !computedRow.selectable

                let addedByColumnFilter = false
                const isAnd = this.filterOperator === 'AND'
                if(this.columnModel?.dataColumns) {
                    const filteredColumns = this.columnModel.dataColumns.filter(fc => fc.filter !== '')
                    if(filteredColumns.length > 0) {
                        toAddRow = null
                        filteredColumns.forEach(fc => {
                            if(toAddRow == null) {
                                toAddRow = this.searchInColumn(computedRow, fc, fc.filter)
                            }
                            else {
                                const match = this.searchInColumn(computedRow, fc, fc.filter)
                                if(isAnd) {
                                    toAddRow = toAddRow && match
                                } else {
                                    toAddRow = toAddRow || match
                                }
                                addedByColumnFilter = true
                            }

                        })
                    }
                }
                // If fullfills top filter, add it
                if(this.privateSearchTerm) {

                    if(this.searchOnlyInColumns) {
                        if(this.searchByColumns(computedRow, this.privateSearchTerm)){
                            toAddRow = isAnd ? toAddRow : true;
                        } else if(!addedByColumnFilter) {
                            toAddRow = false;
                        }
                    } else if(this.searchValue(computedRow, this.privateSearchTerm)){
                        toAddRow = isAnd ? toAddRow : true;
                    } else if(!addedByColumnFilter) {
                        toAddRow = false;
                    }

                }
                if(toAddRow) rows.push(computedRow)
            })
        }

        // Apply order if any
        if(this.order && this.order.key !== null && !this.delegateSort) {
            const column = this.columnModel.dataColumns.find((c) => c && c.key === this.order.key);
            rows = sortFunction(rows, column, this.order.direction === 'desc');
        }

        // Get columns for row grouping and sort them
        this.rowGroupingColumns = this.columnModel.dataColumns ? this.columnModel.dataColumns.reduce((arr, col) => {
            if (col.rowGrouping && !col.hidden) {
                arr.push(col);
            }
            return arr;
        }, []).sort((a, b) => a.rowGrouping - b.rowGrouping) : [];
        // Order to group columns
        if (this.rowGroupingColumns.length > 1 || (this.rowGroupingColumns.length === 1 && (!this.order || this.order.key !== this.rowGroupingColumns[0].key))) {
            // The array is traversed in reverse so the higher levels are sorted last
            for (let i = this.rowGroupingColumns.length - 1; i >= 0; --i) {
                rows = sortFunction(rows, this.rowGroupingColumns[i], (this.order && this.order.key === this.rowGroupingColumns[i].key && this.order.direction === 'desc'));
            }
        }

        // Set paginator and compute pagination
        this.paginator.totalRecords = rows?rows.length:0;
        this.paginator.totalPages = rows?Math.ceil(rows.length/this.paginator.pageSize):1;
        this.computedRows = rows;

        this._isColumnFixed = false;
        this.computePagination()
        // console.timeEnd('omDatatable --> computeRows()')
    }

    computePagination() {
        // console.time('omDatatable computePagination');
        this.displayedRows = []
        if(this.hidePaginator) {
            this.displayedRows = this.computedRows.map((r, i) => {
                r._index = i
                return r;
            });
        } else {
            // force first page if current page exceeds boundaries
            if(this.paginator.totalRecords < this.paginator.pageSize) {
                this.paginator.currentPage = 1
            }
            const initialRecordIndex = this.paginator.currentPage === 1 ? 0 : (this.paginator.currentPage * this.paginator.pageSize) - this.paginator.pageSize;
            const endRecordIndex = (this.paginator.currentPage * this.paginator.pageSize)
            this.displayedRows = this.computedRows.slice(initialRecordIndex, endRecordIndex).map((r, i) => {
                r._index = i
                return r;
            });
        }

        /**
         * Event containing the data of the rows after filtering, ordering and pagination
         * @event filter
         * @type object
         * @property {number} displayedRows - The number of displayed rows after pagination,
         * @property {number} totalRows - The number of total rows filtered,
         * @property {number} pageSize - The page size,
         * @property {number} globalSearch - The global search term if any
         * @property {number} columnSearch - The column labels filtered by if any
         */
        this.dispatchEvent(new CustomEvent('filter', {
            detail: {
                displayedRows: this.displayedRows.length,
                totalRows: this.computedRows.length,
                pageSize: this.paginator.pageSize,
                globalSearch: this.privateSearchTerm,
                columnSearch: this.filterColumnLabels
            },
            composed: true,
            bubbles: true
        } ))
        // console.timeEnd('omDatatable computePagination');

    }

    onSelect(evt) {
        // Si se ha seleccionado el 'Seleccionar todos' marcamos/desmarcamos todos los check
        if(evt && evt.currentTarget && evt.currentTarget.dataset.key === 'all'){
            this.template.querySelectorAll('input[type="checkbox"]').forEach(cb => {cb.checked = evt.currentTarget.checked})
            if(evt.currentTarget.checked === true) {
                // select full page
                this.selectAllDisplayedRows();
            } else {
                // clear selection
                this.selectedRows = []
            }
            this.computeRows()
        } else {
            // Estado del checkbox
            const sel = evt.currentTarget.checked;
            // saco el elmento sobre le que he hecho clic
            const selectedkey = evt.currentTarget.dataset.key;


            // Busco en todas la filas el elemento con dicho key
            const row = this.computedRows.find(item => item.key === selectedkey)

            // añadimos o quitamos a la lista
            if(sel) {
                this.selectedRows.push(row)
            } else {
                this.selectedRows = this.selectedRows.filter(r => r.item.key !== selectedkey)
            }


            // Si la seleccion no es multiple, tengo que dejar solo el que he seleccionado
            if(!this.multiselectable) {
                this.selectedRows = this.selectedRows.filter(r => r.item.key === selectedkey)
            }

            this.computeRows()

        }

        this.emitSelection()
    }

    /**
     * Tells parents which records has been selected
     * @fires selection
     * @private
     */
    emitSelection() {
        /**
         * Event containing the selected rows
         * @event selection
         * @type Array<object>
         */
        this.dispatchEvent(new CustomEvent('selection', { detail: this.selectedRows, bubbles: true, composed: true}))
    }

    /**
     * Searches recursively a term in a given object looking over all its own properties
     * @private
     */
    searchValue(source, term) {
        let t = null;
        term.split(';').forEach(element => {
            if(element !== ''){
                if (source instanceof Array) {
                    let match = false;
                    for (let i = source.length - 1; i >= 0; i--) {
                        match = this.searchValue(source[i], element)
                        if (match) break;
                    }
                    t = t !== null ? t && match : match;
                } else if (source instanceof Object) {
                    let match = false;
                    Object.keys(source).forEach(key => {
                        if(!match) {
                            match = this.searchValue(source[key], element)
                        }
                    })
                    t = t !== null ? t && match : match;
                } else if (source != null) {
                    let strtest; let match; let comp
                    if(this.fuzzySearch) {
                        comp = removeDiacritics(element.toString().toLowerCase())
                        strtest = removeDiacritics(source.toString().toLowerCase())
                        match = new RegExp(comp.split("").join(".").replace(/[-[\]{}()*+?.,\\^$|#\\s]/g, '\\$&')).test(strtest) || strtest.indexOf(comp) !== -1
                    } else {
                        strtest = source.toString().toLowerCase()
                        match = new RegExp(element.toLowerCase().split("").join(".").replace(/[-[\]{}()*+?.,\\^$|#\\s]/g, '\\$&')).test(strtest) || strtest.indexOf(element.toLowerCase()) !== -1
                    }
                    t = t !== null ? t && match : match;
                }
            }
        })
        if(t) return t;
        return false;
    }

    searchByColumns(item, term) {
        let numMatches = 0
        let expectedMatches = 0
        term.split(';').forEach(element => {
            if(element !== ''){
                expectedMatches += 1;
                const uniqueMatches = new Set()
                for(let i = this.columnModel.dataColumns.length - 1; i >= 0; i--) {
                    const col = this.columnModel.dataColumns[i];
                    const colMatch = this.searchInColumn(item, col, element)
                    if(colMatch && !uniqueMatches.has(element)) {
                        uniqueMatches.add(element)
                        numMatches += 1
                    }
                }
            }
        })

        return expectedMatches <= numMatches
    }

    searchInColumn(item, col, term){
        let match =  false;
        const val = getItemValue(item, col)
        if(val === undefined) return false
        let strtest; let comp
        if(col.type === 'date' || col.type === 'datetime') {
            const compDate = new Date(term)
            const strTestDate = new Date(val)
            if(col.type === 'date' || (col.type === 'datetime' && col.filterAttributes.onlyByDates === true)) {
                compDate.setHours(0,0,0,0)
                strTestDate.setHours(0,0,0,0)
            }
            comp = Date.UTC(compDate.getUTCFullYear(), compDate.getUTCMonth(), compDate.getUTCDate(),compDate.getUTCHours(), compDate.getUTCMinutes(), compDate.getUTCSeconds());
            strtest = Date.UTC(strTestDate.getUTCFullYear(), strTestDate.getUTCMonth(), strTestDate.getUTCDate(),strTestDate.getUTCHours(), strTestDate.getUTCMinutes(), strTestDate.getUTCSeconds());
            // comp = compDate.getTime()
            // strtest = strTestDate.getTime()
            match = comp === strtest
        }
        else if(this.fuzzySearch) {
            comp = removeDiacritics(term.toString().toLowerCase())
            strtest = removeDiacritics(val.toString().toLowerCase())
            if(col.filterAttributes.exact === true){
                match = comp === strtest
            } else {
                match = new RegExp(comp.split("").join(".").replace(/[-[\]{}()*+?.,\\^$|#\\s]/g, '\\$&')).test(strtest) || strtest.indexOf(comp) !== -1
            }
        } else {
            strtest = val.toString().toLowerCase()
            comp = term.toString().toLowerCase()

            if(col.filterAttributes.exact === true){
                match = comp === strtest
            } else {
                match = new RegExp(comp.toString().toLowerCase().split("").join(".").replace(/[-[\]{}()*+?.,\\^$|#\\s]/g, '\\$&')).test(strtest) || strtest.indexOf(comp.toString().toLowerCase()) !== -1
            }
        }
        return match;
    }

    filterColumnLabels = new Map()

    onChangeColumnFilter(ev) {

        if (this._columnSearchThrottlingTimeout) {
            clearTimeout(this._columnSearchThrottlingTimeout)
        }

        const col = this.columnModel.dataColumns.find(c => c.key === ev.target.dataset.col)
        if(col) {
            col.filter = ev.detail !== null && ev.detail !== undefined ? ev.detail : ''
            if(col.filter !== '') {
                this.filterColumnLabels.set(col.key, col)
            } else {
                this.filterColumnLabels.delete(col.key)

            }

            // eslint-disable-next-line @lwc/lwc/no-async-operation
            this._columnSearchThrottlingTimeout = setTimeout(() => {
                this.computeRows()
            }, 750)
        }

    }

    @track filtersStatus = {
        show: true,
        variant: 'brand'
    }

    toggleFilters() {
        if(this.filtersStatus.show) {
            this.filtersStatus = {
                show: false,
                variant: 'border-filled'
            }
        } else {
            this.filtersStatus = {
                show: true,
                variant: 'brand'
            }
        }
        this._isColumnFixed = false;
    }

    onChangeColumnMassiveEdition(ev) {
        const col = this.columnModel.dataColumns.find(c => c.key === ev.target.dataset.col)
        this.template.querySelectorAll(`c-om-datatable-cell[data-col-key="${col.key}"]`).forEach(dtc => dtc.setCellValue(ev.detail) )

    }

    changePageSize(evt){
        // this.paginator.pageSize = parseInt(evt.detail[0].value, 10)
        this.resetTableScroll()
        this.paginator.pageSize = parseInt(evt.target.value, 10)
        this.computeRows()
    }

    /**
     * Tells parents which text has been entered in the global search
     * @fires 'searchterm'
     * @private
     */
    emitSearchValue(){
        /**
         * Event with the search term writed by the user
         * @event searchterm
         * @type string
         */
        this.dispatchEvent(new CustomEvent('searchterm', { detail: this.privateSearchTerm, composed: true, bubbles: true } ))
    }

    onCellTabulation(ev) {
        // find the next editable column and enter editing mode
        const nextColidx = this.columnModel.dataColumns.findIndex((col, idx) => col.editable === true && idx > ev.detail.colIndex)

        if(nextColidx !== -1) {
            this.template.querySelector(`c-om-datatable-cell[data-col-index="${nextColidx}"][data-row-index="${ev.detail.rowIndex}"]`).startCellEditing()
        }
    }

    handleCellEdit(ev) {
        if(ev.detail === true) {
            this.numberEditingCells += 1
        }
        if(ev.detail === false) {
            this.numberEditingCells -= 1
        }
    }

    setNewCellValue(evt){
        if(evt && evt.detail){
            let baseObj = this.privateRows.find(row => row.key === evt.detail.rowKey);
            // Look for the property to change.
            const identifiers = evt.detail.column.split('.');
            // Move to the next level, using Object.assign to make the objects writable.
            while(identifiers.length > 1 && baseObj != null) {
                const identifier = identifiers.shift();
                const newObj = { ...baseObj[identifier]}
                baseObj[identifier] = newObj;
                baseObj = newObj;
            }
            // If the property to change is found then set the new value.
            if (baseObj != null) {
                baseObj[identifiers.shift()] = evt.detail.value;
            }

            this.computeRows();

            // If row is selected, replace in selected array with new edited values
            const selIdx = this.selectedRows.findIndex(r => r.key === evt.detail.rowKey)
            if(selIdx !== -1) {
                this.selectedRows[selIdx] = this.computedRows.find(r => r.key === evt.detail.rowKey)
                this.emitSelection()
            }
        }
    }


    @track fileXLS = '';

    @track exporting = false


    /**
     * @type {object}
     * @property {Paginator} paginator - The previous pagiantor options object
     * @property {array} unhiddenColumnsExporting - The columns being unhidden when exporting
     */
    @track previousStatus = {
        paginator: {},
        unhiddenColumnsExporting: []
    }

    get mainClasses() {
        return {
            loader: this.exporting ? 'slds-show' : 'slds-hide',
            body: this.exporting ? 'slds-hide' : 'slds-show',
        }
    }

    /**
     * Exports the table to an excel file
     * @public
     */
    @api exportToExcel(){

        // Dump previous status
        this.previousStatus.paginator = { ...this.paginator}

        // Show all records
        this.paginator.pageSize = this.computedRows.length
        this.paginator.currentPage = 1

        // Show all columns?
        if(this.exportHiddenColumns) {
            this.columnModel.dataColumns.forEach((c, idx) => {
                if(c.hidden){
                    this.previousStatus.unhiddenColumnsExporting.push(idx);
                    c.hidden = false
                }
            })
        }

        this.computeRows()

        this.exporting = true
    }

    finishExportToExcel() {
        // eslint-disable-next-line @lwc/lwc/no-async-operation
        setTimeout(() => {
            const tableElm = this.template.querySelector('table')
            // Try 3 -- Iteramos los tHead y tBody del elemento (será lento para tablas gordas) con fors normales
            let content = ''
            if(tableElm.tHead) {
                const hrows = tableElm.tHead.rows.length;
                for(let hi = 0; hi < hrows; hi++) {
                    content += '<tr>'
                    const row = tableElm.tHead.rows[hi]
                    const clen = row.cells.length
                    for(let ci = 0; ci < clen; ci++) {
                        const cell = row.cells[ci]
                        content += `<th colspan="${cell.colSpan}" rowspan="${cell.rowSpan}">${cell.outerText}</th>`
                    }
                    content += '</tr>'
                }
            }
            if(tableElm.tBodies) {
                const trows = tableElm.tBodies[0].rows.length;
                for(let hi = 0; hi < trows; hi++) {
                    content += '<tr>'
                    const row = tableElm.tBodies[0].rows[hi]
                    const clen = row.cells.length
                    for(let ci = 0; ci < clen; ci++) {
                        const cell = row.cells[ci]
                        if(cell.style.display !== 'none') {
                            content += `<td colspan="${cell.colSpan}" rowspan="${cell.rowSpan}"> ${cell.outerText}</td>`
                        }
                    }
                    content += '</tr>'
                }
            }

            const tmp = `<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">
                <head>
                    <!--[if gte mso 9]>
                        <xml>
                            <x:ExcelWorkbook>
                                <x:ExcelWorksheets>
                                    <x:ExcelWorksheet>
                                        <x:Name>Sheet 1</x:Name>
                                        <x:WorksheetOptions>
                                            <x:DisplayGridlines/>
                                        </x:WorksheetOptions>
                                    </x:ExcelWorksheet>
                                </x:ExcelWorksheets>
                            </x:ExcelWorkbook>
                        </xml>
                    <![endif]-->
                    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
                </head>
                <body>
                    <table>${content}</table>
                </body>
            </html>`;

            const bdata = window.URL.createObjectURL(new Blob([tmp], { type : 'application/octet-stream' }))
            const downloader = this.template.querySelector('.hidden-xls-generator')
            downloader.setAttribute('href', bdata)
            downloader.setAttribute('download', this.exportFileName)
            downloader.click()


            content = ''

            this.exporting = false
            this.paginator = { ...this.previousStatus.paginator }
            this.computeRows()

            // Show all columns?
            if(this.exportHiddenColumns) {
                this.previousStatus.unhiddenColumnsExporting.forEach(i => {
                    this.columnModel.dataColumns[i].hidden = true;
                })
                this.previousStatus.unhiddenColumnsExporting = [];
            }
        }, 500)
    }

    dndStatus = {
        from: { index: -1, data: null },
        to: { index: -1, data: null }
    }

    onCellClick(ev) {
        const row = this.displayedRows.find(r => r.key === ev.currentTarget.dataset.rowKey)
        const column = this.columnModel.dataColumns.find(c => c.key === ev.currentTarget.dataset.columnKey)
        /**
         * Event emmited when a cell is clicked
         * @event cellclick
         * @type object
         */
        this.dispatchEvent(new CustomEvent('cellclick', { detail: { row, column }}))
    }

    onRowClick(ev) {
        const row = this.displayedRows.find(r => r.key === ev.currentTarget.dataset.key)
        /**
         * Event emmited when a row is clicked
         * @event rowclick
         * @type object
         */
        this.dispatchEvent(new CustomEvent('rowclick', { detail: { row }}))
    }

    onRowDrag(ev) {
        if(!this.draggableRows) return
        this.dndStatus.from.index = ev.currentTarget.dataset.index
        this.dndStatus.from.data = this.displayedRows.find(r => r.key === ev.currentTarget.dataset.key)
    }

    onRowDrop(ev) {
        if(!this.draggableRows) return
        this.template.querySelector(`tr[data-key="${ ev.currentTarget.dataset.key}"]`).classList.remove("draggingOver")
        this.dndStatus.to.index = ev.currentTarget.dataset.index
        this.dndStatus.to.data = this.displayedRows.find(r => r.key === ev.currentTarget.dataset.key)
        /**
         * Event containing the origin and destination of the drag and drop action
         * @event dragdroprow
         * @type Object
         * @property {object} from - The index and data of the column that originated the drag event
         * @property {object} to - The index and data of the column that handled the drop event
         */
        this.dispatchEvent(new CustomEvent('dragdroprow', { detail: this.dndStatus, bubbles: false }))
    }

    onRowDragOver(ev) {
        if(!this.draggableRows) return
        this.template.querySelector(`tr[data-key="${ ev.currentTarget.dataset.key}"]`).classList.add("draggingOver")
        ev.preventDefault();
    }

    onRowDragLeave(ev) {
        if(!this.draggableRows) return
        this.template.querySelector(`tr[data-key="${ ev.currentTarget.dataset.key}"]`).classList.remove("draggingOver")
        this.dndStatus.to.index = -1
        this.dndStatus.to.data = null
    }
}