/**
 * Pretty paint a code snippet using Google's prettify library
 * @module OmCodePrettifier
 * @version 1.0.0
 */
import { LightningElement, api } from 'lwc';
import { loadScript, loadStyle  } from 'lightning/platformResourceLoader';
import prettify from '@salesforce/resourceUrl/prettify';

export default class OmCodePrettifier extends LightningElement {
  /**
   * The code to prettify
   * @type string
   */
  @api code = null

  /**
   * The language of the code
   * @type string
   * @example 'html'
   * @example 'js'
   */
  @api lang = null

  prettier = null
  srInitialized = false

  printCode() {
    let data = '' + this.code;
    let lang = this.lang;
    let elm = this.template.querySelector('pre')
    let fdata
    //Count whitespace till first character
    let whiteSpaceCount = data.search(/\S/);
    //Split newLines and remove whitespaces (if exists)
    fdata = data.split('\n').map(line => {
      let fline = ''
      let toindex = -1
      for(let i = 0; i < whiteSpaceCount; i++) {
        if(line.charAt(i) !== ' ') break
        toindex++
      }

      if(toindex !== -1) {
        fline = line.substr(toindex, line.length - 1)
      }

      return fline
    }).join('\n')

    if(lang === 'html' || lang === 'xml') {
      fdata = fdata
        .replace(/></g,">\n<")
        .replace(/</g,"&lt;")
        .replace(/>/g,"&gt;")
    } else {
      fdata = data
    }

    //eslint-disable-next-line
    elm.innerHTML = this.prettier.prettyPrintOne(fdata, lang)
  }

  renderedCallback() {
    console.log('omCodePrettifier: renderedCallback called');

      if(this.prettier) {
        this.printCode()
      }
      if (this.srInitialized) {
          return;
      }

      Promise.all([
        loadStyle(this, prettify + '/styles.css'),
        loadScript(this, prettify + '/script.js')
      ])
        .then(() => {
            console.log('omCodePrettifier: prettier loaded ok')
            this.prettier = window.PR;
            this.srInitialized = true;
        })
        .catch(error => {
            console.log('omCodePrettifier: prettier load error' + error)
            this.srInitialized = true;
        });
  }

}