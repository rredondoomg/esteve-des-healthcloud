import actualizarEstadoPedido from '@salesforce/apex/OM_ActualizarEstadoPedido.actualizarEstadoPedido';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import { LightningElement, track, api } from 'lwc';


export default class OmActualizarEstadoPedido extends LightningElement {
    _recordId;
    recordId;
    @track hayErrores = false;
    @track error = '';
    @track spinner = true;

    @api set recordId(value) {
        this._recordId = value;
        if(this.recordId != 'undefined'){
            this.actualizarEstadoPedido(this.recordId);
        }
    }

    get recordId() {
        return this._recordId;
    }

    actualizarEstadoPedido(recordId){
        actualizarEstadoPedido({ recordId })
        .then(response => {
            this.spinner = false;
            console.log('Success actualizarEstadoPedido');
            console.log('Response vale: '+JSON.stringify(response));
            if(response[false] == ''){
                this.dispatchEvent(new ShowToastEvent({ title: '', message: 'Estado actualizado correctamente', variant: 'success'}));
                eval("$A.get('e.force:refreshView').fire()");
            }else if(response[true] !== 'respuestaNull'){
                if(response[true] === 'Item not found'){
                    this.dispatchEvent(new ShowToastEvent({ title: '', message: 'Pedido no encontrado', variant: 'error'}));
                    this.hayErrores = true;
                    this.error = 'El pedido no se encuentra en Atlas.';
                }else{
                    this.dispatchEvent(new ShowToastEvent({ title: '', message: response[true], variant: 'error'}));
                    this.hayErrores = true;
                    this.error = 'El estado no ha podido ser actualizado.';
                }
                
            }
            
        })
        .catch(error => {
            this.spinner = false;
            console.log('ERROR actualizarEstadoPedido '+JSON.stringify(error));
            this.dispatchEvent(new ShowToastEvent({ title: '', message: 'Error al intentar actualizar el estado del pedido desde Atlas '+ JSON.stringify(error), variant: 'error'}));

        })
    }
}