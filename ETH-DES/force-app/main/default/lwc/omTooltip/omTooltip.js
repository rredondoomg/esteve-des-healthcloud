import { LightningElement, api, track } from 'lwc';

/**
 * Component to show custom SLDS tooltips
 * @module OmTooltip
 * @version 1.0.0
 */
export default class OmTooltip extends LightningElement {

    @api icon = 'utility:info';
    @api value = '';
    @api size = 'small';
    @api bgColor = '#16325C';
    @api textColor = 'white'

    @track uniqueClass = '';

    @track show = false;

    generateId() {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        var charactersLength = characters.length;
        for ( let i = 0; i < 10; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    toggleShow() {
        this.show = !this.show;
    }
    
    connectedCallback() {
        this.uniqueClass = this.generateId();
    }

    renderedCallback() {
        if (this.hasRendered) return;
        this.hasRendered = true;
    
        const style = document.createElement('style');
        style.innerText = `
            .tooltiptext {
                background-color: ` + this.bgColor + `;
                color: ` + this.textColor + `;
          }
        `;
        this.template.querySelector('.'+this.uniqueClass).appendChild(style);
      }

}