/**
 * Generic component to authenticate, CRUD Google Drive Files, and get IA suggestions from them
 * @module OmGoogleDriveExplorer
 */

import { LightningElement, api, track, wire } from 'lwc';
import LOCALE from '@salesforce/i18n/locale';

// Cusotm templates
import { default as descriptiontemplate} from './templates/omGoogleDriveExplorerTags.html';
import { default as celltemplate} from './templates/omGoogleDriveExplorerCell.html';

// Dependencies
import OmResumableUploader from 'c/omResumableUploader';
import { getSLDSIconFromMimeType, handleError } from 'c/omContentUtils';

// Labels
import omExplorerActionEdit from '@salesforce/label/c.omExplorerActionEdit'
import omExplorerActionEditProvider from '@salesforce/label/c.omExplorerActionEditProvider'
import omExplorerActionDelete from '@salesforce/label/c.omExplorerActionDelete'
import omExplorerColumnCreatedBy from '@salesforce/label/c.omExplorerColumnCreatedBy'
import omExplorerColumnLastModifiedBy from '@salesforce/label/c.omExplorerColumnLastModifiedBy'
import omExplorerColumnName from '@salesforce/label/c.omExplorerColumnName'
import omExplorerColumnSize from '@salesforce/label/c.omExplorerColumnSize'
import omExplorerColumnTags from '@salesforce/label/c.omExplorerColumnTags'
import omExplorerDeleteSuccess from '@salesforce/label/c.omExplorerDeleteSuccess'
import omExplorerDeleteError from '@salesforce/label/c.omExplorerDeleteError'
import omExplorerSuccess from '@salesforce/label/c.omExplorerSuccess'
import omExplorerUpload from '@salesforce/label/c.omExplorerUpload'
import omExplorerUpdateSuccess from '@salesforce/label/c.omExplorerUpdateSuccess'
import omExplorerSearchAllFolders from '@salesforce/label/c.omExplorerSearchAllFolders'
import omSearch from '@salesforce/label/c.omSearch'
import omExplorerEditMassive from '@salesforce/label/c.omExplorerEditMassive'
import omExplorerRefresh from '@salesforce/label/c.omExplorerRefresh'
import omExplorerFilters from '@salesforce/label/c.omExplorerFilters'
import omExplorerProviderFilters from '@salesforce/label/c.omExplorerProviderFilters'
import omExplorerCustomProperties from '@salesforce/label/c.omExplorerCustomProperties'
import omExplorerApply from '@salesforce/label/c.omExplorerApply'
import omExplorerClearFilters from '@salesforce/label/c.omExplorerClearFilters'
import omExplorerDownload from '@salesforce/label/c.omExplorerDownload'
import omExplorerClose from '@salesforce/label/c.omExplorerClose'
import omExplorerSelectedFiles from '@salesforce/label/c.omExplorerSelectedFiles'
import omExplorerDeleteConfirm from '@salesforce/label/c.omExplorerDeleteConfirm'
import omExplorerDeleteConfirmMsg from '@salesforce/label/c.omExplorerDeleteConfirmMsg'
import omExplorerPreview from '@salesforce/label/c.omExplorerPreview'
import omExplorerPreviewLoading from '@salesforce/label/c.omExplorerPreviewLoading'
import omExplorerHelp from '@salesforce/label/c.omExplorerHelp'
import omExplorerSelectedFilesNo from '@salesforce/label/c.omExplorerSelectedFilesNo'
import omExplorerFileInfo from '@salesforce/label/c.omExplorerFileInfo'
import omExplorerFilterCreatedDate from '@salesforce/label/c.omExplorerFilterCreatedDate'
import omExplorerFilterMime from '@salesforce/label/c.omExplorerFilterMime'
import omExplorerFilterModifiedDate from '@salesforce/label/c.omExplorerFilterModifiedDate'
import omExplorerFilterScope from '@salesforce/label/c.omExplorerFilterScope'
import omExplorerFilterStarred from '@salesforce/label/c.omExplorerFilterStarred'
import omExplorerFilterTrashed from '@salesforce/label/c.omExplorerFilterTrashed'

import omExplorerSearchFullText from '@salesforce/label/c.omExplorerSearchFullText'
import omExplorerSearchNames from '@salesforce/label/c.omExplorerSearchNames'
import omExplorerSearchMGlobal from '@salesforce/label/c.omExplorerSearchMGlobal'
import omExplorerSearchMFolder from '@salesforce/label/c.omExplorerSearchMFolder'
import omExplorerInitial from '@salesforce/label/c.omExplorerInitial'
import omExplorerRoot from '@salesforce/label/c.omExplorerRoot'
import omExplorerIAMoving from '@salesforce/label/c.omExplorerIAMoving'
import omExplorerIAResults from '@salesforce/label/c.omExplorerIAResults'
import omExplorerIAWaiting from '@salesforce/label/c.omExplorerIAWaiting'
import omExplorerTagsManage from '@salesforce/label/c.omExplorerTagsManage'
import omExplorerTagsAddSelected from '@salesforce/label/c.omExplorerTagsAddSelected'
import omExplorerCreateFolder from '@salesforce/label/c.omExplorerCreateFolder'

import omAIError from '@salesforce/label/c.omAIError'
import omAIErrorConfig from '@salesforce/label/c.omAIErrorConfig'
import omAIErrorNoFile from '@salesforce/label/c.omAIErrorNoFile'
import omExplorerUploadSuccess from '@salesforce/label/c.omExplorerUploadSuccess'
import omExplorerCopySuccess from '@salesforce/label/c.omExplorerCopySuccess'

import omHelpUpload from '@salesforce/label/c.omHelpUpload'
import omHelpAiTypes from '@salesforce/label/c.omHelpAiTypes'


// Events and Message Channels
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import contentChannel from '@salesforce/messageChannel/OM_ContentChannel__c'
import { publish, MessageContext } from 'lightning/messageService'


/**
 * @class
 */
export default class OmGoogleDriveExplorer extends LightningElement {

    @wire(MessageContext)
    messageContext;

    @track labels = {
        omExplorerSearchAllFolders,
        omSearch,
        omExplorerEditMassive,
        omExplorerRefresh,
        omExplorerFilters,
        omExplorerProviderFilters,
        omExplorerCustomProperties,
        omExplorerApply,
        omExplorerClearFilters,
        omExplorerActionEdit,
        omExplorerDownload,
        omExplorerClose,
        omExplorerSelectedFiles,
        omExplorerSelectedFilesNo,
        omExplorerColumnName,
        omExplorerColumnTags,
        omExplorerDeleteConfirm,
        omExplorerDeleteConfirmMsg,
        omExplorerPreviewLoading,
        omExplorerHelp,
        omExplorerUpload,
        omExplorerFileInfo,
        omExplorerSearchFullText,
        omExplorerSearchNames,
        omExplorerSearchMGlobal,
        omExplorerSearchMFolder,
        omExplorerActionDelete,
        omExplorerIAMoving,
        omExplorerIAResults,
        omExplorerIAWaiting,
        omExplorerTagsManage,
        omExplorerTagsAddSelected,
        omHelpUpload,
        omHelpAiTypes,
        omExplorerCreateFolder
    }

    /**
     * If user should be able to create a folder
     * @type Boolean
     */
    @api selectionText = 'Add selection'

    /**
     * If user should be able to create a folder
     * @type Boolean
     */
    @api allowFolderCreation = false

    /**
     * If user should be able to navigate between folders
     * @type Boolean
     */
    @api allowNavigation = false

    /**
     * If user should be able to upload files
     * @type Boolean
     */
    @api allowUploads = false

    /**
     * If user should be able to update files
     * @type Boolean
     */
    @api allowEdition = false

    @api allowDeletion = false;
    @api allowSearch = false;
    @api allowFilters = false;
    @api allowSelection = false;
    @api allowTagging = false;
    @api allowAiSuggestions = false;

    /**
     * If user should be unable to loguut one logged in
     * @type Boolean
     */
    @api disableSignOut = false

    /**
     * Display mode of the inner datatable. It can be 'table' or 'list'
     */
    @api displayMode = 'table'

    /**
     * List of all custom properties to use
     * @type Array
     */
    @api customProperties = null

    /**
     * List of all custom app properties to use
     * @type Array
     */
    @api customAppProperties = null

    /**
     * Addtional filters to add
     * @type Object
     */
    @api driveFilters = null

    /**
     * Default language
     * @type String
     */
    @api language = 'en'

    /**
     * Default language
     * @type String
     */
    @api globalSearch = false

    /**
     * Function to generate idexable text for Drive files
     * @type Function
     */
    @api textIndexer = null

    /**
     * Function to call before the file is uploaded. Used to fill custom data on files
     * @type Function
     */
    @api fileDecorator = null

    /**
     * Function to call when accesing file info
     * @type Function
     */
    @api fileInfo = null

    /**
     * Google Cloud Storage bucket name to use to store AI info
     * @type String
     * @default 'aibucket'
     */
    @api aiBucketName = 'aibucket'
    
    /**
     * Natural Language Processing salience threshold to filter tags by
     * @type Number
     * @default 0.04
     */
    @api aiNlpSalience = 0.04

    /**
     * Minimun number of suggestions to display from AI results, even if they not fullfills salience threshold
     * @type Number
     * @default 5
     */
    @api aiMinSuggestions = 5

    /**
     * Default action on file click (not folder). It can be 'edit' or 'preview'
     * @type String
     * @default 'edit'
     */
    @api actionOnClick = 'edit'

    /**
     * initial folder from where explorer will begin to navigate
     * @type String
     * @default 'root'
     */
    @api 
    get initialFolderId () {
        return this._initialFolderId
    }

    set initialFolderId (value) {
        if (value) {
            this._initialFolderId = value
            this.filters.parentFolder.value = value
            this.filters.pathItems = [{ label: omExplorerInitial, id: value} ]
        }
    }

    _initialFolderId = 'root'
    _baseColumns = []
    composedColumns

    @api
    get baseColumns() {
        return this._baseColumns;
    }

    set baseColumns(val) {
        let cols = [
            // Action column
            { 
                sortable: false,
                hidable: false,
                type: 'actions',
                fieldName: '_none',
                fixedWidth: 50,
                label: '',
                typeAttributes: { 
                    iconName: 'utility:down'
                }
            }
        ]
        if (val) {
            val.forEach((col) => {
                if(col === 'name') {
                    cols.push({ sortable: true, hidable: false, field: 'name', style: 'min-width: 300px',  fieldName: 'name', label: omExplorerColumnName, component: { template: celltemplate }, listTitle: true })
                }
        
                if(col === 'tags') {
                    cols.push({ sortable: false, hidable: false, field: '_tags', fieldName: '_tags', label: omExplorerColumnTags , component: { template: descriptiontemplate }})
                }
        
                if(col === 'size') {
                    cols.push({ sortable: false, hidable: false, value: (item) => { return !this.isProviderFile(item) ? this.humanFileSize(item.size, true) : ''}, key: 'size', label: omExplorerColumnSize, listSubTitle: true})
                }
        
                if(col === 'createdby') {
                    cols.push({ sortable: false, hidable: false, editable: false, type: "text", field: '_createdby', fieldName: '_createdby', key: 'createdby', label: omExplorerColumnCreatedBy})
                }
        
                if(col === 'lastmodifiedby') {
                    cols.push({ sortable: false, hidable: false, field: '_lastmodifiedby', fieldName: '_lastmodifiedby', key: 'lastmodifiedby', label: omExplorerColumnLastModifiedBy, listSubTitle: true})
                }
            })
        }

       

        this._baseColumns = cols
    }

    @api get columns() {
        if(!this.composedColumns) {
            return this._baseColumns
        }
        return this.composedColumns
    }
    set columns(value) {
        let cols = [].concat(this._baseColumns)
        if(value) {
            cols = cols.concat(value)
        }
        this.composedColumns = cols
    }

    /**
     * Function to allow the component to find a langugage valid ISO code from a given file
     * Receives the Google File
     * @type Function
     */
    @api langFinder = null;

    /**
     * Google user access data (access_token and refresh_token)
     * @type Object
     */
    @api 
    get userAccess () {
        return this.uAccess
    }
    set userAccess (value) {
        this.uAccess = value
        this.storeApi.setCredentials(value)
        this.initExplorer()
    }
    

    /**
     * Provider API implementation. Should extend "omBaseApi"
     * @type Object
     * @default undefined
     */
    @api 
    get providerApi () {
        return this.storeApi
    }

    set providerApi (value) {
        this.storeApi = value
        this.initExplorer()
    }

    /**
     * Provider IA API implementation
     * @type Object
     * @default undefined
     */
     @api 
     get iaApi () {
         return this.storeApi
     }
 
     set iaApi (value) {
         this.storeApi = value
         this.initExplorer()
     }

    // Provider API wrapper instance
    storeApi = null;
    
    // Provider IA API wrapper instance
    AiApi   = null;

    //User access private  data object
    uAccess = null

    //Items to pass to the table
    @track files = []

    //If the user is signed in provider system
    @track isSignedIn = false

    //Auth access token
    @track explorerAccessToken = ''

    //Current file selected. Used in modals (info, tags, authors...)
    @track currentFile = null

    //Last raw response
    @track lastResponse = {}

    //List of files selected from the table (checkboxes)
    @track filesSelected = []

    // If slot name="options" has some content
    @track hasCustomOptions = false;

    //Modal triggers
    @track modals = {
        help: false,
        createFolder: false,
        filePreview: false,
        fileEdit: false,
        delete: false,
        massiveEdit: false,
        tags: false
    }
    //Section toggling
    @track show = {
        suggestions: false,
        dropzone: false
    }
    //Map of suggested tags for current file
    @track currentSuggestedTags = {}
    //List of suggested tags selected by the user
    @track suggestedTagsSelected = []
    get suggestedTagsSelectedStr() {
        return JSON.stringify(this.suggestedTagsSelected)
    }
    //Custom paginator model
    @track paginator = {
        lastPage: false,
        pageSize: 200,
        currentPage: 1,
        totalPages: 1000,
        totalRecords: 1,
        currentListPageTokens: [],
        previousToken: null,
        lastUsedToken: null,
        nextToken: null,
        order: null
    }

    //Filters model
    @track filters = {
        scope: { value: 'own', label: omExplorerFilterScope },
        search: { value: '', label: omSearch },
        searchMode: { value: 'local', label: omExplorerSearchMGlobal }, // base on api prop
        mimeType: { value: null, label: omExplorerFilterMime },
        createdTime: { value: null, label: omExplorerFilterCreatedDate },
        createdTimeTo: { value: null, label:omExplorerFilterCreatedDate },
        modifiedTime: { value: null, label: omExplorerFilterModifiedDate },
        modifiedTimeTo: { value: null, label: omExplorerFilterModifiedDate },
        visibility: { value: null, label: 'visibility' },
        trashed: { value: false, label: omExplorerFilterTrashed },
        starred: { value: false, label: omExplorerFilterStarred },
        parentFolder: { value: 'root', label: 'parentFolder' },
        //Custom properties
        props: {},

        // Navigation items
        pathItems: [
            { label: omExplorerRoot, id: 'root' }
        ]
    }

    //Datetime filters valid operators
    @track dateTimeOperators = ['>', '>=', '=', '!=', '<', '<=']
    //Explorer status model
    @track status = {
        working: false,
        level: 'info',
        show: false,
    }
    //Upload status
    @track uploadStatus = {
        working: false,
        level: 'info',
        percent: 0,
        show: false,
        files: []
    }
    //Statuses for IA actions
    // @track searchingInGCS = false
    @track uploadingToStorage = false
    @track uploadingToStorageProgress = 0
    @track waitingForResults = false
    @track annotatingVideo = false

    @track massiveData = {
        appProperties: {}
    }

    @track newFolderData = {
        name: ''
    }

    get filtersWithValues () {
        let filters = []
        Object.keys(this.filters).forEach(fkey => {
            let filter =  this.filters[fkey]
            if(fkey === 'props') {
                Object.keys(filter).forEach(cp => {
                    let customPro = this.customAppProperties.find(p => p.key === cp)
                    if(customPro && filter[cp] !== '') {
                        filters.push({isCustomProp: true, key: cp, label: customPro.label })
                    }
                })
            }
            else if( (fkey !== 'parentFolder' && fkey !== 'search' && fkey !== 'searchMode' && fkey !== 'pathItems' ) && (filter.value && filter.value !== 'own')) {
                //filters.push({key: fkey, label: filter.label + ': ' + filter.value})
                filters.push({isCustomProp: false, key: fkey, label: filter.label })
            }
        })
        return filters;
    }

    get hasIAEnabled() {
        return this.allowAiSuggestions
    }

    get previewFrameClass() {
        return this.storeApi.provider + ' previewFrame'
    }

    get isOneDrive() {
        return this.storeApi.provider === 'OneDrive'
    }

    get isSharepoint() {
        return this.storeApi.provider === 'Sharepoint'
    }

    get isGoogleDrive() {
        return this.storeApi.provider === 'GoogleDrive'
    }

    get canSearchGlobally () {
        return this.allowNavigation === true && this._initialFolderId === 'root'
    }

    get hasCustomAppProperties () {
        return this.customAppProperties != null
    }

    get explorerLogo () {
        return this.storeApi ? this.storeApi.getLogoUrl() : ''
    }

    get explorerTitle () {
        return this.storeApi ? this.storeApi.getTitle() : 'Content explorer'
    }

    //Computes if there are some files selected in table
    get hasFilesSelected() {
        return this.filesSelected && this.filesSelected.length > 0
    }

    get showExplorerOptions () {
        return (this.allowEdition && this.allowSelection) || this.allowFolderCreation || this.hasCustomOptions
    }

    get suggestedTagsFlat() {
        let retrieved = []

        Object.keys(this.currentSuggestedTags).forEach(skey => {
            retrieved = retrieved.concat(this.currentSuggestedTags[skey]).filter(r => r.name.length < 80)
        })

        //Append currentfile tags and make an unique set
        if(this.currentFile && this.currentFile.description) {
            let filetags = this.currentFile.description.split('#').map(n => n.trim()).filter((n) => n.trim().length > 0);
            retrieved = retrieved.filter(r => {
                return filetags.indexOf(r.name.trim()) === -1
            })
        }

        retrieved = [... new Set(retrieved)]

        return retrieved
    }


    /**
     * METHODS
     */

    handleSort(ev) {
        this.paginator.order = ev.detail.order;
        this.listFiles(null, true)
    }

    handleRowActions(ev) {
        let row = ev.detail.row
        switch (ev.detail.action) {
            case 'fileclick':
                this.onFileClick(row)
            break;
            case 'filepreview':
                this.openPreview(row)
            break;
            case 'fileedit':
                this.onFileEdit(row)
            break;
            case 'filedelete':
                this.onFileDelete(row)
            break;
            case 'fileeditprovider':
                if (row.webUrl) {
                    window.open(row.webUrl, '_blank')
                } else if (row.webViewLink) {
                    window.open(row.webViewLink, '_blank')
                }
            break;
            default:
        }
    }

    rowActions = (row) => {
        let acts = []

        if (!this.storeApi.isFolder(row)) {
            acts.push({ label: omExplorerPreview, name: 'filepreview', event: 'filepreview', iconName: 'utility:preview' })
        }
        if (this.allowEdition) {
            acts.push({ label: omExplorerActionEdit, name: 'fileedit', event: 'fileedit', iconName: 'utility:edit' })
        }
        if (this.allowDeletion) {
            acts.push({ label: omExplorerActionDelete, name: 'filedelete', event: 'filedelete', iconName: 'utility:delete' })
        }

        acts.push({ label: omExplorerActionEditProvider + ' ' + this.explorerTitle, name: 'fileeditprovider', event: 'fileeditprovider', iconName: 'utility:edit_form', disabled: this.isFolder(row) })
        return acts
    }

    handleOptionsSlotChange (evt) {
        this.hasCustomOptions = evt.target.assignedElements().length !== 0;
    }

    onSearch(term) {
        this.filters.search.value = term;
        this.listFiles(null, true);
    }

    @api
    refreshPage() {
        this.listFiles(null, true);
    }

    toggleFiltersMenu() {
        if (this.$refs.filtersMenu) this.$refs.filtersMenu.toggleMenu();
    }

    onSelectItems(payload) {
        this.filesSelected = payload.detail
        this.dispatchEvent(new CustomEvent('selection', { detail: this.filesSelected, bubbles: true, composed: true}))
    }

    onNextPage() {
        this.paginator.currentPage++;
        this.listFiles(1, false);
    }

    onPrevPage() {
        this.paginator.currentPage--;
        this.paginator.lastPage = false;
        this.listFiles(0, false);
    }

    get disablePrevious() {
        return this.paginator.currentPage <= 1;
    }

    get disableNext() {
        return this.paginator.lastPage;
    }

    getFolderPath() {
        return this.filters.pathItems;
    }

    /**
     * Creates an object with app defined custom properties with empty values to ensure existance
     */
    getDefaultAppProperties() {
        let def = {};
        if (this.customAppProperties) {
            this.customAppProperties.forEach((e) => {
                def[e.key] = '';
            });
        } else {
            def = null;
        }

        return def;
    }

    applyFilters() {
        this.listFiles(null, true);
        this.toggleFilters()
    }

    clearFilter(ev) {

        let filterKey = ev.target.dataset.key
        let isCustomProp = ev.target.dataset.custom === 'true'

        if (isCustomProp) {
            this.filters.props[filterKey] = ''
        } else {
            //special cases
            switch (filterKey) {
                case 'scope':
                    this.filters.scope.value = 'own';
                    break
                case 'search':
                    this.filters.search.value = '';
                    this.clearSearch();
                    break;
                case 'parentFolder':
                    this.filters.parentFolder.value = null;
                    this.filters.pathItems = [];
                    break;
                default:
                    if (this.filters[filterKey].value === true) {
                        this.filters[filterKey].value = false
                    } else {
                        this.filters[filterKey].value = null
                    }
                    // this.$set(this.filters, filterKey, null)
            }
        }

        this.listFiles(null);
    }

    clearFilters() {
        // this.$refs.filtersMenu.hideMenu();
        this.toggleFilters()
        this.clearSearch();

        this.filters.scope.value = 'own'
        this.filters.search.value = ''
        this.filters.mimeType.value = null
        this.filters.createdTime.value = null
        this.filters.createdTimeTo.value = null
        this.filters.modifiedTime.value = null
        this.filters.modifiedTimeTo.value = null
        this.filters.visibility.value = null
        this.filters.trashed.value = false
        this.filters.starred.value = false
        this.filters.parentFolder.value = this.initialFolderId
        this.filters.props = {}
        
        this.listFiles(null, true);
    }

    clearSearch() {
        let el = this.template.querySelector('lightning-input[data-id="searchInput"');
        if (el) {
            el.value = ''
        }
        this.filters.search.value = '';
        this.isSearching = false;
    }

    onClickFolderPath(event) {
        this.goToFolder(event.target.dataset.id, event.target.dataset.label);
    }

    goToFolder(folderId, folderName) {

        // Limpiar busqueda
        this.clearSearch()

        this.filters.search.value = '';
        this.isSearching = false;

        //look if already exists in path
        let cIndex = this.filters.pathItems.findIndex(it => it.id === folderId)
        let newItems = [this.filters.pathItems[0]];
        if (cIndex !== -1) {
            newItems = this.filters.pathItems.slice(0, cIndex + 1);
            this.filters.pathItems = newItems;
        } else {
            this.filters.pathItems.push({ label: folderName, id: folderId })
        }

        this.filters.parentFolder.value = folderId;
        this.dispatchEvent(new CustomEvent('drivefilters', { detail: this.filters, bubbles: true, composed: true}))
        this.listFiles(null, true);
    }

    viewSuggestedTags(file) {
        this.currentFile = file;
        //empty util file is downloaded
        this.currentSuggestedTags = {};
        this.suggestedTagsSelected = [];
        this.findSuggestedTagsForFile(file, 'en');
        this.show.suggestions = true;
    }

    closeFileModal() {
        this.toggleFileModal(null)
    }

    closeFilePreviewModal() {
        this.toggleFilePreviewModal(null)
    }

    toggleFilePreviewModal(file) {
        this.currentFile = file;
        this.modals.filePreview = !this.modals.filePreview;
        if(!this.modals.filePreview) {
            this.currentFilePreview = null;
            this.currentFile = null
            this.loadingPreview = false
            this.dispatchEvent(new CustomEvent('openfile', { detail: null, bubbles: true, composed: true}))
        }
    }

    @track loadingPreview = false
    @track currentFilePreview = null
    @track showPreviewAsThumbnail = false
    async openPreview (file) {
        // Saber si realmente hace falta
        this.currentFile = await this.providerApi.getItem(this.currentFile );
        this.currentFile = this.normalizeRecord(this.currentFile )

        if (this.currentFile) {
            this.showPreviewAsThumbnail = false
            this.loadingPreview = true
            if(this.modals.fileEdit) {
                this.modals.fileEdit = false
            }
            this.toggleFilePreviewModal(this.currentFile)
            try {
                this.currentFilePreview = await this.storeApi.getFilePreview(this.currentFile)
            } catch(e) {
                this.currentFilePreview = null;
                this.showPreviewAsThumbnail = true
            }
            this.loadingPreview = false
        }
    }

    goToEdit() {
        this.toggleFilePreviewModal(this.currentFile)
        this.toggleFileModal(this.currentFile)
    }

    loadingFile = false
    async toggleFileModal(file) {

        this.loadingFile = true;
        
        this.modals.fileEdit = !this.modals.fileEdit;
        if(!this.modals.fileEdit) {
            this.currentFile = null
            this.modals.tags = false;
            this.waitingForResults = false
            this.dispatchEvent(new CustomEvent('openfile', { detail: null, bubbles: true, composed: true}))
        } else {
            if(file) {
                this.currentFile = await this.providerApi.getItem(file );
                this.currentFile = this.normalizeRecord(this.currentFile )
            }

            this.dispatchEvent(new CustomEvent('openfile', { detail: this.currentFile, bubbles: true, composed: true}))
        }

        this.loadingFile = false;

    }

    /**
     * Opens up tags section in file modal
     */
    toggleTagsModal() {
        this.modals.tags = !this.modals.tags;
        this.waitingForResults = false
        if(this.modals.tags === true) {
            if(this.hasIAEnabled) {
                this.viewSuggestedTags(this.currentFile)
            }
        }
    }

    /**
     * Selects or unselects a given suggested tag.
     * Looks for duplicates to avoid select same tag multiple times
     *
     * @param      {string}  tag     The tag
     */
    toggleSuggestedTagSelection(ev) {
        let tag = this.suggestedTagsFlat.find(t => t.name === ev.target.dataset.name)
        tag.selected = !tag.selected;
        if (tag.selected) {
            this.suggestedTagsSelected.push(tag.name);
        } else {
            this.suggestedTagsSelected = this.suggestedTagsSelected.filter((item) => {
                return item !== ev.target.dataset.name
            })
        }
    }

    /**
     * Downloads given file. Google docs files, should be  downloaded vía "export" function.
     *
     * @param      {GoogleDriveFile}    file       The file
     * @return     {Promise}  promise  A promise that resolves with Blob data of the downloaded file
     */
    exportFile(file) {
        var self = this;
        return new Promise((resolve, reject) => {

            
            var exportRequest = {
                'fileId': file.id,
                'mimeType': file.mimeType
            }
            if (self.isProviderFile(file)) {
                //Export
                // storeApi.client.drive.files.export(exportRequest).then((response) => resolve(response));
            } else {
                //Binary download
                //Opcion 1 : XMLHttpRequest (fail)
                //Opcion 2: google API request (fail)
                //Opcion 2: google drive API request (fail igual que la anterior)

                //Opcion 3: Fetch JS
                let opts = {
                    url: 'https://www.googleapis.com/drive/v3/files/' + file.id + '?alt=media',
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/octect-stream',
                        'Authorization': 'Bearer ' + this.explorerAccessToken
                    },
                    mode: 'cors',
                    cache: 'default'
                }

                this.fetchRequest(opts)
                    .then(function (response) {
                        response.blob().then((b) => {
                            b.name = file.name;
                            b.lastModifiedDate = new Date();
                            resolve(b)
                        });
                    })
            }
        });
    }

    /**
     * Call different Google Cloud Platform APIs based on the type of the file
     * and fills 'currentSuggestedTags' object with all different responses
     * 
     * Emits 'aisuggestions' when some labels are found
     *
     * @param      {Object}  file    The Google Drive file
     * @param      {String}  lang    The language code to use in the API calls
     */
    async findSuggestedTagsForFile(file, lang) {
        if (!this.authData.apiKey) throw new Error('API key not found')

        // if(isFileSupportedByIA(file)) {
            this.waitingForResults = true
            this.currentSuggestedTags = {};
            
            let response = await this.makeGetStorageRequest(file, true, 'json')
            if(response !== 404) {
                this.waitingForResults = false
                this.currentSuggestedTags = this.extractAnnotations(file, response)
                this.$emit('aisuggestions', this.currentSuggestedTags);
            } else {
                //File not exists, upload it
                let blob = await this.exportFile(file)
                await this.uploadToCloudStorage([blob], file)
                await this.waitForIAResults(file)
            }
        // } else {
        //     this.dispatchEvent(new ShowToastEvent({ title: 'File error', message: 'The file selected is not currently supported', variant: 'error', mode: 'dismissable'}));
        // }
    }

    /**
     * Parses the IA results based of file type
     * @param {Object} file The file to be processed
     * @param {Object} results The JSON results obtained from IA process
     */
    extractAnnotations(file, results) {
        let type = this.getMainFileType(file)
        if(type === 'office' || type === 'text' || type === 'pdf' || type === 'audio') {
            return this.extractTextAnnotations(results)
        } else if(type === 'video' ) {
            return this.extractVideoAnnotations(results)
        } else if(type === 'image' ) {
            return this.extractImageAnnotations(results)
        }
        return {}
    }

    extractImageAnnotations(parsed) {
        var labelTags = new Set();
        var landmarkTags = new Set();
        var logoTags = new Set();
        var textTags = new Set();
        var faceTags = new Set();

        if (parsed.labelAnnotations)
            parsed.labelAnnotations.forEach(function (ann) { labelTags.add(ann.description.trim()) });

        if (parsed.logoAnnotations)
            parsed.logoAnnotations.forEach(function (ann) { logoTags.add(ann.description.trim()) });

        if (parsed.textAnnotations)
            parsed.textAnnotations.forEach(function (ann) { textTags.add(ann.description.trim()) });


        if (parsed.faceAnnotations) {
            //Convert 
            parsed.faceAnnotations.forEach(function (ann) {
                var wantedLikelihood = ['POSSIBLE', 'LIKELY', 'VERY_LIKELY'];
                if (wantedLikelihood.indexOf(ann.joyLikelihood) !== -1) faceTags.add('Feliz');
                if (wantedLikelihood.indexOf(ann.sorrowLikelihood) !== -1) faceTags.add('Miedo');
                if (wantedLikelihood.indexOf(ann.angerLikelihood) !== -1) faceTags.add('Enfado');
                if (wantedLikelihood.indexOf(ann.surpriseLikelihood) !== -1) faceTags.add('Sorpresa');
                if (wantedLikelihood.indexOf(ann.underExposedLikelihood) !== -1) faceTags.add('underExposed');
                if (wantedLikelihood.indexOf(ann.blurredLikelihood) !== -1) faceTags.add('blurred');
                if (wantedLikelihood.indexOf(ann.headwearLikelihood) !== -1) faceTags.add('headwear');
            });
        }

        return {
            label: [... labelTags].map(e => { return { name: e, selected: false }}),
            landmark: [... landmarkTags].map(e => { return { name: e, selected: false }}),
            logo: [... logoTags].map(e => { return { name: e, selected: false }}),
            text: [... textTags].map(e => { return { name: e, selected: false }}),
            face: [... faceTags].map(e => { return { name: e, selected: false }})
        }
    }

    extractVideoAnnotations(cached) {
        let labelTags = new Set()
        if (cached && cached.annotation_results) {
            cached.annotation_results.forEach((annList) => {
                //Find segment labels
                if (annList.segment_label_annotations) {
                    annList.segment_label_annotations.forEach((segLabel) => {
                        // labelTags.push({ name: segLabel.entity.description, selected: false })
                        labelTags.add(segLabel.entity.description)
                    })
                }
                //Find shot labels
                if (annList.shot_label_annotations) {
                    annList.shot_label_annotations.forEach((annLabel) => {
                        // labelTags.push({ name: annLabel.entity.description, selected: false })
                        labelTags.add(annLabel.entity.description)
                    })
                }
            })
        }
        let mapped = [... labelTags].map(tag => {return { name: tag, selected: false }})
        return mapped
        
    } 

    extractTextAnnotations(parsed) {
        var salienceThreshold = this.aiNlpSalience;
        let res = {
            entities: [],
            categories: []
        }

        if(Array.isArray(parsed)) {
            parsed.forEach(p => {
                let res2  = this.extractSingle(p, salienceThreshold)
                res.entities = res.entities.concat(res2.entities)
                res.categories = res.categories.concat(res2.categories)
            })
        } else {
            res = this.extractSingle(parsed, salienceThreshold)
        }
        
        return res
        
    } 

    extractSingle(result, salienceThreshold){
        let entities = new Set();
        let categories = new Set();
        let discarded = new Set()

        if(result) {
            //Extract text entitites. Use Salience to pick only desired results
            if (result.entities) {
                result.entities.forEach(function (ann) {
                    if (ann.salience > salienceThreshold)
                        entities.add(ann.name.trim());
                    else
                        discarded.add(ann.name.trim());
                });
            }

            //Fill results until minimum tag number
            if(entities.size < this.aiMinSuggestions) {
                for(let i = entities.size; i < this.aiMinSuggestions; i++)
                    if(discarded[i]) entities.add(discarded[i])
            }
            
            //Go for text categories
            if (result.categories) {
                result.categories.forEach(function (ann) {
                    categories.add(ann.name.trim());
                });
            }
        }
        return  {
            entities: [... entities].map(e => {return {name: e, selected: false }}),
            categories: [... categories].map(e => {return {name: e, selected: false }})
        }
    }

    /**
     * Polls Google Cloud Storage for IA result file each 5 seconds
     * @param {Objet} file The file to check results for
     */
    async waitForIAResults (file) {
        console.log('@@ GoogleDriveExplorer waitForIAResults ' + file.name)
        this.waitingForResults = true
        let results = await this.makeGetStorageRequest(file, true, 'json')
        if(results !== 404) {
            this.waitingForResults = false
            this.currentSuggestedTags = this.extractAnnotations(file , results)
        } else {
            //eslint-disable-next-line
            setTimeout(() => {
                if(this.waitingForResults === true) {
                    this.waitForIAResults(file)
                }
            }, 5000)
        }
    }

    /**
     * Calls Google's Cloud Storage with for a given file
     *
     * @param      {Object}  googleFile    The Google Cloud Storage file descriptor
     * @param      {Boolean}  download    If the file shoudl be downloaded. If false, promise will resolve
     * @param      {String}  resultAs    The type to be casted when recieving response when download is true (blob, json or text). Default blob.
     * @return     {Promise}  promise  A promise that resolves with json content of the file or binary content if 
     */
    async makeGetStorageRequest(googleFile, download, resultAs) {
        var self = this;
        let lang = this.langFinder ? this.langFinder(googleFile) : 'en-US';
        let objectName = googleFile.id + '.' + lang + '.' + googleFile.fullFileExtension + '.iaresult.json'
        // this.searchingInGCS = true
        resultAs = resultAs ? resultAs : 'blob';
        console.log('@@ GoogleDriveExplorer -> makeGetStorageRequest');
        try {

            let altMedia = download ? '&alt=media' : '';
            let response = await fetch("https://www.googleapis.com/storage/v1/b/" + self.aiBucketName + "/o/" + objectName + "?key=" + self.authData.apiKey + altMedia,
                    {
                        method: 'GET',
                        mode: 'cors',
                        cache: 'default',
                        credentials: 'include',
                        headers: {
                            'Authorization': 'Bearer ' + this.explorerAccessToken
                        }
                    })
            
            if (response.status === 404) {
                // this.searchingInGCS = false
                return 404
            } else if(response.status === 200) {
                if (download) {
                    let b
                    switch (resultAs) {
                        case 'blob':
                            b = await response.blob()
                            b.name = objectName;
                            b.lastModifiedDate = new Date();
                            break;
                        case 'json':
                            b = await response.json()
                            break;
                        case 'text':
                            b = await response.text()
                            break;
                        default:
                            b = await response.text()
                            break;
                    }
                    // this.searchingInGCS = false
                    return b

                }
                //When no downloading, we just resolve with content json
                return await response.json()
            }
            this.dispatchEvent(new ShowToastEvent({ title: omAIError, message: omAIErrorConfig + ' ' + response.statusText, variant: 'error', mode: 'dismissable'}));
            throw new Error('Your GCS bucket is not configured properly: ' + response.statusText)
            

        } catch(e) {
            //resolve with null. File not found
            this.dispatchEvent(new ShowToastEvent({ title: omAIError, message: omAIErrorNoFile, variant: 'error', mode: 'dismissable'}));
            return null
            // throw 'Your GCS bucket is not configured properly: ' + response.statusText
        }
        // });
    }

    /**
     * Handles all errors in the component. Should be called on all promise rejections / failures
     *
     * @param      {Object|String|Array}  error   The error message or errors collection 
     */
    handleError(error) {
        //TODO Normalize errors
        console.error(`Drive explorer handled error -> ${error}`);
        let {title, message } = handleError(error)
        this.dispatchEvent(new ShowToastEvent({ title: title, message: message, variant: 'error', mode: 'dismissable'}))
    }

    /**
     * Transforms numbver of byte to human readable text
     *
     * @param      {Number}  file    The number of bytes
     * @param      {Boolean}  si    If International System of metrics should be used
     */
    humanFileSize(bytes, si) {
        let thresh = si ? 1000 : 1024;
        if (Math.abs(bytes) < thresh) {
            return bytes + ' B';
        }
        let units = si
            ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
            : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
        let u = -1;
        do {
            bytes /= thresh;
            ++u;
        } while (Math.abs(bytes) >= thresh && u < units.length - 1);
        return bytes.toFixed(1) + ' ' + units[u];
    }

    /**
     * Gets main type of the file based of its mime-type
     * Used to find IA service for the file
     *
     * @param      {Object}  file    The Google Drive file object
     */
    getMainFileType(file) {
        if (file.mimeType.startsWith('video/')) {
            return 'video';
        } else if (file.mimeType.startsWith('image/')) {
            return 'image';
        } else if (file.mimeType.startsWith('audio/')) {
            return 'audio';
        }else if (
            file.mimeType.startsWith('text/')
            || file.fileExtension === 'csv'
            || file.fileExtension === 'component') {
            return 'text';
        } else if (
            file.mimeType === 'application/pdf') {
            return 'pdf';
        } else if (
            file.mimeType.indexOf('officedocument') !== -1
            || file.mimeType.indexOf('ms-word') !== -1
            || file.mimeType.indexOf('msword') !== -1
            || file.mimeType.indexOf('ms-excel') !== -1
            || file.mimeType.indexOf('msexcel') !== -1) {
            return 'office';
        }

        return 'document';
    }

    /**
     * Finds if a given file is a Google Apps file
     *
     * @param      {Object}  file    The Google Drive file object
     * @return  {Boolean}  True if file is Google file
     */
    isProviderFile(file) {
        return this.storeApi.isProviderFile(file)
    }

    /**
     * Finds if a given file is a Google folder
     *
     * @param      {Object}  file    The Google Drive file object
     * @return  {Boolean}  True if file is Google folder
     */
    isFolder(file) {
        return this.storeApi.isFolder(file);
    }

    /**
     * Signs out from porvider
     */
    signOut() {
        this.dispatchEvent(new CustomEvent('signout', { bubbles: true, composed: true}));
        this.isSignedIn = false
    }

    /**
     * Adds a given "tag" te to specified file description
     *
     * @param      {GoogleDriveFile}  file    The file
     * @param      {string}  tag     The tag
     */
    appendTagToDescription(file, tag) {
        // this.storeApi.addTag(file, tag);
        
        var newText = '#' + tag;
        if (!file._tags) {
            file._tags = newText;
        } else {
            file._tags += ' ' + newText;
        }
    }

    /**
     * Removes a tag from a Google Drive file description.
     *
     * @param      {GoogleDriveFile}  file    The GDrive file
     * @param      {string}  tag     The tag
     */
    removeTagFromDescription(file, tag) {
        //this.storeApi.removeTag(file, tag);
        
        var newText = '#' + tag;
        file._tags = file._tags.replace(newText, '');
    }

    /**
     * Removes a tag. Handler called on vf-tagger's 'remove-tag' event
     *
     * @param      {GoogleDriveFile}  file    The file
     * @param      {String}  tag     The tag
     */
    removeTag(ev) {
        let tag = ev.detail
        this.removeTagFromDescription(this.currentFile, tag);
        // if (doUpdate)
        //     this.updateFile(this.currentFile);
    }

    /**
     * Adds a tag. Handler called on vf-tagger's 'add-tag' event
     *
     * @param      {Event}  ev    The event emmited from tagger component
     */
    addTagToFile(ev) {
        let tag = ev.detail
        this.appendTagToDescription(this.currentFile, tag);
    }

    /**
     * Adds currnet selected suggested tags to the current file selected.
     */
    addSelectedSuggestedTagsToFile() {
        var tags = this.suggestedTagsSelected;
        if (tags.length > 0) {
            tags.forEach((tag) => {
                this.appendTagToDescription(this.currentFile, tag);
            })
        }
        this.suggestedTagsSelected = []
    }
    get hasNotSuggestedTagsSelected() {
        return this.suggestedTagsSelected.length === 0
    }

    onSaveInfoModal() {
        this.updateFile(this.currentFile, false)
    }

    closeMassiveModal() {
        this.massiveData = { appProperties: {} }
        this.modals.massiveEdit = false
    }

    openMassiveModal() {
        this.modals.massiveEdit = true
    }

    openFolderModal () {
        this.modals.folderCreate = true
    }

    closeFolderModal () {
        this.newFolderData = { name: '' }
        this.modals.folderCreate = false
    }
    
    /**
     * Dumps all massive data to all selected files and updates them
     * @param {Event} ev The click event on Save
     */
    onSaveMassiveModal(ev) {
        if(this.filesSelected && this.filesSelected) {
            this.filesSelected.forEach(file => {
                let update = false;
                let f = this.files.find(fl => fl.id === file.item.id)
                console.log(JSON.stringify(f))
                if(this.massiveData.appProperties) {
                    Object.keys(this.massiveData.appProperties).forEach(propKey => {
                        f.appProperties[propKey] = this.massiveData.appProperties[propKey]
                        update = true
                    })
                }

                if(update) {
                    this.updateFile(f, false)
                }
            })
        }

        this.closeMassiveModal()
    }
    /**
     * Updates Gdrive file (only metadata)
     *
     * @param      {GoogleDriveFile}  file    The file
     */
    async updateFile(file, afterUpload) {

        try {

            let indexableText = '';
            if (this.textIndexer) { 
                indexableText = this.textIndexer(file);
            }
            if(this.fileDecorator) {
                file = this.fileDecorator(file)
            }
            let custompropkeys = this.customAppProperties ? this.customAppProperties.map(prop => prop.key) : []
            await this.storeApi.update(file, indexableText, custompropkeys)
    
            if(!afterUpload) {
                // Notify action to everyone
                this.dispatchEvent(new ShowToastEvent({ title: omExplorerSuccess, message: omExplorerUpdateSuccess, variant: 'success', mode: 'dismissable'}));
                publish(this.messageContext, contentChannel, { action: 'update', content: file });
                this.$emit('updatefile', file)
               
                // Refresh file list
                this.listFiles(2, false)
            }
        } catch(e) {
            this.handleError(e);
        }
    }

    /**
     * Updates a list of Google Drive files (only metadata)
     * Emits 'delete-file' when it finishes
     *
     * @param  {Object} file  The Google Drive file to be deleted
     */
    updateFiles(files) {
        return new Promise((resolve, reject) => {
            var numFiles = files.length;
            var processedFiles = 0;

            function sendFile(file) {
                var indexableText;
                if (this.textIndexer) {
                    indexableText = this.textIndexer(file);
                }
                let self = this;
                let request = {
                    'fileId': file.id,
                    'name': file.name,
                    'description': file.description,
                    'appProperties': file.appProperties,
                    'contentHints': {
                        'indexableText': indexableText
                    },
                    'removeParents': file.removeParents,
                    'addParents': file.addParents
                };

                this.storeApi.update(request)
                    .then((response) => {
                        console.log('@@ GoogleDriveExplorer -> UPDATED file ' + processedFiles);
                        processedFiles++;
                        if (numFiles === processedFiles) {
                            resolve(response);
                        } else {

                            sendFile(files[processedFiles]);
                        }
                    })
                    .catch((error) => {
                        self.handleError(error.result.error.errors);
                        // self.handleError(error.result.error.errors); 
                        reject(error)
                    });
            }
            //begin process
            sendFile.call(this, files[0]);

        });
    }

    /**
     * Deletes a file from repository
     * Emits 'delete-file' when it finishes
     *
     * @param  {Object} file  The file to be deleted
     */
    async deleteFile(file) {
        try {
            let result = await this.storeApi.delete(file)
            console.log('Delete result ' + JSON.stringify(result))
            this.dispatchEvent(new ShowToastEvent({ title: omExplorerSuccess, message: omExplorerDeleteSuccess, variant: 'success', mode: 'dismissable'}))
            publish(this.messageContext, contentChannel, { action: 'delete', content: file })
            this.listFiles(null, true)
        } catch(e) {
            this.handleError(e)
            //this.dispatchEvent(new ShowToastEvent({ title: omExplorerError, message: omExplorerDeleteError, variant: 'error', mode: 'dismissable'}));
        }
    }

    normalizeRecord(f) {
        if (this.storeApi.isFolder(f)) {
            f._sldsIcon = 'doctype:folder'
        } else {
            f._sldsIcon = 'doctype:'+ getSLDSIconFromMimeType(f)
        }

        f._thumbnail = f.thumbnails && f.thumbnails[0] 
            ? f.thumbnails[0].large.url
            : f.thumbnailLink;
        
        f._path = '';
        
        if(f.webUrl) {
            let parts = f.webUrl.split('/');
            let from = parts.indexOf('sites');
            f._path = parts.slice(from + 1, parts.length).join('/')
        }
        
        f._downloadUrl = f['@microsoft.graph.downloadUrl'] 
            ? f['@microsoft.graph.downloadUrl']
            : f.webContentLink;

        f._tags = this.storeApi.getTags(f)

        let createdDate = f.fileSystemInfo && f.fileSystemInfo.createdDateTime ? new Date(f.fileSystemInfo.createdDateTime) : new Date(f.createdTime);
        let formattedDate = createdDate ? ' (' + new Intl.DateTimeFormat(LOCALE, { dateStyle: 'short', timeStyle: 'short' }).format(createdDate) + ')' : '';
        f._createdby = f.createdBy 
            ? f.createdBy.user ? f.createdBy.user.displayName : f.createdBy.application.displayName
            : f.lastModifyingUser ? f.lastModifyingUser.displayName : '';
        f._createdby += formattedDate;

        let modTime = f.fileSystemInfo && f.fileSystemInfo.lastModifiedDateTime ? new Date(f.fileSystemInfo.lastModifiedDateTime) : new Date(f.modifiedTime);
        let formattedModDate = modTime ? ' (' + new Intl.DateTimeFormat(LOCALE, { dateStyle: 'short', timeStyle: 'short' }).format(modTime) + ')' : '';
        f._lastmodifiedby = f.lastModifiedBy 
            ? f.lastModifiedBy.user ? f.lastModifiedBy.user.displayName : f.lastModifiedBy.application.displayName
            : f.lastModifyingUser ? f.lastModifyingUser.displayName : '';
        f._lastmodifiedby += formattedModDate;

        return f;
    }

    /**
     * List GDrive files using a query generated by using selected filters.
     * Emits 'files' event when it finishes
     *
     * @param {Number}  isNext  Indicates which page token should be taken (0 is previous, 1 is next, 2 the last one used)
     * @param {Boolean}  doClear  If pagination status should be cleared
     */
    async listFiles(isNext, doClear) {
        this.status.working = true;
        let desiredPageToken = null;
        if(isNext === 0) {
            desiredPageToken = this.paginator.previousToken;
        } else if(isNext === 1) {
            desiredPageToken = this.paginator.nextToken;
        } else if(isNext === 2) {
            desiredPageToken = this.paginator.lastUsedToken;
        }

        if(doClear) {
            this.paginator.currentPage = 1;
            this.paginator.currentListPageTokens = []
        }

        try {

            let { files, nextPageToken } = await this.storeApi.list(desiredPageToken, this.filters, this.customAppProperties, this.paginator)

            // Normalize some fields
            files = files.map(f => {
                return this.normalizeRecord(f);
            })

            this.dispatchEvent(new CustomEvent('files', { detail: files, bubbles: true, composed: true}));
            this.files = files;

            //Set pagination to allow going previous and next
            //this.previousResponse = this.lastResponse;
            //this.lastResponse = response;

            if(nextPageToken) {
                //this.paginator.currentListPageTokens.push(nextPageToken);
                this.paginator.currentListPageTokens[this.paginator.currentPage] = nextPageToken;
                console.log('currentPaginatorPages', this.paginator.currentListPageTokens)
                this.paginator.lastPage = false;
            } else {
                this.paginator.lastPage = true;
            }

            this.paginator.previousToken = this.paginator.currentListPageTokens[this.paginator.currentPage - 2];
            this.paginator.lastUsedToken = this.paginator.currentListPageTokens[this.paginator.currentPage - 1];
            this.paginator.nextToken = this.paginator.currentListPageTokens[this.paginator.currentPage];

            this.status.working = false;
        } catch(err) {
            this.handleError(err)
            //let msg = err.message;
            //this.dispatchEvent(new ShowToastEvent({ title: omExplorerError, message: msg, variant: 'error', mode: 'dismissable'}));
            this.status.working = false;
        }

        return this.files
    }

    /**
     * Copy a list of files from origianl location to new location
     * Emits 'create-folders' event when it finishes (before resolving the pormise)
     *
     * @param  {Object}  folderNames  The  anmes of the new folders
     * @param  {Object}  parents  The parent folders of created ones. This will apply to all of them
     * @return  {Promise}  promise  A promise that resolves with Google Drives's create action response
     */
    createFolders(folderNames, parents) {
        let self = this;
        return new Promise((resolve, reject) => {
            let numFolders = folderNames.length;
            let processedFolders = 0;
            let createdFolders = [];

            function createSingleFolder(folderName) {
                this.storeApi.createFolder({name: folderName}, this.filters)
                    .then((response) => {
                        console.log('@@ GoogleDriveExplorer -> CREATED folder ' + processedFolders)
                        processedFolders++;
                        createdFolders.push(response.result)

                        if (numFolders === processedFolders) {
                            console.log(createdFolders);
                            // self.$emit('create-folders', createdFolders);
                            self.dispatchEvent(new CustomEvent('createfolders', { detail: createdFolders, bubbles: true, composed: true}))
                            resolve(response);
                        } else {
                            createSingleFolder(folderNames[processedFolders]);
                        }
                    })
                    .catch((error) => {
                        self.handleError(error);
                        // self.handleError(error); 
                        reject(error)
                    });
            }
            //begin process
            createSingleFolder.call(this, folderNames[0]);

        });
    }

    async onSaveCreateFolder() {
        await this.createFolder({name: this.newFolderData.name})
        this.closeFolderModal()
    }

    @api
    async createFolder(fileMetadata) {
        try {
            let createResult = await this.storeApi.createFolder(fileMetadata, this.filters)
            this.dispatchEvent(new CustomEvent('createfolder', { detail: createResult, bubbles: true, composed: true}))
            this.dispatchEvent(new ShowToastEvent({ title: omExplorerSuccess, message: omExplorerSuccess, variant: 'success', mode: 'dismissable'}));
            this.listFiles(null, true);
            console.log('Folder created ', createResult.id)
            return createResult;
        } catch(err) {
            this.handleError(err)
        }
    }

    /**
     * Copy a list of files from origianl location to new location
     *
     * @param      {Object}  fileWithOr  The copy description with two keys: "file" with the new file and "originalId" with the file to be copied
     */
    copyFile(fileWithOr) {
        let indexableText;

        if (this.textIndexer) {
            indexableText = this.textIndexer(fileWithOr.file);
        }

        let request = fileWithOr.file;
        request.fileId = fileWithOr.originalId;

        this.storeApi.copy(request)
            .then((response) => { 
                // self.$emit('copy-file', file)
                this.dispatchEvent(new ShowToastEvent({ title: omExplorerSuccess, message: omExplorerCopySuccess, variant: 'success', mode: 'dismissable'}));
                this.dispatchEvent(new CustomEvent('copyfile', { detail: file, bubbles: true, composed: true}))
                publish(this.messageContext, contentChannel, { action: 'copy', content: file })
            })
            .catch((error) => this.handleError(error.result.error.errors));
    }

    /**
     * Copy a list of files from origianl location to new location
     *
     * @param      {Array}  filesWithOriginal  The file list to be copied
     */
    copyFiles(filesWithOriginal) {
        var self = this;
        return new Promise((resolve, reject) => {
            var numFiles = filesWithOriginal.length;
            var processedFiles = 0;
            var copiedFiles = [];

            function copySingleFile(fileWithOr) {
                var indexableText;
                if (this.textIndexer) {
                    indexableText = this.textIndexer(fileWithOr.file);
                }
                let request = fileWithOr.file;
                request.fileId = fileWithOr.originalId;

                this.storeApi.copy(request)
                    .then((response) => {


                        console.log('@@ GoogleDriveExplorer -> Massive copy -> COPIED file ' + processedFiles);
                        processedFiles++;

                        let newFile = request;
                        delete newFile.fileId;
                        newFile.id = response.result.id;
                        newFile.name = response.result.name;
                        newFile.mimeType = response.result.mimeType;
                        copiedFiles.push(newFile);


                        if (numFiles === processedFiles) {
                            // self.$emit('copy-file', newFile);
                            self.dispatchEvent(new CustomEvent('copyfile', { detail: newFile, bubbles: true, composed: true}))
                            publish(this.messageContext, contentChannel, { action: 'copy', content: newFile })
                            resolve(copiedFiles);
                        } else {
                            // self.$emit('copy-file', newFile);
                            self.dispatchEvent(new CustomEvent('copyfile', { detail: newFile, bubbles: true, composed: true}))
                            copySingleFile(filesWithOriginal[processedFiles]);

                        }
                    })
                    .catch((error) => { self.handleError(error.result.error.errors); reject(error) });
            }
            //begin process
            copySingleFile.call(this, filesWithOriginal[0]);

        });
    }

    /**
     * Upload files to GCS to be analyzed by AI APIs
     *
     * @param      {Array}  fileList  The file list to be uploaded
     * @param      {String}  fileid  The id of Google Drive file. Will be the name of the file in GCS to avoid duplicates
     */
    uploadToCloudStorage(fileList, file) {
        return new Promise((resolve, reject) => {

            var file1 = fileList[0];
            let lang = this.langFinder ? this.langFinder(file) : 'en-US';
            this.uploadingToStorage = true;

            let uploaderGCS = new OmResumableUploader({
                url: 'https://www.googleapis.com/upload/storage/v1/b/' + this.aiBucketName + '/o?uploadType=resumable&key=' + this.authData.apiKey + '&name=' + file.id + '.' + lang + '.'+ file.fullFileExtension,
                params: { uploadType: 'resumable' },
                file: file1,
                token : this.explorerAccessToken,
                onComplete: (f) => {
                    console.log('@@ GoogleDriveExplorer -> Storage upload complete ' + f);
                    this.uploadingToStorage = false;
                    this.uploadingToStorageProgress = 0
                    this.dispatchEvent(new ShowToastEvent({ title: 'Upload success **', message: 'File moved to Google Cloud Storage to analyze **', variant: 'success', mode: 'dismissable'}));
                    publish(this.messageContext, contentChannel, { action: 'uploadGCS', content: newFile })
                    resolve(JSON.parse(f))
                },
                onProgress: (p) => { 
                    console.log('@@ GoogleDriveExplorer -> Storage upload progress ' + p) 
                    this.uploadingToStorageProgress = parseInt((p.loaded / p.total) * 100, 10)
                },
                onError: (e) => {
                    console.log('@@ GoogleDriveExplorer -> Storage upload error' + e);
                    this.uploadingToStorage = false;
                    this.uploadingToStorageProgress = 0
                    this.dispatchEvent(new ShowToastEvent({ title: 'Upload failed', message: 'File uploaded to Google Cloud Storage ' + e, variant: 'error', mode: 'dismissable'}));
                    reject(e)
                }
            })

            uploaderGCS.upload();
        });

    }

    uploadViaButton () {
        this.template.querySelector('[data-id="hiddenInputFile"]').click()
    }

    /**
     * Handler executed where a files/s are dropped and loaded in the drop-zone.
     * Opens a new GDrive resumable upload
     *
     * @param      {Array}  fileList  The file list
     */
    onDroppedFile(ev) {
        if(!this.allowUploads) {
            console.log('Uploads not allowed. Please review config')
            return;
        }
        try {
            let fileList = ev.detail ? ev.detail : ev.target.files
            this.upload(fileList)
        } catch(e) {
            this.handleError(e.message)
        }

    }

    @api
    upload(fileList, folderId) {
        // Get file form dropzone or input file
        let folder = folderId ? folderId : this.filters.parentFolder.value;

        Array.from(fileList).forEach(file1 => {
            let decorator = this.fileDecorator ? this.fileDecorator.bind(this) : null;
            let filekey = file1.name + new Date().getTime() + (Math.random() * (10 - 1) + 1)
            this.storeApi.upload(
                file1,
                folder,
                this.onUploadProgress(file1, filekey).bind(this),
                this.onUploadComplete(file1, filekey).bind(this),
                this.onUploadError(file1, filekey).bind(this),
                decorator
            )
    
            //Due to reactivity flaws, to track changes on depp array, we need to reassign the value to the porperty
            let ustatus = JSON.parse(JSON.stringify(this.uploadStatus))
            ustatus.show = true;
            ustatus.percent = 0;
            ustatus.working = true;
            ustatus.files.push({
                name: file1.name,
                percent: 0,
                key: filekey
            })
    
            this.uploadStatus = ustatus

            this.template.querySelector('[data-id="hiddenInputFile"]').value = ''
        })
   
    }

    /**
     * Function called when a file finishes its uploading process
     *
     * @param      {Object}  file    The file
     * @return     {Function}  The error handler
     */
    onUploadComplete(file, key) {
        var self = this;
        return async function (uploadedFile) {
            let upFile = JSON.parse(uploadedFile);

            let upStatus = self.uploadStatus.files.find(f => key === f.key );
            if (upStatus) {
                upStatus.percent = 100;
            }
            let remaining = self.uploadStatus.files ? self.uploadStatus.files.find(sta => {
                return  sta.percent < 100
            }) : null

            

            if(self.fileDecorator) {
                let files = await self.listFiles(null, true)
                let newFile = files.find(f => upFile.id === f.id)
                // Actualizar el fichero
                await self.updateFile(newFile, true);
                /*if(!upFile.appProperties) {
                    upFile.appProperties =  {}
                }
                upFile = self.fileDecorator(upFile)*/
            }

            if(!remaining) {
                self.uploadStatus.show = false;
                self.listFiles(null, true)
            }
  
            self.dispatchEvent(new ShowToastEvent({ title: omExplorerSuccess, message: omExplorerUploadSuccess, variant: 'success', mode: 'dismissable'}));
            self.dispatchEvent(new CustomEvent('uploadfile', { detail: upFile, bubbles: true, composed: true}))
            publish(this.messageContext, contentChannel, { action: 'upload', content: upFile })
            
        }
    }

    /**
     * Function called when a file progresses on upload status
     *
     * @param      {File}  file    The file being uploaded
     * @return     {Function}  The function handling to upload progress
     */
    onUploadProgress(file, key) {
        var self = this;
        return function (progress) {
            var upStatus = self.uploadStatus.files.find(f => f.key === key);
            if (upStatus) {
                upStatus.percent = parseInt((progress.loaded / progress.total) * 100, 10);
            }
        }
    }

    /**
     * Function called when a file had to errors on upload
     *
     * @param      {File}  file    The file
     * @return     {Function}  The function handling to upload progress
     */
    onUploadError(file) {
        return (d) => {
            this.handleError(d);
        }
    }

    @track showFilters = false
    toggleFilters() {
        this.showFilters = !this.showFilters
    }

    get filterButtonVariant() {
        return this.showFilters ? 'brand' : 'border-filled'
    }
    get searchButtonVariant() {
        return this.showSearch ? 'brand' : 'border-filled'
    }
    get filterPanelClass() {
        return this.showFilters ? '' : 'slds-hide'
        //return this.showFilters ? 'filters-show' : 'filters-hide'
    }
    get searchClass() {
        return this.showSearch ? 'slds-grid' : 'slds-grid slds-hide'
        //return this.showSearch ? 'slds-grid search-show' : 'slds-grid search-hide'
    }

    onFilterValueChange(event) {
        if(event.target.type === 'checkbox' || event.target.type === 'toggle') {
            this.filters[event.target.name].value = event.target.checked
        } else if(!event.target.type) {
            this.filters[event.target.name].value = event.detail.value
        } else {
            this.filters[event.target.name].value = event.target.value
        }
    }

    showSearch = false
    toggleSearch () {
        this.showSearch = !this.showSearch
    }

    @track isSearching = false;


    _previousPathItems = null;
    onSearchCommit() {

        if(this.globalSearch) {
            this.filters.searchMode.value = 'global'
        }
   
        if(this.filters.search.value) {
            if(this._previousPathItems === null) {
                this._previousPathItems = JSON.parse(JSON.stringify(this.filters.pathItems));
            }
            this.filters.pathItems = [
                { label: 'Search results', id: 'root' }
            ]
            this.isSearching = true
        } else {
            this.isSearching = false
            this.filters.pathItems = this._previousPathItems;
            this._previousPathItems = null
        }

        this.listFiles(null, true)

    }

    onChangeCurrentFileInput(event) {
        if(event.target.type === 'checkbox') {
            this.currentFile[event.target.name] = event.target.checked
        } else if(!event.target.type) {
            this.currentFile[event.target.name] = event.detail.value
        } else {
            this.currentFile[event.target.name] = event.target.value
        } 
    }

    onChangeCurrentFileAppProperty(event) {
        if(!this.currentFile.appProperties) {
            this.currentFile.appProperties = {}
        }
        this.currentFile.appProperties[event.target.dataset.property] = event.detail;
    }

    onChangeNewFolderProperty(event) {
        this.newFolderData[event.target.dataset.property] = event.target.value
    }

    onChangeMassiveAppProperty(event) {
        this.massiveData.appProperties[event.target.dataset.property] = event.detail
    }

    onChangeFiltersAppProperty(event) {
        this.filters.props[event.target.dataset.property] = event.detail
    }

    //Available scopes in filters panels
    get availableScopes () {
        return [
            { label: 'My files', value: 'own' },
            { label: 'Shared with me', value: 'shared' }
        ]
    }

    //List of available mime types in Google Drive
    get availableMimeTypes () {
        return [
            { value: 'video/', label: 'All video types'},
            { value: 'image/', label: 'All image types'},
            { value: 'text/', label: 'All text types'},
            { value: 'application/vnd.google-apps.folder', label: 'Google Drive Folder'},
            { value: 'application/vnd.google-apps.document', label: 'Google Document'},
            { value: 'application/vnd.google-apps.spreadsheet', label: 'Google Spreadsheet'},
            { value: 'application/vnd.google-apps.drawing', label: 'Google Drawing'},
            { value: 'application/vnd.google-apps.file', label: 'Google File'},
            { value: 'application/vnd.google-apps.audio', label: 'Google Audio'},
            { value: 'application/vnd.google-apps.form', label: 'Google From'},
            { value: 'application/vnd.google-apps.fusiontable', label: 'Google FusionTable'},
            { value: 'application/vnd.google-apps.map', label: 'Google Map'},
            { value: 'application/vnd.google-apps.photo', label: 'Google Photo'},
            { value: 'application/vnd.google-apps.presentation', label: 'Google File'},
            { value: 'application/vnd.google-apps.script', label: 'Google Script'},
            { value: 'application/vnd.google-apps.site', label: 'Google Site'},
            { value: 'application/vnd.google-apps.unknown', label: 'unknown'},
            { value: 'application/vnd.google-apps.video', label: 'Google Video'},
            { value: 'application/vnd.google-apps.drive-sdk', label: 'Google Drive SDK'}
        ]
    }

    //List of availabel mime types to send to IA services
    @track iaAvailabelMimeTypes = {
        'image' : [
            {label: '.jpg', mime : 'image/jpg'},
            {label: '.gif', mime : 'image/gif'},
            {label: '.png', mime : 'image/png'}
        ],
        'audio' : [
            {label: '.mp3', mime: 'audio/mp3'}
        ],
        'video' : [
            {label: '.avi', mime: 'video/x-msvideo'},
            {label: '.mpeg', mime: 'video/mpeg'},
            {label: '.ogv', mime: 'video/ogg'},
            {label: '.mp4', mime: 'video/mp4'},
        ],
        'text' : [
            {label : '.pdf', mime: 'application/pdf' },
            {label : '.html', mime: 'text/html' },
            {label : '.text', mime: 'text/plain' }
        ],
        'office' : [
            {label: '.docx', mime : 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'},
            {label: '.xlsx', mime : 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'},
            {label: '.pptx', mime : 'application/vnd.openxmlformats-officedocument.presentationml.presentation'}
        ],
        'other': [
            {label: '.odt', mime : 'application/vnd.oasis.opendocument.text'},
            {label: '.ods', mime : 'application/vnd.oasis.opendocument.spreadsheet'},
            {label: '.odp', mime : 'application/vnd.oasis.opendocument.presentation'}
        ]
    }

    //Getters on every type of supported file to render in Help modal
    get iaImageTypes () { return this.iaAvailabelMimeTypes.image }
    get iaAudioTypes () { return this.iaAvailabelMimeTypes.audio }
    get iaVideoTypes () { return this.iaAvailabelMimeTypes.video }
    get iaOfficeTypes () { return this.iaAvailabelMimeTypes.office }
    get iaTextTypes () { return this.iaAvailabelMimeTypes.text }
    get iaOtherTypes () { return this.iaAvailabelMimeTypes.other }

    get tagsStr() {
        return JSON.stringify(this.currentSuggestedTags)
    }

    get disableEditionInputs() {
        return !this.allowEdition
    }

    $emit (eventname, payload){
        this.dispatchEvent(new CustomEvent(eventname, { detail: payload, bubbles: true, composed: true}))
    }

    /**
     * Opens "Help" modal
     */
    openHelpModal() {
        this.modals.help = true
    }

    /**
     * Closes "Help" modal
     */
    closeHelpModal() {
        this.modals.help = false
    }

    getListIcon(row) {
        return row.item._sldsIcon
    }

    /**
     * Handles click event on file name. Opens up file modal for editing
     * @param {Object} file The row clicked
     */
    async onFileClick(file) {

        let f = file.detail && file.detail.id ? this.files.find(fi => fi.id === file.detail.id) : file

        // Follow links in URL files
        if(f.name.endsWith('.url')) {
            //window.open(f.webUrl, '_blank')
            fetch(f._downloadUrl)
                .then(r => r.text())
                .then(b => {
                    let url = b.split('URL=').pop().split('\n')[0];
                    if(url) window.open(url, '_blank')
                })

        } else if(this.isFolder(f)) {
            if(this.allowNavigation) {
                this.goToFolder(f.id, f.name)
            } else {
                console.error('You have navigation disabled')
            }
        } else {
            if(this.actionOnClick === 'edit') {
                this.toggleFileModal(f)
            } else {
                this.openPreview(f)
            }
        }
    }

    onFileEdit(file) {
        this.toggleFileModal(file)
    }

    /**
     * Handles click event on file delete action. Opens up file modal for confirming deletion
     * @param {Event} ev The click event on file name
     */
    onFileDelete(file) {
        //let file = this.files.find(f => f.id === ev.detail.id)
        this.currentFile = file
        this.modals.delete = true
    }

    closeDeletionModal() {
        this.modals.delete = false
        this.currentFile = null
    }

    saveDeletionModal() {
        if(this.currentFile) {
            this.deleteFile(this.currentFile)
        }
        this.modals.delete = false

    }

    connectedCallback() {        
        //Append listener to postMessage, to get the auth
        window.addEventListener('message', this.onCallbackMessage.bind(this))
    }

    disconnectedCallback() {
        //remove listener when disconnecting 
        window.removeEventListener('message', this.onCallbackMessage.bind(this))
    }

    initExplorer () {
        //Check if already have userdata and provider configuration 
        if(this.uAccess && this.storeApi) {
            // let udata = JSON.parse(data)
            this.explorerAccessToken = this.uAccess.access_token
            this.isSignedIn = true
            this.listFiles(null, false)
        }
    }

    onCallbackMessage(msg) {
      if (msg.data && typeof msg.data.indexOf === 'function') {
        this.dispatchEvent(new CustomEvent('usercredentials')) 
      }
    }

    /**
     * Signs-out from Google
     */
    signIn() {
      window.open(this.storeApi.getAuthUrl(), 'callbackpage', 'width="300px",height="500px"')
    }
    

}