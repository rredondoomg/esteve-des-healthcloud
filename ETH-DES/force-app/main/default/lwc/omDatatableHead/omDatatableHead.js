import { LightningElement, api } from 'lwc';

import { getCellAttributes } from 'c/omDatatableUtils'

import omExact from '@salesforce/label/c.omExact'
import omHide from '@salesforce/label/c.omHide'
import omWrap from '@salesforce/label/c.omWrap'
import omStack from '@salesforce/label/c.omStack'
import omOnlyDates from '@salesforce/label/c.omOnlyDates'

export default class OmDatatableHead extends LightningElement {

    labels = {
        omHide,
        omOnlyDates,
        omWrap,
        omExact,
        omStack
    }

    /**
     * If the datatable is being exported
     * @type boolean
     * @default false
    */
    @api exporting = false;

    /**
     * If the cell will be resisable
     * @type boolean
     * @default false
     */
    @api resizable = false;

    @api
    get column() {
        return this._column;
    }

    set column(val) {
        this.cellAttributes = getCellAttributes(val, null)
        this._column = val;
        this.hasHeaderIcon = this.cellAttributes.headerIconName !== ''
    }

    @api colIndex

    /**
     * The table order
     * @type object
     * @default undefined
     */
    @api order;

    /**
     * The value is beign editing from in-cell button
     * @type string
     * @default ''
     */
    @api hideColumnActions = false

    get isSortable () {
        return this.column && this.column.sortable === true;
    }

    get isSorted () {
        return this.order && this.order.key === this.column.key
    }

    get isDateType() {
        return this.column.type === 'date' || this.column.type === 'datetime';
    }

    get actionsHidden () {
        return this.hideColumnActions || this.column.hideActions
    }

    get cellGridClass(){
        let gridClass = this.isSorted ? `om-datatable-head slds-is-sorted slds-is-sorted_${this.order.direction}` : 'om-datatable-head'


        if(this.cellAttributes && this.cellAttributes.alignment){
            const {alignment} = this.cellAttributes;
            switch(alignment){
            case 'left':
                gridClass += ' slds-grid_align-spread';
                break;

            case 'center':
                gridClass += ' slds-grid_align-center';
                break;

            case 'right':
                gridClass += ' slds-grid_align-end';
                break;

            default:
                break;
            }
        }

        return gridClass;
    }

    hasHeaderIcon = false

    cellAttributes = {};

    _isResizing = false;

    _resizingElem = null;

    _resizingElmRect = null;

    connectedCallback() {
        // Do this to allow removal of listener
        this._handleResize = this.handleResize.bind(this)
        this._stopResize = this.stopResize.bind(this)
    }

    onHeaderClick() {
        this.dispatchEvent(new CustomEvent('sort', { detail: { column: this.column }, composed: true, bubbles: true }))
    }

    onActionsClick (event) {
        event.stopPropagation()
        event.stopImmediatePropagation()
    }

    handleActionSelect (event) {
        const selectedItemValue = event.detail.value;
        this.dispatchEvent(
            new CustomEvent(
                'columnaction',
                {
                    detail: {
                        action: selectedItemValue,
                        column: this.column
                    },
                    bubbles: true,
                    composed: true }
            )
        );
    }

    @api getContentsWidth() {
        let width = 0;
        this.template.querySelectorAll('.width-calculation-element>*').forEach(elem => {
            width += elem.scrollWidth;
        });
        return width;
    }

    beginResize(ev) {
        this._isResizing = true;
        this._resizingElem = ev.currentTarget;
        this._resizingElmRect = this._resizingElem.getBoundingClientRect()
        document.addEventListener('mousemove', this._handleResize);
        document.addEventListener('mouseup', this._stopResize);
    }

    stopResize(ev) {
        this._isResizing = false;
        this.dispatchEvent(new CustomEvent('beginresize', { detail: { column: this.column, clientX: ev.clientX }, composed: true, bubbles: true }))

        // Restore elemnts
        this._resizingElem.style.left = null;
        this._resizingElem = null;
        this._resizingElmRect = null;

        document.removeEventListener('mousemove', this._handleResize);
        document.removeEventListener('mouseup', this._stopResize);
    }

    handleResize(ev) {
        if(this._isResizing) {
            this._resizingElem.style.left = `${ev.clientX - Math.round(this._resizingElmRect.left)  }px`
        }
    }

}