/**
 * Custom ListView component to display Salesforce records
 * @module OmListView
 * @version 1.1.1
 * @requires OmDatatable
 */
import { api, track, wire, LightningElement } from 'lwc';
import { default as actionsTemplate } from './templates/omListViewActions.html';
import getListViewColumns from '@salesforce/apex/OM_LWC_pack.getListViewColumns';
import getRecords from '@salesforce/apex/OM_LWC_pack.getRecords';
import { subscribe, unsubscribe } from 'lightning/empApi';
import { NavigationMixin } from 'lightning/navigation';
import omEdit from '@salesforce/label/c.omEdit';
import omView from '@salesforce/label/c.omView';
import omDelete from '@salesforce/label/c.omDelete';
import omLog from 'c/omLog';



export default class OmListView extends NavigationMixin(LightningElement){

    /**
     * Salesforce object api name
     * @type String
     * @required
     */
    @api object;

    /**
     * Salesforce field API name for selected sObject type. This fields will be displayed as columns
     * @type String
     */
    @api fields;

    /**
     * Salesforce field API name for selected sObject type. This fields will exists in the underlying data, but will not redered
     * @type String
     */
    @api hiddenFields;

    /**
     * Salesforce SOQL filters to apply to WHERE clause when querying records
     * @type String
     * @default ''
     */
    @api filters = '';

    /**
     * Record Id from Lightning Exprience record page
     * @type String 
     */
    @api recordId;

    /**
     * Pre-queried list of records. Should contain at least 'Id' field
     * If present, a query filter will be generate
     * @type sObject[]
     * @default undefined
     */
    @api records;

    /**
     * When using omListView as relatedList, the field used to retrived the relationship
     * @type String
     */
    @api relationField;

    /**
     * If the Id field should be displayed as column
     * @type Boolean
     * @default false
     */
    @api showId;

    /**
     * If the label of the related lookup record (if any) should be displayed
     * @type Boolean
     * @default false
     */
    @api showRelationshipObjectLabel = false;

    /**
     * Function to calculate the CSS class to append to record row. Receives a datatable "row" as parameter
     * @type Function
     * @default undefined
     */
    @api rowClass;

    /**
     * Function to calculate the CSS inline styles to append to record row. Receives a datatable "row" as parameter
     * @type Function
     * @default undefined
     */
    @api rowStyle;

    /**
     * Function to calculate the the data- attribute to append to each row. Receives a datatable "row" as parameter
     * @type Function
     * @default undefined
     */
    @api rowValue;

    /**
     * Column keys to be clickable
     * @type Column[]
     * @default []
     * @todo David
     */
    @api clickableColumns;

    /**
     * Pre-selected items
     * @type object[]
     * @default []
     */
    @api selectedRecords;
    
    /**
     * If the table should have a button to display detail columns
     * @type boolean
     * @default false
     */
    @api detailable = false

    /**
     * If the table columns should be resizable
     * @type boolean
     * @default false
     */
     @api resizable = false

    /**
     * The HTML template to apply in the details area
     * @type Template
     */
    @api detailsTemplate = undefined

    /**
     * If the table should allow inline editing for rows
     * @type boolean
     * @default false
     */
    @api editable = false

    /**
     * If the table should allow to search by columns
     * @type boolean
     * @default false
     */
    @api filterable = false


    /**
     * If the selection would allow multiple rows. Only applies if 'selectable' is true
     * @type boolean
     * @default false
     */
    @api multiselectable;

    /**
     * If the table should allow selection of records
     * @type boolean
     * @default false
     */
    @api selectable;

    /**
     * If the search input should be displayed
     * @type boolean
     * @default false
     */
    @api showSearchInput;

    /**
     * If the search should only apply to data appearing in columns instead of all fields
     * @type boolean
     * @default false
     */
    @api searchOnlyInColumns = false

    /**
     * If the search should ignore diacritics when searching (high performance impact)
     * @type boolean
     * @default false
     */
    @api fuzzySearch = false

    /**
     * The unique field present in all 'rows' passed to the table. 
     * This field is important as it will be neccesary to speed up thing a find row status quickier
     * @type boolean
     * @default false
     */
    @api uniqueField = 'Id';

    /**
     * Tooltip information
     * @type string[]
     * @default null
     * @todo David
     */
    @api tooltipInfo;


    /**
     * If full paginator should be hidden
     * @type boolean
     * @default false
     */
    @api hidePaginator;

    /**
     * If paginator "Show {} of {}" text should be hidden
     * @type boolean
     * @default false
     */
    @api hidePaginatorShow;

    /**
     * If paginator central buttons should be hidden
     * @type boolean
     * @default false
     */
    @api hideNumberButtons;

    /**
     * If paginator "Next" and "Previous" buttons should be hidden
     * @type boolean
     * @default false
     */
    @api hideControlButtons;

    /**
     * If paginator rows per page selector should be hidden
     * @type boolean
     * @default false
     */
    @api hideRowsNumberSelector;

    /**
     * Action on clicked cell (view, edit, list)
     * @type String
     * @default 'view'
     */
    @api actionName = 'view'

    /**
     * Indicates if record are going to be opened on clicked cell
     * @type Boolean
     * @default false
     */
    @api openRecord
    
    _recordViewable
    @api 
    get recordViewable() {
        return this._recordViewable
    }

    set recordViewable(val) {
        this._recordViewable = val
        this.generateRowActions()
    }

    _recordDeleteable
    @api 
    get recordDeleteable() {
        return this._recordDeleteable
    }

    set recordDeleteable(val) {
        this._recordDeleteable = val
        this.generateRowActions()
    }

    _recordEditable
    @api 
    get recordEditable() {
        return this._recordEditable
    }

    set recordEditable(val) {
        this._recordEditable = val
        this.generateRowActions()
    }

    _apiRowActions
    @api 
    get rowActions() {
        return this._rowActions
    }

    set rowActions(val) {
        this._apiRowActions = val
        if(val === undefined) {
            this._rowActions = function() { return [] }
        } else {
            this.generateRowActions()
        }
    }

    /** Change Data Capture */
    /**
     * Indicates if list view has to subscribe to CDC
     * @type Boolean
     * @default false
     */
    @api listenChanges = false
    subscriptionCDC = {}

    get channelName() {
        return this.object.includes('__c') ? '/data/' + this.object + '__ChangeEvent' : '/data/' + this.object + 'ChangeEvent'
    }

    /**
     * Datetime format to show (in moment.js notation). Example: 'DD MM YYYY hh:mm:ss'
     * @type string
     * @default ''
     */
    @api datetimeFormat = '';

    /**
     * List of labels to show, separated by commas. Wildcard '#' to set the field label
     * @type string
     */
    @api labelList = '';

    /**
     * List of fields that must be shown as HTML, separated by commas
     * @type string
     */
    @api htmlFields = '';

     /**
     * Search term to apply.
     * @type string
     * @default ''
     */
    @api searchTerm;

    /**
     * Multi-line text truncation in the table cell. Max 7 lines of text
     * @type boolean
     * @default false
     */
    @api truncate = false;

    /**
     * Name of Platform Evento to subscribe
     * @type string
     * @default null
     */
    @api platformEvent;
    subscriptionPE = {}

    /**
     * Platform Event recordId API Name of field to filter events fired
     * @type string
     * @default null
     */
    @api idFieldName;
    
    _polling = false
    
    /**
     * If the component should start polling to display changes in records
     * @type boolean
     * @default false
     */
    @api 
    get polling() {
        return this._polling
    }

    set polling(val) {
        this._polling = val
        if(!val && this._pollingTimeout) {
            clearTimeout(this._pollingTimeout)
        }
        if(val) {
            this.doPolling()
        }
    }

    /**
     * Interval in seconds of polling
     * @type string/integer
     * @default '30'
     */
    @api pollingInterval = '30'

    @track loading = true;

    connectedCallback() {
        if(this.listenChanges || this.platformEvent) this.handleSubscribe()
    }

    disconnectedCallback() {
         this.handleUnsubscribe()
    }

    doPolling() {
        this._pollingTimeout = setTimeout(() => {
            this.refreshData()
            this.doPolling()
        }, (parseInt(this.pollingInterval, 10) * 1000));
    }
    

    handleSubscribe() {
        if(this.listenChanges){
            const messageCallbackCDC = () => {
                this.refreshData()
            };

            subscribe(this.channelName, -1, messageCallbackCDC).then(response => {
                this.subscriptionCDC = response
            });
        }

        if(this.platformEvent){
            const messageCallbackPE = response => {
                if (this.idFieldName){
                    if (response && response.data && response.data.payload && response.data.payload[this.idFieldName] && response.data.payload[this.idFieldName] === this.recordId){
                        this.refreshData()
                    }
                }else{
                    this.refreshData()
                }
            };

            subscribe('/event/' + this.platformEvent, -1, messageCallbackPE).then(response => {
                this.subscriptionPE = response
            });
        }
    }

    handleUnsubscribe() {
        unsubscribe(this.subscriptionCDC, () => {});
        unsubscribe(this.subscriptionPE, () => {});
    }

    /**
     * Labels for actions
     */
    labels = {
        omEdit,
        omView,
        omDelete
    }

    generateRowActions() {
        let actions = []
        if(this.recordViewable) actions.push({  label: this.labels.omView, name: 'recordview', iconName: 'utility:preview'})
        if(this.recordEditable) actions.push({  label: this.labels.omEdit, name: 'recordedit', iconName: 'utility:edit'})
        if(this.recordDeleteable) actions.push({  label: this.labels.omDelete, name: 'recorddelete', iconName: 'utility:delete'})

        this._rowActions = (row) => {
            let acts = actions;
            if(this._apiRowActions && typeof this._apiRowActions === 'function') {
                acts = [...acts, ...this._apiRowActions(row) ]
            }
            return acts
        }
    }

    handleRowActions(ev) {
        this.dispatchEvent(new CustomEvent(ev.detail.action, { detail: { Id: ev.detail.row.Id }, composed: true, bubbles: true}))
    }

    _rowActions

    @api refreshData(){
        this.getRecordsJS(this.object, this.fields, this.getFullFilters());
    }

    _objectColumns = []

    get objectColumns() {
        if(this.recordViewable || this.recordEditable || this.recordDeleteable || this._apiRowActions !== undefined) {
            //return [ { fieldName: '_actions_', label: '', component: { template : actionsTemplate } } , ...this._objectColumns ]
            return [ 
                { 
                    fieldName: '_actions_',
                    label: '',
                    type: 'actions',
                    fixedWidth: 50,
                    typeAttributes: {
                        iconName: 'utility:chevrondown'
                    }
                } , 
                ...this._objectColumns 
            ]
        } 
        return this._objectColumns;
    }

    @track _records;

    getRecordIdFilter() {
        return this.recordId && this.relationField ? this.relationField + ' = \'' + this.recordId + '\'' : '';
    }

    /**
     * Generates WHERE clause based on all possible filters existing
     * @private
     */
    getFullFilters() {
        let recIdFilter = this.getRecordIdFilter()

        let localFilters = this.filters && this.recordId 
            ? this.filters.replace('#recordId', "'" + this.recordId + "'") 
            : this.filters ? this.filters : '';
        
        let recordListFilter
        if(this.records && this.records.length > 0) {
            recordListFilter = this.records.map(r => r.Id).join(`','`)
            recordListFilter = ` Id IN ('${recordListFilter}')`
        }

        // Compose final clause
        let clause = ''
        if(recIdFilter) {
            clause += recIdFilter
        }

        if(localFilters) {
            clause += (clause === '' ? localFilters : ` AND  ${localFilters}` )
        }

        if(recordListFilter) {
            clause += (clause === '' ? recordListFilter : ` AND  ${recordListFilter}` )
        }

        omLog.info('Clause generated ' + clause)
        console.log('Clause generated ' + clause)

        return clause
        // AccountId in (SELECT ParentId FROM Account WHERE Id=:recordId) OR AccountId = :recordId
    }

    // Flow output
    @api 
    get selection() {
      return this._selection
    }
    
    onrowselection (evt) {
        this._selection = evt.detail.map(row => {
          let it = Object.assign({}, row.item)
          if(it.key) delete it.key
          return it
        })
    }

    @wire(getListViewColumns, { objectApiName: '$object', fields: '$fields', filters: '$filters', showRelationshipObjectLabel: '$showRelationshipObjectLabel', labelList: '$labelList', htmlFields: '$htmlFields'})
    setColumns({error, data}){
        if(data){
            this._objectColumns = [];
            data.forEach(column => {
                let auxColumn = JSON.parse(JSON.stringify(column))
                if((column.fieldName === 'Id' && this.showId) || column.fieldName !== 'Id'){
                    if(this.clickableColumns && this.clickableColumns.includes(column.fieldName)) {
                        auxColumn.isClickable = true
                    }
                    this._objectColumns.push(auxColumn);
                }
            });
            let fieldsToQuery = this.hiddenFields ? this.fields +','+this.hiddenFields :  this.fields
            this.getRecordsJS(this.object, fieldsToQuery, this.getFullFilters());
            this.loading = false;
        }else if(error) {
            console.error(JSON.stringify(error));
        }
    }

    getRecordsJS(object, fields, filters) {
        getRecords({
            objectApiName: object,
            fields: fields,
            filters: filters
        })
        .then( records => {
            let recordsAux = [];
            records.forEach(el => {
                let aux = Object.assign({},el);
                if(Array.isArray(this.selectedRecords)){
                    if(this.selectedRecords.find( (selected) => { 
                        if(typeof selected === 'object'){
                            return el[this.uniqueField] === selected[this.uniqueField] ||
                            selected.item ? el[this.uniqueField] === selected.item[this.uniqueField] : false;
                        }
                        return el[this.uniqueField] === selected;
                    })){
                        aux.checked = true;
                    }
                }else{
                    if(el[this.uniqueField] === this.selectedRecords){
                        aux.checked = true;
                    }
                }
                recordsAux.push(aux);
            });
            this._records = JSON.parse(JSON.stringify(recordsAux));
            
        })
        .catch( error => {
            console.log('Error: ' + JSON.stringify(error));
        });
    }

    navigateToRecordViewPage(evt) {
        if(this.openRecord && evt && evt.detail && evt.detail.item){
            this[NavigationMixin.Navigate]({
                type: 'standard__recordPage',
                attributes: {
                    recordId: evt.detail.item.Id,
                    actionName: this.actionName
                }
            });
        }
    }

}