/**
 * Dynamic input field for Salesforce object fields
 * @module OmInput
 * @version 1.1.0
 */
import { LightningElement, api, wire, track } from 'lwc';
import { getObjectInfo, getPicklistValues  } from 'lightning/uiObjectInfoApi';

export default class OmInput extends LightningElement {
  /**
   * The record from where the data is going to be retrieved
   * @type object
   * @default undefined
   */
  @api record

  /**
   * The attribute on the "record" used to retrive the info.
   * If empty, value will be tried to be obtained from a porperty called as the SAlesforce field name
   * @type string
   * @default undefined
   */
  @api property

  /**
   * The Salesforce object where the field lives 
   * @type string
   * @default undefined
   */
  @api sobject

  /**
   * The Salesforce object field to retrieve schema
   * @type string
   * @default undefined
   */
  @api field

  /**
   * The label for the field. If empty, defaults to Salesforce object field label
   * @type string
   * @default undefined
   */
  @api label

  /**
   * If the input should not be editable by the user
   * @type Boolean
   * @default false
   */
  @api disabled = false

  fieldMeta

  @track error
  @track picklistValues = []

  isPicklist = false
  isTextArea = false
  isRichText = false
  isLookup = false
  referenceTo = null
  isInput = false
  // properties for lightning-input
  @track input = {
    type: 'text'
  }

  masterRecordTypeId = '012000000000000AAA'

  @wire(getObjectInfo, { objectApiName: '$sobject' })
  objectInfo({data, error}) {
    if (data) {
        if(this.field) {
          this.fieldMeta = data.fields[this.field]
          if(this.fieldMeta) {
            console.log('omInput - fieldMeta datatype ' + this.fieldMeta.dataType)
            if(this.fieldMeta.dataType === 'Picklist') {
              this.isPicklist = true
            } else if(this.fieldMeta.dataType === 'Reference') {
              this.isLookup = true
              this.referenceTo = this.fieldMeta.referenceToInfos[0].apiName
            } else if(this.fieldMeta.dataType === 'TextArea') {
              if(this.fieldMeta.extraTypeInfo === 'PlainTextArea') {
                this.isTextArea = true
              } else if (this.fieldMeta.extraTypeInfo === 'RichTextArea') {
                this.isRichText = true
              }
            } else {
              // lightning-input type compatible
              this.isInput = true
              this.setLightningInputType()
            }
            this.error = undefined;
          } else {
            this.error = 'Field ' + this.field + ' doest not exists or current user has not permission to access it'
          }
        } else {
          this.error = 'Field attribute not introduced'
        }
        console.log('omInput - wire objectInfo error (with data) ' + this.error)

    } else if (error) {
        this.error = error;
        console.log('omInput - wire objectInfo error ' + this.error)
    }

  }

  @wire(getPicklistValues, {recordTypeId: '012000000000000AAA', fieldApiName: '$fullAPIName'})
  getPicklistValues({data, error}) {
    if (data) {
        this.picklistValues = data.values
        this.error = undefined;
    } else if (error) {
        this.error = error;
    }
  }

  get displayedLabel() {
    return this.label ? this.label : this.fieldMeta.label
  }

  get fullAPIName() {
    return this.sobject+'.'+this.field
  }

  get value() {
    return this.record ? (this.property ? this.record[this.property] : this.record[this.field]) : ''
  }

  setLightningInputType() {
    if(!this.fieldMeta) {
      this.input.type = 'text'
    } else {
      switch(this.fieldMeta.dataType) {
        case 'String':
          this.input.type = 'text'
          break
        case 'Double':
          this.input.type = 'number'
          break
        case 'Date':
          this.input.type = 'date'
          break
        case 'DateTime':
          this.input.type = 'datetime'
          break
        default:
          this.input.type = 'text'
          break
      }
    }
  }
  onChangeCurrentLookup (ev) {
    console.log('lookup ev', ev)
    let value = ev.detail[0] ? ev.detail[0].Id : null
    ev.stopImmediatePropagation()
    ev.stopPropagation()
    /**
     * Event fired when the value of the input changes
     * @event change
     * @type Array|Object|String|Number
     */
    this.dispatchEvent(new CustomEvent('change', { detail: value, bubbles: true, composed: true}))
  }

  onChangeCurrentFileInput (ev) {
    ev.stopImmediatePropagation()
    ev.stopPropagation()
    /**
     * Event fired when the value of the input changes
     * @event change
     * @type Array|Object|String|Number
     */
    this.dispatchEvent(new CustomEvent('change', { detail: ev.target.value, bubbles: true, composed: true}))
  }
}