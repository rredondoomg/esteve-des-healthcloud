/**
 * HTML5 audio implmentation with SLDS styling for controls
 * @module OmAudio
 * @version 1.0.0
 */
import { LightningElement, api } from 'lwc';

export default class OmAudio extends LightningElement {

  /**
   * URl source for the audio (must be served over HTTPS)
   * @type String
   * @default undefined
   */
  @api 
  get src() {
    return this._src
  }

  set src(val) {
    this._src = val
    this.ready = false
  }

  get isPaused() {
    return this.audioElm && this.audioElm.paused
  }

  get isPlaying() {
    return !this.isPaused
  }

  get muted() {
    return this.volume < 1
  }

  get highVolume() {
    return this.volume > 6 && this.volume <= 10
  }

  get lowVolume() {
    return this.volume >= 1 && this.volume <= 6
  }
  
  get durationStr() {
    return new Date(this.duration * 1000).toISOString().substr(11, 8)
  }

  get currentDurationStr() {
    return new Date(this.progress * 1000).toISOString().substr(11, 8)
  }

  _src
  audioElm
  listenersReady = false
  duration = 0
  progress = 0
  volume = 10
  ready = false

  renderedCallback() {
    console.log('omAudio renderedCallback')
    if(!this.listenersReady) {
      this.audioElm = this.template.querySelector('[data-id="audioElm"]')
      this.audioElm.addEventListener('timeupdate', this.onAudioTimeUpdate.bind(this))
      this.audioElm.addEventListener('canplay', this.onAudioLoad.bind(this))
      this.listenersReady = true
    }
  }

  onAudioLoad() {
    console.log('omAudio onAudioLoad')
    this.ready = true
    this.volume = this.audioElm.volume * 10
    this.duration = this.audioElm.duration
  }

  onAudioTimeUpdate() {
    console.log('omAudio onAudioTimeUpdate')
    this.progress = this.audioElm.currentTime
  }

  /**
   * Plays the audio
   */
  @api play() {
    console.log('omAudio play')
    if(this.audioElm.paused) {
      this.audioElm.play()
    }
  }

  /**
   * Pauses the audio
   */
  @api pause() {
    console.log('omAudio pause')
    this.audioElm.pause()
  }

  /**
   * Stops the audio
   */
  @api stop() {
    console.log('omAudio stop')
    this.audioElm.pause()
    this.audioElm.currentTime = 0
  }

  handleVolumeChange(event) {
    console.log('omAudio handleVolumeChange')
    this.volume = event.target.value
    this.audioElm.volume = event.target.value / 10
  }

  handleProgressChange(event) {
    console.log('omAudio handleProgressChange')
    this.audioElm.currentTime = event.target.value
  }
}