/**
 * Component to display a SLDS modal
 * @module OmModal
 * @version 1.1.1
 */
import { LightningElement, api } from 'lwc';
import save from '@salesforce/label/c.omSave';
import cancel from '@salesforce/label/c.omCancel';


export default class OmModal extends LightningElement {

    /** ******************** API ********************* */


     /**
      * Label to show in the header with a bold style
      * @type string
      * @default ''
      */
     @api headerLabel = '';

     /**
      * Label for cancel button
      * @type string
      * @default omCancel SF Label
      */
     @api cancelLabel = cancel;

     /**
      * Label for save button
      * @type string
      * @default omSave SF Label
      */
     @api saveLabel = save;

     /**
      * Modal size. Applies SLDS based on this attribute
      * Values are 'small', 'medium', 'large'
      * @type String
      * @default 'small'
      */
     @api size = 'small'

     /**
      * If Header modal should be hidden
      * @type boolean
      * @default false
      */
      @api hideHeader;

     /**
      * If footer modal should be hidden
      * @type boolean
      * @default false
      */
     @api hideFooter;

     /**
      * If footer modal buttons should be hidden
      * @type boolean
      * @default false
      */
     @api hideFooterButtons;

     /**
      * If cancel button should be hidden
      * @type boolean
      * @default false
      */
     @api hideCancelButton;

     /**
      * If save button should be hidden
      * @type boolean
      * @default false
      */
     @api hideSaveButton;

     /**
      * If close button should be hidden
      * @type boolean
      * @default false
      */
     @api hideCloseButton;

     /**
      * If cancel button should be disabled
      * @type Boolean
      * @default false
      */
     @api disabledCancelButton

     /**
      * If save button should be disabled
      * @type Boolean
      * @default false
      */
     @api disabledSaveButton

     /**
      * If save button should be disabled
      * @type Boolean
      * @default false
      */
     @api disableCloseOutside = false

     /**
      * Custom CSS style to apply to the modal container
      * @type String
      * @default ''
      */
     @api containerStyle = ''

     /**
      * Custom CSS style to apply to the modal body
      * @type String
      * @default ''
      */
     @api bodyStyle = ''


     get modalClass() {
         return "slds-modal slds-fade-in-open slds-modal_" + this.size;
     }

     closeIt() {
         /**
          * Event fired when the modal wants to close (when clicking on close icon or clicking outside )
          * @event close
          * @type string
          */
         this.dispatchEvent(new CustomEvent('close', { detail: 'close' , composed: true, bubbles: false } ));
     }

     /**
      * Closes the modal qhen clicking outside its content (if enabled)
      * @param {Object} evt The click event
      * @private
      */
     closeModalOutside(evt) {
         if(!this.disableCloseOutside && evt && evt.target && evt.target.dataset && evt.target.dataset && evt.target.dataset.where){
             this.closeIt();
         }
     }

     cancelIt() {
         /**
          * Event fired when clicking on "Cancel" button (if present)
          * @event cancel
          * @type string
          */
         this.dispatchEvent(new CustomEvent('cancel', { detail: 'cancel' , composed: true, bubbles: false } ));
     }

     saveIt() {
         /**
          * Event fired when clicking on "Save" button (if present)
          * @event save
          * @type string
          */
         this.dispatchEvent(new CustomEvent('save', { detail: 'save' , composed: true, bubbles: false } ));
     }

}