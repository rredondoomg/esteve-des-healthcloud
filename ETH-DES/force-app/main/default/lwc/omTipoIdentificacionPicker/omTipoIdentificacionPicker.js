import { LightningElement, api, track, wire } from 'lwc'
import { getRecord } from 'lightning/uiRecordApi'

export default class OmTipoIdentificacionPicker extends LightningElement {
    
    /**
     * Client Id selected in the previous screen
     */
    @api clientId = ''

    @api previousSelectedTipoIdentificacion = ''

    /**
     * Flow output
     */
    @api 
    get selectedTipoIdentificacion() {
      return this._selection
    }

    @api
    validate() {
        if(this._selection != null && this._selection != '') { 
            return { isValid: true }; 
        } 
        else { 
            return { 
                isValid: false, 
                errorMessage: 'Debe rellenar el tipo de identificación.' 
            }; 
        }
    }

    @track options = []

    @wire(getRecord, { recordId: '$clientId', fields: ['Cliente__c.IdentificacionPac__c'] })
    wiredRecord({ error, data }) {
        if (error) {
            console.error(JSON.stringify(error))
        } else if (data) {
            this.options = data.fields.IdentificacionPac__c.value?.split(';').map( val => ({ label : val, value : val}));
        }
    }

    connectedCallback(){
        this._selection = this.previousSelectedTipoIdentificacion
   }   

    setTipoIdentificacion (evt) {
        this._selection = evt.detail.value
        this.previousSelectedTipoIdentificacion = evt.detail.value
    }
}