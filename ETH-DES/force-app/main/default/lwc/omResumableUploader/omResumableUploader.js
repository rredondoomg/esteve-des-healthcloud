/**
 * Service module to handle resumable upload to external serv ices that support it such as Google Drive
 * @module OmResumableUploader
 * @version 1.1.0
 */

/**
 * Helper class to handle upload retries
 * @class
 */
class RetryHandler {
  /**
   * Constructs the handler
   */
  constructor() {
    this.interval = 1000; // Start at one second
    this.maxInterval = 60 * 1000; // Don't wait longer than a minute
  }

  /**
   * Invoke the function after waiting
   *
   * @param {function} fn Function to invoke
   */
  retry(fn) {
    //eslint-disable-next-line
    setTimeout(fn, this.interval);
    this.interval = this.nextInterval_();
  }

  /**
   * Reset the counter (e.g. after successful request.)
   */
  reset() {
    this.interval = 1000;
  }

  /**
   * Calculate the next wait time.
   * @return {number} Next wait interval, in milliseconds
   *
   * @private
   */
  nextInterval_() {
    var interval = this.interval * 2 + this.getRandomInt_(0, 1000);
    return Math.min(interval, this.maxInterval);
  }

  /**
   * Get a random int in the range of min to max. Used to add jitter to wait times.
   *
   * @param {number} min Lower bounds
   * @param {number} max Upper bounds
   * @private
   */
  getRandomInt_(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }
}

/**
 * @classdesc Module for perfom resumable uploads to Google services such as Drive or Cloud Storage. Does not have UI (only functional)
 * @class OmResumableUploader
 * @memberof module:OmResumableUploader
 */
export default class OmResumableUploader {
  /**
   * Module for perfom resumable uploads to Google services such as Drive or Cloud Storage. Does not have UI (only functional)
   * @param {Object} options Options to apply to the upload
   * @constructs
   */
  constructor(options) {
    var noop = function () {};
    this.file = options.file;
    this.contentType =
      options.contentType || this.file.type || "application/octet-stream";
    this.metadata = options.metadata || {
      title: this.file.name,
      mimeType: this.contentType
    };
    this.token = options.token;
    this.onComplete = options.onComplete || noop;
    this.onProgress = options.onProgress || noop;
    this.onError = options.onError || noop;
    this.offset = options.offset || 0;
    this.chunkSize = options.chunkSize || 0;
    this.retryHandler = new RetryHandler();

    this.uploadUrlInBody = options.uploadUrlInBody || false;
    this.uploadUrlBodyKey = "uploadUrl";

    this.url = options.url;
    if (!this.url) {
      let params = options.params || {};
      params.uploadType = "resumable";
      this.url = this.buildUrl_(options.fileId, params, options.baseUrl);
    }
    this.httpMethod = options.fileId ? "PUT" : "POST";
  }

  /**
   * Initiate the upload.
   */
  async upload() {
    let opts = {
      method: this.httpMethod,
      mode: "cors",
      cache: "default",
      headers: {
        "Content-Type": "application/json",
        "X-Upload-Content-Length": this.file.size,
        "X-Upload-Content-Type": this.contentType
      },
      body: JSON.stringify(this.metadata)
    };

    if (this.token) {
      opts.headers.Authorization = "Bearer " + this.token;
    }

    try {
      let resp = await fetch(this.url, opts);
      if (resp.status < 400) {
        if (this.uploadUrlInBody) {
          let bod = await resp.json();
          this.url = bod[this.uploadUrlBodyKey];
        } else {
          this.url = resp.headers.get("Location");
        }
        this.sendFile_();
      } else {
        this.onUploadError_(resp);
      }
    } catch (e) {
      this.onUploadError_(e);
    }
  }

  /**
   * Send the actual file content.
   *
   * @private
   */
  async sendFile_() {
    var content = this.file;
    var end = this.file.size;

    if (this.offset || this.chunkSize) {
      // Only bother to slice the file if we're either resuming or uploading in chunks
      if (this.chunkSize) {
        end = Math.min(this.offset + this.chunkSize, this.file.size);
      }
      content = content.slice(this.offset, end);
    }

    // let opts = {
    //   method: 'PUT',
    //   mode: 'cors',
    //   cache: 'default',
    //   headers: {
    //     'Content-Type': this.contentType,
    //     'Content-Range': "bytes " + this.offset + "-" + (end - 1) + "/" + this.file.size,
    //     'X-Upload-Content-Type': this.file.type
    //   },
    //   body: content
    // }

    // try {
    //   let resp = await fetch(this.url, opts)
    //   this.onContentUploadSuccess_(resp)
    // } catch(e) {
    //   this.onContentUploadError_(e)
    // }

    let xhr = new XMLHttpRequest();
    xhr.open("PUT", this.url, true);
    xhr.setRequestHeader("Content-Type", this.contentType);
    xhr.setRequestHeader(
      "Content-Range",
      "bytes " + this.offset + "-" + (end - 1) + "/" + this.file.size
    );
    // xhr.setRequestHeader("X-Upload-Content-Type", this.file.type);
    if (xhr.upload) {
      xhr.upload.addEventListener("progress", this.onProgress);
    }
    xhr.onload = this.onContentUploadSuccess_.bind(this);
    xhr.onerror = this.onContentUploadError_.bind(this);
    xhr.send(content);
  }

  /**
   * Query for the state of the file for resumption.
   *
   * @private
   */
  resume_() {
    var xhr = new XMLHttpRequest();
    xhr.open("PUT", this.url, true);
    xhr.setRequestHeader("Content-Range", "bytes */" + this.file.size);
    //xhr.setRequestHeader("X-Upload-Content-Type", this.file.type);
    if (xhr.upload) {
      xhr.upload.addEventListener("progress", this.onProgress);
    }
    xhr.onload = this.onContentUploadSuccess_.bind(this);
    xhr.onerror = this.onContentUploadError_.bind(this);
    xhr.send();
  }

  /**
   * Extract the last saved range if available in the request.
   *
   * @param {XMLHttpRequest} xhr Request object
   */
  extractRange_(resp) {
    var range = resp.headers.get("Range");
    if (range) {
      this.offset = parseInt(range.match(/\d+/g).pop(), 10) + 1;
    }
  }

  /**
   * Handle successful responses for uploads. Depending on the context,
   * may continue with uploading the next chunk of the file or, if complete,
   * invokes the caller's callback.
   *
   * @private
   * @param {object} e XHR event
   */
  async onContentUploadSuccess_(e) {
    if (e.currentTarget.status === 200 || e.currentTarget.status === 201) {
      this.onComplete(e.currentTarget.response);
    } else if (e.currentTarget.status === 308) {
      this.extractRange_(e.currentTarget);
      this.retryHandler.reset();
      this.sendFile_();
    } else {
      this.onContentUploadError_(e);
    }
  }

  /**
   * Handles errors for uploads. Either retries or aborts depending
   * on the error.
   *
   * @private
   * @param {object} e XHR event
   */
  async onContentUploadError_(e) {
    if (e.currentTarget.status && e.currentTarget.status < 500) {
      this.onError(e.currentTarget.response);
    } else {
      this.retryHandler.retry(this.resume_.bind(this));
    }
  }

  /**
   * Handles errors for the initial request.
   *
   * @private
   * @param {object} e XHR event
   */
  onUploadError_(res) {
    this.onError(res); // TODO - Retries for initial upload
  }

  /**
   * Construct a query string from a hash/object
   *
   * @private
   * @param {object} [params] Key/value pairs for query string
   * @return {string} query string
   */
  buildQuery_(params) {
    params = params || {};
    return Object.keys(params)
      .map(function (key) {
        return encodeURIComponent(key) + "=" + encodeURIComponent(params[key]);
      })
      .join("&");
  }

  /**
   * Build the drive upload URL
   *
   * @private
   * @param {string} [id] File ID if replacing
   * @param {object} [params] Query parameters
   * @return {string} URL
   */
  buildUrl_(id, params, baseUrl) {
    let url = baseUrl || "https://www.googleapis.com/upload/drive/v3/files/";
    if (id) {
      url += id;
    }
    let query = this.buildQuery_(params);
    if (query) {
      url += "?" + query;
    }
    return url;
  }
}