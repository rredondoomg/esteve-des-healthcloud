/**
 * Component to render tiles when showing table as list. Should not be used outside OmDatatable
 * @module OmDatatableTile
 * @version 1.0.0
 * @requires OmDatatableUtils
 * @see OmDatatable
 */
import { LightningElement, api } from 'lwc';

export default class OmDatatableTile extends LightningElement {
    _columns
    @api get columns() { return this._columns}
    set columns(val) {
        this._columns = val
        this.titleColumns = this.columns.filter(c => c.listTitle === true)
        this.subtitleColumns = this.columns.filter(c => c.listSubTitle === true)
        this.actionColumn = this.columns.find(c => c.type === 'actions')
        this.hasTypeActions = this.actionColumn !== undefined
    }
    @api row;
    @api showLabels = false;
    @api icon

    titleColumns
    subtitleColumns
    actionColumn
    hasTypeActions = true 


    get iconName () {
        if(typeof this.icon === 'function') { 
            return this.icon(this.row)
        }
        return this.icon

    }
}