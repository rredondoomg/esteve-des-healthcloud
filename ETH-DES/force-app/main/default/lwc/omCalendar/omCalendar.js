/**
 * Fullcalendar v4 implementation in LWC
 * @module OmCalendar
 * @version 1.0.0
 */
import { LightningElement, api, track, wire} from 'lwc'
import getVisualforceDomain from '@salesforce/apex/OM_Calendar.getVisualforceDomain';

export default class OmCalendar extends LightningElement {
    
    vfDomain = null
    initialized = false

    @wire(getVisualforceDomain, { siteName: '$communitySiteName' })
    getVisualforceDomainW({ data, error }) {
        if (error){
            console.error('Error on omCalendar.getVisualforceDomain: ' + error + ' / ' + JSON.stringify(error))
        }else if (data) {
            let url = new URL(data + '/apex/omCalendarVF')
            // url.searchParams.set('param', true) // Ready to add parameters in URL
            this.vfDomain = url.href
        }
    }

    //Tracks if any @api property has changed to notify children iframe
    @track apiChanged = false

    /**
     * Site Name. If informed, determines if the calendar is shown on a community (Required to set the internal URL of the calendar)
     * @type string
     * @default null
     */
    @api communitySiteName = null

    _events
    /**
     * List of events to be displayed in the calendar
     * @type array
     * @default []
     */
    @api 
    get events() { return this._events }
    set events(value) {
        let firstSet = this._events ? false : true

        this._events = []
        value.forEach(ev => {
            let copy = {}
            Object.keys(ev).forEach((key) => {
                copy[key === 'endEvent' ? 'end' : key] = ev[key]
            })
            this._events.push(copy)
        })

        if (firstSet){
            this.apiChanged = true
        }else{
            this.sendEventsToCalendar()
        }
    }

    /**
     * Set de date/time to show on Calendar load
     * @type datetime
     * @default null (today)
     */
    @api defaultDate = null

    /**
     * Info to display on Header-Left. Options: title / prev / next / prevYear / nextYear / today / dayGridMonth / dayGridWeek / dayGridDay / dayGrid / timeGridWeek / timeGridDay / timeGrid / listYear / listMonth / listWeek / listDay / list
     * @type string
     * @default 'prev,next today'
     */
    @api headerLeft = 'prev,next today'

    /**
     * Info to display on Header-Center. Options: title / prev / next / prevYear / nextYear / today / dayGridMonth / dayGridWeek / dayGridDay / dayGrid / timeGridWeek / timeGridDay / timeGrid / listYear / listMonth / listWeek / listDay / list
     * @type string
     * @default 'title'
     */
    @api headerCenter = 'title'

    /**
     * Info to display on Header-Right. Options: title / prev / next / prevYear / nextYear / today / dayGridMonth / dayGridWeek / dayGridDay / dayGrid / timeGridWeek / timeGridDay / timeGrid / listYear / listMonth / listWeek / listDay / list
     * @type string
     * @default 'dayGridMonth,timeGridWeek,timeGridDay'
     */
    @api headerRight = 'dayGridMonth,timeGridWeek,timeGridDay'

    /**
     * Default view to display on load. Options: dayGridMonth/dayGridWeek/dayGridDay/dayGrid/timeGridWeek/timeGridDay/timeGrid/listYear/listMonth/listWeek/listDay/list
     * @type string
     * @default 'dayGridMonth'
     */
    @api defaultView = 'dayGridMonth'

    /**
     * Determines if day names and week names are clickable.
     * @type boolean
     * @default false
     */
    @api navLinks = false

    /**
     * Limits the number of events displayed on a day. The rest will show up in a "more" popover.
     * @type boolean
     * @default false
     */
    @api eventLimit = false

    /**
     * Display custom button to create new record
     * @type boolean
     * @default false
     */
    @api newButton = false

    /**
     * Label to show on the "New" button
     * @type string
     * @default 'New'
     */
    @api newButtonLabel = 'New'

    /**
     * First day of week - 0: Sunday, 1: Monday...
     * @type integer
     * @default 1
     */
    @api firstDay = 1

    /**
     * Locale of Calendar - 'es', 'en', 'fr'...
     * @type string
     * @default 'en'
     */
    @api locale = 'en'

    /**
     * Timezone of Calendar - 'local', 'UTC', 'America/New_York', 'Europe/Madrid'
     * @type string
     * @default 'local'
     */
    @api timeZone = 'local'

    /**
     * Whether or not to display a marker indicating the current time.
     * @type boolean
     * @default false
     */
    @api nowIndicator = false 

    /**
     * Determines if the events can be dragged and resized.
     * @type boolean
     * @default false
     */
    @api editable = false 

    /**
     * Determines if events being dragged and resized are allowed to overlap each other.
     * @type boolean
     * @default false
     */
    @api eventOverlap = false 

    /**
     * If true, it will show cursor:pointer when hovering an event
     * @type boolean
     * @default false
     */
    @api eventCursorPointer = false 

    /**
     * Label for the "Today" button
     * @type string
     * @default 'Today'
     */
    @api todayButtonText = 'Today'

    /**
     * Label for the "Month" button
     * @type string
     * @default 'Month'
     */
    @api monthButtonText = 'Month'

    /**
     * Label for the "Week" button
     * @type string
     * @default 'Week'
     */
    @api weekButtonText = 'Week'

    /**
     * Label for the "Day" button
     * @type string
     * @default 'Day'
     */
    @api dayButtonText = 'Day'

    /**
     * Label for the "List" button
     * @type string
     * @default 'List'
     */
    @api listButtonText = 'List'

    /**
     * Label for the "All-day" section
     * @type string
     * @default 'All-day'
     */
    @api allDayText = 'All-day'

    /**
     * Min Height for the Events on TimeGrid view
     * @type integer
     * @default 40
     */
    @api timeGridEventMinHeight = 40
    
     /**
     * Height in css format of the Calendar. [20px; / 50vh;...]
     * @type string
     * @default null
     */
    @api calendarHeight = null

    /**
     * Navigate to the specified Date
     */
    @api navigateToDate(value) {
        this.sendMessageToIframe('navigateToDate', { 
            selectedDate: value
        })
    }

    @track pxHeight = 0

    get dynamicHeight () {
        return 'height: ' + (this.calendarHeight ? this.calendarHeight : this.pxHeight + 'px;')
    }

    get iframestyle () {
        return ''
    }

    //iFrame load status. Only can communicate with frame if content window is loaded
    @track frameloaded = false
    oniframeload() { 
        this.frameloaded = true
    }

    

    renderedCallback() {
        //This parts is to send down @api properties when change
        if(this.apiChanged && this.frameloaded) {
            //eslint-disable-next-line 
            setTimeout(() => {
                this.sendMessageToIframe('render', { 
                    events: this._events,
                    headerLeft: this.headerLeft,
                    headerCenter: this.headerCenter,
                    headerRight: this.headerRight,
                    defaultView: this.defaultView,
                    navLinks: this.navLinks,
                    editable: this.editable,
                    eventLimit: this.eventLimit,
                    newButton: this.newButton,
                    newButtonLabel: this.newButtonLabel,
                    firstDay: parseInt(this.firstDay, 10),
                    locale: this.locale,
                    timeZone: this.timeZone,
                    nowIndicator: this.nowIndicator,
                    eventOverlap: this.eventOverlap,
                    todayButtonText: this.todayButtonText,
                    monthButtonText: this.monthButtonText,
                    weekButtonText: this.weekButtonText,
                    dayButtonText: this.dayButtonText,
                    listButtonText: this.listButtonText,
                    allDayText: this.allDayText,
                    defaultDate: this.defaultDate,
                    timeGridEventMinHeight: parseInt(this.timeGridEventMinHeight, 10),
                    eventCursorPointer: this.eventCursorPointer
                })
                this.apiChanged = false
            }, 800)
        }

        //Here onwards, only initialization things
        if (this.initialized) {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
          return;
        }

        this.initialized = true
        this.pxHeight = this.template.querySelector('.content').offsetHeight
    }

    sendMessageToIframe(method, params) {
        //TODO: Sanitize input?
        let data = {
            method: method,
            params: JSON.parse(JSON.stringify(params))
        }

        let iframeElem = this.template.querySelector('iframe')
        if (iframeElem && iframeElem.contentWindow){
            iframeElem.contentWindow.postMessage(data, '*')
        }
    }

    sendEventsToCalendar() {
        this.sendMessageToIframe('eventsUpdate', { 
            events: this.events
        })
    }

    connectedCallback() {
        window.addEventListener('message', (event) => {
            // console.log({message: 'OmCalendar window.addEventListener method',method: event.data.method, params:JSON.stringify(event.data.params)})
            switch(event.data.method) {
                case 'eventClick' :
                    this.onEvent('eventclick', event.data.params) // An event has been clicked
                    break;
                case 'dateClick' :
                    this.onEvent('dateclick', event.data.params) // A cell of a day or a timespan has been clicked
                    break;
                case 'newButtonClick' :
                    this.onEvent('newbuttonclick', event.data.params) // The "new" button has been clicked
                    break;
                case 'eventMouseEnter' :
                    this.onEvent('eventmouseenter', event.data.params) // Triggered when the user mouses over an event.
                    break;
                case 'eventMouseLeave' :
                    this.onEvent('eventmouseleave', event.data.params) // Triggered when the user mouses out of an event
                    break;
                case 'eventDrop' :
                    this.onEvent('eventdrop', event.data.params) // Triggered when dragging stops and the event has moved to a different day/time.
                    break;
                case 'eventResize' :
                    this.onEvent('eventresize', event.data.params) // Triggered when resizing stops and the event has changed in duration.
                    break;
                case 'currentDate' :
                    this.onEvent('currentdate', event.data.params) // Triggered everytime that a new set of dates has been rendered.
                    break;
                case 'bodyHeight' :
                    this.setAutoHeight(event.data.params);
                    break;
                default:
                    console.log('OmCalendar no action informed')
                    break;
            }
        
        })
    }

    setAutoHeight(data) {
        if (!this.calendarHeight){
            this.pxHeight = data + 30
        }
    }

    onEvent(eventName, eventData) {
        this.dispatchEvent(new CustomEvent(eventName, { detail: eventData, bubbles: true, compose: true }))
    }
}