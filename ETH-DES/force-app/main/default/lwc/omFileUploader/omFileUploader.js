/**
 * @module OmFileUploader
 * @version 1.0.0
 * @apex OM_FileUploader
 */
import { LightningElement, api } from 'lwc';
import uploadContent from '@salesforce/apex/OM_FileUploader.uploadContent'
import { FlowNavigationNextEvent } from 'lightning/flowSupport';

export default class OmFileUploader extends LightningElement {
    /**
     * Record Id. Will be used to create links to uploaded files
     * Will be FirstPublicationId from ContentVersion and a record in ContentDocumentLink
     * @type string
     */
    @api recordId;

    /**
     * Related record Ids. For ContentVersion, will create a ContentDocumentLink
     * @type String[]
     * @default undefined
     */
    @api relatedRecordIds

    /**
     * ShareType for ContentDocumentLinks (V = Viewer, C = Collaborator, I = Inferred)
     * @type String
     * @default 'V'
     */
    @api shareType = 'V'

    /**
     * Comma-separated files extensions to accept
     * Ex: .xlsx, .xls, .csv, .png, .jpg, .doc, .docx, .pdf, .zip
     * @type String
     * @default ''
     */
    @api accepts = ''

    /**
     * If should allow multiple file upload
     * @type Boolean
     * @default false
     */
    @api multiple = false


    /**
     * For Flows only: If the component should call next screen when upload finishes
     * @type Boolean
     * @default false
     */
    @api nextOnFinish = false

    /**
     * Label for the field
     * @type String
     * @default 'Select file'
     */
    @api inputLabel = 'Select file'

    /**
     * Label for the button when multiple files allowed
     * @type String
     * @default 'Upload'
     */
    @api uploadLabel = 'Upload'

    /**
     * Output variable for flows with ContentDocument Ids created.
     * Empty for Attachments and Documents
     * @type String[]
     * @private
     */
    _createdDocuments

    @api get createdDocuments () {
        return this._createdDocuments
    }

    @api get createdDocumentIds () {
        return this._createdDocuments ? this._createdDocuments.map(d => d.Id) : []
    }

    /**
     * Output variable for flows with record Ids created
     * @type String[]
     * @private
     */
    _createdRecords

    @api get createdRecords () {
        return this._createdRecords
    }

    @api get createdRecordIds () {
        return this._createdRecords ? this._createdRecords.map(s => s.Id) : []
    }

    /**
     * Output variable for flows with ContentDocumentLink Ids created
     * Empty for Attachments and Documents
     * @type String[]
     * @private
     */
    _createdLinks

    @api get createdLinks () {
        return this._createdLinks
    }

    @api get createdLinksIds () {
        return this._createdLinks ? this._createdLinks.map(d => d.Id) : []
    }

    filesData = []
    uploadResults = []
    lastkey = 0

    get hasFilesData () {
        return this.filesData.length > 0
    }

    openfileUpload(event) {
        const file = event.target.files[0]
        let reader = new FileReader()
        reader.onload = () => {
            var base64 = reader.result.split(',')[1]
            this.filesData = this.filesData.concat(
                [{
                    filename: file.name,
                    size: file.size,
                    base64: base64,
                    result: {
                        message: 'Waiting for upload',
                        isPending: true,
                        isOk: true,
                        isUploading: false
                    },
                    key: this.lastkey
                }]
            )

            this.lastkey++;

            console.log(this.filesData)

            if(!this.multiple) {
                this.beginUpload()
            }
        }
        reader.readAsDataURL(file)
    }

    setDataStatus(i, status) {
        this.filesData[i].result = status
        // Clone to force rerender
        let aux = JSON.stringify(this.filesData)
        this.filesData = JSON.parse(aux)
    }
    
    beginUpload(){
        this.isUploading = true
        for( let i = 0; i < this.filesData.length; i++) {
            let dat = this.filesData[i]
            this.setDataStatus(i, { isUploading: true, isPending: false, message: 'Uploading...' })
            // eslint-disable-next-line @lwc/lwc/no-async-operation
            setTimeout(async () => {
                try {

                    // eslint-disable-next-line no-await-in-loop
                    let result = await uploadContent({ 
                        base64: dat.base64,
                        filename: dat.filename,
                        recordId: this.recordId,
                        relatedRecordIds: this.relatedRecordIds,
                        shareType: this.shareType  
                    })
                    this.setDataStatus(i, result)
                } catch (e) {
                    console.log('error happened', e)
                    this.setDataStatus(i, { isUploading: false, isPending: false, isOk: false, message: e.body.message })
                }
                this.checkFinishUpload()
            }, 50)
        }
    }

    checkFinishUpload () {
        let allFinished = []
        let errors = []

        this.filesData.forEach(d => {
            if (d.result.isPending === false && d.result.isUploading === false) {
                allFinished.push(d)
            }
            if( d.result.isOk === false ) {
                errors.push(d)
            }
        })

        console.log('checkFinishUpload ' + this.filesData.length + ' - ' + allFinished.length)

        if(this.filesData.length === allFinished.length) {
            this.finishUpload(allFinished, errors)
        }
    }

    finishUpload(oks, errors) {
        console.log('Upload finished')
        this.isUploading = false;
        if(errors.length === 0) {
            this.filesData = []
        }
        // Fill flow output vars

        let createdDocuments = []
        let createdRecords = []
        let createdLinks = []

        oks.forEach(d => {
            createdDocuments.push(d.result.contentDocument)
            createdRecords.push(d.result.record)
            createdLinks = createdLinks.concat(d.result.links)
        })

        this._createdDocuments = createdDocuments
        this._createdRecords = createdRecords
        this._createdLinks = createdLinks

        /**
         * Event emiited when upload finishes
         * @event finish
         * @type object
         * @property {array} createdDocuments Array containing ContentDocument created
         * @property {array} createdRecords Array containing Contentversions created
         * @property {array} createdLinkss Array containing ContentDocumentLink created
         */
        this.dispatchEvent(new CustomEvent('finish', {
            detail: {
                createdDocuments,
                createdRecords,
                createdLinks
            },
            bubbles: true,
            composed: true
        }))

        if (this.nextOnFinish) {
            console.log('Emiiting flow event')
            this.dispatchEvent(new FlowNavigationNextEvent())
        }
        
    }
}