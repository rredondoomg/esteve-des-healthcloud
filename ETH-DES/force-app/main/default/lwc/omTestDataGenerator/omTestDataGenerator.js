import { LightningElement, api, track, wire } from 'lwc';

export default class OmTestDataGenerator extends LightningElement {

    @api mockarooApi = ''

    @track selectedObject = null;
    @track selectedFields = [];

    @track numberOfRecords = 100;

    //Map of object name, faked fields and parent relationship name (for trees)
    dataToFake = [];

    //Computed
    get isLoading() { return false }
    get hasFakeData() { return this.dataToFake && this.dataToFake.length > 0 }

    @track dataToFakeStr = ''

    handleObjectChange(event) {
        this.selectedObject = event.detail.sobject
        this.selectedFields = event.detail.fields
        this.addToSelection()
    }

    handleSelectedFields(event){
        let selrows = event.detail.selectedRows;
        this.selectedFields = event.detail.selectedRows;
        for (let i = 0; i < selrows.length; i++){
            console.log("You selected: " + selrows[i].label);
        }
        console.log(this.selectedFields);
    }

    addToSelection() {
        if(this.dataToFake === undefined) {
            this.dataToFake = []
        }
        let tof = {
            object: this.selectedObject,
            fields: [],
            children: [],
            numberOfRecords: this.numberOfRecords
        }
        for (let i = 0; i < this.selectedFields.length; i++){
            tof.fields.push(this.selectedFields[i])
        }

        this.dataToFake = this.dataToFake.concat([tof])
        this.selectedObject = null;
        this.selectedFields = [];

        this.currentStep = '2'
    }

    /**
     * Tracks the current step
     * 1 - Select base plan object
     * 2 - Select children tree objects
     * 3 - Generate fake objects
     * 4 - Download files
     */
    @track currentStep = '1'
    get isStep1(){ return this.currentStep === '1'}
    get isStep2(){ return this.currentStep === '2'}
    get isStep3(){ return this.currentStep === '3'}
    get isStep4(){ return this.currentStep === '4'}


    @track planFiles = null;
    @track recordFiles = [];

    saveChildSelectionModal(ev) {
      let tof = {
        object: ev.detail.sobject,
        relationshipField: ev.detail.relationshipField,
        fields: [],
        children: [],
        numberOfRecords: this.numberOfRecords
      }
      //Only copy necessary data
      for (let i = 0; i < ev.detail.fields.length; i++){
          let f = ev.detail.fields[i]
          tof.fields.push({
            compoundFieldName: f.compoundFieldName,
            extraTypeInfo: f.extraTypeInfo,
            name: f.name,
            nameField: f.nameField,
            length: f.length,
            picklistValues: f.picklistValues,
            soapType: f.soapType,
            type: f.type
          })
      }

      let elm = this.dataToFake.find(e => e.object === ev.target.dataset.object)
      elm.children.push(tof)

      //debug
      this.dataToFakeStr = JSON.stringify(this.dataToFake, null, 2)

    }

    getFakeData() {
        //Get collections to retrieve
        if(this.dataToFake.length === 0) {
            console.error('No selected data to fake')
        } else {
            //generate a plan for each base object
            this.dataToFake.forEach(async (d) => {
                let plan = await this.fakeRecords(d)
                console.log('Plan generated')
                console.log(JSON.stringify(plan))
            })
        }

    }

    fakeFieldTransform(field) {
        let fieldType = field.type;

        //For first and last names
        if(field.extraTypeInfo === 'personname') {
            if(field.name.indexOf('First') !== -1) {
                let genders = ['(Female)', '(Male)']
                return 'First Name ' + genders[Math.round(Math.random())]
            }
            
            return 'Last Name'
            
        }
        //For addresses
        else if(field.compoundFieldName && field.compoundFieldName.indexOf('Address') !== -1) {
            if(field.name.indexOf('Street') !== -1) {
                return 'Street Name'
            } else if(field.name.indexOf('PostalCode') !== -1) {
                return 'Postal Code'
            } else if(field.name.indexOf('City') !== -1) {
                return 'City'
            } else if(field.name.indexOf('State') !== -1) {
                return 'State'
            } else if(field.name.indexOf('Country') !== -1) {
                return 'Country Code'
            }
        } else {
            switch(fieldType) {
                case 'picklist':
                    return 'Custom List'
                case 'date':
                    return 'Date'
                case 'phone':
                    return 'Phone'
                case 'url':
                    return 'URL'
                case 'string': 
                    return 'Words'
                case 'textarea': 
                    return 'Words'
                default:
                    return 'Words'
            }
        }

        return ''
    }

    async fakeRecords(config) {
        const recordMetadata = []
        // const recordMetadata = {
        //     name: 'name',
        //     email: 'email',
        //     website: 'url',
        //     amount: 'currency',
        //     phone: 'phoneNumber',
        //     closeAt: 'dateInFuture',
        // };
        if(config.fields) {
            config.fields.forEach(field => {
                let fc = {
                    name: field.name,
                    type: this.fakeFieldTransform(field)
                }
                if(field.type === 'picklist') {
                    fc.values = field.picklistValues.filter(pl => pl.active === true).map(pl => pl.value)
                }
                recordMetadata.push(fc)
            })
        }
        let data = await this.getFakeCollection(config, recordMetadata)
        //Transform Mockaroo response into a valid SFDX data structure and link referencies
        let mockedRecords = data.map((rec, idx) => {
            let obj =  {...rec}
            obj.attributes = {
                type: config.object,
                referenceId: config.object + 'Ref' + idx
            }
            if(config.relationshipField) {
                obj[config.relationshipField] = '@'
            }
            return obj
        })
        let trans = { records : mockedRecords }
        console.log(JSON.stringify(trans, null, 2))

        //look in children
        if(config.children && config.children.length > 0) {
            config.children.forEach(async (ch) => {
                await this.fakeRecords(ch)
            })
        }

        return trans
        
    }

    /**
     * Calls Mockaroo API to generate mock records
     * @param {Object} objectConfig SObject mocking config
     * @param {Object} recordMetadata Fields to get for each record
     */
    async getFakeCollection(objectConfig, recordMetadata) {
      let data = [ { Name : 'Mocked name 1'}, { Name : 'Mocked name 2' } ]
      console.log(JSON.stringify(recordMetadata, null, 2))
    //   let response = await fetch('https://api.mockaroo.com/api/generate.json?count=' + amountOfRecords, {
    //       method: 'POST',
    //       headers: {'Content-Type': 'application/json; charset=utf-8'},
    //       body: JSON.stringify({amountOfRecords,recordMetadata}),
    //   })
    //   if(response.status == 200) {
    //     data = await response.json()
    //   }
      return data
    }

    

    saveToFileCSV(){

    }

    saveToJSON() {

    }
}