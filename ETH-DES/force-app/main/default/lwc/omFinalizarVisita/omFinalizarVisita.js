import comprobarStocks from '@salesforce/apex/OM_AtlasCallout.comprobarStocks';
import finalizarVisita from '@salesforce/apex/OM_TranscursoVisitas_Controller.finalizarVisita';
import mostrarCheckFinalizarSinFirmar from '@salesforce/apex/OM_TranscursoVisitas_Controller.mostrarCheckFinalizarSinFirmar';
import comprobarSoloUnEquipoPrincipal from '@salesforce/apex/OM_TranscursoVisitas_Controller.comprobarSoloUnEquipoPrincipal';
import {fireEvent} from 'c/pubsub';
import { getRecordNotifyChange } from 'lightning/uiRecordApi';
import {CurrentPageReference} from 'lightning/navigation';
// import comprobarSiEsInstalacion from '@salesforce/apex/OM_TranscursoVisitas_Controller.comprobarSiEsInstalacion';

import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import { LightningElement, api, track, wire } from 'lwc';

export default class OmFinalizarVisita extends LightningElement {
    @api recordId;
    @track erroresFungibles = [];
    @track erroresEquipos = [];

    @track equiposConErrores = false;
    @track fungiblesConErrores = false;
    @track procesoOK = false;
    @track showSpinner = false;

    @track errorInicio = false;
    @track errorFin = false;
    @track finalizarSinFirmarSeguimiento = false;
    @track showFinalizarSinFirmar = false;

    @track esInicio = false;
    @track esSeguimiento = false;
    @track esFin = false;
    @track esIntervencion = false;

    @track hayErrores = false;

    @track finalizarSinFirmar = false;
    @track showFinalizarSinFirmar;

    @wire(CurrentPageReference) pageRef
    
    // @track showCMP;

    connectedCallback(){
        this.mostrarCheckFinalizarSinFirmar();
        // this.showCMP = true;
        console.log('RECORD ID EN EL LWC '+this.recordId);
        this.showSpinner = true;

        if(!this.esIntervencion){
            this.comprobarSoloUnEquipoPrincipal();
        }
    }

    comprobarSoloUnEquipoPrincipal(){
        comprobarSoloUnEquipoPrincipal({serviceAppId : this.recordId})
        .then(response => {
            
            //Si no hay errores se comprueba el stock, si hay dos equipos principales o no hay ninguno ppal, mandar ERROR
            if(response[false] == ''){
                this.comprobarStocks();
            }else{
                this.dispatchEvent(new ShowToastEvent({ title: '', message: response[true], variant: 'error'}));
                this.showSpinner = false;
                this.cancelar();
            }
        })
        .catch(error => {
            console.log('ERROR comprobarSoloUnEquipoPrincipal '+JSON.stringify(error));
            // this.dispatchEvent(new ShowToastEvent({ title: '', message: 'Error al actualizar los equipos suministrados', variant: 'error'}));
            this.showSpinner = false;
        })
    }
    comprobarStocks(){
        comprobarStocks({serviceAppId : this.recordId})
        .then(response => {
            // console.log('Respuesta APEX /stock-equipo: '+JSON.stringify(response));

            console.log('Respuesta vale: '+JSON.stringify(response));
            if(response['ErroresFungibles']){
    
                this.erroresFungibles = response['ErroresFungibles'];
                console.log('erroresFungibles '+this.erroresFungibles);
                
                this.fungiblesConErrores = true;

                this.hayErrores = true;
            }
            if(response['ErroresEquipos']){
                this.erroresEquipos = response['ErroresEquipos'];
                console.log('erroresEquipos '+this.erroresEquipos);
                this.equiposConErrores = true;

                this.hayErrores = true;
            }
            
            if(response['ErroresFungibles'] || response['ErroresEquipos']){
                this.dispatchEvent(new ShowToastEvent({ title: '', message: 'Error de stock en los elementos al finalizar visita.', variant: 'error'}));
            }

            if(response['false'] == '' ){
                console.log('Mensaje del toast: '+response['false']);
                console.log('Datos atlas: '+JSON.stringify(response['datos']));
                // this.dispatchEvent(new ShowToastEvent({ title: '', message: 'El proceso ha ido OK', variant: 'success'}));
                this.procesoOK = true;
                // this.insertNewEquipoSuministrado(response['datos']);
                // this.showAddEquipoModal = false;

                //NO HAY ERRORES. SE FINALIZA LA VISITA
                // this.finalizarVisita();

            }

            // this.finalizarVisita();

            this.showSpinner = false;

        })
        .catch(error => {
            console.log('ERROR comprobarStocks '+JSON.stringify(error));
            // this.dispatchEvent(new ShowToastEvent({ title: '', message: 'Error al actualizar los equipos suministrados', variant: 'error'}));
            this.showSpinner = false;
        })

        
    }

    finalizarVisita(){
        finalizarVisita({serviceAppId : this.recordId, finalizarVisitaSinFirmar : this.finalizarSinFirmar})
        .then(response => {
            console.log('Respuesta APEX /stock-equipo: '+JSON.stringify(response));
            if(response['true']){
                console.log('Mensaje del toast: '+response['false']);
                console.log('Datos atlas: '+JSON.stringify(response['datos']));
                this.dispatchEvent(new ShowToastEvent({ title: '', message: response['true'], variant: 'error'}));
            }else{
                this.dispatchEvent(new ShowToastEvent({ title: '', message: response['false'], variant: 'success'}));

                const valueChangeEvent = new CustomEvent("closecomponent", {
                    detail: false
                  });
                  // Fire the custom event
                  this.dispatchEvent(valueChangeEvent);

                  getRecordNotifyChange([{recordId: this.recordId}]);
                fireEvent(this.pageRef, 'disableTranscursoVisita', { operation : 'disable' } )
            }

        })
        .catch(error => {
            console.log('ERROR finalizarVisita '+JSON.stringify(error));
            // this.dispatchEvent(new ShowToastEvent({ title: '', message: 'Error al actualizar los equipos suministrados', variant: 'error'}));
            this.showSpinner = false;
            this.dispatchEvent(new ShowToastEvent({ title: '', message: JSON.stringify(error) , variant: 'error'}));

        })
    }

    mostrarCheckFinalizarSinFirmar(){
        mostrarCheckFinalizarSinFirmar({serviceAppId : this.recordId})
        .then(response => {
            // console.log('Respuesta APEX /stock-equipo: '+JSON.stringify(response));


            if(response['Seguimiento'] == true){
                //Si devuelve true le saco el checkbox para terminar sin firmar
                this.showFinalizarSinFirmar = true;
                this.errorInicio = false;
                this.errorFin = false;

                this.esInicio = false;
                this.esSeguimiento = true;
                this.esFin = false;
            }
            if(response['Seguimiento'] == false){
                //Si devuelve true le saco el checkbox para terminar sin firmar
                this.showFinalizarSinFirmar = false;
                this.errorInicio = false;
                this.errorFin = false;

                this.esInicio = false;
                this.esSeguimiento = true;
                this.esFin = false;
            }

            if(response['Inicio'] == true){
                //Si devuelve true, error
                this.showFinalizarSinFirmar = false;
                this.errorInicio = true;
                this.errorFin = false;

                this.esInicio = true;
                this.esSeguimiento = false;
                this.esFin = false;
            }
            if(response['Inicio'] == false){
                //Si devuelve true le saco el checkbox para terminar sin firmar
                this.showFinalizarSinFirmar = false;
                this.errorInicio = false;
                this.errorFin = false;

                this.esInicio = true;
                this.esSeguimiento = false;
                this.esFin = false;

                if(response['Seguimiento'] == true){
                    //Si devuelve true le saco el checkbox para terminar sin firmar
                    this.showFinalizarSinFirmar = true;
                    this.errorInicio = false;
                    this.errorFin = false;
    
                    this.esInicio = false;
                    this.esSeguimiento = true;
                    this.esFin = false;
                }
                if(response['Seguimiento'] == false){
                    //Si devuelve true le saco el checkbox para terminar sin firmar
                    this.showFinalizarSinFirmar = false;
                    this.errorInicio = false;
                    this.errorFin = false;
    
                    this.esInicio = false;
                    this.esSeguimiento = true;
                    this.esFin = false;
                }
            }
            
            if(response['Fin'] == true){
                //Si devuelve true, error   
                this.showFinalizarSinFirmar = false;
                this.errorInicio = false;
                this.errorFin = true;

                this.esInicio = false;
                this.esSeguimiento = false;
                this.esFin = true;

                console.log('Entra en el true')
            }
            if(response['Fin'] == false){
                console.log('Entra en el false')
                if(response['Seguimiento'] == true){
                    //Si devuelve true le saco el checkbox para terminar sin firmar
                    console.log('Entra en el seguimiento true')
                    this.showFinalizarSinFirmar = true;
                    this.errorInicio = false;
                    this.errorFin = false;
    
                    this.esInicio = false;
                    this.esSeguimiento = true;
                    this.esFin = false;
                }
                if(response['Seguimiento'] == false){
                    //Si devuelve true le saco el checkbox para terminar sin firmar
                    console.log('Entra en el seguimiento false')
                    this.showFinalizarSinFirmar = false;
                    this.errorInicio = false;
                    this.errorFin = false;
    
                    this.esInicio = false;
                    this.esSeguimiento = true;
                    this.esFin = false;
                }
            }

            if(response['Intervencion'] == true){
                //Si devuelve true le saco el checkbox para terminar sin firmar
                this.showFinalizarSinFirmar = true;
                this.errorInicio = false;
                this.errorFin = false;

                this.esInicio = false;
                this.esSeguimiento = true;
                this.esIntervencion = true;
                this.esFin = false;
            }


            if(response == true){
                this.showFinalizarSinFirmar = true;
                // console.log('Mensaje del toast: '+response['false']);
                // console.log('Datos atlas: '+JSON.stringify(response['datos']));
                // this.dispatchEvent(new ShowToastEvent({ title: '', message: response['true'], variant: 'error'}));
            }

        })
        .catch(error => {
            console.log('ERROR mostrarCheckFinalizarSinFirmar '+JSON.stringify(error));
            // this.dispatchEvent(new ShowToastEvent({ title: '', message: 'Error al actualizar los equipos suministrados', variant: 'error'}));
            // this.showSpinner = false;
        })
    }

    cancelar(event){
        //Mandar mediante evento al padre para que oculte el componente

        // this.showCMP = false;

        const valueChangeEvent = new CustomEvent("closecomponent", {
            detail: false
          });
          // Fire the custom event
          this.dispatchEvent(valueChangeEvent);

        //TODO BORRAR VARIABLES SI PROCEDE
    }

    handleClickFinalizarSinFirmar(event){
        this.finalizarSinFirmar = event.target.checked;
        console.log('Check finalizar sin firmar: '+this.finalizarSinFirmar);
    }
}