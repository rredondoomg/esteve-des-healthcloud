/**
 * Component that uses ChartJS to render standard report custom charts
 * @module OmChart
 * @version 1.0.0
 */
import { LightningElement, track, api } from 'lwc';
import { loadScript } from 'lightning/platformResourceLoader';
import chartjs from '@salesforce/resourceUrl/chart';
import getReportData from '@salesforce/apex/OM_LWC_pack.getReportData';
import runQuery from '@salesforce/apex/OM_LWC_pack.runQuery';

const colors = [
  'rgb(0, 160, 224)',
  'rgb(255, 99, 132)',
  'rgb(255, 159, 64)',
  'rgb(255, 205, 86)',
  'rgb(75, 192, 192)',
  'rgb(54, 162, 235)',
  'rgb(0, 160, 224)',
]

export default class OmChart extends LightningElement {
    @track error
    get hasError() {
      return this.error !== null && this.error !== '' && this.error !== undefined
    }
    chart
    chartjsInitialized = false

    //Basic configuration

    /**
     * Type of chart. Valid values are 'line', 'bar'
     * @type String
     * @default 'line'
     */
    @api type

    /**
     * CSS width formthe underlying canvas
     * @type String
     * @default ''
     */
    @api width

    /**
     * CSS height formthe underlying canvas
     * @type String
     * @default ''
     */
    @api height

    /**
     * If X and Y axis labels should be hidden
     * @type Boolean
     * @default false
     */
    @api hideLabels = false

    /**
     * If X axis labels should be hidden
     * @type Boolean
     * @default false
     */
    @api hideXLabels = false

    /**
     * If Y axis labels should be hidden
     * @type Boolean
     * @default false
     */
    @api hideYLabels = false

    /**
     * X axis start number
     * @type Number
     * @default 0
     */
    @api startX = 0

    /**
     * Y axis start number
     * @type Number
     * @default 0
     */
    @api startY = 0

    _colors = colors

    /**
     * Palette of colors to use. By default uses base ChartJS color palette
     * @type Array<String>
     * @default Array
     */
    @api 
    set colors(value) { this._colors = value }
    get colors() { return this._colors}

    /**
     * For Lighning App Builder only: Pound sign (#) separated values for colors array
     * @type String
     * @default '''
     */
    @api 
    set colorsAB(value) { this._colors = value.indexOf('#') !== -1 ? value.split('#') : [value] }
    get colorsAB() { return this._colors.join('#')  }
    

    /**
     * Full ChartJs configuration object, used to go full custom with ChartJS. Consider using components properties instead
     * If this property is informed, other configuration component properties such as 'type', 'options', 'chartData' and more are ignored.
     * See https://www.chartjs.org/docs/latest/configuration/ for more info
     * @type Object
     * @default undefined
     */
    @api config

    /**
     * Full ChartJs data object
     * If this property is informed, report/query data is ignored.
     * See https://www.chartjs.org/docs/latest/configuration/#dataset-configuration for more info
     * @type Object
     * @default undefined
     */
    @api chartData

    /**
     * Full ChartJs configuration options object
     * If this property is informed, other configuration component properties such as 'hideLabels', 'startX' or 'dartData' and more are ignored.
     * See https://www.chartjs.org/docs/latest/configuration/ for more info
     * @type Object
     * @default undefined
     */
    @api options

    //Data from reports
    /**
     * Id or DeveloperName of the report to pull data from
     * @type String
     * @default undefined
     */
    @api reportId

    /**
     * Valid SOQL query tu run aggregate results
     * @type String
     * @default undefined
     */
    @api aggregatedQuery

    /**
     * When specifying an Aggregated query, inform which fields is going to be the label for the dataset
     * @type String
     * @default undefined
     */
    @api aggregatedQueryGroupField

    /**
     * When specifying an Aggregated query, customize each dataset.
     * See https://www.chartjs.org/docs/latest/charts/radar.html#dataset-properties for all available options
     * @type Array<Object>
     * @default undefined
     */
    @api aggregatedQueryDatasets

    _filterIndexes = []

    /**
     * Indexes in report filter to override data before run. Must have same length as 'filterValues' property
     * @type Array<Number>
     * @default undefined
     */
    @api 
    set filterIndexes(value) { this._filterIndexes = value }
    get filterIndexes() { return this._filterIndexes}

    @api 
    set filterIndexesAB(value) { this._filterIndexes = value.indexOf('#') !== -1 ? value.split('#') : [value] }
    get filterIndexesAB() { return this._filterIndexes.join('#')  }

    _filterValues = []
    /**
     * Values in report filter to override data before run. Must have same length as 'filterIndexes' property
     * @type Array<Number>
     * @default undefined
     */
    @api 
    set filterValues(value) { this._filterValues = value }
    get filterValues() { return this._filterValues}

    @api 
    set filterValuesAB(value) { this._filterValues = value.indexOf('#') !== -1 ? value.split('#') : [value] }
    get filterValuesAB() { return this._filterValues.join('#')  }

    //inner

    async getConfig() {
      let repdata = await this.getData()
      let conf = {}
      if(this.config && Object.keys(this.config).length > 2) {
        console.log('omChart -> getConfig returning API config')
        conf = this.config
      } else {
        conf.type = this.type ? this.type : 'line'
        if(repdata) {
          conf.data = repdata
        } else {
          conf.data = this.chartData ? JSON.parse(JSON.stringify(this.chartData)) : {
            datasets: [{data: [1], label: 'No data'}],
            labels: ['No data']
          }
        }
        conf.options = this.options ? this.options : {
          onClick: this.onChartClick.bind(this),
          responsive: true,
          maintainAspectRatio: true,
          legend: {
            display: !this.hideLabels
          },
          scale: {
            ticks: {
                beginAtZero: true,
                stepSize: 1,
                minor: false,
                major: false,
                callback: (e) => { e }
            }
          },
          scales: {
            xAxes: [{
                ticks: {
                    display: !this.hideXLabels,
                    min: parseInt(this.startX, 10)
                }
            }],
            yAxes: [{
              ticks: {
                  display: !this.hideYLabels,
                  min: parseInt(this.startY, 10)
              }
          }]
          }
        }

      }
      console.log('omChart -> getConfig called')
      console.log(JSON.stringify(conf))
      return conf
    }

    async getData() {
        if(this.reportId) {
          console.log('omChart -> getData called. Fetching report')
          try {
            let asObject = JSON.parse(await getReportData({idOrDevname: this.reportId, filterIndexes: this._filterIndexes, filterValues: this._filterValues}))
            //Format data to ChartJS datasets form
            let labels = asObject.groupingsDown.groupings.map((g) => g.label)
            let numLab = labels.length
            let datasets = {}
            let cin = 0
            asObject.groupingsDown.groupings.forEach((g) => {
              cin = 0
              let key = g.key
              //has subgrouping
              if(g.groupings){
                g.groupings.forEach((g2) => {
                  let val = asObject.factMap[g2.key+'!T'].aggregates[0].value
                  if(!val) val = 0

                  if(!datasets[g2.key]) {
                    let idata = []
                    //Fill data with zeroes
                    for(let i = 0; i < numLab; i++) {
                      idata.push(0)
                    }
                    datasets[g2.key] = {
                      data: idata,
                      label: g2.label,
                      backgroundColor: this._colors[cin]
                    }
                  }
                  // eslint-disable-next-line radix
                  datasets[g2.key].data[parseInt(key)] = val
                  cin++
                })
              }
            })


            //single grouping
            if(Object.values(datasets).length === 0) {
              datasets['0'] = {
                data: Object.values(asObject.factMap).filter(f => f.rows !== null).map(f => f.aggregates[0].value).reverse(),
                label: asObject.reportMetadata.name,
                backgroundColor: this._colors[0]
              }
            }


            let dat = {
              datasets: Object.values(datasets),
              labels: labels
            }
            JSON.stringify(dat)
            return dat
          } catch(e) {
            console.log('Report data failed')
            console.log(JSON.stringify(e))
            this.error = e.body.message
          }
         
          return null
        } else if (this.aggregatedQuery) {
            let asObject = JSON.parse(await runQuery({query: this.aggregatedQuery}))
            console.log('Aggretersult ', asObject)
            //Format data to ChartJS datasets form
            let labels = new Set()
            let datasets = {}
            let cin = 0

            //One dataset for each value
            this.aggregatedQueryDatasets.forEach(vls => {
              datasets[vls.field] = Object.assign({}, vls)

              datasets[vls.field].data = []
              if(!datasets[vls.field].backgroundColor) {
                datasets[vls.field].backgroundColor = this._colors[cin]
              }
              if(!datasets[vls.field].borderColor) {
                datasets[vls.field].borderColor = this._colors[cin]
              }

              cin++


            })

            asObject.forEach(aggRes => {
              
              labels.add(aggRes[this.aggregatedQueryGroupField.field])
              this.aggregatedQueryDatasets.forEach(vls => {
                datasets[vls.field].data.push(aggRes[vls.field])
              })
              
            })

            let dat = {
              datasets: Object.values(datasets),
              labels: [...labels]
            }
            JSON.stringify(dat)
            return dat
        }

        return null
    }

    async renderChart() {
        console.log('omChart -> renderChart called')
        let conf = await this.getConfig()
      
        const canvas = document.createElement('canvas')
        if(this.width){
          canvas.width = this.width
          canvas.style.width = this.width
        }
        if(this.height){
          canvas.height = this.height
          canvas.style.height = this.height
        }

        this.template.querySelector('div.chart').appendChild(canvas)
        const ctx = canvas.getContext('2d')
        this.chart = new window.Chart(ctx, conf)
    }

    onChartClick(evt) {
      let activePoints = this.chart.getElementAtEvent(evt);
      let label = null
      let value = null
      if(activePoints.length > 0) {
        let clickedDatasetIndex = activePoints[0]._datasetIndex;
        let clickedElementIndex = activePoints[0]._index;
        let clickedDatasetPoint = this.chart.data.datasets[clickedDatasetIndex];
        label = clickedDatasetPoint.label;
        value = clickedDatasetPoint.data[clickedElementIndex];
      }

      console.log('Clicked on ' + label + ' ' + value);

    }

    /**
     * Retrives fresh info and updates chart
     */
    @api
    async update() {
      console.log('omChart -> update chart called')
      let conf = await this.getConfig()

      if(this.chart) {
        this.chart.data = conf.data
        this.chart.update()
      }
  }

    renderedCallback() {
      console.log('omChart -> renderedCallback called')

        if (this.chartjsInitialized) {
            return
        }
        this.chartjsInitialized = true

        loadScript(this, chartjs)
            .then(() => {
                this.renderChart()
            })
            .catch(error => {
                this.error = error
            })
    }
}