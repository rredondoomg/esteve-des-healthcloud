/**
 * It renders an UI that allows to manage the content settings
 * @module OmContentSetting
 */
import { LightningElement, track } from 'lwc';
import contentsr from '@salesforce/resourceUrl/contentsr';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { deleteRecord } from 'lightning/uiRecordApi';
import getAuthURL from '@salesforce/apex/OM_ContentService.getAuthorizationEndpoint';
import signOutApex from '@salesforce/apex/OM_ContentSettingsCtrl.signOut';
import checkLoginStatus from '@salesforce/apex/OM_ContentSettingsCtrl.checkLoginStatus';
import checkValidQuery from '@salesforce/apex/OM_ContentSettingsCtrl.checkValidQuery';
import getSharepointSiteId from '@salesforce/apex/OM_ContentService.getSharepointSiteId';
import listSharepointDrives from '@salesforce/apex/OM_ContentService.listSharepointDrives';
import Id from '@salesforce/user/Id';
/** Import labels */
import omAddnewsetting from '@salesforce/label/c.omAddnewsetting';
import omCreateanewconte from '@salesforce/label/c.omCreateanewconte';
import omContentSettings from '@salesforce/label/c.omContentSettings';
import omSelectaprovider from '@salesforce/label/c.omSelectaprovider';
import omBack from '@salesforce/label/c.omBack';
import omRequiredfields from '@salesforce/label/c.omRequiredfields';
import omYouhavetosignou from '@salesforce/label/c.omYouhavetosignou';
import omSignIn from '@salesforce/label/c.omSignIn';
import omFillinthefields from '@salesforce/label/c.omFillinthefields';
import omAdditionalfield from '@salesforce/label/c.omAdditionalfield';
import omMaincustommappi from '@salesforce/label/c.omMaincustommappi';
import omAddspecificsett from '@salesforce/label/c.omAddspecificsett';
import omFinish from '@salesforce/label/c.omFinish';
import omFormat from '@salesforce/label/c.omFormat';
import omAddedproperties from '@salesforce/label/c.omAddedproperties';
import omSpecificsetting from '@salesforce/label/c.omSpecificsetting';
import omObjectcriteria from '@salesforce/label/c.omObjectcriteria';
import omSelectobject from '@salesforce/label/c.omSelectobject';
import omCriteriaSOQLfo from '@salesforce/label/c.omCriteriaSOQLfo';
import omCheckquery from '@salesforce/label/c.omCheckquery';
import omCustomsettings from '@salesforce/label/c.omCustomsettings';
import omAddingnewproper from '@salesforce/label/c.omAddingnewproper';
import omCreatenew from '@salesforce/label/c.omCreatenew';
import omSave from '@salesforce/label/c.omSave';
import omEditproperty from '@salesforce/label/c.omEditproperty';
import omAddproperty from '@salesforce/label/c.omAddproperty';
import omEditcustomsetti from '@salesforce/label/c.omEditcustomsetti';
import omAddcustomsettin from '@salesforce/label/c.omAddcustomsettin';
import omSuccess from '@salesforce/label/c.omSuccess';
import omSIGNEDITCORRECT from '@salesforce/label/c.omSIGNEDITCORRECT';
import omThenewsettingsh from '@salesforce/label/c.omThenewsettingsh';
import omErrordeletingre from '@salesforce/label/c.omErrordeletingre';
import omErrorsigningout from '@salesforce/label/c.omErrorsigningout';
import omTryagain from '@salesforce/label/c.omTryagain';
import omExplorerSettings from '@salesforce/label/c.omExplorerSettings';
import omSettingsTenantDriveList from '@salesforce/label/c.omSettingsTenantDriveList';
import omGlobalCustomProperties from '@salesforce/label/c.omGlobalCustomProperties';
import omMainSettings from '@salesforce/label/c.omMainSettings';
import omObjectSettings from '@salesforce/label/c.omObjectSettings';


import {default as omContentSettingsSharepointColumns} from './omContentSettingsSharepointColumns.html'

export default class OmContentSetting extends LightningElement {

  omContentSettingsSharepointColumns = omContentSettingsSharepointColumns;

  /** Private vars */
  onloadrecordeditform = true;
  objectChildCreated = false;
  signingIt = false;
  userId = Id;

  /** Labels */
  label = {
    omAddnewsetting,
    omCreateanewconte,
    omContentSettings,
    omSelectaprovider,
    omBack,
    omRequiredfields,
    omYouhavetosignou,
    omSignIn,
    omFillinthefields,
    omAdditionalfield,
    omMaincustommappi,
    omAddspecificsett,
    omFinish,
    omFormat,
    omAddedproperties,
    omSpecificsetting,
    omObjectcriteria,
    omSelectobject,
    omCriteriaSOQLfo,
    omCheckquery,
    omCustomsettings,
    omAddingnewproper,
    omCreatenew,
    omSave,
    omEditproperty,
    omAddproperty,
    omEditcustomsetti,
    omAddcustomsettin,
    omSuccess,
    omSIGNEDITCORRECT,
    omThenewsettingsh,
    omErrordeletingre,
    omErrorsigningout,
    omTryagain,
    omExplorerSettings,
    omSettingsTenantDriveList,
    omGlobalCustomProperties,
    omMainSettings,
    omObjectSettings
  };

  /** Static resources */
  GoogleDrive = contentsr + '/images/GoogleDrive.png';
  MicrosoftSharePoint = contentsr + '/images/MicrosoftSharePoint.png';
  MicrosoftOneDrive = contentsr + '/images/MicrosoftOneDrive.png';

  /**
   * If settings modal should be rendered
   * @private
   * @type boolean
   * @default false
   */
  @track showNewSettingModal = false;

  /**
   * Step to show in the settings modal
   * @private
   * @type integer
   * @default 0
   */
  @track step = 0;

  /**
   * If the modal should be render in edit mode
   * @private
   * @type boolean
   * @default false
   */
  @track editMode = false;

  @track loading = false;

  get hiddenIfLoading () {
    return this.loading ? 'slds-hide' : 'slds-show'
  }

  /**
   * Store the provide logo to show in the modal header (check setSelectedProvider())
   * @private
   * @type string
   * @default ''
   */
  @track selectedProvider = '';

  /**
   * Main object to store the main setting
   * @private
   * @type object
   */
  @track record = {
    id: '',
    OM_Provider__c: null,
    OM_Auth_type__c: '',
    OM_Site_id__c: '',
    OM_Drive_id__c: '',
    OM_Redirect_uri__c: '',
  };

  /**
   * If object settings section should be rendered
   * @private
   * @type boolean
   * @default false
   */
  @track objectSetting = false;

  /**
   * If user is logged in the specific provider
   * @private
   * @type boolean
   * @default false
   */
  @track logged = false;

  /**
   * If required fields have been filled
   * @private
   * @type boolean
   * @default false
   */
  @track filledRequiredFields = false;

  /**
   * If it exists any pending change, and some buttons should be disabled
   * @private
   * @type boolean
   * @default true
   */
  @track pendingChanges = true;

  /**
   * It sets the property id is being edited (record-id record-edit-form)
   * @private
   * @type string
   * @default ''
   */
  @track propertyId = '';

  /**
   * It store custom settings for a specific object
   * @private
   * @type object
   */
  @track customSetting = { Id: '', OM_Object__c: '', OM_Criteria__c: '' };

  

  /**
   * List of Sharepoint document libraries
   * @private
   */
  @track sharepointDrives = [];

  /** GETTERS */

  /**
   * If the main settings has beed created is edited
   */
  get existsMainSettings() {
    return this.record.id !== ''
  }

  /**
   * If the selected existes (first setep on creation or edtiion)
   */
  get existProvider() {
    return this.record.OM_Provider__c !== null
  }

  /**
   * Tabset visibility class. Cannot use if:true because record-edit-form should be rendered to fetch info when editing
   */
  get tabSetWrapperClass() {
    return this.record.OM_Provider__c !== null ? 'slds-show' : 'slds-hide'
  }

  /**
   * If provider is Microsoft. Needed to fill aditional fields
   */
  get isMicrosoft() {
    return (
      this.record.OM_Provider__c === 'MicrosoftSharePoint' ||
      this.record.OM_Provider__c === 'MicrosoftOneDrive'
    );
  }

  /**
   * If provider is Sharepoint. Needed to fill aditional fields
   */
  get isSharePoint() {
    return this.record.OM_Provider__c === 'MicrosoftSharePoint';
  }

  /**
   * If provider is Onedrive. Needed to fill aditional fields
   */
  get isOneDrive() {
    return this.record.OM_Provider__c === 'MicrosoftOneDrive';
  }

  /**
   * If the alert info should be displayed, notifying the user to logout prior editing auth fields
   */
  get showEditLogoutInfo() {
    return this.editMode && this.logged;
  }

  get settingFilter() {
    return "OM_Parent_setting__c = '" + this.record.id + "'";
  }
  get childSettingsFilter() {
    return "OM_Parent_setting__c = '" + this.record.id + "'";
  }

  get objectCustomProperyFilter() {
    return "OM_Parent_setting__c = '" + this.customSetting.Id + "'";
  }

  get customSettingBtnLabel() {
    return !this.editMode ? this.label.omCreatenew : this.label.omSave;
  }
  get addPropertyBtnLabel() {
    return this.propertyId
      ? this.label.omEditproperty
      : this.label.omAddproperty;
  }
  get addCustomSettingBtnLabel() {
    return this.customSetting.Id
      ? this.label.omEditcustomsetti
      : this.label.omAddcustomsettin;
  }
  get formatLabel() {
    return (
      this.label.omFormat + ': https://subdomain.sharepoint.com/sites/sitename'
    );
  }

  get notFilledRequiredFields() {
    return !this.filledRequiredFields;
  }
  get notLoggedAndFilledRequiredFields() {
    return !this.logged && this.filledRequiredFields;
  }
  get notReadyToSave() {
    return this.notFilledRequiredFields;
  }
  get disabledAuthParams() {
    return this.logged || this.signingIt;
  }
  get notSelectedObject() {
    return !this.customSetting.OM_Object__c;
  }



  /*get showPropertiesSection() {
    return (
      !this.objectSetting || (this.objectSetting && this.objectChildCreated)
    );
  }*/

  @track showPropertiesSection = false

  connectedCallback() {
    /**
     * Listen to the message when the provider login is done.
     * postMessage will come from OM_OAuthCallback visualforce
     */
    window.addEventListener('message', this.signedIt.bind(this));
  }

  renderedCallback() {
    this.record.OM_Redirect_uri__c = this.record.OM_Redirect_uri__c
      ? this.record.OM_Redirect_uri__c
      : window.location.href
          .split('/lightning')[0]
          .replace(
            '.lightning.force.com',
            '--c.visualforce.com/apex/OM_OAuthCallback'
          );
    this.checkRequiredFields();
  }

  toggleModal() {
    this.loading = true;
    this.showNewSettingModal = !this.showNewSettingModal;
    if (!this.showNewSettingModal) {
      this.resetForm();
      this.showPropertiesSection = false;
      this.template.querySelector('[data-id="settingsListView"]').refreshData();
    }
  }

  /** Steps methods */
  goHome() {
    if (this.editMode) {
      this.resetForm();
      this.toggleModal();
    } else {
      this.resetForm();
    }
  }

  checkRequiredFields() {
    this.filledRequiredFields =
      !!this.record.OM_Name__c &&
      !!this.record.OM_Client_id__c &&
      !!this.record.OM_Client_secret__c &&
      !!this.record.OM_Redirect_uri__c &&
      !!this.record.OM_Scope__c &&
      !!this.record.OM_Auth_type__c &&
      ((this.isSharePoint &&
        (!!this.record.OM_Site_Url__c || this.record.OM_Site_id__c)) ||
        !this.isSharePoint) &&
      ((this.isMicrosoft && !!this.record.OM_Tenant__c) || !this.isMicrosoft);
  }

  setSelectedProvider() {
    switch (this.record.OM_Provider__c) {
      case 'GoogleDrive':
        this.selectedProvider = this.GoogleDrive;
        break;
      case 'MicrosoftSharePoint':
        this.selectedProvider = this.MicrosoftSharePoint;
        break;
      case 'MicrosoftOneDrive':
        this.selectedProvider = this.MicrosoftOneDrive;
        break;
      default:
        break;
    }
  }

  setProvider(evt) {
    this.step = 1;
    this.record.OM_Provider__c = evt.currentTarget.dataset.provider;
    this.setSelectedProvider();
  }

  setFieldValue(evt) {
    if (evt.detail.checked !== undefined) {
      this.record[evt.currentTarget.dataset.field] = evt.detail.checked;
    } else {
      this.record[evt.currentTarget.dataset.field] = evt.detail.value;
    }
    this.checkRequiredFields();
  }

  editCustomSetting(evt) {
    
    this.editMode = true;
    this.record.id = evt.detail.Id;
    this.step = 1;
    this.toggleModal();
  }

  /************************************************************
   * HANDLE METHODS FOR RECORD EDIT FORM OF SETTINGS
   ************************************************************/

  /**
   * When moving to other tab that is not for object setting creation/edition, hide the form
   */
  onTabActive () {
    this.objectSetting = false
  }
  /**
   * When the form is loaded, the record fields are stored in @track record
   * This allows to get some fields that are not showed in input-fields as OM_Provider
   * Also it sets some initial vars
   * @param {*} evt
   */
  async onLoadSettingsForm(evt) {
    
    if (this.editMode && this.onloadrecordeditform) {
     
      let recordId = Object.keys(evt.detail.records).length > 0 ? Object.keys(evt.detail.records)[0] : ''
      let settingsName = evt.detail.records[recordId].fields.OM_Name__c.value
      this.logged = await checkLoginStatus({ settingsName: settingsName});

      
      Object.keys(evt.detail.records[recordId].fields).forEach((key) => {
        this.record[key] = evt.detail.records[recordId].fields[key].value;
      });
      
      this.onloadrecordeditform = false;
      this.pendingChanges = false;
      this.checkRequiredFields();
      this.setSelectedProvider();

      if(this.logged) {
        await this.getDrivesList()
      }
    }

    this.loading = false
  }

  /**
   * Submit the settings form setting the selected provider
   * @param {*} evt
   */
  handleSubmit(evt) {
    evt.preventDefault();
    let fields = evt.detail.fields;
    if (this.record.OM_Provider__c !== null) {
      fields.OM_Provider__c = this.record.OM_Provider__c;
    }
    this.template.querySelector('[data-id="settingsForm"]').submit(fields);
  }

  /**
   * When a settings has been saved the settings list view is refreshed and fields are reset
   * @param {*} evt
   */
  handleSuccess(evt) {
    console.log(JSON.stringify(evt.detail));
    Object.keys(evt.detail.fields).forEach((key) => {
      this.record[key] = evt.detail.fields[key].value;
    });
    this.record.id = evt.detail.id;

    if (!this.signingIt) {
      this.step = 2;
      this.pendingChanges = false;
      this.dispatchEvent(
        new ShowToastEvent({
          title: this.label.omSuccess,
          message: this.label.omThenewsettingsh,
          variant: 'success',
        })
      );
      this.template.querySelector('[data-id="settingsListView"]').refreshData();
      const inputFields = this.template.querySelectorAll(
        '[data-id="settingsForm"] lightning-input-field'
      );
      if (inputFields) {
        inputFields.forEach((field) => {
          field.reset();
        });
      }
    }
  }

  onErrorSettingsForm() {
    this.logged = false;
    this.signingIt = false;
  }

  /************************************************************
   * END HANDLE METHODS FOR RECORD EDIT FORM OF SETTINGS
   ************************************************************/

  /************************************************************
   * HANDLERS FOR CUSTOM SETTING CONFIGURATION FOR A SPECIFIC OBJECT | RECORD EDIT FORM
   ************************************************************/

  /*addSpecificObjSetting() {
    this.objectSetting = true;
    this.addCustomProperties();
  }*/

  onloadCustomSetting(evt) {
    if (evt && evt.detail && evt.detail.records) {
      Object.keys(
        evt.detail.records[Object.keys(evt.detail.records)].fields
      ).forEach((key) => {
        this.customSetting[key] =
          evt.detail.records[Object.keys(evt.detail.records)].fields[key].value;
      });
    }
    //this.addSpecificObjSetting();
  }


  cancelObjectSettingForm() {
    const inputFields = this.template.querySelectorAll(
      '[data-id="customSettingsForm"] lightning-input-field'
    );
    if (inputFields) {
      inputFields.forEach((field) => {
        field.reset();
      });
    }
    this.objectSetting = false;
  }

  submitObjectSettingForm () {
    this.template.querySelector('[data-id="customSettingsFormSubmitBtn"]').click()
  }

  /**
   * Before submit It sets the parent setting
   * @param {*} evt
   */
  async handleCustomSettingSubmit(evt) {
    evt.preventDefault();
    let fields = evt.detail.fields;
    fields.OM_Parent_setting__c = this.record.id;
    fields.OM_Object__c = this.customSetting.OM_Object__c;
    fields.OM_Criteria__c = this.customSetting.OM_Criteria__c;

    if (
      !this.customSetting.OM_Criteria__c ||
      (this.customSetting.OM_Criteria__c && (await this.checkQuery()))
    ) {
      this.template
        .querySelector('[data-id="customSettingsForm"]')
        .submit(fields);
    } else {
      this.dispatchEvent(
        new ShowToastEvent({
          title: 'Error',
          message: 'Check query',
          variant: 'error',
        })
      );
    }
  }

  handleCustomSettingSuccess() {
    if (this.template.querySelector('[data-id="childSettingsListView"]')) {
      this.template
          .querySelector('[data-id="childSettingsListView"]')
          .refreshData()
    }
    const inputFields = this.template.querySelectorAll(
      '[data-id="customSettingsForm"] lightning-input-field'
    );
    if (inputFields) {
      inputFields.forEach((field) => {
        field.reset();
      });
    }
    this.customSetting = { Id: '', OM_Object__c: '', OM_Criteria__c: '' };
    this.objectSetting = false
    // this.step = 1;
  }

  logError(evt) {
    console.error(JSON.stringify(evt.detail));
  }

  /************************************************************
   * END HANDLERS FOR CUSTOM SETTING CONFIGURATION FOR A SPECIFIC OBJECT | RECORD EDIT FORM
   ************************************************************/

  /************************************************************
   * HANDLERS FOR CUSTOM PROPERTIES RECORD EDIT FORM
   ************************************************************/
  cancelPropertyForm() {
    const inputFields = this.template.querySelectorAll(
      '[data-id="propertiesForm"] lightning-input-field'
      );
      if (inputFields) {
        inputFields.forEach((field) => {
          field.reset();
        });
      }
      
    this.propertyId = '';
    this.showPropertiesSection = false;
  }

  handleSummitProperty(evt) {
    evt.preventDefault();
    let fields = evt.detail.fields;
    // Relate with main setting or with the current object setting selected
    fields.OM_Parent_setting__c = this.objectSetting ? this.customSetting.Id : this.record.id;
    this.template.querySelector('[data-id="propertiesForm"]').submit(fields);
  }

  handleSuccessProperty() {
    let globalPs = this.template.querySelector('[data-id="propertiesListView"]')
    if(globalPs) globalPs.refreshData();

    let objPs = this.template.querySelector('[data-id="objectPropertiesListView"]')
    if(objPs) objPs.refreshData();

    const inputFields = this.template.querySelectorAll(
      '[data-id="propertiesForm"] lightning-input-field'
    );
    this.propertyId = '';
    if (inputFields) {
      inputFields.forEach((field) => {
        field.reset();
      });
    }

    this.showPropertiesSection = false;
  }

  closePropertiesSection() {
    this.showPropertiesSection = false;

  }

  /************************************************************
   * END HANDLERS FOR CUSTOM PROPERTIES RECORD EDIT FORM
   ************************************************************/

  /***************************
   * LOGIN METHODS
   ***************************/

  signIn() {
    if (this.signingIt) {
      getAuthURL({ settingsName: this.record.OM_Name__c, userId: this.userId })
        .then((res) => {
          window.open(res);
          this.signingIt = false;
        })
        .catch((error) => {
          this.logError(error);
          this.signingIt = false;
        });
    } else {
      if (!this.signingIt) {
        this.signingIt = true;
        //this.template.querySelector('[data-id="settingsForm"]') .submit(this.record);
        this.template.querySelector('[data-id="settingsFormSubmit"]').click();
        
        // eslint-disable-next-line
        setTimeout(() => this.signIn(), 500);
      }
    }
  }

  signOut() {
    signOutApex({ settingsId: this.record.id })
      .then((res) => {
        if (res) {
          this.logged = false;
        } else {
          this.dispatchEvent(
            new ShowToastEvent({
              title: this.label.omErrorsigningout,
              message: this.label.omTryagain,
              variant: 'error',
            })
          );
        }
      })
      .catch((error) => {
        this.dispatchEvent(
          new ShowToastEvent({
            title: this.label.omErrorsigningout,
            message: error.body.message,
            variant: 'error',
          })
        );
      });
  }

  async getSiteId() {
    this.record.OM_Site_id__c = await getSharepointSiteId({
      settingsName: this.record.OM_Name__c,
      siteUrl: this.record.OM_Site_Url__c,
    });
  }

  async getDrivesList() {
    this.sharepointDrives = await listSharepointDrives({
      settingsName: this.record.OM_Name__c,
    });
  }

  /**
   * Columns to display sharepoint drives
   * @private
   */
  @track sharepointDrivesColumns = [
    { fieldName: 'id', label: 'Id' , style: 'width: 60%'},
    { fieldName: 'name', label: 'Name' }
  ];

  /**
   * Columns to display sharepoint drive list columns
   * @private
   */
  @track sharepointListColsColumns = [
    { fieldName: 'name', label: 'API' },
    { fieldName: 'displayName', label: 'DisplayName' },
    { fieldName: 'custom', label: 'Is custom', type: 'checkbox', filter: true }
  ];

  sharepointColumnsData = (row) => {
    return {
      columns: this.sharepointListColsColumns,
      rows: row.item.columns
    }
  }

  handleDriveSelection(ev) {
    this.record.OM_Drive_id__c = ev.detail[0].item.id;
    this.template.querySelector('lightning-input-field[data-field="OM_Drive_id__c"]').value = ev.detail[0].item.id;
  }

  async signedIt() {
    this.logged = true;
    try {
      await this.getSiteId();
    } catch(e) {
      console.error('Cannot retrieve site Id')
    }

    try {
      await this.getDrivesList();
    } catch(e) {
      console.error('Cannot retrieve dirve list')
    }
    this.dispatchEvent(
      new ShowToastEvent({
        title: this.label.omSuccess,
        message: this.label.omSIGNEDITCORRECT,
        variant: 'success',
      })
    );
  }


  /***************************
   * END LOGIN METHODS
   ***************************/


  addGlobalCustomProperty(evt) {
    this.showPropertiesSection = true;
  }

  addObjectCustomProperty(evt) {
    this.showPropertiesSection = true;
  }

  editCustomProperty(evt) {
    this.propertyId = evt.detail.Id;
    //this.step = 3;
    this.showPropertiesSection = true;
  }

  createChildCustomSetting(evt) {
    this.customSetting.Id = '';
    this.step = 3;
    this.objectSetting = true;
  }

  editChildCustomSetting(evt) {
    this.customSetting.Id = evt.detail.Id;
    this.step = 3;
    this.objectSetting = true;
  }

  deleteRcrd(evt) {
    let listviewName = evt.currentTarget.dataset.id;
    deleteRecord(evt.detail.Id)
      .then(() => {
        this.dispatchEvent(
          new ShowToastEvent({
            title: this.label.omSuccess,
            message: 'Record deleted',
            variant: 'success',
          })
        );
        this.template
          .querySelector('[data-id="' + listviewName + '"]')
          .refreshData();
      })
      .catch((error) => {
        this.dispatchEvent(
          new ShowToastEvent({
            title: this.label.omErrordeletingre,
            message: error.body.message,
            variant: 'error',
          })
        );
      });
  }

  resetForm() {
    this.step = 0;
    this.record = {
      id: '',
      OM_Provider__c: null,
      OM_Auth_type__c: '',
      OM_Site_id__c: '',
      OM_Drive_id__c: '',
      OM_Redirect_uri__c: '',
    };
    this.customSetting = { Id: '', OM_Object__c: '', OM_Criteria__c: '' };
    this.logged = false;
    this.selectedProvider = '';
    this.objectSetting = false;
    this.objectChildCreated = false;
    this.editMode = false;
    this.onloadrecordeditform = true;
  }

  setSelectedObject(evt) {
    this.customSetting.OM_Object__c = evt.detail.sobject;
  }

  setCriteria(evt) {
    this.customSetting.OM_Criteria__c = evt.detail.value;
  }

  async checkQuery(evt) {
    let check = await checkValidQuery({
      objectName: this.customSetting.OM_Object__c,
      condition: this.customSetting.OM_Criteria__c,
    });
    if (evt) {
      if (check) {
        this.dispatchEvent(
          new ShowToastEvent({
            title: this.label.omSuccess,
            message: 'Query is ok',
            variant: 'success',
          })
        );
      } else {
        this.dispatchEvent(
          new ShowToastEvent({
            title: 'Error',
            message: 'Query is nok',
            variant: 'error',
          })
        );
      }
    }
    return check;
  }
}