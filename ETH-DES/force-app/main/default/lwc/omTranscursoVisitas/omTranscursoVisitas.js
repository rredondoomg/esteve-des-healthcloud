import getTratamientosVisita from '@salesforce/apex/OM_TranscursoVisitas_Controller.getTratamientosVisita'
import getCausaSustitucion from '@salesforce/apex/OM_TranscursoVisitas_Controller.getCausaSustitucion'
// import consultaFungiblesWS from '@salesforce/apex/OM_TranscursoVisitas_Controller.callFungiblesWS';
// import deleteFungible from '@salesforce/apex/OM_TranscursoVisitas_Controller.deleteFungible';
// import editFungible from '@salesforce/apex/OM_TranscursoVisitas_Controller.editFungible';
import insertNewFungible from '@salesforce/apex/OM_TranscursoVisitas_Controller.insertNewFungible';
import isVisitaVirtual from '@salesforce/apex/OM_TranscursoVisitas_Controller.isVisitaVirtual';
import modifyEquipoSuministrado from '@salesforce/apex/OM_TranscursoVisitas_Controller.modifyEquipoSuministrado';
import insertNewEquipoSuministrado from '@salesforce/apex/OM_TranscursoVisitas_Controller.insertNewEquipoSuministrado';
import resetEquipoConLectura from '@salesforce/apex/OM_TranscursoVisitas_Controller.resetEquipoConLectura';
import retirarEquipoSuministrado from '@salesforce/apex/OM_TranscursoVisitas_Controller.retirarEquipoSuministrado';
import retirarFungibleSuministrado from '@salesforce/apex/OM_TranscursoVisitas_Controller.retirarFungibleSuministrado';
import checkStockEquipo from '@salesforce/apex/OM_TranscursoVisitas_Controller.checkStockEquipo';
import checkStockFungible from '@salesforce/apex/OM_TranscursoVisitas_Controller.checkStockFungible';
import activarTratamientoRevisado from '@salesforce/apex/OM_TranscursoVisitas_Controller.activarTratamientoRevisado';
import reinstalarEquipoSuministrado from '@salesforce/apex/OM_TranscursoVisitas_Controller.reinstalarEquipoSuministrado'

import { CurrentPageReference, NavigationMixin } from 'lightning/navigation';


import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import { LightningElement, api, track, wire } from 'lwc';
import {registerListener} from 'c/pubsub';

const actions = [
    { label: 'Edit', name: 'edit' },
    { label: 'Delete', name: 'delete' },
];

const columns = [
    {  
        label: "Name",  
        fieldName: "recordLink",  
        type: "url",  
        typeAttributes: { label: { fieldName: "Name", type: 'text', sortable: false, hideDefaultActions:true }, target: "_self" }  
       },
    // { label: 'Fungibles Name', fieldName: 'Name', type: 'text', sortable: false, hideDefaultActions:true },
    { label: 'Cantidad', fieldName: 'Cantidad__c', type: 'text', sortable: false, hideDefaultActions:true },
    { label: 'Marca - Modelo', fieldName: 'Marca_Modelo__c', type: 'text', sortable: false, hideDefaultActions:true },
    {
        type: 'action',
        typeAttributes: { rowActions: actions },
    },
];

    

export default class OmTranscursoVisitas extends NavigationMixin (LightningElement) {
    
    @api recordId;
    @track tratamientosVisita = {};
    @track loadedTratamientosVisita = false;
    
    @track showModalConfirmacionEquipoDelete = false;
    @track showModalConfirmacionFungibleDelete = false;
    @track showModalConfirmacionEquipoReinstalar = false;
    @track equipoSuninistradoToDeleteId = '';
    @track equipoSuministradoToReinstall = '';
    @track fungibleSuninistradoToDeleteId = '';
    @track visitaTratamientoRetiradaEquipo = '';    
    // @track visitaTratamientoReinstalacionEquipo = '';    
    @track showModalConfirmacionReset = false;
    @track equipoSuministradoToResetId = '';
    @track equiposSuministradosToUpdatePrincipal = {};
    @track equiposSuministradosToUpdateTelematico = {};
    @track equiposSuministradosInsertLecturaActual = {};
    @track loadedData = false;

    @track equipoSuministradoToResetValue;
    
    visitaTratamientoToAdd;
    visitaId;
    fungibleId;
    // tratamientoId;
    equipoid;
    cantidadFungible;
    // equipoSuministrado;
    numeroLote;
    pendiente;
    @track isVisitaVirtual;
    addingFungible = false

    showAddFungibleModal = false;
    showAddEquipoModal = false;

    @track readOnlyMode = false;
    @track intervencionNoProgramada = false;

    @track refreshEquipos = false;
    
    @track codigoEquipoSuministrado;
    @track esPrincipal;
    @track esTelematico;
    @track codigoEquipoSuministrado;

    @track causaSustitucionValue = "";
    @track causaSustitucionOptions = [];

    @track casoFungible;

    columns = columns;
    
    activeSectionMessage = '';
    activeSections = ['A', 'B', 'C'];

    get disabledVirtual(){
        return this.readOnlyMode || this.isVisitaVirtual
    }

    @wire(CurrentPageReference) pageRef

        @wire(isVisitaVirtual, {recordId : '$recordId'})
        isVisitaVirtualJS(value) {
            const { data, error } = value;
            // console.log('Llamada es visita virtual: '+data);
            this.isVisitaVirtual = data;

            if (error) {
                console.log('(error isVisitaVirtualJS---> ' + JSON.stringify(error));
                this.isVisitaVirtual = false;
            }
            console.log('Es visita virtual vale: '+this.isVisitaVirtual);

        };


        openVFScanner(event) {
            this[NavigationMixin.Navigate]({
                type: 'standard__webPage',
                attributes: {
                    url: '/apex/om_QRScannerVF?IdTratamientoVisita='+event.target.dataset.visitaid+'&serviceAppointment='+this.recordId
                }
            }).then(generatedUrl => {
                window.open(generatedUrl);
            });
        }

    connectedCallback(){
        registerListener('disableTranscursoVisita', this.disableTranscursoVisita, this)
        this.getTratamientosVisita();
        this.getCausaSustitucion();
        
    }

    disableTranscursoVisita(evt){
        this.readOnlyMode = true;
    }

    activarTratamientoRevisado(tratamientoVisita, idFormulario, mapaResetLectura){
        activarTratamientoRevisado({idVisitaTratamiento : tratamientoVisita, idFormulario : idFormulario, mapaResetLectura : mapaResetLectura})
        .then(response => {
            console.log('Se marca como revisado el tratamiento');
            eval("$A.get('e.force:refreshView').fire()");
           
        })
        .catch(error => {
            console.log('ERROR activarTratamientoRevisado '+JSON.stringify(error));
            // this.dispatchEvent(new ShowToastEvent({ title: '', message: 'Error al actualizar los equipos suministrados', variant: 'error'}));

        })
    }

    checkStockEquipo(){
        checkStockEquipo({equipoCodigoEth : this.codigoEquipoSuministrado, serviceAppointment : this.recordId, tratamientoToAdd : this.visitaTratamientoToAdd})
        .then(response => {
            console.log('Respuesta APEX /stock-equipo: '+JSON.stringify(response));

            if(response['true']){
                this.dispatchEvent(new ShowToastEvent({ title: '', message: response['true'], variant: 'error'}));
            }else{
                console.log('Mensaje del toast: '+response['false']);
                console.log('Datos atlas: '+JSON.stringify(response['datos']));
                // this.dispatchEvent(new ShowToastEvent({ title: '', message: response['false'], variant: 'success'}));
                this.insertNewEquipoSuministrado(response['datos']);
            }
           
        })
        .catch(error => {
            console.log('ERROR checkStockEquipo '+JSON.stringify(error));
            // this.dispatchEvent(new ShowToastEvent({ title: '', message: 'Error al actualizar los equipos suministrados', variant: 'error'}));

        })
    }

    checkStockFungible(){
        console.log('Estoy en checkStockFungible');

        checkStockFungible({cantidad : this.cantidadFungible, idFungible : this.fungibleId, tratamientoVisita : this.visitaTratamientoToAdd, serviceAppointment : this.recordId})
        .then(response => {
            console.log('Respuesta APEX /stock-fungible: '+JSON.stringify(response));

            if(response['true']){
                this.dispatchEvent(new ShowToastEvent({ title: '', message: response['true'], variant: 'error'}));
            }else{
                this.insertNewFungible();
                // this.dispatchEvent(new ShowToastEvent({ title: '', message: 'SE CREARIA EL FUNGIBLE', variant: 'success'}));

            }
           
        })
        .catch(error => {
            console.log('ERROR checkStockFungible '+JSON.stringify(error));
            // this.dispatchEvent(new ShowToastEvent({ title: '', message: 'Error al actualizar los equipos suministrados', variant: 'error'}));

        })
    }

    handleClickEquipoSum(event){
        console.log('Cambio el elemento: '+event.target.name);
        console.log('checked: '+event.target.checked);
        console.log('Id donde lo cambio: '+event.target.dataset.key);
        // console.log('Antes mapa Ppal '+this.equiposSuministradosToUpdatePrincipal);
        // console.log('Antes mapa Tel '+this.equiposSuministradosToUpdateTelematico);
        switch (event.target.name) {
            case 'equipoPrincipal':
                console.log('Dentro del switch');
                this.equiposSuministradosToUpdatePrincipal[event.target.dataset.key] = event.target.checked;
                break;
            case 'equipoTelematico':
                console.log('Dentro del switch');
                this.equiposSuministradosToUpdateTelematico[event.target.dataset.key] = event.target.checked;
                break;
            default:
                console.log('Dentro del switch en lectura actual');
                if(event.target.value != null && event.target.value != 0){
                    this.equiposSuministradosInsertLecturaActual[event.target.dataset.key] = event.target.value;
                }else{
                    delete this.equiposSuministradosInsertLecturaActual[event.target.dataset.key];
                }
                break;
        }
        
        console.log('Mapa Ppal '+JSON.stringify(this.equiposSuministradosToUpdatePrincipal));
        console.log('Mapa Tel '+JSON.stringify(this.equiposSuministradosToUpdateTelematico));
        console.log('Mapa lecturas '+JSON.stringify(this.equiposSuministradosInsertLecturaActual));
        // equiposSuministradosToUpdate[event.target.dataset.key] = 
    }

    cancelarCambiosEquipos(event){
        this.refreshEquipos = false;
        this.getTratamientosVisita();
        this.dispatchEvent(new ShowToastEvent({ title: '', message: 'Cambios cancelados', variant: 'success'}));
        this.equiposSuministradosToUpdatePrincipal = {};
        this.equiposSuministradosToUpdateTelematico = {};
        this.equiposSuministradosInsertLecturaActual = {};
    }

    guardarEquipos(event){
        this.modifyEquipoSuministrado(event);
    }

    modifyEquipoSuministrado(event){
        debugger
        this.visitaTratamientoToAdd = event.target.dataset.visitaid;
        modifyEquipoSuministrado({serviceAppointment : this.recordId,
                                equiposPpalesMap : this.equiposSuministradosToUpdatePrincipal, 
                                equiposTelematicosMap :  this.equiposSuministradosToUpdateTelematico, 
                                equiposLecturasMap : this.equiposSuministradosInsertLecturaActual})
        .then(response => {
            // console.log('tamanio: '+response.visTrat.length);
            console.log('RESPUESTA DE ERROR: '+JSON.stringify(response));
            if(response == false){ //NO HAY ERRORES DE LECTURAS INTRODUCIDAS
                this.getTratamientosVisita();
                console.log('Success modifyEquipoSuministrado ');
                this.dispatchEvent(new ShowToastEvent({ title: '', message: 'Equipos suministrados actualizados', variant: 'success'}));
                this.activarTratamientoRevisado(this.visitaTratamientoToAdd, null, null); 
            }else{
                this.dispatchEvent(new ShowToastEvent({ title: '', message: 'Las lecturas actuales deben ser superiores a las anteriores.', variant: 'warning'}));
            }

           
        })
        .catch(error => {
            console.log('ERROR modifyEquipoSuministrado '+JSON.stringify(error));
            this.dispatchEvent(new ShowToastEvent({ title: '', message: 'Error al actualizar los equipos suministrados', variant: 'error'}));

        })
    }

    deleteRow(row) {
        const { id } = row;

        // console.log('Id elemento a borrar: '+row.id);
        const index = this.findRowIndexById(id);
        if (index !== -1) {
            this.data = this.data
                .slice(0, index)
                .concat(this.data.slice(index + 1));
        }
    }

    findRowIndexById(id) {
        let ret = -1;
        this.data.some((row, index) => {
            if (row.id === id) {
                ret = index;
                return true;
            }
            return false;
        });
        return ret;
    }

    editRow(row) {
        this.record = row;
    }

    insertNewFungible() {
        console.log('Id visita '+this.visitaId);
        console.log('id fungible '+this.fungibleId);
        console.log('id cantidad '+this.cantidadFungible);
        console.log(' pendiente '+this.pendiente);
        // if(!this.pendiente){
        //     this.pendiente = false;
        // }
        if(this.isVisitaVirtual){
            this.pendiente = true;
        }else{
            this.pendiente = false;
        }
        insertNewFungible({ recordId : this.visitaTratamientoToAdd, fungibleId : this.fungibleId, 
            cantidad : this.cantidadFungible, tratamiento : this.tratamientoId, pendiente : this.pendiente, serviceAppointment : this.recordId, lote : this.numeroLote  })
            .then(response => {
                console.log('Success insert fungible');

                if(response["false"] == ''){
                    // this.refreshEquipos = false;    
                    this.getTratamientosVisita();
                    this.showAddFungibleModal = false;
                    this.dispatchEvent(new ShowToastEvent({ title: '', message : 'Fungible añadido correctamente', variant: 'success'}));  
                    this.activarTratamientoRevisado(this.visitaTratamientoToAdd, null, null); 
                    this.addingFungible = false;
                }else{
                    console.log('Entra al ELSE de coger tratamientos y toast');  
                    this.dispatchEvent(new ShowToastEvent({ title: '', message: response["true"], variant: 'error'}));   
                }    

            })
            .catch(error => {
                console.log('Error insert fungible '+JSON.stringify(error));
                this.error = error;
                this.dispatchEvent(new ShowToastEvent({ title: '', message: 'Error al insertar fungible : '+error, variant: 'error'}));   
                this.addingFungible = false;
            });

    }

    // deleteFungibleJS(id, row) {
    //     deleteFungible({ recordId : id})
    //         .then(result => {
    //             console.log('Success deleteFungibleJS');
    //             // this.deleteRow(row);

    //             this.showConfirmationDeleteModal = false;
    //             this.showNotification('Success', 'Fungible deleted correctly', 'success');
    //             this.getFungiblesVisitaJS(false);

    //         })
    //         .catch(error => {
    //             console.log('Error deleteFungibleJS '+JSON.stringify(error));
    //             this.error = error;
    //         });
    // }

    // consultaFungiblesWSJS(){
    //     if(this.fungibleId == undefined){
    //         this.showNotification('Error', 'Es obligatorio seleccionar un fungible.', 'error');
    //     }else if(this.cantidadFungible == undefined || this.cantidadFungible == "" || this.cantidadFungible == 0){
    //         this.showNotification('Error', 'Introduce una cantidad mayor que 0.', 'error');
    //     }else if(!this.isVisitaVirtual && (this.numeroLote == undefined || this.numeroLote == "")){
    //         this.showNotification('Error', 'Si la visita no es virtual, el número de lote es obligatorio.', 'error');
    //     }else{
    //         consultaFungiblesWS({isUpdate : false, recordId : this.visitaId, fungibleId : this.fungibleId, cantidad : this.cantidadFungible,
    //             numeroLote : this.numeroLote, pendiente : this.pendiente})
    //         .then(result => {
    //             console.log('this.fungibleId vale '+this.fungibleId);
    //             console.log('this.cantidadFungible vale '+this.cantidadFungible);
    //             console.log('this.numeroLote vale '+this.numeroLote);

    //             var responseWSFungibles = {};
    //             responseWSFungibles = result;
    //             console.log('Respuesta apex '+JSON.stringify(responseWSFungibles));
    //             if (200 in responseWSFungibles){
    //                 if(responseWSFungibles[200].includes('PROCESS KO Stock Insuficiente')){
    //                     console.log('Contiene process KO');
    //                     this.showNotification('Error', responseWSFungibles[200], 'error');
    //                 }else{
    //                     console.log('Respuesta de APEX '+JSON.stringify(responseWSFungibles));
    //                     this.showNotification('Success', 'Fungible created correctly', 'success');
    //                     this.clearVariables();
    //                     this.showAddFungibleModal = false;
    //                     this.getFungiblesVisitaJS(false);
    //                 }
    //             }else if(401 in responseWSFungibles){
    //                 this.showNotification('Error', responseWSFungibles[401], 'error');
    //             }else if(500 in responseWSFungibles){
    //                 this.showNotification('Error', responseWSFungibles[500], 'error');
    //             }else if(-1 in responseWSFungibles){
    //                 this.showNotification('Error', responseWSFungibles[-1], 'error');
    //             }
                

    //         })
    //         .catch(error => {
    //         console.log('Error consultaFungiblesWSJS '+JSON.stringify(error));
    //         this.error = error;
    //         });
    //     }
    //     // this.showSpinner = false;
    // }

    handleClickNewEquipo(event){
        console.log('Entro en handleClickNewEquipo');
        this.showAddEquipoModal = true;

        console.log('Tratamiento de la visita donde pulso: '+event.target.dataset.visitaid);
        this.visitaTratamientoToAdd = event.target.dataset.visitaid;



        
        // console.log('Debería abrir el modal de add');
        // this.visitaTratamientoToAdd = event.target.dataset.visitaid;

        console.log('VisitaTratamientoToAdd vale '+visitaTratamientoToAdd);
    }

    closeAddEquipoSuministradoModal(event){
        this.showAddEquipoModal = false;
    }

    addEquipoSuministrado(event){
        console.log('Logica de añadir equipo');

        this.checkStockEquipo();


    }

    addFungibleSuministrado(event){
        if(!this.addingFungible){
            this.addingFungible = true
            this.createFungible(event)
        }
    }
    
    handleChangeCodigoEquipoSuministrado(event){
        this.codigoEquipoSuministrado = event.target.value;
    }

    handleChangeEquipoPrincipal(event){
        this.esPrincipal = event.target.checked;
        console.log('this.esPrincipal vale: '+this.esPrincipal);
    }

    handleChangeEquipoTelematico(event){
        this.esTelematico = event.target.checked;
        console.log('this.esTelematico vale: '+this.esTelematico);

    }

    handleChangeCausaSustitucion(event){
        this.causaSustitucionValue = event.currentTarget.value;
        console.log('this.causa_Sustitucion: '+this.causaSustitucionValue);
    }
    
    getCausaSustitucion () {	
        console.log('Entra en getCausaSustitucion');
        getCausaSustitucion({   })
        .then(response => {
            console.log('Success getCausaSustitucion');
            var result = response;
            var resultObject = JSON.parse(result);               
            this.causaSustitucionOptions = resultObject;
            console.log('Lista de CausaSustitucionOptions'+JSON.stringify(this.causaSustitucionOptions));
        })
        .catch(error => {
            console.log('ERROR getCausaSustitucion');
            var errors = error;
            if (errors) {
                if (errors[0] && errors[0].message) {
                    console.log("Error message: " + 
                                errors[0].message);
                }
            } else {
                console.log("Unknown error");
            }
        })

	} 


    insertNewEquipoSuministrado(stockEquiposResponse) {
        console.log('visitaTratamientoToAdd '+this.visitaTratamientoToAdd);
        console.log('esPrincipal '+this.esPrincipal);
        console.log('esTelematico '+this.esTelematico);
        if(!this.esPrincipal){
            this.esPrincipal = false;
        }
        if(!this.esTelematico){
            this.esTelematico = false;
        }

        console.log('Antes de llamar a insertNewEquipoSuministrado');
        insertNewEquipoSuministrado({recordId : this.visitaTratamientoToAdd, codigoEquipoSuministrado : this.codigoEquipoSuministrado, esPrincipal : this.esPrincipal, esTelematico : this.esTelematico,
                                        stockEquiposResponse : stockEquiposResponse, causaSustitucionValue : this.causaSustitucionValue })
            .then(response => {
                console.log('Success insert equipo');
                console.log('Respuesta APEX insertNewEquipoSuministrado: '+JSON.stringify(response));
                
                if(response["false"] == ''){
                    console.log('Entra al if de coger tratamientos y toast');
                    // this.refreshEquipos = false;    
                    this.getTratamientosVisita();
                    this.showAddEquipoModal = false;
                    this.dispatchEvent(new ShowToastEvent({ title: '', message : 'Equipo añadido correctamente desde Atlas', variant: 'success'}));   
                    
                    this.activarTratamientoRevisado(this.visitaTratamientoToAdd, null, null);
                }else{
                    console.log('Entra al ELSE de coger tratamientos y toast');  
                    this.dispatchEvent(new ShowToastEvent({ title: '', message: response["true"], variant: 'error'}));
                }                
            })
            .catch(error => {
                console.log('Error insert Equipo '+JSON.stringify(error));
                this.error = error;
            });

    }

    handleClickNewFungible(event){
        console.log('Debería abrir el modal de add');
        this.visitaTratamientoToAdd = event.target.dataset.visitaid;

        console.log('VisitaTratamientoToAdd vale '+this.visitaTratamientoToAdd);
        this.showAddFungibleModal = true;

        // this.getTratamientoFromTratamientoVisita();
    }

    // getTratamientoFromTratamientoVisita(){

    //     getTratamientoFromTratamientoVisita({tratVisita : this.visitaTratamientoToAdd})
    //         .then(response => {
    //             console.log('Respuesta de getTratamientoFromTratamientoVisita '+JSON.stringify(response));
    //             this.casoFungible = response;               
    //             console.log('valor de la variable '+JSON.stringify(this.casoFungible));

    //         })
    //         .catch(error => {
    //             console.log('Error getTratamientoFromTratamientoVisita '+JSON.stringify(error));
    //             this.error = error;
    //         });
    // }

    closeAddFungibleModal(event){
        this.showAddFungibleModal = false;
    }

    //Handlers CREACION FUNGIBLE VISITA
    handleChangeVisita(event){
        this.visitaId = event.detail;
        console.log('this.visitaId vale: '+JSON.stringify(this.visitaId));
    }

    handleChangeFungible(event){
        console.log('event.detail vale: '+JSON.stringify(event.detail));
        if(event.detail != undefined && event.detail.Id != undefined){
            this.fungibleId = event.detail.Id;
        }else{
            this.fungibleId = null;
        }
        console.log('this.fungibleId vale: '+JSON.stringify(this.fungibleId));

    }

    handleChangeEquipo(event){
        console.log('event.detail vale: '+JSON.stringify(event.detail));
        if(event.detail != undefined && event.detail.Id != undefined){
            this.equipoid = event.detail.Id;
        }else{
            this.equipoid = null;
        }
        console.log('this.fungibleId vale: '+JSON.stringify(this.equipoid));

    }

    // handleChangeEquipoSuministrado(event){
    //     console.log('Equipo suministrado seleccionado: '+JSON.stringify(event.detail));
    //     if(event.detail != undefined && event.detail.Id != undefined){
    //         this.equipoSuministrado = event.detail.Id;
    //     }else{
    //         this.equipoSuministrado = null;
    //     }
    //     console.log('this.fungibleId vale: '+JSON.stringify(this.equipoSuministrado));
    // }

    // handleChangeTratamiento(event){
    //     console.log('event.detail vale: '+JSON.stringify(event.detail));
    //     if(event.detail != undefined && event.detail.Id != undefined){
    //         this.tratamientoId = event.detail.Id;
    //     }else{
    //         this.tratamientoId = null;
    //     }
    //     console.log('this.tratamientoId vale: '+JSON.stringify(this.tratamientoId));

    // }

    handleChangeCantidad(event){
        this.cantidadFungible = event.detail.value;
        // console.log('Longitud cantidad Fungible: '+this.cantidadFungible.length);
        console.log('this.cantidadFungible vale: '+JSON.stringify(this.cantidadFungible));
    }

    handleChangeNumeroLote(event){
        this.numeroLote = event.detail.value;
        console.log('this.numeroLote vale: '+JSON.stringify(this.numeroLote));
    }

    // handleChangePendiente(event){
    //     this.pendiente = event.detail.checked;
    //     console.log('this.pendiente vale: '+JSON.stringify(this.pendiente));
    // }
    //FIN handlers CREACION FUNGIBLE VISITA

    handleChangeUpdateCantidad(event){
        this.cantidadFungibleUpdate = event.detail.value;
        console.log('this.cantidadFungibleUpdate vale: '+JSON.stringify(this.cantidadFungibleUpdate));
    }

    handleChangeUpdateNumeroLote(event){
        this.numeroLoteUpdate = event.detail.value;
        console.log('this.numeroLoteUpdate vale: '+JSON.stringify(this.numeroLoteUpdate));
    }

    handleChangeUpdatePendiente(event){
        this.pendienteUpdate = event.detail.checked;
        console.log('this.pendienteUpdate vale: '+JSON.stringify(this.pendienteUpdate));
    }
    //FIN Handlers UPDATE FUNGIBLE VISITA

    showNotification(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }

    createFungible(event){
        // this.showSpinner = true;
        if(!this.visitaId){
            this.visitaId = this.recordId;
        }
        console.log('Entra en createFungible');
        this.checkStockFungible();
        // this.insertNewFungible();

    }

    acceptModificationModal(event){
        this.editFungibleJS();
    }

    editFungibleJS() {
        console.log();
       if(this.cantidadFungibleUpdate == undefined || this.cantidadFungibleUpdate == "" || this.cantidadFungibleUpdate == 0){
            this.showNotification('Error', 'Introduce una cantidad mayor que 0.', 'error');
        }else if(!this.isVisitaVirtual && (this.numeroLoteUpdate == undefined || this.numeroLoteUpdate == "")){
            this.showNotification('Error', 'Si la visita no es virtual, el número de lote es obligatorio.', 'error');
        }else{

        editFungible({recordId : this.rowIdToAction, numeroLote : this.numeroLoteUpdate, 
                        cantidad : this.cantidadFungibleUpdate, pendiente : this.pendienteUpdate })
            .then(result => {
                
                let responseWSFungibles = result;
                if (200 in responseWSFungibles){
                    console.log('Success editFungibleJS');

                    if(responseWSFungibles[200].includes('PROCESS KO Stock Insuficiente')){
                        console.log('Contiene process KO');
                        this.showNotification('Error', responseWSFungibles[200], 'error');
                    }else{
                        console.log('Respuesta de APEX '+JSON.stringify(responseWSFungibles));
                        this.showNotification('Success', responseWSFungibles[200], 'success');
                        this.clearVariables();
                        this.showModificationModal = false;
                        this.getFungiblesVisitaJS(false);
                    }
                }else if(401 in responseWSFungibles){
                    this.showNotification('Error', responseWSFungibles[401], 'error');
                }else if(500 in responseWSFungibles){
                    this.showNotification('Error', responseWSFungibles[500], 'error');
                }else if(-1 in responseWSFungibles){
                    this.showNotification('Error', responseWSFungibles[-1], 'error');
                }



                // this.deleteRow(row);
                // this.showConfirmationDeleteModal = false;
                // this.showNotification('Success', 'Fungible deleted correctly', 'success');
            })
            .catch(error => {
                console.log('Error editFungibleJS '+JSON.stringify(error));
                this.error = error;
                // this.showNotification('Error', error.body.fieldErrors.Numero_Lote__c[0].message, 'error');
            });
        }
    }

    clearVariables(){
        this.visitaId = null;
        this.fungibleId = null;
        this.cantidadFungible = null;
        this.numeroLote = null;
        this.pendiente = null;
        this.isVisitaVirtual = null;
    
        this.cantidadFungibleUpdate = null;
        this.numeroLoteUpdate = null;
        this.pendienteUpdate = null;
    
        this.rowToAction = null;
        this.rowIdToAction = null;
    }

    openDeleteModalSuministradoConfirmacion(event){
        console.log('Id equipo fungible para borrar currentTarget: '+event.currentTarget.dataset.equipoid);
        this.showModalConfirmacionEquipoDelete = true;
        console.log('Muestro modal de DELETE: '+this.showModalConfirmacionEquipoDelete);
        this.equipoSuninistradoToDeleteId = event.currentTarget.dataset.equipoid;
        console.log('Id equipo fungible para borrar VARIABLE: '+this.equipoSuninistradoToDeleteId);
        this.visitaTratamientoRetiradaEquipo = event.currentTarget.dataset.visitatratamientoid;
        console.log('Visita tratamiento para asociar en retirada: '+this.visitaTratamientoRetiradaEquipo);
    }

    openReinstalarModalSuministradoConfirmacion(event){
        console.log('Id equipo fungible para reinstalar currentTarget: '+event.currentTarget.dataset.equipoid);
        this.showModalConfirmacionEquipoReinstalar = true;
        console.log('Muestro modal de reinstalar: '+this.showModalConfirmacionEquipoReinstalar);
        this.equipoSuministradoToReinstall = event.currentTarget.dataset.equipoid;
        console.log('Id equipo fungible para reinstalar VARIABLE: '+this.equipoSuministradoToReinstall);
        // this.visitaTratamientoReinstalacionEquipo = event.currentTarget.dataset.visitatratamientoid;
    }

    openDeleteModalFungibleConfirmacion(event){
        console.log('Id fungible para borrar currentTarget: '+event.currentTarget.dataset.fungibleid);
        this.showModalConfirmacionFungibleDelete = true;
        console.log('Muestro modal de DELETE: '+this.showModalConfirmacionFungibleDelete);
        this.fungibleSuninistradoToDeleteId = event.currentTarget.dataset.fungibleid;
        console.log('Id fungible para borrar VARIABLE: '+this.fungibleSuninistradoToDeleteId);
        this.visitaTratamientoRetiradaEquipo = event.currentTarget.dataset.visitatratamientoid;
        console.log('Visita tratamiento para asociar en retirada: '+this.visitaTratamientoRetiradaEquipo);
    }


    closeDeleteModalSuministradoConfirmacion(){
        this.showModalConfirmacionEquipoDelete = false;
        this.equipoSuninistradoToDeleteId = '';
        this.visitaTratamientoRetiradaEquipo = '';
        console.log('Id equipo fungible para borrar VARIABLE: '+this.equipoSuninistradoToDeleteId);
    }

    closeReinstallModalSuministradoConfirmacion(){
        this.showModalConfirmacionEquipoReinstalar = false;
        this.equipoSuministradoToReinstall = '';
        // this.visitaTratamientoReinstalacionEquipo = '';
    }

    closeDeleteModalFungibleConfirmacion(){
        this.showModalConfirmacionFungibleDelete = false;
        this.equipoSuninistradoToDeleteId = '';
        this.visitaTratamientoRetiradaEquipo = '';
        console.log('Id equipo fungible para borrar VARIABLE: '+this.equipoSuninistradoToDeleteId);
    }


    confirmacionBorrarEquipoSuministrado(){
        // window.alert('Borrar del CMP y marcar retirado Equipo suministrado con Id '+this.equipoSuninistradoToDeleteId);
        console.log('Voy a retirar el equipo suministrado con id: '+this.equipoSuninistradoToDeleteId);
        console.log('Le voy a poner la visita tratamiento de retirada: '+this.visitaTratamientoRetiradaEquipo);
        this.retirarEquipoSuministrado();
    }

    confirmacionReinstalarEquipoSuministrado(){
        console.log('Voy a reinstalar el equipo suministrado con id: '+this.equipoSuministradoToReinstall);
        // console.log('Le voy a poner la visita tratamiento de retirada: '+this.visitaTratamientoReinstalacionEquipo);
        this.reinstalarEquipoSuministrado();
    }

    confirmacionBorrarFungibleSuministrado(){
        // window.alert('Borrar del CMP y marcar retirado Equipo suministrado con Id '+this.fungibleSuninistradoToDeleteId);
        console.log('Voy a retirar el fungible suministrado con id: '+this.fungibleSuninistradoToDeleteId);
        this.retirarFungibleSuministrado();
    }


    openResetModalSuministradoConfirmacion(event){
        console.log('Id equipo fungible para reset currentTarget: '+event.currentTarget.dataset.equipoid);
        this.showModalConfirmacionReset = true;
        console.log('Muestro modal de RESET: '+this.showModalConfirmacionReset);
        this.equipoSuministradoToResetId = event.currentTarget.dataset.equipoid;
        console.log('Id equipo fungible para reset VARIABLE: '+this.equipoSuministradoToResetId);
    }

    retirarEquipoSuministrado(){
        retirarEquipoSuministrado({serviceAppointment : this.recordId, identificadorEquipo : this.equipoSuninistradoToDeleteId, identificadorVisitaTratamiento : this.visitaTratamientoRetiradaEquipo})
        
        .then(response => {
            if(response["false"] == ''){
                this.getTratamientosVisita();
                this.showModalConfirmacionEquipoDelete = false;
                this.activarTratamientoRevisado(this.visitaTratamientoRetiradaEquipo, null, null);
            }
            else{
                this.dispatchEvent(new ShowToastEvent({ title: '', message: response["true"], variant: 'error'}));
            }

        })
        .catch(error => {
            
        })
    }

    reinstalarEquipoSuministrado(){
        reinstalarEquipoSuministrado({identificadorEquipo : this.equipoSuministradoToReinstall})
        
        .then(response => {
            if(response["false"] == ''){
                this.getTratamientosVisita();
                this.showModalConfirmacionEquipoReinstalar = false;
            }
            else{
                this.dispatchEvent(new ShowToastEvent({ title: '', message: response["true"], variant: 'error'}));
            }

        })
        .catch(error => {
            
        })
    }

    retirarFungibleSuministrado(){
        retirarFungibleSuministrado({serviceAppointment : this.recordId, identificadorFungible : this.fungibleSuninistradoToDeleteId})
        .then(response => {
            this.getTratamientosVisita();
            this.showModalConfirmacionFungibleDelete = false;
            this.activarTratamientoRevisado(this.visitaTratamientoRetiradaEquipo, null, null);
        })
        .catch(error => {
            
        })
    }

    handleChangeEquipoSuministradoLectura(event){
        this.equipoSuministradoToResetValue = event.target.value;
        console.log('Valor equipo suministrado reset: '+this.equipoSuministradoToResetValue);
    }

    confirmacionResetEquipoSuministrado(){
            // window.alert('Reset del CMP con Id '+this.equipoSuministradoToResetId);
            console.log('Valor id a resetear: '+this.equipoSuministradoToResetId);
            console.log('Valor lectura: '+this.equipoSuministradoToResetValue);
            var mapaEquipoResetToLectura = {};
            mapaEquipoResetToLectura[this.equipoSuministradoToResetId] = this.equipoSuministradoToResetValue;

            if(this.equipoSuministradoToResetValue == ''){
                this.dispatchEvent(new ShowToastEvent({ title: '', message: 'Introduce una lectura.', variant: 'error'}));
            }else{
                this.resetEquipoConLectura(mapaEquipoResetToLectura);
            }
    }

    resetEquipoConLectura(mapaResetLectura){
        resetEquipoConLectura({ serviceAppointment : this.recordId, equipoParaResetearConLectura : mapaResetLectura})
        
        .then(response => {
            if(response == false){
                this.dispatchEvent(new ShowToastEvent({ title: '', message: 'Lectura reseteada correctamente', variant: 'success'}));
                this.closeResetModalConfirmacion();
                this.activarTratamientoRevisado(null, null, mapaResetLectura);
            }else{
                this.dispatchEvent(new ShowToastEvent({ title: '', message: 'Para resetear un equipo tiene que existir una lectura anterior', variant: 'warning'}));
            }
                       
        })
        .catch(error => {
            console.log('ERROR resetEquipoConLectura '+JSON.stringify(error));
            this.dispatchEvent(new ShowToastEvent({ title: '', message: 'Error al resetear el equipo suministrados', variant: 'error'}));

        })
    }

    closeResetModalConfirmacion(){
        this.showModalConfirmacionReset = false;
        this.equipoSuministradoToResetId = '';
        console.log('Id equipo equipo para reset VARIABLE: '+this.equipoSuministradoToResetId);
    }

    
    
    handleToggleSection(event) {
        this.activeSectionMessage =
            'Open section name:  ' + event.detail.openSections;
    }

    handleSetActiveSectionC() {
        const accordion = this.template.querySelector('.example-accordion');

        accordion.activeSectionName = activeSections;
    }


    




    

    getTratamientosVisita(){
        getTratamientosVisita({recordId : this.recordId})
        .then(response => {
            // console.log('tamanio: '+response.visTrat.length);

            this.tratamientosVisita = response;
            // console.log(JSON.stringify(response));
            console.log('Rafa response.visTrat.length: '+response.visTrat.length);
            if(response.visTrat.length > 0){
                this.loadedTratamientosVisita = true;
                this.readOnlyMode = response.ReadOnlyMode;
                this.intervencionNoProgramada = response.intervencionNoProgramada;
                // console.log('Modo lectura vale: '+this.readOnlyMode);
                this.refreshEquipos = true;
            }
            // if(estoyEnCallback){
            //     if(response.visTrat.length > 0){
            //         this.actualSection = response.visTrat[0].Name;
            //         console.log('Estaba en la sección '+this.actualSection);
            //     }
            // }
            // console.log(JSON.stringify(response));
            // console.log(JSON.stringify(this.tratamientosVisita));

            this.loadedData = true;
        })
        .catch(error => {
            console.log('ERROR getTratamientosVisita '+JSON.stringify(error));
            this.loadedData = true;
        })
    }

    handleSuccessRecordEditForm(event) {
        // console.log('Cambios guardados correctamente', event.detail.id);
        this.dispatchEvent(new ShowToastEvent({ title: '', message: 'Cambios registrados en el sistema', variant: 'success'}));

        console.log('Intento id formulario: '+JSON.stringify(event.detail.id));


        this.activarTratamientoRevisado(null, event.detail.id, null);
    }

    handleErrorRecordEditForm(event){
        this.dispatchEvent(new ShowToastEvent({ title: '', message: 'Error al guardar cambios', variant: 'error'}));
    }

    handleResetRecordEditForm(event){
        const inputFields = this.template.querySelectorAll(
            'lightning-input-field'
        );
        if (inputFields) {
            inputFields.forEach(field => {
                field.reset();
            });
            this.dispatchEvent(new ShowToastEvent({ title: '', message: 'Se han cancelado los cambios', variant: 'success'}));
        }
    }
}