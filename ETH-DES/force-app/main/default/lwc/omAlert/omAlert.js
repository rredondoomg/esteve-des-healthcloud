/**
 * Component wrapper for Lightning Desing System alerts
 * @module OmAlert
 * @version 1.0.0
 */
import { LightningElement, api } from 'lwc';

/**
 * Custom alert
 * @class OmAlert
 */
export default class OmAlert extends LightningElement {

  /**
   * Level of the alert. Can be 'info', 'warning', 'error'
   * @type string
   * @default 'info'
   */
  @api level = 'info'

  /**
   * If texture CSS should be applied to the alert
   * @type boolean
   * @default false
   */
  @api texture = false

  /**
   * Message to show. It default slot is filled, this will not show
   * @type string
   * @default ''
   */
  @api message = ''

  /**
   * lightning-icon path name. Displayed next to the message
   * @type string
   * @default ''
   */
  @api iconName = ''

  /**
   * If the close button should be displayed
   * @type boolean
   * @default false
   */
  @api dismissable = false

  /**
   * If the iconName prop has been informed by the user
   * @private
   */
  get hasIcon() {
    return this.iconName !== ''
  }

  /**
   * Compute icon color based on level
   */
  get iconVariant() {
    return this.level === 'error' || this.level === 'info' ? 'inverse' : 'default'
  }

  /**
   * Computed CSS class to apply to root element
   * @private
   */
  get notifyClass() {
    let cssCls = 'slds-notify slds-notify_alert '
    if(this.texture) {
      cssCls += ' slds-theme_alert-texture'
    }
    cssCls += ' slds-theme_'+ this.level
    return cssCls;
  }

  dismiss() {
    /**
     * Event fired when clicking on close button
     * @event dismiss
     */
    this.dispatchEvent(new CustomEvent('dismiss', { composed : true, bubbles: true }))
  }

}