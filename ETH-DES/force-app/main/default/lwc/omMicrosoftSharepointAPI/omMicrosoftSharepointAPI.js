import OmResumableUploader from 'c/omResumableUploader';
import OmBaseAPI from 'c/omBaseAPI';

export default class OmMicrosoftSharepointAPI extends OmBaseAPI {
  provider = 'Sharepoint'


  async delete(file){
    await this.fetchRequest({
      url: this.getEndpoint('update', file),
      method: 'DELETE'
    })
    return true
  }

  async getItem(file) {
    let res = await this.fetchRequest({
      url: this.getEndpoint('get', file),
      method: 'GET'
    })

    let result = await res.json()
    result.appProperties =  result.listItem ? result.listItem.fields : {};
    return result;
  }

  async create(file, filters){
    let parent = filters?.parentFolder?.value ? filters.parentFolder.value : (file.parentId ? file.parentId : 'root')
    let f = { ...file };
    delete f.parentId;
    let r = await this.fetchRequest({
      url: this.getEndpoint('newFolder', f, parent),
      method: 'POST',
      body: f,
    });
    return r.json()
  }
  
  async createFolder(file, filters){
    let finalFile = Object.assign(file, { 

      name:file.name, 
      folder:{},
      '@microsoft.graph.conflictBehavior': 'rename'
    })
    return this.create(finalFile, filters)
  }

  upload(file, folder, onProgress, onComplete, onError, decorator) {

    let metadata = {
      "item": {
        // "@odata.type": "microsoft.graph.driveItemUploadableProperties",
        // "@name.conflictBehavior": "rename",
        "@microsoft.graph.conflictBehavior": "rename",
        "name": file.name
      }
    }

    if (decorator) {
        metadata = decorator(metadata);
    }

    let uploader = new OmResumableUploader({
      url: this.getEndpoint('uploadSession', file, folder),
      file: file,
      metadata: metadata,
      uploadUrlInBody: true,
      uploadUrlBodyKey: 'uploadUrl',
      token: this.credentials.access_token,
      onComplete: onComplete,
      onProgress: onProgress,
      onError: onError
    });

    uploader.upload();

  }

  async globalSearch(filters) {
    let searchResp = await this.fetchRequest({
      url: 'https://graph.microsoft.com/v1.0/search/query',
      method: 'POST',
      body: {
        "requests": [
          {
            "entityTypes": ['driveItem'],
            "query": {
              "queryString": filters.search.value
            }
          }
        ]
      }
    })

    let searchJson = await searchResp.json()

    debugger

    let files = searchJson?.value[0]?.hitsContainers[0]?.hits.map(hit => hit.resource)
    let response = searchJson

    console.log('Graph search results', files)
    return { files, response }
  }

  async getFilePreview(file) {
    let endp = this.getEndpoint('preview', file)
    let previewResp = await this.fetchRequest({
      url: endp,
      method: 'POST'
    })
    let rjson = await previewResp.json();
    return rjson.getUrl;
    
  }

  async list(pageToken, filters, customProperties, paginator) {

      let request = {
          '$select': 'id,size,name,file,description,folder,sharepointids,content.downloadUrl,graph.sourceUrl,createdBy,lastModifiedBy,fileSystemInfo,webUrl,parentReference',
          '$top': paginator.pageSize
      };

      let searchItems = ['driveItem']

      return this.msList(request, pageToken, filters, customProperties, paginator)

  }

  async msList(request, pageToken, filters, customProperties, paginator) {


    let files = []
    let doGlobalSearch = false
    let response, nextPageToken

    if (pageToken) {
      request.$skiptoken = pageToken
    }

    if (paginator.order) {
      request.$orderBy = paginator.order.key + ' ' + paginator.order.direction
    } else {
      request.$orderBy = 'name asc'
    }

    let endp = this.getEndpoint('list')

    if (filters.parentFolder.value && filters.parentFolder.value !== false) {
      endp += '/' + filters.parentFolder.value
    }

    // https://github.com/microsoftgraph/microsoft-graph-docs/issues/4604
    if (filters.search.value && filters.search.value !== '') {
      if(filters.searchMode.value === 'global') {
        doGlobalSearch = true;
      }
      endp += `/search(q='${filters.search.value}')`;
    } else {
      endp += '/children'
      request['$expand' ] = 'listitem($expand=fields),thumbnails'
    }

    console.info('@@ omMicrosoftAPI -> List files endpoint ---> ' + endp);
    
    if(doGlobalSearch) {
      let searchResult = await this.globalSearch(filters)
      files = searchResult.files
      response = searchResult.response
    } else {

      let r = await this.fetchRequest({
          url: endp,
          params: request,
          method: 'GET'
      })
      response = await r.json()
      
      files = response.value ? response.value.map((file) => {
        if (!file.appProperties) {
          file.appProperties =  file.listItem ? file.listItem.fields : {};
          //TODO see how we could do this
            // file.appProperties = Object.assign({}, this.getDefaultAppProperties());
        }
        return file;
      }) : [];
    }

    // Update pagination
    if(response['@odata.nextLink']) {
      let params = new URLSearchParams(response['@odata.nextLink']);
      nextPageToken = params.get("$skiptoken");
    }

      // Filter files - if search has been executed, remove results
      files = files.filter(file => {
        //If not in search results, remove
        /*if(searchedItemsids !== null && !filters.searchAllFolders.value) {
          return searchedItemsids.indexOf(file.id) !== -1
        }*/

        let match = true

        /*if (filters.search.value && filters.search.value !== '' && !filters.searchAllFolders.value) {
          match = match && file.parentReference.id === filters.parentFolder
        }*/

        /*if (filters.search.value && filters.search.value !== '') {
          match = match && file.name.indexOf(filters.search.value) !== -1
        }*/
        if (filters.createdTime.value && filters.createdTime.value !== '') {
          match = match && (new Date(file.fileSystemInfo.createdDateTime) >= new Date(filters.createdTime.value))
        }
        if (filters.createdTimeTo.value && filters.createdTimeTo.value !== '') {
          match = match && (new Date(file.fileSystemInfo.createdDateTime) <= new Date(filters.createdTimeTo.value))
        }

        if (filters.mimeType.value && filters.mimeType.value !== '' && file.file) {
            match = match && (file.file.mimeType.indexOf(filters.mimeType.value) !== -1 && file.folder !== undefined)
        }

        if (customProperties) {
          customProperties.forEach((e) => {
              if (filters.props[e.key] && filters.props[e.key] !== '') {
                if ( file.listItem?.fields[e.key]) {
                  match = match && file.listItem?.fields[e.key].indexOf(filters.props[e.key]) !== -1
                } else {
                  match = false
                }
              }
          })
        }

        return match
      })

      return { files, response, nextPageToken }
  }

  async update(file, indexableText, customPropertyKeys) {
    if (file) {
      let params = {}
      // if(file.removeParents) params.removeParents = file.removeParents
      // if(file.addParents) params.addParents = file.addParents

      let body = {
          'name': file.name,
          'description': file.description,
      };

      let propsToSend = {}
      customPropertyKeys.forEach(pk => {
        propsToSend[pk] = file.appProperties[pk]
      })

      
      // Update DriveItem
      let res = await this.fetchRequest({
          url: this.getEndpoint('update', file),
          method: 'PATCH',
          params: params,
          body: body
      })

      // Update custom columns
      if (file.sharepointIds) {
        propsToSend.ContentTags = file._tags;

        let listurl = this.getEndpoint('base', file) + '/lists/' + file.sharepointIds.listId + '/items/' + file.sharepointIds.listItemUniqueId
        await this.fetchRequest({
          url: listurl,
          method: 'PATCH',
          params: params,
          body: {
            'description': file.description,
            'fields' : propsToSend
          }
        })
      }

      // Update ListItem to append custom columns
      
      if(res.error && res.error.code) {
          console.log('Error updating file ' + JSON.stringify(res.error))
      } else {
          //self.dispatchEvent(new ShowToastEvent({ title: 'Update success', message: 'File updated in Google Drive', variant: 'success', mode: 'dismissable'}));
          //self.$emit('updatefile', file)
      }
    }
  }

  async download () {
    throw new Error('Not implemented')
  }

  isProviderFile () {
    return false;
  }

  isFolder(file) {
    return file.folder && file.folder.childCount !== undefined;
  }

  getTags (file) {
    return file.listItem?.fields?.ContentTags;
  }

  getLogoUrl() {
    return this.logoUrl ? this.logoUrl : this.staticresourceUrl + '/images/SharePointLogo.svg'
  }

  getTitle() {
    return this.title ? this.title : 'Sharepoint'
  }
}