/**
 * Dynamic inpiut used form filtering and editing cells
 * @module OmDatatableCell
 * @version 1.0.0
 * @requires OmSelect
 * @requires OmLookup
 */
import { LightningElement, api, track } from 'lwc';
import { getItemValue, getTypeFromColumn, getInputFormatterFromColumn, getTypeAttributes, getFilterAttributes } from 'c/omDatatableUtils'
import allOption from '@salesforce/label/c.omAllOption'
import omLog from 'c/omLog'

const typesUsingAttributes = ['number', 'date', 'datetime']

export default class OmDatatableInput extends LightningElement {

  @track picklistOptions;

  @api nullable = false

  @api tabIndex = null

  @api asFilter = false

  @api disabled = false

  _column
  @api get column() {
    return this._column
  }

  set column(value) {
    this._column = value
    if(value.options) {
      this.picklistOptions = [... value.options]
      if(this.nullable) {
        if(!value.options.find(v => v.value === '')) {
          this.picklistOptions = [{label: allOption, value : ''}, ... value.options]
        }
      }
    }
    if(value.type && typesUsingAttributes.includes(value.type)) {
      this.typeAttributes = getTypeAttributes(value, null)
    }

  }

  _row
  @api 
  get row() { return this._row }
  set row(val) {
    this._row = val
    if(this.isCheckbox && this.rowValue !== '') {
        this.checked = this.rowValue
    }
  }

  @api placeholder = ''

  get inputPlaceholder() {
    if(this.placeholder) return this.placeholder + ' ' + this.column.label
    return ''
  }

  @track indetermined = undefined
  @track typeAttributes = {}

  initialized = false
  _indeterminateable

  @api 
  get indeterminateable() {
    return this.indetermined
  }

  set indeterminateable(value) {
    this.indetermined = value
    this._indeterminateable = value
  }

  renderedCallback() {
    if(this.indetermined !== undefined && !this.initialized) {
      let cb = this.template.querySelector('.keyboardMode--trigger')
      if(cb) {
        cb.indeterminate = this.indetermined
        this.initialized = true
      }
    }
  }

  //TODO
  get rowValue() {
    if(!this.row) return ''
    return getItemValue(this.row, this.column)
  }

  get isEditable() {
    return this.column && this.column.editable && !this.column.value
  }

  get cellStyle () {
      return this.column && this.column.isClickable ? 'clickable' : '';
  }

  get noTypeDefined() {
      return this.column && !this.column.type
  }

  get isLightningInput(){
      return !this.isPicklist && 
        this.inputType !== 'button' &&
        this.inputType !== 'button-icon' &&
        this.inputType !== 'image' && 
        this.inputType !== 'svg' && 
        this.inputType !== 'textarea' && 
        this.inputType !== 'checkbox'
  }

  get isNumber(){ return this.inputType === 'number' }
  get isText(){ return this.inputType === 'text' }
  get isTextArea(){ return this.inputType === 'textarea' }
  get isPicklist(){ return this.inputType === 'picklist' }
  get isDatetime(){ return this.inputType === 'datetime' }
  get isTooltip(){ return this.inputType === 'tooltip' }
  get isCheckbox(){ return this.inputType === 'checkbox' }

  get showEdition() {
      return this.isEditable && (this.row.editing || this.cellEditing)
  }

  get showEditionButton() {
      return this.isEditable && !this.row.editing && !this.cellEditing
  }

  get inputType(){
      let t = getTypeFromColumn(this.column)
      if(t === 'datetime' && this.column.filterAttributes && this.column.filterAttributes.onlyByDates === true && this.asFilter === true) {
        return 'date'
      }
      return t
      
  }
  
  get inputFormatter(){
    return getInputFormatterFromColumn(this.column)
  }

  /*get typeAttributes() {
    return getTypeAttributes(this.column, this.row)
  }*/

  get filterAttributes() {
    return getFilterAttributes(this.column)
  }

  onInputBlur(ev) {
    omLog.log('omDatatableInput onInputBlur')
    ev.preventDefault()
    ev.stopPropagation()
    this.dispatchEvent(new CustomEvent('blur', { detail: ev.target.value, bubbles: false } ));
  }

  @track checked = false

  onCheckboxClick(ev) {
    // indeterminado -> true -> false -> indeterminado
    ev.stopPropagation()
    ev.preventDefault()
    let cb = this.template.querySelector('.keyboardMode--trigger')

    if(this._indeterminateable) {
      if(this.indetermined) {
        this.indetermined = false
        this.checked = true
      }
      else if(!this.indetermined && this.checked === true){
        this.checked = false
      }
      else if(this.checked === false) {
        this.indetermined = true
      }
    } else {
      this.checked = !this.checked
    }

    if(cb) {
      //eslint-disable-next-line
      setTimeout(() => {
        cb.indeterminate = this.indetermined
        cb.checked = this.checked
      }, 200)
    }

    let value = this.indetermined === true ? '' : this.checked
    this.dispatchEvent(new CustomEvent('change', { detail: value, composed: true, bubbles: true } ));
  }

  @api focus() {
      if(this.isCheckbox) {
        this.template.querySelector('input[type="checkbox"').focus()
      } else if(this.isPicklist) {
        this.template.querySelector('c-om-select').focus()
      } else if(this.isTextArea) {
        this.template.querySelector('lightning-textarea').focus()
      } else {
        this.template.querySelector('lightning-input').focus()
      }
    
  }

  onInputChange(ev) {
    omLog.log('omDatatableInput onInputChange')
    ev.stopPropagation()
    let value = ev.target.value
    this.dispatchEvent(new CustomEvent('change', { detail: value, composed: true, bubbles: true } ));
  }

  onInputKeyPress(ev) {
    omLog.log('omDatatableInput onInputKeyPress')
    ev.stopPropagation()
    if(ev.keyCode && ev.keyCode === 9) {
      ev.preventDefault()
      this.dispatchEvent(new CustomEvent('tabulation', { detail: { row: this.row }, composed: true, bubbles: true } ));
    }

    if(ev.keyCode && ev.keyCode === 13 && !this.isTextArea) {
      this.dispatchEvent(new CustomEvent('enter', { detail: ev.target.value, composed: true, bubbles: true } ));
    }
  }

  onPicklistChange(ev) {
    ev.stopPropagation()
  }

  emitValue(ev) {
    omLog.log('omDatatableInput emitValue')
    ev.stopPropagation()
    let value = ''
    if(ev.target.dataset.type === 'checkbox') {
      value = ev.target.checked
    } else if(ev.target.dataset.type === 'picklist') {
      value = ev.detail[0].value
    }
    else value = ev.target.value
    this.dispatchEvent(new CustomEvent('change', { detail: value, composed: true, bubbles: true } ));
  }
}