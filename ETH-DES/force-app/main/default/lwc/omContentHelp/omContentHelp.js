import { LightningElement, wire, track } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import contentsrulr from '@salesforce/resourceUrl/contentsr';

export default class OmContentHelp extends LightningElement {
  @track helpSection = null;
  @track helpSectionUrl = null;
  @track error = false;

  @wire(CurrentPageReference)
  getStateParameters(currentPageReference) {
    if (currentPageReference) {
      console.log('state', currentPageReference.state);
      this.helpSection = currentPageReference.state.c__section;
      switch (this.helpSection) {
        case 'config':
          this.helpSectionUrl = contentsrulr + '/helpdocs/helpConfig.pdf';
          break;
        default:
          this.error = true
      }
    }
  }
}