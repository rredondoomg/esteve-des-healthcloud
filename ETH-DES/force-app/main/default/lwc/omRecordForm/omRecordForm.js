import { LightningElement, api, wire, track } from 'lwc';
import { getRecordUi, getRecordCreateDefaults  } from 'lightning/uiRecordApi';

// TODO: Comenta lo que hagas. Si lo vas haciendo sobre la marcha luego te va a costar menos (o a mi, que me suelo pegar atracones comentando cositas que otrs (ejem ejem DCAstellon) no comentan)
// Puedes ver ejemplo justo debajo
export default class OmRecordForm extends LightningElement {
  /**
  * The sObject to use
  * @type String
  * @required
  * @default undefined
  */
  @api sobject;
  /**
  * The Id of the record to edit/view
  * @type String
  * @required
  * @default undefined
  */
  @api recordId
  /**
  * The Id of the record to edit/view
  * @type String
  * @required
  * @default undefined
  */
  @api recordTypeId
  /**
  * Modo del formulario
  * @type String
  * @required
  * @default undefined
  */  
  @api mode = 'create'
  /**
  * Esconder los botones del header
  * @type Boolean
  * @default false
  */  
  @api hideButtonHeader = false
  /**
  * Mostrar el footer
  * @type Boolean
  * @default false
  */  
  @api showFooter = false
  /**
  * Lista de campos no necesarios a quitar del form
  * @type Array
  * @default []
  */  
  @api omitedFields = []
  /**
   * Chequeamos si el formulario esta en modo view
   */  
  get isViewMode() {
    return this.mode === 'view'
  }
  /**
   * Chequeamos si el formulario esta en modo editable
   */  
  get isEditMode() {
    return this.mode === 'editable'
  }
  /**
  * Lockear una vez seleccionada la accion de guardar 
  * @type Boolean
  * @default false
  */  
  @track saveDisabled = false
    /**
  * Variable donde metemos el formulario de create formateado para mostrarlo por pantalla
  */  
  @track layoutSections;
  /**
  * Id actual del record si existe
  * @type String
  */  
  @track record;
    /**
  * Pantalla de carga activa
  * @type Boolean
  * @default false
  */  
  @track loading = true;
    /**
  * Variable donde metemos el formulario de editable/view formateado para mostrarlo por pantalla
  */  
  @track layoutSectionsViewEdit;
  /**
  * Obtenemos los datos del estandar si el modo es create
  */  
  // Este WIRE solo vale para cuando se vayan a crear registros
  // Para edicion y ver, tendras que usar "getRecordUi" con el "recordId" que te informen
  @wire(getRecordCreateDefaults, { objectApiName: '$sobject' , recordTypeId:'$recordTypeId'})
  createRecordDefaults({ data, error }) {
    if(data) {
      this.loading = false;
      console.log('Created', data)

      // Esto seria lo ideal, pero LWC solo deja meter String o Number el los "key" de los for:each, asi que tenemos que añadirlos
      // this.layoutInfo = data.layout;

      // Para ello usamos map. Tendremos que anidar tantos como sean necesarios, ya que algunos items que viene en la respuesta no tienen algo unico, como un id o un label
      // Utilizo barras bajas (_) para evitar conflictos con claves que puedan existir
      this.layoutSections = this.extractLayoutSections(data.layout)

      this.record = data.record;
      console.log('Data formated Created', this.layoutSections)
    }
  }

  /**
  * Obtenemos los datos del recorId si el modo es view/editable
  */  
  @wire(getRecordUi, { recordIds: '$recordId' , layoutTypes: 'Full', modes: 'View'})
  createRecordUiView({ data, error }) {
    if(data) {
      console.log('Record Id', data)
      this.loading = false;

      this.layoutSectionsViewEdit = this.extractLayoutSections(data.layouts[this.sobject][data.records[this.recordId].recordTypeId]['Full']['View'])

      this.record = data.record;
      console.log('Data formated View', this.layoutSectionsViewEdit)
    }
  }

  // Añadidos por hacer que veo:
  // Que se indique algo como "showRecordTypeSelction" que muestre un modal y que te salga un pickjlist o radio button para escoger el recordtype ---
  // Om modal + buscar los diferente recorType que tiene dispobibles 
  // Done:
  // Que sean responsive (que puedas usar el layout Full en moviles)
  // cosa de moviles que no pillo ni papa -Css
  // Pasar un array con los campos (no required ) que no se quieran mostra / editar
  // recibimos el array y quitamos los campos que no quiera despues de los layout section
  // Añadir slots, por ejmeplo para meter botones custom - lo puso marcos en el header, lo pongo en el footer tambien 

  /**
   * Extracts and prepares the layout info to be rendered in a LWC
   * @param {Object} layoutFromUiApi 
   * @returns The sections with the data mapped ready to iterate in a LWC
   */
  extractLayoutSections(layoutFromUiApi) {
    return layoutFromUiApi.sections.map(section => {
      // Copiamos el objeto y lo modificamos
      let mappedSection = Object.assign({}, section)

      // Hacemos lo mimso con las row de cada seccion
      mappedSection.layoutRows = mappedSection.layoutRows.map((row, ridx) => {
        let clonedRow = Object.assign({}, row)
        clonedRow._key = ridx;

        // Y lo mismo con los items de cada row
        clonedRow.layoutItems = row.layoutItems.map((item, itemIdx) => {
          let clonedItem = Object.assign({}, item)
          let required = clonedItem.required
          clonedItem._key = itemIdx;

          // Y más de lo mismo con los componentes de cada item + cequeo si esta en la lista para quitarlo
          clonedItem.layoutComponents = item.layoutComponents.map((compo, compoIdx) => {
            let found = this.omitedFields.find(element => element === compo.apiName)      
            if(found === undefined || (required && found != undefined)){  
              let clonedCompo = Object.assign({}, compo)
              clonedCompo._key = compoIdx;
              return clonedCompo;
            }
            return {}
          })
          // Aprovechamos el .map y asi añadimos las clases de CSS que queramos aplicar, por ejemplo en funcion de su numero de "columns"
          clonedItem._complexType = clonedItem.layoutComponents.length > 1
          clonedItem._editable = this.mode === 'editable' ? clonedItem.editableForNew : clonedItem.editableForUpdate
          clonedItem._readonly = this.mode === 'editable' ? !clonedItem.editableForNew : !clonedItem.editableForUpdate
          clonedItem._class = mappedSection.columns === 2 ? 'slds-col slds-size_1-of-2 ' : 'slds-col slds-size_1-of-1'
          return clonedItem;
        })
        return clonedRow;
      })
      return mappedSection;
    })
  }
  /**
  * Hacemos que al enviar un submit cambie el boton de estado por si tarda en cargar el formulario
  */
  handleSubmit() {
    console.log('Submit')
    this.saveDisabled = true
  }
  /**
  * Eviamos el evento de que se han creado/guardado un nuevo registro
  */
  handleSuccess(ev) {
    console.log('Success')
    const selectedEvent = new CustomEvent('created', { detail: {recordId: ev.detail.id, record: ev.detail.fields}});
    this.dispatchEvent(selectedEvent);
  }
  /**
  * Lanzamos el evento si se selecciona el boton de cancelar del footer
  */
  handleCancel(ev) {
    const selectedEvent = new CustomEvent('cancel');
    this.dispatchEvent(selectedEvent);
  }
}