/**
 * Component to display a docked composer, sticked to the bottom right 
 * of the screen without the need of use UtilityBar API from Salesforce
 * @module OmDockedComposer
 * @version 1.0.0
 */
import { LightningElement, api } from 'lwc';

/**
 * @class OmDockedComposer
 */
export default class OmDockedComposer extends LightningElement {
  /**
   * The title of the docker composer
   * @type string
   * @default 'Composer'
   */
  @api title = 'Composer'

  /**
   * The needed height
   * @type string
   * @default '300px''
   */
  @api height = '300px'

  /**
   * The icon name to display in the top bar
   * @type string
   * @default 'utility:help'
   */
  @api iconName = 'utility:help'


  get wrapperStyle() {
    return `height: ${this.height}, minWidth: '615px', overflowX: 'auto'`
  }
}