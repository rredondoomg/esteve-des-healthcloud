/**
 * Custom content uploader. Available in flows
 * @module OmContentUploader
 */
import { api, track, LightningElement } from 'lwc';
import upsertContents from '@salesforce/apex/OM_ContentService.upsertContents';
import deleteContents from '@salesforce/apex/OM_ContentService.deleteContents';
import { processContentSettings, onProviderCredentials, onSignOut } from 'c/omContentUtils';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

/**
 * @class
 */
export default class OmContentUploader extends LightningElement {

  _recordId = null;

  /**
   * The recordId
   */
  @api 
  get recordId () {
    return this._recordId
  }

  set recordId (value) {
    console.log('Setter recordId')
    this._recordId = value
  }

  @api 
  get settingToUse () {
    return this.customMetadataName
  }

  set settingToUse (value) {
    console.log('Setter settingToUse')

    processContentSettings(value, this._recordId, true).
      then(({ apiConfig, userAccess, settings, settingName, customAppProperties, providerImplementation}) => {
        console.log('Setter settingToUse then')
        this.providerImplementation = providerImplementation;
        this.settings = settings;
        this.settingName = settingName;
        this.apiConfig = apiConfig;
        this.userAccess = userAccess;
        this.customAppProperties = customAppProperties

        this.initialize();
      })
  }

  initialize() {
    this.initialized = true;
    

    if (this.signedIn) {
      this.providerImplementation.setCredentials(this.userAccess)
    }
  }

  @api initialFolderId = 'root'

  @track errors
  // The custom metadata has been retrieved and the Google Explorer can be displayed
  @track initialized = false
  @track providerImplementation = false
  @track settingName = ''
  @track customAppProperties = null
  @track settings = {}
  @track apiConfig = {}
  @track userAccess = null


  @api get contentUploadedId () {
    return this.fileUploaded !== null ? this.fileUploaded.id : '';
  };

  @api get contentUploadedName () {
    return this.fileUploaded !== null ? this.fileUploaded.name : '';
  };

  @track fileUploaded = null;
  @track progress = 0;
  @track uploading = false
  // @track signedIn = false
  @track tries = 0

  get signedIn () {
    return this.userAccess && this.userAccess.access_token;
  }

  /**
   * Gets the file, process the CSV into a JSON objets and sends to APEX to further process
   * @param {Event} ev The event emitted by the input file
   * @private
   */
  onFileSelected (ev) {
    let fileList = ev.target.files
    var file = fileList[0]
    this.tries = 0
    this.uploadFile(file)
  }

  /**
   * Uploads the file in the provider
   * @param {Object} file The file selected
   * @private
   */
  uploadFile (file) {
    //Send to Provider
    let self = this;
    this.uploading = true;
    this.progress = 0;
    this.fileUploaded = null;

    this.providerImplementation.upload(
      file,
      {
        parentFolder: this.initialFolderId,
      },
      (p) => { 
        console.log('Upload progress...', p)
        self.progress = parseInt((p.loaded / p.total) * 100)
      },
      (e) => { 
        console.log('Upload complemte...', e)
        self.uploading = false;
        self.fileUploaded = JSON.parse(e);
        self.dispatchEvent(new ShowToastEvent({ title: 'Success **', message: 'File uploaded correcly **', variant: 'success', mode: 'dismissable'}));
      },
      (e) => {
        console.log('Upload error...', e)
        self.uploading = false;
        this.dispatchEvent(new ShowToastEvent({ title: 'Error **', message: 'File couldn`t be uploaded **', variant: 'error', mode: 'dismissable'}));

        
      },
      null
     )
  }

  onUploadComplete(e) {
    this.dispatchEvent(new ShowToastEvent({ title: 'Success', message: 'File uploaded correcly', variant: 'success', mode: 'dismissable'}));
    //Emit event to close quick action
    this.dispatchEvent(new CustomEvent('complete', { bubbles: true, composed: true}))
  }

  onUploadError(e) {
    //Emit event to close quick action
    this.dispatchEvent(new ShowToastEvent({ title: 'Error', message: 'File couldn`t be uploaded', variant: 'error', mode: 'dismissable'}));
  }

}