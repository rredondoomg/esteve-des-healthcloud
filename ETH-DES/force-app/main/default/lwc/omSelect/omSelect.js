/**
 * Component to render dual list box o combobox and allow to select Salesforce records, values obtained from Salesforce schema or custom values
 * @module OmSelect
 * @version 1.0,0
 */
import { LightningElement, api, track, wire } from 'lwc';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import getValues from '@salesforce/apex/OM_LWC_pack.getValues';
import selected from '@salesforce/label/c.omSelected';
import available from '@salesforce/label/c.omAvailable';
import selectChoose from '@salesforce/label/c.omSelectChoose';
import optionsSelected from '@salesforce/label/c.omOptionsSelected';
import selectAll from '@salesforce/label/c.omSelectAll';
import deselectAll from '@salesforce/label/c.omDeselectAll';

/**
 * @class OmSelect
 */
export default class OmSelect extends LightningElement {

    /**
     * Salesforce Object API name to get the values from
     * @type string
     */
    @api objectApiName = '';

    /**
     * Salesforce Object field API name to get the values from
     * @type string
     */
    @api fieldApiName = '';

    /**
     * SOQL filter conditions to filters records by
     * @type string
     */
    @api filter = '';

    /**
     * If the options to show are going to be Salesforce records
     * @type boolean
     * @default false
     */
    @api areRecords = false;


    _value = ''

    /**
     * Value passed to rendered Lighning Web components
     * @type string
     * @default ''
     */
    // @api value = '';
    @api
    get value() { return this._value }

    set value(nv) { this._value = nv }

    /**
     * Default value to preselect an option
     * @type string
     * @default ''
     */
    @api defaultValue = '';

    @track _records = []

    /**
     * Records to be able to select (options)
     * @type Array
     * @default []
     */
    // @api records = [];
    @api
    get records() { return this._records }

    set records(nv) {
        this._records = nv
        const aux = JSON.parse(JSON.stringify(nv))
        this._records = aux.map( el => {
            el.selected = false
            return el
        })
    }

    /**
     * If the input should be disabled
     * @type boolean
     * @default false
     */
    @api disabled = false;

    /**
     * If the input should be required
     * @type boolean
     * @default false
     */
    @api required = false;


    /**
     * If the input should acceptu multiple selections
     * @type boolean
     * @default false
     */
    @api isMultiple = false;


    /**
     * Label passed to inner LWC. If empty, variant will change to 'stacked-label'
     * @type string
     * @default ''
     */
    @api label = '';

    /**
     * Label obtained from Schema with getObjectInfo
     * @private
     */
    @track fieldLabel = '';

    /**
     * For duallistbox, left label
     * @type string
     * @default ''
     */
    @api sourceLabel = available;

    /**
     * For duallistbox, right label
     * @type string
     * @default ''
     */
    @api selectedLabel = selected;

    /**
     * Help text
     * @type string
     * @default ''
     */
    @api fieldLevelHelp = '';

    /**
     * The variant changes the appearance of the component. Accepted variants include standard, label-hidden, label-inline, and label-stacked.
     * This value defaults to standard. Use label-hidden to hide the label but make it available to assistive technology.
     * Use label-inline to horizontally align the label and dual listbox. Use label-stacked to place the label above the component.
     * @type string
     * @default ''
     */
    @api variant = '';

    /**
     * Input placeholder
     * @type string
     * @default ''
     */
    @api placeholder = '';

    /**
     * If select all / deselect all buttons should be hidden (multi select)
     * @type boolean
     * @default false
     */
    @api hideAllButtons = false;

    /**
     * Minimum number of options required in the selected options listbox. (multi select)
     * @type integer
     * @default 0
     */
    @api min = 0;

    /**
     * Maximum number of options allowed in the selected options listbox. (multi select)
     * @type integer
     * @default 99999
     */
    @api max = 99999;

    @api multipleui = '1'

    @track comboStyle = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click'

    get getValue() {
        return (this.value ? this.value : this.defaultValue) ? this.defaultValue : '';
    }

    get getLabel() {
        return (this.label !== '' ? this.label : this.objectApiName !== '' && this.fieldLabel !== '' && this.variant === 'label-stacked') ? this.fieldLabel : '';
    }

    get variantLabel() {
        if(this.variant !== ''){
            return this.variant
        }
        if(!this.getLabel)
            return 'label-hidden'
        return ''
    }

    get multiple1() { return this.multipleui === '1' }

    get multiple2() { return this.multipleui === '2' }

    get selectedOptionsLabel() { return this._records.filter(el => el.selected ).length + ' ' + this.labels.optionsSelected }

    labels = {
        selectAll,
        deselectAll,
        optionsSelected
    }

    openned = false

    /**
     * LWC lifecycle hook
     * @private
     */
    connectedCallback() {
        if(this.objectApiName && this.fieldApiName){
            this.getValuesJs()
        }
    }

    getValuesJs() {
        getValues({ objName: this.objectApiName, fieldName: this.fieldApiName, areRecords: !!this.areRecords, filter: this.filter })
            .then((data) => {
                const opts = [];
                if(!this.isMultiple){
                    opts.push({ label: selectChoose, value: ''});
                    this._records = opts.concat(JSON.parse(data));
                }else{
                    this._records = JSON.parse(data).map(el => {
                        el.selected = false
                        return el
                    })
                }
            })
            .catch(() => {
            });
    }

    @wire(getObjectInfo, { objectApiName: '$objectApiName' })
    objectInfo({ data }) {
        if (data && data.fields[this.fieldApiName]) this.fieldLabel = data.fields[this.fieldApiName].label;
    }


    /**
     * Selects all available records/options
     * @fires select
     */
    @api selectAll() {
        this._value = this.records.map( el => el.value )
        this.dispatchEvent(new CustomEvent('select', { detail: this.records }));
    }

    /**
     * Deselects all available records/options
     * @fires select
     */
    @api deselectAll() {
        this._value = []
        this.dispatchEvent(new CustomEvent('select', { detail: [] }));
    }

    /**
     * Set the focus on the underlying input
     */
    @api focus() {
        if(this.isMultiple) {
            this.template.querySelector('lightning-dual-listbox').focus()
        } else {
            this.template.querySelector('lightning-combobox').focus()
        }
    }

    /**
     * Handles blur event on comobobox and duallistbox
     * @private
     * @fires blurselect
     */
    handleBlur(ev) {
        ev.stopPropagation()
        ev.stopImmediatePropagation()
        /**
         * Event fired when element lost focus. Cannot be named 'blur' due bubbling conflicts with standard event
         * @event blurselect
         */
        this.dispatchEvent(new CustomEvent('blurselect'));
    }

    /**
     * Handles change event on comobobox and duallistbox
     * @private
     * @fires select
     */
    handleChange(e) {
        const records = [];
        if(e.detail && e.detail.value && Array.isArray(e.detail.value) && e.detail.value.length > 0){
            e.detail.value.forEach(element => {
                const recordFound = this.records.find(record => record.value === element);
                if(recordFound){
                    records.push(recordFound);
                }
            });
        } else if(e.detail && e.detail.value && !Array.isArray(e.detail.value) && e.detail.value.length > 0){
            // David: no chekebas que records tenga valores (el .find te puede dar undefined)
            const rec = this.records.find(r => r.value && r.value.toString() === e.detail.value.toString())

            if(rec)
                records.push({
                    label: rec.label,
                    value: e.detail.value
                })
        } else if (e.detail && (e.detail.value === '' || e.detail.value === null)){
            records.push({
                label: e.detail.value,
                value: e.detail.value
            });
        }
        /**
         * Event containing the selected records/options
         * @event select
         * @type Array
         */
        this.dispatchEvent(new CustomEvent('select', { detail: records }));
    }

    toggleCombo(evt) {
        const closeDivCombo = this.template.querySelectorAll('.close-div-combo');
        if (evt.target.id.includes('combobox')) {
            this.openned = !this.openned
            this.comboStyle = this.openned ? 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-is-open' : 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click'
            closeDivCombo[0].style.display = closeDivCombo[0].style.display === 'initial' ? 'none' : 'initial'
        }
    }

    setOption(evt) {
        this._records.find( el => el.value === evt.currentTarget.dataset.option ).selected = evt.currentTarget.dataset.value === 'false'
        this.dispatchEvent(new CustomEvent('select', { detail : { value : this._records.filter( el => el.selected ) }}))
    }

    closeIt() {
        this.openned = false
        this.comboStyle = this.comboStyle.split('slds-is-open')[0]
    }

    closeDiv() {
        const closeDivCombo = this.template.querySelectorAll('.close-div-combo');
        closeDivCombo[0].style.display = 'none';
        this.openned = false
        this.comboStyle = this.comboStyle.split('slds-is-open')[0]
    }

}