/**
 * Custom log utility service to render colorfull logs on console and easy toggle fro logging
 * @module OmLog
 * @version 1.0.0
 */
var enabled = false
var app = ''

function enable(appName) {
 app = appName
 enabled = true
}

function log(msg) {
  if(enabled) console.log(app + ' ' + msg);
}

function info(msg) {
  if(enabled) console.log('%c' + app + ' ' + msg, 'padding: 0.2em; background: #fff; color: black');
}

function warn(msg) {
  if(enabled) console.log('%c' + app + ' ' + msg, 'padding: 0.2em; background: orange; color: grey');
}

function error(msg) {
  if(enabled) console.log('%c' + app + ' ' + msg, 'padding: 0.2em; background: red; color: white');
}

function success(msg) {
  if(enabled) console.log('%c' + app + ' ' + msg, 'padding: 0.2em; background: green; color: white');
}



export {
  enable,
  log,
  info,
  warn,
  error,
  success
}