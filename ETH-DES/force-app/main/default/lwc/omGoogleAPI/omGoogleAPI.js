import OmResumableUploader from 'c/omResumableUploader';
import OmBaseAPI from 'c/omBaseAPI';

export default class OmGoogleAPI extends OmBaseAPI {
  provider = 'GoogleDrive';

  getLogoUrl() {
    return this.staticresourceUrl + '/images/GoogleDriveLogo.svg'
  }

  getTitle() {
    return 'Google Drive'
  }

  async delete(file) {
    await this.fetchRequest({
      url: this.getEndpoint('update', file),
      method: 'DELETE'
    })
    return true
  }

  async getItem(file) {
    let res = await this.fetchRequest({
      url: this.getEndpoint('get', file),
      method: 'GET'
    })

    let result = await res.json()
    return result;
  }

  async getFilePreview(file) {
    return Promise.resolve(file.webViewLink.replace('/view', '/preview'))
  }

  async create(file, filters){
    let r = await this.fetchRequest({
      url: this.getEndpoint('newFolder', file),
      method: 'POST',
      body: file,
    });

    return r.json()
  }

  
  async createFolder(file, filters){
    let parents = file.parents ? file.parents : (file.parentId ? [file.parentId] : [filters.parentFolder.value])
    let finalFile = Object.assign(file, { 
      parents: parents,
      mimeType: 'application/vnd.google-apps.folder'
    })
     
    return this.create(finalFile, filters)
  }

  upload(file, folder, onProgress, onComplete, onError, decorator) {
    //Send to Google drive

    // TODO: default properties??
    let metadata = {
      // 'appProperties': Object.assign({}, self.getDefaultAppProperties()),
      name: file.name,
      mimeType: file.type,
      parents: [folder],
      appProperties: {}
    };

    if (decorator) {
      metadata = decorator(metadata);
    }
    let uploader = new OmResumableUploader({
      file: file,
      metadata: metadata,
      token: this.credentials.access_token,
      onComplete: onComplete,
      onProgress: onProgress,
      onError: onError,
    });

    uploader.upload();
    return metadata;
  }

  async list(pageToken, filters, customProperties, paginator) {

    let request = {
      pageSize: paginator.pageSize,
      corpus: 'user',
      fields:
        'nextPageToken, files(id, name, mimeType, iconLink, thumbnailLink, webContentLink, fullFileExtension, fileExtension, size, description, contentHints, createdTime, modifiedTime, lastModifyingUser, webViewLink, owners, parents, appProperties)',
    };

    if (pageToken) {
      request.pageToken = pageToken;
    }

    if (paginator.order) {
      request.orderBy = paginator.order.key + ' ' + paginator.order.direction
    } else {
      request.orderBy = 'name asc'
    }
    //query already exiting flag
    let qe = false;
    let req = [];

    if (filters.search.value && filters.search.value !== '') {
      req.push(
        "name contains '" +
          filters.search.value +
          "' or fullText contains '" +
          filters.search.value +
          "'"
      );
    }

    if (filters.mimeType.value && filters.mimeType.value !== '') {
      req.push(" mimeType contains '" + filters.mimeType.value + "'");
    }

    if (filters.createdTime.value && filters.createdTime.value !== '') {
      req.push("createdTime >= '" + filters.createdTime.value + "'");
    }

    if (filters.createdTimeTo.value && filters.createdTimeTo.value !== '') {
      req.push("createdTime <= '" + filters.createdTimeTo.value + "'");
    }

    if (filters.modifiedTime.value && filters.modifiedTime.value !== '') {
      req.push("modifiedTime >= '" + filters.modifiedTime.value + "'");
    }

    if (filters.modifiedTimeTo.value && filters.modifiedTimeTo.value !== '') {
      req.push("modifiedTime <= '" + filters.modifiedTimeTo.value + "'");
    }

    if (filters.visibility.value && filters.visibility.value !== null) {
      req.push("visibility = '" + filters.visibility.value + " '");
    }

    if (filters.starred.value && filters.starred.value !== false) {
      req.push('starred = ' + filters.starred.value);
    }

    if (filters.trashed.value && filters.trashed.value !== false) {
      req.push('trashed = ' + filters.trashed.value);
    }

    if (filters.parentFolder.value && filters.parentFolder.value !== false) {
      req.push("'" + filters.parentFolder.value + "' in parents");
    }

    if (customProperties) {
      customProperties.forEach((e) => {
        if (filters.props[e.key] && filters.props[e.key] !== '') {
          req.push(
            `(appProperties has { key='${e.key}' and value='${
              filters.props[e.key]
            }' })`
          );
        }
      });
    }

    request.q = req.join(' and ');
    console.info('@@ omGoogleAPI -> Drive list files query ---> ' + request.q);

    // gapi.client.drive.files.list(request)
    let r = await this.fetchRequest({
      url: this.endpoints.list,
      params: request,
      method: 'GET',
    });
    let response = await r.json();

    let files = response.files.map((file) => {
      if (!file.appProperties) {
        //TODO see how we could do this
        file.appProperties = {}
      } else {
        //look for defined properties not existing in current appProperties
        if (customProperties) {
          customProperties.forEach((appProp) => {
            if (!file.appProperties[appProp.key]) {
              file.appProperties[appProp.key] = appProp.default;
            }
            //check jsontype
            if (appProp.jsonType === 'Boolean') {
              let boo = file.appProperties[appProp.key] === 'true';
              file.appProperties[appProp.key] = boo;
            }
          });
        }
      }
      return file;
    });

    return { files, response, nextPageToken: response.nextPageToken };
  }

  async update(file, indexableText) {
    let params = {};
    if (file.removeParents) params.removeParents = file.removeParents;
    if (file.addParents) params.addParents = file.addParents;
    file.description = file._tags;

    let body = {
      name: file.name,
      description: file.description,
      appProperties: file.appProperties,
      contentHints: {
        indexableText: indexableText,
      },
    };

    let r = await this.fetchRequest({
      url: this.getEndpoint('update', file),
      method: 'PATCH',
      params: params,
      body: body,
    });

    let response = await r.json();

    if (response.error && response.error.code) {
      console.log('Error updating file ' + JSON.stringify(response.error));
    } else {
    }
  }

  async download() {
    let opts = {
      url: this.getEndpoint('update', file) + '?alt=media',
      method: 'GET',
      headers: {
        'Content-Type': 'application/octect-stream',
        Authorization: 'Bearer ' + this.credentials.access_token,
      },
      mode: 'cors',
      cache: 'default',
    };

    let response = await this.fetchRequest(opts);
    response.blob().then((b) => {
      b.name = file.name;
      b.lastModifiedDate = new Date();
      resolve(b);
    });
  }

  isProviderFile(file) {
    return file.mimeType.indexOf('application/vnd.google-apps') !== -1;
  }

  isFolder(file) {
    return file.mimeType === 'application/vnd.google-apps.folder';
  }

  getTags(file) {
    return file.description;
  }
}