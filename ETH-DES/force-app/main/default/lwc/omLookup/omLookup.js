/**
 * Component to display a custom loookup for Salesforce records or custom lists
 * It can display in dropdown mode or in popup mode
 * @module OmLookup
 * @version 1.1.0
 */
import { LightningElement, api, track } from 'lwc'
import getResults from '@salesforce/apex/OM_Lookup.getResults'
import getSalesforceObjects from '@salesforce/apex/OM_Lookup.getSalesforceObjects'
import noResultFound from '@salesforce/label/c.omNoResultFound'
import loading from '@salesforce/label/c.omLoading'
import inputRequired from '@salesforce/label/c.omInputRequired'
import inputInvalid from '@salesforce/label/c.omInputInvalid'


export default class OmLookup extends LightningElement {

    currentSearchText = ''

    @track selectedRecords = [];

    /**
     * If the isearch should use SOSL instead of SOQL
     * @type boolean
     * @default false
     */
    @api sosl = false

    /**
     * SLDS icon name to display for the search action
     * @type string
     * @default 'utility:search'
     */
    @api icon = 'utility:search'

    /**
     * Title for the modal when displayed
     * @type string
     * @default ''
     */
    @api modalHeaderLabel = '';

    /**
     * If the input and actions shoudl be disabled
     * @type boolean
     * @default false
     */
    @api disabled;

    /**
     * If the lookup can select multiple records
     * @type boolean
     * @default false
     */
    @api multiselectable;

    /**
     * If the seach input shiould be displayed in the modal
     * @type boolean
     * @default false
     */
    @api showSearchInput;

    /**
     * If the paginator should be hided in the modal
     * @type boolean
     * @default false
     */
    @api hidePaginator;

    /**
     * If the paginator page numbers should be hided in the modal
     * @type boolean
     * @default false
     */
    @api hideNumberButtons;

    /**
     * If the paginator control buttons should be hided in the modal
     * @type boolean
     * @default false
     */
    @api hideControlButtons;

    /**
     * If the modal action should be disabled
     * @type boolean
     * @default false
     */
    @api disableActionLoupe;

    _sobject

    /**
     * Salesforce object API name to looks record for
     * @type string
     * @default ''
     */
    @api
    get sobject() {
        return this._sobject
    }

    set sobject(val) {
        this._sobject = val
        if (this.value) {
            this.getValue(val)
        }
    }

    /**
     * Salesforce object fields API names, separated by commas
     * @private
     * @type string
     * @default ''
     */
    @track sobjectFields = ''

    /**
     * Salesforce object fields API names, separated by commas
     * @type string
     * @default ''
     */
    @api get fields(){ return this.sobjectFields}

    set fields(vale) { this.sobjectFields = vale }

    /**
     * String with query filter to apply when searching, such as
     * "MiCampo__c = 'valor' OR MiCampo2__c = 'valor'"
     * @type string
     * @default '''
     */
    @api filter = '';

    /**
     * List of preselected items
     * @type string | Array
     * @default undefined
     */
    @api selectedItems;

    /**
     * Salesforce object fields API names, separated by commas
     * @private
     * @type string
     * @default ''
     */
    @track selectRecordName = '';

    /**
     * Label for the lookup field
     * @type string
     * @default undefined
     */
    @api label;

    /**
     * If a value is required to select after seeing the results
     * @type boolean
     * @default false
     */
    @api required = false;

    // Private list of records to search. To be filled with Apex call or from public property
    _searchRecords = []

    /**
     * Pre-defined list of records to search in
     * @type Array
     * @default []
     */
    @api records

    /**
     * SLDS icon path to show in the results and in the selected record
     * @type string
     * @default 'standard:record_lookup'
     */
    @api
    get iconName() {
        return this._iconName;
    }

    set iconName(val) {
        this._iconName = val
    }

    _iconName = 'standard:record_lookup'

    /**
     * Datetime format to show (in moment.js notation). Example: 'DD MM YYYY hh:mm:ss'
     * @type string
     * @default ''
     */
    @api datetimeFormat = '';

    /**
     * Placeholder to show in search input
     * @type string
     * @default ''
     */
    @api placeholder = ''

    /* Others */
    @api uniqueField = 'Id';

    /**
     * Records number to return as preview results
     * @type string
     * @default '4'
     */
    @api recordsNumber = '4'


    _multiEntity = false

    _doneEntityRestriction = false

    _restrictedEntities = null

    loadingObjects = false

    /**
     * If a entity dropdown should be displayed to allow multiple sObjects in lookup
     * @type boolean
     * @default false
     */
    @api
    get multiEntity() {
        return this._multiEntity;
    }

    set multiEntity(val) {
        this._multiEntity = val
        this.loadingObjects = true
        getSalesforceObjects()
            .then(res => {
                this.entities = res.map(r => {
                    const tinfo = JSON.parse(r.tabInfo);
                    let iconPath = 'standard:record_lookup';
                    if (tinfo) {
                        const svgicon = tinfo.icons ? tinfo.icons.find(i => i.contentType.indexOf('svg') !== -1) : 'utility/record.svg'
                        if(svgicon) {
                            const iconPathParts = svgicon.url.split('/');
                            iconPath = `${iconPathParts[iconPathParts.length - 2]  }:${  iconPathParts[iconPathParts.length - 1].replace('.svg', '')}`

                        }
                    }
                    return  {
                        label: r.label,
                        value: r.value,
                        iconPath
                    }
                })
                    .sort((a, b) => a.label.localeCompare(b.label));

                if(!this._doneEntityRestriction && this._restrictedEntities !== null) {
                    this.restrictEntities()
                }
                this.loadingObjects = false;
            })
    }

    /**
     * List of entities API names to restrict in multi-entity dropdown
     * @type Array
     * @default null
     */
    @api
    get restrictedEntities() {
        return this._restrictedEntities
    }

    set restrictedEntities(val) {
        this._restrictedEntities = val
        if(this.multiEntity && !this._doneEntityRestriction && this.entities.length > 0) {
            this.restrictEntities()
        }
    }

    @track entities = []

    restrictEntities() {
        this._doneEntityRestriction = true;
        this.entities = this.entities.filter(ent => this._restrictedEntities.find(restricted => restricted.toLowerCase() === ent.value.toLowerCase()) !== undefined)
    }

    _searchThrottlingTimeout;

    labels = {
        noResultFound,
        loading,
        inputRequired,
        inputInvalid
    }

    _value = ''

    /**
     * Initial value selected
     * @type Object|String
     * @default null
     */
    @api get value() { return this._value }

    set value(val) {
        console.log('omLookup --> value setter', val)
        this._value = val
        if(val === null || val === '' || val === undefined){
            this.resetData(true);
        } else {
            this.getValue(val)
        }
    }

    getValue(val) {
        if(this._sobject){
            getResults({
                objectApiName: this._sobject,
                fields: (this.fieldToShow ? this.fieldToShow : this.fields),
                value: '',
                filter: this.type !== 'modal' ? `Id = '${  val  }'` : '',
                sosl: this.sosl,
                recordsNumber: this.recordsNumber
            })
                .then(result => {
                    this._searchRecords = [];
                    let selectedrec = null
                    if(result.length > 0){
                        if (this.type === 'modal') {
                            result.forEach(element => {
                                this._searchRecords.push({ recId: element.Id, recName: element[this.fieldToShow] });
                                this.selectRecordName = element[this.fieldToShow];
                                selectedrec = element
                            });
                        } else {
                            this.selectRecordName = result[0][this.fieldToShow];
                            selectedrec = result[0]
                        }

                        // this.txtclassname =  'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
                        this.iconFlag = false;
                        this.clearIconFlag = true;
                        this.inputReadOnly = true;

                        this.selectSingleRecord(selectedrec.Id, selectedrec[this.fieldToShow] )
                        // this.emitSelectedRecords();

                    }
                })
                .catch(error => {
                    this.iconFlag = false;
                    this.messageFlag = false;
                    this.loadingText = false;
                    console.log(`-------error-------------${ JSON.stringify(error)}`);
                });
        }
        else if (this.records) {
            let selectedrec = null
            if(typeof val === 'string') {
                selectedrec = { recId: val, recName: val }
            } else {
                selectedrec = { recId: val[this.keyField], recName: val[this.fieldToShow] }
            }

            this.selectRecordName = selectedrec.recName;
            // this.txtclassname =  'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
            this.iconFlag = false;
            this.clearIconFlag = true;
            this.inputReadOnly = true;

            this.selectSingleRecord(selectedrec.recId, selectedrec.recName )
        }
    }

    /**
     * Type for the modal. Can be 'standard' or 'modal'
     * @type string
     * @default 'standard'
     */
    @api type = 'standard'

    /**
     * Field for the selection to show. Also used as a column for the table if not columns or field informed
     * @type string
     * @default 'Name'
     */
    @api fieldToShow = 'Name';

    /**
     * Field used as key. Importante when using "records" property
     * @type string
     * @default 'Id'
     */
    @api keyField = 'Id';


    tableColumns = []

    /**
     * Columns to show in the modal
     * @type array
     */
    @api get columns(){ return this.tableColumns }

    set columns(val){  this.tableColumns = val }

    @api rows;

    /**
     * Clears the selection
     */
    @api clear() {
        this.resetData(true)
    }

    /**
     * Checks validity status
     */
    @api checkValidity() {
        return !this.hasError
    }

    /**
     * Returns validation message
     * @readonly
     * @type string
     */
    @api get validationMessage() {
        return this.errorMessage
    }


    @track messageFlag = false

    @track iconFlag =  true

    @track clearIconFlag = false

    @track inputReadOnly = false

    @track modalLookup = false

    @track loadingText = false

    hasBeenOpened = false

    hasFocus = false

    get isDisabled(){
        return this.disabled || (this.multiEntity && !this._sobject)
    }

    get isComboboxDisabled(){
        return this.disabled || (this.multiEntity && this.hasSelection)
    }

    get isStandard(){
        return this.type === 'standard' || this.type === ''
    }

    get isButton(){
        return this.type === 'modal'
    }

    get isListView(){
        return this._sobject!==''
    }

    get hasIcon() {
        return this.iconName !== ''
    }

    get searchInTable() {
        return this.type === 'standard' ? true : this.showSearchInput
    }

    get disabledButtonCursor(){
        return this.disabled ? 'cursor: no-drop' : 'cursor: pointer'
    }

    get condIconStyle(){
        return this.label ? 'margin: 3px' : ''
    }

    get searchRecordsCount(){
        return this._searchRecords.length > 0;
    }

    get comboboxGroupClass() {
        let base = ''
        if(this.multiEntity) {
            base += 'slds-combobox-group'
        }
        if(this.multiselectable && this.hasSelection) {
            base += ' slds-has-selection'
        }
        return base
    }

    get txtclassname() {
        let base = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
        if(!this.selectRecordName || (this._searchRecords.length || this.loadingText)) {
            base += ' slds-is-open'
        }
        return base;
    }

    get comboboxContainerClass() {
        let base = 'slds-combobox_container '
        if(this.multiEntity) {
            base += 'slds-combobox-addon_end'
        }
        if(this.hasSelection) {
            base += ' slds-has-selection'
        }
        return base
    }

    get hasSelection() {
        return this.selectedRecords && this.selectedRecords.length > 0
    }

    get hasError() {
        return this.required && this.hasBeenOpened && this.hasSelection === false  && !this.hasFocus|| (this.selectRecordName !== '' && this.hasSelection === false && !this.hasFocus)
    }

    get errorMessage() {
        return this.selectRecordName !== '' && this.hasSelection === false && !this.hasFocus
            ? this.labels.inputInvalid
            : this.required && this.hasBeenOpened && this.hasSelection === false && !this.hasFocus ? this.labels.inputRequired : ''
    }

    get formElementClass() {
        let base = "slds-form-element"
        if(this.hasError) {
            base += ' slds-has-error'
        }
        return base
    }

    get comboboxInputClass() {
        let base = 'slds-input slds-combobox__input '
        if(this.hasSelection && !this.multiselectable)  base += ' slds-combobox__input-value'
        return base
    }

    get showLabel() {
        return this.label !== '' && this.label !== undefined
    }

    get asterisk() {
        return this.required ? ' *' : ''
    }


    onChangeObjectType(ev) {
        this._sobject = ev.detail.value;
        this._iconName = this.entities.find(ent => ent.value === this._sobject).iconPath
    }

    toggleModal(evt){
        if(!this.disabled && !this.disableActionLoupe){
            this.modalLookup = !this.modalLookup

            // Si las limpio me cargo lo seleccionado, se queda así de momento
            if(evt && (evt.detail === 'cancel' || evt.detail === 'close')){
                this.resetData(true)
                this.selectedRecords = this.selectedItems

            }

            // Indica que se está cerrando el modal
            if(!this.modalLookup){
                this.emitSelectedRecords(evt.detail);
            }
        }
    }

    onsaveModal() {
        console.log('omLookup --> onsaveModal')
        this.dispatchEvent(new CustomEvent('save', { detail: this.normalizeSelection(), bubbles: true, composed: true}))
        this.dispatchEvent(new CustomEvent('selected', { detail: this.normalizeSelection(), bubbles: true, composed: true}))
        this.modalLookup = !this.modalLookup
    }

    onselection(evt){
        console.log('onLookup --> onSelection')
        if(evt.detail.length === 0) {
            this.resetData(true)
        } else {
            this.selectRecordsFromTable(evt.detail)
        }
    }

    /**
     * @private
     */
    onblurInput(){
        this.messageFlag = false;
        this.loadingText = false;
        this._searchRecords = [];
        this.hasFocus = false
    }

    onFocus() {
        this.hasFocus = true
    }

    /**
     * Click on search icon inside the input text
     * @private
     */
    onIconSearchClick() {
        console.log('omLookup --> onIconSearchClick')
        this._searchRecords = []

        // check existing fields and columns
        if(!this.sobjectFields || (this.sobjectFields && this.sobjectFields.length === 0)) {
            this.sobjectFields = this.fieldToShow
        }

        if(!this.tableColumns || (this.tableColumns && this.tableColumns.length === 0)) {
            this.tableColumns = [{label: this.fieldToShow, fieldName: this.fieldToShow}]
        }

        // Local search. Records --> rows
        if (this._sobject === '') {
            this.rows = []
            this.records.forEach( el => {
                this.rows.push({ [this.fieldToShow] : el })
            })
            this.uniqueField = this.fieldToShow
        }

        this.toggleModal()
    }

    resetAndSearch(ev) {
        console.log('omLookup --> resetAndSearch')
        if(this.disabled) return
        const fromButton = ev.target.dataset.action === 'clear'

        if(!this.multiselectable) {
            this.resetData(fromButton)
        }

        // Focus again the input if clicking on the clear icon button
        if(fromButton) {
            // eslint-disable-next-line
            setTimeout(() => {
                this.template.querySelector('input').focus()
            })
        }
        this.searchField()
    }

    searchField(event) {
        // console.log('omlookup --> search field', this._searchRecords)

        this.hasBeenOpened = true

        if(event){
            this.selectRecordName = this.selectRecordName === event.target.value ? '' : event.target.value
            this._value = event.target.value ? '' : event.target.value
        }

        // server side search
        if (this.sobject) {
            if (this._searchThrottlingTimeout) {
                clearTimeout(this._searchThrottlingTimeout)
            }
            // eslint-disable-next-line @lwc/lwc/no-async-operation
            this._searchThrottlingTimeout = setTimeout(() => {
                this.loadingText = true;
                getResults({
                    objectApiName: this.sobject,
                    fields: this.fields && this.fields.length > 0 ? this.fields.toString() : this.fieldToShow,
                    value: this.selectRecordName ? this.selectRecordName : '',
                    filter: this.filter ? this.filter : '',
                    sosl: this.sosl,
                    recordsNumber: this.recordsNumber
                })
                    .then(result => {
                        this._searchRecords = result.map(element => ({recId: element.Id, recName: element[this.fieldToShow]}))

                        console.log('omlookup --> search field - server search ', JSON.stringify(this._searchRecords, null, 2))


                        this.loadingText = false;

                        // this.txtclassname =  result.length > 0 ? 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-is-open' : 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
                        if(this.selectRecordName.length > 0 && result.length === 0) {
                            this.messageFlag = true;
                        }
                        else {
                            this.messageFlag = false;
                        }

                        this.iconFlag = true;
                        this.clearIconFlag = false
                    })
                    .catch(error => {
                        this.iconFlag = false;
                        this.messageFlag = false;
                        this.loadingText = false;
                        this._searchRecords = [];
                        console.log(`-------error-------------${ JSON.stringify(error)}`);
                    });
                this._searchThrottlingTimeout = null;
            }, 500)
        }
        // Local search
        else {
            // console.log('omlookup --> search field - localsearch', this.selectRecordName)
            this.loadingText = true;
            this._searchRecords = this.records
                .filter((rec) => {
                    if(typeof rec === 'string') {
                        return rec.toLowerCase().indexOf(this.selectRecordName.toLowerCase()) !== -1
                    }
                    return rec[this.fieldToShow] && (rec[this.fieldToShow].toLowerCase().indexOf(this.selectRecordName.toLowerCase()) !== -1)

                })
                .map((rec) => {
                    if(typeof rec === 'string') {
                        return { recId: rec, recName: rec }
                    }
                    return { recId: rec[this.keyField], recName: rec[this.fieldToShow] }

                })
            console.log(`omlookup --> search field - localsearch${  JSON.stringify(this._searchRecords, null, 2)}`)
            this.loadingText = false;
            this.iconFlag = true;
            this.clearIconFlag = false
        }

    }

    /**
     * Called when mousesdown on one combobox element. Selects the record
     * @param {Event} event The mousedown event
     * @private
     */
    setSelectedRecord(event) {
        this.selectSingleRecord(event.currentTarget.dataset.id, event.currentTarget.dataset.name)
        this.emitSelectedRecords();
    }

    /**
     * Selects from table or listview
     * @param {string} selection Array of selected rows
     * @private
     */
    selectRecordsFromTable(selection) {
        this.setSelectionFlags()

        // TODO: is this necesary
        this.selectRecordName = selection[0].item[this.fieldToShow];
        this.selectedRecords = selection.map(row => {
            const sel = row.item
            sel._name = row.item[this.fieldToShow]
            sel._icon = this._iconName
            return sel;
        })
    }

    /**
     * Sets the selection flags when selecting a record
     * @private
     */
    setSelectionFlags() {
        // this.txtclassname =  'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
        this.iconFlag = false;
        this.clearIconFlag = true;
        this.inputReadOnly = true;
    }

    normalizeSelection () {

        return this.selectedRecords
            ? this.selectedRecords.map(sel => {
                const norm = { ...sel}
                delete norm._name
                delete norm._icon
                return norm;
            })
            : []
    }

    /**
     * Selects single record from dropdown
     * @param {string} id Id of the record
     * @param {string} fieldToShowvalue Value of the displayed field
     * @private
     */
    selectSingleRecord(id, fieldToShowvalue) {
        this.setSelectionFlags()

        if (!this.multiselectable) {
            this.selectRecordName = fieldToShowvalue;
            this.selectedRecords = [];
        }

        const selectedRecord = { Id : id }
        selectedRecord[this.fieldToShow] = fieldToShowvalue;

        // For selection iterations
        selectedRecord._name = fieldToShowvalue;
        selectedRecord._icon = this._iconName;

        this.selectedRecords.push(selectedRecord)
    }

    handleRemoveSelection (ev) {
        const {id} = ev.target.dataset
        this.selectedRecords = this.selectedRecords.filter(rec => rec.Id !== id)
        this.emitSelectedRecords()
    }

    resetData(fullReset) {
        const elm = this.template.querySelector('#om-lookup-input')
        if(elm){
            elm.value = ''
        }

        if(fullReset) {
            this.selectRecordName = '';
        }

        this.inputReadOnly = false;
        this.iconFlag = true;
        this.clearIconFlag = false;
        if(this.selectedRecords && this.selectedRecords.length > 0) {
            this.selectedRecords = [];
            this.emitSelectedRecords();
        }
    }

    /**
     * Emits the selection to parent components
     * @private
     * @fires selected
     */
    emitSelectedRecords() {
        console.log('omLookup --> emitSelectedRecords')
        // Solo se enviará el evento si no estamos en modo modal, ya que en modo modal, los registros seleccionados se enviarán al cerrar el modal.
        // o si acabamos de cerrar el modal.
        if(!this.isButton || !this.modalLookup){
            /**
             * Event with the selected records by the user
             * @event selected
             * @type array
             */
            this.dispatchEvent(new CustomEvent('selected', { detail: this.normalizeSelection(), bubbles: true, composed: true}))
        }
    }

}