/**
 * Custom datetime implmentation using "momentjs"
 * @module OmFormattedDatetime
 * @version 1.0.0
 */
import { LightningElement, api, track } from 'lwc';
import { loadScript } from 'lightning/platformResourceLoader';
import momentjs from '@salesforce/resourceUrl/moment';

export default class OmFormattedDatetime extends LightningElement {

    _rowValue
    /**
     * Datetime value
     * @type Object
     * @default ''
     */
    @api
    get rowValue() {
        return this._rowValue
    }
    set rowValue(val) {
        this._rowValue = val;
        this.setFormattedValue()
    }
   
    /**
     * Datetime format to show (in moment.js notation). Example: 'DD MM YYYY hh:mm:ss'
     * @type String
     * @default ''
     */
    @api datetimeFormat = '';

    @track formattedValue;

    momentjsInitialized = false;

    connectedCallback() {  
        if (this.momentjsInitialized) {
            this.setFormattedValue();
            return;
        }

        this.momentjsInitialized = true;

        loadScript(this, momentjs)
            .then(() => {
                this.setFormattedValue();
            })
            .catch(error => {
                console.error(error);
            });
      }

    setFormattedValue() {
        if (this._rowValue && typeof(moment) === typeof(Function)){
            this.formattedValue = window.moment(new Date(this._rowValue)).format(this.datetimeFormat);
        }
    }
}