/**
 * Component to be used by OmDatatable component to render custom markup on the details section for each row
 * @module OmDatatableDetails
 * @version 1.0.0
 */
import { LightningElement, api, track } from 'lwc';
import {default as mainTemplate} from './omDatatableDetails.html'

export default class OmDatatableDetails extends LightningElement {

  /**
   * Custom template to render
   * @type Template
   */
  @api detailsTemplate;

  /**
   * Row definition to work with
   * @type Object
   */
  @api row;
  
  _dataFn;

  @api 
  get detailsData () {
    return this._dataFn
  }

  set detailsData (dFn) {
    this._dataFn = dFn
    this.data = dFn(this.row)
  }

  @track data;

  /**
   * Renders the default template or the one specified in the column (only in body cells)
   */
  render() {
    return !this.detailsTemplate ? mainTemplate : this.detailsTemplate;
  }

  /**
   * Emits a given event name in 'data-event' getting the detail values from data- attributes
   * For its use in replaced templates
   * @param {Event} event The click event
   */
  sendEventFromDataset(event) {
    let eventname = event.target.dataset.event
    let detailObject = {'eventName':event.type}
    Object.keys(event.target.dataset).forEach(dsKey =>{
        if(dsKey !== 'event') {
            detailObject[dsKey] = event.target.dataset[dsKey]
        }
    })
    console.log('Dispatching ' + eventname + ' with ' + JSON.stringify(detailObject))
    this.dispatchEvent( new CustomEvent(eventname, { detail: detailObject, bubbles: true, composed: true }));
  }
}