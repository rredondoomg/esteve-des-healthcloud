/**
 * Module for perfom dataload of little CSVs using APEX
 * @module OmTinyDataLoader
 * @version 1.0.0
 */

import { LightningElement, api, track } from 'lwc'
import { loadScript } from 'lightning/platformResourceLoader'
import papaparse from '@salesforce/resourceUrl/papaparse'
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import saveRecords from '@salesforce/apex/OM_LWC_pack.saveRecords'

/**
 * Lightning component 2
 * @class OmTinyDataLoader
 * @classdesc Lightning component
 * @memberof module:OmTinyDataLoader
 */
export default class OmTinyDataLoader extends LightningElement {
  /**
   * SObject API name
   * @type string
   * @default ''
   */
  @api sobject = ''

  /**
   * DML operation to run
   * @type string
   * @default 'insert'
   */
  @api operation = 'insert'

  /**
   * If operation is 'upsert', the full qualified external ID field API name to use (should exists in CSV file)
   * @type string
   * @default null
   */
  @api externalId = null

  /**
   * Custom field mapping
   * @type object
   */
  @api mapping = null

  /***
   * Download result files
   * @type boolean
   */
  @api download = false

  /**
   * Show results in tables
   * @type boolean
   */
  @api showResults = false

  /**
   * Component is parsing and sending data
   * @type boolean
   * @private
   */
  @track isWorking = false

  //Result handling
  @track displayResults = false
  @track resultColumns = []

  /**
   * Result collections
   * @private
   */
  @track resultErrors = []
  @track resultSuccesses = []

  //Default table/CSV columns
  defaultColumns = [
    { label: 'Message', fieldName: 'message'}
  ]

  get successLabel() {
    return 'Success (' + this.resultSuccesses.length + ')'
  }

  get errorLabel() {
    return 'Errors (' + this.resultErrors.length + ')'
  }

  // Private property to track static on static resource
  papaInitialized = false

  //Reference to papaparse global object. To be setted on loadScript promise resolver
  Papa

  /**
   * Gets the file, process the CSV into a JSON objets and sends to APEX to further process
   * @param {Event} ev The event emitted by the input file
   * @private
   */
  onDroppedFile(ev) {

    this.isWorking = true
    this.resultColumns = []
    this.resultErrors = []
    this.resultSuccesses = []
    this.displayResults = false

    if(!this.sobject) {
      this.dispatchEvent(new ShowToastEvent({ title: 'Load error', message: 'SObject not specified', variant: 'error', mode: 'dismissable'}))

      /**
       * Error event
       * @event error
       * @type string
       */
      this.dispatchEvent(new CustomEvent('error', { detail: 'SObject not specified', composed: true, bubbles: true } ))
      this.isWorking = false
      return
    }

    let fileList = ev.target.files
    const file = fileList[0]

    //TODO: Review second argument (papa config)
    this.Papa.parse(file, {
      header: true,
      skipEmptyLines: true,
      error: (err) =>
      {
        this.dispatchEvent(new ShowToastEvent({ title: 'Load error', message: 'Error parsing file ' + err, variant: 'error', mode: 'dismissable'}))
        this.dispatchEvent(new CustomEvent('error', { detail: 'Error parsing file ' + err, composed: true, bubbles: true } ))
      },
      complete: (parsed) =>
      {
        let records = parsed.data.map(rec => {
          let r = rec
          // Add this key to allow APEX to deserialize sobject correcly
          r.sobjectType = this.sobject
          return r
        })

        //Add fields to exported/datatable info
        let cols = parsed.meta.fields.map(f => {
          return {label: f, fieldName: 'record.'+f}
        })

        this.resultColumns = cols.concat(this.defaultColumns)
        console.log('resulting columns')
        console.log(JSON.stringify(this.resultColumns))

        // executed after all files are complete. Call APEX to save records
        saveRecords({
          records: records,
          action: this.operation,
          externalId: this.externalId
        })
        .then(res => {
          console.log('@@ OmTinyDataLoader saveRecords success')
          console.log(JSON.stringify(res))
          this.resultErrors = res.error
          this.resultSuccesses = res.success
          if(this.showResults) {
            this.displayResults = true
          }
          if(this.download) {
            this.downloadResults('success')
            this.downloadResults('error')
          }
          this.isWorking = false
          /**
           * Completed with success event
           * @event complete
           */
          this.dispatchEvent(new CustomEvent('complete', { detail: null, composed: true, bubbles: true } ))
        })
      }
    })

  }

  /**
   * Downloads a generated CSV file
   * @param {String} type Type of the download CSV. success or error
   * @private
   */
  downloadResults(type) {
    let data
    if(type === 'success') {
      data = this.resultSuccesses
    } else {
      data = this.resultErrors
    }
    console.log('@@ OmTinyDataLoader downloadResults')
    let successcsv = this.Papa.unparse({
      "fields": this.resultColumns.map(col => col.label),
      "data": data.map(res => {
        let value = []
        Object.keys(res.record).forEach(key => value.push(res.record[key]))
        value.push(res.message)
        return value
      })
    })

    let uri = window.URL.createObjectURL(new Blob([successcsv], { type: 'application/octet-stream' }))
    let downloader = this.template.querySelector('a')
    downloader.setAttribute('href', uri)
    downloader.setAttribute('download', type + '.csv')
    downloader.click()
  }

  /**
   * LWC lifecycle hook. Loads Papaparse.js
   * @private
   */
  connectedCallback() {

    if (this.papaInitialized) return

    this.papaInitialized = true

    loadScript(this, papaparse)
      .then(() => {
          this.Papa = window.Papa
          this.error = null
      })
      .catch(error => {
          this.error = error
          this.dispatchEvent(new ShowToastEvent({ title: 'Load error', message: 'Cannot load required dependencies', variant: 'error', mode: 'dismissable'}))
          this.dispatchEvent(new CustomEvent('error', { detail: 'Cannot load required dependencies', composed: true, bubbles: true } ))
      })
  }
}