/**
 * SLDS implementation on a file preview
 * @module OmFilePreview
 * @version 1.0.0
 */
import { LightningElement, api } from 'lwc';

export default class OmFilePreview extends LightningElement {
  /**
   * The name of the file
   * @type string
   * @default ''
   */
  @api name = ''

  /**
   * The SLDS icon path for the image. Usually used 'doctype:' icon set
   * @type string
   * @default ''
   */
  @api iconName = 'doctype:unknown'

  /**
   * The icon image. If existing, will have precedence over 'iconPath'
   * @type string
   * @default ''
   */
  @api iconImg = ''

  /**
   * The file thumbnail URL
   * @type string
   * @default ''
   */
  @api thumbnail = ''

  /**
   * The file download URL
   * @type string
   * @default ''
   */
  @api downloadUrl = ''

  get hasIconImage() {
    return this.iconImg !== ''
  }
  
  get hasDownloadUrl() {
    return this.downloadUrl !== ''
  }

  onEyeClick () {
    this.dispatchEvent(new CustomEvent('previewclick'))
  }
}