/**
 * Custom implementation of GoogleDriveExplorer. Uses Custom metadata records to cinfure and proxies all Drive data to Salesforce records
 * @module OmContentManager
 */
import { api, track, LightningElement } from 'lwc';
import upsertContents from '@salesforce/apex/OM_ContentService.upsertContents';
import deleteContents from '@salesforce/apex/OM_ContentService.deleteContents';
import { processContentSettings, onProviderCredentials, onSignOut, handleError } from 'c/omContentUtils';

import { ShowToastEvent } from 'lightning/platformShowToastEvent';


// Labels
import omCannotRetrieveSettings from '@salesforce/label/c.omCannotRetrieveSettings'

/**
 * @class OmContentManager
 */
export default class OmContentManager extends LightningElement {

  // Private record id
  _recordId = null;
  _customMetadataName = null;
  _initialFolderId = null;
  _globalSearch = false

  /**
   * The record Id where the component has been added
   * @type String
   * @default undefined
   */
  @api 
  get recordId () {
    return this._recordId
  }

  set recordId (value) {
    console.log('Setter recordId')
    this._recordId = value
  }

  /**
   * The unique name of the OM_ContentSetting__c to use
   * @type String
   * @default undefined
   */
  @api 
  get settingToUse () {
    return this._customMetadataName
  }

  set settingToUse (value) {
    this._customMetadataName = value
    
    // Fetches configuration from Apex
    processContentSettings(value, this._recordId, true).
      then(({  userAccess, settings, settingName, customAppProperties, providerImplementation, baseColumnNames}) => {
        this.providerImplementation = providerImplementation;
        this.providerImplementation.setTitle(this.title)
        this.providerImplementation.setLogoUrl(this.logoUrl)
        this.settings = settings;
        this.settings.OM_Initial_folder_id__c = this._initialFolderId ? this._initialFolderId : this.settings.OM_Initial_folder_id__c

        this.settingName = settingName;
        this.userAccess = userAccess;
        this.customAppProperties = customAppProperties
        this.baseColumnNames = baseColumnNames
        this.initialize();
      })
      .catch(e => {
        this.errors = handleError(e, omCannotRetrieveSettings)
      })
  }

  /**
   * The initial folder Id. Will override the Id fetched form config record
   * @type String
   * @default undefined
   */
  @api 
  get initialFolderId () {
    return this._initialFolderId
  }

  set initialFolderId (value) {
    if (value) {
      this._initialFolderId = value
    }
  }

  @api logoUrl

  @api title

  @api 
  get globalSearch () {
    return this._globalSearch
  }

  set globalSearch (value) {
    this._globalSearch = value
  }

  @api displayMode = 'table'

  initialize() {
    this.initialized = true;
    this.errors = null;
  }

  renderedCallback() {
    if(!this._explorerRef) {
      this._explorerRef = this.template.querySelector('c-om-google-drive-explorer');
    }
  }

  // Actual error if any
  @track errors = null

  // If any errors happened
  get hasErrors() {
    return this.errors != null
  }
  // The custom metadata has been retrieved and the Explorer can be displayed
  @track initialized = false

  // Concrete implmentation form selected provider
  @track providerImplementation = false

  @api
  createFolder(file) {
    return this._explorerRef?.createFolder({...file});
  }

  @api
  upload(fileList, folderid) {
    return this._explorerRef?.upload(fileList, folderid);
  }

  @api refresh() {
    this._explorerRef?.refreshPage();
  }

  // The settings name selected
  @track settingName = ''
  
  // The custom properties / fields specified on the 
  @track customAppProperties = null

  // The settings record selected
  @track settings = {}

  // The current file beign worked on in the child explorer
  @track currentFile = null

  // If the file information modal in the explorer is opened
  @track fileOpened = false

  // The current user access
  @track userAccess = null

  @api fileDecorator;

  @track defaultSiteId = ''
  @track defaultDriveId = ''

  @track baseColumnNames = []

  @track langFinder = (file) => {
    return file.appProperties && file.appProperties.language ? file.appProperties.language : 'en-US'
  }

  get customColumns() {
    if (this.customAppProperties) {
      let cloned = JSON.parse(JSON.stringify(this.customAppProperties))
      return cloned.filter(c => c.asColumn === true).map(c => {
        return { sortable: false, hidable: false, fieldName: 'appProperties.' + c.key, label: c.label }
      })
    }

    return []
  }

  get disableSignOut() {
    return this.settings.OM_Auth_type__c === 'Shared'
  }

  async onProviderCredentials () {
    this.userAccess = await onProviderCredentials(this.settingName)
  }


  saveContents(files) {
    console.log('@@@ OMCOntentManager -> Sending contents to save ' + JSON.stringify(files))

    upsertContents({
      recordId: this._recordId,
      settingsName: this.settingToUse,
      contents: files
    })
    .then((result) => {
        console.log('@@@ OMCOntentManager -> Contents saved ' + JSON.stringify(result))
    })
    .catch((error) => {
      console.log('@@@ OMCOntentManager -> Contents saved error' + JSON.stringify(error))
      let err = handleError(error)
      this.dispatchEvent(new ShowToastEvent({ title: err.title, message: err.message, variant: 'error', mode: 'dismissable'}));
        //this.errors = 'Error received: code' + error.errorCode + ', ' + 'message ' + error.body.message;
    });
  }

  removeContents(files) {
    console.log('@@@ OMCOntentManager -> Sending contents to delete ' + JSON.stringify(files))

    deleteContents({
      settingsName: this.settingToUse,
      contents: files
    })
    .then((result) => {
        console.log('@@@ OMCOntentManager -> Contents deleted ' + JSON.stringify(result))
    })
    .catch((error) => {
      console.log('@@@ OMCOntentManager -> Contents deleted error' + JSON.stringify(error))
      let err = handleError(error)
      this.dispatchEvent(new ShowToastEvent({ title: err.title, message: err.message, variant: 'error', mode: 'dismissable'}));
      //this.errors = 'Error received: code' + error.errorCode + ', ' +
            //'message ' + error.body.message;
    });
  }

  onProviderSignOut() {
    onSignOut(this.settingName)
  }
  onFileDeleted(ev) {
    console.log('@@@ OMCOntentManager ->  File deleted ' + JSON.stringify(ev.detail))
    this.removeContents([ev.detail])
  }

  onDriveFiles(files) {
    console.log(files)
  }

  onFileUploaded(ev) {
    console.log('@@@ OMCOntentManager ->  File uploaded ' + JSON.stringify(ev.detail))
    this.saveContents([ev.detail])
  }

  onFileUpdated(ev) {
    console.log('@@@ OMCOntentManager ->  File updated ' + JSON.stringify(ev.detail))
    this.saveContents([ev.detail])
  }

  onDriveSelection(selection) {
      console.log(selection)
  }

  onDriveFilters(filters) {
      console.log(filters)
  }

  onDriveAiSuggestions(suggs) {
      console.log(suggs)
  }

  onDriveError(err) {
      console.error('Error in DriveExplorer catched: ' + err)
  }

  onOpenFile(ev) {
    if(ev.detail) {
      this.fileOpened = true
      this.currentFile = ev.detail
    } else {
      this.fileOpened = false
      this.currentFile = null
    }
  }
}