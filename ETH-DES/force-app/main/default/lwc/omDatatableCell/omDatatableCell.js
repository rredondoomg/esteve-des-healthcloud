/**
 * Datatable cell component. Should not be used outside OmDatatable
 * @module OmDatatableCell
 * @version 1.1.2
 * @requires OmDatatableInput
 * @requires OmDatatableUtils
 * @see OmDatatable
 */
import { LightningElement, api, track } from 'lwc';
import { getItemValue, getTypeFromColumn, getInputFormatterFromColumn, getTypeAttributes, getCellAttributes} from 'c/omDatatableUtils'
import { fireEvent } from 'c/pubsub';
import omLog from 'c/omLog';
import mainTemplate from './omDatatableCell.html'

const typesWithAttributes = [
    'actions',
    'date',
    'datetime',
    'number',
    'url',
    'button',
    'button-icon',
    'image',
    'svg'
]

/**
 * @class OmDatatableCell
 */
export default class OmDatatableCell extends LightningElement {

    /**
     * If the cell is in a tile
     * @type boolean
     * @default false
     */
    @api inTile;

    /**
     * If the datatable is being exported
     * @type boolean
     * @default false
     */
    @api exporting = false;

    /**
     * The column definition
     * @type {DatatableColumn}
     * @default undefined
     */
    @api
    get column() {
        return this._column;
    }

    set column(val) {
        this._column = val
        this.inputType = getTypeFromColumn(this.column)
        this.setRowValue();
    }

    /**
     * the table row
     * @type object
     * @default undefined
     */
    @api
    get row() {
        return this._row;
    }

    set row(val) {
        this._row = val;
        this.setRowValue();
    }


    /**
     * The row index in collection
     * @type number
     * @default undefined
     */
    @api rowIndex


    /**
     * The column index in collection
     * @type number
     * @default undefined
     */
    @api colIndex

    /**
     * The event name to emit on clickable cells
     * @type string
     * @default ''
     */
    @api eventMethod = '';

    /**
     * If the tooltip is visible
     * @type boolean
     * @default false
     */
    @api visibleTooltip = false;

    /**
     * Datetime format to show (in moment.js notation). Example: 'DD MM YYYY hh:mm:ss'
     * @type string
     * @default ''
     */
    @api datetimeFormat = '';

    /**
     * Datetime format to show (in moment.js notation). Example: 'DD MM YYYY'
     * @type string
     * @default ''
     */
    @api dateFormat = '';

    /**
     * To disable paddings
     * @type boolean
     * @default false
     */
    @api compact = false;

    /**
     * The CSS inline style to apply to each cell. Receives a "cell" object and should return a string
     * @type function
     * @params "cell" Each cell
     * @default undefined
     */
    @api cellStyle

    /**
     * Function that return a boolean with disabled property for the cell
     * @type function
     * @params "cell" Each cell
     * @default undefined
     */
    @api
    get disabledCell() {
        return this._disabledCellFn;
    }

    set disabledCell(val) {
        this._disabledCellFn = val;
        this._disabledCell = val && (typeof val === 'function') ? val(this.rowValue, this.row, this.column) : ''
    }

    @track _disabledCell = false;

    _disabledCellFn = false;


    /**
     * The value is beign editing from in-cell button
     * @private
     */
    @track cellEditing = false

    /**
     * Starts inline edit mode in cell
     */
    @api
    startCellEditing(ev) {

        // Dont fire cellclick or rowclick when opening editing
        if(ev) ev.stopPropagation()

        this.cellEditing = true

        // eslint-disable-next-line
        setTimeout(() => {
            this.template.querySelector('c-om-datatable-input').focus()
        }, 100)

        this.dispatchEvent(new CustomEvent('celledit', {detail: true, bubbles: false}))
    }

    @api
    stopCellEditing() {
        this.cellEditing = false
        this.dispatchEvent(new CustomEvent('celledit', {detail: false, bubbles: false}))
    }

    @api
    getContentsWidth() {
        const wce = this.template.querySelector('.width-calculation-element');
        return wce?.scrollWidth || 0;
    }

    /* get actionsHidden () {
        return this.hideColumnActions || this.column.hideActions
    } */

    /**
     * Emits a given event name in 'data-event' getting the detail values from data- attributes
     * For its use in replaced templates
     * @param {Event} event The click event
     * @private
     */
    sendEventFromDataset(event) {
        const eventname = event.target.dataset.event
        const detailObject = {}
        Object.keys(event.target.dataset).forEach(dsKey =>{
            if(dsKey !== 'event') {
                detailObject[dsKey] = event.target.dataset[dsKey]
            }
        })
        omLog.info(`Dispatching ${  eventname  } with ${  JSON.stringify(detailObject)}`)
        this.dispatchEvent( new CustomEvent(eventname, { detail: detailObject, bubbles: true, composed: true }));
    }

    onInputTab(ev) {
        ev.stopPropagation()
        this.dispatchEvent( new CustomEvent('tabulation', { detail: { row: this.row,  rowIndex: this.rowIndex, colIndex: this.colIndex }, bubbles: true, composed: true }));
    }

    /**
     * Private var to store the temporal row value while it is being edited
     */
    temporalValue = ''

    /**
     * Renders the default template or the one specified in the column (only in body cells)
     * @private
     */
    render() {
        return (!this.column || !this.column.component || !this.column.component.template) || this.isHeader ? mainTemplate : this.column.component.template;
    }


    get hasCustomComponent() {
        return this.column && this.column.component && this.column.component.name
    }

    get hasCustomTemplate() {
        return this.column && this.column.component && this.column.component.template
    }

    get _cellStyle() {
        return (this.cellStyle && (typeof this.cellStyle === 'function') ? this.cellStyle(this.rowValue,this.row,this.column) : '')
    }

    setRowValue() {
        if(this._row && this._column) {
            this.rowValue = getItemValue(this.row, this.column);
            this.isEditable = this.column.editable && !this.column.value
            if(this.inputType && typesWithAttributes.includes(this.inputType)) {
                this.typeAttributes = getTypeAttributes(this.column, this.row, this.rowValue)
            }
            this.cellAttributes = getCellAttributes(this.column, this.row)
        }
    }

    @track _row;

    @track rowValue = '';

    @track typeAttributes = {};

    @track cellAttributes = {};

    @track isEditable = false;

    @track inputType;

    get picklistValue() {
        const rval = this.rowValue != null ? `${this.rowValue  }` : '';
        if(this.column && this.column.options) {
            const val = this.column.options.find(op => op.value === rval)
            return val ? val.label : rval
        }
        return rval
    }

    get isDatetime(){ return this.inputType === 'datetime' }

    get isDate(){ return this.inputType === 'date' }

    get isTooltip(){ return this.inputType === 'tooltip'}

    get isNumber(){ return this.inputType === 'number' }

    get isText(){ return this.inputType === 'text' || this.inputType === 'textarea'}

    get isPhone(){ return this.inputType === 'tel'}

    get isUrl() { return this.inputType === 'url' }

    get isPicklist(){ return this.inputType === 'picklist'}

    get isHtml() {return this.inputType === 'html'}

    get isCheckbox(){ return this.inputType === 'checkbox'}

    get isActions(){ return this.inputType === 'actions'}

    get isButton(){ return this.inputType === 'button'}

    get isImage(){ return this.inputType === 'image'}

    get isSvg(){ return this.inputType === 'svg'}

    get isButtonIcon(){ return this.inputType === 'button-icon'}

    get noTypeDefined() {
        return this.column && !this.column.type
    }

    get isCustomDatetimeFormat(){
        return this.datetimeFormat;
    }

    get showEdition() {
        return this.isEditable && ((this.row && this.row.editing) || this.cellEditing)
    }

    get showEditionButton() {
        return this.isEditable && !this.row.editing && !this.cellEditing && !this.inTile
    }

    get typeHasIcon() {
        return this.typeAttributes.iconName !== undefined && this.typeAttributes.iconName !== ''
    }

    get cellHasPrevIcon(){
        return this.cellAttributes && this.cellAttributes.iconName !== '' && this.cellAttributes.iconPosition === 'left'
    }

    get cellHasPostIcon(){
        return this.cellAttributes && this.cellAttributes.iconName !== '' && this.cellAttributes.iconPosition === 'right'
    }

    get cellHasLabel(){
        if(this.cellAttributes.iconLabel){
            return true;
        }
        return false;
    }

    get cellGridClass(){
        let gridClass = `slds-grid om-datatable-cell  ${  this.compact ? ' compact ' : ''}`

        if(!this.isActions) {
            gridClass += `${  this.showEdition ? ' ' : ' slds-truncate '  }${this.column.isClickable ? ' clickable ' : ''}`
        }

        if(this.cellAttributes && this.cellAttributes.alignment){
            const {alignment} = this.cellAttributes;
            switch(alignment){
            case 'left':
                gridClass += ' slds-grid_align-spread';
                break;

            case 'center':
                gridClass += ' slds-grid_align-center';
                break;

            case 'right':
                gridClass += ' slds-grid_align-end';
                break;

            default:
                break;
            }
        }

        return gridClass;
    }

    /* get cellAttributes() {
        return getCellAttributes(this.column, this.row)
    } */


    get inputFormatter(){
        return getInputFormatterFromColumn(this.column)
    }

    get cellInnerClass(){
        return this.column && this.column.wrapText === true ? 'slds-cell-wrap' : 'slds-truncate'
    }

    connectedCallback(){
        this.cellEditing = this.isCheckbox;
    }

    /**
     * When the column is clickable, emits the 'eventMethod' variable event
     * @param {Event} evt The click event on cell
     * @fires clickedcell
     * @private
     */
    emitRow(evt) {
        if (this.column.isClickable) {

            // Adding column info in emitted row
            const info = JSON.parse(JSON.stringify(this.row));
            info.column = this.column;

            fireEvent(this.pageRef, 'clickedCell', {
                params: { detail: info, method: this.eventMethod }
            });

            evt.preventDefault();
            evt.stopPropagation();


            /**
             * Events emitted when a clickable cell is clicked
             * @event clickedcell
             * @type object
             */
            const clickedCell = new CustomEvent('clickedcell', { detail: info, bubbles: true, composed: true });
            this.dispatchEvent(clickedCell);
        }
    }

    handleRowActions(event) {
        const menuItem = this.row.actions.find((item) => item.name === event.detail.value);

        if(menuItem) {
            this.dispatchEvent( new CustomEvent('rowaction', { detail: { action: event.detail.value, row: this.row.item, column: this.column }, bubbles: true, composed: true }));
        }
    }


    fireRowAction(ev) {

        if(this.typeAttributes.name && this.typeAttributes.name !== ''){
            if(this.isUrl) ev.preventDefault()
            /**
             * Event containing the row action (for button, button-icons, )
             * @event rowaction
             * @type Object
             * @property {object} action - The action name specified in "typeAttributes"
             * @property {object} row - The row item data
             * @property {object} column - The column
             */
            this.dispatchEvent( new CustomEvent('rowaction', { detail: { action: this.typeAttributes.name, row: this.row.item, column: this.column }, bubbles: true, composed: true }));
        }

    }

    // NEW
    onInputChange(evt) {
        omLog.info(`omDatatablecell onInputChange with ${  evt.detail}`)
        this.temporalValue = evt.detail
        // Emit directly on checkboxes
        if(this.temporalValue === true || this.temporalValue === false) {
            this.emitValue(this.temporalValue);
        }
    }

    onInputBlur(evt) {
        omLog.info(`omDatatableCell onInputBlur with ${  evt.detail}`)
        if(this.inputType !== 'checkbox') {
            if((this.cellEditing || this.row.editing) && this.temporalValue) {
                this.emitValue(this.temporalValue);
                this.stopCellEditing()
            } else if(!this.temporalValue) {
                this.stopCellEditing()
            }
        }
    }

    onInputEnter(evt) {
        omLog.info('omDatatablecell onInputEnter')
        if((this.cellEditing || this.row.editing)) {
            this.emitValue(evt.detail);
            this.stopCellEditing()
        }
    }

    @api setCellValue(newValue) {
        this.emitValue(newValue);
    }

    /**
     * Emits the new value on edited cell
     * @param {any} value The value to emit
     *
     * @private
     */
    emitValue(value = '') {
        omLog.info(`omDatatableCELL emitValue with ${  value}`)
        const val = value // Sanitize?
        this.temporalValue = null

        /**
         * Event containing the new value introduced in the cell
         * @event newcellvalue
         * @type Object
         * @property {object} column - The fieldName of the column updated
         * @property {string} rowKey - The key of the row updated
         * @property {any} value - The new value introduced
         * @property {object} row - The full row information updated
         */
        this.dispatchEvent( new CustomEvent('newcellvalue', { detail: { column: this.column.fieldName, rowKey: this.row.key, value: val, row: this.row }, bubbles: true, composed: true }));
    }

}