import OmMicrosoftSharepointAPI from 'c/omMicrosoftSharepointAPI';

export default class OmOneDriveAPI extends OmMicrosoftSharepointAPI {
  
  provider = 'OneDrive'

  isFolder(file) {
    return file.folder && file.folder.childCount !== undefined;
  }

  getTags (file) {
    return file.description;
  }

  getLogoUrl() {
    return this.staticresourceUrl + '/images/OneDriveLogo.svg'
  }

  getTitle() {
    return 'OneDrive'
  }



  async list(isNext, filters, customProperties, paginator) {

    let request = {
        '$select': 'id,size,name,file,description,folder,content.downloadUrl,graph.sourceUrl,createdBy,lastModifiedBy,fileSystemInfo,webUrl',
        '$orderby': 'name asc',
        '$expand': 'thumbnails',
    };

    let searchItems = ['driveItem']

    return this.msList(request, searchItems, isNext, filters, customProperties, paginator)

  }
}