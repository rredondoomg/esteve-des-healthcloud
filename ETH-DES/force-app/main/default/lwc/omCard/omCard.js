/**
 * Custom SLDS card implementation
 * @module OmCard
 * @version 1.0.0
 */
import { LightningElement, api } from 'lwc';

export default class OmCard extends LightningElement {
    
    /********************** API **********************/
    
    /* Style */
    @api icon = 'custom:custom19';

    /* Labels */
    @api cardLabel = '';

    @api count;

    /**************************************************/


}