import { LightningElement, track, api, wire } from 'lwc';

import getPatientInfoJSON from '@salesforce/apex/OM_VidSigner_Controller.getPatientInfoJSON'
import getJSONVidSigner from '@salesforce/apex/OM_VidSigner_Controller.getJSONVidSigner'
import getLanguageSJON from '@salesforce/apex/OM_VidSigner_Controller.getLanguageSJON'
import getVisitType from '@salesforce/apex/OM_VidSigner_Controller.getVisitType'
import getVisitStatusEnCurso from '@salesforce/apex/OM_VidSigner_Controller.getVisitStatusEnCurso'
import getDNIPaciente from '@salesforce/apex/OM_VidSigner_Controller.getDNIPaciente'

import deviceType from '@salesforce/client/formFactor'


import { NavigationMixin } from 'lightning/navigation';

import Selecciona_Idioma from '@salesforce/label/c.Selecciona_Idioma';
import Tipo_Contrato from '@salesforce/label/c.Tipo_Contrato';
import Nombre_Tutor from '@salesforce/label/c.Nombre_Tutor';
import DNI_Tutor from '@salesforce/label/c.DNI_Tutor';
import DNI_Paciente from '@salesforce/label/c.DNI_Paciente';
import Ciudad_Firma from '@salesforce/label/c.Ciudad_Firma';
import En_Calidad from '@salesforce/label/c.En_Calidad';
import Firmar_Consentimiento from '@salesforce/label/c.Firmar_Consentimiento';
import Firmar_Baja from '@salesforce/label/c.Firmar_Baja';
import Firmar_Comp_Osasunbidea from '@salesforce/label/c.Firmar_Comp_Osasunbidea';
import ERROR_GENERAL from '@salesforce/label/c.ERROR_GENERAL';
import Clientes_OSASUNBIDEA from '@salesforce/label/c.Clientes_OSASUNBIDEA';
import Validacion_Campos from '@salesforce/label/c.Validacion_Campos';
import Validacion_Tutor from '@salesforce/label/c.Validacion_Tutor';
import Validacion_Tutor_Baja from '@salesforce/label/c.Validacion_Tutor_Baja';


export default class Firma_VidSigner extends NavigationMixin(LightningElement) {
    Label = {
        Selecciona_Idioma,
        Tipo_Contrato,
        Nombre_Tutor,
        DNI_Tutor,
        DNI_Paciente,
        Ciudad_Firma,
        En_Calidad,
        Firmar_Consentimiento,
        Firmar_Baja,
        Firmar_Comp_Osasunbidea,
        ERROR_GENERAL,
        Clientes_OSASUNBIDEA,
        Validacion_Campos,
        Validacion_Tutor,
        Validacion_Tutor_Baja
    };

    @api recordId;
    @track nif = "";
    @track nif_tutor ="";
    @track error = false;
    @track errorMessage = "";
    @track vError = false;
    @track vErrorMessage = '';
    
    // ET201801-246: Añadimos campos
    @track idiomaSelec = "ESP";
    // @track "languageList" type="Array";
    @track languageList = [];
    
    @track cName = "";
    @track city = "";
    @track contractType = "";

    // 405 - OSASUNBIDEA
    @track osasunbidea = false;
    @track en_calidad = "";

    @track showCMP = false;

    //Flags render buttons signing
    @track isAlta;
    @track isBaja;
    @track isVisita;

    @track isEnCurso;

    @track operationType;

    dniPaciente;

    connectedCallback(){
        console.log('Tipo de dispositivo --> ' +deviceType);
        this.getVisitStatusEnCurso();
        this.getDNIPaciente();
        // this.getVisitType();  
        // this.getPatientInfoJSONJS();
        // this.getLanguagesJS();
    }    

    startLogic(){ 
        this.getVisitType();  
        // this.getPatientInfoJSONJS(deviceType);
        this.getPatientInfoJSONJS();
        this.getLanguagesJS();
        this.showCMP = true;
    }

    handleClick (event) {
         var doc = event.currentTarget.value;

        if(event.currentTarget.value == 'cons'){
            this.operationType = 'Consentimiento';
        }else if(event.currentTarget.value == 'baja'){
            this.operationType = 'Baja';
        }else if(event.currentTarget.value == 'parte_visita'){
            this.operationType = 'Parte_visita';
        }

        this.getUrl(doc);
    }

    getVisitStatusEnCurso(){
        getVisitStatusEnCurso({recordId : this.recordId})
        .then(response => {
            this.isEnCurso = response;
        })
        .catch(error => {
            console.log('ERROR getVisitStatusEnCurso');
          
        })
    }

    getDNIPaciente(){
        getDNIPaciente({recordId: this.recordId})
        .then(response => {
            this.dniPaciente = response;
        }).catch(error => {
            console.log('ERROR getDNIPaciente');
        })
    }

    getVisitType(){
        getVisitType({recordId : this.recordId})
        .then(response => {
            console.log('Tipo de visita: '+response);

            if(response == 'Fin'){
                this.isAlta = false;
                this.isBaja = true;
                this.isVisita = false;
            }else if(response == 'Alta'){
                console.log('Es '+response);
                this.isAlta = true;
                this.isBaja = false;
                this.isVisita = false;
            }else{
                this.isAlta = false;
                this.isBaja = false;
                this.isVisita = true;
            }
            console.log('Va por success getVisityType');
        })
        .catch(error => {
            console.log('ERROR getVisitType');
        })
    }


    getPatientInfoJSONJS() {	
        console.log('Entra en getPatientInfoJSONJS');
        console.log('RecordId '+ this.recordId);
        getPatientInfoJSON({recordId : this.recordId})
        .then(response => {
            var result = response;
            var resultObject = JSON.parse(result);
            this.nif = resultObject.nif;
            this.contractType = resultObject.cType;

            //405
            var osasunbideaStr = Clientes_OSASUNBIDEA;
            var osasunbidea = osasunbideaStr ? osasunbideaStr.includes("[" + resultObject.cliAtlasId + "]"): false;
            this.osasunbidea = osasunbidea;
        })
        .catch(error => {
        console.log('ERROR getPatientInfoJSONJS');
            var errors = error;
            if (errors) {
                if (errors[0] && errors[0].message) {
                    console.log("Error message: " + 
                             errors[0].message);
                }
            } else {
                console.log("Unknown error");
            }
        })
		
	}

    getUrl(doc) {
        var nif = this.nif;
        var nif_tutor = this.nif_tutor; 
        var en_calidad = this.en_calidad; 
        var lan = this.idiomaSelec;
        var city = this.city;
        var cName = this.cName;
        var cType = this.contractType;
        
        //gestionamos campos obligatorios
        //si uno de los campos de tutor está relleno, deben de estarlo todos
        if(doc == 'cons' && ((nif_tutor && !cName) || (!nif_tutor && cName))){
            console.log('Validación 1');
            this.vError = true;
            this.vErrorMessage = Validacion_Tutor;
            return;
        }
        else if(doc == 'baja' && (nif_tutor || cName)){
            console.log('Validación 2');

            this.vError = true;
            this.vErrorMessage = Validacion_Tutor_Baja;
            return;
        }else if(doc == 'parte_visita'){
            console.log('Es parte visita.');
            console.log('CONTROLAR CAMPOS');
        }

        //La ciudad y el nif del paciente es obligatorio
        else if(!city || !nif){
            console.log('Validación 3');

            this.vError = true;
            this.vErrorMessage = Validacion_Campos;
            return;
        }else{
            console.log('Validación 4');

            this.vError = false;
        }

        //Validaciones Osasunbidea - 405
        if(doc == 'compOsasun' && ((!nif_tutor && (cName || en_calidad)) || (!cName && (nif_tutor || en_calidad)) || (!en_calidad && (nif_tutor || cName)))){
            console.log('Validación 5');
            
            this.vError = true;
            this.vErrorMessage = Validacion_Tutor;
            return;
        }

        var signatureNIF = (nif === "") ? this.dniPaciente : nif;

        console.log('signatureNIF vale: '+signatureNIF);
        console.log('this.dniPaciente vale: '+this.dniPaciente);
        console.log('nif vale: '+nif);
        console.log('Antes de llamar a vidSgirner ');

        getJSONVidSigner({
            recordId : this.recordId,
            nif : nif,
            doc : doc,
            lan : lan,
            cType: cType,
            city: city,
            cName: cName,
            nif_tutor: nif_tutor,
            tutorType: en_calidad,
            operationType : this.operationType/*,
            deviceType : deviceType   */
        })
        .then(res => {
            var result = res;
            
            if(result.success){

                this[NavigationMixin.Navigate]({
                    type: 'standard__webPage',
                    attributes: {
                        url: 'vidsigner://vidsignerbio/VID_SIGNATURES_USER/' + signatureNIF
                    }
                }).then(generatedUrl => {
                    window.open(generatedUrl);
                });
            
            }else{
                this.errorMessage = result.message;
                this.vError = true;
            } 
        })
        .catch(error => {
            console.log('Sale por error y es: '+JSON.stringify(error));
            var errors = error;
            if (errors) {
                if (errors[0] && errors[0].message) {
                    console.log("Error message: " + 
                             errors[0].message);
                    this.vError = true;
                    this.errorMessage = errors[0].message;
                }
            } else {
                console.log("Unknown error");
            }
        })
        
    }

    closeModal(event){
        this.showCMP = false;
    }

    handleChange(event){
        console.log('donde cambio: ' +event.currentTarget.name);
        switch (event.currentTarget.name) {
            case 'idioma':
                this.idiomaSelec = event.currentTarget.value;
            break;
            case 'city':
                this.city = event.currentTarget.value;
            break;        
            case 'nif':
                this.nif = event.currentTarget.value;
            break;  
        }
    }

    getLanguagesJS () {	
        console.log('Entra en getLanguagesJS');
        
        getLanguageSJON({   })
        .then(response => {
            console.log('Success getLanguageSJON');

            var result = response;
            var resultObject = JSON.parse(result);               
            this.languageList = resultObject;
            console.log('Lista de lenguajes '+JSON.stringify(this.languageList));
            
        })
        .catch(error => {
            console.log('ERROR getLanguageSJON');

            var errors = error;
            if (errors) {
                if (errors[0] && errors[0].message) {
                    console.log("Error message: " + 
                                errors[0].message);
                }
            } else {
                console.log("Unknown error");
            }
        })

	}   

}