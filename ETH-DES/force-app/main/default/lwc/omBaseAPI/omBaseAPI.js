import refreshToken from '@salesforce/apex/OM_ContentService.refreshToken'
import contentsrulr from '@salesforce/resourceUrl/contentsr'


export default class OmBaseAPI {
  credentials = {};
  appConfig = {};
  endpoints = {};
  settingName = {};
  authUrl = null;
  defaultDriveId = null;
  defaultSiteId = null;
  staticresourceUrl = contentsrulr;
  title = null;
  logoUrl = null;

  constructor(cfg) {
    this.authUrl = cfg.authUrl;
    this.endpoints = cfg.endpoints;
    this.settingName = cfg.settingName;
    this.defaultDriveId = cfg.defaultDriveId;
    this.defaultSiteId = cfg.defaultSiteId;
  }

  setCredentials(credentials) {
    this.credentials = JSON.parse(JSON.stringify(credentials));
  }

  getEndpoint(service, file, parent) {
    let endp = this.endpoints[service];
    if (parent?.includes('/')) {
      endp = endp.replace('{parentId}:','{parentId}')
      endp = endp.replace('items/', '')
    }
    if (file) {
      let driveId = file.parentReference?.driveId ?? this.defaultDriveId
      let siteId = file.parentReference?.siteId ?? this.defaultSiteId
      endp = endp.replace('{id}', file.id)
      endp = endp.replace('{filename}', file.name)
      endp = endp.replace('{siteid}', siteId)
      endp = endp.replace('{driveId}', driveId)
    }
    if (parent) {
      if (parent.includes('/') && parent.endsWith(':/')) {
        parent = parent.substring(0, parent.length - 2)
      }
      endp = endp.replace('{parentId}', parent)
    }
    endp = endp.replace('{siteid}', this.defaultSiteId)
    endp = endp.replace('{driveId}', this.defaultDriveId)


    return endp;
  }

  getAuthUrl() {
    return this.authUrl
  }

  async fetchRequest(opts, retry = true) {
    /* Options keys
        method
        url
        params
        body
        headers
    */
    let endpoint = opts.url;

    if (opts.params && typeof opts.params === 'object' ) {
        let params = Object.keys(opts.params).map((key) => encodeURIComponent(key) + '=' + encodeURIComponent(opts.params[key])).join('&');
        endpoint = params !== '' ? endpoint +'?' + params : endpoint
    }

    let conf = {
        method: opts.method,
        mode: 'cors',
        cache: 'default'
    }

    if(opts.body && (opts.method === 'POST' || opts.method === 'PATCH')) {
        conf.body = JSON.stringify(opts.body)
    }

    if(!conf.headers) {
        conf.headers = {
            'Authorization': 'Bearer ' + this.credentials.access_token,
            'Content-Type': 'application/json'
        }
    }
    
    console.log('Calling --> ' + endpoint)
    let response = await fetch(endpoint, conf)

    //Invalid credentials, les try to refresh the token and overwrite the response
    if(retry && response.status === 401) {
      let jsonValue = await refreshToken({ settingsName: this.settingName , userid: null})
      this.setCredentials(JSON.parse(jsonValue));
      //Call again the API
      if(conf.headers.Authorization){
          conf.headers.Authorization = 'Bearer ' + this.credentials.access_token
      }
      response = await fetch(endpoint, conf)
      await this.handleResponseErrors(response)
      
    }
    await this.handleResponseErrors(response)
    return response
  } 

  async handleResponseErrors(response) {
    if(response.status >= 400) {
      let errjson, msg = response.statusText;
      try {
        errjson = await response.json()
        msg = errjson.error ? errjson.error.message : 'API Error'
        console.log('Error calling API JSON', errjson)
      } catch(err) {
        console.log('Cabnot parse erro JSON')
      }
      throw new Error(msg)
    }
    return true
  }

  // To be implemented by provider
  delete() {
    throw new Error('To implement in childs')
  }

  upload() {
    throw new Error('To implement in childs')
  }

  list() {
    throw new Error('To implement in childs')
  }

  update() {
    throw new Error('To implement in childs')
  }

  create() {
    throw new Error('To implement in childs')
  }

  createFolder() {
    throw new Error('To implement in childs')
  }

  isFolder() {
    throw new Error('To implement in childs')
  }

  getLogoUrl() {
    return '/img/salesforce-noname-logo-v2.svg?width=128&height=128'
  }

  getTitle() {
    return 'Content explorer'
  }

  setLogoUrl(val) {
    this.logoUrl = val
  }

  setTitle(val) {
    this.title = val
  }
  
}