/**
 * Component to show and manage tags/labels
 * @module OmTagger
 * @version 1.0.0
 */
import { LightningElement, api, track } from 'lwc';

export default class OmTagger extends LightningElement {

  /**
   * If the button to enter in edit mode is displayed
   * @type Boolean
   * @default false
   */
  @api showButton = false

  /**
   * If component is en edit mode
   * @type Boolean
   * @default false
   */
  @api allowTagging = false

  /**
   * number of tags displayed
   * @type String
   * @default ''
   */
  @api tagsDisplayed = 5

  /**
   * Full text where inital tags will be stripped
   * @type String
   * @default ''
   */
  @api fullText = ''

  @track addingTags = false
  @track currentTag = null
  @track viewAllTags = false

  toggleViewAllTags() {
    this.viewAllTags = !this.viewAllTags;
  }
  addCurrentTag() {
    this.template.querySelector('.currentTagInput').focus();
    /**
     * Event emmited when a tag is added
     * @event addtag
     * @type string
     */
    this.dispatchEvent(new CustomEvent('addtag', { detail: this.currentTag, bubbles: true, composed: true}))
    this.currentTag = null;
  }

  removeTag(ev) {
    let tag = ev.target.dataset.tag
    /**
     * Event emmited when a tag is removed
     * @event removetag
     * @type string
     */
    this.dispatchEvent(new CustomEvent('removetag', { detail: tag, bubbles: true, composed: true}))

  }

  beginAddInlineTags() {
    this.currentTag = null;
    this.addingTags = true;
  }

  cancelAddInlineTags() {
    this.currentTag = null;
    this.addingTags = false;
  }

  /**
   * Gets the tags in the fullText
   */
  getTags() {
    var tags = [];

    if (this.fullText) {
      if (this.fullText.indexOf('#') !== -1) {
        tags = this.fullText.split('#');
      } else {
        tags = this.fullText.split(' ');
      }
      tags = tags.filter(function (n) { return n.trim().length > 0 });
    }

    return tags;
  }

  onTagChange(ev) {
    this.currentTag = ev.target.value
  }

  get tags() {
    return this.getTags(this.fullText)
  }

  get hasCurrentTag() {
    return !this.currentTag || this.currentTag === ''
  }
}