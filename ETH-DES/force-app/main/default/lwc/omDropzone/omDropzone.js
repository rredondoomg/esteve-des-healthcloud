/**
 * Creates a drop zone where user can drop a dragged file and allow developer to use de default
 * <slot> to make droppable any kind on element
 * @module OmDropzone
 * @version 1.0.0
 */
import { LightningElement, api, track } from 'lwc';

/**
 * Debounces the execution of a given function
 * @param {function} func Function to debounce
 * @param {number} wait Milliseconds to wait
 * @param {boolean} immediate Immediate execution
 */
// const debounce = (func, wait, immediate) => {
//   var timeout;
//   return function() {
//     var context = this, args = arguments;
//     var later = function() {
//       timeout = null;
//       if (!immediate) func.apply(context, args);
//     };
//     var callNow = immediate && !timeout;
//     clearTimeout(timeout);
//     timeout = setTimeout(later, wait);
//     if (callNow) func.apply(context, args);
//   };
// }

/**
 * Event containing the files dropped
 * @event files
 * @type {File[]}
 */

/**
 * @classdesc TTTCreates a drop zone where user can drop a dragged file and allow developer to use de default
 * <slot> to make droppable any kind on element
 * @class OmDropzone
 */
export default class OmDropzone extends LightningElement {

  /**
   * The message to show when dragging over the element
   * @type string
   * @default 'Drop to upload'
   */
  @api dropMessage = 'Drop to upload'


  @track isDragOver = false
  @track dragCounter = 0

  // @api db = debounce((ev) => {
  //   console.log('onDragLeave debounced ' + ev.currentTarget.contains(ev.target))
  //   // Prevent default behavior (Prevent file from being opened)
  //   ev.preventDefault();
  //   this.dragCounter--;
  //   //change class
  //   if (this.isDragOver === true && this.dragCounter === 0) {
  //     this.isDragOver = false;
  //   }
  // }, 500, true)

  /**
   * Starts the dragover
   * @param {Event} ev The event comming from the dragged over element
   * @private
   */
  onDragOver(ev) {
    console.log('onDragOver ' + ev.currentTarget.contains(ev.target))
    ev.preventDefault();
    ev.stopPropagation();
    this.dragCounter++;
    if (this.isDragOver === false) {
      this.isDragOver = true;
    }
  }
  
  /**
   * Starts the dragleave
   * @param {Event} ev The event comming from the left dragged element
   * @private
   */
  onDragLeave(ev) {
    console.log('onDragLeave ' + ev.currentTarget.contains(ev.target))
    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();
    ev.stopPropagation();
    this.dragCounter--;
    if (this.isDragOver === true && ev.currentTarget.contains(ev.target)) {
      this.isDragOver = false;
    }
  }

  /**
   * Starts the drop. Dispatch the 'files' event if any file has been dropped
   * @fires files
   * @param {Event} ev The event comming from the dropped on element
   * @private
   */
  onDrop(ev) {
    console.log('onDrop ' + ev.currentTarget.contains(ev.target))
    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();
    ev.stopPropagation();
    this.isDragOver = false;
    if (ev.dataTransfer.files) {
      /**
       * Event with the files dropped
       * @type Array
       */
      this.dispatchEvent(new CustomEvent('files', { detail: ev.dataTransfer.files, bubbles: true, composed: true}));
    }
  }

  get dragOverlayClass() {
    return this.isDragOver ? 'dropzoneBackdrop slds-show' : 'slds-hide'
  }

}