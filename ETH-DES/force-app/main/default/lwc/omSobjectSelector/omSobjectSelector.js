/**
 * Component that allows users to select a sobject and a list of fields.
 * Also we can specify a sobject to allow user to select related sobject to that object
 * @module OmSobjectSelector
 * @version 1.0.0
 */
import { LightningElement, api, wire } from 'lwc'
import getSObjects from '@salesforce/apex/OM_LWC_pack.getSObjects'
import describeToJSON from '@salesforce/apex/OM_LWC_pack.describeToJSON'

export default class OmSobjectSelector extends LightningElement {

  /**
   * If the selector should allow to select SObject fields.
   * If false, a picklist for SObject will be displayed
   * @type boolean
   * @default false
   */
  @api selectFields = false

  /**
   * Label to show in the picklist or the button
   * @type string
   */
  @api label

  _sobject

  /**
   * SObject API name to retrieve relations for
   * @example 'Account'
   * @type string
   */
  @api 
  get sobject() { return this._sobject }
  set sobject(val) {
    this._sobject = val
    let matched = this.objectList.find(o => o.value === this.sobject)
    if(matched.children) {
      this.objectList = matched.children.map((c, idx) => {
        return { label: c.relationship + ' - ' +c.object, value: idx+'@'+c.object, key: c.object, field: c.field}
      })
    }
  }

  /**
   * Original selection from picklist. Can contain '@' character
   * @example 'Contact'
   * @type string
   */
  @api originalSelectedObject

  /**
   * Full object list from server
   * @private
   */
  objectList

  /**
   * List of available fields to display in the table
   * @private
   */
  availableFields

  /**
   * Selected fields from table
   * @private
   */
  selectedFields = []


  /**
   * Parsed selection from picklist. This will be emitted
   * @private
   */
  selectedObject

  /**
   * Whether the modal for selection will
   * @private
   */
  showSelectionModal = false

  /**
   * Columns to show in the table
   * @private
   */
  fieldColumns = [
    {label : 'Label', fieldName : 'label', type:'text'},
    {label : 'API Name', fieldName : 'name', type:'text'},
    {label : 'Type', fieldName : 'type', type:'text'}
  ]

  /**
   * Label to display in the button
   * @private
   */
  get buttonLabel() {
    return this.label ? this.label : 'Select SObject'
  }

  /**
   * Whether the table form selecting fields should be
   * @private
   */
  get showFieldSelection() {
    return this.selectFields === true && this.selectedObject !== undefined
  }

  /**
   * Whether the picllsit should be displayed directly instead of a button + modal
   * @private
   */
  get showSelectorDirectly() {
    return this.selectFields === false
  }

  /**
   * Gets a list of sobjects in the org
   * @private
   */
  @wire(getSObjects)
  wiredResult({error, data}) { 
      if (data) {
          let sorted = JSON.parse(JSON.stringify(data)).sort((a, b) => a.label.localeCompare(b.label)) 
          this.objectList = sorted
      } else {
          this.apexError = error
      }
  }

  /**
   * Gets a list of fields for selected object
   * @private
   */
  @wire(describeToJSON, { objectName: '$selectedObject'})
  wiredDescribeToJSON({error, data}) {
      if (data) {
          let objectSchema = JSON.parse(data)
          this.availableFields = objectSchema.fields.filter((f) => f.createable === true)

      } else {
          this.apexError = error
      }
  }

  /**
   * Handles the selection of a sobject. If the value has a '@' character (in children relationships), get the correct value
   * @param {object} event The selection event
   * @private
   */
  handleObjectChange(event) {
    let val = event.detail.value
    this.originalSelectedObject = val
    if(val.indexOf('@') !== -1) {
      this.selectedObject = val.split('@')[1]
    } else {
      this.selectedObject = val
    }

    this.relationshipField = this.objectList.find(ob => ob.value === val).field

    if(this.showSelectorDirectly === true) {
      this.saveSelection()
    } 
  }

  /**
   * Handles the selection of fields in the datatable
   * @param {object} event The selection event in datatable
   * @private
   */
  handleSelectedFields(event){
    let selrows = event.detail.selectedRows
    this.selectedFields = event.detail.selectedRows
    for (let i = 0; i < selrows.length; i++){
        console.log("You selected: " + selrows[i].label)
    }
    console.log(this.selectedFields)
  }


  beginSelection() {
    this.showSelectionModal = true
  }

  closeModal() {
    this.showSelectionModal = false
  }

  /**
   * Emits the selected object and fields (if any)
   * @fires selection
   * @private
   */
  saveSelection() {
    /**
     * @event selection
     * @type object
     * @property {string} sobject The selected object
     * @property {array} fields The selected fields
     */
    this.dispatchEvent(new CustomEvent('selection', { 
      detail: { sobject: this.selectedObject, fields: this.selectedFields, relationshipField: this.relationshipField },
      bubbles: false,
      composed: true
    }))

    this.showSelectionModal = false
  }

}