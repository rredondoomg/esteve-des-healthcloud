/**
 * Component to display a related list of records using OmListView
 * @module OmRelatedList
 * @version 1.0.0
 * @requires OmCard
 * @requires OmListView
 * @requires OmDatatable
 */
import { LightningElement, api, track, wire } from 'lwc';
import { CurrentPageReference} from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';
import { NavigationMixin } from 'lightning/navigation';
import newLabel from '@salesforce/label/c.omNew';

export default class OmRelatedList extends NavigationMixin(LightningElement) {
    
    /**
     * Salesforce object API name
     * @type String
     * @required
     */
    @api sobject

    /**
     * Salesforce field API name for selected sObject type
     * @type String
     * @required
     */
    @api fields

    /**
     * List of fields that must be shown as HTML, separated by commas
     * @type string
     */
    @api htmlFields = '';

    /**
     * Salesforce SOQL filters to apply to WHERE clause when querying records
     * @type String
     * @default ''
     */
    @api filters = ''

    /**
     * Column keys to be clickable
     * @type String
     */
    @api clickableColumns

    /**
     * deprecated - cant be deleted 
     */
    @api rowsPerPage

    /**
     * Record Id from Lightning Experience record page
     * @type String 
     */
    @api recordId
    
    /**
     * When using omListView as relatedList, the field used to retrived the relationship
     * @type String
     */
    @api relationField

    /**
     * Indicates if record are going to be opened on clicked cell
     * @type Boolean
     * @default false
     */
    @api openRecord

    /**
     * Action on clicked cell (view, edit, list)
     * @type String
     * @default 'view'
     */
    @api actionName = 'view'
    
    /**
     * Header icon
     * @type String
     * @default 'custom:custom19'
     */
    @api icon = 'custom:custom19'

    /**
     * Header label
     * @type String
     * @default 'Label'
     */
    @api cardLabel = 'Label'

    /**
     * New label button
     * @type String
     * @default Label.omNew
     */
    @api newLabel = newLabel

    /**
     * If new button should be displayed
     * @type Boolean
     * @default false
     */
    @api showNewButton

    /**
     * If search input should be displayed
     * @type Boolean
     * @default false
     */
    @api showSearchInput

    /**
     * If the search should only apply to data appearing in columns instead of all fields
     * @type boolean
     * @default false
     */
    @api searchOnlyInColumns = false

    /**
     * If the search should ignore diacritics when searching (high performance impact)
     * @type boolean
     * @default false
     */
    @api fuzzySearch = false

    /**
     * If paginator should be hidden
     * @type Boolean
     * @default false
     */
    @api hidePaginator

    /**
     * If number buttons should be hidden
     * @type Boolean
     * @default false
     */
    @api hideNumberButtons

    /**
     * If control buttons should be hidden
     * @type Boolean
     * @default false
     */
    @api hideControlButtons

    /**
     * Row class to special styles
     * @type String
     */
    @api rowClass

    /**
     * Row style to special styles
     * @type String
     */
    @api rowStyle

    /**
     * TODO
     */
    @api rowValue

    /**
     * Indicates if list view has to subscribe to CDC
     * @type Boolean
     * @default false
     */
    @api listenChanges

    /**
     * Datetime format to show (in moment.js notation). Example: 'DD MM YYYY hh:mm:ss'
     * @type string
     * @default ''
     */
    @api datetimeFormat = '';

    /**
     * List of labels to show, separated by commas. Wildcard '#' to set the field label
     * @type string
     */
    @api labelList = '';

    /**
     * Search term to apply.
     * @type string
     * @default ''
     */
    @api searchTerm;

    /**
     * Multi-line text truncation in the table cell. Max 7 lines of text
     * @type boolean
     * @default false
     */
    @api truncate = false;

    /**
     * Name of Platform Evento to subscribe
     * @type string
     * @default null
     */
    @api platformEvent;

    /**
     * Platform Event recordId API Name of field to filter events fired
     * @type string
     * @default null
     */
    @api idFieldName;

    /**
     * If the component should start polling to display changes in records
     * @type boolean
     * @default false
     */
    @api polling = false

    /**
     * Interval in seconds of polling
     * @type string/integer
     * @default '30'
     */
    @api pollingInterval = '30'

    @track rowsCount = 0
    /*************************************************/

    constructor() {
        super();
        this.template.addEventListener('rowslength', this.handleRowsLength.bind(this));
    }

    @wire(CurrentPageReference) pageRef;

    get isListView(){
        return this.sobject!=='';
    }

    newButtonClick(){
        this[NavigationMixin.Navigate]({
            type: 'standard__objectPage',
            attributes: {
                objectApiName: this.sobject,
                actionName: 'new'
            }
        });
        // todo David
        fireEvent(this.pageRef, 'clickedNewButton', {
            params: { detail: this.sobject}
        });
    }


    handleRowsLength(evt) {
        this.rowsCount = evt.detail;
    }

    navigateToRecordViewPage(evt) {
        if(this.openRecord && evt && evt.detail && evt.detail.item){
            this[NavigationMixin.Navigate]({
                type: 'standard__recordPage',
                attributes: {
                    recordId: evt.detail.item.Id,
                    actionName: this.actionName
                }
            });
        }
    }

}