import { LightningElement, api } from 'lwc';

export default class OmRenderIf extends LightningElement {

    @api val;
    @api ev;

    get renderClass(){

        let res = ( this.val instanceof Array && this.val.length>0 ) || this.val;

        if(this.ev === 'false'){ 
            res = !res 
        }

        if(res){
            return 'slds-show';
        } 
            return 'slds-hide';
        

    }

}