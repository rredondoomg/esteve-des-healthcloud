/**
 * Base component for custom implementation of Omega Content. Retrieves infom form Custom Metadata records and 
 * @module OmContentBase
 */
import upsertContents from '@salesforce/apex/OM_ContentService.upsertContents';
import getSettingsForObject from '@salesforce/apex/OM_ContentService.getSettingsForObject';
import removeCurrentUserAccess from '@salesforce/apex/OM_ContentService.removeCurrentUserAccess';
import getAuthUrl from '@salesforce/apex/OM_ContentService.getAuthorizationEndpoint';
import getUserAccess from '@salesforce/apex/OM_ContentService.getCurrentUserAccess';
import getAllEndpoints from '@salesforce/apex/OM_ContentService.getAllEndpoints';
import OmGoogleAPI from 'c/omGoogleAPI';
import OmMicrosoftSharepointAPI from 'c/omMicrosoftSharepointAPI';
import OmOneDriveAPI from 'c/omOneDriveAPI';

import omExplorerError from '@salesforce/label/c.omExplorerError'



function capitalize(s) {
  if (typeof s !== 'string') return ''
  return s.charAt(0).toUpperCase() + s.slice(1)
}

async function processContentSettings (value, recordid, getProps = true) {
  let customMetadataName = value
  let authUrl, endpoints, userAccess

  let result = await getSettingsForObject({ settingsName: value, recordId: recordid })

  // let result = await getSettings({ settingsName: value })
  let settings = result.settings
  let customAppProperties;

  if(result.customProperties && getProps) {
      customAppProperties = result.customProperties.map(cp => {
        return { key : cp.OM_Property_name__c, default : '',
          jsonType : 'String' ,
          label : capitalize(cp.OM_Property_name__c),
          mappedTo: cp.OM_Salesforce_field_name__c,
          asColumn: cp.OM_As_column__c,
          filterable: cp.OM_Filterable__c,
          editable:  cp.OM_Editable__c }
      })
  }


  // Call Apex to get all configs
  authUrl = await getAuthUrl( { settingsName: value } )
  endpoints = await getAllEndpoints( { settingsName: customMetadataName})
  userAccess = await getUserAccess( { settingsName: customMetadataName})

  let apiConfig = 
  {
    authUrl,
    endpoints,
    settingName: customMetadataName,
    defaultDriveId: settings.OM_Drive_id__c,
    defaultSiteId: settings.OM_Site_id__c
  }

  let provider = settings.OM_Provider__c ? settings.OM_Provider__c : settings.OM_Parent_setting__r.OM_Provider__c;
  let baseColumnNames = settings.OM_Columns__c ? settings.OM_Columns__c.split(';') : []

  return { 
    userAccess,
    customAppProperties,
    settings,
    baseColumnNames,
    settingName: customMetadataName,
    providerImplementation: getApiFromProvider(provider, apiConfig)
  }
}


function getApiFromProvider(provider, apiConfig) {
  if(provider === 'MicrosoftOneDrive') {
    return new OmOneDriveAPI(apiConfig);
  } else if(provider === 'MicrosoftSharePoint') {
    return new OmMicrosoftSharepointAPI(apiConfig);
  }
  return new OmGoogleAPI(apiConfig);
}

async function onProviderCredentials (customMetadataName) {
  let userAccess = await getUserAccess( { settingsName: customMetadataName })
  console.log('@@@ OmContentBase -> user acces obtained ' + userAccess)
  return userAccess;
}

async function onSignOut (settingsName) {
  await removeCurrentUserAccess({ settingsName: settingsName });
  this.userAccess = null
  console.log('@@@ OmContentBase -> onSignOut OK ')
  return true;
}

function saveContents (files) {
  console.log('@@@ OmContentBase -> Sending contents to save ' + JSON.stringify(files))

  upsertContents({
    settingsName: this.settingToUse,
    contents: files
  })
  .then((result) => {
      console.log('@@@ OmContentBase -> Contents saved ' + JSON.stringify(result))
  })
  .catch((error) => {
    console.log('@@@ OmContentBase -> Contents saved error' + JSON.stringify(error))

      this.errors = 'Error received: code' + error.errorCode + ', ' +
          'message ' + error.body.message;
  });
}

function getSLDSIconFromMimeType (file) {
  let mime = file.file ? file.file.mimeType : file.mimeType
  if ( file.folder) return 'folder'
  if (mime) {
    if ( mime.indexOf('text/plain') !== -1) return 'txt'
    if ( mime.indexOf('/csv') !== -1) return 'csv'
    if ( mime.indexOf('audio/') !== -1) return 'audio'
    if ( mime.indexOf('video/') !== -1) return 'video'
    if ( mime.indexOf('image/') !== -1) return 'image'
    if ( mime.indexOf('msword') !== -1 || mime.indexOf('ms-word') !== -1 || mime.indexOf('wordprocessingml') !== -1) return 'word'
    if ( mime.indexOf('msexcel') !== -1 || mime.indexOf('ms-excel') !== -1 || mime.indexOf('officedocument.spreadsheetml') !== -1) return 'excel'
    if ( mime.indexOf('powerpoint') !== -1 || mime.indexOf('ms-powerpoint') !== -1 || mime.indexOf('officedocument.presentationml') !== -1) return 'ppt'

    // Swith of other cases
    switch (mime) {
      case 'application/zip': return 'zip'
      case 'application/pdf': return 'pdf'
      case 'application/vnd.google-apps.document': return 'gdoc'
      case 'application/vnd.google-apps.spreadsheet': return 'gsheet'
      case 'application/vnd.google-apps.drawing': return 'gdocs'
      case 'application/vnd.google-apps.file': return 'gdocs'
      case 'application/vnd.google-apps.audio': return 'gdocs'
      case 'application/vnd.google-apps.form': return 'gform'
      case 'application/vnd.google-apps.fusiontable': return 'gdocs'
      case 'application/vnd.google-apps.map': return 'gdocs'
      case 'application/vnd.google-apps.photo': return 'gdocs'
      case 'application/vnd.google-apps.presentation': return 'gpres'
      case 'application/vnd.google-apps.script': return 'gdocs'
      case 'application/vnd.google-apps.site': return 'gdocs'
      case 'application/vnd.google-apps.unknown': return 'gdocs'
      case 'application/vnd.google-apps.video': return 'gdocs'
      default:
        return 'unknown'
    }
  }

  return 'unknown'
}

function handleError(err, msg) {
  console.log('base error handling')
  let error = {
    title: omExplorerError,
    message: 'An unknown error happended **'
  }
  if (msg) {
    error.message = msg
  } else {
    if(typeof err === 'string') {
      try {
        let jsonError = JSON.parse(err);
        if(jsonError.error && jsonError.error.message) {
          error.message = jsonError.error.message
        }
      } catch(e) {
        error.message = err
      } 
    } else if(Array.isArray(err)) {
      // TODO 
    } else {
      // Apex error
      if (err.body) {
        error.title = err.body.exceptionType;
        error.message = err.body.message;
      }
      if (err.message) {
        error.message = err.message;
      }
    }
  }

  return error;
}

export default  {
  capitalize,
  processContentSettings,
  onSignOut,
  getApiFromProvider,
  onProviderCredentials,
  saveContents,
  getSLDSIconFromMimeType,
  handleError
}