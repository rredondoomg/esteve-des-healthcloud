/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 09-27-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
trigger ETH_ServiceAppointmentTrigger on ServiceAppointment (after update, after insert, before delete) {
    Trigger_Controller__mdt triggerController = ETH_Utils.getTriggerController();
    if(triggerController.Service_Appointment_Trigger__c){
        Id recordTypeTRD = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('Visita_TRD').getRecordTypeId();
        Id recordTypePharmate = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('Visita_Pharmate').getRecordTypeId();
        List<ServiceAppointment> visitasToInsert = new List<ServiceAppointment>();
        List<AssignedResource> assignedResourcesToInsert = new List<AssignedResource>();
        List<ServiceAppointment> visitasWithoutResourceToInsert = new List<ServiceAppointment>();
        List<Task> tasksWithoutResourceToInsert = new List<Task>();
        List<Case> casesToUpdate = new List<Case>();
        List<Id> accountIds = new List<Id>();
        List<Id> protocolStepsId = new List<Id>();
        List<Id> sAppIds = new List<Id>();
         
        if(!Trigger.isDelete){
            for(ServiceAppointment app: Trigger.new){
                accountIds.add(app.AccountId);
                sAppIds.add(app.Id);
                if(app.Paso_Protocolo__c != null){
                    protocolStepsId.add(app.Paso_Protocolo__c);
                }
            }
        }

        // nombre worktype-worktype
        Map<String, Worktype> worktypes;
        // id worktype - id worktypegroup
        Map<Id, Id> worktypeGroupMap;
        // AccountId- Case
        Map<Id, List<Case>> casesByAccountMap;
        // Id Protocolo - Pasos
        Map<Id, List<Paso_Protocolo__c>> protocolListPasosMap;
        // Id paso - paso (con info del protocolo padre)
        Map<Id, Paso_Protocolo__c> protocolStepData;
        //id caso, nombre del tratamiento
        Map<Id, String> tratamientoName = new Map<Id, String>();
        //Algunos mapas de datos generales para la descripción, asunto... Las claves son el AccountId
        Map<Id, String> accountPostalCode = new Map<Id, String>();
        Map<Id, String> accountName = new Map<Id, String>();
        Map<Id, Address> accountAddress = new Map<Id, Address>();
        Map<Id, String> accountMovilidad = new Map<Id, String>();
        Map<Id, Double> citasSinAcudirByAccount = new Map<Id, Double>();
        //Más mapas de datos, las claves son el tratamiento
        Map<Id, List<Case>> tratamientosByAccount = new Map<Id, List<Case>>();
        Map<Id, Double> tratamientoFrecuencia = new Map<Id, Double>();
        //El segundo mapa tiene dos claves: 'Virtual' o 'Presencial'
        Map<Id, Map<String, String>> tratamientoLugaresDisponibles = new Map<Id, Map<String, String>>();
        // postalCode- service territory (pharmate)
        Map<String, List<ServiceTerritory>> serviceTerritoryByPostalCode;
        // id service territory - service territory member (para consultar el Service Resource)
        Map<Id, List<ServiceTerritoryMember>> serviceTerritoryMembersByParentTerritory;
        List<Id> parentServiceTerritoriesId = new List<Id>();
        List<Id> worktypesAutoagendamiento;
        Group grupoPharmateQueue;
        Group contactCenter;
        Map<Id, Map<String, List<TimeSlot>>> operatingHoursTimeSlotMap;
        Map<Id, AppointmentTopicTimeSlot> topicTimeSlotMap;
        List<Id> operatingHoursIds = new List<Id>();
        List<Id> timeSlotIds = new List<Id>();
        Autoagendamiento__mdt parametrosAutoagendamiento;
        Map<String, CaseTeamRole> caseTeamRolesByName;
        List<CaseTeamMember> caseMembersToInsert = new List<CaseTeamMember>();
        Map<Id, List<CaseTeamMember>> caseTeamMembersByCase;
        List<ServiceTerritory> cretas;
        Map<Id, List<Visita_tratamiento__c>> visitaTratamientoBySApp;
        
        Id profileId;
        Profile contactProfesionalProfile;

        List<ServiceAppointment> visitasToCallEquiposYFungiblesAtlas = new List<ServiceAppointment>();

        /*
            _         _ _     _          __            
        | |__ _  _| | |__ | |__  ___ / _|___ _ _ ___
        | '_ \ || | | / / | '_ \/ -_)  _/ _ \ '_/ -_)
        |_.__/\_,_|_|_\_\ |_.__/\___|_| \___/_| \___| 
        */       
        

        if(Trigger.isBefore) {
            if(Trigger.isUpdate) {
                //para validaciones
                protocolStepData = ETH_ServiceAppointmentTriggerHandler.getStepsProtocolData(protocolStepsId);
                protocolListPasosMap = ETH_ServiceAppointmentTriggerHandler.getStepsByProtocol(protocolStepData.keySet());
                casesByAccountMap = ETH_ServiceAppointmentTriggerHandler.getCasesByAccountAndAccountInfo(accountIds, accountPostalCode, accountName, tratamientoName, accountAddress, accountMovilidad, tratamientoFrecuencia, tratamientosByAccount, tratamientoLugaresDisponibles, citasSinAcudirByAccount);
            }
            if(Trigger.isDelete){
                profileId = UserInfo.getProfileId();
                contactProfesionalProfile = [SELECT Id FROM Profile WHERE Name = 'Contact Profesional'];
            }
            // if(Trigger.isUpdate) {}
        }
    
        if(Trigger.isAfter) {
            if(Trigger.isInsert) {
                ETH_ServiceAppointmentTriggerHandler.smsValidation(Trigger.new);
            }
            if(Trigger.isUpdate) {
                ETH_ServiceAppointmentTriggerHandler.smsValidation(Trigger.new, Trigger.oldMap);
                if( ((Trigger.new.size() == 1) && (((Trigger.new[0].Status == 'Completada') && (Trigger.oldMap.get(Trigger.new[0].Id).Status != Trigger.new[0].Status)) || ((Trigger.new[0].Status == 'Cancelada') && (Trigger.new[0].Motivo_cancel__c == 'Paciente no acude' || Trigger.new[0].Motivo_cancel__c == 'Fin de tratamiento') && (Trigger.oldMap.get(Trigger.new[0].Id).Status != Trigger.new[0].Status)))) || (Trigger.new.size() > 1) ){
                    worktypes = ETH_Utils.getWorktypes();
                    worktypeGroupMap = ETH_Utils.getWorktypeGroupEquivalence();
                    worktypesAutoagendamiento = ETH_Utils.getWorktypeByTipo(worktypes, 'Alta');
                    worktypesAutoagendamiento.addAll(ETH_Utils.getWorktypeByTipo(worktypes, 'Seguimiento'));
                    casesByAccountMap = ETH_ServiceAppointmentTriggerHandler.getCasesByAccountAndAccountInfo(accountIds, accountPostalCode, accountName, tratamientoName, accountAddress, accountMovilidad, tratamientoFrecuencia, tratamientosByAccount, tratamientoLugaresDisponibles, citasSinAcudirByAccount);
                    List<Id> caseIds = new List<Id>();
                    for(Id id: casesByAccountMap.keySet()){
                        for(Case ca: casesByAccountMap.get(id)){
                            caseIds.add(ca.Id);
                        }
                    }
                    protocolStepData = ETH_ServiceAppointmentTriggerHandler.getStepsProtocolData(protocolStepsId);
                    protocolListPasosMap = ETH_ServiceAppointmentTriggerHandler.getStepsByProtocol(protocolStepData.keySet());
                    serviceTerritoryByPostalCode = ETH_ServiceAppointmentTriggerHandler.getServiceTerritoriesByPostalCode(accountPostalCode.values(), parentServiceTerritoriesId);
                    cretas = ETH_ServiceAppointmentTriggerHandler.getCRETAS();
                    //Añadir los CRETA como padres para que nos traiga los boxes también
                    for(ServiceTerritory creta: cretas){
                        parentServiceTerritoriesId.add(creta.Id);
                    }
                    serviceTerritoryMembersByParentTerritory = ETH_ServiceAppointmentTriggerHandler.getServiceResourcesByTerritory(parentServiceTerritoriesId);
                    for(List<ServiceTerritoryMember> members: serviceTerritoryMembersByParentTerritory.values()){
                        for(ServiceTerritoryMember member: members){
                            operatingHoursIds.add(member.OperatingHoursId);
                        }
                    }
                    operatingHoursTimeSlotMap = ETH_ServiceAppointmentTriggerHandler.getFranjasHorarias(operatingHoursIds, timeSlotIds);
                    topicTimeSlotMap = ETH_ServiceAppointmentTriggerHandler.getTopicTimeSlot(timeSlotIds);
                    contactCenter = ETH_ServiceAppointmentTriggerHandler.getQueue('Contact Center');
                    grupoPharmateQueue = ETH_ServiceAppointmentTriggerHandler.getQueue('Grupo Pharmate');
                    parametrosAutoagendamiento = ETH_ServiceAppointmentTriggerHandler.getRangoDias();
                    caseTeamRolesByName = ETH_Utils.getCaseTeamRolesByName();
                    caseTeamMembersByCase = ETH_ServiceAppointmentTriggerHandler.getCaseTeamMemberByCase(caseIds);
                    visitaTratamientoBySApp = ETH_ServiceAppointmentTriggerHandler.getVisitaTratamientoBySApp(sAppIds);
                }
            }
        }
                
    
        /*                         _           _   _            
                _ _ ___ __ ___ _ _ __| |  __ _ __| |_(_)___ _ _  ___
            | '_/ -_) _/ _ \ '_/ _` | / _` / _|  _| / _ \ ' \(_-<
            |_| \___\__\___/_| \__,_| \__,_\__|\__|_\___/_||_/__/
        */
        if(Trigger.isDelete && Trigger.isBefore){
            for(ServiceAppointment app : (List<ServiceAppointment>) Trigger.old) {
                if(profileId == contactProfesionalProfile.Id){
                    app.addError('No dispone de los suficientes permisos para borrar una visita');
                }
            }
        }
        
        if(!Trigger.isDelete){
            for(ServiceAppointment app : (List<ServiceAppointment>) Trigger.new) {
                // if(Trigger.isBefore) {
                //     if(Trigger.isUpdate) {}
                //     if(Trigger.isUpdate) {}
                // }
    
                if(Trigger.isAfter) {
                    if(Trigger.isInsert) {}
                    if(Trigger.isUpdate) {
                        if((app.Status == 'Completada') && (Trigger.oldMap.get(app.Id).Status != app.Status) && Trigger.new.size() == 1){
                            if(app.RecordTypeId == recordTypeTRD){
                                visitasToCallEquiposYFungiblesAtlas.add(app);
                            }
                        }
    
                        //Control de autoagendamiento
                        if(((app.Status == 'Completada') && (Trigger.oldMap.get(app.Id).Status != app.Status)) || 
                        ((app.Status == 'Cancelada') && (app.Motivo_cancel__c == 'Fin de tratamiento') && (app.Paso_Protocolo__c == null)) || 
                        ((app.Status == 'Cancelada') && (app.Motivo_cancel__c == 'Paciente no acude') && ((citasSinAcudirByAccount.get(app.AccountId) < 2) || (app.RecordTypeId == recordTypePharmate)) && !(ETH_Utils.getWorktypeByTipo(worktypes, 'Alta').contains(app.worktypeId)) && (Trigger.oldMap.get(app.Id).Status != app.Status))){
                            //Si es de alta/seguimiento
                            if(worktypesAutoagendamiento.contains(app.WorktypeId)){
                                //------------------------------Flujo TRD/Oxigenoterapia------------------------------
                                if(app.Paso_Protocolo__c == null){
                                    // Comprobar si existe otra visita de alta de la misma línea de negocio pues en tal caso, no genera seguimiento
                                    if(!ETH_ServiceAppointmentTriggerHandler.tieneAltasSimultaneas(app.accountId, app.Linea_negocio__c) && ETH_ServiceAppointmentTriggerHandler.tieneTratamientosActivos(app.accountId, app.Linea_negocio__c)){
                                        String lineaNegocio = app.Linea_negocio__c;
                                        //Ver los tratamientos del paciente y quedarnos con la menor frecuencia
                                        List<Case> tratamientos = tratamientosByAccount.get(app.AccountId);
                                        //Filtrar los tratamientos para quedarnos solo con los de la línea de negocio correspondiente
                                        ETH_AgendamientoWrapper wrapper = ETH_ServiceAppointmentTriggerHandler.getFrecuenciaAndModalidadTRD(tratamientos, tratamientoLugaresDisponibles, accountMovilidad.get(app.AccountId), app.Tipo_lugar__c, lineaNegocio);
                                        
                                        Worktype worktype = ETH_Utils.getWorktypeByName(worktypes, 'Seguimiento', lineaNegocio, wrapper.lugarVisita, wrapper.tipoLugar);
                                        String appointmentType = ETH_Utils.getAppointmentType(wrapper.lugarVisita, wrapper.tipoLugar);
                                        Boolean asignacionContact = false;
        
                                        List<ServiceTerritory> selectedServiceTerritories = serviceTerritoryByPostalCode.get(accountPostalCode.get(app.AccountId));
                                        ServiceTerritory selectedServiceTerritory = ETH_ServiceAppointmentTriggerHandler.selectServiceTerritory(selectedServiceTerritories, cretas, worktype, accountAddress.get(app.AccountId).getLatitude(), accountAddress.get(app.AccountId).getLongitude());
                                        if(selectedServiceTerritory == null) {
                                            System.debug('De los Service Territories para este código postal, no se encontró ninguno acorde.');
                                            asignacionContact = true;
                                        }
        
                                        //Obtener los ServiceTerritoryMembers
                                        //En caso de CRETA serán Boxes y en caso de Domicilio serán profesionales
                                        List<ServiceTerritoryMember> serviceTerritoryMemberCandidates;
                                        if(!asignacionContact){
                                            //En caso de que fuera a domicilio, reducimos la lista al profesional/es que corresponda
                                            if(!worktype.Name.contains('CRETA')){
                                                try{
                                                    //En TRD no tenemos que filtrar por roles
                                                    serviceTerritoryMemberCandidates = serviceTerritoryMembersByParentTerritory.get(selectedServiceTerritory.ParentTerritoryId);
                                                    if(serviceTerritoryMemberCandidates == null){
                                                        System.debug('No se encontraron Service Territory Members en el Service Territory');
                                                        asignacionContact = true;
                                                    }
                                                    else{
                                                        String rolProfesional = lineaNegocio == 'TRD' ? 'Asistencial' : 'Técnico';
                                                        List<ServiceTerritoryMember> profesionalSelected = new List<ServiceTerritoryMember>();
                                                        for(ServiceTerritoryMember member : serviceTerritoryMemberCandidates){
                                                            if(member.Roles__c != null && member.Roles__c.contains(rolProfesional)){
                                                                    profesionalSelected.add(member);
                                                            }
                                                        }
                                                        serviceTerritoryMemberCandidates = profesionalSelected;
                                                    }
                                                }catch(Exception e){
                                                    System.debug('No se encontraron Service Territory Members en el Service Territory');
                                                    asignacionContact = true;
                                                }
                                            }
                                            else{
                                                //Consultar los boxes del CRETA (no cogemos el padre)
                                                serviceTerritoryMemberCandidates = serviceTerritoryMembersByParentTerritory.get(selectedServiceTerritory.Id);
                                            }
                                        }
        
                                        // Coger los tratamientos de TRD menos los excepcionales
                                        List<Case> cases = casesByAccountMap.get(app.AccountId);
                                        List<Case> casesNoExcepcionales = new List<Case>();
                                        for(Case ca: cases){
                                            if(ca.Tratamiento__r.Protocolo__c == null && ca.Tratamiento__r.Linea_de_negocio__c == lineaNegocio){
                                                casesNoExcepcionales.add(ca);
                                            }
                                        }
        
                                        // Date siguienteFechaPrevista = DateTime.now().dateGMT().addDays(Integer.valueOf(wrapper.frecuencia));
                                        Date siguienteFechaPrevista = app.Fecha_Realizada__c.dateGMT().addDays(Integer.valueOf(wrapper.frecuencia));
                                        String subject = lineaNegocio == 'TRD' ? 'Seguimiento - TRD' : 'Seguimiento - Oxigenoterapia'; 
        
                                        if(asignacionContact || !(serviceTerritoryMemberCandidates.size() >= 1)){
                                            // No ha encontrado profesional o boxes
                                            // Asignamos a la cola de CONTACT con tarea
                                            Id serviceTerritoryId = selectedServiceTerritory == null ? null : ( worktype.Name.contains('CRETA') ? selectedServiceTerritory.Id : selectedServiceTerritory.ParentTerritoryId);
                                            String msg = serviceTerritoryId == null ? 'Motivo: no se encontró un Service Territory con el código postal adecuado.' : 'Motivo: no se encontraron profesionales acordes en el territorio correspondiente.';
        
                                            // No permitimos visitas sin ServiceTerritory y por lo tanto lanzamos error
                                            if(selectedServiceTerritory == null){
                                                app.addError('No se encontró ningún territorio de servicio para el código postal del paciente.');
                                            }
    
                                            ServiceAppointment visitaSeguimiento = new ServiceAppointment(
                                                RecordTypeId = recordTypeTRD,
                                                Linea_negocio__c = lineaNegocio,
                                                AppointmentType = appointmentType,
                                                OwnerId = contactCenter.Id,
                                                ParentRecordId = app.AccountId,
                                                Status = 'Prevista',
                                                Subject = subject,
                                                Fec_prev__c = ETH_ServiceAppointmentTriggerHandler.adaptarFechaMismoDiaSemana(Date.newInstance(app.Fecha_Realizada__c.year(), app.Fecha_Realizada__c.month(), app.Fecha_Realizada__c.day()), siguienteFechaPrevista),
                                                City = accountAddress.get(app.AccountId).City,
                                                Country = accountAddress.get(app.AccountId).Country,
                                                State = accountAddress.get(app.AccountId).State,
                                                Street = accountAddress.get(app.AccountId).Street,
                                                PostalCode = accountAddress.get(app.AccountId).PostalCode,
                                                ServiceTerritoryId = serviceTerritoryId,
                                                WorkTypeId = worktype.Id,
                                                DurationType = 'Minutes',
                                                Duration = worktypes.get(worktype.Name).EstimatedDuration
                                            );
                                            visitasWithoutResourceToInsert.add(visitaSeguimiento);
            
                                            Task tareaSeguimiento = new Task(
                                                OwnerId = contactCenter.Id,
                                                Description = 'Nueva visita de seguimiento por agendar para el paciente ' + accountName.get(app.AccountId) + '. ' + msg,
                                                Subject = 'Nueva visita de seguimiento por agendar'
                                                //WhatId = caseToUpdate.Id
                                            );
                                            tasksWithoutResourceToInsert.add(tareaSeguimiento); 
                                        }
                                        else{
                                            // Agendamos
                                            ServiceTerritoryMember selectedMember = null;
                                            Datetime fechaAgendada = null;
        
                                            ETH_AgendamientoWrapper wrapperResult = ETH_ServiceAppointmentTriggerHandler.agendamientoCaller(app.Fecha_Realizada__c, siguienteFechaPrevista, wrapper.frecuencia, parametrosAutoagendamiento, serviceTerritoryMemberCandidates, worktype, operatingHoursTimeSlotMap, topicTimeSlotMap);
                                            selectedMember = wrapperResult.member;
                                            fechaAgendada = wrapperResult.fecha;
        
                                            //Distinguimos por si ha encontrado fecha o no
                                            if(fechaAgendada != null){
                                                Id ownerId;
                                                String status = 'Prevista';
                                                String rol;
                                                Boolean isRequiredResource = false;
                                                Boolean isPrimaryResource = false;
                                                if(worktype.Name.contains('CRETA')){
                                                    ownerId = contactCenter.Id;
                                                    rol = 'Box CRETA';
                                                }
                                                else{
                                                    ownerId = selectedMember.ServiceResource.RelatedRecordId;
                                                    rol = lineaNegocio == 'TRD' ? 'Asistencial' : 'Técnico';
                                                }
        
                                                ServiceAppointment visitaSeguimiento = new ServiceAppointment(
                                                    RecordTypeId = recordTypeTRD,
                                                    AppointmentType = appointmentType,
                                                    Linea_negocio__c = lineaNegocio,
                                                    OwnerId = ownerId,
                                                    ParentRecordId = app.AccountId,
                                                    Status = status,
                                                    Subject = subject,
                                                    City = accountAddress.get(app.AccountId).City,
                                                    Country = accountAddress.get(app.AccountId).Country,
                                                    State = accountAddress.get(app.AccountId).State,
                                                    Street = accountAddress.get(app.AccountId).Street,
                                                    PostalCode = accountAddress.get(app.AccountId).PostalCode,
                                                    ServiceTerritoryId = selectedMember.ServiceTerritoryId,
                                                    WorkTypeId = worktype.Id,
                                                    DurationType = 'Minutes',
                                                    Duration = worktypes.get(worktype.Name).EstimatedDuration,
                                                    SchedStartTime = fechaAgendada,
                                                    SchedEndTime = fechaAgendada.addMinutes(worktype.EstimatedDuration.intValue())
                                                );
                                                visitasToInsert.add(visitaSeguimiento);
                
                                                AssignedResource assignedResource = new AssignedResource(
                                                    ServiceResourceId = selectedMember.ServiceResourceId,
                                                    IsRequiredResource = isRequiredResource,
                                                    IsPrimaryResource = isPrimaryResource,
                                                    Role = rol
                                                );
                                                assignedResourcesToInsert.add(assignedResource);
        
                                            }
                                            else{
                                                //distinguimos CRETA de Domicilio.
                                                Id ownerId;
                                                if(worktype.Name.contains('CRETA')){
                                                    //CRETA sin AssignedResource, solo con el Service Territory y a CONTACT
                                                    ownerId = contactCenter.Id;
                                                    selectedMember = serviceTerritoryMemberCandidates.get(0);
                                                }
                                                else{
                                                    //Domicilio con Assigned Resource y owner él mismo
                                                    ownerId = selectedMember.ServiceResource.RelatedRecordId;
                                                    AssignedResource assignedResource = new AssignedResource(
                                                        ServiceResourceId = selectedMember.ServiceResourceId,
                                                        IsRequiredResource = false,
                                                        IsPrimaryResource = false,
                                                        Role = lineaNegocio == 'TRD' ? 'Asistencial' : 'Técnico'
                                                    );
                                                    assignedResourcesToInsert.add(assignedResource);
                                                }
        
                                                ServiceAppointment visitaSeguimiento = new ServiceAppointment(
                                                    RecordTypeId = recordTypeTRD,
                                                    AppointmentType = appointmentType,
                                                    Linea_negocio__c = lineaNegocio,
                                                    OwnerId = ownerId,
                                                    ParentRecordId = app.AccountId,
                                                    Status = 'Prevista',
                                                    Subject = subject,
                                                    Fec_prev__c = ETH_ServiceAppointmentTriggerHandler.adaptarFechaMismoDiaSemana(Date.newInstance(app.Fecha_Realizada__c.year(), app.Fecha_Realizada__c.month(), app.Fecha_Realizada__c.day()), siguienteFechaPrevista),
                                                    City = accountAddress.get(app.AccountId).City,
                                                    Country = accountAddress.get(app.AccountId).Country,
                                                    State = accountAddress.get(app.AccountId).State,
                                                    Street = accountAddress.get(app.AccountId).Street,
                                                    PostalCode = accountAddress.get(app.AccountId).PostalCode,
                                                    ServiceTerritoryId = selectedMember.ServiceTerritoryId,
                                                    WorkTypeId = worktype.Id,
                                                    DurationType = 'Minutes',
                                                    Duration = worktypes.get(worktype.Name).EstimatedDuration
                                                );
                                                visitasToInsert.add(visitaSeguimiento);
                                            }
                                            
                                            // Controlar el Case Team
                                            // if(!worktype.Name.contains('CRETA')){
                                            //     if(casesNoExcepcionales != null){
                                            //         for(Case ca: casesNoExcepcionales){
                                            //             List<CaseTeamMember> teamMembers = caseTeamMembersByCase.get(ca.Id);
                                            //             Boolean memberDentro = false;
                                            //             if(teamMembers != null){
                                            //                 for(CaseTeamMember member: teamMembers){
                                            //                     if(member.MemberId == selectedMember.ServiceResource.RelatedRecordId){
                                            //                         memberDentro = true;
                                            //                         break;
                                            //                     }
                                            //                 }
                                            //             }
                                                        
                                            //             String caseRole = lineaNegocio == 'TRD' ? 'Asistencial' : 'Técnico';
                                            //             if(!memberDentro){
                                            //                 CaseTeamMember caseTeamMember = new CaseTeamMember(
                                            //                     ParentId = ca.Id,
                                            //                     MemberId = selectedMember.ServiceResource.RelatedRecordId,
                                            //                     TeamRoleId = caseTeamRolesByName.get(caseRole).Id
                                            //                 );
                                            //                 caseMembersToInsert.add(caseTeamMember);
                                            //             }
                                            //         }
                                            //     }
                                            // }
                                        }
        
                                        // Calcular la próxima visita estimada
                                        if(casesNoExcepcionales != null){
                                            if(visitaTratamientoBySApp.containsKey(app.Id)){
                                                for(Case ca: casesNoExcepcionales){
                                                    for(Visita_tratamiento__c visitaTratamiento: visitaTratamientoBySApp.get(app.Id)){
                                                        if(visitaTratamiento.Estado__c == 'Revisado' && visitaTratamiento.Tratamiento__c == ca.Id){
                                                            ca.NextServiceAppointment__c = DateTime.now().dateGMT().addDays(Integer.valueOf(tratamientoFrecuencia.get(ca.Tratamiento__c)));
                                                            casesToUpdate.add(ca);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
        
                                //------------------------------Flujo Pharmate // TRD excepcional // Oxigenoterapia excepcional------------------------------
                                if(app.Paso_Protocolo__c != null){
                                    Id protocoloPadre = protocolStepData.get(app.Paso_Protocolo__c).protocolo_Padre__c;
                                    List<Paso_Protocolo__c> pasos = protocolListPasosMap.get(protocoloPadre);
                                    Paso_Protocolo__c nextStep = ETH_ServiceAppointmentTriggerHandler.getNextStep(pasos, protocolStepData.get(app.Paso_Protocolo__c), protocolStepData.get(app.Paso_Protocolo__c).protocolo_Padre__r.tipo_prot__c);
                                    List<ServiceTerritory> selectedServiceTerritories = serviceTerritoryByPostalCode.get(accountPostalCode.get(app.AccountId));
        
                                    Worktype worktype;
                                    Id workTypeGroup;
                                    String appointmentType;
                                    Boolean asignacionGrupoPharmate = false;
                                    String lineaNegocio = app.Linea_negocio__c;
    
                                    // Campo Movilidad. Tenemos en cuenta si el paciente NO tiene movilidad. En caso de no tener y que la siguiente cita sea CRETA-PRESENCIAL, se pasa a DOMICILIO-PRESENCIAL
                                    if(accountMovilidad.get(app.AccountId) == 'No' && nextStep.lugar_visita__c == 'CRETA' && nextStep.tipo_Lugar__c == 'Presencial'){
                                        worktype = ETH_Utils.getWorktypeByName(worktypes, nextStep.tipo_visita__c, lineaNegocio, 'Domicilio', 'Presencial');
                                        workTypeGroup = worktypeGroupMap.get(worktype.Id);
                                        appointmentType = ETH_Utils.getAppointmentType('Domicilio', 'Presencial');
                                    }
                                    else{
                                        worktype = ETH_Utils.getWorktypeByName(worktypes, nextStep.tipo_visita__c, lineaNegocio, nextStep.lugar_visita__c, nextStep.tipo_Lugar__c);
                                        workTypeGroup = worktypeGroupMap.get(worktype.Id);
                                        appointmentType = ETH_Utils.getAppointmentType(nextStep.lugar_visita__c, nextStep.tipo_Lugar__c);
                                    }
    
                                    ServiceTerritory selectedServiceTerritory = ETH_ServiceAppointmentTriggerHandler.selectServiceTerritory(selectedServiceTerritories, cretas, worktype, accountAddress.get(app.AccountId).getLatitude(), accountAddress.get(app.AccountId).getLongitude());
                                    if(selectedServiceTerritory == null) {
                                        System.debug('De los Service Territories para este código postal, no se encontró ninguno acorde.');
                                        app.addError('No se encontró ningún territorio de servicio para el código postal del paciente.');
                                        asignacionGrupoPharmate = true;
                                    }
                                    
                                    //Obtener los ServiceTerritoryMembers
                                    //En caso de CRETA serán Boxes y en caso de Domicilio serán profesionales
                                    List<ServiceTerritoryMember> serviceTerritoryMemberCandidates;
                                    if(!asignacionGrupoPharmate){
                                        //En caso de que fuera a domicilio, reducimos la lista al profesional/es que corresponda
                                        if(!worktype.Name.contains('CRETA')){
                                            try{
                                                serviceTerritoryMemberCandidates = serviceTerritoryMembersByParentTerritory.get(selectedServiceTerritory.ParentTerritoryId);
                                                if(serviceTerritoryMemberCandidates == null){
                                                    System.debug('No se encontraron Service Territory Members en el Service Territory');
                                                    asignacionGrupoPharmate = true;
                                                }
                                                else{
                                                    List<ServiceTerritoryMember> profesionalSelected = new List<ServiceTerritoryMember>();
                                                    for(ServiceTerritoryMember member : serviceTerritoryMemberCandidates){
                                                        if(member.Roles__c != null && member.Roles__c.contains(nextStep.tipo_prof__c)){
                                                                profesionalSelected.add(member);
                                                        }
                                                    }
                                                    serviceTerritoryMemberCandidates = profesionalSelected;
                                                }
                                            }catch(Exception e){
                                                System.debug('No se encontraron Service Territory Members en el Service Territory');
                                                asignacionGrupoPharmate = true;
                                            }
                                        }
                                        else{
                                            //Consultar los boxes del CRETA (no cogemos el padre)
                                            serviceTerritoryMemberCandidates = serviceTerritoryMembersByParentTerritory.get(selectedServiceTerritory.Id);
                                        }
                                    }
    
                                    // Encontrar el Case relacionado
                                    List<Case> cases = casesByAccountMap.get(app.AccountId);
                                    Case caseToUpdate;
                                    for(Case ca: cases){
                                        if(ca.Id == app.Tratamiento__c){
                                            caseToUpdate = ca;
                                            break;
                                        }
                                    }
        
                                    // Date siguienteFechaPrevista = DateTime.now().dateGMT().addDays(Integer.valueOf(nextStep.frecuencia__c));
                                    Date siguienteFechaPrevista = app.Fecha_Realizada__c.dateGMT().addDays(Integer.valueOf(nextStep.frecuencia__c));
    
                                    Id recordTypeCita;
                                    Id ownerColaCita;
                                    String subject;
                                    if(lineaNegocio == 'Pharmate'){
                                        recordTypeCita = recordTypePharmate;
                                        ownerColaCita = grupoPharmateQueue.Id;
                                        subject = nextStep.Name + ' - ' + nextStep.protocolo_Padre__r.Name;
                                    }
                                    else{
                                        recordTypeCita = recordTypeTRD;
                                        ownerColaCita = contactCenter.Id;
                                        subject = 'Seguimiento - ' + tratamientoName.get(app.Tratamiento__c);
                                    } 
    
                                    if(asignacionGrupoPharmate || !(serviceTerritoryMemberCandidates.size() >= 1)){
                                        // No ha encontrado profesional o boxes
                                        // Asignamos a la cola de grupo Pharmate con tarea
                                        Id serviceTerritoryId = selectedServiceTerritory == null ? null : ( worktype.Name.contains('CRETA') ? selectedServiceTerritory.Id : selectedServiceTerritory.ParentTerritoryId);
                                        String msg = serviceTerritoryId == null ? 'Motivo: no se encontró un Service Territory con el código postal adecuado.' : 'Motivo: no se encontraron profesionales acordes en el territorio correspondiente.';
    
                                        ServiceAppointment visitaSeguimiento = new ServiceAppointment(
                                            RecordTypeId = recordTypeCita,
                                            AppointmentType = appointmentType,
                                            OwnerId = ownerColaCita,
                                            Linea_negocio__c = lineaNegocio,
                                            ParentRecordId = app.AccountId,
                                            Fec_prev__c = ETH_ServiceAppointmentTriggerHandler.adaptarFechaMismoDiaSemana(Date.newInstance(app.Fecha_Realizada__c.year(), app.Fecha_Realizada__c.month(), app.Fecha_Realizada__c.day()), siguienteFechaPrevista),
                                            Status = 'Prevista',
                                            Subject = subject,
                                            City = accountAddress.get(app.AccountId).City,
                                            Country = accountAddress.get(app.AccountId).Country,
                                            State = accountAddress.get(app.AccountId).State,
                                            Street = accountAddress.get(app.AccountId).Street,
                                            PostalCode = accountAddress.get(app.AccountId).PostalCode,
                                            Paso_Protocolo__c = nextStep.Id,
                                            ServiceTerritoryId = serviceTerritoryId,
                                            WorkTypeId = worktype.Id,
                                            Tratamiento__c = app.Tratamiento__c,
                                            DurationType = 'Minutes',
                                            Duration = worktypes.get(worktype.Name).EstimatedDuration
                                        );
                                        visitasWithoutResourceToInsert.add(visitaSeguimiento);
        
                                        Task tareaSeguimiento = new Task(
                                            OwnerId = ownerColaCita,
                                            Description = 'Nueva visita de seguimiento por agendar para el paciente ' + accountName.get(app.AccountId) + '. ' + msg,
                                            Subject = 'Nueva visita de seguimiento por agendar',
                                            WhatId = caseToUpdate.Id
                                        );
                                        tasksWithoutResourceToInsert.add(tareaSeguimiento); 
                                    }
                                    else{
                                        // Agendamos
                                        ServiceTerritoryMember selectedMember = null;
                                        Datetime fechaAgendada = null;
    
                                        ETH_AgendamientoWrapper wrapperResult = ETH_ServiceAppointmentTriggerHandler.agendamientoCaller(app.Fecha_Realizada__c, siguienteFechaPrevista, Double.valueOf(nextStep.frecuencia__c), parametrosAutoagendamiento, serviceTerritoryMemberCandidates, worktype, operatingHoursTimeSlotMap, topicTimeSlotMap);
                                        selectedMember = wrapperResult.member;
                                        fechaAgendada = wrapperResult.fecha;
    
                                        //Distinguimos por si ha encontrado fecha o no
                                        if(fechaAgendada != null){
                                            Id ownerId;
                                            String status = 'Prevista';
                                            String role;
                                            Boolean isRequiredResource = false;
                                            Boolean isPrimaryResource = false;
                                            if(worktype.Name.contains('CRETA')){
                                                ownerId = ownerColaCita;
                                                role = 'Box CRETA';
                                            }
                                            else{
                                                ownerId = selectedMember.ServiceResource.RelatedRecordId;
                                                role = nextStep.tipo_prof__c;
                                            }
    
                                            ServiceAppointment visitaSeguimiento = new ServiceAppointment(
                                                RecordTypeId = recordTypeCita,
                                                AppointmentType = appointmentType,
                                                Linea_negocio__c = lineaNegocio,
                                                OwnerId = ownerId,
                                                ParentRecordId = app.AccountId,
                                                Status = status,
                                                Subject = subject,
                                                City = accountAddress.get(app.AccountId).City,
                                                Country = accountAddress.get(app.AccountId).Country,
                                                State = accountAddress.get(app.AccountId).State,
                                                Street = accountAddress.get(app.AccountId).Street,
                                                PostalCode = accountAddress.get(app.AccountId).PostalCode,
                                                Paso_Protocolo__c = nextStep.Id,
                                                ServiceTerritoryId = selectedMember.ServiceTerritoryId,
                                                WorkTypeId = worktype.Id,
                                                Tratamiento__c = app.Tratamiento__c,
                                                DurationType = 'Minutes',
                                                Duration = worktypes.get(worktype.Name).EstimatedDuration,
                                                SchedStartTime = fechaAgendada,
                                                SchedEndTime = fechaAgendada.addMinutes(worktype.EstimatedDuration.intValue())
                                            );
                                            visitasToInsert.add(visitaSeguimiento);
            
                                            AssignedResource assignedResource = new AssignedResource(
                                                ServiceResourceId = selectedMember.ServiceResourceId,
                                                Role = role,
                                                IsRequiredResource = isRequiredResource,
                                                IsPrimaryResource = isPrimaryResource
                                            );
                                            assignedResourcesToInsert.add(assignedResource);
    
                                        }
                                        else{
                                            //distinguimos CRETA de Domicilio.
                                            Id ownerId;
                                            if(worktype.Name.contains('CRETA')){
                                                //CRETA sin AssignedResource, solo con el Service Territory y al Grupo pharmate
                                                ownerId = ownerColaCita;
                                                selectedMember = serviceTerritoryMemberCandidates.get(0);
                                            }
                                            else{
                                                //Domicilio con Assigned Resource y owner él mismo
                                                ownerId = selectedMember.ServiceResource.RelatedRecordId;
                                                AssignedResource assignedResource = new AssignedResource(
                                                    ServiceResourceId = selectedMember.ServiceResourceId,
                                                    Role = nextStep.tipo_prof__c,
                                                    IsRequiredResource = false,
                                                    IsPrimaryResource = false
                                                );
                                                assignedResourcesToInsert.add(assignedResource);
                                            }
    
                                            ServiceAppointment visitaSeguimiento = new ServiceAppointment(
                                                RecordTypeId = recordTypeCita,
                                                AppointmentType = appointmentType,
                                                Linea_negocio__c = lineaNegocio,
                                                OwnerId = ownerId,
                                                ParentRecordId = app.AccountId,
                                                Fec_prev__c = ETH_ServiceAppointmentTriggerHandler.adaptarFechaMismoDiaSemana(Date.newInstance(app.Fecha_Realizada__c.year(), app.Fecha_Realizada__c.month(), app.Fecha_Realizada__c.day()), siguienteFechaPrevista),
                                                Status = 'Prevista',
                                                Subject = subject,
                                                City = accountAddress.get(app.AccountId).City,
                                                Country = accountAddress.get(app.AccountId).Country,
                                                State = accountAddress.get(app.AccountId).State,
                                                Street = accountAddress.get(app.AccountId).Street,
                                                PostalCode = accountAddress.get(app.AccountId).PostalCode,
                                                Paso_Protocolo__c = nextStep.Id,
                                                ServiceTerritoryId = selectedMember.ServiceTerritoryId,
                                                WorkTypeId = worktype.Id,
                                                Tratamiento__c = app.Tratamiento__c,
                                                DurationType = 'Minutes',
                                                Duration = worktypes.get(worktype.Name).EstimatedDuration
                                            );
                                            visitasToInsert.add(visitaSeguimiento);
                                        }
    
                                        // // Controlar el Case Team
                                        // if(!worktype.Name.contains('CRETA')){
                                        //     List<CaseTeamMember> teamMembers = caseTeamMembersByCase.get(caseToUpdate.Id);
                                        //     Boolean memberDentro = false;
                                        //     if(teamMembers != null){
                                        //         for(CaseTeamMember member: teamMembers){
                                        //             if(member.MemberId == selectedMember.ServiceResource.RelatedRecordId){
                                        //                 memberDentro = true;
                                        //                 break;
                                        //             }
                                        //         }
                                        //     }
                                            
                                        //     if(!memberDentro){
                                        //         CaseTeamMember caseTeamMember = new CaseTeamMember(
                                        //             ParentId = caseToUpdate.Id,
                                        //             MemberId = selectedMember.ServiceResource.RelatedRecordId,
                                        //             TeamRoleId = caseTeamRolesByName.get(nextStep.tipo_prof__c).Id
                                        //         );
                                        //         caseMembersToInsert.add(caseTeamMember);
                                        //     }
                                        // }
                                    }
    
                                    // Calcular la próxima visita estimada
                                    caseToUpdate.NextServiceAppointment__c = siguienteFechaPrevista;
                                    casesToUpdate.add(caseToUpdate);
                                }
                            }
                        }
                    }
                }
            }
        }
    
    
        /*
                _         _ _           __ _          
            | |__ _  _| | |__  __ _ / _| |_ ___ _ _
            | '_ \ || | | / / / _` |  _|  _/ -_) '_|
            |_.__/\_,_|_|_\_\ \__,_|_|  \__\___|_| 
        */
            
        if(Trigger.isBefore) {
            // if(Trigger.isInsert) {}
            // if(Trigger.isUpdate) {}
        }
    
        if(Trigger.isAfter) {
            if(Trigger.isInsert) {}
            if(Trigger.isUpdate) {
                update casesToUpdate;
                System.debug(visitasToInsert);
                insert visitasToInsert;
                for(Integer i = 0; i < assignedResourcesToInsert.size(); i++) {
                    assignedResourcesToInsert.get(i).ServiceAppointmentId = visitasToInsert.get(i).Id;
                }
                insert assignedResourcesToInsert;
    
                insert visitasWithoutResourceToInsert;
                for(Integer i = 0; i < visitasWithoutResourceToInsert.size(); i++){
                    tasksWithoutResourceToInsert.get(i).Service_Appointment__c = visitasWithoutResourceToInsert.get(i).Id;
                }
                insert tasksWithoutResourceToInsert;
                insert caseMembersToInsert;

                if(!visitasToCallEquiposYFungiblesAtlas.isEmpty()){
                    OM_AtlasCallout.checkEquiposFungibles(visitasToCallEquiposYFungiblesAtlas[0].Id);
                    OM_AtlasCallout.checkEquiposSuministrados(visitasToCallEquiposYFungiblesAtlas[0].Id);
                    OM_AtlasCallout.informarEquiposRetirados(visitasToCallEquiposYFungiblesAtlas[0].Id);
                }
            }
        }
    }
}