trigger OM_ContentEventTrigger on OM_Content_event__e (after insert) {
  for (OM_Content_event__e event : Trigger.New) {
    if (event.OM_Action__c == 'refreshToken') {
      System.debug('Refresh token event received ' + event);
      OM_ContentSetting__c settings = OM_ContentService.getSettings(event.OM_Settings_name__c);
      OM_ContentService.updateUserAccess(settings, event.OM_UserId__c, event.OM_JSON_data__c);    
    }
  }
}