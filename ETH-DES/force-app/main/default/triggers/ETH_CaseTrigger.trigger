/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 08-30-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
trigger ETH_CaseTrigger on Case (after insert, before insert) {
    Trigger_Controller__mdt triggerController = ETH_Utils.getTriggerController();
    if(triggerController.Case_Trigger__c){
        Id recordTypeTratamientoPharmate = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Tratamiento_Pharmate').getRecordTypeId();
        Id recordTypeTratamientoTRD = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Tratamiento_TRD').getRecordTypeId();

        List<ServiceAppointment> visitasToInsert = new List<ServiceAppointment>();
        List<ServiceAppointment> visitasWithoutResourceToInsert = new List<ServiceAppointment>();
        List<Task> tasksWithoutResourceToInsert = new List<Task>();
        List<AssignedResource> assignedResourcesToInsert = new List<AssignedResource>();
        List<Id> caseIds = new List<Id>();
        List<Id> accountIds = new List<Id>();
        List<Case> casesToUpdate = new List<Case>();
        Map<Id, Id> caseTratamientoMapPharmate =  new Map<Id, Id>();
        List<Id> tratamientoIdsTRD = new List<Id>();
        Map<Id, Tratamiento__c> tratamientosInfoTRD;
        List<Id> upabIds = new List<Id>();
        List<Id> clienteIds = new List<Id>();
        for(Case ca : (List<Case>) Trigger.new){
            caseIds.add(ca.Id);
            System.debug(ca.SourceId);
            accountIds.add(ca.AccountId);
            clienteIds.add(ca.Cliente__c);
            upabIds.add(ca.Upab__c);
            //solo para pharmate para encontrar el primer paso del protocolo (puede que extendamos a los excepcionales de TRD)
            if(recordTypeTratamientoPharmate == ca.RecordTypeId){
                caseTratamientoMapPharmate.put(ca.Id, ca.Tratamiento__c);
            }
            else if(recordTypeTratamientoTRD == ca.RecordTypeId){
                tratamientoIdsTRD.add(ca.Tratamiento__c);
            }
        }
    
        //Id caso- info del caso
        Map<Id, Case> relatedInfoFromCase;
        Map<Id, Account> addressByAccount;
        //Buscamos el primer paso del protocolo correspondiente a cada tratamiento. caseId-paso
        Map<Id, Paso_Protocolo__c> firstStepProtocolPharmate;
        //Nombre del worktype- worktype
        Map<String, Worktype> worktypes;
        //IdUsuario-ServiceResource
        Map<Id, ServiceResource> serviceResourcesPharmate;
        //codPostal- ServiceTerritory (Pharmate y TRD)
        Map<String, List<ServiceTerritory>> serviceTerritoriesByPostalCode;
        List<Id> parentServiceTerritoriesId = new List<Id>();
        Map<String, CaseTeamRole> caseTeamRolesByName;
        List<CaseTeamMember> caseMemberToInsert = new List<CaseTeamMember>();
        List<ServiceTerritory> cretas;
        // id service territory - service territory member (para consultar el Service Resource)
        Map<Id, List<ServiceTerritoryMember>> serviceTerritoryMembersByParentTerritory;
        Map<Id, Map<String, List<TimeSlot>>> operatingHoursTimeSlotMap;
        Map<Id, AppointmentTopicTimeSlot> topicTimeSlotMap;
        List<Id> operatingHoursIds = new List<Id>();
        List<Id> timeSlotIds = new List<Id>();
        Autoagendamiento__mdt parametrosAutoagendamiento;
        // List<Tipificado_Casos__mdt> tipificadoCasos;
        // Map<String, Lote__c> lotesByName;
        // Map<Id, EmailMessage> emailMessageByCase;
        Group contactCenter;
        Map<Id, Maestro_UPAB__c> upabAbreviacion;
        Map<Id, Cliente__c> clienteAbreviacion;

        /*
            _         _ _     _          __            
        | |__ _  _| | |__ | |__  ___ / _|___ _ _ ___
        | '_ \ || | | / / | '_ \/ -_)  _/ _ \ '_/ -_)
        |_.__/\_,_|_|_\_\ |_.__/\___|_| \___/_| \___| 
        */     

        // Tanto si es before como en el after necesitamos estas consultas
        if(Trigger.isInsert){
            worktypes = ETH_Utils.getWorktypes();
            firstStepProtocolPharmate = ETH_CaseTriggerHandler.getFirstStepProtocol(caseTratamientoMapPharmate);
            addressByAccount = ETH_CaseTriggerHandler.getAddressByAccount(accountIds);
            List<String> postalCodes = new List<String>();
            for(Account account: addressByAccount.values()) {
                postalCodes.add(account.ShippingPostalCode);
            }
            serviceTerritoriesByPostalCode = ETH_ServiceAppointmentTriggerHandler.getServiceTerritoriesByPostalCode(postalCodes, parentServiceTerritoriesId);
            tratamientosInfoTRD = ETH_CaseTriggerHandler.getTratamientosInfo(tratamientoIdsTRD);
        }

        if(Trigger.isBefore) {
            if(Trigger.isInsert) {
                //En el before no tenemos el createdBy. Ojo si fueran varios.
                //Esta comprobación solo tiene sentido cuando se ha dado de alta UN tratamiento en Pharmate
                if(Trigger.new.size() == 1 && recordTypeTratamientoPharmate == Trigger.new[0].RecordTypeId){
                    List<Id> userIdsAltaPharmate = new List<Id>();
                    userIdsAltaPharmate.add(UserInfo.getUserId());
                    serviceResourcesPharmate = ETH_CaseTriggerHandler.getServiceResourceByUserId(userIdsAltaPharmate);
                }
                upabAbreviacion = ETH_CaseTriggerHandler.getUpabAbreviacion(upabIds);
                clienteAbreviacion = ETH_CaseTriggerHandler.getClienteAbreviacion(clienteIds);
            }
            // if(Trigger.isUpdate) {}
        }
        
        if(Trigger.isAfter) {
            if(Trigger.isInsert) {
                caseTeamRolesByName = ETH_Utils.getCaseTeamRolesByName();
                List<Id> userIdsAltaPharmate = new List<Id>();
                for(Case ca : (List<Case>) Trigger.new){
                    if(recordTypeTratamientoPharmate == ca.RecordTypeId){
                        userIdsAltaPharmate.add(ca.CreatedById);
                    }
                }
                serviceResourcesPharmate = ETH_CaseTriggerHandler.getServiceResourceByUserId(userIdsAltaPharmate);

                //Registros que necesitamos para TRD
                cretas = ETH_ServiceAppointmentTriggerHandler.getCRETAS();
                //Añadir los CRETA como padres para que nos traiga los boxes también
                for(ServiceTerritory creta: cretas){
                    parentServiceTerritoriesId.add(creta.Id);
                }
                serviceTerritoryMembersByParentTerritory = ETH_ServiceAppointmentTriggerHandler.getServiceResourcesByTerritory(parentServiceTerritoriesId);
                for(List<ServiceTerritoryMember> members: serviceTerritoryMembersByParentTerritory.values()){
                    for(ServiceTerritoryMember member: members){
                        operatingHoursIds.add(member.OperatingHoursId);
                    }
                }
                operatingHoursTimeSlotMap = ETH_ServiceAppointmentTriggerHandler.getFranjasHorarias(operatingHoursIds, timeSlotIds);
                topicTimeSlotMap = ETH_ServiceAppointmentTriggerHandler.getTopicTimeSlot(timeSlotIds);
                parametrosAutoagendamiento = ETH_ServiceAppointmentTriggerHandler.getRangoDias();
                relatedInfoFromCase = ETH_CaseTriggerHandler.getRelatedInfoFromCase(caseIds);
                contactCenter = ETH_ServiceAppointmentTriggerHandler.getQueue('Contact Center');
                // lotesByName = ETH_CaseTriggerHandler.getLotesByName();
                // tipificadoCasos = ETH_Utils.getTipificadoCasos();
                // emailMessageByCase = ETH_CaseTriggerHandler.getEmailMessagesByCase(caseIds);
            }
            //if(Trigger.isUpdate) {}
        }

        /*                         _           _   _            
                _ _ ___ __ ___ _ _ __| |  __ _ __| |_(_)___ _ _  ___
            | '_/ -_) _/ _ \ '_/ _` | / _` / _|  _| / _ \ ' \(_-<
            |_| \___\__\___/_| \__,_| \__,_\__|\__|_\___/_||_/__/
        */
        for(Case ca : (List<Case>) Trigger.new) {
            if(Trigger.isBefore) {
                if(Trigger.isInsert) {
                    if(triggerController.Auto_Altas_Case_Trigger__c){
                        //validaciones. Se lanza la última que encuentre
                        //La comprobación del serviceTerritory no aplica a incidencias
                        //Solo Pharmate
                        if(recordTypeTratamientoPharmate == ca.RecordTypeId){
                            Id serviceTerritoryId;
                            try{
                                List<ServiceTerritory> serviceTerritories = serviceTerritoriesByPostalCode.get(addressByAccount.get(ca.AccountId).ShippingPostalCode);
                                if(serviceTerritories != null){
                                    Id serviceTerritoryRecordType = Schema.SObjectType.ServiceTerritory.getRecordTypeInfosByDeveloperName().get('Domicilio_Pharmate').getRecordTypeId();
                                    for(ServiceTerritory serviceTerritory: serviceTerritories){
                                        if(serviceTerritory.RecordTypeId == serviceTerritoryRecordType){
                                            serviceTerritoryId = serviceTerritory.Id;
                                            break;
                                        }
                                    }
                                }
                                if(serviceTerritoryId == null){
                                    ca.addError('No se encontraron Service Territories para este código postal.');
                                }
                            }catch(Exception e){
                                ca.addError('No se encontraron Service Territories para este código postal.');
                            }

                            if(addressByAccount.containsKey(ca.AccountId) && addressByAccount.get(ca.AccountId).ShippingPostalCode == null){
                                ca.addError('Debe informar la dirección del paciente para dar de alta un tratamiento. Por favor, especifique al menos el código postal');
                            }
                        }   

                        //Solo para Pharmate
                        if(recordTypeTratamientoPharmate == ca.RecordTypeId){
                            Id pasoProtocoloId;
                            try{
                                pasoProtocoloId = firstStepProtocolPharmate.get(ca.Tratamiento__c).Id;
                                if(pasoProtocoloId == null){
                                    ca.addError('Error al buscar el primer paso del protocolo.');
                                }
                            }catch(Exception e){
                                ca.addError('Error al buscar el primer paso del protocolo.');
                            }

                            //calcular siguiente cita
                            try{
                                ca.NextServiceAppointment__c = Date.today().addDays(Integer.valueOf(firstStepProtocolPharmate.get(ca.Tratamiento__c).frecuencia__c));
                            }catch(Exception e){
                                ca.addError('El tratamiento tiene que tener un protocolo con pasos y todos sus respectivos datos.');
                            }

                            // Códigos de identificación para pacientes de Pharmate
                            // Buscar si ya existe algún tratamiento con ese cliente + upab
                            String codigoPaciente = ETH_CaseTriggerHandler.getCodigoPacientePharmate(clienteAbreviacion.get(ca.Cliente__c).Abreviacion__c, upabAbreviacion.get(ca.Upab__c).Abreviacion__c, ca);
                            ca.Cod_Iden__c = codigoPaciente;
                            
                        }
                        else if(recordTypeTratamientoTRD == ca.RecordTypeId){
                            //calcular siguiente cita
                            try{
                                ca.NextServiceAppointment__c = Date.today();
                            }catch(Exception e){
                                ca.addError('El tratamiento tiene que tener la frecuencia de alta especificada.');
                            }
                        }

                        //Esta comprobación solo tiene sentido cuando se ha dado de alta UN tratamiento en Pharmate
                        if(Trigger.new.size() == 1 && recordTypeTratamientoPharmate == ca.RecordTypeId){
                            Id serviceResourceId;
                            try{
                                serviceResourceId = serviceResourcesPharmate.get(UserInfo.getUserId()).Id;
                                if(serviceResourceId == null){
                                    ca.addError('No se encontró un Service Resource asociado a su usuario.');
                                }
                            }catch(Exception e){
                                ca.addError('No se encontró un Service Resource asociado a su usuario.');
                            }
                        }
                    }
                }
                //if(Trigger.isUpdate) {}
            }
    
            if(Trigger.isAfter) {
                if(Trigger.isInsert) {
                    // if(emailMessageByCase.containsKey(ca.Id)){
                    //     // Caso procedente de Email to Case, tipificamos
                    //     ETH_CaseTriggerHandler.tipificarCasoEmail(tipificadoCasos, ca, lotesByName, emailMessageByCase.get(ca.Id));
                    //     casesToUpdate.add(ca);
                    // }

                    if(triggerController.Auto_Altas_Case_Trigger__c){
                        // -------------------------TRD------------------------------------
                        // if(recordTypeTratamientoTRD == ca.RecordTypeId){
                        //     //Si es UHD (hospital) o el tratamiento tiene el check de no planificar altas, derivamos a CONTACT
                        //     Boolean asignacionCONTACT = false;
                        //     Worktype worktype;
                        //     String appointmentType;

                        //     //Las altas son siempre presenciales, CRETA o domicilio lo determina el campo movilidad del paciente
                        //     if(relatedInfoFromCase.get(ca.Id).Account.Movilidad__c == 'Si'){
                        //         //CRETA
                        //         worktype = ETH_Utils.getWorktypeByName(worktypes, 'Alta', 'TRD', 'Presencial', 'CRETA');
                        //         appointmentType = ETH_Utils.getAppointmentType('CRETA', 'Presencial');
                        //     }
                        //     else{
                        //         //Domicilio
                        //         worktype = ETH_Utils.getWorktypeByName(worktypes, 'Alta', 'TRD', 'Presencial', 'Domicilio');
                        //         appointmentType = ETH_Utils.getAppointmentType('Domicilio', 'Presencial');
                        //     }

                        //     if((ca.Type == 'UHD') || (tratamientosInfoTRD.get(ca.Tratamiento__c).No_plan_altas__c)){
                        //         asignacionCONTACT = true;
                        //     }
                        //     else{
                        //         List<ServiceTerritory> selectedServiceTerritories = serviceTerritoriesByPostalCode.get(addressByAccount.get(ca.AccountId).ShippingPostalCode);
                        //         ServiceTerritory selectedServiceTerritory = ETH_ServiceAppointmentTriggerHandler.selectServiceTerritory(selectedServiceTerritories, cretas, worktype, Double.valueOf(addressByAccount.get(ca.AccountId).ShippingLatitude), Double.valueOf(addressByAccount.get(ca.AccountId).ShippingLongitude));
                        //         if(selectedServiceTerritory == null) {
                        //             //app.addError('De los Service Territories para este código postal, no se encontró ninguno acorde.');
                        //             System.debug('De los Service Territories para este código postal, no se encontró ninguno acorde.');
                        //             asignacionCONTACT = true;
                        //         }

                        //         List<ServiceTerritoryMember> serviceTerritoryMemberCandidates;
                        //         //Obtener los ServiceTerritoryMembers
                        //         //En caso de CRETA serán Boxes y en caso de Domicilio serán profesionales
                        //         if(!asignacionCONTACT) {
                        //             //En caso de que fuera a domicilio, reducimos la lista al profesional/es que corresponda
                        //             if(!worktype.Name.contains('CRETA')){
                        //                 try{
                        //                     serviceTerritoryMemberCandidates = serviceTerritoryMembersByParentTerritory.get(selectedServiceTerritory.ParentTerritoryId);
                        //                     if(serviceTerritoryMemberCandidates == null){
                        //                         //app.addError('No se encontraron Service Territory Members en el Service Territory');
                        //                         System.debug('No se encontraron Service Territory Members en el Service Territory');
                        //                         asignacionCONTACT = true;
                        //                     }
                        //                 }catch(Exception e){
                        //                     //app.addError('No se encontraron Service Territory Members en el Service Territory');
                        //                     System.debug('No se encontraron Service Territory Members en el Service Territory');
                        //                     asignacionCONTACT = true;
                        //                 }
                        //             }
                        //             else{
                        //                 //Consultar los boxes del CRETA (no cogemos el padre)
                        //                 serviceTerritoryMemberCandidates = serviceTerritoryMembersByParentTerritory.get(selectedServiceTerritory.Id);
                        //             }
                        //         }

                        //         Date siguienteFechaPrevista = DateTime.now().dateGMT().addDays(Integer.valueOf(tratamientosInfoTRD.get(ca.Tratamiento__c).fre_vis_ini__c));
                                
                        //         if(asignacionCONTACT || !(serviceTerritoryMemberCandidates.size() >= 1)){
                        //             // No ha encontrado profesional o boxes
                        //             // Asignamos a la cola de CONTACT con tarea
                        //             Id serviceTerritoryId = selectedServiceTerritory == null ? null : ( worktype.Name.contains('CRETA') ? selectedServiceTerritory.Id : selectedServiceTerritory.ParentTerritoryId);
                        //             String msg = serviceTerritoryId == null ? 'Motivo: no se encontró un Service Territory con el código postal adecuado.' : 'Motivo: no se encontraron profesionales acordes en el territorio correspondiente.';

                        //             ServiceAppointment visitaAlta = new ServiceAppointment(
                        //                 RecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('Visita_TRD').getRecordTypeId(),
                        //                 AppointmentType = appointmentType,
                        //                 OwnerId = contactCenter.Id,
                        //                 ParentRecordId = ca.AccountId,
                        //                 Status = 'Prevista',
                        //                 Subject = 'Alta ' + tratamientosInfoTRD.get(ca.Tratamiento__c).Name,
                        //                 City = addressByAccount.get(ca.AccountId).ShippingCity,
                        //                 Country = addressByAccount.get(ca.AccountId).ShippingCountry,
                        //                 State = addressByAccount.get(ca.AccountId).ShippingState,
                        //                 Street = addressByAccount.get(ca.AccountId).ShippingStreet,
                        //                 PostalCode = addressByAccount.get(ca.AccountId).ShippingPostalCode,
                        //                 ServiceTerritoryId = serviceTerritoryId,
                        //                 WorkTypeId = worktype.Id,
                        //                 // Tratamiento__c = ca.Tratamiento__c,
                        //                 DurationType = 'Minutes',
                        //                 Duration = worktypes.get(worktype.Name).EstimatedDuration
                        //             );
                        //             visitasWithoutResourceToInsert.add(visitaAlta);

                        //             Task tareaSeguimiento = new Task(
                        //                 OwnerId = contactCenter.Id,
                        //                 Description = 'Nueva visita de alta por agendar para el paciente ' + relatedInfoFromCase.get(ca.Id).Account.Name +  ' con el tratamiento ' + tratamientosInfoTRD.get(ca.Tratamiento__c).Name + '. ' + msg,
                        //                 Subject = 'Nueva visita de alta por agendar',
                        //                 WhatId = ca.Id
                        //             );
                        //             tasksWithoutResourceToInsert.add(tareaSeguimiento); 
                        //         }
                        //         else{
                        //             // Agendamos
                        //             // En las altas no vamos a hacer ajuste del día para que coincida pues son frecuencias muy bajas (3 días)
                        //             // Hacemos una copia de fechaPrevista porque realmente es correcto lo que se haya calculado
                        //             Date fechaReferencia = DateTime.now().dateGMT();
                        //             Time timeInicioReferencia = null;
                        //             Date fechaPrevistaAjustada;                                
                        //             if(ETH_Utils.getDayOfTheWeek(fechaReferencia) != ETH_Utils.getDayOfTheWeek(siguienteFechaPrevista)){
                        //                 fechaPrevistaAjustada = ETH_ServiceAppointmentTriggerHandler.adaptarFechaMismoDiaSemana(fechaReferencia, siguienteFechaPrevista);
                        //             }
                        //             else{
                        //                 fechaPrevistaAjustada = siguienteFechaPrevista;
                        //             }

                        //             // Calcular el rango de días en el que vamos a buscar
                        //             Integer rangoDias;
                        //             Date fechaMaximaRango;
                        //             Date fechaMinimaRango;
                        //             if(tratamientosInfoTRD.get(ca.Tratamiento__c).fre_vis_ini__c >= parametrosAutoagendamiento.Umbral__c){
                        //                 rangoDias = Integer.valueOf(parametrosAutoagendamiento.Rango_Dias_Superior__c);
                        //                 fechaMaximaRango = fechaPrevistaAjustada.addDays(rangoDias);
                        //                 fechaMinimaRango = fechaPrevistaAjustada.addDays(-rangoDias);
                        //             }
                        //             else{
                        //                 rangoDias = Integer.valueOf(parametrosAutoagendamiento.Rango_Dias_Inferior__c);
                        //                 fechaMaximaRango = fechaPrevistaAjustada.addDays(rangoDias);
                        //                 fechaMinimaRango = fechaPrevistaAjustada;
                        //             }

                        //             //Aquí guardamos el los resultados, el box/persona y fecha+hora
                        //             ServiceTerritoryMember selectedMember = null;
                        //             Datetime fechaAgendada = null;
                        //             if((worktype.Name.contains('CRETA'))){
                        //                 //Primero necesitamos los IDs de los calendarios que tienen el nombre del Service Resource
                        //                 Map<Id, String> SMTIdNameBox = new Map<Id, String>();
                        //                 for(ServiceTerritoryMember member: serviceTerritoryMemberCandidates){
                        //                     SMTIdNameBox.put(member.Id, member.ServiceResource.Name);
                        //                 }

                        //                 Map<String, Id> boxNameCalendarId = ETH_ServiceAppointmentTriggerHandler.getCalendarIdsCRETA(SMTIdNameBox.values());

                        //                 //Eventos de los boxes candidatos 
                        //                 //id calendar - mapa(fecha, lista eventos)
                        //                 Map<Id, Map<String, List<Event>>> agendaBoxes = ETH_ServiceAppointmentTriggerHandler.getAgendaEventos(fechaMinimaRango, fechaMaximaRango, boxNameCalendarId.values());
                                
                        //                 //Wrapper porque en el caso del agendamiento de boxes necesitamos saber el box y la fecha
                        //                 ETH_AgendamientoWrapper wrapper;
                        //                 if(fechaReferencia != null){
                        //                     wrapper = ETH_ServiceAppointmentTriggerHandler.agendamientoBoxFechaFijada(serviceTerritoryMemberCandidates, operatingHoursTimeSlotMap, topicTimeSlotMap, 
                        //                     agendaBoxes, fechaPrevistaAjustada, fechaReferencia, timeInicioReferencia, fechaMinimaRango, fechaMaximaRango, worktype, SMTIdNameBox, boxNameCalendarId);
                        //                     selectedMember = wrapper.member;
                        //                     fechaAgendada = wrapper.fecha;
                        //                 }
                        //             }
                        //             else{
                        //                 //Búsqueda en agenda del profesional
                        //                 List<Id> recursosId = new List<Id>();
                        //                 Map<Id, ServiceTerritoryMember> usuarioSMT = new Map<Id, ServiceTerritoryMember>();
                        //                 for(ServiceTerritoryMember member: serviceTerritoryMemberCandidates){
                        //                     recursosId.add(member.ServiceResource.RelatedRecordId);
                        //                     usuarioSMT.put(member.ServiceResource.RelatedRecordId, member);
                        //                 }

                        //                 //id usuario - map(fecha, eventos)
                        //                 Map<Id, Map<String, List<Event>>> agendaProfesionales = ETH_ServiceAppointmentTriggerHandler.getAgendaEventos(fechaMinimaRango, fechaMaximaRango, recursosId);

                        //                 ETH_AgendamientoWrapper wrapper = ETH_ServiceAppointmentTriggerHandler.autoagendamientoProfesional(usuarioSMT, agendaProfesionales, operatingHoursTimeSlotMap, topicTimeSlotMap, fechaPrevistaAjustada, fechaReferencia, timeInicioReferencia, fechaMinimaRango, fechaMaximaRango, worktype);
                        //                 selectedMember = wrapper.member;
                        //                 fechaAgendada = wrapper.fecha;
                        //             }

                        //             //Distinguimos por si ha encontrado fecha o no
                        //             if(fechaAgendada != null){
                        //                 Id ownerId;
                        //                 String status;
                        //                 if(worktype.Name.contains('CRETA')){
                        //                     ownerId = contactCenter.Id;
                        //                     status = 'Programada';
                        //                 }
                        //                 else{
                        //                     ownerId = selectedMember.ServiceResource.RelatedRecordId;
                        //                     status = 'Prevista';
                        //                 }

                        //                 ServiceAppointment visitaAlta = new ServiceAppointment(
                        //                     RecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('Visita_TRD').getRecordTypeId(),
                        //                     AppointmentType = appointmentType,
                        //                     OwnerId = ownerId,
                        //                     ParentRecordId = ca.AccountId,
                        //                     Status = status,
                        //                     Subject = 'Alta ' + tratamientosInfoTRD.get(ca.Tratamiento__c).Name,
                        //                     City = addressByAccount.get(ca.AccountId).ShippingCity,
                        //                     Country = addressByAccount.get(ca.AccountId).ShippingCountry,
                        //                     State = addressByAccount.get(ca.AccountId).ShippingState,
                        //                     Street = addressByAccount.get(ca.AccountId).ShippingStreet,
                        //                     PostalCode = addressByAccount.get(ca.AccountId).ShippingPostalCode,
                        //                     ServiceTerritoryId = selectedMember.ServiceTerritoryId,
                        //                     WorkTypeId = worktype.Id,
                        //                     // Tratamiento__c = ca.Tratamiento__c,
                        //                     DurationType = 'Minutes',
                        //                     Duration = worktypes.get(worktype.Name).EstimatedDuration,
                        //                     SchedStartTime = fechaAgendada,
                        //                     SchedEndTime = fechaAgendada.addMinutes(worktype.EstimatedDuration.intValue())
                        //                 );
                        //                 visitasToInsert.add(visitaAlta);
        
                        //                 AssignedResource assignedResource = new AssignedResource(
                        //                     ServiceResourceId = selectedMember.ServiceResourceId,
                        //                     Role = selectedMember.Role,
                        //                     IsRequiredResource = true
                        //                 );
                        //                 assignedResourcesToInsert.add(assignedResource);

                        //             }
                        //             else{
                        //                 //distinguimos CRETA de Domicilio.
                        //                 Id ownerId;
                        //                 if(worktype.Name.contains('CRETA')){
                        //                     //CRETA sin AssignedResource, solo con el Service Territory y a CONTACT
                        //                     ownerId = contactCenter.Id;
                        //                     selectedMember = serviceTerritoryMemberCandidates.get(0);
                        //                 }
                        //                 else{
                        //                     //El que menos citas tuviese
                        //                     selectedMember = selectedMember;
                        //                     //Domicilio con Assigned Resource y owner él mismo
                        //                     ownerId = selectedMember.ServiceResource.RelatedRecordId;
                        //                     AssignedResource assignedResource = new AssignedResource(
                        //                         ServiceResourceId = selectedMember.ServiceResourceId,
                        //                         Role = selectedMember.Role,
                        //                         IsRequiredResource = true
                        //                     );
                        //                     assignedResourcesToInsert.add(assignedResource);
                        //                 }

                        //                 ServiceAppointment visitaAlta = new ServiceAppointment(
                        //                     RecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('Visita_TRD').getRecordTypeId(),
                        //                     AppointmentType = appointmentType,
                        //                     OwnerId = ownerId,
                        //                     ParentRecordId = ca.AccountId,
                        //                     Status = 'Prevista',
                        //                     Subject = 'Alta ' + tratamientosInfoTRD.get(ca.Tratamiento__c).Name,
                        //                     City = addressByAccount.get(ca.AccountId).ShippingCity,
                        //                     Country = addressByAccount.get(ca.AccountId).ShippingCountry,
                        //                     State = addressByAccount.get(ca.AccountId).ShippingState,
                        //                     Street = addressByAccount.get(ca.AccountId).ShippingStreet,
                        //                     PostalCode = addressByAccount.get(ca.AccountId).ShippingPostalCode,
                        //                     ServiceTerritoryId = selectedMember.ServiceTerritoryId,
                        //                     WorkTypeId = worktype.Id,
                        //                     // Tratamiento__c = ca.Tratamiento__c,
                        //                     DurationType = 'Minutes',
                        //                     Duration = worktypes.get(worktype.Name).EstimatedDuration
                        //                 );
                        //                 visitasToInsert.add(visitaAlta);
                        //             }
                        //         }
                        //     }
                        // }

                        //-------------------------PHARMATE------------------------------------  
                        if(recordTypeTratamientoPharmate == ca.RecordTypeId){
                            Worktype worktype = ETH_Utils.getWorktypeByName(worktypes, 'Alta', 'Pharmate', firstStepProtocolPharmate.get(ca.Tratamiento__c).lugar_visita__c, firstStepProtocolPharmate.get(ca.Tratamiento__c).tipo_Lugar__c);
                            Id pasoProtocoloId = firstStepProtocolPharmate.get(ca.Tratamiento__c).Id;
                            List<ServiceTerritory> serviceTerritories= serviceTerritoriesByPostalCode.get(addressByAccount.get(ca.AccountId).ShippingPostalCode);

                            Id serviceTerritoryId;
                            for(ServiceTerritory serviceTerritory: serviceTerritories){
                                if(serviceTerritory.RecordTypeId == Schema.SObjectType.ServiceTerritory.getRecordTypeInfosByDeveloperName().get('Domicilio_Pharmate').getRecordTypeId()){
                                    serviceTerritoryId = serviceTerritory.ParentTerritoryId;
                                    break;
                                }
                            }
                            
                            String appointmentType = ETH_Utils.getAppointmentType(firstStepProtocolPharmate.get(ca.Tratamiento__c).lugar_visita__c, firstStepProtocolPharmate.get(ca.Tratamiento__c).tipo_Lugar__c);
                            ServiceAppointment visitaAlta = new ServiceAppointment(
                                RecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('Visita_Pharmate').getRecordTypeId(),
                                AppointmentType = appointmentType,
                                Linea_negocio__c = 'Pharmate',
                                OwnerId = ca.CreatedById,
                                ParentRecordId = ca.AccountId,
                                Status = 'Prevista',
                                Subject = firstStepProtocolPharmate.get(ca.Tratamiento__c).Name + ' - ' + firstStepProtocolPharmate.get(ca.Tratamiento__c).protocolo_Padre__r.Name,
                                City = relatedInfoFromCase.get(ca.Id).Account.ShippingCity,
                                Country = relatedInfoFromCase.get(ca.Id).Account.ShippingCountry,
                                State = relatedInfoFromCase.get(ca.Id).Account.ShippingState,
                                Street = relatedInfoFromCase.get(ca.Id).Account.ShippingStreet,
                                PostalCode = relatedInfoFromCase.get(ca.Id).Account.ShippingPostalCode,
                                Paso_Protocolo__c = pasoProtocoloId,
                                ServiceTerritoryId = serviceTerritoryId,
                                WorkTypeId = worktype.Id,
                                Tratamiento__c = ca.Id,
                                DurationType = 'Minutes',
                                Duration = worktypes.get(worktype.Name).EstimatedDuration
                            );
                            visitasToInsert.add(visitaAlta);
        
                            Id serviceResourceId = serviceResourcesPharmate.get(ca.CreatedById).Id;
        
                            AssignedResource assignedResource = new AssignedResource(
                                ServiceResourceId = serviceResourceId,
                                IsRequiredResource = true
                            );
                            assignedResourcesToInsert.add(assignedResource);
                        }
                        CaseTeamMember caseTeamMember = new CaseTeamMember(
                            ParentId = ca.Id,
                            MemberId = ca.CreatedById,
                            TeamRoleId = caseTeamRolesByName.get('Care Coordinator').Id
                        );
                        caseMemberToInsert.add(caseTeamMember);
                    }

                    if(triggerController.Create_Folder_Case_Trigger__c && (recordTypeTratamientoPharmate == ca.RecordTypeId || recordTypeTratamientoTRD == ca.RecordTypeId)){
                        ETH_CaseTriggerHandler.createFolderSharePoint(relatedInfoFromCase.get(ca.Id).Tratamiento__r.Tratamiento_ET__c, relatedInfoFromCase.get(ca.Id).Account.IdAnonimo__c);
                    }
                }
                if(Trigger.isUpdate) {
                    if(triggerController.Create_Folder_Case_Trigger__c && ca.Crear_Carpeta_Sharepoint_Manual__c && (Trigger.oldMap.get(ca.Id).Crear_Carpeta_Sharepoint_Manual__c != ca.Crear_Carpeta_Sharepoint_Manual__c)){
                        ETH_CaseTriggerHandler.createFolderSharePoint(relatedInfoFromCase.get(ca.Id).Tratamiento__r.Tratamiento_ET__c, relatedInfoFromCase.get(ca.Id).Account.IdAnonimo__c);
                    }
                }
            }
        }
    
    
        /*
                _         _ _           __ _          
            | |__ _  _| | |__  __ _ / _| |_ ___ _ _
            | '_ \ || | | / / / _` |  _|  _/ -_) '_|
            |_.__/\_,_|_|_\_\ \__,_|_|  \__\___|_| 
        */
            
        // if(Trigger.isBefore) {
        //     if(Trigger.isInsert) {}
        //     if(Trigger.isUpdate) {}
        // }
    
        if(Trigger.isAfter) {
            if(Trigger.isInsert) {
                insert visitasToInsert;
                for(Integer i = 0; i < assignedResourcesToInsert.size(); i++) {
                    assignedResourcesToInsert.get(i).ServiceAppointmentId = visitasToInsert.get(i).Id;
                }
                insert assignedResourcesToInsert;
                insert caseMemberToInsert;
                update casesToUpdate;
            }
            //if(Trigger.isUpdate) {}
        }   
    }
}