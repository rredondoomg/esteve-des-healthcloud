/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 04-18-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
trigger ETH_PasoProtocoloTrigger on Paso_Protocolo__c (before delete) {
    Map<Id, Integer> protocolosWithCases;
    List<Id> protocolosIds = new List<Id>();
    for(Paso_Protocolo__c paso: Trigger.old){
        protocolosIds.add(paso.protocolo_Padre__c);
    }
    
    /*
     _         _ _     _          __            
    | |__ _  _| | |__ | |__  ___ / _|___ _ _ ___
    | '_ \ || | | / / | '_ \/ -_)  _/ _ \ '_/ -_)
    |_.__/\_,_|_|_\_\ |_.__/\___|_| \___/_| \___| 
    */       
    if(Trigger.isBefore) {
        // if(Trigger.isInsert) {}
        // if(Trigger.isUpdate) {}
        if(Trigger.isDelete) {
            protocolosWithCases = ETH_ProtocoloTriggerHandler.protocolosWithCases(protocolosIds);
        }
    }

    // if(Trigger.isAfter) {
    //     if(Trigger.isInsert) {}
    //     if(Trigger.isUpdate) {}
    // }
         

    /*                         _           _   _            
         _ _ ___ __ ___ _ _ __| |  __ _ __| |_(_)___ _ _  ___
        | '_/ -_) _/ _ \ '_/ _` | / _` / _|  _| / _ \ ' \(_-<
        |_| \___\__\___/_| \__,_| \__,_\__|\__|_\___/_||_/__/
    */

    for(Paso_Protocolo__c paso : (List<Paso_Protocolo__c>) Trigger.old) {
        if(Trigger.isBefore) {
            if(Trigger.isDelete){
                if(protocolosWithCases.keySet().contains(paso.protocolo_Padre__c)){
                    paso.addError('No es posible eliminar este paso de protocolo debido a que existen tratamientos relacionados.');
                }
            }
        }
    }

    // for(Object__c opp : (List<Object__c>) Trigger.new) {
    //     if(Trigger.isBefore) {
    //         if(Trigger.isInsert) {}
    //         if(Trigger.isUpdate) {}
    //     }

    //     if(Trigger.isAfter) {
    //         if(Trigger.isInsert) {}
    //         if(Trigger.isUpdate) {}
    //     }
    // }


    /*
         _         _ _           __ _          
        | |__ _  _| | |__  __ _ / _| |_ ___ _ _
        | '_ \ || | | / / / _` |  _|  _/ -_) '_|
        |_.__/\_,_|_|_\_\ \__,_|_|  \__\___|_| 
    */
     
    // if(Trigger.isBefore) {
    //     if(Trigger.isInsert) {}
    //     if(Trigger.isUpdate) {}
    // }

    // if(Trigger.isAfter) {
    //     if(Trigger.isInsert) {}
    //     if(Trigger.isUpdate) {}
    // }
}