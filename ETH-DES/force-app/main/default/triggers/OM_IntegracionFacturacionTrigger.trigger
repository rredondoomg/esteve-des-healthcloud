trigger OM_IntegracionFacturacionTrigger on Integracion_Facturacion__c (After update) {

    if(Trigger.isAfter){
        if(Trigger.isUpdate){
            List<AsyncApexJob> batchReprocesarFacturacionSACYL = [SELECT Id, ApexClass.Name, Status 
                                                                    FROM AsyncApexJob 
                                                                    WHERE ApexClass.Name = 'Batch_ReprocesarFacturacionSACYL' AND (Status = 'Processing' OR Status = 'Preparing' OR Status = 'Queued')];
        
            if(batchReprocesarFacturacionSACYL.isEmpty()){
                // system.schedule('Schedule_ReprocesarFacturacionSACYLBatch', '0 0 13/0 16/1  ? ',new Schedule_ReprocesarFacturacionSACYLBatch());


                Boolean reprocesarAlguno = false;

                for (Integracion_Facturacion__c intFactAux : Trigger.new) {
                    if(Trigger.oldMap.get(intFactAux.Id).Reprocesar__c == false && intFactAux.Reprocesar__c == true){
                        reprocesarAlguno = true;
                    }
                }

                if(reprocesarAlguno){

                    DateTime sysTime = System.now();
                    sysTime = sysTime.addMinutes(1);
                    String day = string.valueOf(sysTime.day());
                    String month = string.valueOf(sysTime.month());
                    String minute = string.valueOf(sysTime.minute());
                    String second = string.valueOf(sysTime.second());
                    String year = string.valueOf(sysTime.year());
                    String hour = string.valueOf(sysTime.hour());
                    String strJobName = 'Schedule_ReprocesarFacturacionSACYLBatch' + second + '' + minute + '' + hour + '' + day + '' + month + '_' + year;
                    String chronJobId = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + 
                                            sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
                    Schedule_ReprocesarFacturacionSACYLBatch exe = new Schedule_ReprocesarFacturacionSACYLBatch();
                    System.debug('AQUI ESTA EL CRON '+chronJobId);
                                
                    String idCron = System.schedule(strJobName, chronJobId, exe);
                    System.debug('AQUI ESTA LA ID DEL CRON '+idCron);
                }
            }
        }
    }
}