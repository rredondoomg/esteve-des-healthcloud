/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 04-18-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
trigger ETH_ProtocoloTrigger on protocolo__c (before delete) {
    Map<Id, Integer> protocolosWithCases;
    List<Id> protocolosIds = new List<Id>();
    for(protocolo__c protocolo: Trigger.old){
        protocolosIds.add(protocolo.Id);
    }
    /*
     _         _ _     _          __            
    | |__ _  _| | |__ | |__  ___ / _|___ _ _ ___
    | '_ \ || | | / / | '_ \/ -_)  _/ _ \ '_/ -_)
    |_.__/\_,_|_|_\_\ |_.__/\___|_| \___/_| \___| 
    */       
    if(Trigger.isBefore) {
        // if(Trigger.isInsert) {}
        // if(Trigger.isUpdate) {}
        if(Trigger.isDelete) {
            protocolosWithCases = ETH_ProtocoloTriggerHandler.protocolosWithCases(protocolosIds);
        }
    }

    // if(Trigger.isAfter) {
    //     if(Trigger.isInsert) {}
    //     if(Trigger.isUpdate) {}
    // }
         

    /*                         _           _   _            
         _ _ ___ __ ___ _ _ __| |  __ _ __| |_(_)___ _ _  ___
        | '_/ -_) _/ _ \ '_/ _` | / _` / _|  _| / _ \ ' \(_-<
        |_| \___\__\___/_| \__,_| \__,_\__|\__|_\___/_||_/__/
    */

    for(protocolo__c protocolo : (List<protocolo__c>) Trigger.old) {
        if(Trigger.isBefore) {
            if(Trigger.isDelete){
                if(protocolosWithCases.keySet().contains(protocolo.Id)){
                    protocolo.addError('No es posible eliminar este protocolo debido a que existen tratamientos relacionados.');
                }
            }
        }
    }

    // for(protocolo__c protocolo : (List<protocolo__c>) Trigger.new) {
        // if(Trigger.isBefore) {
            // if(Trigger.isDelete){}
            // if(Trigger.isInsert) {}
            // if(Trigger.isUpdate) {}
        // }

        // if(Trigger.isAfter) {
        //     if(Trigger.isInsert) {}
        //     if(Trigger.isUpdate) {}
        // }
    // }


    /*
         _         _ _           __ _          
        | |__ _  _| | |__  __ _ / _| |_ ___ _ _
        | '_ \ || | | / / / _` |  _|  _/ -_) '_|
        |_.__/\_,_|_|_\_\ \__,_|_|  \__\___|_| 
    */
     
    // if(Trigger.isBefore) {
    //     if(Trigger.isInsert) {}
    //     if(Trigger.isUpdate) {}
    // }

    // if(Trigger.isAfter) {
    //     if(Trigger.isInsert) {}
    //     if(Trigger.isUpdate) {}
    // }
}