/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 05-30-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
trigger ETH_EmailMessageTrigger on EmailMessage (after insert) {
    List<Case> casesToUpdate = new List<Case>();
    List<Id> caseIds = new List<Id>();
    for(EmailMessage emailMessage : (List<EmailMessage>) Trigger.new){
        caseIds.add(emailMessage.ParentId);
    }
    List<Tipificado_Casos__mdt> tipificadoCasos;
    Map<Id, Case> cases;
    /*
    _         _ _     _          __            
    | |__ _  _| | |__ | |__  ___ / _|___ _ _ ___
    | '_ \ || | | / / | '_ \/ -_)  _/ _ \ '_/ -_)
    |_.__/\_,_|_|_\_\ |_.__/\___|_| \___/_| \___| 
    */       
    // if(Trigger.isBefore) {
        //     if(Trigger.isInsert) {}
        //     if(Trigger.isUpdate) {}
        // }
        
        if(Trigger.isAfter) {
            if(Trigger.isInsert) {
                cases = new Map<Id, Case>([SELECT Id, Priority, Subtipo__c, Type, Territorio__c, Subject, Description, RecordTypeId FROM Case WHERE Id IN :caseIds]);
                tipificadoCasos = ETH_EmailMessageTriggerHandler.getTipificadoCasos();
            }
        // if(Trigger.isUpdate) {}
    }
         

    /*                         _           _   _            
         _ _ ___ __ ___ _ _ __| |  __ _ __| |_(_)___ _ _  ___
        | '_/ -_) _/ _ \ '_/ _` | / _` / _|  _| / _ \ ' \(_-<
        |_| \___\__\___/_| \__,_| \__,_\__|\__|_\___/_||_/__/
    */
    for(EmailMessage emailMessage : (List<EmailMessage>) Trigger.new) {
        // if(Trigger.isBefore) {
        //     if(Trigger.isInsert) {}
        //     if(Trigger.isUpdate) {}
        // }

        if(Trigger.isAfter) {
            if(Trigger.isInsert) {
                if(cases.containsKey(emailMessage.ParentId)){
                    Case ca  = ETH_EmailMessageTriggerHandler.tipificarCasoEmail(tipificadoCasos, cases.get(emailMessage.ParentId), emailMessage);
                    if(ca != null){
                        casesToUpdate.add(ca);
                    }
                }
            }
            // if(Trigger.isUpdate) {}
        }
    }


    /*
         _         _ _           __ _          
        | |__ _  _| | |__  __ _ / _| |_ ___ _ _
        | '_ \ || | | / / / _` |  _|  _/ -_) '_|
        |_.__/\_,_|_|_\_\ \__,_|_|  \__\___|_| 
    */
     
    // if(Trigger.isBefore) {
    //     if(Trigger.isInsert) {}
    //     if(Trigger.isUpdate) {}
    // }

    if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            update casesToUpdate;
        }
        // if(Trigger.isUpdate) {}
    }
}