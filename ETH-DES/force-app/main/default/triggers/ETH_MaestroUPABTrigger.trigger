/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 06-08-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
trigger ETH_MaestroUPABTrigger on Maestro_UPAB__c (before delete, before update) {
    List<Id> upabIds = new List<Id>();
    Map<Id, Integer> upabWithCases = new Map<Id, Integer>();

    /*
     _         _ _     _          __            
    | |__ _  _| | |__ | |__  ___ / _|___ _ _ ___
    | '_ \ || | | / / | '_ \/ -_)  _/ _ \ '_/ -_)
    |_.__/\_,_|_|_\_\ |_.__/\___|_| \___/_| \___| 
    */       
    if(Trigger.isBefore) {
        // if(Trigger.isInsert) {}
        if(Trigger.isDelete || Trigger.isUpdate) {
            for(Maestro_UPAB__c upab: Trigger.old){
                upabIds.add(upab.Id);
            }

            for(AggregateResult ar: [SELECT Upab__c, COUNT(Id)n_tratamientos FROM Case WHERE Upab__c IN :upabIds GROUP BY Upab__c]){
                String upabId = String.valueOf(ar.get('Upab__c'));
                Integer cases = Integer.valueOf(ar.get('n_tratamientos'));
                upabWithCases.put(upabId, cases);
            }
        }
    }

    // if(Trigger.isAfter) {
    //     if(Trigger.isInsert) {}
    //     if(Trigger.isUpdate) {}
    // }
         

    /*                         _           _   _            
         _ _ ___ __ ___ _ _ __| |  __ _ __| |_(_)___ _ _  ___
        | '_/ -_) _/ _ \ '_/ _` | / _` / _|  _| / _ \ ' \(_-<
        |_| \___\__\___/_| \__,_| \__,_\__|\__|_\___/_||_/__/
    */
    
    List<Maestro_UPAB__c> maestros = Trigger.isDelete ? Trigger.old : Trigger.new;

    for(Maestro_UPAB__c upab : (List<Maestro_UPAB__c>) maestros) {
        if(Trigger.isBefore) {
            if(Trigger.isDelete) {
                if(upabWithCases.keySet().contains(upab.Id)){
                    upab.addError('No es posible eliminar este Maestro UPAB debido a que existen tratamientos relacionados.');
                }
            }
            if(Trigger.isUpdate){
                if(upabWithCases.keySet().contains(upab.Id)){
                    if(Trigger.oldMap.get(upab.Id).Name != upab.Name){
                        upab.addError('No es posible cambiar el nombre de este Maestro UPAB debido a que existen tratamientos relacionados.');
                    }
                    else if(Trigger.oldMap.get(upab.Id).Abreviacion__c != null && Trigger.oldMap.get(upab.Id).Abreviacion__c != upab.Abreviacion__c){
                        upab.addError('No es posible cambiar la abreviación de este Maestro UPAB debido a que existen tratamientos relacionados.');
                    }
                }
            }
        }

        // if(Trigger.isAfter) {
        //     if(Trigger.isInsert) {}
        //     if(Trigger.isUpdate) {}
        // }
    }


    /*
         _         _ _           __ _          
        | |__ _  _| | |__  __ _ / _| |_ ___ _ _
        | '_ \ || | | / / / _` |  _|  _/ -_) '_|
        |_.__/\_,_|_|_\_\ \__,_|_|  \__\___|_| 
    */
     
    // if(Trigger.isBefore) {
    //     if(Trigger.isInsert) {}
    //     if(Trigger.isUpdate) {}
    // }

    // if(Trigger.isAfter) {
    //     if(Trigger.isInsert) {}
    //     if(Trigger.isUpdate) {}
    // }
}