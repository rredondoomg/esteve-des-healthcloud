/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 06-06-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
trigger ETH_ClienteTrigger on Cliente__c (before update) {
    List<Id> clienteIds = new List<Id>();
    Map<Id, Integer> clienteWithCases = new Map<Id, Integer>();

    /*
     _         _ _     _          __            
    | |__ _  _| | |__ | |__  ___ / _|___ _ _ ___
    | '_ \ || | | / / | '_ \/ -_)  _/ _ \ '_/ -_)
    |_.__/\_,_|_|_\_\ |_.__/\___|_| \___/_| \___| 
    */       
    if(Trigger.isBefore) {
        // if(Trigger.isInsert) {}
        if(Trigger.isUpdate) {
            for(Cliente__c cliente: Trigger.new){
                clienteIds.add(cliente.Id);
            }

            for(AggregateResult ar: [SELECT Cliente__c, COUNT(Id)n_tratamientos FROM Case WHERE Cliente__c IN :clienteIds GROUP BY Cliente__c]){
                String clienteId = String.valueOf(ar.get('Cliente__c'));
                Integer cases = Integer.valueOf(ar.get('n_tratamientos'));
                clienteWithCases.put(clienteId, cases);
            }
        }
    }

    // if(Trigger.isAfter) {
    //     if(Trigger.isInsert) {}
    //     if(Trigger.isUpdate) {}
    // }
         

    /*                         _           _   _            
         _ _ ___ __ ___ _ _ __| |  __ _ __| |_(_)___ _ _  ___
        | '_/ -_) _/ _ \ '_/ _` | / _` / _|  _| / _ \ ' \(_-<
        |_| \___\__\___/_| \__,_| \__,_\__|\__|_\___/_||_/__/
    */
    for(Cliente__c cliente : (List<Cliente__c>) Trigger.new) {
        if(Trigger.isBefore) {
            // if(Trigger.isInsert) {}
            if(Trigger.isUpdate) {
                if(clienteWithCases.keySet().contains(cliente.Id)){
                    if(Trigger.oldMap.get(cliente.Id).Cod_cliente__c != cliente.Cod_cliente__c){
                        cliente.addError('No es posible cambiar el código de cliente debido a que tiene tratamientos relacionados.');
                    }
                }
            }
        }

        // if(Trigger.isAfter) {
        //     if(Trigger.isInsert) {}
        //     if(Trigger.isUpdate) {}
        // }
    }


    /*
         _         _ _           __ _          
        | |__ _  _| | |__  __ _ / _| |_ ___ _ _
        | '_ \ || | | / / / _` |  _|  _/ -_) '_|
        |_.__/\_,_|_|_\_\ \__,_|_|  \__\___|_| 
    */
     
    // if(Trigger.isBefore) {
    //     if(Trigger.isInsert) {}
    //     if(Trigger.isUpdate) {}
    // }

    // if(Trigger.isAfter) {
    //     if(Trigger.isInsert) {}
    //     if(Trigger.isUpdate) {}
    // }
}