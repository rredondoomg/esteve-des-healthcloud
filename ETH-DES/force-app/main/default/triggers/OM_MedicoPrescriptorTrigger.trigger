trigger OM_MedicoPrescriptorTrigger on Med_Presc__c (before delete) {

    if(Trigger.isBefore){
        if(Trigger.isDelete){
            OM_MedicoPrescriptorTrigger_Handler.beforeDelete(Trigger.oldMap);
        }
    }
}