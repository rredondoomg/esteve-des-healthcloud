/**
 * @description       : 
 * @author            : rredondo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 30-08-2022
 * @last modified by  : rredondo@omegacrmconsulting.com
**/
trigger ETH_UserTrigger on User (after insert) {


    /*
        _         _ _     _          __            
    | |__ _  _| | |__ | |__  ___ / _|___ _ _ ___
    | '_ \ || | | / / | '_ \/ -_)  _/ _ \ '_/ -_)
    |_.__/\_,_|_|_\_\ |_.__/\___|_| \___/_| \___| 
    */    




    /*                         _           _   _            
            _ _ ___ __ ___ _ _ __| |  __ _ __| |_(_)___ _ _  ___
        | '_/ -_) _/ _ \ '_/ _` | / _` / _|  _| / _ \ ' \(_-<
        |_| \___\__\___/_| \__,_| \__,_\__|\__|_\___/_||_/__/
    */



    /*
            _         _ _           __ _          
        | |__ _  _| | |__  __ _ / _| |_ ___ _ _
        | '_ \ || | | / / / _` |  _|  _/ -_) '_|
        |_.__/\_,_|_|_\_\ \__,_|_|  \__\___|_| 
    */


    if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            
            //Recogemos los Permisssion Set a dar de alta por defecto
            List<PermissionSet> insertListPermissionSet= [SELECT id FROM PermissionSet WHERE Name in (
            'SDocs_User','Salesforce_Scheduler_Resource',
            'HealthCloudSocialDeterminants','HealthCloudAppointmentManagement',
            'HealthCloudMemberServices','HealthCloudFoundation',
            'HealthCloudUtilizationManagement','HealthCloudAdmin',
            'HealthCloudPermissionSetLicense','HealthCloudStandard','OM_ContentUser',
            'ServiceUserPsl','New_Salesforce_Mobile_App')];

            List<PermissionSetAssignment> insertListPermissionSetAssignment = new List<PermissionSetAssignment>();
            //List<ServiceResource> insertListServiceResource= new List<ServiceResource>();
            for (user app : (List<user>) Trigger.new){
                for (PermissionSet insertPermissionSet: insertListPermissionSet ){
                    System.debug('+++app.Id'+app.Id);
                    System.debug('+++app.Name'+app.Name);
                    System.debug('++++insertPermissionSet.ID'+insertPermissionSet.Id);
                    PermissionSetAssignment insertPermissionSetAssignment = new PermissionSetAssignment (PermissionSetId = insertPermissionSet.Id, AssigneeId = app.Id);
                    insertListPermissionSetAssignment.add(insertPermissionSetAssignment);
                }
                // ServiceResource insertServiceResource = new ServiceResource(
                //     ResourceType='T',
                //     RelatedRecordId=app.Id,
                //     Name= app.LastName
                // );
                //insertListServiceResource.add(insertServiceResource);
            }

            //insert insertListServiceResource;
            if (!test.isRunningTest()){ insert insertListPermissionSetAssignment;}
        }
    }

}