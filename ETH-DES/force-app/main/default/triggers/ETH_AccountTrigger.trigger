/**
 * @description       : Account Trigger
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 05-17-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
trigger ETH_AccountTrigger on Account (after insert, after update, before insert) {
    Trigger_Controller__mdt triggerController = ETH_Utils.getTriggerController();
    if(triggerController.Account_Trigger__c){
        List<Id> accountsId = new List<Id>();
    
        /*
         _         _ _     _          __            
        | |__ _  _| | |__ | |__  ___ / _|___ _ _ ___
        | '_ \ || | | / / | '_ \/ -_)  _/ _ \ '_/ -_)
        |_.__/\_,_|_|_\_\ |_.__/\___|_| \___/_| \___| 
        */       
        // if(Trigger.isBefore) {
        //     if(Trigger.isInsert) {}
        //     if(Trigger.isUpdate) {}
        // }
    
        if(Trigger.isAfter) {
            if(Trigger.isInsert) {
                for(Account account: Trigger.new){
                    if(account.ShippingLatitude != null && account.ShippingLongitude != null){
                        accountsId.add(account.Id);
                    }
                }
            }
            if(Trigger.isUpdate) {
                for(Account account: Trigger.new){
                    if(Trigger.oldMap.get(account.Id).ShippingLatitude != account.ShippingLatitude || Trigger.oldMap.get(account.Id).ShippingLongitude != account.ShippingLongitude){
                        accountsId.add(account.Id);
                    }
                }
            }
        }
             
    
        /*                         _           _   _            
             _ _ ___ __ ___ _ _ __| |  __ _ __| |_(_)___ _ _  ___
            | '_/ -_) _/ _ \ '_/ _` | / _` / _|  _| / _ \ ' \(_-<
            |_| \___\__\___/_| \__,_| \__,_\__|\__|_\___/_||_/__/
        */
        for(Account acc : (List<Account>) Trigger.new) {
            // if(Trigger.isBefore) {
            //     if(Trigger.isInsert) {
            //     }
            //     if(Trigger.isUpdate) {}
            // }
    
            if(Trigger.isAfter) {
                if(Trigger.isInsert) {
                    if(triggerController.Create_Folder_Account_Trigger__c){
                        ETH_AccountTriggerHandler.createFolderSharePoint(acc.IdAnonimo__c);
                    }
                }
                if(Trigger.isUpdate) {
                    if(triggerController.Create_Folder_Account_Trigger__c){
                        if((Trigger.oldMap.get(acc.Id).Crear_Carpeta_Sharepoint_Manual__c != acc.Crear_Carpeta_Sharepoint_Manual__c) && (acc.Crear_Carpeta_Sharepoint_Manual__c == true)){
                            ETH_AccountTriggerHandler.createFolderSharePoint(acc.IdAnonimo__c);
                        }
                    }
                }
            }
        }
    
    
        /*
             _         _ _           __ _          
            | |__ _  _| | |__  __ _ / _| |_ ___ _ _
            | '_ \ || | | / / / _` |  _|  _/ -_) '_|
            |_.__/\_,_|_|_\_\ \__,_|_|  \__\___|_| 
        */
         
        // if(Trigger.isBefore) {
        //     if(Trigger.isInsert) {}
        //     if(Trigger.isUpdate) {}
        // }
    
        if(!accountsId.isEmpty()){
            if(Trigger.isAfter) {
                if(Trigger.isInsert || Trigger.isUpdate) {
                    ETH_AccountTriggerHandler.setZonasConflictivas(accountsId);
                }
            }
        }
    }
}