trigger OM_ContentTrigger on OM_Content__c (before insert, before update) {
  //TODO : Move to custom metadata
  Integer numberOfTagsToProcess = 2;

  // Fill OM_Tag_n__c with most relevant tags to be able to pull it from reports and dashboards
  for(sObject cont : Trigger.new) {
    //Strip content
    if(cont.get('OM_Tags__c') != null) {
      if(cont.get('OM_Tags__c').toString().contains('#')){
        List<String> tags = cont.get('OM_Tags__c').toString().trim().split('#');
        //Check first element blank
        if(tags.get(0) == '') {
          tags.remove(0);
        }
  
        for(Integer i = 0; i < numberOfTagsToProcess; i++) {
          if(tags.size() >= i+1) {
            String fieldname = 'OM_Tag_'+ (i+1) +'__c';
            cont.put(fieldname, tags.get(i));
          }
        }
      }
    } else {
      //Clear content if tags has been deleted
      for(Integer i = 0; i < numberOfTagsToProcess; i++) {
        cont.put('OM_Tag_'+ (i+1) +'__c', '');
      }
    }
  }
}