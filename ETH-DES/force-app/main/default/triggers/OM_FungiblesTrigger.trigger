trigger OM_FungiblesTrigger on Fungibles_sum__c (after update) {

    if(Trigger.isAfter){
        if(Trigger.isUpdate){
            OM_FungiblesTrigger_Handler.enviarFungibleAtlas(Trigger.new, Trigger.oldMap);
            OM_FungiblesTrigger_Handler.enviarFungibleCorreosExpressHermes(Trigger.new, Trigger.oldMap);
        }
    }
   
}