/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 07-13-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
trigger ContenedorTrigger on Contenedor__c (before update) {
    List<Id> userIds = new List<Id>();
    List<Id> usersWithContenedor = new List<Id>();
    /*
     _         _ _     _          __            
    | |__ _  _| | |__ | |__  ___ / _|___ _ _ ___
    | '_ \ || | | / / | '_ \/ -_)  _/ _ \ '_/ -_)
    |_.__/\_,_|_|_\_\ |_.__/\___|_| \___/_| \___| 
    */       
    if(Trigger.isBefore) {
        if(Trigger.isUpdate) {
            for(Contenedor__c contenedor : (List<Contenedor__c>) Trigger.new) {
                userIds.add(contenedor.Usuario_asignado__c);
            }

            for(Contenedor__c contenedor: [SELECT Id, Usuario_asignado__c FROM Contenedor__c WHERE Usuario_asignado__c IN :userIds]){
                usersWithContenedor.add(contenedor.Usuario_asignado__c);
            }
        }
    }
         

    /*                         _           _   _            
         _ _ ___ __ ___ _ _ __| |  __ _ __| |_(_)___ _ _  ___
        | '_/ -_) _/ _ \ '_/ _` | / _` / _|  _| / _ \ ' \(_-<
        |_| \___\__\___/_| \__,_| \__,_\__|\__|_\___/_||_/__/
    */
    for(Contenedor__c contenedor : (List<Contenedor__c>) Trigger.new) {
        if(Trigger.isBefore) {
            if(Trigger.isUpdate) {
                if(usersWithContenedor.contains(contenedor.Usuario_asignado__c) && (contenedor.Usuario_asignado__c != null) && (Trigger.oldMap.get(contenedor.Id).Usuario_asignado__c != contenedor.Usuario_asignado__c)){
                    contenedor.addError('Este usuario ya cuenta con un contenedor asignado.');
                }
            }
        }
    }


    /*
         _         _ _           __ _          
        | |__ _  _| | |__  __ _ / _| |_ ___ _ _
        | '_ \ || | | / / / _` |  _|  _/ -_) '_|
        |_.__/\_,_|_|_\_\ \__,_|_|  \__\___|_| 
    */
}