/**
 * @description       : 
 * @author            : fizquierdo@omegacrmconsulting.com
 * @group             : 
 * @last modified on  : 04-27-2022
 * @last modified by  : fizquierdo@omegacrmconsulting.com
**/
trigger ETH_TratamientoTrigger on tratamiento__c (before delete) {
    Map<Id, Integer> tratamientosWithCases;
    List<Id> tratamientosId = new List<Id>();
    for(tratamiento__c tratamiento: Trigger.old){
        tratamientosId.add(tratamiento.Id);
    }
    /*
     _         _ _     _          __            
    | |__ _  _| | |__ | |__  ___ / _|___ _ _ ___
    | '_ \ || | | / / | '_ \/ -_)  _/ _ \ '_/ -_)
    |_.__/\_,_|_|_\_\ |_.__/\___|_| \___/_| \___| 
    */       
    if(Trigger.isBefore) {
        if(Trigger.isDelete){
            tratamientosWithCases = ETH_ProtocoloTriggerHandler.tratamientosWithCases(tratamientosId);
        }
        // if(Trigger.isInsert) {}
        // if(Trigger.isUpdate) {}
    }

    // if(Trigger.isAfter) {
    //     if(Trigger.isInsert) {}
    //     if(Trigger.isUpdate) {}
    // }
         

    /*                         _           _   _            
         _ _ ___ __ ___ _ _ __| |  __ _ __| |_(_)___ _ _  ___
        | '_/ -_) _/ _ \ '_/ _` | / _` / _|  _| / _ \ ' \(_-<
        |_| \___\__\___/_| \__,_| \__,_\__|\__|_\___/_||_/__/
    */
    for(tratamiento__c tratamiento : (List<tratamiento__c>) Trigger.old) {
        if(Trigger.isBefore) {
            if(Trigger.isDelete){
                if(tratamientosWithCases.keySet().contains(tratamiento.Id)){
                    tratamiento.addError('No es posible eliminar un Tipo de tratamiento con tratamientos asociados.');
                }
            }
        }

        // if(Trigger.isAfter) {
        //     if(Trigger.isInsert) {}
        //     if(Trigger.isUpdate) {}
        // }
    }


    /*
         _         _ _           __ _          
        | |__ _  _| | |__  __ _ / _| |_ ___ _ _
        | '_ \ || | | / / / _` |  _|  _/ -_) '_|
        |_.__/\_,_|_|_\_\ \__,_|_|  \__\___|_| 
    */
     
    // if(Trigger.isBefore) {
    //     if(Trigger.isInsert) {}
    //     if(Trigger.isUpdate) {}
    // }

    // if(Trigger.isAfter) {
    //     if(Trigger.isInsert) {}
    //     if(Trigger.isUpdate) {}
    // }
}