/**
 * Lightning Out app for LWC
 * @module OmLightningOutBridge
 * @memberof Aura
 */

  /**
 * Lightning Out app for LWC
 * @name OmLightningOutBridge.Controller
 */
({
    /**
     * This is my test action
     * @param {object} component The component
     * @param {object} event The event
     * @param {object} helper The JS helper
     */
    myAction : function(component, event, helper) {

    }
})