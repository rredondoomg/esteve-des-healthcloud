/**
 * Lightning Out app for LWC
 * @module OmLightningOutBridge
 * @memberof Aura
 */

 /**
 * Lightning Out app for LWC
 * @name OmLightningOutBridge.Helper
 */
({
    /**
     * This is a helper method
     */
    helperMethod : function() {

    }
})